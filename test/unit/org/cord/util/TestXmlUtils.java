package org.cord.util;

import static org.cord.util.XmlUtils.encodeName;
import static org.cord.util.XmlUtils.isValidName;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

@Test
public class TestXmlUtils
{
  // ==========================================================================
  // isValidName

  public void isValidName_validNamesAreAccepted()
  {
    assertTrue(isValidName("a"));
    assertTrue(isValidName("_a"));
    assertTrue(isValidName("a_"));
    assertTrue(isValidName("a-"));
    assertTrue(isValidName("a1"));
  }

  public void isValidName_leadingDigitsAndDashesAreRejected()
  {
    assertFalse(isValidName("1"));
    assertFalse(isValidName("1a"));
    assertFalse(isValidName("1-"));
    assertFalse(isValidName("1-a"));
    assertFalse(isValidName("-"));
    assertFalse(isValidName("-a"));
    assertFalse(isValidName("-1"));
    assertFalse(isValidName("-1a"));
  }

  public void isValidName_emptyStringsAreRejected()
  {
    assertFalse(isValidName(""));
  }

  private static final String[] INVALID_NAMES = {
      // Basic Latin
      ":a", "a:", ":a", "a:a", "&a", "a&", "&a", "a&a",

      // Latin-1 supplement
      "£a", "a£", "£a", "a£a",

      // Supplemental encodings
      "\uD834\uDD22a", "a\uD834\uDD22", "\uD834\uDD22a", "a\uD834\uDD22a" };

  public void isValidName_invalidCharactersAreRejected()
  {
    for (String name : INVALID_NAMES) {
      assertFalse(isValidName(name), name);
    }
  }

  // ==========================================================================
  // encodeName

  public void encodeName_emptyStringsAreEncodedToValidNames()
  {
    assertTrue(isValidName(encodeName("")));
  }

  public void encodeName_invalidNamesAreEncodedToValidNames()
  {
    for (String name : INVALID_NAMES) {
      assertTrue(isValidName(encodeName(name)), name);
    }
  }

  public void encodeName_uniqueNamesAreEncodedUniquely()
  {
    String[][] names = {
        // This will fail with encoders that "flatten" invalid characters to
        // a single valid one
        { "a&b", "a@b" },
        // Check that things that look like encodings don't pass
        // straight through
        { "*", "002a" }, { "*", "_002a" }, { "*", "__002a" } };

    for (String[] name : names) {
      assertTrue(isValidName(encodeName(name[0])), name[0]);
      assertTrue(isValidName(encodeName(name[1])), name[1]);
      assertTrue(!encodeName(name[0]).equals(encodeName(name[1])),
                 name[0] + " encodes the same as " + name[1]);
    }
  }
}
