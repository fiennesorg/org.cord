package org.cord.util;

import static java.util.Arrays.deepEquals;
import static org.cord.util.ParamsAccumulator.accumulate;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

public class TestParamsAccumulator
{

  static class TestParams
    implements ParamsAccumulator.Invoker<Object>
  {
    @Override
    public Object invoke(Object[] params)
    {
      return params;
    }
  }

  public final TestParams __testParams = new TestParams();

  private Object get(Object params,
                     String key)
  {
    return ((SimpleGettable) params).get(key);
  }

  @Test
  public void zeroArgumentsWorks()
  {
    Object params = accumulate(0, __testParams);
    assertTrue(deepEquals((Object[]) params, new Object[] {}));
  }

  @Test
  public void oneArgumentWorks()
  {
    Object params = accumulate(1, __testParams);
    assertTrue(deepEquals((Object[]) get(params, "1"), new Object[] { "1" }));
  }

  @Test
  public void twoArgumentsWork()
  {
    Object params = accumulate(2, __testParams);
    params = get(params, "2");
    params = get(params, "1");
    assertTrue(deepEquals((Object[]) params, new Object[] { "2", "1" }),
               ((Object[]) params).toString());
  }

  @Test
  public void threeArgumentsWork()
  {
    Object params = accumulate(3, __testParams);
    params = get(params, "2");
    params = get(params, "3");
    params = get(params, "1");
    assertTrue(deepEquals((Object[]) params, new Object[] { "2", "3", "1" }),
               ((Object[]) params).toString());
  }

  @Test
  public void curriedCallingHasSeparateState()
  {
    Object params1 = accumulate(3, __testParams);

    params1 = get(params1, "2");
    Object params2 = get(params1, "4");
    params2 = get(params2, "5");
    params1 = get(params1, "3");
    params1 = get(params1, "1");

    assertTrue(deepEquals((Object[]) params1, new Object[] { "2", "3", "1" }),
               ((Object[]) params1).toString());

    assertTrue(deepEquals((Object[]) params2, new Object[] { "2", "4", "5" }),
               ((Object[]) params1).toString());
  }
}
