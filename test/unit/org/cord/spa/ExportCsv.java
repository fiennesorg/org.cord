package org.cord.spa;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cord.mirror.DbBooter;
import org.cord.sql.ConnectionPool;

import com.google.common.base.Joiner;
import com.google.common.io.Closer;
import com.opencsv.CSVWriter;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public class ExportCsv
{
  public static void main(String[] args)
      throws IOException, SQLException
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Closer closer = Closer.create();
    try {
      Connection connection = null;
      try {
        connection =
            DriverManager.getConnection(ConnectionPool.getConnectionRequest("127.0.0.1",
                                                                            DbBooter.INITPARAM_PROTOCOL_DEFAULT,
                                                                            "Spa_beta",
                                                                            "root",
                                                                            "seeekwell",
                                                                            "utf-8"));
        Writer writer = closer.register(new FileWriter("/Users/alex/cvsroot/spa-tmp/SPA-BFS.csv"));
        CSVWriter csvWriter = new CSVWriter(writer);
        writeNext(csvWriter,
                  "isbn",
                  "title",
                  "subTitle",
                  "sortTitle",
                  "imprint",
                  "pubDate",
                  "format",
                  "author",
                  "hasGardners",
                  "hasStock",
                  "extent",
                  "textLanguage",
                  "isOutOfPrint",
                  "slug",
                  "longDescription",
                  "BIC",
                  "SPA");
        Statement bdsBookStatement = connection.createStatement();
        ResultSet bdsBookResults =
            bdsBookStatement.executeQuery("select id,isbn,title,subTitle,sortTitle,imprint,pubDate,format,author,hasGardners,hasStock,extent,textLanguage,isOutOfPrint,bdsSlug,spaSlug,longDescription from BdsBook");
        Statement bicStatement = connection.createStatement();
        while (bdsBookResults.next()) {
          ResultSet bicResults =
              bicStatement.executeQuery("select BicCategory.id, BicCategory.title, BicCategory.code from BicCategory,BdsBookBicCategory where BdsBookBicCategory.bdsBook_id="
                                        + bdsBookResults.getInt("id")
                                        + " and BdsBookBicCategory.isDefined!=0 and BdsBookBicCategory.bicCategory_id=BicCategory.id");

          StringBuilder bicBuf = new StringBuilder();
          IntSet bicIds = new IntOpenHashSet();
          while (bicResults.next()) {
            if (bicBuf.length() > 0) {
              bicBuf.append("\n");
            }
            bicBuf.append(bicResults.getString("BicCategory.code"))
                  .append(": ")
                  .append(bicResults.getString("BicCategory.title"));
            bicIds.add(bicResults.getInt("BicCategory.id"));
          }
          String bicCategories = bicBuf.toString();
          bicResults.close();
          String spaCategories;
          if (bicIds.size() > 0) {
            System.out.println(bicIds);
            ResultSet spaResults =
                bicStatement.executeQuery("select SpaCategory.title from SpaCategory, BicCategory where BicCategory.spaCategory_id=SpaCategory.id and BicCategory.id in ("
                                          + Joiner.on(',').join(bicIds)
                                          + ") group by SpaCategory.id");
            StringBuilder spaBuf = new StringBuilder();
            while (spaResults.next()) {
              if (spaBuf.length() > 0) {
                spaBuf.append("/n");
              }
              spaBuf.append(spaResults.getString("SpaCategory.title"));
            }
            spaCategories = spaBuf.toString();
            System.out.println(spaCategories);
          } else {
            spaCategories = "";
          }
          writeNext(csvWriter,
                    bdsBookResults.getString("isbn"),
                    bdsBookResults.getString("title"),
                    bdsBookResults.getString("subTitle"),
                    bdsBookResults.getString("sortTitle"),
                    bdsBookResults.getString("imprint"),
                    sdf.format(new Date(bdsBookResults.getLong("pubDate"))),
                    bdsBookResults.getString("format"),
                    bdsBookResults.getString("author"),
                    bool(bdsBookResults.getInt("hasGardners")),
                    bool(bdsBookResults.getInt("hasStock")),
                    bdsBookResults.getString("extent"),
                    bdsBookResults.getString("textLanguage"),
                    bool(bdsBookResults.getInt("isOutOfPrint")),
                    slug(bdsBookResults.getString("bdsSlug"), bdsBookResults.getString("spaSlug")),
                    bdsBookResults.getString("longDescription"),
                    bicCategories,
                    spaCategories);
        }
      } finally {
        if (connection != null) {
          connection.close();
        }
      }
    } finally {
      closer.close();
    }
  }

  private static void writeNext(CSVWriter csvWriter,
                                String... values)
  {
    csvWriter.writeNext(values);
  }

  private static String bool(int i)
  {
    return Boolean.toString(i != 0);
  }

  private static String slug(String bdsSlug,
                             String spaSlug)
  {
    return spaSlug.length() == 0 ? bdsSlug : spaSlug;
  }
}
