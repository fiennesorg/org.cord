package org.cord.mirror.field;

import java.util.Date;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.DbBooter;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.NullUnsupportedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.util.DateQuantiser.Granularity;
import org.cord.util.SettableMap;

public class TestFastDateField
{
  /*
   * create database TestFastDateField; create table Test ( id int not null, timestamp bigint not
   * null);
   */
  public TestFastDateField()
      throws MirrorTableLockedException, MirrorInstantiationException, NullUnsupportedException,
      MirrorFieldException
  {
    String transactionPassword = Db.createTransactionPassword();
    Db db = DbBooter.getDb(DbBooter.INITPARAM_JDBCDRIVERNAME_DEFAULT,
                           DbBooter.INITPARAM_PROTOCOL_DEFAULT,
                           "127.0.0.1",
                           "TestFastDateField",
                           "root",
                           "seeekwell",
                           transactionPassword,
                           1,
                           10,
                           1000,
                           0.5f,
                           Thread.NORM_PRIORITY + 1,
                           "utf-8",
                           new SettableMap());
    final ObjectFieldKey<Date> TIMESTAMP = FastDateField.createKey("timestamp");
    db.add(new AbstractTableWrapper("Test") {
      @Override
      protected void initTable(Db db,
                               String pwd,
                               Table table)
          throws MirrorTableLockedException
      {
        table.addField(new FastDateField(TIMESTAMP,
                                         "Timestamp",
                                         DateSupplierNow.getInstance(),
                                         Granularity.SECOND));
      }
    });
    db.preLock();
    db.lock();
    Table test = db.getTable("Test");
    TransientRecord a = test.createTransientRecord();
    System.out.println(a.get("timestamp"));
    Date t = new Date(10000);
    System.out.println(t);
    a.setField("timestamp", t);
    PersistentRecord b = a.makePersistent(null);
    System.out.println(b.get("timestamp"));

    db.shutdown(true);
  }

  public static void main(String[] args)
      throws Exception
  {
    new TestFastDateField();
  }
}
