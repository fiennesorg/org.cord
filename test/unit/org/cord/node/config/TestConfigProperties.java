package org.cord.node.config;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestConfigProperties
{
  private ConfigProperties _props = new ConfigProperties();
  private ConfigProperties _production;

  @BeforeClass
  public void setUp()
  {
    _props.put("org.cord.node.db.name", "theDbName");

    _props.put("org.cord.db.user", "globalUser");
    _props.put("org.cord.db.type", "globalType");
    _props.put("org.cord.template.loader", "globalLoader");
    _props.put("org.cord.node.production.db.user", "productionUser");

    _props.put("org.cord.node.password", "globalPassword");
    _props.put("org.cord.node.production.logging", "productionLogging");

    _props.put("org.cord.node.caching", "productionCaching");
    _props.put("org.cord.node.production.caching", "productionCaching");

    _production = _props.getTree("org.cord.node.production");

    System.out.println(_production);
  }

  @Test
  public void plainUsageWorks()
  {
    assertEquals(_props.get("org.cord.node.db.name"), "theDbName");
    assertNull(_props.get("org.cord.node.name"));
  }

  @Test
  public void qualifiedPropsAreFoundInParent()
  {
    assertEquals(_production.get("db.user"), "productionUser");
    assertEquals(_production.get("db.type"), "globalType");
    assertEquals(_production.get("db.password"), "globalPassword");
    assertEquals(_production.get("template.loader"), "globalLoader");
    assertNull(_production.get("user"));
  }

  @Test
  public void unqualifiedPropsAreFoundInParent()
  {
    assertEquals(_production.get("password"), "globalPassword");
    assertEquals(_production.get("logging"), "productionLogging");
    assertNull(_production.get("user"), null);
  }

  @Test
  public void invalidQualifiedPropsAreNotFoundAsUnqualifiedPropsInParent()
  {
    assertNull(_production.get("file.password"), null);
  }

  @Test
  public void parentPropsAreOverriddenByChild()
  {
    assertEquals(_production.get("caching"), "productionCaching");
  }

  @Test
  public void getPropertiesReturnsValidData()
  {
    Properties props = _production.getProperties();
    assertEquals(props.getProperty("logging"), "productionLogging");
    assertEquals(props.getProperty("db.user"), "productionUser");
    assertEquals(props.getProperty("db.type"), "globalType");
  }

  @Test
  public void simplePropertyReferencesWork()
  {
    ConfigProperties props = new ConfigProperties();
    props.put("ref1", "val1");
    props.put("ref2", "ref1: ${ref1}  ref3: ${ref3}");
    props.put("ref3", "val3");

    assertEquals("val1", props.get("ref1"));
    assertEquals("ref1: val1  ref3: val3", props.get("ref2"));
    assertEquals("val3", props.get("ref3"));
  }

  @Test
  public void parentPropertyReferencesWork()
  {
    ConfigProperties props = new ConfigProperties();
    props.put("a.ref1", "val1");
    props.put("a.b.ref2", "ref1: ${ref1}");
    assertEquals("ref1: val1", props.get("a.b.ref2"));
  }

  @Test
  public void nonExistentTreesAreReplacedByParents()
  {
    ConfigProperties props = new ConfigProperties();
    props.put("foo.bar.val", "foo.bar.val");
    assertEquals("foo.bar.val", props.getTree("foo.bar.baz.qux").get("val"));
  }

  @Test
  public void expansionIsRecursive()
  {
    ConfigProperties props = new ConfigProperties();
    props.put("val1", "foo");
    props.put("val2", "val");
    props.put("val3", "${${val2}1}");
    assertEquals("foo", props.get("val3"));
  }

  @Test(expectedExceptions = { org.cord.util.LogicException.class })
  public void expansionDoesNotRecurseInfinitely()
  {
    ConfigProperties props = new ConfigProperties();

    props.put("a", "${a}");
    props.get("a");
  }

  @Test
  public void mergingTreesCopiesValues()
  {
    ConfigProperties src = new ConfigProperties();
    src.put("a.b.c", "src.c");
    src.put("a.b", "src.b");

    ConfigProperties dst = new ConfigProperties();
    dst.put("a.b.c", "dst.c");
    dst.put("a.b", "dst.b");
    dst.put("a.b1", "dst.b1");

    dst.merge(src);

    assertEquals("src.c", dst.get("a.b.c"));
    assertEquals("src.b", dst.get("a.b"));
    assertEquals("dst.b1", dst.get("a.b1"));

    src.put("a.b.c", "src.c2");
    assertEquals("src.c", dst.get("a.b.c"));
  }

  @Test
  public void defaultValuesAreSubstituted()
  {
    ConfigProperties props = new ConfigProperties();
    props.put("val1", "foo");
    props.put("val2", "${val1:-bar}");
    assertEquals("foo", props.get("val2"));
    props.put("val3", "${val0:-bar}");
    assertEquals("bar", props.get("val3"));
  }

  @Test
  public void treeReferencesWork()
  {
    ConfigProperties props = new ConfigProperties();

    props.put("node.deployment.foo.appserver.libpath", "deployment-libpath");
    props.put("node.deployment.foo.bar.appserver.port", "deployment-port");
    props.link("config.deployment", props.getTree("node.deployment.foo.bar.baz"));

    ConfigProperties production = props.getTree("node.production");

    assertEquals(production.get("config.deployment.appserver.port"), "deployment-port");
    assertNull(production.get("config.deployment.appserver.libpath"));
    assertEquals(production.get("::config.deployment::appserver.port"), "deployment-port");
    assertEquals(production.get("::config.deployment::appserver.libpath"), "deployment-libpath");

    props.put("node.appserver.port", "${::config.deployment::appserver.port}");
    assertEquals(production.get("appserver.port"), "deployment-port");
  }
}
