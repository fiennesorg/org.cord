create table AsBigDecimal (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id), 
  regionStyle_id int not null,
  value float not null, index (value)
);
