-- WordDensityIndex Tables

drop table if exists AsWord;
create table AsWord (
  id int not null auto_increment, primary key (id),
  word varchar(255) not null, index (word),
  hash int not null
);

drop table if exists AsRankedWord;
create table AsRankedWord (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id), 
  regionStyle_id int not null,
  asWord_id int not null,
  density int not null
);