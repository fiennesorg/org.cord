-- Create the new columns
alter table Node
  add description varchar(255) not null,
  add isNoIndex int not null,
  add isNoFollow int not null;

-- Migrate existing data, ignoring descriptions that are identical to the page
-- title
delete from RegionStyle where compilerName="HtmlMetaTag";

update Node, HtmlMetaTag
  set Node.description = HtmlMetaTag.description
  where Node.id = HtmlMetaTag.node_id
  and Node.title != HtmlMetaTag.description;

drop table HtmlMetaTag;
