alter table Node add nodeGroupStyle_id int not null;
update Node,NodeGroup,NodeGroupStyle set Node.nodeGroupStyle_id=NodeGroupStyle.id where Node.parentNodeGroup_id = NodeGroup.id and NodeGroup.nodeGroupStyle_id = NodeGroupStyle.id;
alter table Node drop parentNodeGroup_id;
drop table NodeGroup;
alter table RegionStyle drop isFull;
alter table RegionStyle drop isSummary;
alter table RegionStyle drop minimum;
alter table RegionStyle drop maximum;

alter table BinaryAssetInstance add node_id int not null after region_id;
alter table BinaryAssetInstance add index(node_id);
alter table BinaryAssetInstance add regionStyle_id int not null after node_id;
alter table BinaryAssetInstance add index(regionStyle_id);
update BinaryAssetInstance,Region,Node set BinaryAssetInstance.node_id=Node.id,BinaryAssetInstance.regionStyle_id=Region.regionStyle_id where BinaryAssetInstance.region_id = Region.id and Region.node_id = Node.id;
alter table BinaryAssetInstance drop region_id;

alter table HtmlInstance add node_id int not null after region_id;
alter table HtmlInstance add index(node_id);
alter table HtmlInstance add regionStyle_id int not null after node_id;
alter table HtmlInstance add index(regionStyle_id);
update HtmlInstance,Region,Node set HtmlInstance.node_id=Node.id,HtmlInstance.regionStyle_id=Region.regionStyle_id where HtmlInstance.region_id = Region.id and Region.node_id = Node.id;
alter table HtmlInstance drop region_id;

alter table HtmlMetaTag add node_id int not null after region_id;
alter table HtmlMetaTag add index(node_id);
alter table HtmlMetaTag add regionStyle_id int not null after node_id;
alter table HtmlMetaTag add index(regionStyle_id);
update HtmlMetaTag,Region,Node set HtmlMetaTag.node_id=Node.id,HtmlMetaTag.regionStyle_id=Region.regionStyle_id where HtmlMetaTag.region_id = Region.id and Region.node_id = Node.id;
alter table HtmlMetaTag drop region_id;

alter table NtmlInstance add node_id int not null after region_id;
alter table NtmlInstance add index(node_id);
alter table NtmlInstance add regionStyle_id int not null after node_id;
alter table NtmlInstance add index(regionStyle_id);
update NtmlInstance,Region,Node set NtmlInstance.node_id=Node.id,NtmlInstance.regionStyle_id=Region.regionStyle_id where NtmlInstance.region_id = Region.id and Region.node_id = Node.id;
alter table NtmlInstance drop region_id;

alter table SimpleTextInstance add node_id int not null after region_id;
alter table SimpleTextInstance add index(node_id);
alter table SimpleTextInstance add regionStyle_id int not null after node_id;
alter table SimpleTextInstance add index(regionStyle_id);
update SimpleTextInstance,Region,Node set SimpleTextInstance.node_id=Node.id,SimpleTextInstance.regionStyle_id=Region.regionStyle_id where SimpleTextInstance.region_id = Region.id and Region.node_id = Node.id;
alter table SimpleTextInstance drop region_id;

alter table SingleImageInstance add node_id int not null after region_id;
alter table SingleImageInstance add index(node_id);
alter table SingleImageInstance add regionStyle_id int not null after node_id;
alter table SingleImageInstance add index(regionStyle_id);
update SingleImageInstance,Region,Node set SingleImageInstance.node_id=Node.id,SingleImageInstance.regionStyle_id=Region.regionStyle_id where SingleImageInstance.region_id = Region.id and Region.node_id = Node.id;
alter table SingleImageInstance drop region_id;

drop table Region;