update Node as N, TimedItem as T
  set N.description = T.listingSummary
  where N.id = T.node_id;
alter table TimedItem
  drop listingSummary;
