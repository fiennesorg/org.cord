-- Tables for TimedItems
drop table if exists TimedItem;
create table TimedItem (
    -- TableContentCompilerDb
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    title varchar(255) not null, index(title),
    isPublished int not null,
    -- TimedItemDb
    listingSummary varchar(255) not null,
    sortDate bigint not null
);
