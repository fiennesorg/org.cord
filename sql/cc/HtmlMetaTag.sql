drop table if exists HtmlMetaTag;
create table HtmlMetaTag (
  -- TableContentCompilerDb
  id int not null auto_increment, primary key (id),
  node_id int not null, index (node_id),
  regionStyle_id int not null, index (regionStyle_id),
  style_id int not null,
  isDefined int not null,

  -- HtmlMetaTagDb
  pageKeywords varchar(255) not null,
  cascadingKeywords varchar(255) not null
);
