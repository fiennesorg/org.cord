-- Tables for ArchivedTimedItems

drop table if exists ArchivedTimedItem;

create table ArchivedTimedItem (
    -- TableContentCompilerDb
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    title varchar(255) not null, index(title),
    isPublished int not null,

    -- TimedItemDb
    listingSummary varchar(255) not null,
    sortDate bigint not null,
    archiveDate bigint not null,
    parentNode_id int not null
);

-- Dummy value to make child menu node code work
insert into ArchivedTimedItem values (-1,0,0,0,0,'ArchivedTimedItemJoin',0,'',0,0,0);
