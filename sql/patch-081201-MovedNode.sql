drop table if exists MovedNode;
create table MovedNode (
  id int not null auto_increment, primary key (id),
  parentNode_id int not null,
  oldFilename varchar(255) not null,
    index (parentNode_id, oldFilename),
  movedNode_id int not null,
    index (movedNode_id),
  timestamp bigint not null
);
