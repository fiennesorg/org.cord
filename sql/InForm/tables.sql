-- Content Template Table creation script
-- default implementation has no custom fields hence empty...

drop table if exists InFormConfig;
create table InFormConfig (
	id int not null auto_increment, primary key (id),
	node_id int not null, index (node_id),
	regionStyle_id int not null,
	style_id int not null, 
	isDefined int not null,
	data mediumtext not null
);

