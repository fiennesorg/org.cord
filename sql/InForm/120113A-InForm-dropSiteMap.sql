-- ensure that you have deleted any SiteMap pages using the CMS **before** running this script.

delete from NodeStyle where id = 35;
delete from NodeGroupStyle where id = 35;
delete from RegionStyle where nodeStyle_id = 35;

delete from NodeGroupStyle where parentNodeStyle_id = 35 or childNodeStyle_id = 35;