-- InForm framework bootup SQL

insert into BinaryAssetStyle values (1, 'gntmlAsset',1,0,'/dyn',1,0,1,'',1,'',0);

insert into ScalableSingleImageStyle values (0, 'pageImgOpt',1,0,'default','{}');
insert into ScalableSingleImageStyle values (0, 'pageImgComp',0,1,'default','{}');

insert into User values (4,'system',  '','system',  md5('system'),  0,'',1,0,'','',1,'{}');
insert into User values (5,'designer','','designer',md5('designer'),0,'',1,0,'','',1,'{}');
insert into User values (6,'editor',  '','editor',  md5('editor'),  0,'',1,0,'','',1,'{}');
insert into User values (7,'preview', '','preview', md5('preview'), 0,'',1,0,'','',1,'{}');
update User set password='forbidden' where id = 3;

insert into Acl values (5,'editor',1,0,1);
insert into UserAcl values (0,1,5), (0,4,5), (0,5,5), (0,6,5);
insert into Acl values (6,'designer',1,0,1);
insert into UserAcl values (0,1,6), (0,4,6), (0,5,6);
insert into Acl values (7,'system',1,0,1);
insert into UserAcl values (0,1,7), (0,4,7);

insert into Acl values (10,'Create main content pages',1,0,1);
-- 11
insert into Acl values (12,'Create main timed-page listings',1,0,1);
insert into Acl values (13,'Create main image-page listings',1,0,1);
insert into Acl values (14,'Create main index listings',1,0,1);
-- 15

insert into Acl values (20,'Create deep content pages',1,0,1);
-- 21
insert into Acl values (22,'Create deep timed-page listings',1,0,1);
insert into Acl values (23,'Create deep image-page listings',1,0,1);
insert into Acl values (24,'Create deep index listings',1,0,1);
-- 25

insert into Acl values (30,'Configure timed-page listings',1,0,1);
insert into Acl values (31,'Configure image-page listings',1,0,1);
insert into Acl values (32,'Configure index listings',1,0,1);
insert into Acl values (33,'Edit grid proportions',1,0,1);

-- Home
insert into NodeGroupStyle values (1,'Home', 0,1, 1,1, 1, 1,1, 0,0);
insert into NodeStyle values (1,'Home', '',0, 1, 2,1, 5,1, 1,1, 1,1, '', 'InForm/home.wm.html', 'InForm/home.view.wm.html','InForm/page.edit.wm.html',0,1,1,1);
insert into RegionStyle values (0,1,1,'gridDefinition','','Grid Definition','GridDefinition','compulsory',33,0);
insert into RegionStyle values (0,1,1,'inFormConfig','','InForm Configuration','InFormConfig','compulsory',33,0);

-- <content>
insert into NodeGroupStyle values (2,'Content', 1,2, 10,1, 1, 0,-1, 0,0);
insert into NodeStyle values (2,'Content', '',1, -1, 2,1, 5,1, 10,1, 10,1, '','','InForm/content.view.wm.html','InForm/page.edit.wm.html',0,0,1,1);

-- Search
insert into NodeGroupStyle values (4,'Search', 1,4, 1,1, 99, 1,1, 0,0);
insert into NodeStyle values (4,'Search', 'Search',0, 1, 2,1, 5,1, 1,1, 1,1, '', '', 'InForm/search.view.wm.html','InForm/page.edit.wm.html',0,1,1,0);

-- <timedPageListing>
insert into NodeGroupStyle values (10,'Feed', 1,10, 12,1, 1, 0,-1, 0,0);
insert into NodeStyle values (10,'Feed', '',1, -1, 2,1, 5,1, 22,1, 22,1, '','','InForm/timedPageListing.view.wm.html','InForm/page.edit.wm.html',0,0,1,1);
insert into RegionStyle values (0,10,1,'timedPageListing','','Feed Configuration','TimedPageListing','compulsory',30,0);

  -- <timedPage>
  insert into NodeGroupStyle values (11, 'Feed Item', 10,11, 5,1, 1, 0,-1, 0,0);
  insert into NodeStyle values (11,'Feed Item', '',1, -1, 2,1, 5,1, 5,1, 5,1, '','','InForm/timedPageListing/timedPage.view.wm.html','InForm/page.edit.wm.html',0,0,1,0);
  insert into RegionStyle values (0,11,1,'timedPage','','Feed Item Configuration','TimedPage','compulsory',0,0);
  
  -- RSS
  insert into NodeGroupStyle values (13,'rss.xml', 10,13, 1,1, 1, 1,1, 0,0);
  insert into NodeStyle values (13,'rss.xml', 'rss.xml',0, -1, 2,1, 1,1, 1,1, 1,1, '','InForm/timedPageListing/rss.wm.xml','InForm/timedPageListing/rss.wm.xml','InForm/page.edit.wm.html',1,1,1,0);

-- RSS
insert into NodeGroupStyle values (14,'rss.xml', 1,14, 1,1, 1, 1,1, 0,0);
insert into NodeStyle values (14,'rss.xml', 'rss.xml',0, -1, 2,1, 1,1, 1,1, 1,1, '','InForm/rss.wm.xml','InForm/rss.wm.xml','InForm/page.edit.wm.html',1,1,1,0);    

-- <imagePageListing>
insert into NodeGroupStyle values (20,'Gallery', 1,20, 13,1, 1, 0,-1, 0,0);
insert into NodeStyle values (20,'Gallery', '',1, -1, 2,1, 5,1, 23,1, 23,1, '','','InForm/imagePageListing.view.wm.html','InForm/page.edit.wm.html',0,0,1,1);
insert into RegionStyle values (0,20,1,'imagePageListing','','Gallery Configuration','ImagePageListing','compulsory',31,0);

  -- <imagePage>
  insert into NodeGroupStyle values (21, 'Picture', 20,21, 5,1, 1, 0,-1, 0,0);
  insert into NodeStyle values (21,'Picture', '',1, -1, 2,1, 5,1, 5,1, 5,1, '','','InForm/imagePageListing/imagePage.view.wm.html','InForm/page.edit.wm.html',0,0,1,0);
  insert into RegionStyle values (0,21,1,'imagePage','','Picture Configuration','ImagePage','compulsory',0,0);

  -- RSS
  insert into NodeGroupStyle values (24,'rss.xml', 20,24, 1,1, 1, 1,1, 0,0);
  insert into NodeStyle values (24,'rss.xml', 'rss.xml',0, -1, 2,1, 1,1, 1,1, 1,1, '','InForm/imagePageListing/rss.wm.xml','InForm/imagePageListing/rss.wm.xml','InForm/page.edit.wm.html',1,1,1,0);

-- <indexListing>
insert into NodeGroupStyle values (30,'Index Category', 1,30, 14,1, 1, 0,-1, 0,0);
insert into NodeStyle values (30,'Index Category', '',1, -1, 2,1, 5,1, 24,1, 24,1, '','','InForm/indexListing.view.wm.html','InForm/page.edit.wm.html', 0,0,1,1);
insert into RegionStyle values (0,30,1,'indexListing','','Index Category Configuration','IndexListing','compulsory',32,0);

  -- <indexValue>
  insert into NodeGroupStyle values (31,'Index Value', 30,31, 32,1, 1, 0,-1, 0,0);
  insert into NodeStyle values (31,'Index Value', '',1, -1, 2,1, 5,1, 32,1, 32,1, '','','InForm/indexListing/indexValue.view.wm.html','InForm/page.edit.wm.html', 0,0,1,0);
  insert into RegionStyle values (0,31,1,'indexValue','','Index Value Configuration','IndexValue','compulsory',32,0);

-- sitemap.xml
insert into NodeGroupStyle values (36, 'sitemap.xml', 1,36, 1,1, 1, 1,1, 0,0);
insert into NodeStyle values (36, 'sitemap.xml', 'sitemap.xml',0, -1, 2,1, 1,1, 1,1, 1,1, 'InForm/sitemap.wm.xml','InForm/sitemap.wm.xml','InForm/sitemap.wm.xml','InForm/page.edit.wm.html',1,1,1,0);


-- Login
insert into NodeGroupStyle values (1000,'LoginGroup', 1,1000, 1,1, 99, 1,1, 0,0);
insert into NodeStyle values (1000,'Login','Login',0, 1, 2,1, 5,1, 1,1, 1,1, '','','InForm/login.view.wm.html','InForm/page.edit.wm.html', 0,1,1,0);

-- 404
insert into NodeGroupStyle values (1001,'FourOhFourGroup',1,1001, 1,1, 99, 1,1,0,0);
insert into NodeStyle values (1001,'FourOhFour','404',1, 1, 2,1, 5,1, 1,1, 1,1, '','','InForm/404.view.wm.html','InForm/page.edit.wm.html',0,1,1,0);

-- feedback error
insert into NodeGroupStyle values (1002,'FeedbackGroup',1,1002, 1,1, 99, 1,1, 0,0);
insert into NodeStyle values (1002,'Feedback','Feedback',0, 1, 2,1, 1,1, 1,1, 1,1, '','','InForm/feedback.view.wm.html','InForm/page.edit.wm.html',0,1,1,0);

--- CSS
insert into NodeGroupStyle values (1003,'CSS',1,1003, 1,1, 99, 1,1, 0,0);
insert into NodeStyle values (1003,'CSS','grid.css',0, 1, 2,1, 1,1, 1,1, 1,1, '','InForm/grid.wm.css','InForm/grid.wm.css','',1,1,0,0);
