drop table if exists InFormConfig;
create table InFormConfig (
	id int not null auto_increment, primary key (id),
	node_id int not null, index (node_id),
	regionStyle_id int not null,
	style_id int not null, 
	isDefined int not null,
	dataSrc mediumtext not null
);

insert into RegionStyle values (0,1,1,'inFormConfig','','InForm Configuration','InFormConfig','compulsory',33,0);
insert into InFormConfig values (1,1,last_insert_id(),3,1, '{}');