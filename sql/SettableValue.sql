# table creation script for working with the
# org.cord.mirror.SettableValue cache.

drop table if exists SettableValue;
create table SettableValue (
        id int not null auto_increment,
                primary key (id),
        namespace varchar(32) not null,
        valueKey varchar(255) not null,
                index(namespace,valueKey),
        value varchar(255),
        originalValue varchar(255)
);
