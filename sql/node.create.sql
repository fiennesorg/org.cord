-- database definition for Node;

drop table if exists NodeLink;

drop table if exists Node;

create table Node (
  id int not null auto_increment, primary key (id),
  parentNode_id int not null, index (parentNode_id),
  number int not null,
  title varchar(255) not null,
  htmlTitle varchar(255) not null,
  filename varchar(255) not null, index (filename),
  isPublished int not null, index (isPublished),
  creationTime bigint not null,
  modificationTime bigint not null,
  nodeStyle_id int not null, index (nodeStyle_id),
  owner_id int not null,
  viewAcl_id int not null,
  editAcl_id int not null,
  publishAcl_id int not null,
  deleteAcl_id int not null,
  nodeGroupStyle_id int not null, index (nodeGroupStyle_id),
  description text not null,
  isNoIndex int not null,
  isNoFollow int not null,
  isMenuItem int not null,
  isSearchable int not null,
  json mediumtext not null
);

drop table if exists NodeGroup;

drop table if exists NodeRegion;
drop table if exists Region;

drop table if exists NodeStyle;

create table NodeStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  filename varchar(255) not null, 
  isEditableFilename int not null,
  owner_id int not null,
  viewAcl_id int not null,
  updateViewAcl_id int not null,
  editAcl_id int not null,
  updateEditAcl_id int not null,
  publishAcl_id int not null,
  updatePublishAcl_id int not null,
  deleteAcl_id int not null,
  updateDeleteAcl_id int not null,
  cssPath varchar(255) not null,
  rootTemplatePath varchar(255) not null,
  viewTemplatePath varchar(255) not null,
  editTemplatePath varchar(255) not null,
  isRenderReset int not null,
  defaultIsPublished int not null,
  mayEditPublished int not null,
  isMenuItem int not null);

drop table if exists NodeGroupStyle;

create table NodeGroupStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, 
  parentNodeStyle_id int not null,
  childNodeStyle_id int not null, 
  createChildAcl_id int not null,
  updateCreateChildAcl_id int not null,
  number int not null, index (parentNodeStyle_id, name, number),
  minimum int not null,
  maximum int not null,
  isUpdateLinked int not null,
  isDeleteLinked int not null);

drop table if exists RegionStyle;

create table RegionStyle (
  id int not null auto_increment, primary key (id),
  nodeStyle_id int not null, 
  number int not null, 
  name varchar(32) not null, index (nodeStyle_id, name, number),
  aliasName varchar(32) not null,
  title varchar(255) not null,
  compilerName varchar(255) not null,
  compilerStyleName varchar(255) not null,
  editAcl_id int not null,
  updateEditAcl_id int not null
);

drop table if exists UserStyle;

create table UserStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null,
  hasPlainTextPassword int not null,
  hasPasswordClue int not null,
  mayDelete int not null,
  isMultiSession int not null);

insert into UserStyle values (1, 'Paranoid',       0, 0, 0, 0);
insert into UserStyle values (2, 'HighSecurity',   0, 0, 1, 0);
insert into UserStyle values (3, 'MediumSecurity', 0, 1, 1, 0);
insert into UserStyle values (4, 'LowSecurity',    1, 1, 1, 0);
insert into UserStyle values (5, 'Anonymous',      0, 0, 0, 1);

drop table if exists User;

create table User (
  id int not null auto_increment, primary key (id),
  name varchar(255) not null,
  email varchar(255) not null,
  login varchar(255) not null, index (login),
  password varchar(255) not null,
  creationDate bigint not null,
  ipFilter varchar(255) not null,
  status int not null,
  secretCode bigint not null,
  plainTextPassword varchar(255) not null,
  passwordClue varchar(255) not null,
  userStyle_id int not null, index(userStyle_id),
  json mediumtext not null
);

insert into User values (1,'root','webmaster@localhost','root',md5('as234b2'),0,'127.0.0.1/32',1,0,'','',1, '{}');
insert into User values (2,'anonymous','webmaster@localhost','anonymous',md5(''),0,'',1,0,'','',5, '{}');
insert into User values (3,'Core Administrator','','core',md5('core'),0,'',1,0,'','',1, '{}');

drop table if exists UserAcl;

create table UserAcl (
  id int not null auto_increment, primary key (id),
  user_id int not null, index (user_id),
  acl_id int not null, index (acl_id));

insert into UserAcl values (0,1,1);
insert into UserAcl values (0,2,3);
insert into UserAcl Values (0,1,4);
insert into UserAcl values (0,3,4);

drop table if exists Acl_Acl;

create table Acl_Acl (
  id int not null auto_increment, primary key (id),
  containerAcl_id int not null, index (containerAcl_id),
  nestedAcl_id int not null, index (nestedAcl_id)
);

drop table if exists Acl;

create table Acl (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  isInclusiveAcl int not null,
  includeOwner int not null,
  editAcl_id int not null);

insert into Acl values (1,'None',1,0,1);
insert into Acl values (2,'Public',0,0,1);
insert into Acl values (3,'Any logged in',0,0,1);
insert into Acl values (4,'coreAdminOnly',1,0,1);

drop table if exists MovedNode;
create table MovedNode (
  id int not null auto_increment, primary key (id),
  parentNode_id int not null,
  oldFilename varchar(255) not null,
    index (parentNode_id, oldFilename),
  movedNode_id int not null,
    index (movedNode_id),
  timestamp bigint not null
);

-- SimpleText Content Compiler

drop table if exists SimpleTextInstance;

create table SimpleTextInstance (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  text text not null,
  html text not null);

drop table if exists SimpleTextStyle;

create table SimpleTextStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  isOptional int not null,
  isDefined int not null,
  maxLength int not null,
  supportsLinebreaks int not null,
  editorRows int not null,
  editorColumns int not null,
  editCss varchar(255) not null,
  viewCss varchar(255) not null,
  isBlock int not null);

insert into SimpleTextStyle values (1, 'default', 0, 1, 255, 0, 1, 40, 'defaultFont', 'defaultFont', 0);
insert into SimpleTextStyle values (2, 'optional', 1, 0, 255, 0, 1, 40, 'defaultFont', 'defaultFont', 0);

drop table if exists SingleImageInstance;

create table SingleImageInstance (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  webpath varchar(128) not null,
  width int not null,
  height int not null,
  alt varchar(255) not null,
  format_id int not null, 
  sizeKb int not null,
  css varchar(32) not null);

drop table if exists SingleImageStyle;

create table SingleImageStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  isOptional int not null,
  isDefined int not null,
  defaultFilename varchar(255) not null,
  defaultWidth int not null,
  defaultHeight int not null,
  targetWidth int not null,
  targetHeight int not null,
  imageScalingMode int not null,
  webpathRoot varchar(255) not null,
  defaultAlt varchar(255) not null,
  hasEditableAlt int not null,
  format_id int not null, 
  isFixedFormat int not null,
  css varchar(32) not null);

-- insert into SingleImageStyle values (0,'default', 0, 1, 'default.png',64,64,64,64,3,'/images/default/','SingleImageInstance',1, 1, 0, '');
-- insert into SingleImageStyle values (0,'optional', 1, 0, 'default.png',64,64,64,64,3,'/images/default/','SingleImageInstance',1, 1, 0, '');

drop table if exists SingleImageFormat;

create table SingleImageFormat (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null,
  imParameter varchar(255) not null,
  imFormat varchar(255) not null,
  extension varchar(255) not null);

insert into SingleImageFormat values (1, 'PNG: full colour', '', 'PNG', '.png');
insert into SingleImageFormat values (2, 'GIF: 256 colour (dithered)', '-dither', 'GIF', '.gif');
insert into SingleImageFormat values (3, 'GIF: 256 colour (no-dither)', '+dither', 'GIF', '.gif');
insert into SingleImageFormat values (4, 'JPEG: (low quality)', '-quality 50', 'JPEG', '.jpeg');
insert into SingleImageFormat values (5, 'JPEG: (medium quality)', '-quality 75', 'JPEG', '.jpeg');
insert into SingleImageFormat values (6, 'JPEG: (high quality)', '-quality 95', 'JPEG', '.jpeg');

drop table if exists HtmlInstance;

create table HtmlInstance (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  html text not null);

drop table if exists HtmlStyle;

create table HtmlStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32), index (name),
  isOptional int not null,
  isDefined int not null,
  textareaCss varchar(255) not null,
  textareaCols int,
  textareaRows int);

insert into HtmlStyle values (0, 'default', 0, 1, 'defaultFont', 40, 8);
insert into HtmlStyle values (0, 'optional', 1, 0, 'defaultFont', 40, 8);

drop table if exists BinaryAssetFormat;

create table BinaryAssetFormat (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null,
  extension varchar(255) not null,
  mimetype varchar(255) not null,
  iconPath varchar(255) not null,
  iconWidth int not null,
  iconHeight int not null);

insert into BinaryAssetFormat values (1, 'Adobe PDF', '.pdf', 'application/pdf','/node/icons/pdf.gif',24,24);
insert into BinaryAssetFormat values (2, 'JPEG Image', '.jpeg', 'image/jpeg','/node/icons/jpg.gif',24,24);
insert into BinaryAssetFormat values (3, 'JPG Image', '.jpg', 'image/jpeg','/node/icons/jpg.gif',24,24);
insert into BinaryAssetFormat values (4, 'Gif Image', '.gif', 'image/gif','/node/icons/gif.gif',24,24);
insert into BinaryAssetFormat values (5, 'PNG Image', '.png', 'image/png','/node/icons/png.gif',24,24);
insert into BinaryAssetFormat values (6, 'Tiff Image', '.tiff', 'image/tiff','/node/icons/tif.gif',24,24);
insert into BinaryAssetFormat values (7, 'Tif Image', '.tif', 'image/tiff','/node/icons/tif.gif',24,24);
insert into BinaryAssetFormat values (8, 'PSD Image', '.psd', 'image/x-photoshop','/node/icons/psd.gif',24,24);
insert into BinaryAssetFormat values (9, 'Shockwave Flash', '.swf', 'application/x-shockwave-flash','/node/icons/swf.gif',24,24);
insert into BinaryAssetFormat values (10, 'Quicktime Movie', '.mov', 'video/quicktime','/node/icons/mov.gif',24,24);
insert into BinaryAssetFormat values (11, 'MPEG Movie', '.mpg', 'video/mpeg','/node/icons/mpg.gif',24,24);
insert into BinaryAssetFormat values (12, 'MP3 Audio', '.mp3', 'audio/mpeg','/node/icons/mp3.gif',24,24);
insert into BinaryAssetFormat values (13, 'MS Word Document', '.doc', 'application/msword','/node/icons/doc.gif',24,24);
insert into BinaryAssetFormat values (14, 'MS Excel Spreadsheet', '.xls', 'application/excel','/node/icons/xls.gif',24,24);
insert into BinaryAssetFormat values (15, 'MS Powerpoint Presentation', '.ppt', 'application/vnd.ms-powerpoint','/node/icons/ppt.gif',24,24);
insert into BinaryAssetFormat values (16, 'Comma Separated Values', '.csv', 'text/plain','/node/icons/csv.gif',24,24);
insert into BinaryAssetFormat values (17, 'Plain Text Document', '.txt', 'text/plain','/node/icons/txt.gif',24,24);
insert into BinaryAssetFormat values (18, 'Rich Text Format Document', '.rtf', 'text/rtf','/node/icons/rtf.gif',24,24);
insert into BinaryAssetFormat values (19, 'Stuffit Archive', '.sit', 'application/x-stuffit','/node/icons/sit.gif',24,24);
insert into BinaryAssetFormat values (20, 'Zip Archive', '.zip', 'application/zip','/node/icons/zip.gif',24,24);
insert into BinaryAssetFormat values (21, 'TGZ Archive', '.tgz', 'application/x-gtar','/node/icons/tgz.gif',24,24);
insert into BinaryAssetFormat values (22, 'GZIP Archive', '.gz', 'application/x-gzip','/node/icons/gzip.gif',24,24);
insert into BinaryAssetFormat values (23, 'TAR Archive', '.gz', 'application/x-tar','/node/icons/tar.gif',24,24);
insert into BinaryAssetFormat values (24, 'MPEG-4 Movie', '.mp4', 'video/mpeg','/node/icons/mpg.gif',24,24);
insert into BinaryAssetFormat values (25, 'Windows Media Video', '.wmv', 'video/x-ms-wvx', 'node/icons/wmv.gif', 24,24);
insert into BinaryAssetFormat values (26, 'Real Audio', '.ra','audio/x-realaudio','node/icon/ra.gif',24,24);
insert into BinaryAssetFormat values (27, 'Flash Video', '.flv','video/x-flv','node/icon/flv.gif',24,24);

insert into BinaryAssetFormat values (100,'Unspecified', '', '', '', 0,0);


drop table if exists BinaryAssetStyle;

create table BinaryAssetStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  isOptional int not null,
  isDefined int not null,
  folder varchar(255) not null,
  format_id int not null, index(format_id),
  isFixedFormat int not null,
  hasTitle int not null,
  defaultTitle varchar(255) not null,
  hasDescription int not null,
  defaultDescription varchar(255) not null,
  maxSizeKb int not null);

-- insert into BinaryAssetStyle values (1,'default',0,1,'/assets',1,0,1,'Binary Asset Title',1,'Binary Asset Description',256);
-- insert into BinaryAssetStyle values (2,'optional',1,0,'/assets',1,0,1,'Binary Asset Title',1,'Binary Asset Description',256);

drop table if exists BinaryAssetInstance;

create table BinaryAssetInstance (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null, 
  isUploaded int not null,
  path varchar(255) not null,
  folder varchar(255) not null,
  filename varchar(255) not null,
  format_id int not null, 
  sizeKb int not null,
  title varchar(255) not null,
  description varchar(255) not null);

drop table if exists AbstractStyle;

create table AbstractStyle (
	id int not null auto_increment, primary key (id),
	name char(32) not null, index (name),
	isOptional int not null,
	isDefined int not null);

insert into AbstractStyle values (1, 'default', 0, 1);
insert into AbstractStyle values (2, 'optional', 1, 0);
insert into AbstractStyle values (3, 'compulsory', 0,1);
insert into AbstractStyle values (4, 'optional_defined', 1,1);


drop table if exists WordIndex;

create table WordIndex (
	id int not null auto_increment, 
		primary key (id),
	wordHash int not null,
		index (wordHash),
	node_id int not null,
		index (node_id));

drop table if exists SettableValue;
create table SettableValue (
        id int not null auto_increment,
                primary key (id),
        namespace varchar(32) not null,
        valueKey varchar(255) not null,
                index(namespace,valueKey),
        value varchar(255),
        originalValue varchar(255)
);

drop table if exists FormAt;
create table FormAt (
	id int not null auto_increment, primary key (id),
	node_id int not null, index (node_id),
	regionStyle_id int not null, index (regionStyle_id),
	style_id int not null, 
	isDefined int not null, 
	json mediumtext not null,
	outputFormat varchar(255) not null,
	output text not null
);