alter table Disqus change dataSrc data mediumtext not null;
alter table InFormConfig change dataSrc data mediumtext not null;
alter table ImagePage change dataSrc data mediumtext not null;
alter table ImagePageListing change dataSrc data mediumtext not null;
alter table TimedPage change dataSrc data mediumtext not null;
alter table TimedPageListing change dataSrc data mediumtext not null;
alter table Node change jsonSrc json mediumtext not null;
alter table User change jsonSrc json mediumtext not null;
