drop table if exists FormAt;
create table FormAt (
	id int not null auto_increment, primary key (id),
	node_id int not null, index (node_id),
	regionStyle_id int not null, index (regionStyle_id),
	style_id int not null, 
	isDefined int not null, 
	jsonSource text not null,
	outputFormat varchar(255) not null,
	output text not null
);