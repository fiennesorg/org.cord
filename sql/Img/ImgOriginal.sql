drop table if exists ImgOriginal;
create table ImgOriginal(
  id int not null auto_increment, primary key(id),
  width int not null,
  height int not null,
  sizeKb int not null,
  webPath varchar(255) not null,
  imgFormat_id int not null,
  filename varchar(255) not null,
  timestamp bigint not null,
  title varchar(255) not null,
  description varchar(255) not null,
  replacementImgOriginal_id int not null,
  md5 varchar(255),
  data mediumtext not null,
  owner varchar(255) not null, index (owner),
  croppedFromImgOriginal_id int not null, index (croppedFromImgOriginal_id)
);