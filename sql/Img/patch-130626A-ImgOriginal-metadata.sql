alter table ImgOriginal add filename varchar(255) not null;
alter table ImgOriginal add timestamp bigint not null;
alter table ImgOriginal add title varchar(255) not null;
alter table ImgOriginal add description varchar(255) not null;
alter table ImgOriginal add replacementImgOriginal_id int not null;
alter table ImgOriginal add md5 varchar(255);

update ImgOriginal set filename = substring(webpath,  length(webpath) + 2 - locate("/", reverse(webpath)));
update ImgOriginal set title = filename;
update ImgOriginal set timestamp = id * 60000;
