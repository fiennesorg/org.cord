drop table if exists ImgOriginal;
create table ImgOriginal(
  id int not null auto_increment, primary key(id),
  width int not null,
  height int not null,
  sizeKb int not null,
  webPath varchar(255) not null,
  imgFormat_id int not null
);