drop table if exists ImgFormat;
create table ImgFormat (
  id int not null auto_increment, primary key(id),
  name varchar(255) not null,
  englishName varchar(255) not null,
  imParameter varchar(255) not null,
  imFormat varchar(255) not null,
  extension varchar(255) not null,
  isDefaultConfig int not null,
  isDefinedFormat int not null
);

insert into ImgFormat values (1, 'PNG', 'PNG: full colour', '', 'PNG', '.png', 1, 1);
insert into ImgFormat values (2, 'GIF_DITHER', 'GIF: 256 colour (dithered)', '-dither FloydSteinberg', 'GIF', '.gif', 1, 1);
insert into ImgFormat values (3, 'GIF_NODITHER', 'GIF: 256 colour (no-dither)', '+dither', 'GIF', '.gif', 0, 1);
insert into ImgFormat values (4, 'JPEG_LOW', 'JPEG: (low quality)', '-quality 50', 'JPEG', '.jpeg', 0, 1);
insert into ImgFormat values (5, 'JPEG_MEDIUM', 'JPEG: (medium quality)', '-quality 75', 'JPEG', '.jpeg', 1, 1);
insert into ImgFormat values (6, 'JPEG_HIGH', 'JPEG: (high quality)', '-quality 95', 'JPEG', '.jpeg', 0, 1);
insert into ImgFormat values (7, 'UNKNOWN', 'Unknown format', '', '', '', 1, 0);
