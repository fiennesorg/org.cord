drop table if exists Acl_Acl;

create table Acl_Acl (
  id int not null auto_increment, primary key (id),
  containerAcl_id int not null, index (containerAcl_id),
  nestedAcl_id int not null, index (nestedAcl_id)
);
