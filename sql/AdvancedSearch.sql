-- core population script for the AdvancedSearch framework;
-- Alex Fiennes;

drop table if exists AsContentCompiler;
create table AsContentCompiler (
  id int not null auto_increment, primary key (id),
  name varchar(255) not null, index (name),
  backendTableName varchar(255) not null
);

drop table if exists AsContentCompilerField;
create table AsContentCompilerField (
  id int not null auto_increment, primary key (id),
  asContentCompiler_id int not null, index (asContentCompiler_id),
  fieldName varchar(255) not null, index (fieldName)
);

-- AdvancedWordSearch linkage table;

drop table if exists AsWordHash;
create table AsWordHash (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id),
  regionStyle_id int not null,
  wordHash int not null, index (wordHash)
);

-- AdvancedDateSearch linkage table;

drop table if exists AsDate;
create table AsDate (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id),
  regionStyle_id int not null,
  fastdate bigint not null, index (fastdate)
);

-- AdvancedBooleanSearch linkage table;

drop table if exists AsBoolean;
create table AsBoolean (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id),
  regionStyle_id int not null,
  flag int not null, index (flag)
);

-- AdvancedIntegerSearch linkage table;

drop table if exists AsInteger;
create table AsInteger (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id), 
  regionStyle_id int not null,
  value int not null, index (value)
);

-- AdvancedFloatSearch linkage table;

drop table if exists AsBigDecimal;
create table AsBigDecimal (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id), 
  regionStyle_id int not null,
  value decimal(12,2) not null, index (value)
);

-- WordDensityIndex Tables

drop table if exists AsWord;
create table AsWord (
  id int not null auto_increment, primary key (id),
  word varchar(255) not null, index (word),
  hash int not null
);

drop table if exists AsRankedWord;
create table AsRankedWord (
  id int not null auto_increment, primary key (id),
  asContentCompilerField_id int not null,
  node_id int not null, index (node_id), 
  regionStyle_id int not null,
  asWord_id int not null,
  density int not null
);
