alter table IndexListing add isAscendingRanking int not null;
alter table IndexListing add minRanking int not null;
alter table IndexListing add maxRanking int not null;
update IndexListing set isAscendingRanking=1, minRanking=1, maxRanking=10;