drop table if exists ScalableSingleImage;
create table ScalableSingleImage (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  imgFormat_id int not null,
  imgOriginal_id int not null,
  alt varchar(255) not null);

drop table if exists ScalableSingleImageStyle;
create table ScalableSingleImageStyle (
  id int not null auto_increment, primary key (id),
  name varchar(32) not null, index (name),
  isOptional int not null,
  isDefined int not null,
  extends varchar(255) not null,
  json_text mediumtext not null
);

insert into ScalableSingleImageStyle values (1,'default',1,0,'','{ mustUpload: true }');

