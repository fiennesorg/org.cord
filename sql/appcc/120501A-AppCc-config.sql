alter table ImagePageListing add configSrc mediumtext not null;
update ImagePageListing set configSrc='{}';

alter table ImagePage add configSrc mediumtext not null;
update ImagePage set configSrc='{}';

alter table TimedPageListing add configSrc mediumtext not null;
update TimedPageListing set configSrc='{}';

alter table TimedPage add configSrc mediumtext not null;
update TimedPage set configSrc='{}';