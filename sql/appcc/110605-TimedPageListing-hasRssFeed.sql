alter table TimedPageListing add hasRssFeed int not null;
update TimedPageListing set hasRssFeed = 1;
