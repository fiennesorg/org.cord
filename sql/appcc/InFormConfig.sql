drop table if exists InFormConfig;
create table InFormConfig (
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    configSrc mediumtext not null
);
