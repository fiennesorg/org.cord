alter table TimedPageListing add rssSubscribeTitle varchar(255) not null after rssTitle;
update TimedPageListing set rssSubscribeTitle = rssTitle;
