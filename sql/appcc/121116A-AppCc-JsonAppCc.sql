alter table ImagePageListing add dataSrc mediumtext not null after isDefined;
update ImagePageListing set dataSrc=configSrc;
alter table ImagePageListing drop configSrc;

alter table ImagePage add dataSrc mediumtext not null after isDefined;
update ImagePage set dataSrc=configSrc;
alter table ImagePage drop configSrc;

alter table TimedPageListing add dataSrc mediumtext not null after isDefined;
update TimedPageListing set dataSrc=configSrc;
alter table TimedPageListing drop configSrc;

alter table TimedPage add dataSrc mediumtext not null after isDefined;
update TimedPage set dataSrc=configSrc;
alter table TimedPage drop configSrc;
