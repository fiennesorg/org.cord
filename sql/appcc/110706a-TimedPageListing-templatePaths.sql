alter table TimedPageListing add listingTemplatePath varchar(255);

update TimedPageListing set listingTemplatePath = 'appcc/TimedPageListing/listing/singleList.wm.html';

alter table TimedPageListing add itemTemplatePath varchar(255);

update TimedPageListing set itemTemplatePath = 'appcc/TimedPage/title.wm.html';