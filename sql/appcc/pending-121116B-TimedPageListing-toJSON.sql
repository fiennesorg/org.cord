drop function if exists jsonString;
create function jsonString(s varchar(255))
returns varchar(255) deterministic
return concat('"',replace(replace(s,'\\','\\\\'),'"','\\"'),'"');

drop function if exists jsonBoolean;
create function jsonBoolean(i int)
returns varchar(255) deterministic
return if(i!=0,'true','false');

select concat(
	'{ ',
	'"pageSize":',		pageSize,		',',
	'"itemName":',		jsonString(itemName),		',',
	'"itemsName":',		jsonString(itemsName),		',',
	'"listingStyle":',	jsonString(listingStyle),	',',
	'"hasRssFeed":',	jsonBoolean(hasRssFeed),
	' }') from TimedPageListing;
