drop table if exists IndexListing;
create table IndexListing (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  title varchar(255) not null,
  isPublished int not null,
  acceptsMultiple int not null,
  acceptsNewValues int not null
);

drop table if exists IndexPage;
create table IndexPage (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null
);

drop table if exists IndexValue;
create table IndexValue (
  id int not null auto_increment, primary key (id),
  indexListing_id int not null, index (indexListing_id),
  value varchar(255) not null
);

drop table if exists IndexPage_IndexValue;
create table IndexPage_IndexValue (
  id int not null auto_increment, primary key (id),
  indexPage_id int not null, index (indexPage_id),
  indexValue_id int not null, index (indexValue_id),
  isDefined int not null
);