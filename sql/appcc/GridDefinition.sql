drop table if exists GridDefinition;
create table GridDefinition (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  columnWidth int not null,
  gutterSize int not null,
  hasHPadSpacer int not null,
  lineHeight int not null,
  hashstamp bigint not null
);
