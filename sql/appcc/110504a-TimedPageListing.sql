drop table if exists TimedPageListing;
create table TimedPageListing (
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    title varchar(255) not null, index(title),
    isPublished int not null,
    pageSize int not null,
    homePageSize int not null,
    itemName varchar(255) not null,
    itemsName varchar(255) not null,
    listingStyle varchar(255) not null
);

drop table if exists TimedPage;
create table TimedPage (
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    title varchar(255) not null, index(title),
    isPublished int not null,
    timedPageListing_id int not null, index (timedPageListing_id),
    summary varchar(255) not null,
    startTime bigint not null,
    endTime bigint not null,
    timedPageType varchar(255) not null
);
