drop table if exists ImagePageListing;
create table ImagePageListing (
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    data mediumtext not null,
    title varchar(255) not null,
    isPublished int not null,
    listingRows int not null,
    hasRssFeed int not null,
    rssTitle varchar(255) not null,
    publicRssUrl varchar(255),
    itemName varchar(255) not null,
    itemsName varchar(255) not null
);

drop table if exists ImagePage;
create table ImagePage (
    id int not null auto_increment, primary key (id),
    node_id int not null, index (node_id),
    regionStyle_id int not null, index (regionStyle_id),
    style_id int not null,
    isDefined int not null,
    data mediumtext not null,
    title varchar(255) not null,
    isPublished int not null, index (isPublished),
    number int not null,
    imagePageListing_id int not null, index (imagePageListing_id),
    isZoomable int not null,
    isDownloadable int not null,
    showImage int not null
);