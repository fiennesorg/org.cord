drop table if exists IndexListing;
create table IndexListing (
  id int not null auto_increment, primary key (id),
  node_id int not null, 
    index (node_id),
  regionStyle_id int not null,
    index (regionStyle_id),
  style_id int not null, 
  isDefined int not null,
  title varchar(255) not null,
  isPublished int not null,
  acceptsMultiple int not null,
  acceptsNewValues int not null,
  indexListingTemplate varchar(255) not null,
  indexValueTemplate varchar(255) not null,
  indexPageTemplate varchar(255) not null,
  indexPageOrdering varchar(255) not null,
  minRanking int not null,
  maxRanking int not null
);

alter table IndexPage add isPublished int not null;
alter table IndexPage add indexPageTemplate varchar(255);

drop table if exists IndexValue;
create table IndexValue (
  id int not null auto_increment, primary key (id),
  node_id int not null, index (node_id),
  regionStyle_id int not null, index (regionStyle_id),
  style_id int not null,
  isDefined int not null,
  indexListing_id int not null, index (indexListing_id),
  title varchar(255) not null,
  isPublished int not null,
  sortTitle varchar(255) not null,
  indexValueTemplate varchar(255)
);

drop table if exists IndexPage_IndexValue;
create table IndexPage_IndexValue (
  id int not null auto_increment, primary key (id),
  indexPage_id int not null, index (indexPage_id),
  indexValue_id int not null, index (indexValue_id),
  isDefined int not null,
  description varchar(255) not null,
  ranking int not null
);