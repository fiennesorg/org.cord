drop table if exists Sitemap;
create table Sitemap (
	id int not null auto_increment, primary key (id),
	node_id int not null, index (node_id),
	regionStyle_id int not null, index (regionStyle_id),
	style_id int not null,
	isDefined int not null,
	mapRootNode_id int not null,
	generations int not null,
	pageTypes varchar(255) not null,
	listingTemplate varchar(255) not null
);
