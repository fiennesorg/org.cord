#!/bin/bash
set -e 

user="root"
pwd="seeekwell"
db="$1"
from=$2

patch()
{
	if (($1 > $from)); then
		echo
		echo "running $2" 
		cat $2 | mysql -u $user -p$pwd $db
	else
		echo "skipping $2"
	fi
}

patch  70101 ./patch-070101-AsBigDecimal.sql
patch  70101 ../../awtwf/sql/patch-070101-outputsWebmacro.sql
patch  70101 ./patch-070101-AsInteger.sql
patch  71019 ../../awtwf/sql/patch-071019-mediumtext.sql
patch  71021 ./patch-071021-userstyle-ismultisession.sql

patch  80815 ./patch-080815-node-generation.sql
patch  80826 ./cc/TimedItem.patch-080827-create.sql
patch  80905 ../../awtwf/sql/patch-080905-blockname.sql
patch  80913 ../../awtwf/sql/patch-080913-zoomedImage.sql
patch  80921 ../../awtwf/sql/patch-080921-internalImageLinks.sql
patch  81201 ./patch-081201-MovedNode.sql
patch  81220 ./patch-081220-wordDensityIndex.sql

patch  90207 ../../awtwf/sql/patch-090207-embedded_link_titles.sql
patch  90221 ../../awtwf/sql/patch-090221-GntmlPlusBlockStyle-rename-headers.sql
patch  90221 ../../awtwf/sql/patch-090221-GntmlPlusBlockStyle.sql
patch  90612 ./patch-090612-remove-HtmlMetaTag.sql
patch  90619 ./cc/TimedItem.patch-081207-create.sql
patch  90619 ./cc/TimedItem.patch-081208-add-parentnode-id.sql
patch  90619 ./cc/TimedItem.patch-090619-remove-listingSummary.sql

patch 100204 ./patch-100204-fix-animated-gifs.sql
patch 100316 ./cc/TimedItem.patch-100316-add-sticky-flag.sql
patch 101208 ./Img/101208a-ImgFormat.sql
patch 101208 ./Img/101208b-ImgOriginal.sql
patch 101208 ./Img/101208c-ImgScaled.sql
patch 101208 ../../awtwf/sql/101208a-GntmlScalableImage.sql
# patch 101230 ../../awtwf/sql/patch-101230-add-GntmlEmbeddedVideo.sql

patch 110504 ./appcc/110504a-TimedPageListing.sql
patch 110504 ./appcc/110504b-ImagePageListing.sql
patch 110510 ./appcc/110510-TimedPageListing-homePageOrdering.sql
patch 110527 ./appcc/110527a-IndexListing.sql 
patch 110530 ./patch-110530.1-Node_isMenuItem.sql
patch 110530 ./patch-110530.2-Node_isSearchable.sql 
patch 110530 ./appcc/110530-TimedPageListing-dropsummary.sql
patch 110531 ../../awtwf/sql/patch-110531-GntmlNavigation.sql 
patch 110605 ./appcc/110605-ImagePageListing-hasRssFeed.sql 
patch 110605 ./appcc/110605-TimedPageListing-hasRssFeed.sql 
patch 110608 ./appcc/110608-ImagePageListing-rssTitle.sql 
patch 110608 ./appcc/110608-TimedPageListing-rssTitle.sql
patch 110612 ./appcc/110612a-GridDefinition.sql 
patch 110616 ./appcc/110616a-TimedPageListing-publicRssUrl.sql 
patch 110616 ./appcc/110616b-ImagePageListing-publicRssUrl.sql 
patch 110616 ./appcc/110616c-TimedPageListing-homePageTitle.sql 
patch 110622 ./appcc/110622a-ImagePageListing-number.sql 
patch 110624 ../../awtwf/sql/110624-GntmlScalableImage-functionality.sql
patch 110627 ./appcc/110627a-IndexPage_IndexValue-descriptionranking.sql
patch 110629 ./appcc/110629a-ScalableSingleImage.sql 
patch 110630 ./110630a-RegionStyle-pageImage.sql 
patch 110706 ./appcc/110706a-TimedPageListing-templatePaths.sql 
patch 110708 ./appcc/110708a-ImagePageListing-itemName.sql 
patch 110708 ./appcc/110708b-IndexListing-minRanking.sql 
patch 110713 ./appcc/110713a-ImagePage-showImage.sql 
patch 110720 ../../awtwf/sql/110720-GntmlPageList.sql
patch 110725 ./appcc/110725a-IndexListing-reinit.sql 
patch 110725 ./appcc/110725b-Grid-pageImage.sql 
patch 110808 ./appcc/110808a-GridDefinition-hasHPadSpacer.sql 
patch 110808 ./appcc/110808b-TimedPageListing-notNulls.sql 
patch 111112 ./appcc/111112a-GridDefinition-hashstamp.sql 
patch 111220 ../../awtwf/sql/patch-111220-GntmlTimedPageListingBlock.sql
patch 111221 ../../awtwf/sql/111221-GntmlStyle-config.sql

patch 120109 ../../awtwf/sql/patch-120109-GntmlSiteMap.sql
patch 120113 ./appcc/120113A-TimedPageListing-removeHomeHighlights.sql 
patch 120113 ./InForm/120113A-InForm-dropSiteMap.sql
patch 120115 ../../awtwf/sql/patch-120115A-GntmlSiteMap-rootNodePolicy.sql
patch 120115 ../../awtwf/sql/patch-120115B-GntmlPageList-listTitle.sql
patch 120116 ./appcc/120116A-TimedPageListing-rssSubscribeTitle.sql 
patch 120216 ../../awtwf/sql/patch-120216A-configValue.sql
patch 120318 ../../awtwf/sql/patch-120318A-EmbedVideo.sql
patch 120319 ../../awtwf/sql/patch-120319A-ScalableImage.sql
patch 120319 ../../awtwf/sql/patch-120319B-DownloadBlockFactory.sql
patch 120322 ../../awtwf/sql/patch-120322A-GntmlIndexListingBlock.sql
patch 120327 ./patch-120327A-descriptions.sql 
patch 120419 ../../awtwf/sql/patch-120419A-GntmlMultiTimedPageListingBlock.sql
patch 120516 ../../awtwf/sql/patch-120516A-GntmlRssBlock.sql
patch 120501 ./appcc/120501A-AppCc-config.sql 
patch 120523 ./InForm/patch-120523-preview.sql
patch 121113 ./patch-121113A-Acl_Acl.sql 
patch 121114 ../../awtwf/sql/patch-121114A-CyclingImage.sql
patch 121116 ./appcc/121116A-AppCc-JsonAppCc.sql
patch 121123 ./appcc/121123A-TimedPageListing-JSON_templates.sql
patch 121204 ./appcc/121204A-IndexValue-sortTitle.sql

patch 130423 ./patch-130423A-AsBigDecimal.value.sql
patch 130424 ./InForm/patch-130424A-InFormConfig.sql

patch 130626 ./Img/patch-130626A-ImgOriginal-metadata.sql

patch 130716 ./patch-130716-Disqus.sql
patch 130717 ./patch-130717-AbstractStyle-optional_defined.sql

patch 130802 ./Img/patch-130802A-ImgOriginal-dataSrc.sql

patch 131031 ./patch/FormAt-SubDocs/patch-131031-FormAt.sql

patch 131116 ./patch-131116A-Node_jsonSrc.sql
patch 150109 ./patch-150109A-User_jsonSrc.sql

patch 160106 ./Img/patch-160106A-ImgOriginal.owner.sql

patch 160419 ../../awtwf/sql/patch-160419A-GntmlNextPrev.sql

patch 170507 ./Img/patch-170507A-ImgOriginal.croppedFromImgOriginal_id.sql

patch 170713 ./patch/WritableDataFilter/patch-170713A-PureJsonObjectField.sql

patch 170717 ./patch/WritableDataFilter/patch-170717A-ImgOriginal-data.sql

patch 171102 ./patch/FormAt-SubDocs/patch-171102-FormAt-json.sql
