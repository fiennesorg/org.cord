#text #begin
<script type="text/javascript">
	var updateForm = document.getElementById('updateForm');
	var submit = updateForm.submit;
	updateForm.submit = function(anchor) {
		if (anchor) {
			var i = updateForm.action.indexOf('#');
			if (i == -1) {
				updateForm.action = updateForm.action + "#" + anchor;
			} else {
				updateForm.action = updateForm.action.substring(0,i+1) + anchor;
			}
    	}
    	submit.call(updateForm);
	};
</script>
#end