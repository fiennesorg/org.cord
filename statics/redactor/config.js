$(function() {

  var redactor = $('#redactor');

  $.ajaxSetup( {
    error : function(jqXHR, exception) {
      if (jqXHR.status === 0) {
        alert('Not connect.\n Verify Network.');
      } else if (jqXHR.status == 404) {
        alert('Requested page not found. [404]');
      } else if (jqXHR.status == 500) {
        alert('Internal Server Error [500].');
      } else if (exception === 'parsererror') {
        alert('Requested JSON parse failed.');
      } else if (exception === 'timeout') {
        alert('Time out error.');
      } else if (exception === 'abort') {
        alert('Ajax request aborted.');
      } else {
        alert('Uncaught Error.\n' + jqXHR.responseText);
      }
    }
  });

  var documentId = redactor.attr('data-documentId');

  redactor
      .redactor( {
        buttons : [ 'html', '|', 'bold', 'italic', 'nodelink', '|', 'justify',
            'block_width', 'add_div', '|', 'move_up', 'move_down',
            'delete_block' ],
        buttonsCustom : {
          nodelink : {
            title : "Internal Link",
            callback : function(buttonName, buttonDOM, buttonObject) {
              console.log('Internal Link fired...');
              var contents = this;
              $.ajax( {
                url : "/",
                data : {
                  view : "Element_Create",
                  format : 'HTML_EDIT',
                  factoryName : "InlineTest",
                  documentId : documentId,
                  contents : this.getSelectionText()
                },
                async : false,
                dataType : "html",
                success : function(response) {
                  console.log(response);
                  var inline = $(response);
                  contents.insertNode(inline);
                  console.log(contents.getBlock());
                  contents.sync();
                }
              });
            }
          },
          add_div : {
            title : 'Add Div',
            callback : function(buttonName, buttonDOM, buttonObject) {
              console.log('Add Div');
              var prevBlock = resolveParentBlock(this.getBlock());
              $.ajax( {
                url : "/",
                data : {
                  view : "Element_Create",
                  format : 'HTML_EDIT',
                  factoryName : "Dummy",
                  documentId : documentId
                },
                async : false,
                dataType : "html",
                success : function(response) {
                  console.log(response);
                  var newBlock = $(response);
                  newBlock.insertAfter(prevBlock);
                }
              });
              this.sync();
            }
          },
          delete_block : {
            title : "Delete Block",
            callback : function(buttonName, buttonDOM, buttonObject) {
              $(resolveCurrentBlock()).css("color", "red");
              $("#Delete_Block_Confirm").dialog( {
                resizable : false,
                height : 140,
                modal : true,
                buttons : {
                  Delete : function() {
                    var currentBlock = $(resolveCurrentBlock());
                    currentBlock.slideUp(function() {
                      currentBlock.remove();
                    });
                    $(this).dialog("close");
                  },
                  Cancel : function() {
                    $(resolveCurrentBlock()).css("color", "inherit");
                    $(this).dialog("close");
                  }
                }
              });
            }
          },
          block_width : {
            title : "Floats",
            dropdown : {
              no_float : {
                title : 'No float',
                callback : block_width_callback
              },
              left_2 : {
                title : 'Left float, 2 wide',
                callback : block_width_callback
              },
              left_3 : {
                title : 'Left float, 3 wide',
                callback : block_width_callback
              }
            }
          },
          move_up : {
            title : 'Move Up',
            callback : function(buttonName, buttonDOM, buttonObject) {
              var block = resolveParentBlock(this.getBlock());
              var prev = $(block).prev();
              if (prev) {
                prev.before(block);
                this.setCaret(block, 0);
                this.sync();
              }
            }
          },
          move_down : {
            title : 'Move Down',
            callback : function(buttonName, buttonDOM, buttonObject) {
              var block = resolveParentBlock(this.getBlock());
              var next = $(block).next();
              if (next) {
                next.after(block);
                this.setCaret(block, 0);
                this.sync();
              }
            }
          }
        },
        convertDivs : false,
        // changeCallback : function(html) {
        // var block = resolveParentBlock(this.getBlock());
        // $('#blockclass').html('class:' + block.className);
        // $('#offset').html('offset:' + this.getCaretOffset(block));
        // },
        minHeight : 400,
        // allowedTags : [ 'p', 'div', 'span', 'b', 'strong', 'i', 'em', 'dl',
        // 'dt', 'dd' ],
        shortcuts : false,
        focus : true,
        tidyHtml : false
      });

  function updateBlockPreview(block) {
    console.log('updateBlockPreview(' + block + ')');
    block = $(block);
    var previewURL = block.attr("data-previewURL");
    console.log('previewURL: ' + previewURL);
    if (previewURL) {
      $.get(previewURL, function(data) {
        block.replaceWith(data);
      });
    }
  }

  function resolveParentBlock(element) {
    var parent = element.parentElement;
    if (parent == null) {
      return null;
    }
    if (parent.className.indexOf("redactor_editor") != -1) {
      return element;
    }
    return resolveParentBlock(parent);
  }
  ;

  function resolveCurrentBlock() {
    return resolveParentBlock(redactor.redactor('getBlock'));
  }

  function block_width_callback(buttonName, bottonDOM, buttonObj) {
    this.blockRemoveClass('left_2 left_3');
    this.blockSetClass(buttonName);
  }
  ;

  $('div.redactor_editor').click(function() {
    var block = resolveCurrentBlock();
    $('#blockclass').html('class:' + block.className);
    $('#offset').html('offset:' + redactor.redactor('getCaretOffset', block));
  });

  function openModalEdit(target) {
    var editable = $(target).closest('[data-editURL]');
    if (editable.length == 0) {
      return false;
    }
    var block = resolveCurrentBlock();
    var src = editable.attr("data-editURL");
    var title = editable.attr("data-title") || "Undefined title";
    var width = editable.attr("data-width") || 400;
    var height = editable.attr("data-height") || 400;
    iframe.attr( {
      width : +width,
      height : +height,
      src : src
    });
    editable.addClass("editing");
    dialog.dialog("option", "title", title).dialog("option", "close",
        function() {
          console.log('openModalEdit close');
          editable.removeClass("editing");
          if (editable.attr('data-previewURL')) {
            updateBlockPreview(editable);
          }
        }).dialog("open");
    return true;
  }

  $('div.redactor_editor').dblclick(function(e) {
    openModalEdit(e.target) && e.preventDefault();
  });

  $('div.redactor_editor').keydown(function(event) {
    console.log('keydown...');
    var immutable = $(redactor.redactor('getParent')).closest('.immutable');
    if (immutable.length == 1) {
      switch (event.which) {
      case 37: // cursor
      case 38: // cursor
      case 39: // cursor
      case 40: // cursor
        // all the cursor keys are allowed to work as normal on an immutable...
        return;
      }
      immutable = immutable[0];
      console.log(immutable.nodeName);
      var block = resolveCurrentBlock();
      if (immutable == block) {
        console.log('immutable == block');
      }
      console.log(event.which);
      switch (immutable.nodeName) {
      case 'INLINE':
        switch (event.which) {
        case 13:
          openModalEdit(immutable);
          break;
        case 32:
          var offset = redactor.redactor('getCaretOffset', immutable);
          if (offset == 0) {
            $(immutable).before("&nbsp;");
//            redactor.redactor('setCaret', block, redactor.redactor('getCaretOffset', block) - 2);
            var selection = window.getSelection();
            selection.removeAllRanges();
            var range = new Range();
            range.setStartBefore(immutable);
            range.setEndBefore(immutable);
            selection.addRange(range);
          } else if ($.trim($(immutable).text()).length == offset) {
            $(immutable).after("&nbsp;");
            redactor.redactor('setCaret', block, redactor.redactor('getCaretOffset', block) + 2);
          }
          break;
        }
        break;
      default:
        switch (event.which) {
        case 13: // enter
          if (redactor.redactor('getCaretOffset', immutable) == 0) {
            // insert a blank paragraph before an immutable by pressing enter at
            // the start of the immutable
            $(block).before("<p>&nbsp;</p");
            redactor.redactor('setCaret', $(block).prev(), 0);
          } else if ($.trim($(block).text()).length == redactor.redactor(
              'getCaretOffset', block)) {
            // insert a blank paragraph after an immutable by pressing enter
            // anywhere in the body of the immutable
            $(block).after("<p>&nbsp;</p>");
            redactor.redactor('setCaret', $(block).next(), 0);
          } else {
            // pressing return anywhere else on an immutable opens the edit
            // window
            openModalEdit(block);
          }
          break;
        }
      }
      event.preventDefault();
      return;
    }
    // keystrokes on non-immutables
    switch (event.which) {
    case 8: // backspace
      // don't let people delete an immutable block from the start of the next
      // block...
      var block = resolveCurrentBlock();
      if (redactor.redactor('getCaretOffset', block) == 0) {
        var prev = $(block).prev().get(0);
        if (prev && prev.className.indexOf("immutable") != -1) {
          event.preventDefault();
          return;
        }
      }
      break;
    case 46: // del
      // don't let people remove the formatting on the start of an immutable by
      // pressing del from the block before...
      var block = resolveCurrentBlock();
      if ($.trim($(block).text()).length == redactor.redactor('getCaretOffset',
          block)) {
        var next = $(block).next().get(0);
        if (next && next.className.indexOf("immutable") != -1) {
          event.preventDefault();
          return;
        }
      }
      break;
    }
  });

  var iframe = $('<iframe frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>');

  var dialog = $("<div></div>").append(iframe).appendTo("body").dialog( {
    autoOpen : false,
    modal : true,
    resizable : false,
    width : "auto",
    height : "auto",
    close : function() {
      iframe.attr("src", "");
    }
  });

});
