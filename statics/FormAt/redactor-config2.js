$(function() {
	$.Redactor.prototype.alignment = function()
	{
		return {
			langs: {
				en: {
    				"align": "Align",
					"align-left": "Align Left",
					"align-center": "Align Center",
					"align-right": "Align Right",
					"align-justify": "Align Justify",
				}
			},
			init: function()
			{
				var that = this;
				var dropdown = {};

				dropdown.left = { title: that.lang.get('align-left'), func: that.alignment.invoke };
				dropdown.center = { title: that.lang.get('align-center'), func: that.alignment.invoke };
				dropdown.right = { title: that.lang.get('align-right'), func: that.alignment.invoke };
				dropdown.justify = { title: that.lang.get('align-justify'), func: that.alignment.invoke };

				var button = this.button.add('alignment', this.lang.get('align'));
				this.button.addDropdown(button, dropdown);
			},
			invoke: function(buttonName) {
				this.buffer.set();
				this.block.removeClass('text-left');
    		    		this.block.removeClass('text-center');
    		    		this.block.removeClass('text-right');
    		    		this.block.removeClass('text-justify');
				this.block.addClass('text-' + buttonName);
				this.core.editor().focus();
			}
		};
	};
	
	$.Redactor.prototype.addImage = function() {
		return {
			init: function() {
				var button = this.button.add('addImage', 'Add Image');
				this.button.addCallback(button, this.addImage.invoke);
			},
			invoke: function(buttonName) {
				console.log(buttonName);
				var prevBlock = this.selection.block();
				$.ajax({
					data: {
						view: "Element_Create",
						format: "HTML_EDIT",
						factoryName: "Image",
						documentId: documentId
					},
					async: false,
					dataType: "html",
					success: function(response) {
						console.log(response);
						redactor.redactor('caret.end', prevBlock);
						redactor.redactor('insert.html', response);
						block_openEditModal($(response));
					}
				});
			}
		};
	};
	
	$.Redactor.prototype.nodeLink = function() {
		return {
			init: function() {
				var b = this.button.add('nodeLink', 'Node Link');
				this.button.addCallback(b, this.nodeLink.invoke);
			},
			invoke: function(buttonName) {
				console.log(buttonName);
				$.ajax({
					data: {
						view: "Element_Create",
						format: "HTML_EDIT",
						factory: "NodeLink",
						document: documentId,
						wrappedHtml: this.selection.html()
					},
					async: false,
					dataType: "html",
					success: function(response) {
						console.log(response);
						redactor.redactor('selection.replace', response);						
					}
				});
			}
		};
	};

	
	var redactor = $('#redactor');
	var documentId = redactor.attr('data-documentId');
	
	function toSubDoc(block)
	{
		return $(block).closest('.subdoc').get(0);
	}
	
	function toLayout(block)
	{
		return $(block).closest('*[data-factory="Layout"]').get(0);
	}

	$.Redactor.prototype.editBlockLayout = function() {
		return {
			init: function() {
				var button = this.button.add('editBlockLayout', 'Edit Block Layout');
				this.button.addCallback(button, this.editBlockLayout.invoke);
			}, 
			invoke: function(buttonName) {
				console.log(buttonName);
				var block = this.selection.block();
				var layout = toLayout(block);
				console.log('layout:' + layout);
				if (layout) {
					block_openEditModal($(layout));
				} else {
					$.ajax({
						data: {
							view: "Element_Create",
							format: "HTML_EDIT",
							factoryName: "Layout",
							documentId: documentId,
							wrappedHtml: $(block).get(0).outerHTML
						},
						async: false,
						dataType: "html",
						success: function(response) {
							console.log(response);
							$(block).replaceWith(response);
							redactor.redactor('code.sync');
							block_openEditModal($(response));
						}
					});
				}
			}
		};
	};
	
	$.Redactor.prototype.move = function() {
		return {
			init: function() {
				var dropdown = {};
				dropdown.moveUp = { title: 'Move Block Up', func: this.move.moveBlockUp }
				dropdown.moveDown = { title: 'Move Block Down', func: this.move.moveBlockDown }
				dropdown.moveContentsUp = { title: 'Move Contents Up', func: this.move.moveContentsUp }
				dropdown.moveContentsDown = { title: 'Move Contents Down', func: this.move.moveContentsDown }
				var button = this.button.add('moveBlock', 'Move');
				this.button.addDropdown(button, dropdown);
			},
			moveBlockUp: function(buttonName) {
				this.selection.save();
				var currentBlock = this.selection.block();
				if (currentBlock) {
					currentBlock = toSubDoc(currentBlock) || currentBlock;
					var previousBlock = $(currentBlock).prev();
					if (this.utils.isRedactorParent(previousBlock)) {
						$(previousBlock).before($(currentBlock));
					}
				}
				this.selection.restore();
				this.core.editor().focus();
			},
			moveBlockDown: function(buttonName) {
				this.selection.save();
				var currentBlock = this.selection.block();
				if (currentBlock) {
					currentBlock = toSubDoc(currentBlock) || currentBlock;
					var nextBlock = $(currentBlock).next();
					if (this.utils.isRedactorParent(nextBlock)) {
						$(nextBlock).after($(currentBlock));
					}
				}
				this.selection.restore();
				this.core.editor().focus();
			},
			moveContentsUp: function(buttonName) {
				this.selection.save();
				var currentBlock = this.selection.block();
				if (currentBlock) {
					var previousBlock = $(currentBlock).prev();
					if (previousBlock.length > 0) {
						// block isn't the first of something
						var previousSubDoc = toSubDoc(previousBlock);
						if (previousSubDoc && previousSubDoc != toSubDoc(currentBlock)) {
							// block was preceeded by a different subdoc so append the block into the subdoc...
							$(previousSubDoc).children().last().after($(currentBlock));
						} else {
							// block was preceeded by another block so just move it up...
							$(currentBlock).prev().before($(currentBlock));
						}
					} else {
						// block is the first of something
						var currentSubDoc = toSubDoc(currentBlock);
						if (currentSubDoc) {
							// block is the first in a subDoc
							$(currentSubDoc).before($(currentBlock));
							// *** need to check is currentSubDoc is now empty and delete or add empty <p>???
						} else {
							// first block in doc - do nothing.
						}
					}
				}
				this.selection.restore();
				this.core.editor().focus();
			},
			moveContentsDown: function(buttonName) {
				this.selection.save();
				var currentBlock = this.selection.block();
				if (currentBlock) {
					var nextBlock = $(currentBlock).next();
					if (nextBlock.length > 0) {
						// block isn't the last of something
						var nextSubDoc = toSubDoc(nextBlock);
						if (nextSubDoc && nextSubDoc != toSubDoc(currentBlock)) {
							// block was followed by a different subdoc so append the block into the subdoc...
							$(nextSubDoc).children().first().before($(currentBlock));
						} else {
							// block was followed by another block so just move it up...
							$(currentBlock).next().after($(currentBlock));
						}
					} else {
						// block is the last of something
						var currentSubDoc = toSubDoc(currentBlock);
						if (currentSubDoc) {
							// block is the first in a subDoc
							$(currentSubDoc).after($(currentBlock));
							// *** need to check is currentSubDoc is now empty and delete or add empty <p>???
						} else {
							// last block in doc - do nothing.
						}
					}
				}
				this.selection.restore();
				this.core.editor().focus();
			}
		};
	}
	
	

	function updateSubDocClass(jBlock) {
		var subDoc = jBlock.closest('.subdoc').get(0);
		var jSubDocClass = $('#subDocClass');
		if (subDoc) {
			jSubDocClass.val($(subDoc).attr('data-subDocClass'));
//			jSubDocClass.removeAttr('disabled');
		} else {
			jSubDocClass.val('');
//			jSubDocClass.attr('disabled', 'disabled');
		}
	}

	function getOrCreateSubDoc(jBlock) {
		var existingSubBlock = $(jBlock).closest('.subdoc').get(0);
		if (existingSubBlock) {
			return $(existingSubBlock);
		}
		return $(jBlock).wrap("<div data-factory='SubDocElement' class='subdoc'></div>").parent();
	}
	
	$('#subDocClass').change(function() {
		var newClass = $(this).val();
		var jSubDoc = getOrCreateSubDoc(redactor.redactor('core.object').selection.block());
		jSubDoc.removeClass(jSubDoc.attr('data-subDocClass'));
		jSubDoc.addClass(newClass);
		jSubDoc.attr('data-subDocClass', newClass);
	});
	
	redactor.redactor({
		plugins: ['alignment', 'addImage', 'nodeLink', 'move', 'editBlockLayout'],
		imageEditable: false,
		callbacks: {
			click: function(e) {
				var block = this.selection.block();
				var jBlock = $(block);
				updateSubDocClass(jBlock);
					var subdoc = $(e.target).closest('.subdoc');
					if (subdoc.length == 1) {
						console.log('edit subdoc...');
						$(redactor.redactor('core.editor')).children().addClass('immutable');
						subdoc.removeClass('immutable');
					} else {
						console.log('edit core...');
						$(redactor.redactor('core.editor')).children().not('.plugin').not('.subdoc').removeClass('immutable');
						$(redactor.redactor('core.editor')).children('.subdoc').addClass('immutable');
					}
			},
			keyup: function(e) {
				var block = this.selection.block();
				var jBlock = $(block);
				var keyCode = e.originalEvent.keyCode;
				// keys that are permitted regardless of context...
				switch(keyCode) {
					case 37: // left arrow
					case 38: // up arrow
					case 39: // right arrow
					case 40: // down arrow
						updateSubDocClass(jBlock);
				}
			},
			keydown: function(e) {
				var block = this.selection.block();
				var jBlock = $(block);
				var keyCode = e.originalEvent.keyCode;
				// keys that are permitted regardless of context...
				switch(keyCode) {
					case 37: // left arrow
					case 38: // up arrow
					case 39: // right arrow
					case 40: // down arrow
						return;
				}
				if (jBlock.closest('.immutable').length > 0) {
					// keypress on an immutable block...
					return false;
				} else {
					// keypress on a non-immutable block...
					switch (keyCode) {
						case 8: // backspace
							if (this.offset.get(jBlock) == 0) {
								var prev = jBlock.prev().get(0);
								if (!prev || $(prev).closest('.immutable').length > 0) {
									return false;
								}
							}
							break;
						case 46: // del
							if ($.trim(jBlock.text()).length == this.offset.get(jBlock)) {
								var next = jBlock.next().get(0);
								if (!next || $(next).closest('.immutable').length > 0) {
									return false;
								}
							}
					}
				}
			},
			init: function() {
				$(this.core.editor()).dblclick(function(e) {
					var editable = $(e.target).closest('[data-editURL]');
					if (editable.length == 1) {
						block_openEditModal(editable);
					} 
				});
			}
		}
	});
	
	function block_openEditModal(editable) {
		var editURL = editable.attr('data-editURL');
		if (editURL) {
			modal_iframe.attr({
				src: editable.attr('data-editURL'),
				width:400,
				height:400
			});
			modal_dialog
				.dialog("option", "close", function() {
					console.log("modal_dialog-close");
					$(editable).load(window.location.href, { 
							view: "Element_RenderPreview",
							elementKey: editable.attr('key'),
							factoryName: editable.attr('data-factory'),
							documentId: documentId
						});
				})
				.dialog("open");
		}
	}
	
	function updateBlockPreview(block) {
		console.log('updateBlockPreview(' + block + ')');
		block = $(block);
		var previewURL = block.attr("data-previewURL");
		console.log('previewURL: ' + previewURL);
		if (previewURL) {
			$.get(previewURL, function(data) {
				console.log(data);
				block.replaceWith(data);
			});
		}
	}
	
	var modal_iframe = $('<iframe frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>');

	var modal_dialog = $("<div></div>")
		.append(modal_iframe)
		.appendTo("body").dialog( {
			autoOpen : false,
			modal : true,
			resizable : false,
			width : "auto",
			height : "auto",
			close : function() {
				$.get(window.location.href, 
					{
						view: "Element_RenderPreview",
						factoryName: 
					},
					function(data) {
					});
			}
		});
});