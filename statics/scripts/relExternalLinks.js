function externalLinks() { 
  if (!document.getElementsByTagName) return; 
  var anchors = document.getElementsByTagName("a"); 
  for (var i=0; i<anchors.length; i++) { 
    var anchor = anchors[i]; 
    if (anchor.getAttribute("href") && 
        anchor.getAttribute("rel") == "external") { 
      anchor.target = "_blank";
      if (anchor.getAttribute("title")) {
        anchor.title= anchor.getAttribute("title") + " (Opens in a new window)";
      } else {
        anchor.title = "(Opens in a new window)";
      }
    } 
  } 
} 
if (window.onload != null) {
  var oldOnload = window.onload;
  window.onload = function(a) {
    oldOnload(a);
    externalLinks();
  }
} else {
  window.onload = externalLinks;
}
