function padToRhythm(selector, lineheight, minMargin) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    var margin = lineheight - error;
    if (margin < minMargin) {
      margin = margin + lineheight;
    }
    if (error != 0) {
      $(this).css('margin-bottom', margin + 'px');
    }
  });
}

function padEqualRhythm(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var margin = lineheight - error;
      var top = Math.round(margin / 2);
      var bottom = margin - top;
      $(this).css('margin-top', top + 'px');
      $(this).css('margin-bottom', bottom + 'px');
    }
  });
}

function vr_pad_bottom(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var padding = lineheight - error;
      $(this).css('padding-bottom', padding + 'px');
    }
  });  
}

function vr_pad_top(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var padding = lineheight - error;
      $(this).css('padding-top', padding + 'px');
    }
  });  
}

function vr_pad_both(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var padding = lineheight - error;
      var top = Math.round(padding / 2);
      var bottom = padding - top;
      $(this).css('padding-top', top + 'px');
      $(this).css('padding-bottom', bottom + 'px');
    }
  });
}

function vr_margin_bottom(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var margin = lineheight - error;
      $(this).css('margin-bottom', margin + 'px');
    }
  });  
}

function vr_margin_top(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var margin = lineheight - error;
      $(this).css('margin-top', margin + 'px');
    }
  });  
}

function vr_margin_both(selector, lineheight) {
  $(selector).each(function(index) {
    var height = $(this).outerHeight();
    var error = height % lineheight;
    if (error != 0) {
      var margin = lineheight - error;
      var top = Math.round(margin / 2);
      var bottom = margin - top;
      $(this).css('margin-top', top + 'px');
      $(this).css('margin-bottom', bottom + 'px');
    }
  });
}

function vr_field(selector, fieldheight) {
  $(selector).each(function(index) { 
    var top = $(this).offset().top;
    var error = top % fieldheight;
    if (error != 0) {
      var height = fieldheight - error;
      $(this).css('height', height + 'px');
    }      
  });
}

