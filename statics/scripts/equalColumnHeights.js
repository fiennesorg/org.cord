function getInnerChildDivs(parentElement) {
  var childDivs = new Array();
  var outerChildren = parentElement.childNodes;
  for (i = 0; i < outerChildren.length; i++) {
    if (outerChildren[i].tagName == "DIV") {
      innerChildren = outerChildren[i].childNodes;
      for (j = 0; j < innerChildren.length; j++) {
        if (innerChildren[j].tagName == "DIV") {
          childDivs.push(innerChildren[j]);
        }
      }
    }
  }
  return childDivs;
}

function resizeChildren(parentId) {
  var parent = document.getElementById(parentId);
  var children = getInnerChildDivs(parent);
  var maxHeight = 0;
  for (i = 0; i < children.length; i++) {
    children[i].style.height="1%";
    maxHeight = Math.max(maxHeight, children[i].offsetHeight);
  }
  for (i = 0; i < children.length; i++) {
    children[i].style.height = maxHeight + "px";
  }
  return maxHeight;
}
