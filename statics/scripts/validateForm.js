var fullMessage;
//-------------------------------------------------------------------------------------------------------//
function initFullMessage()
{
   fullMessage = '';
}
//-------------------------------------------------------------------------------------------------------//
function trim(s)
{
  while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
  {
    s = s.substring(1,s.length);
  }

  while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length) == '\n') || (s.substring(s.length-1,s.length) == '\r'))
  {
    s = s.substring(0,s.length-1);
  }
  return s;
}
//-------------------------------------------------------------------------------------------------------//
function validateEmail(email)
{
  var filter = /^[a-zA-Z0-9_\.\-\+]+\@([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9]+$/;
  return filter.test(email);
}
//-------------------------------------------------------------------------------------------------------//
function validateText(strTest)
{
   var x = trim(strTest);
   return (x.length > 0);

}
//-------------------------------------------------------------------------------------------------------//
function validateSelectList(oSelect,showAlert)
{
   var testList = oSelect.selectedIndex > -1;
   result = sendMessage(testList,displayName(oSelect),oSelect.type,showAlert);
   return result;
}
//-------------------------------------------------------------------------------------------------------//
function validateCheckBox(oCheckBox,showAlert)
{
    var testCheck = oCheckBox.checked;
    result = sendMessage(testCheck,displayName(oCheckBox),oCheckBox.type,showAlert);
    return result;
}
//-------------------------------------------------------------------------------------------------------//
function validateField(fieldName,fieldValue,fieldType,showAlert)
{
   var result = true;

   switch(fieldType)
   {
      case 'email':
        result = sendMessage(validateEmail(fieldValue),fieldName,fieldType,showAlert);
        break;
      case 'text':
        result = sendMessage(validateText(fieldValue),fieldName,fieldType,showAlert);
        break;
   }

   return result;
}
//-------------------------------------------------------------------------------------------------------//
function sendMessage(validateResult,fieldName,fieldType,showAlert,confirmFieldName)
{
    var message;

    if(validateResult == false)
    {
      switch(fieldType)
      {
         case 'email':
           message = 'Email address entered is not valid';
           break;
         case 'text':
           message = fieldName + ' is a required field';
           break;
         case 'checkbox':
           message = fieldName + ' must be checked';
           break;
         case 'select':
         case 'select-one':
         case 'select-multiple':
            message = 'You must choose an item from ' + fieldName;
            break;
         case 'confirm':
            message = 'The value of ' + fieldName + ' must match the value of ' + confirmFieldName;
      }

      if(showAlert == true)
      {
        alert(message);
      }
      else
      {
        fullMessage = fullMessage + '\n' + message;
      }

      return false;
    }
    else
    {
      return true;
    }
}
//-------------------------------------------------------------------------------------------------------//
function sendFullMessage()
{
  alert(fullMessage);
}
//-------------------------------------------------------------------------------------------------------//
function capsFirstLetter(strValue)
{
   var firstLetter;
   var newString;

   firstLetter = strValue.substring(0,1);
   firstLetter = firstLetter.toUpperCase();
   newString = strValue.substring(1,strValue.length);
   newString = firstLetter + newString;

   return newString;
}
//-------------------------------------------------------------------------------------------------------//
function isConfirmField(fieldName)
{
   var findConfirm = fieldName.toLowerCase().indexOf('confirm');
   if(findConfirm != -1)
   {
     return true;
   }
   else
   {
     return false;
   }
}
//-------------------------------------------------------------------------------------------------------//
function checkConfirmField(oForm,oElement,showAlert)
{
	var result = false;

	var fieldToConfirm =
		oElement.name.toLowerCase().substring("confirm".length);

	if(oForm[fieldToConfirm]) {
		oElementToConfirm = oForm[fieldToConfirm];
		result = oElement.value == oElementToConfirm.value;
		sendMessage(result,displayName(oElement),'confirm',showAlert,displayName(oElementToConfirm));
	}

	return result;
}
//-------------------------------------------------------------------------------------------------------//
function checkFieldType(fieldName)
{
    var findEmail = fieldName.toLowerCase().indexOf('email');

    if(findEmail != -1)
    {
      return 'email';
    }
    else
    {
      return 'text';
    }
}
//-------------------------------------------------------------------------------------------------------//
function getLabelsForElements()
{
    var labels = document.getElementsByTagName('LABEL');
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].htmlFor != '') {
             var elem = document.getElementById(labels[i].htmlFor);
             if (elem) {
                elem.label = labels[i];
                if (!elem.label.textContent && elem.label.innerText) {
                  elem.label.textContent = elem.label.innerText;
                }
             }
        }
    }
}

function displayName(oElement)
{
   var nameToDisplay;

   if(oElement.title != undefined && trim(oElement.title) != '')
   {
        nameToDisplay = oElement.title;
   }
   else if (oElement.label && oElement.label.textContent)
   {
        nameToDisplay = '"' + oElement.label.textContent + '"';
   }
   else
   {
        nameToDisplay = capsFirstLetter(oElement.name);
   }

   return nameToDisplay;

}
//-------------------------------------------------------------------------------------------------------//
function validateForm(oForm)
{
   var result = true;
   var finalResult = true;
   var formField;

   getLabelsForElements();

   if(oForm.validSubmit.value == 1)
   {
      if(trim(oForm.requiredParameterList.value) != '')
      {
        initFullMessage();
        var requiredFields=oForm.requiredParameterList.value.split(",");
        for(var i=0; i<requiredFields.length; i++)
        {
          formField = oForm[requiredFields[i]]
          if(formField)
          {
            if(isConfirmField(formField.name) == false)
            {
              if(formField.type == 'checkbox')
              {
                 result = validateCheckBox(formField);
              }
              else if(formField.type == 'select' ||
            		  formField.type == 'select_one' ||
            		  formField.type == 'select-multiple')
              {
                 result = validateSelectList(formField);
              }
              else
              {
                 result = validateField(displayName(formField),
                		 				formField.value,
                		 				checkFieldType(formField.name));
              }
            }
            else
            {
               result = checkConfirmField(oForm,formField);
            }
            finalResult = finalResult && result;
          }
        }

        if(finalResult == false)
        {
          sendFullMessage();
        }
        else
        {
          oForm.submit();
        }
      }
      else
      {
        oForm.submit();
      }
   }

}
//-------------------------------------------------------------------------------------------------------//
