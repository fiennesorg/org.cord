/*
 * getCookie(): given a cookie name, get its value. Returns null if the cookie can't be found.
 * From http://www.webreference.com/js/column8/functions.html
 *
 */
function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return unescape(dc.substring(begin + prefix.length, end));
}
 

/* cookie manipulation functions from: http://www.quirksmode.org/js/cookies.html */

function createCookie(name,value,days,domain) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else
    var expires = "";
  document.cookie = name + "=" + value
    + expires
    + ((domain) ? "; domain=" + domain : "")
    + "; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ')
      c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0)
      return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}

/*
 * utmvCookieCheck(): given a value, read the __utmv cookie and see if
 * that value is already set. Return true if so, false otherwise.
 * from http://www.startupcto.com/marketing-tech/google-analytics/setvar-and-the-zero-bounce-rate-bug
 */
function utmvCookieCheck(value) {
  var utmvCookie = readCookie("__utmv"); 
 
  if (utmvCookie == null)
    return false;
 
  // get rid of the Google's domain prefix ID, which appear on all
  // GA cookies
  utmvCookie = utmvCookie.replace(/^\d*\./, '');
 
  return (utmvCookie == value) ? true : false;
}
