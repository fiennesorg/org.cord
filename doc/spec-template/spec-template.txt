  <url name> - numbering follows directory depth

Quote: The quote block provides a short overview of 
what the page is for and what can be achieved.

The next set of sections provide a set of Use Cases 
that can be performed on this page. They are broken up 
by user types starting with the lowest (anonymous user) 
and ending with the highest (administrator). It is 
assumed that each one inherits the lesser 
functionalities and then adds their own.

  Public Functionality

* bulleted list of functions that a non-logged in user 
  can perform on the page

  Member Functionality

* bulleted list of functions that a member can perform 
  on the page.

  Admin Functionality

* bulleted list of functions that an admin can perform 
  on the page

  Content Regions

This is a list (using the Description paragraph class) 
of named regions that are registered on this page, 
along with any supporting information that is necessary 
to describe what they are for.

  <regionName> <ContentCompilerName>(<styleName>). 
  <supporting information>

  Webmacro Templates

This is a list (using the Description paragraph class) 
of the webmacro templates that are associated with the 
page. These should be indexed, thereby letting a coder 
look up the template that is puzzling them and find out 
which section it is assocated with.

  <templateName> <webmacro path>

If we use this format then this already goes an awful 
long way towards locking the site down, and is also 
readable enough for the client to be able to look at it 
without freaking out. If we ommit the Content Regions 
and the Webmacro Templates then it is definitely client 
friendly, and I would like to use this as an initial 
structure for func specs in the future, thereby 
ensuring that there is a single document that evolves 
from proposal, to spec, to implementation record.

There are other sections later on that define 
ContentCompilers and the fields that they use, but this 
is of a lesser priority at present.
