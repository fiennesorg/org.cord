6.2 - Paragraphs of text

Mr Harding had married early in life, and was the father of two daughters. The eldest, Susan, was born soon after his marriage; the other, Eleanor, not till ten years later.

At the time at which we introduce him to our readers he was living as precentor at Barchester with his youngest daughter, then twenty-four years of age

I must go down to the sea again, 
To the lonely sea and the sky;
I left my shoes and socks there - 
I wonder if they’re dry?
-- S. Milligan