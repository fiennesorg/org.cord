package org.cord.mirror.recordsource;

import java.util.AbstractList;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.common.base.Preconditions;

/**
 * An implementation of IdList that is backed dynamically by a JSONArray of ints.
 * 
 * @author alex
 */
public class JsonIdList
  extends AbstractList<Integer>
  implements IdList
{
  private final Table __table;
  private final JSONArray __jsonArray;

  public JsonIdList(TableWrapper tableWrapper,
                    JSONArray jsonArray)
  {
    __table = Preconditions.checkNotNull(tableWrapper, "tableWrapper").getTable();
    __jsonArray = Preconditions.checkNotNull(jsonArray, "jsonArray");
  }

  @Override
  public Integer get(int index)
  {
    try {
      return __jsonArray.getInt(index);
    } catch (JSONException e) {
      throw new RuntimeException(String.format("%s is backed by %s which contains %s at %s",
                                               this,
                                               __jsonArray,
                                               __jsonArray.opt(index),
                                               index));
    }
  }

  @Override
  public int size()
  {
    return __jsonArray.length();
  }

  @Override
  public Table getTable()
  {
    return __table;
  }
}
