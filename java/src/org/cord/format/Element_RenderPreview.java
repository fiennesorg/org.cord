package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.util.ExceptionUtil;
import org.cord.util.StringUtils;
import org.webmacro.Context;

public class Element_RenderPreview<A extends AppMgr>
  extends EditableElementView<A>
{
  public static final String NAME = "Element_RenderPreview";

  // public static final String DATA_PREVIEWURL = "data-previewURL";

  // /**
  // * Optional String that will contain the HTML that this Element should be wrapping in the
  // * generated preview. If not defined and the Element expects to wrap something then it is up to
  // * the Element to decide what it should be padded with, if anything.
  // */
  // public static final String IN_CONTENTS = "contents";

  public Element_RenderPreview(FormAtMgr<A> formAtMgr,
                               ViewAuthenticator<A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Element: Render preview view",
          viewAuthenticator);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           EditableElement element)
      throws NodeRequestException
  {
    StringBuilder buf = new StringBuilder();
    String webmacro = null;
    try {
      element.appendRedactorEditHtml(buf, context);
      webmacro = "evaluate:" + buf;
    } catch (Exception e) {
      webmacro = "evaluate:" + StringUtils.noWebmacro(ExceptionUtil.printStackTrace(e));
    }
    return new NodeRequestSegment(getNodeMgr(),
                                  nodeRequest,
                                  node,
                                  context,
                                  webmacro).setIsRootTemplatePath(true);
  }

  public static String getUrl(Document document,
                              EditableElementFactory elementFactory,
                              String blockId)
  {
    return getUrl(NAME, document, elementFactory, blockId);
  }

}
