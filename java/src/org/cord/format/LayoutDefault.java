package org.cord.format;

/**
 * LayoutDefault represents a SubDocElement that exists in the normal information flow of a
 * document. It will have left and right margins and may therefore be different widths but will
 * follow on below the previous LayoutDefault block. Documents will have a LayoutDefault that is to
 * be implemented by {@link RedactorElement}s that model top level blocks so they will all have
 * common margins.
 * 
 * @author alex
 */
public class LayoutDefault
  extends SubDocLayout
{

}
