package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.util.DebugConstants;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

public abstract class DocumentView<A extends AppMgr>
  extends FormAtView<A>
{
  public static final String IN_DOCUMENTID = "documentId";

  public DocumentView(FormAtMgr<? extends A> formAtMgr,
                      String name,
                      String title,
                      ViewAuthenticator<? super A> viewAuthenticator,
                      PostViewAction postViewAction)
  {
    super(formAtMgr,
          name,
          title,
          viewAuthenticator,
          postViewAction);
  }

  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    DebugConstants.method(this, "getInstance", nodeRequest, node, "<context>", acl);
    Object documentIdObj = nodeRequest.get(IN_DOCUMENTID);
    Document document =
        getFormAtMgr().getDocumentFactory().optDocument(NumberUtil.toInteger(documentIdObj, null),
                                                        getTransaction(nodeRequest, node));
    if (document == null) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       "Unable to resolve %s#%s",
                                       IN_DOCUMENTID,
                                       documentIdObj);
    }
    NodeRequestSegment result = getInstance(nodeRequest, node, context, acl, document);
    return result;
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    Document document)
      throws NodeRequestException;

  public static String getUrl(String viewName,
                              Document document)
  {
    return document.getUrl() + "?view=" + viewName + "&" + IN_DOCUMENTID + "=" + document.getId();
  }
}
