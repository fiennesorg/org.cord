package org.cord.format;

import java.io.IOException;
import java.io.StringReader;

import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.cord.util.StringBuilderIOException;
import org.cord.util.XmlUtils;
import org.json.ImmutableJSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONArray;
import org.json.WritableJSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.Hashing;

import nu.validator.htmlparser.dom.HtmlDocumentBuilder;

public class Document
{
  public static WritableJSONObject createDocumentJson()
  {
    try {
      WritableJSONObject jDocument = new WritableJSONObject();
      jDocument.put(JSON_ELEMENTS, new WritableJSONArray());
      jDocument.put(JSON_FACTORYSTORES, new WritableJSONObject());
      return jDocument;
    } catch (JSONException e) {
      throw new LogicException(e);
    }
  }

  private final FormAtMgr<?> __formAtMgr;
  private ImmutableList<Element> _elements;
  private final int __documentID;
  private final JSONObject __jObj;
  private final String __url;

  private final LayoutDefault __layoutDefault;

  public Document(FormAtMgr<?> formAtMgr,
                  int documentID,
                  JSONObject json,
                  String url,
                  LayoutDefault layoutDefault)
      throws JSONException
  {
    __formAtMgr = formAtMgr;
    __documentID = documentID;
    if (json != null) {
      __jObj = json;
      _elements = formAtMgr.jsonToElements(this, json.getJSONArray(JSON_ELEMENTS));
    } else {
      __jObj = new WritableJSONObject();
      __jObj.put(JSON_ELEMENTS, new WritableJSONArray());
      _elements = ImmutableList.of();
    }
    __url = Preconditions.checkNotNull(url, "url");
    __layoutDefault = Preconditions.checkNotNull(layoutDefault);
    if (ImmutableJSONObject.class.equals(json.getClass())) {
      new IllegalStateException("Document created around ImmutableJSONObject").printStackTrace();
    }
  }

  public LayoutDefault getLayoutDefault()
  {
    return __layoutDefault;
  }

  public String createElementKey()
  {
    return Hashing.md5().newHasher().putDouble(Math.random()).hash().toString();
  }

  // private Table<String, String, Element> __elementsCache = HashBasedTable.create();

  // public Element optCachedElement(String factoryName,
  // String key)
  // {
  // return __elementsCache.row(factoryName).get(key);
  // }

  // protected void cache(EditableElement element)
  // {
  // String elementKey = element.getKey();
  // __elementsCache.put(element.getFactory().getName(), elementKey, element);
  // }

  public Optional<JSONObject> getElementJson(String key)
  {
    return getElementJson(key, __jObj);
  }

  public static final String JSON_KEY = "key";
  public static final String JSON_ELEMENTS = "elements";

  private Optional<JSONObject> getElementJson(String key,
                                              JSONObject jObj)
  {
    if (key.equals(jObj.optString(JSON_KEY))) {
      return Optional.of(jObj);
    }
    JSONArray elements = jObj.optJSONArray(JSON_ELEMENTS);
    if (elements != null) {
      for (Object o : elements) {
        if (o instanceof JSONObject) {
          Optional<JSONObject> childMatch = getElementJson(key, (JSONObject) o);
          if (childMatch.isPresent()) {
            return childMatch;
          }
        }
      }
    }
    return Optional.absent();
  }

  public ImmutableList<Element> getElements()
  {
    return _elements;
  }

  public final String getUrl()
  {
    return __url;
  }

  public void updateFromRedactor(String redactorHtml)
      throws SAXException
  {
    org.w3c.dom.Document document;
    try {
      document = new HtmlDocumentBuilder().parse(new InputSource(new StringReader(redactorHtml)));
    } catch (IOException e) {
      throw new LogicException(e);
    }
    org.w3c.dom.Element html = document.getDocumentElement();
    org.w3c.dom.Element body = XmlUtils.getUniqueChild(html, "body", true);
    _elements = parseXmlDocument(body.getChildNodes(), true);
  }

  public ImmutableList<Element> parseXmlDocument(NodeList nodeList,
                                                 boolean dropWhitespaceElements)
  {
    ImmutableList.Builder<Element> builder = ImmutableList.builder();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node primaryNode = nodeList.item(i);
      if (primaryNode.getNodeType() == Node.ELEMENT_NODE) {
        Element element = getFormAtMgr().nodeToElement(this, primaryNode);
        if (!(element.isWhitespace() & dropWhitespaceElements)) {
          builder.add(element);
        }
      }
    }
    return builder.build();
  }

  public final FormAtMgr<?> getFormAtMgr()
  {
    return __formAtMgr;
  }

  public final int getId()
  {
    return __documentID;
  }

  public static final String JSON_FACTORYSTORES = "factoryStores";

  public synchronized WritableJSONObject getFactoryStore(String factoryName)
  {
    try {
      JSONObject factoryStores = __jObj.optJSONObject(JSON_FACTORYSTORES);
      if (factoryStores == null) {
        factoryStores = new WritableJSONObject();
        __jObj.put(JSON_FACTORYSTORES, factoryStores);
      }
      JSONObject factoryStore = factoryStores.optJSONObject(factoryName);
      if (factoryStore == null) {
        factoryStore = new WritableJSONObject();
        factoryStores.put(factoryName, factoryStore);
      }
      return ObjectUtil.castTo(factoryStore, WritableJSONObject.class);
    } catch (JSONException e) {
      throw new LogicException("Unexpected error initialising FactoryStore for " + factoryName, e);
    }
  }

  public void saveMetaData(EditableElement element)
  {
    try {
      getFactoryStore(element.getFactory().getName()).put(element.getKey(), element.getMetaData());
    } catch (JSONException e) {
      throw new LogicException(e);
    }
  }

  public JSONObject toJSON()
      throws JSONException
  {
    __jObj.put(JSON_ELEMENTS, getFormAtMgr().elementsToJSONArray(_elements));
    return __jObj;
  }

  /**
   * Generate the ReadOutput of the entire document by appending each of the
   * {@link Element#toReadOutput(Context)} for the contained Blocks into a single ReadOutput.
   */
  public ReadOutput toReadOutput(Context context)
  {
    ReadOutput out = ReadOutput.EMPTY;
    for (Element block : _elements) {
      out = out.append(block.toReadOutput(context)).appendLineBreak();
    }
    return out;
  }

  /**
   * Generate the editable HTML view of this Document by invoking
   * {@link Element#appendRedactorEditHtml(Appendable, Context)} on each of the Blocks in the
   * Document.
   */
  public void appendEditHtml(Appendable buf,
                             Context context)
      throws PropertyException, ResourceException, IOException, JSONException
  {
    for (Element block : _elements) {
      block.appendRedactorEditHtml(buf, context);
      buf.append('\n');
    }
  }

  /**
   * Invoke {@link #appendEditHtml(Appendable, Context)} onto an internal StringBuilder and return
   * the result.
   */
  public String toEditHtml(Context context)
      throws PropertyException, ResourceException, JSONException
  {
    StringBuilder buf = new StringBuilder();
    try {
      appendEditHtml(buf, context);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("id", getId()).toString();
  }
}
