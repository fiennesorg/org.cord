package org.cord.format;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;

import com.google.common.base.Optional;

public class SubDocElementFactory
  extends ElementFactory
{
  public SubDocElementFactory(FormAtMgr<?> formAtMgr)
  {
    super(formAtMgr,
          SubDocElement.NAME);
  }

  @Override
  public Optional<SubDocElement> nodeToElement(Document document,
                                               Node domNode)
  {
    if (domNode.getNodeType() == Node.ELEMENT_NODE) {
      org.w3c.dom.Element domElement = (org.w3c.dom.Element) domNode;
      if (SubDocElement.NAME.equals(domElement.getAttribute(SubDocElement.DATA_FACTORY))) {
        return Optional.of(new SubDocElement(document,
                                             this,
                                             getFormAtMgr().domToElements(document, domElement),
                                             domElement.getAttribute(SubDocElement.DATA_SUBDOCCLASS)));
      }
    }
    return Optional.absent();
  }

  @Override
  public Optional<SubDocElement> parseJSON(Document document,
                                           Object o)
      throws JSONException
  {
    if (o instanceof JSONObject) {
      JSONObject jObj = (JSONObject) o;
      if (SubDocElement.NAME.equals(jObj.optString(SubDocElement.JSON_FACTORY))) {
        return Optional.of(new SubDocElement(document,
                                             this,
                                             getFormAtMgr().jsonToElements(document, jObj),
                                             jObj.optString(SubDocElement.JSON_SUBDOCCLASS)));
      }
    }
    return Optional.absent();
  }
}
