package org.cord.format;

import org.cord.node.PostViewAction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.AppView;
import org.cord.node.app.ViewAuthenticator;

public abstract class FormAtView<A extends AppMgr>
  extends AppView<A>
{
  private final FormAtMgr<? extends A> __formAtMgr;

  public FormAtView(FormAtMgr<? extends A> formAtMgr,
                    String name,
                    String title,
                    ViewAuthenticator<? super A> viewAuthenticator,
                    PostViewAction postViewAction)
  {
    super(formAtMgr.getAppMgr(),
          name,
          title,
          viewAuthenticator,
          postViewAction);
    __formAtMgr = formAtMgr;
  }

  public final FormAtMgr<? extends A> getFormAtMgr()
  {
    return __formAtMgr;
  }
}
