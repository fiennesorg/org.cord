package org.cord.format;

public enum OutputFormat {
  HTML,
  WEBMACRO,
  TEMPLATEPATH
}
