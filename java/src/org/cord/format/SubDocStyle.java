package org.cord.format;

/**
 * SubDocStyle is the common parent class of ElementStyle implementations that can only be utilised
 * by {@link SubDocElement}s. Raw {@link RedactorElement} blocks will only be able to implement
 * {@link RedactorBlockPresentation}.
 * 
 * @author alex
 */
public abstract class SubDocStyle
  extends ElementStyle
{

}
