package org.cord.format;

import org.cord.util.ObjectUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;

import com.google.common.base.Optional;

/**
 * A BlockFactory that has all of the meta-data handled by the Redactor editor and produces Blocks
 * that don't have an associated ID or any meta-data other than what is supplied directly from the
 * editor.
 * 
 * @author alex
 */
public class RedactorElementFactory
  extends ElementFactory
{
  private final boolean __isBlockElement;

  public RedactorElementFactory(FormAtMgr<?> formAtMgr,
                                String name,
                                boolean isBlockElement)
  {
    super(formAtMgr,
          name);
    __isBlockElement = isBlockElement;
  }

  protected boolean isBlockElement()
  {
    return __isBlockElement;
  }

  @Override
  public Optional<RedactorElement> nodeToElement(Document document,
                                                 org.w3c.dom.Node domNode)
  {
    if (domNode.getNodeType() == Node.ELEMENT_NODE) {
      org.w3c.dom.Element domElement = (org.w3c.dom.Element) domNode;
      if (domElement.getTagName().equalsIgnoreCase(getName())) {
        return Optional.of(new RedactorElement(document,
                                               this,
                                               Optional.fromNullable(domElement.getAttribute(RedactorElement.TAG_CLASS)),
                                               Optional.fromNullable(domElement.getAttribute(RedactorElement.TAG_CLASS)),
                                               getFormAtMgr().domToElements(document, domElement)));
      }
    }
    return Optional.absent();
  }

  @Override
  public Optional<RedactorElement> parseJSON(Document document,
                                             Object o)
      throws JSONException
  {
    JSONObject json = ObjectUtil.castTo(o, JSONObject.class);
    return Optional.of(new RedactorElement(document,
                                           this,
                                           Optional.fromNullable(json.optString(RedactorElement.JSON_EDITCLASS)),
                                           Optional.fromNullable(json.optString(RedactorElement.JSON_READCLASS)),
                                           getFormAtMgr().jsonToElements(document, json)));
  }

}
