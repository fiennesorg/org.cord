package org.cord.format;

import org.w3c.dom.Node;

import com.google.common.base.Optional;

/**
 * {@link ElementFactory} that returns {@link CharacterData} {@link Element}s if the XML
 * {@link Node} is a {@link Node#TEXT_NODE} representing the characters. This will be utilised for
 * everything that isn't some kind of markup or functional implementation.
 * 
 * @author alex
 */
public class CharacterDataFactory
  extends ElementFactory
{
  public static final String NAME = "CDATA";

  public CharacterDataFactory(FormAtMgr<?> formAtMgr)
  {
    super(formAtMgr,
          NAME);
  }

  public CharacterDataElement toElement(Document document,
                                        String contents)
  {
    return new CharacterDataElement(document, this, contents);
  }

  @Override
  public Optional<CharacterDataElement> nodeToElement(Document document,
                                                      Node node)
  {
    if (node.getNodeType() == Node.TEXT_NODE) {
      return Optional.of(new CharacterDataElement(document, this, node.getTextContent()));
    }
    return Optional.absent();
  }

  @Override
  public Optional<CharacterDataElement> parseJSON(Document document,
                                                  Object o)
  {
    if (o instanceof String) {
      return Optional.of(new CharacterDataElement(document, this, o.toString()));
    }
    return Optional.absent();
  }

}
