package org.cord.format;

import java.io.IOException;

import org.json.JSONException;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

/**
 * ElementRenderer is an enum that contains different implementations for rendering an
 * EditableElement for different purposes.
 * 
 * @author alex
 */
public enum ElementRenderer {
  JSON_DATA {
    @Override
    public String render(EditableElement element,
                         Context context)
        throws JSONException
    {
      return element.getDataAttributes().toString();
    }
  },
  HTML_EDIT {
    @Override
    public String render(EditableElement element,
                         Context context)
        throws PropertyException, ResourceException, IOException, JSONException
    {
      StringBuilder buf = new StringBuilder();
      element.appendRedactorEditHtml(buf, context);
      return buf.toString();
    }
  },
  HTML_READ {
    @Override
    public String render(EditableElement element,
                         Context context)
    {
      return element.toReadOutput(context).getOutput();
    }
  };

  public abstract String render(EditableElement element,
                                Context context)
      throws JSONException, PropertyException, ResourceException, IOException;

}
