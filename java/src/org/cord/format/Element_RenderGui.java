package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.webmacro.Context;

/**
 * Render the edit view of an Element so that the output of the file can be processed by
 * {@link Element_Update}
 */
public class Element_RenderGui<A extends AppMgr>
  extends EditableElementView<A>
{
  public static final String NAME = "Element_RenderGui";

  public static final String DATA_WIDTH = "data-width";
  public static final String DATA_HEIGHT = "data-height";
  public static final String DATA_EDITURL = "data-editURL";

  public Element_RenderGui(FormAtMgr<A> formAtMgr,
                           ViewAuthenticator<A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Block: Render GUI view",
          viewAuthenticator);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           EditableElement element)
      throws NodeRequestException
  {
    try {
      return new NodeRequestSegment(getNodeMgr(),
                                    nodeRequest,
                                    node,
                                    context,
                                    element.getModalEditTemplatePath(context)).setIsRootTemplatePath(true);
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       e,
                                       "Unexpected error rendering the GUI template for %s",
                                       element);
    }
  }

  public static String getUrl(Document document,
                              EditableElementFactory factory,
                              String key)
  {
    return getUrl(NAME, document, factory, key);
  }
}
