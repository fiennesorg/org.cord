package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.util.DebugConstants;
import org.cord.util.ObjectUtil;
import org.webmacro.Context;

public abstract class EditableElementFactoryView<A extends AppMgr>
  extends DocumentView<A>
{
  public static final String IN_FACTORYNAME = "factoryName";
  public static final String OUT_FACTORY = "factory";

  public EditableElementFactoryView(FormAtMgr<? extends A> formAtMgr,
                                    String name,
                                    String title,
                                    ViewAuthenticator<? super A> viewAuthenticator,
                                    PostViewAction postViewAction)
  {
    super(formAtMgr,
          name,
          title,
          viewAuthenticator,
          postViewAction);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           Document document)
      throws NodeRequestException
  {
    String factoryName = nodeRequest.getString(IN_FACTORYNAME);
    EditableElementFactory factory =
        ObjectUtil.castTo(getFormAtMgr().compElementFactoryByName(factoryName),
                          EditableElementFactory.class);
    DebugConstants.variable("factory", factory);
    context.put(OUT_FACTORY, factory);
    return getInstance(nodeRequest, node, context, acl, document, factory);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    Document document,
                                                    EditableElementFactory factory)
      throws NodeRequestException;

  public static String getUrl(String viewName,
                              Document document,
                              EditableElementFactory factory)
  {
    return getUrl(viewName, document) + "&" + IN_FACTORYNAME + "=" + factory.getName();
  }

}
