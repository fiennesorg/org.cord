package org.cord.format;

import org.cord.util.Gettable;
import org.json.JSONException;

import com.google.common.base.Preconditions;

public abstract class EditableElementFactory
  extends ElementFactory
{
  private final RedactorTagType __redactorTagType;

  public EditableElementFactory(FormAtMgr<?> formAtMgr,
                                String name,
                                RedactorTagType redactorTagType)
  {
    super(formAtMgr,
          name);
    __redactorTagType = Preconditions.checkNotNull(redactorTagType, "editHtmlTagType");
  }

  public RedactorTagType getRedactorTagType()
  {
    return __redactorTagType;
  }

  public abstract EditableElement getExistingElement(Document document,
                                                     String key);

  public abstract EditableElement createNewElement(Document document,
                                                   Gettable initParams)
      throws JSONException;
}
