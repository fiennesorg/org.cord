package org.cord.format;

import java.util.List;
import java.util.NoSuchElementException;

import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.w3c.dom.Node;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

public abstract class EditableContainerElementFactory
  extends EditableElementFactory
{
  public EditableContainerElementFactory(FormAtMgr<?> formAtMgr,
                                         String name,
                                         RedactorTagType redactorTagType)
  {
    super(formAtMgr,
          name,
          redactorTagType);
  }

  public JSONObject getMetadata(Document document,
                                String elementKey)
  {
    JSONObject metadata = document.getFactoryStore(getName()).optJSONObject(elementKey);
    if (metadata == null) {
      throw new NoSuchElementException("Unable to resolve JSON metadata for " + elementKey + " on "
                                       + this + " in " + document);
    }
    return metadata;
  }

  /**
   * Resolve an instance of the Element that this factory produces by looking up the metadata via
   * the given id.
   * 
   * @return The appropriate Element. Never null.
   * @throws NoSuchElementException
   *           if key cannot be resolved on document
   */
  @Override
  public final EditableContainerElement getExistingElement(Document document,
                                                           String elementKey)
  {
    JSONObject metadata = getMetadata(document, elementKey);
    List<Element> childElements;
    Optional<JSONObject> elementJson = document.getElementJson(elementKey);
    if (elementJson.isPresent()) {
      try {
        childElements = getFormAtMgr().jsonToElements(document, elementJson.get());
      } catch (Exception e) {
        throw new LogicException("Error trying to parse JSON children of " + elementJson + " in "
                                 + document,
                                 e);
      }
    } else {
      childElements = ImmutableList.of();
    }
    return createElement(document,
                         elementKey,
                         metadata,
                         childElements,
                         GettableUtils.emptyGettable());
  }

  @Override
  public EditableElement createNewElement(Document document,
                                          Gettable initParams)
      throws JSONException
  {
    return createNewElement(document, initParams, ImmutableList.<Element> of());
  }

  /**
   * @param document
   *          The Document that the new Element will be utilised in. I think that this is
   *          compulsory, or if it isn't then if your ElementFactory requires a Document then it can
   *          fail with an error.
   * @param initParams
   *          Optional set of additional bootup params for the new Element. If it is not defined, or
   *          if the expected params are not contained then the Element should still be created with
   *          a default configuration.
   * @return The new initialised item, or null if this factory doesn't support creation of items in
   *         this way.
   */
  public final EditableContainerElement createNewElement(Document document,
                                                         Gettable initParams,
                                                         List<Element> wrappedElements)
      throws JSONException
  {
    return createElement(document,
                         document.createElementKey(),
                         new WritableJSONObject(),
                         wrappedElements,
                         initParams);
  }

  /**
   * @param metadata
   *          The optional JSON representation of the data for this Element
   * @param initParams
   *          The optional Gettable containing values that should be utilised for the Element
   */
  protected abstract EditableContainerElement createElement(Document document,
                                                            String key,
                                                            JSONObject metadata,
                                                            List<Element> childElements,
                                                            Gettable initParams);

  @Override
  public final Optional<? extends EditableContainerElement> nodeToElement(Document document,
                                                                          Node redactorNode)
  {
    if (redactorNode.getNodeType() == Node.ELEMENT_NODE) {
      org.w3c.dom.Element redactorElement = (org.w3c.dom.Element) redactorNode;
      if (getName().equals(redactorElement.getAttribute(EditableContainerElement.DATA_FACTORY))) {
        String elementKey = redactorElement.getAttribute(EditableContainerElement.DATA_KEY);
        JSONObject metadata = getMetadata(document, elementKey);
        List<Element> childElements = getFormAtMgr().domToElements(document, redactorElement);
        return Optional.of(createElement(document,
                                         elementKey,
                                         metadata,
                                         childElements,
                                         GettableUtils.emptyGettable()));
      }
    }
    return Optional.absent();
  }

  @Override
  public final Optional<? extends EditableContainerElement> parseJSON(Document document,
                                                                      Object o)
      throws JSONException
  {
    if (o instanceof JSONObject) {
      JSONObject jObj = (JSONObject) o;
      if (getName().equals(jObj.getString(EditableContainerElement.JSON_FACTORY))) {
        return Optional.of(getExistingElement(document,
                                              jObj.getString(EditableContainerElement.JSON_KEY)));
      }
    }
    return Optional.absent();
  }

}
