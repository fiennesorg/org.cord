package org.cord.format;

import java.io.IOException;

import org.json.JSONException;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

public class Elements
{
  public static ReadOutput toReadOutput(ReadOutput readOutput,
                                        Iterable<Element> elements,
                                        Context context)
  {
    for (Element element : elements) {
      readOutput = readOutput.append(element.toReadOutput(context));
    }
    return readOutput;
  }

  public static ReadOutput toReadOutput(Iterable<Element> elements,
                                        Context context)
  {
    return toReadOutput(ReadOutput.EMPTY, elements, context);
  }

  public static void appendRedactorHtml(Appendable buf,
                                        Context context,
                                        Iterable<Element> elements)
      throws PropertyException, ResourceException, IOException, JSONException
  {
    for (Element element : elements) {
      element.appendRedactorEditHtml(buf, context);
    }
  }
}
