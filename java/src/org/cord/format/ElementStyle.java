package org.cord.format;

/**
 * ElementStyle is the abstract base class of objects that can describe the way that an Element
 * should be presented. Subclasses will divide up the type of properties that an ElementStyle can
 * impose and how they can be combined. The base ElementStyle is responsible for imposing a common
 * interface on translating this into HTML read and edit outputs.
 * 
 * @author alex
 */
public abstract class ElementStyle
{

}
