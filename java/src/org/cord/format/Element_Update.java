package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadException;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.webmacro.Context;

/**
 * Process the output from {@link Element_RenderGui} and update the state of an {@link Element}
 */
public class Element_Update<A extends AppMgr>
  extends EditableElementView<A>
{
  public static final String NAME = "Element_Update";

  public Element_Update(FormAtMgr<A> formAtMgr,
                        ViewAuthenticator<? super A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Block: Update state",
          viewAuthenticator);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           EditableElement element)
      throws NodeRequestException
  {
    try {
      element.update(nodeRequest);
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(), node, e, "Unexpected update error");
    }
    element.getDocument().saveMetaData(element);
    throw new ReloadException(node, Element_RenderGui.NAME, null);
  }

}
