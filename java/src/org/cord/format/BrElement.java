package org.cord.format;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.Optional;

public class BrElement
  extends JsonObjectElement
{
  public static final String NAME = "BR";

  public static void register(FormAtMgr<?> formAtMgr)
  {
    formAtMgr.register(new ElementFactory(formAtMgr, NAME) {
      @Override
      public Optional<? extends Element> nodeToElement(Document document,
                                                       Node redactorNode)
      {
        if (redactorNode.getNodeType() == Node.ELEMENT_NODE) {
          org.w3c.dom.Element redactorElement = (org.w3c.dom.Element) redactorNode;
          if (getName().equalsIgnoreCase(redactorElement.getTagName())) {
            return Optional.of(new BrElement(document, this));
          }
        }
        return Optional.absent();
      }

      @Override
      public Optional<? extends Element> parseJSON(Document document,
                                                   Object o)
          throws JSONException
      {
        if (o instanceof JSONObject) {
          JSONObject jObj = (JSONObject) o;
          if (getName().equals(jObj.getString(JSON_FACTORY))) {
            return Optional.of(new BrElement(document, this));
          }
        }
        return Optional.absent();
      }
    });
  }

  private BrElement(Document document,
                    ElementFactory elementFactory)
  {
    super(document,
          elementFactory);
  }

  @Override
  public void appendRedactorEditHtml(Appendable buf,
                                     Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    buf.append("<br/>");
  }

  @Override
  public ReadOutput toReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, "<br/>");
  }

  /**
   * @return true
   */
  @Override
  public boolean isWhitespace()
  {
    return true;
  }
}
