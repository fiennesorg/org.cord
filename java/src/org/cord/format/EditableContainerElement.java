package org.cord.format;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.base.Optional;

public abstract class EditableContainerElement
  extends EditableElement
{
  private final List<Element> __childElements;

  public EditableContainerElement(Document document,
                                  EditableElementFactory elementFactory,
                                  String elementKey,
                                  JSONObject metadata,
                                  List<Element> childElements)
  {
    super(document,
          elementFactory,
          elementKey,
          metadata);
    __childElements = childElements;
  }

  public List<Element> getContents()
  {
    return __childElements;
  }

  @Override
  public final ReadOutput toWrappedReadOutput(Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    if (__childElements.size() > 0) {
      return Elements.toReadOutput(__childElements, context);
    }
    Optional<?> wrappedHtml = (Optional<?>) context.get(Element_Create.WM_WRAPPEDHTML);
    if (wrappedHtml != null && wrappedHtml.isPresent()) {
      return new ReadOutput(OutputFormat.HTML, wrappedHtml.get().toString());
    }
    return ReadOutput.EMPTY;
  }

  @Override
  public final void appendWrappedEditHtml(Appendable buf,
                                          Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    if (__childElements.size() > 0) {
      Elements.appendRedactorHtml(buf, context, __childElements);
    } else {
      buf.append(Element_Create.getWrappedHtml(context).or(""));
    }
  }

  public static final String JSON_ELEMENTS = Document.JSON_ELEMENTS;

  @Override
  protected void toString(ToStringHelper tsh)
  {
    super.toString(tsh);
    tsh.add("childElements", __childElements);
  }

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject json = super.toJSON();
    json.put(JSON_ELEMENTS, getFormAtMgr().elementsToJSONArray(__childElements));
    return json;
  }

  /**
   * @return true if all of {@link #getContents()} are whitespace. Override this if this
   *         EditableContainerElement is still visible even if it doesn't contain any visible child
   *         element.
   */
  @Override
  public boolean isWhitespace()
  {
    for (Element element : getContents()) {
      if (!element.isWhitespace()) {
        return false;
      }
    }
    return true;
  }

}
