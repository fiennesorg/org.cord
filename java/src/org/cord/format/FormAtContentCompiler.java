package org.cord.format;

import java.util.Map;

import org.cord.mirror.AlwaysCachingSimpleRecordOperation;
import org.cord.mirror.Db;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.app.AppCC;
import org.cord.node.app.AppMgr;
import org.cord.node.app.TemplatePaths;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NotImplementedException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;

import com.google.common.base.Supplier;

public class FormAtContentCompiler<A extends AppMgr>
  extends AppCC<A>
{
  public static final String NAME = "FormAt";

  public static final ObjectFieldKey<JSONObject> JSON = PureJsonObjectField.createKey("json");
  public static final ObjectFieldKey<String> OUTPUTFORMAT = StringField.createKey("outputFormat");
  public static final ObjectFieldKey<String> OUTPUT = StringField.createKey("output");

  public static final RecordOperationKey<Document> DOCUMENT =
      RecordOperationKey.create("document", Document.class);

  private final DocumentFactory<A> __documentFactory;
  private final FormAtMgr<A> __formAtMgr;

  public FormAtContentCompiler(A appMgr)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("FormAt/FormAt.view.wm.html", "FormAt/FormAt.edit.wm.html"));
    __formAtMgr = new FormAtMgr<A>(appMgr, new Supplier<DocumentFactory<A>>() {
      @Override
      public DocumentFactory<A> get()
      {
        return getDocumentFactory();
      }
    });
    __formAtMgr.registerDefaultElementFactories();
    __documentFactory = new DocumentFactoryBridge(__formAtMgr);
  }

  public DocumentFactory<A> getDocumentFactory()
  {
    return __documentFactory;
  }

  class DocumentFactoryBridge
    extends DocumentFactory<A>
  {
    public DocumentFactoryBridge(FormAtMgr<A> formAtMgr)
    {
      super(formAtMgr);
    }

    @Override
    public Document createDocument(String url)
    {
      // TODO implement DocumentFactoryBridge.createDocument(url)
      throw new NotImplementedException();
    }

    @Override
    public Document optDocument(Integer id,
                                Viewpoint viewpoint)
    {
      PersistentRecord instance = getInstanceTable().getOptRecord(id, viewpoint);
      if (instance == null) {
        return null;
      }
      return instance.comp(DOCUMENT);
    }

    @Override
    public void saveDocument(Document document)
    {
      // TODO implement DocumentFactoryBridget.saveDocument
      throw new NotImplementedException();
    }
  }

  public final FormAtMgr<A> getFormAtMgr()
  {
    return __formAtMgr;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException
  {
    instances.addField(new PureJsonObjectField(JSON,
                                               "JSON",
                                               new FieldValueSupplier<WritableJSONObject>() {
                                                 @Override
                                                 public WritableJSONObject supplyValue(TransientRecord record)
                                                 {
                                                   return Document.createDocumentJson();
                                                 }
                                               }));
    instances.addRecordOperation(new AlwaysCachingSimpleRecordOperation<Document>(DOCUMENT) {
      @Override
      public Document calculate(TransientRecord instance,
                                Viewpoint viewpoint)
      {
        try {
          return new Document(getFormAtMgr(),
                              instance.getId(),
                              instance.comp(JSON),
                              instance.comp(NODE).comp(Node.URL),
                              new LayoutDefault());
        } catch (JSONException e) {
          throw new LogicException(String.format("Cannot parse %s on %s into a Document",
                                                 instance.comp(JSON),
                                                 instance),
                                   e);
        }
      }
    });
    instances.addField(new StringField(OUTPUTFORMAT, "Output Format"));
    instances.addField(new StringField(OUTPUT, "Output", StringField.LENGTH_TEXT, ""));
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      FeedbackErrorException
  {
    boolean isUpdated = super.updateTransientInstance(node,
                                                      regionStyle,
                                                      instance,
                                                      style,
                                                      transaction,
                                                      inputs,
                                                      outputs,
                                                      user);
    String editHtml = inputs.getString("editHtml");
    if (editHtml != null) {
      // editHtml = editHtml.replace("&nbsp;", " ");
      // editHtml = editHtml.replace("&", "&amp;");
      // editHtml = editHtml.replace("<br>", "<br/>");
      DebugConstants.variable("editHtml", editHtml);
      Document document = instance.comp(DOCUMENT);
      try {
        document.updateFromRedactor(editHtml);
        instance.setField(JSON, document.toJSON().toString());
        ReadOutput output = document.toReadOutput(getAppMgr().newContext());
        instance.setField(OUTPUTFORMAT, output.getOutputFormat().toString());
        instance.setField(OUTPUT, output.getOutput());
        System.err.println(instance.comp(JSON).toString(2));
      } catch (Exception e) {
        throw new FeedbackErrorException("FormAt ContentCompiler Update",
                                         node,
                                         e,
                                         "Cannot parse %s into XML",
                                         editHtml);
      }
    }
    return isUpdated;
  }

}
