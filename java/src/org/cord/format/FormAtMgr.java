package org.cord.format;

import java.util.Map;
import java.util.NoSuchElementException;

import org.cord.node.NodeManager;
import org.cord.node.NodeRequestSegmentManager;
import org.cord.node.app.AppComponent;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONArray;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.webmacro.WebMacro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

public class FormAtMgr<A extends AppMgr>
  implements AppComponent<A>
{
  private Map<String, ElementFactory> __elementFactories = Maps.newConcurrentMap();

  private final Supplier<DocumentFactory<A>> __documentFactorySupplier;
  private DocumentFactory<A> __documentFactory;

  private final A __appMgr;
  private final WebMacro __webMacro;

  private final CharacterDataFactory __characterDataFactory;

  private FormAtMgr(A appMgr,
                    WebMacro webMacro,
                    Supplier<DocumentFactory<A>> documentFactorySupplier)
  {
    __appMgr = appMgr;
    __webMacro = webMacro;
    __documentFactorySupplier =
        Preconditions.checkNotNull(documentFactorySupplier, "documentFactorySupplier");
    __characterDataFactory = new CharacterDataFactory(this);
    register(__characterDataFactory);
  }

  public CharacterDataFactory getCharacterDataFactory()
  {
    return __characterDataFactory;
  }

  /**
   * Create a new manager deriving the WebMacro from the passed AppMgr.
   */
  public FormAtMgr(A appMgr,
                   Supplier<DocumentFactory<A>> documentFactorySupplier)
  {
    this(appMgr,
         appMgr.getNodeMgr().getWebMacro(),
         documentFactorySupplier);
    NodeRequestSegmentManager nrsm = appMgr.getNodeMgr().getNodeRequestSegmentManager();
    // ViewAuthenticator<A> editAcl = new NamedAclAuthenticator<A>(appMgr, Node.EDITACL);
    ViewAuthenticator<A> editAcl = null;
    nrsm.register(new Element_Create<A>(this, editAcl));
    nrsm.register(new Element_RenderGui<A>(this, editAcl));
    nrsm.register(new Element_RenderPreview<A>(this, editAcl));
    nrsm.register(new Element_RenderRead<A>(this, editAcl));
    nrsm.register(new Element_Update<A>(this, editAcl));
  }

  public static final String RF_P = "p";
  public static final String RF_H2 = "h2";
  public static final String RF_H3 = "h3";
  public static final String RF_H4 = "h4";
  public static final String RH_STRONG = "strong";
  public static final String RH_B = "b";
  public static final String RH_EM = "em";
  public static final String RH_I = "i";

  public static final ImmutableList<String> RF_BLOCKS = ImmutableList.of(RF_P, RF_H2, RF_H3, RF_H4);

  public static final ImmutableList<String> RF_INLINES =
      ImmutableList.of(RH_STRONG, RH_B, RH_EM, RH_I);

  public void registerDefaultElementFactories()
  {
    for (String rf : RF_BLOCKS) {
      register(new RedactorElementFactory(this, rf, true));
    }
    for (String rf : RF_INLINES) {
      register(new RedactorElementFactory(this, rf, false));
    }
    register(new SubDocElementFactory(this));
    LayoutElement.register(this);
    BrElement.register(this);
    NodeLinkElement.register(this);
    ImageElement.register(this);
  }

  /**
   * Create a new testing manager that has a WebMacro for template resolution but which doesn't bind
   * into a node system.
   */
  public FormAtMgr(WebMacro webMacro,
                   Supplier<DocumentFactory<A>> documentFactorySupplier)
  {
    this(null,
         webMacro,
         documentFactorySupplier);
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

  public final WebMacro getWebMacro()
  {
    return __webMacro;
  }

  public final DocumentFactory<A> getDocumentFactory()
  {
    if (__documentFactory == null) {
      __documentFactory = __documentFactorySupplier.get();
    }
    return __documentFactory;
  }

  public void register(ElementFactory elementFactory)
  {
    __elementFactories.put(elementFactory.getName(), elementFactory);
  }

  public Element nodeToElement(Document document,
                               Node node)
  {
    for (ElementFactory elementFactory : __elementFactories.values()) {
      Optional<? extends Element> element = elementFactory.nodeToElement(document, node);
      if (element.isPresent()) {
        return element.get();
      }
    }
    return getCharacterDataFactory().toElement(document, "Unable to parse " + node);
  }

  public ImmutableList<Element> nodeListToElements(Document document,
                                                   NodeList nodeList)
  {
    ImmutableList.Builder<Element> builder = ImmutableList.builder();
    for (int i = 0; i < nodeList.getLength(); i++) {
      builder.add(nodeToElement(document, nodeList.item(i)));
    }
    return builder.build();
  }

  /**
   * Get the compulsory ElementFactory with the specified name.
   * 
   * @throws NoSuchElementException
   *           if there is no ElementFactory with the given name.
   */
  public ElementFactory compElementFactoryByName(String name)
      throws NoSuchElementException
  {
    try {
      return __elementFactories.get(name);
    } catch (Exception e) {
      throw new NoSuchElementException(String.format("Unable to resolve \"%s\" from %s",
                                                     name,
                                                     __elementFactories.keySet()));
    }
  }

  /**
   * Resolve an ElementFactory that can parse the given JSON representation.
   * 
   * @param o
   *          The JSON representation. If this is a String then it will be passed to
   *          {@link CharacterDataFactory}, otherwise it is expected to be a {@link JSONObject} with
   *          an {@link JsonObjectElement#JSON_FACTORY} field to resolve the factory.
   * @throws RuntimeException
   *           if it is not possible to resolve a factory to handle the JSON representation.
   * @return The appropriate ElementFactory. Never null.
   */
  public ElementFactory compElementFactoryForJSON(Object o)
      throws RuntimeException
  {
    if (o instanceof String) {
      return compElementFactoryByName(CharacterDataFactory.NAME);
    }
    if (o instanceof JSONObject) {
      return compElementFactoryByName(((JSONObject) o).optString(JsonObjectElement.JSON_FACTORY));
    }
    throw new IllegalArgumentException(String.format("Cannot resolve ElementFactory to parse %s of class %s",
                                                     o,
                                                     o.getClass()));
  }

  public Element jsonToElement(Document document,
                               Object o)
      throws JSONException
  {
    return compElementFactoryForJSON(o).parseJSON(document, o).get();
  }

  public ImmutableList<Element> domToElements(Document document,
                                              Node parentNode)
  {
    if (parentNode == null || parentNode.getNodeType() != Node.ELEMENT_NODE) {
      return ImmutableList.of();
    }
    NodeList childNodes = parentNode.getChildNodes();
    final int length = childNodes.getLength();
    if (length == 0) {
      return ImmutableList.of();
    }
    ImmutableList.Builder<Element> builder = ImmutableList.builder();
    for (int i = 0; i < length; i++) {
      builder.add(nodeToElement(document, childNodes.item(i)));
    }
    return builder.build();
  }

  public final WritableJSONArray elementsToJSONArray(Iterable<Element> elements)
      throws JSONException
  {
    WritableJSONArray jArr = new WritableJSONArray();
    for (Element element : elements) {
      Object o = element.toJSON();
      if (o != null) {
        jArr.put(o);
      }
    }
    return jArr;
  }

  public final ImmutableList<Element> jsonToElements(Document document,
                                                     JSONArray jArr)
      throws JSONException
  {
    ImmutableList.Builder<Element> builder = ImmutableList.builder();
    for (int i = 0; i < jArr.length(); i++) {
      builder.add(jsonToElement(document, jArr.opt(i)));
    }
    return builder.build();
  }

  public final ImmutableList<Element> jsonToElements(Document document,
                                                     JSONObject jObj)
      throws JSONException
  {
    JSONArray jArr = jObj.optJSONArray(Document.JSON_ELEMENTS);
    return jArr == null ? ImmutableList.<Element> of() : jsonToElements(document, jArr);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("appMgr", __appMgr).toString();
  }
}
