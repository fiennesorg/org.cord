package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.webmacro.Context;

public class Element_RenderRead<A extends AppMgr>
  extends EditableElementView<A>
{
  public static final String NAME = "Element_RenderRead";

  public Element_RenderRead(FormAtMgr<A> formAtMgr,
                            ViewAuthenticator<A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Block: Render read view",
          viewAuthenticator);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           EditableElement element)
      throws NodeRequestException
  {
    ReadOutput readOutput = element.toReadOutput(context);
    String webmacro = null;
    switch (readOutput.getOutputFormat()) {
      case HTML:
      case WEBMACRO:
        webmacro = "evaluate:" + readOutput.getOutput();
        break;
      case TEMPLATEPATH:
        webmacro = readOutput.getOutput();
        break;
    }
    return new NodeRequestSegment(getNodeMgr(),
                                  nodeRequest,
                                  node,
                                  context,
                                  webmacro).setIsRootTemplatePath(true);
  }

}
