package org.cord.format;

import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.node.img.ImgOriginal;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.webmacro.TagBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.Optional;

public class ImageElement
  extends EditableStandaloneElement
{
  public static final String NAME = "Image";

  public static void register(FormAtMgr<?> formAtMgr)
  {
    formAtMgr.register(new EditableStandaloneElementFactory(formAtMgr,
                                                            NAME,
                                                            RedactorTagType.IMMUTABLE_DIV) {

      @Override
      protected EditableStandaloneElement createElement(Document document,
                                                        String elementKey,
                                                        JSONObject metadata,
                                                        Gettable initParams)
      {
        return new ImageElement(document, this, elementKey, metadata);
      }
    });
  }

  public static final String JSON_IMGORIGINAL_ID = "imgOriginal_id";
  public static final String IN_IMGORIGINAL_ID = JSON_IMGORIGINAL_ID;

  public ImageElement(Document document,
                      EditableElementFactory factory,
                      String key,
                      JSONObject metadata)
  {
    super(document,
          factory,
          key,
          metadata);
    try {
      metadata.put("foo", "bar (init)");
    } catch (JSONException e) {
      throw new LogicException(e);
    }
  }

  @Override
  public void update(Gettable in)
      throws Exception
  {
    getMetaData().put(JSON_IMGORIGINAL_ID, NumberUtil.toInteger(in.get(IN_IMGORIGINAL_ID)));
  }

  @Override
  public ReadOutput toWrappedReadOutput(Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    StringBuilder buf = new StringBuilder();
    appendWrappedEditHtml(buf, context);
    return new ReadOutput(OutputFormat.HTML, buf.toString());
  }

  @Override
  public void appendWrappedEditHtml(Appendable buf,
                                    Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    Optional<PersistentRecord> imgOriginal = getImgOriginal();
    if (imgOriginal.isPresent()) {
      TagBuilder img = imgOriginal.get().comp(ImgOriginal.IMGTAG);
      img.build(buf, false);
    } else {
      buf.append("No image selected");
    }
    System.err.println(buf);
  }

  public Optional<PersistentRecord> getImgOriginal()
  {
    return Optional.fromNullable(getFormAtMgr().getNodeMgr()
                                               .getImgMgr()
                                               .getImgOriginal()
                                               .getTable()
                                               .getOptRecord(getMetaData().optInteger(JSON_IMGORIGINAL_ID)));
  }

  @Override
  public String getModalEditTemplatePath(Context context)
      throws PropertyException, ResourceException
  {
    return "FormAt/Image/Image.edit.wm.html";
  }

}
