package org.cord.format;

import org.cord.webmacro.TagBuilder;

public enum RedactorTagType {
  IMMUTABLE_DIV("div", "immutable plugin"),
  CONTAINER_DIV("div", "plugin"),
  LAYOUT_DIV("div", ""),
  IMMUTABLE_INLINE("span", "immutable plugin"),
  CONTAINER_INLINE("span", "plugin");

  private final String __redactorHtmlTag;
  private final String __coreClassNames;

  RedactorTagType(String redactorHtmlTag,
                  String coreClassNames)
  {
    __redactorHtmlTag = redactorHtmlTag;
    __coreClassNames = coreClassNames;
  }

  public final String getHtmlTag()
  {
    return __redactorHtmlTag;
  }

  public final TagBuilder getTagBuilder(String classNames)
  {
    TagBuilder tagBuilder = new TagBuilder(getHtmlTag());
    if (__coreClassNames.length() > 0 | classNames.length() > 0) {
      tagBuilder.addAttribute("class", __coreClassNames + " " + classNames);
    }
    return tagBuilder;
  }
}
