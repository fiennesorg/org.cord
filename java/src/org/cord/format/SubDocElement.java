package org.cord.format;

import java.util.List;

import org.json.JSONException;
import org.json.WritableJSONObject;
import org.webmacro.Context;

import com.google.common.base.MoreObjects.ToStringHelper;

public class SubDocElement
  extends ContainerElement
{
  public static final String NAME = "SubDocElement";

  public static final String JSON_SUBDOCCLASS = "subdocclass";
  public static final String DATA_SUBDOCCLASS = "data-" + JSON_SUBDOCCLASS;

  private final String __subDocClass;

  public SubDocElement(Document document,
                       ElementFactory blockFactory,
                       List<Element> contents,
                       String subDocClass)
  {
    super(document,
          blockFactory,
          contents);
    __subDocClass = subDocClass;
  }

  @Override
  protected void toString(ToStringHelper tsh)
  {
    super.toString(tsh);
    tsh.add("subDocClass", __subDocClass);
  }

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject container = super.toJSON();
    container.put(JSON_SUBDOCCLASS, __subDocClass);
    return container;
  }

  @Override
  protected String getHeaderEditHtml(Context context)
  {
    return "<div " + Element.DATA_FACTORY + "=\"" + NAME + "\" class=\"subdoc " + __subDocClass
           + " immutable\" " + DATA_SUBDOCCLASS + "=\"" + __subDocClass + "\">";
  }

  @Override
  protected String getFooterEditHtml(Context context)
  {
    return "</div>";
  }

  @Override
  protected ReadOutput getHeaderReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, getHeaderEditHtml(context));
  }

  @Override
  protected ReadOutput getFooterReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, getFooterEditHtml(context));
  }
}
