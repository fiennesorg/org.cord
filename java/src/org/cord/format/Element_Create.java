package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.util.DebugConstants;
import org.webmacro.Context;

import com.google.common.base.Enums;
import com.google.common.base.Optional;

public class Element_Create<A extends AppMgr>
  extends EditableElementFactoryView<A>
{
  public static final String NAME = "Element_Create";

  public static final String IN_FORMAT = "format";
  public static final String IN_WRAPPEDHTML = "wrappedHtml";

  public static final String WM_WRAPPEDHTML = IN_WRAPPEDHTML;

  public static final String OUT_BLOCK = "block";
  public static final String OUT_JSON = "json";

  public Element_Create(FormAtMgr<A> formAtMgr,
                        ViewAuthenticator<A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Block: Create",
          viewAuthenticator,
          InternalSuccessOperation.getInstance());
  }

  @SuppressWarnings("unchecked")
  public static Optional<String> getWrappedHtml(Context context)
  {
    Object o = context.get(WM_WRAPPEDHTML);
    return o == null ? Optional.<String> absent() : (Optional<String>) o;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           Document document,
                                           EditableElementFactory factory)
      throws NodeRequestException
  {
    ElementRenderer format =
        Enums.getIfPresent(ElementRenderer.class, nodeRequest.getString(IN_FORMAT))
             .or(ElementRenderer.JSON_DATA);
    try {
      Optional<String> wrappedHtml = Optional.fromNullable(nodeRequest.getString(IN_WRAPPEDHTML));
      context.put(WM_WRAPPEDHTML, wrappedHtml);
      DebugConstants.variable("wrappedHtml", wrappedHtml);
      EditableElement element = factory.createNewElement(document, nodeRequest);
      document.saveMetaData(element);
      String output = format.render(element, context);
      DebugConstants.variable("output", output);
      context.put("output", output);
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       e,
                                       "Error creating new Element with %s for %s",
                                       factory,
                                       document);
    }
    return new NodeRequestSegment(getNodeMgr(),
                                  nodeRequest,
                                  node,
                                  context,
                                  "evaluate:$output").setIsRootTemplatePath(true);
  }

}
