package org.cord.format;

import org.json.JSONException;
import org.json.WritableJSONObject;

public abstract class JsonObjectElement
  extends Element
{
  public static final String JSON_FACTORY = "factory";

  public JsonObjectElement(Document document,
                           ElementFactory elementFactory)
  {
    super(document,
          elementFactory);
  }

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject jObj = new WritableJSONObject();
    jObj.put(JSON_FACTORY, getFactory().getName());
    return jObj;
  }

}
