package org.cord.format;

import java.util.NoSuchElementException;

import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.w3c.dom.Node;

import com.google.common.base.Optional;

public abstract class EditableStandaloneElementFactory
  extends EditableElementFactory
{
  public EditableStandaloneElementFactory(FormAtMgr<?> formAtMgr,
                                          String name,
                                          RedactorTagType redactorTagType)
  {
    super(formAtMgr,
          name,
          redactorTagType);
  }
  /**
   * Resolve an instance of the Element that this factory produces by looking up the metadata via
   * the given id.
   * 
   * @return The appropriate Element. Never null.
   * @throws NoSuchElementException
   *           if key cannot be resolved on document
   */
  @Override
  public final EditableStandaloneElement getExistingElement(Document document,
                                                            String elementKey)
  {
    JSONObject metadata = document.getFactoryStore(getName()).optJSONObject(elementKey);
    if (metadata == null) {
      throw new NoSuchElementException("Unable to resolve " + elementKey + " on " + this + " in "
                                       + document);
    }
    return createElement(document, elementKey, metadata, GettableUtils.emptyGettable());
  }

  /**
   * @param document
   *          The Document that the new Element will be utilised in. I think that this is
   *          compulsory, or if it isn't then if your ElementFactory requires a Document then it can
   *          fail with an error.
   * @param initParams
   *          Optional set of additional bootup params for the new Element. If it is not defined, or
   *          if the expected params are not contained then the Element should still be created with
   *          a default configuration.
   * @return The new initialised item, or null if this factory doesn't support creation of items in
   *         this way.
   */
  @Override
  public final EditableStandaloneElement createNewElement(Document document,
                                                          Gettable initParams)
      throws JSONException
  {
    return createElement(document,
                         document.createElementKey(),
                         new WritableJSONObject(),
                         initParams);
  }

  /**
   * @param metadata
   *          The optional JSON representation of the data for this Element
   * @param initParams
   *          The optional Gettable containing values that should be utilised for the Element
   */
  protected abstract EditableStandaloneElement createElement(Document document,
                                                             String key,
                                                             JSONObject metadata,
                                                             Gettable initParams);

  @Override
  public final Optional<? extends EditableStandaloneElement> nodeToElement(Document document,
                                                                           Node redactorNode)
  {
    if (redactorNode.getNodeType() == Node.ELEMENT_NODE) {
      org.w3c.dom.Element redactorElement = (org.w3c.dom.Element) redactorNode;
      if (getName().equals(redactorElement.getAttribute(EditableStandaloneElement.DATA_FACTORY))) {
        return Optional.of(getExistingElement(document,
                                              redactorElement.getAttribute(EditableStandaloneElement.DATA_KEY)));
      }
    }
    return Optional.absent();
  }

  @Override
  public final Optional<? extends EditableStandaloneElement> parseJSON(Document document,
                                                                       Object o)
      throws JSONException
  {
    if (o instanceof JSONObject) {
      JSONObject jObj = (JSONObject) o;
      if (getName().equals(jObj.getString(EditableStandaloneElement.JSON_FACTORY))) {
        return Optional.of(getExistingElement(document,
                                              jObj.getString(EditableStandaloneElement.JSON_KEY)));
      }
    }
    return Optional.absent();
  }

}
