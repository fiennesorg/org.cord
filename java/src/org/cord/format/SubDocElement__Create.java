package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.webmacro.Context;

/**
 * Create the metadata for a new SubDocElement that wraps an existing Element and return the wrapped
 * HTML around it.
 * 
 * @author alex
 * @param <A>
 */
public class SubDocElement__Create<A extends AppMgr>
  extends DocumentView<A>
{
  public static final String NAME = "SubDocElement__Create";

  public static final String IN_WRAPPEDHTML = "wrappedHtml";

  public SubDocElement__Create(FormAtMgr<? extends A> formAtMgr,
                               ViewAuthenticator<? super A> viewAuthenticator)
  {
    super(formAtMgr,
          NAME,
          "Create new SubDoc",
          viewAuthenticator,
          InternalSuccessOperation.getInstance());
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           Document document)
      throws NodeRequestException
  {
    // TODO Auto-generated method stub
    return null;
  }

}
