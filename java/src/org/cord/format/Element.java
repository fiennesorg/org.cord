package org.cord.format;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import org.json.JSONException;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

/**
 * An Element is the abstract superclass of an item that is part of building a FormAt Document.
 * 
 * @author alex
 */
public abstract class Element
{
  public static final String DATA_FACTORY = "data-factory";

  private final Document __document;
  private final ElementFactory __elementFactory;

  public Element(Document document,
                 ElementFactory elementFactory)
  {
    __document = checkNotNull(document);
    __elementFactory = checkNotNull(elementFactory);
  }

  /**
   * @return The controlling FormAtMgr - never null.
   */
  public final FormAtMgr<?> getFormAtMgr()
  {
    return getFactory().getFormAtMgr();
  }

  /**
   * @return The containing Document - never null
   */
  public final Document getDocument()
  {
    return __document;
  }

  /**
   * @return The ElementFactory that produced this Element - never null
   */
  public ElementFactory getFactory()
  {
    return __elementFactory;
  }

  /**
   * Transform this Element into the HTML that should be shown inside the Redactor editor when in
   * edit mode. This may involve parsing a template to generate the output, but the code that is
   * appended to buf should be static HTML that can be shown and edited in redactor as-is.
   */
  public abstract void appendRedactorEditHtml(Appendable buf,
                                              Context context)
      throws IOException, ResourceException, PropertyException, JSONException;

  /**
   * Transform this Element into the public ReadOutput format to represent published form of
   * contents.
   */
  public abstract ReadOutput toReadOutput(Context context);

  /**
   * Convert this Element into a representation in JSON that is suitable to be added to the JSON
   * structure that represents the the Element tree for the containg document..
   * 
   * @return The valid JSON representation.
   */
  public abstract Object toJSON()
      throws JSONException;

  protected void toString(ToStringHelper tsh)
  {
    tsh.add("factory", getFactory());
  }

  @Override
  public String toString()
  {
    ToStringHelper tsh = MoreObjects.toStringHelper(this);
    toString(tsh);
    return tsh.toString();
  }

  public abstract boolean isWhitespace();
}
