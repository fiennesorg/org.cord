package org.cord.format;

/**
 * SubDocLayout is the parent class for implementations that state how a {@link SubDocElement}
 * should behave in the document flow. Each {@link SubDocElement} will have 1 {@link SubDocLayout}.
 * Alternative sub-classes provide different models as to how the layout should function.
 * 
 * @author alex
 */
public abstract class SubDocLayout
  extends SubDocStyle
{

}
