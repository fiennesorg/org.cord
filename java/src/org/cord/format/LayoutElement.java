package org.cord.format;

import java.util.List;

import org.cord.util.Gettable;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

public class LayoutElement
  extends EditableContainerElement
{
  public static final String NAME = "Layout";

  public static void register(FormAtMgr<?> formAtMgr)
  {
    formAtMgr.register(new EditableContainerElementFactory(formAtMgr,
                                                           NAME,
                                                           RedactorTagType.LAYOUT_DIV) {
      @Override
      protected EditableContainerElement createElement(Document document,
                                                       String key,
                                                       JSONObject metadata,
                                                       List<Element> childElements,
                                                       Gettable initParams)
      {
        return new LayoutElement(document, this, key, metadata, childElements);
      }
    });
  }

  public LayoutElement(Document document,
                       EditableElementFactory elementFactory,
                       String elementKey,
                       JSONObject metadata,
                       List<Element> childElements)
  {
    super(document,
          elementFactory,
          elementKey,
          metadata,
          childElements);
  }

  public static final String JSON_CSSCLASS = "cssClass";
  public static final String IN_CSSCLASS = JSON_CSSCLASS;

  @Override
  public void update(Gettable in)
      throws Exception
  {
    getMetaData().put(JSON_CSSCLASS, in.getString(IN_CSSCLASS));
  }

  @Override
  public String getModalEditTemplatePath(Context context)
      throws PropertyException, ResourceException
  {
    return "FormAt/Layout/Layout.edit.wm.html";
  }

  @Override
  public WritableJSONObject getDataAttributes()
      throws JSONException
  {
    WritableJSONObject attributes = super.getDataAttributes();
    attributes.put("class", getMetaData().optString(JSON_CSSCLASS));
    return attributes;
  }

}
