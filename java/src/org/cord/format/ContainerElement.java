package org.cord.format;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.WritableJSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects.ToStringHelper;

public abstract class ContainerElement
  extends JsonObjectElement
{

  private final List<Element> __elements;

  protected ContainerElement(Document document,
                             ElementFactory elementFactory,
                             List<Element> elements)
  {
    super(document,
          elementFactory);
    __elements = elements;
  }

  public List<Element> getContents()
  {
    return __elements;
  }

  protected ReadOutput getNestedReadOutput(Context context)
  {
    return Elements.toReadOutput(getContents(), context);
  }

  protected abstract String getHeaderEditHtml(Context context);
  protected abstract String getFooterEditHtml(Context context);

  @Override
  public void appendRedactorEditHtml(Appendable buf,
                                     Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    buf.append(getHeaderEditHtml(context));
    Elements.appendRedactorHtml(buf, context, getContents());
    buf.append(getFooterEditHtml(context));
  }

  public static final String JSON_ELEMENTS = Document.JSON_ELEMENTS;

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject container = super.toJSON();
    container.put(JSON_ELEMENTS, getFormAtMgr().elementsToJSONArray(getContents()));
    return container;
  }

  protected abstract ReadOutput getHeaderReadOutput(Context context);

  protected abstract ReadOutput getFooterReadOutput(Context context);

  @Override
  public final ReadOutput toReadOutput(Context context)
  {
    return getHeaderReadOutput(context).append(getNestedReadOutput(context))
                                       .append(getFooterReadOutput(context));
  }

  @Override
  protected void toString(ToStringHelper tsh)
  {
    super.toString(tsh);
    tsh.add("elementsContainer", getContents());
  }

  @Override
  public boolean isWhitespace()
  {
    for (Element element : getContents()) {
      if (!element.isWhitespace()) {
        return false;
      }
    }
    return true;
  }
}
