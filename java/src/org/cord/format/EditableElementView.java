package org.cord.format;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.webmacro.Context;

/**
 * Base class for a View that resolves a given Element from a given ElementFactory and passes it to
 * the implementing class.
 */
public abstract class EditableElementView<A extends AppMgr>
  extends EditableElementFactoryView<A>
{
  public static final String IN_ELEMENTKEY = "elementKey";

  public static final String OUT_ELEMENT = "element";

  public EditableElementView(FormAtMgr<? extends A> formAtMgr,
                             String name,
                             String title,
                             ViewAuthenticator<? super A> viewAuthenticator)
  {
    super(formAtMgr,
          name,
          title,
          viewAuthenticator,
          InternalSuccessOperation.getInstance());
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           Document document,
                                           EditableElementFactory factory)
      throws NodeRequestException
  {
    System.err.println(getName());
    String key = nodeRequest.getString(IN_ELEMENTKEY);
    EditableElement element;
    try {
      element = factory.getExistingElement(document, key);
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       e,
                                       "Unable to resolve %s-%s#%s",
                                       factory,
                                       IN_ELEMENTKEY,
                                       key);
    }
    context.put(OUT_ELEMENT, element);
    return getInstance(nodeRequest, node, context, acl, element);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    EditableElement element)
      throws NodeRequestException;

  public static String getUrl(String viewName,
                              Document document,
                              EditableElementFactory factory,
                              String elementKey)
  {
    return getUrl(viewName, document, factory) + "&" + IN_ELEMENTKEY + "=" + elementKey;
  }

}
