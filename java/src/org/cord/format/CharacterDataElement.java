package org.cord.format;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.webmacro.Context;

/**
 * {@link Element} implementation that wraps a constant String of character data.
 */
public class CharacterDataElement
  extends Element
{
  private String __contents;

  protected CharacterDataElement(Document document,
                                 CharacterDataFactory characterDataFactory,
                                 String contents)
  {
    super(document,
          characterDataFactory);
    __contents = checkNotNull(contents);
  }

  @Override
  public void appendRedactorEditHtml(Appendable buf,
                                     Context context)
      throws IOException
  {
    buf.append(__contents);
  }

  @Override
  public ReadOutput toReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, __contents);
  }

  @Override
  public Object toJSON()
      throws JSONException
  {
    return __contents;
  }

  @Override
  public String toString()
  {
    return "\"" + __contents + "\"";
  }

  /**
   * @return true if there is no visible characters contained in the contents string
   */
  @Override
  public boolean isWhitespace()
  {
    return StringUtils.isWhitespace(__contents);
  }

}
