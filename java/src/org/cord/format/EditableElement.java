package org.cord.format;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Iterator;

import org.cord.util.ExceptionUtil;
import org.cord.util.Gettable;
import org.cord.webmacro.TagBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.base.Preconditions;

public abstract class EditableElement
  extends JsonObjectElement
{
  private final String __key;

  private final JSONObject __metadata;

  public EditableElement(Document document,
                         EditableElementFactory factory,
                         String key,
                         JSONObject metadata)
  {
    super(document,
          factory);
    __key = checkNotNull(key, "key");
    __metadata = Preconditions.checkNotNull(metadata, "metadata");
  }

  @Override
  public EditableElementFactory getFactory()
  {
    return (EditableElementFactory) super.getFactory();
  }

  public final String getKey()
  {
    return __key;
  }

  public final JSONObject getMetaData()
  {
    return __metadata;
  }

  /**
   * expose the JSON contents of {@link #getMetaData()} via a Map style accessor for webmacro
   * compatibility.
   */
  public final Object get(Object key)
  {
    return getMetaData().opt(key.toString());
  }

  public static final String DATA_KEY = "key";

  /**
   * Get the JSON representation of the attributes that should be made available to the HTML layer
   * as attributes on the editable Element tag. Subclasses should extend this method rather than
   * replacing it.
   * 
   * @return {@link WritableJSONObject} with the {@link Element_RenderGui#DATA_WIDTH},
   *         {@link Element_RenderGui#DATA_HEIGHT} and {@link Element_RenderGui#DATA_EDITURL} values
   *         defined.
   */
  public WritableJSONObject getDataAttributes()
      throws JSONException
  {
    WritableJSONObject attr = new WritableJSONObject();
    attr.put(Element_RenderGui.DATA_WIDTH, getEditWidth())
        .put(Element_RenderGui.DATA_HEIGHT, getEditHeight())
        .put(Element_RenderGui.DATA_EDITURL,
             Element_RenderGui.getUrl(getDocument(), getFactory(), getKey()))
        .put(DATA_FACTORY, getFactory().getName())
        .put(DATA_KEY, getKey());
    // .put(Element_RenderPreview.DATA_PREVIEWURL,
    // Element_RenderPreview.getUrl(getDocument(), getFactory(), getKey()));
    return attr;
  }

  /**
   * Get the width of the edit interface for this Element.
   * 
   * @return default value of 400 - subclasses should override this as required.
   */
  public int getEditWidth()
  {
    return 400;
  }

  /**
   * Get the height of the edit interface for this Element.
   * 
   * @return default value of 400 - subclasses should override this as required.
   */
  public int getEditHeight()
  {
    return 400;
  }

  /**
   * Resolve the given webmacro template and evaluate it against the supplied Context as a String.
   */
  protected final String evaluateAsString(String template,
                                          Context context)
      throws PropertyException, ResourceException
  {
    return getFactory().getFormAtMgr()
                       .getWebMacro()
                       .getTemplate(template)
                       .evaluateAsString(context);
  }

  /**
   * The values held in this Element should be updated to match the values contained in the Gettable
   * in. This will generally be invoked by the processing the output of
   * {@link #getModalEditTemplatePath(Context)}, although this is not guaranteed.
   * 
   * @throws Exception
   *           Any exceptions thrown (runtime or not) will be treated as a failure condition and
   *           reported to the user.
   */
  public abstract void update(Gettable in)
      throws Exception;

  @Override
  protected void toString(ToStringHelper tsh)
  {
    super.toString(tsh);
    tsh.add("key", getKey());
  }

  @Override
  public final ReadOutput toReadOutput(Context context)
  {
    ReadOutput readOutput = ReadOutput.EMPTY;
    readOutput = readOutput.append(toReadOutputHeader(context));
    try {
      readOutput = readOutput.append(toWrappedReadOutput(context));
    } catch (Exception e) {
      readOutput =
          readOutput.append(new ReadOutput(OutputFormat.HTML,
                                           "<pre>" + ExceptionUtil.printStackTrace(e) + "</pre>"));
    }
    readOutput = readOutput.append(toReadOutputFooter(context));
    return readOutput;
  }

  public ReadOutput toReadOutputHeader(Context context)
  {
    TagBuilder htmlTag = new TagBuilder(getFactory().getRedactorTagType().getHtmlTag());
    htmlTag.addAttribute("class", "JsonElement");
    return new ReadOutput(OutputFormat.HTML, htmlTag.build(true));
  }

  public ReadOutput toReadOutputFooter(Context context)
  {
    return new ReadOutput(OutputFormat.HTML,
                          new TagBuilder(getFactory().getRedactorTagType()
                                                     .getHtmlTag()).getClose());
  }

  public abstract ReadOutput toWrappedReadOutput(Context context)
      throws IOException, ResourceException, PropertyException, JSONException;

  @Override
  public final void appendRedactorEditHtml(Appendable buf,
                                           Context context)
      throws IOException, ResourceException, PropertyException, JSONException
  {
    TagBuilder htmlTag = getFactory().getRedactorTagType().getTagBuilder(getFactory().getName());
    JSONObject data = getDataAttributes();
    Iterator<String> keys = data.keys();
    while (keys.hasNext()) {
      String key = keys.next();
      htmlTag.addAttribute(key, data.get(key));
    }
    htmlTag.build(buf, true);
    appendWrappedEditHtml(buf, context);
    htmlTag.close(buf);
  }

  public abstract void appendWrappedEditHtml(Appendable buf,
                                             Context context)
      throws IOException, ResourceException, PropertyException, JSONException;

  public abstract String getModalEditTemplatePath(Context context)
      throws PropertyException, ResourceException;

  public static final String JSON_KEY = "key";

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject jObj = super.toJSON();
    jObj.put(JSON_KEY, getKey());
    return jObj;
  }

}
