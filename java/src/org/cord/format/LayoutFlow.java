package org.cord.format;

/**
 * A sequence of one or more {@link LayoutFlow} {@link SubDocElement}s will break the default
 * document flow and fill up the available space, wrapping onto more lines if necessary. The size of
 * the elements will remain consistent as the responsive design gets smaller so they may wrap onto
 * more lines as a result. As soon as a block {@link Element} follows that isn't a
 * {@link LayoutFlow} then the layout will revert to the appropriate model for that
 * {@link SubDocLayout}
 * 
 * @author alex
 */
public class LayoutFlow
  extends SubDocLayout
{

}
