package org.cord.format;

/**
 * A LayoutDivider breaks the default flow of the document and introduces a horizontal strip that is
 * split up into one or more LayoutDivider components where the width is expressed as a percentage
 * of the total width of the available space. This space will compact as the responsive layout
 * collapses - ie a 50% LayoutDivider will always be 50% of the available space. As soon as you get
 * a block that whose {@link SubDocLayout} is not a LayoutDivider then the document flow returns to
 * the appropriate model for the given {@link SubDocLayout}.
 * 
 * @author alex
 */
public class LayoutDivider
  extends SubDocLayout
{

}
