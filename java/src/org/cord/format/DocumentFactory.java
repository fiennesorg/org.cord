package org.cord.format;

import org.cord.mirror.Viewpoint;
import org.cord.node.app.AppMgr;
import org.json.JSONException;

import com.google.common.base.Preconditions;

public abstract class DocumentFactory<A extends AppMgr>
{
  private final FormAtMgr<A> __formAtMgr;

  public DocumentFactory(FormAtMgr<A> formAtMgr)
  {
    __formAtMgr = Preconditions.checkNotNull(formAtMgr, "formAtMgr");
  }

  public FormAtMgr<A> getFormAtMgr()
  {
    return __formAtMgr;
  }

  /**
   * Create a new Document with an internally derived unique id. Note that it is not the
   * responsibility of the DocumentFactory to save the Document as part of this method.
   */
  public abstract Document createDocument(String url)
      throws JSONException;

  /**
   * Resolve an existing Document by id, returning null if it is not possible to resolve id for
   * whatever reason.
   */
  public abstract Document optDocument(Integer id,
                                       Viewpoint viewpoint);

  public abstract void saveDocument(Document document);
}
