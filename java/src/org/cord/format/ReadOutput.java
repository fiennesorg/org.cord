package org.cord.format;

import org.cord.util.StringUtils;

public class ReadOutput
{
  public static final ReadOutput EMPTY = new ReadOutput(OutputFormat.HTML, "");

  private final OutputFormat __outputFormat;
  private final String __output;

  public ReadOutput(OutputFormat outputFormat,
                    String output)
  {
    __outputFormat = outputFormat;
    __output = output;
  }

  public final OutputFormat getOutputFormat()
  {
    return __outputFormat;
  }

  public final String getOutput()
  {
    return __output;
  }

  private String getTemplateInclude(String templatePath)
  {
    return "#include as template \"" + templatePath + "\"";
  }

  @Override
  public String toString()
  {
    return "ReadOutput(" + __outputFormat + ", \"" + getOutput() + "\")";
  }

  public ReadOutput append(OutputFormat outputFormat,
                           String output)
  {
    if (output.length() == 0) {
      return this;
    }
    switch (this.getOutputFormat()) {
      case HTML:
        switch (outputFormat) {
          case HTML:
            return new ReadOutput(OutputFormat.HTML, this.getOutput() + output);
          case WEBMACRO:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  StringUtils.noWebmacro(this.getOutput()) + output);
          case TEMPLATEPATH:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  StringUtils.noWebmacro(this.getOutput())
                                                         + getTemplateInclude(output));
        }
        break;
      case WEBMACRO:
        switch (outputFormat) {
          case HTML:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  this.getOutput() + StringUtils.noWebmacro(output));
          case WEBMACRO:
            return new ReadOutput(OutputFormat.WEBMACRO, this.getOutput() + output);
          case TEMPLATEPATH:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  this.getOutput() + getTemplateInclude(output));
        }
        break;
      case TEMPLATEPATH:
        switch (outputFormat) {
          case HTML:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  getTemplateInclude(this.getOutput())
                                                         + StringUtils.noWebmacro(output));
          case WEBMACRO:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  getTemplateInclude(this.getOutput()) + output);
          case TEMPLATEPATH:
            return new ReadOutput(OutputFormat.WEBMACRO,
                                  getTemplateInclude(this.getOutput())
                                                         + getTemplateInclude(output));
        }
        break;
    }
    throw new IllegalStateException(this + ".append(" + outputFormat
                                    + ",...) has unknown combination of OutputFormat");
  }

  public ReadOutput append(ReadOutput that)
  {
    if (that == null || that.getOutput().length() == 0) {
      return this;
    }
    return append(that.getOutputFormat(), that.getOutput());
  }

  public ReadOutput appendLineBreak()
  {
    return new ReadOutput(getOutputFormat(), getOutput() + "\n");
  }
}
