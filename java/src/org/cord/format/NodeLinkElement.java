package org.cord.format;

import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.webmacro.TagBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.Optional;

public class NodeLinkElement
  extends EditableContainerElement
{
  public static final String NAME = "NodeLink";

  public static void register(FormAtMgr<?> formAtMgr)
  {
    formAtMgr.register(new EditableContainerElementFactory(formAtMgr,
                                                           NAME,
                                                           RedactorTagType.CONTAINER_INLINE) {
      @Override
      protected EditableContainerElement createElement(Document document,
                                                       String elementKey,
                                                       JSONObject metadata,
                                                       List<Element> childElements,
                                                       Gettable initParams)
      {
        return new NodeLinkElement(document, this, elementKey, metadata, childElements);
      }
    });
  }

  public static final String JSON_TARGETNODE_ID = "targetNode_id";
  public static final String IN_TARGETNODE_ID = JSON_TARGETNODE_ID;

  private NodeLinkElement(Document document,
                          EditableElementFactory elementFactory,
                          String elementKey,
                          JSONObject metadata,
                          List<Element> childElements)
  {
    super(document,
          elementFactory,
          elementKey,
          metadata,
          childElements);
  }

  @Override
  public void update(Gettable in)
      throws JSONException
  {
    getMetaData().put(JSON_TARGETNODE_ID, NumberUtil.toInteger(in.get(IN_TARGETNODE_ID)));
  }

  @Override
  public String getModalEditTemplatePath(Context context)
      throws PropertyException, ResourceException
  {
    return "FormAt/NodeLink/NodeLink.edit.wm.html";
  }

  public Optional<PersistentRecord> getTargetNode()
  {
    return Optional.fromNullable(getFormAtMgr().getNodeMgr()
                                               .getNode()
                                               .getTable()
                                               .getOptRecord(getMetaData().optInteger(JSON_TARGETNODE_ID)));
  }

  @Override
  public ReadOutput toReadOutputHeader(Context context)
  {
    TagBuilder a = new TagBuilder("a");
    Optional<PersistentRecord> targetNode = getTargetNode();
    if (targetNode.isPresent()) {
      a.addAttribute("href", targetNode.get().comp(Node.URL));
    }
    return new ReadOutput(OutputFormat.HTML, a.build(true));
  }

  @Override
  public ReadOutput toReadOutputFooter(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, "</a>");
  }

}
