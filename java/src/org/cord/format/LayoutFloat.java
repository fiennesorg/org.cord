package org.cord.format;

/**
 * A {@link LayoutFloat} {@link SubDocElement} will sit to the left or right of the default document
 * flow and may interrupt it. It will have a width and optional left and right margins.
 * 
 * @author alex
 */
public class LayoutFloat
  extends SubDocLayout
{

}
