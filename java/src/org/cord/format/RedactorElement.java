package org.cord.format;

import java.util.List;

import org.cord.webmacro.TagBuilder;
import org.json.JSONException;
import org.json.WritableJSONObject;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;

public class RedactorElement
  extends ContainerElement
{
  private final Optional<String> __editClass;
  private final Optional<String> __readClass;
  private final TagBuilder __editTagBuilder;
  private final TagBuilder __readTagBuilder;

  public static final String TAG_CLASS = "class";

  public RedactorElement(Document document,
                         RedactorElementFactory redactorFactory,
                         Optional<String> editClass,
                         Optional<String> readClass,
                         List<Element> contents)
  {
    super(document,
          redactorFactory,
          contents);
    __editTagBuilder = new TagBuilder(redactorFactory.getName());
    __editClass = editClass;
    if (editClass.isPresent()) {
      __editTagBuilder.addAttribute(TAG_CLASS, editClass.get());
    }
    __readClass = readClass;
    __readTagBuilder = new TagBuilder(redactorFactory.getName());
    if (readClass.isPresent()) {
      __readTagBuilder.addAttribute(TAG_CLASS, readClass.get());
    }
  }

  @Override
  public RedactorElementFactory getFactory()
  {
    return (RedactorElementFactory) super.getFactory();
  }

  @Override
  protected String getFooterEditHtml(Context context)
  {
    return __editTagBuilder.getClose();
  }

  @Override
  protected ReadOutput getFooterReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, __readTagBuilder.getClose());
  }

  @Override
  protected String getHeaderEditHtml(Context context)
  {
    return __editTagBuilder.getOpen();
  }

  @Override
  protected ReadOutput getHeaderReadOutput(Context context)
  {
    return new ReadOutput(OutputFormat.HTML, __readTagBuilder.getOpen());
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("tag", getFactory().getName())
                      .add("contents", getContents())
                      .toString();
  }

  public static final String JSON_READCLASS = "readClass";
  public static final String JSON_EDITCLASS = "editClass";

  @Override
  public WritableJSONObject toJSON()
      throws JSONException
  {
    WritableJSONObject json = super.toJSON();
    if (__editClass.isPresent()) {
      json.put(JSON_EDITCLASS, __editClass.get());
    }
    if (__readClass.isPresent()) {
      json.put(JSON_READCLASS, __readClass.get());
    }
    return json;
  }

}
