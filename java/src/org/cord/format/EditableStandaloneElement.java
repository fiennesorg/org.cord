package org.cord.format;

import org.json.JSONObject;

public abstract class EditableStandaloneElement
  extends EditableElement
{

  public EditableStandaloneElement(Document document,
                                   EditableElementFactory factory,
                                   String key,
                                   JSONObject metadata)
  {
    super(document,
          factory,
          key,
          metadata);
  }

  /**
   * @return false because an EditableStandaloneElement will contain something visible even if it is
   *         a message stating that further configuration is required. Override this if this is not
   *         the case.
   */
  @Override
  public boolean isWhitespace()
  {
    return false;
  }

}
