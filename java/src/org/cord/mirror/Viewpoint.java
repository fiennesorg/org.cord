package org.cord.mirror;

import java.util.Collection;
import java.util.Set;

public interface Viewpoint
{
  public final static Viewpoint NULL = null;

  public int getType();

  public boolean isUpdated();

  public boolean isUpdatedTable(Table table);

  public Set<Table> getUpdatedTables();

  public boolean contains(PersistentRecord record);

  public boolean contains(Table table,
                          int id);

  /**
   * Get the given record from the perspective of this Viewpoint as appropriate.
   * 
   * @return Either the appropriate WritableRecord or the PersistentRecord if this Viewpoint doesn't
   *         hold a lock on the record.
   * @throws MirrorNoSuchRecordException
   *           If it is not possible to resolve the record.
   */
  public PersistentRecord getRecord(Table table,
                                    Integer id)
      throws MirrorNoSuchRecordException;

  /**
   * Filter the given record from the perspective of this Viewpoint as appropriate.
   * 
   * @param record
   *          The optional record to filter
   * @return null if record was null, failing that the appropriate PersistentRecord or
   *         WritableRecord depending on whether record is locked by this Viewpoint
   */
  public PersistentRecord getRecord(PersistentRecord record);

  public int getRecordCount();

  public int getRecordCount(Table table);

  public Collection<PersistentRecord> getRecords(Table table);

  /**
   * Get a {@link Set} of the {@link Table} that are locked in some way under this Viewpoint.
   * 
   * @return The locked tables. If there are none then this should be an empty Set rather than null.
   */
  public Set<Table> getTables();

  public String debug();
}
