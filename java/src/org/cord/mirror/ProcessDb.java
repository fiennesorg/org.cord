package org.cord.mirror;

import java.util.ArrayList;
import java.util.List;

/**
 * Take a configured and initialised Db and then apply a series of DbProcessor objects to it.
 * 
 * @author alex
 * @see DbProcessor
 */
public class ProcessDb
{
  private final List<DbProcessor> __dbProcessors = new ArrayList<DbProcessor>();

  private final Db __db;

  private final String __pwd;

  public ProcessDb(Db db,
                   String pwd)
  {
    __db = db;
    __pwd = pwd;
  }

  public void addDbProcessor(DbProcessor processor)
  {
    if (processor != null) {
      __dbProcessors.add(processor);
    }
  }

  public List<DbProcessor> getApplicableDbProcessors(Table table)
  {
    List<DbProcessor> applicableProcessors = new ArrayList<DbProcessor>();
    for (DbProcessor dbProcessor : __dbProcessors) {
      if (dbProcessor.shouldProcess(table)) {
        applicableProcessors.add(dbProcessor);
      }
    }
    return applicableProcessors;
  }

  public void applyProcessors(List<DbProcessor> dbProcessors,
                              PersistentRecord roRecord)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    try (Transaction t =
        __db.createTransaction(Db.DEFAULT_TIMEOUT, Transaction.TRANSACTION_UPDATE, __pwd)) {
      WritableRecord rwRecord = t.edit(roRecord);
      for (DbProcessor dbp : dbProcessors) {
        dbp.process(rwRecord);
      }
      t.commit();
    } 
  }

  public void runDbProcessors()
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    for (Table table : __db.getTables()) {
      List<DbProcessor> applicableProcessors = getApplicableDbProcessors(table);
      // TODO: work out whether or not we need equivalent of below MnsrEx
      // catching
      if (applicableProcessors.size() > 0) {
        for (PersistentRecord r : table) {
          applyProcessors(applicableProcessors, r);
        }
        // RecordIdList ids = table.getAllRecordsQuery().getIds();
        // for (int i = 0; i < ids.size(); i++) {
        // try {
        // PersistentRecord r = table.getRecord(ids.get(i));
        // applyProcessors(applicableProcessors, r);
        // } catch (MirrorNoSuchRecordException e) {
        // // ignore this - not important - no record means no processing...
        // }
        // }
      }
    }
  }
}
