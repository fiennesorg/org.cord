package org.cord.mirror;

import org.cord.util.DuplicateNamedException;
import org.cord.util.Named;
import org.cord.util.Shutdownable;

/**
 * Interface describing db initialisers that may be passed to a Db object to provide functionality
 * for the db.
 */
public interface DbInitialiser
  extends Named, Shutdownable
{
  /**
   * Request the DbInitialiser to set-up the db with the appropriate permissions.
   * 
   * @throws MirrorTableLockedException
   *           If the database has already locked some tables that the initialiser requires to
   *           update to complete the operation.
   * @throws DuplicateNamedException
   *           If the initialiser attempts to create some features on the db that already exist.
   *           This implies a namespace clash between initialisers which can be avoided through the
   *           careful design of naming conventions.
   */
  public void init(Db db,
                   String pwd)
      throws MirrorTableLockedException, DuplicateNamedException;
}
