package org.cord.mirror;

public class DeletedRecordException
  extends MissingRecordException
{
  /**
   * MissingRecordException that signifies that the record that you have asked for has been recently
   * deleted.
   */
  private static final long serialVersionUID = 1L;

  public DeletedRecordException(String message,
                                Table table,
                                Integer id,
                                Throwable cause)
  {
    super(message,
          table,
          id,
          cause);
  }
}
