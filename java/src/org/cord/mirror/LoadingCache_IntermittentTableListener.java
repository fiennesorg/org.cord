package org.cord.mirror;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.cord.util.DebugConstants;
import org.cord.util.NamedImpl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;

/**
 * Wrapper around a guava {@link LoadingCache} that is automatically invalidated when messages from
 * given TableListeners are received thereby forcing recalculation.
 * 
 * @author alex
 * @param <K>
 *          The class of keys that are used to resolve values from the TableListeningCache.
 * @param <V>
 *          The class of values that are retrieved from the TableListeningCache.
 */
public abstract class LoadingCache_IntermittentTableListener<K, V>
  extends NamedImpl
  implements TableListener
{
  private final LoadingCache<K, V> __cache;
  private final AtomicBoolean __hasData = new AtomicBoolean(false);
  private final Object __sync = new Object();
  private ImmutableList<Table> _tableListenerTargets = null;

  public LoadingCache_IntermittentTableListener(String name)
  {
    super(name);
    __cache = CacheBuilder.newBuilder().softValues().build(new CacheLoader<K, V>() {
      @Override
      public V load(K key)
          throws Exception
      {
        DebugConstants.method(this, "load", key);
        V v = calculate(key, null);
        if (!__hasData.get()) {
          synchronized (__sync) {
            addTableListeners();
            __hasData.set(true);
          }
        }
        return v;
      }

      @Override
      public String toString()
      {
        return LoadingCache_IntermittentTableListener.this + "$CacheLoader";
      }
    });
  }

  protected abstract V calculate(K key,
                                 Viewpoint viewpoint)
      throws Exception;

  protected abstract ImmutableList<Table> getTableListenerTargets();

  protected ImmutableList<Table> getCachedTableListenerTargets()
  {
    if (_tableListenerTargets == null) {
      _tableListenerTargets = getTableListenerTargets();
    }
    return _tableListenerTargets;
  }

  private void addTableListeners()
  {
    DebugConstants.method(this, "registerTableListeners");
    for (Table table : getTableListenerTargets()) {
      table.addTableListener(this);
    }
  }

  /**
   * @return false because we want to de-register ourselves from future updates because we will
   *         re-register ourselves if more updates are received.
   */
  @Override
  public boolean tableChanged(Table table,
                              int recordId,
                              PersistentRecord persistentRecord,
                              int type)
  {
    // DebugConstants.method(this, "tableChanged", table, recordId, persistentRecord, type);
    synchronized (__sync) {
      __cache.invalidateAll();
      __hasData.set(false);
    }
    return false;
  }

  public final V get(K key)
      throws ExecutionException
  {
    return __cache.get(key);
  }

}
