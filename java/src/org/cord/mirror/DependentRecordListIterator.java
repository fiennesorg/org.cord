package org.cord.mirror;

import org.cord.mirror.recordsource.ArrayIdList;

/**
 * RecordListIterator that precaches dependent records from a link record on the source Table that
 * is being iterated across. If you know that you are going to be following a link on every record
 * that you are iterating across then this will give you considerably better performance than
 * resolving items record by record.
 * 
 * @author alex
 */
public class DependentRecordListIterator
  extends RecordListIterator
{
  private final Table __dependentTable;
  private final IntFieldKey __dependentIdKey;

  /**
   * @param sourceRecordList
   *          the RecordList from the source Table that is being iterated across.
   * @param dependentTable
   *          The Table that is linked to from the source records.
   * @param dependentIdKey
   *          The FieldKey that contains the id of the dependent record.
   */
  public DependentRecordListIterator(RecordList sourceRecordList,
                                     Table dependentTable,
                                     IntFieldKey dependentIdKey)
  {
    super(sourceRecordList);
    __dependentTable = dependentTable;
    __dependentIdKey = dependentIdKey;
  }

  /**
   * Precache the precacheSize number of dependent records.
   */
  @Override
  protected void precacheCompleteHook(RecordList sourceRecordList,
                                      int startIndex,
                                      int precacheSize)
  {
    IdList dependentIdList = new ArrayIdList(__dependentTable, precacheSize);
    final int endIndex = startIndex + precacheSize;
    for (int i = startIndex; i < endIndex; i++) {
      PersistentRecord sourceRecord = sourceRecordList.get(i);
      dependentIdList.add(sourceRecord.compInt(__dependentIdKey));
    }
    Table.resolveRecords(dependentIdList, 0, precacheSize);
  }
}
