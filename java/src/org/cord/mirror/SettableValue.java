package org.cord.mirror;

import java.io.File;
import java.util.Collection;

import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.sql.SqlUtil;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;
import org.cord.util.Settable;
import org.cord.util.StringUtils;
import org.cord.util.TypedKey;
import org.cord.util.ValueType;

import com.google.common.collect.ImmutableList;

/**
 * Mirror model of the table that is used to store values in the persistent model of Gettable. The
 * model supports the concept of a source Gettable that defines the default values for items in the
 * Gettable known as the originalValues which is to be used for supplying values in the case of
 * values not being defined in the db. However, this goes a little bit further and maintains a
 * record of the value defined in originalValues in the persistent version, and if originalValues
 * changes then it will override the value stored in the Db. This may be summed up in the following
 * rules:-
 * <ul>
 * <li>If there is no value defined in persistentValues then the value in originalValues is
 * utilised.
 * <li>If there is a value defined in persistentValues, then if the persistent originalValue is the
 * same as the current originalValue then the value defined in persistentValue is returned. This
 * would be the case when the default in originalValues has been overruled by a new value in
 * persistentValues.
 * <li>If there is a value defined in persistentValues, and the persistent originalValue is no
 * longer the same as the value in originalValues then the value from originalValues is returned and
 * the persistent version is updated to reflect the status of the originalValues. This would be the
 * case when the default version was updated since the persistent version was created, and therefore
 * the new defaults should take priority over the previously overridden persistent default.
 * </ul>
 */
public class SettableValue
  extends AbstractTableWrapper
{
  public final static String TABLENAME = "SettableValue";

  public final static ObjectFieldKey<String> NAMESPACE = StringField.createKey("namespace");

  public final static ObjectFieldKey<String> VALUEKEY = StringField.createKey("valueKey");

  public final static ObjectFieldKey<String> VALUE = StringField.createKey("value");

  public final static ObjectFieldKey<String> ORIGINALVALUE = StringField.createKey("originalValue");

  public SettableValue()
  {
    super(TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String transactionPassword,
                        Table _settableValue)
      throws MirrorTableLockedException
  {
    _settableValue.addField(new StringField(NAMESPACE, "Namespace for key", 32, ""));
    _settableValue.addFieldTrigger(ImmutableFieldTrigger.create(NAMESPACE));
    _settableValue.addField(new StringField(VALUEKEY, "Key"));
    _settableValue.addFieldTrigger(ImmutableFieldTrigger.create(VALUEKEY));
    _settableValue.addField(new StringField(VALUE, "Value"));
    _settableValue.addField(new StringField(ORIGINALVALUE, "Original value"));
  }

  private String getFilter(String namespace,
                           String key)
  {
    StringBuilder filter = new StringBuilder();
    filter.append(NAMESPACE).append('=');
    SqlUtil.toSafeSqlString(filter, namespace);
    filter.append(" and ").append(VALUEKEY).append('=');
    SqlUtil.toSafeSqlString(filter, key);
    return filter.toString();
  }

  public PersistentRecord getRecord(String namespace,
                                    String key)
      throws MirrorNoSuchRecordException, MissingRecordException
  {
    String filter = getFilter(namespace, key);
    Query records = getTable().getQuery(filter, filter, null);
    return RecordSources.getFirstRecord(records, null);
  }

  private void update(TransientRecord record,
                      String value,
                      String originalValue)
      throws MirrorFieldException
  {
    if (value != null) {
      record.setField(VALUE, value);
    }
    if (originalValue != null) {
      record.setField(ORIGINALVALUE, originalValue);
    }
  }

  public String set(String namespace,
                    String key,
                    String value,
                    Gettable originalValues)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException
  {
    String originalValue = originalValues.getString(key);
    try {
      PersistentRecord dbValue = getRecord(namespace, key);
      try (Transaction t = getTable().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                Transaction.TRANSACTION_UPDATE,
                                                                getTransactionPassword(),
                                                                "SettableValue.set(...)")) {
        dbValue = t.edit(dbValue, Db.DEFAULT_TIMEOUT);
        update(dbValue, value, originalValue);
        t.commit();
      }
    } catch (MirrorNoSuchRecordException newValueEx) {
      TransientRecord newValue = getTable().createTransientRecord();
      newValue.setField(NAMESPACE, namespace);
      newValue.setField(VALUEKEY, key);
      update(newValue, value, originalValue);
      newValue.makePersistent(null);
    }
    return originalValue;
  }

  public String get(String namespace,
                    String key,
                    Gettable originalValues)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException
  {
    String originalValue =
        StringUtils.toString(originalValues == null ? null : originalValues.get(key));
    try {
      PersistentRecord dbValue = getRecord(namespace, key);
      String value = dbValue.opt(VALUE);
      if (originalValue == null || originalValue.equals(dbValue.opt(ORIGINALVALUE))) {
        return value;
      }
      // if we get here, then it implies that the original values have
      // been updated since the value in the db was last updated so we
      // reset the db...
      set(namespace, key, originalValue, originalValues);
      return originalValue;
    } catch (MirrorNoSuchRecordException mnsrEx) {
      return originalValue;
    }
  }

  public Gettable getGettable(String namespace,
                              Gettable originalValues)
  {
    return new GettableView(namespace, originalValues);
  }

  public Settable getSettable(String namespace,
                              Gettable originalValues,
                              String settableKeysKey,
                              String splitRegex)
  {
    return new SettableView(namespace, originalValues, settableKeysKey, splitRegex);
  }

  private class GettableView
    extends NamedImpl
    implements Gettable
  {
    private final Gettable __originalValues;

    public GettableView(String namespace,
                        Gettable originalValues)
    {
      super(namespace);
      __originalValues = originalValues;
    }

    @Override
    public Object get(Object key)
    {
      try {
        return SettableValue.this.get(getName(), key.toString(), __originalValues);
      } catch (MirrorException mEx) {
        throw new LogicException("Unable to update persistent store", mEx);
      }
    }

    @Override
    public boolean containsKey(Object key)
    {
      return get(key) != null;
    }

    /**
     * @return The value of getPublicKeys() on the original Gettable that this db wrapper is
     *         extending.
     * @see #getOriginalValues()
     */
    @Override
    public Collection<String> getPublicKeys()
    {
      return getOriginalValues().getPublicKeys();
    }

    protected Gettable getOriginalValues()
    {
      return __originalValues;
    }

    @Override
    public Collection<?> getValues(Object key)
    {
      return GettableUtils.wrapValue(get(key));
    }

    @Override
    public Collection<?> getPaddedValues(Object key)
    {
      Collection<?> values = getValues(key);
      return values == null ? ImmutableList.of() : values;
    }

    @Override
    public Boolean getBoolean(Object key)
    {
      return GettableUtils.toBoolean(get(key));
    }

    @Override
    public String getString(Object key)
    {
      return StringUtils.toString(get(key));
    }

    @Override
    public File getFile(Object key)
    {
      return GettableUtils.toFile(get(key));
    }
  }

  private class SettableView
    extends GettableView
    implements Settable
  {
    private final String __settableKeysKey;

    private final String __splitRegex;

    public SettableView(String namespace,
                        Gettable originalValues,
                        String settableKeysKey,
                        String splitRegex)
    {
      super(namespace,
            originalValues);
      __settableKeysKey = settableKeysKey;
      __splitRegex = splitRegex;
    }

    @Override
    public Object set(String key,
                      Object value)
    {
      try {
        return SettableValue.this.set(getName(),
                                      StringUtils.toString(key),
                                      StringUtils.toString(value),
                                      getOriginalValues());
      } catch (MirrorException mEx) {
        throw new LogicException("Error setting value", mEx);
      }
    }

    @Override
    public Object set(TypedKey<?> key,
                      Object value)
    {
      return set(key.getKeyName(), value);
    }

    /**
     * @return EmptyIterator
     */
    @Override
    public Collection<String> getSettableKeys()
    {
      return __settableKeysKey == null
          ? getOriginalValues().getPublicKeys()
          : GettableUtils.getKeys(getOriginalValues(), __settableKeysKey, __splitRegex);
    }

    /**
     * @return null because SettableView doesn't support ValueType classification
     */
    @Override
    public ValueType getValueType(String key)
    {
      return null;
    }
  }
}
