package org.cord.mirror;

/**
 * Interface that marks a {@link Field} as responsible for making some kind of transformation of
 * value representation between {@link TransientRecord}, {@link PersistentRecord} and
 * {@link WritableRecord} states. While there is no reason that other Objects beyond Fields couldn't
 * be RecordStateDataFilter, the registration on {@link Table}s is restricted to Field
 * implementations as it exposes the internal data representations. The intention is for when the
 * Object representation used by a Field has a mutable and an immutable state and we want to use the
 * mutable state for the {@link TransientRecord} and {@link WritableRecord} but only expose an
 * immutable viewpoint onto the data for {@link PersistentRecord}s.
 * 
 * @author alex
 */
public interface RecordStateDataFilter
{
  public void transientToPersistent(TransientRecord transientRecord,
                                    Object[] persistentObjectValues,
                                    int[] persistentIntValues);

  public void persistentToWritable(PersistentRecord persistentRecord,
                                   Object[] writableObjectValues,
                                   int[] writableIntValues);

  public void writableToPersistent(WritableRecord writableRecord,
                                   Object[] persistentObjectValues,
                                   int[] persistentIntValues);
}
