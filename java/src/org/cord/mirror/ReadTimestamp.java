package org.cord.mirror;

import java.util.Comparator;

public class ReadTimestamp
{
  private final int __timestampIndex;

  public ReadTimestamp(Table table) throws MirrorTableLockedException
  {
    __timestampIndex = table.getNextIntValueIndex(this);
    System.err.println("ReadTimestamp(" + table + ") -->" + __timestampIndex);
  }

  public int getEncodedTimestamp(PersistentRecord record)
  {
    return record.getId() == TransientRecord.ID_MISSING
        ? Integer.MAX_VALUE
        : record.getIntValue(__timestampIndex);
  }

  public void setTimestamp(PersistentRecord record,
                           long timestamp)
  {
    setEncodedTimestamp(record, encodeTimestamp(timestamp));
  }

  public void setEncodedTimestamp(PersistentRecord record,
                                  int encodedTimestamp)
  {
    if (record.getId() == TransientRecord.ID_MISSING) {
      System.err.println(this + ": " + record + " --> " + encodedTimestamp);
      new RuntimeException().printStackTrace();
    }
    record.getIntValues()[__timestampIndex] = encodedTimestamp;
  }

  public boolean isOlderThan(PersistentRecord record,
                             long timestamp)
  {
    return isOlderThanEncoded(record, encodeTimestamp(timestamp));
  }

  public boolean isOlderThanEncoded(PersistentRecord record,
                                    int encodedTimestamp)
  {
    return record.getId() == TransientRecord.ID_MISSING
        ? false
        : getEncodedTimestamp(record) < encodedTimestamp;
  }

  public int encodeTimestamp(long timestamp)
  {
    return (int) ((timestamp >> 10) & Integer.MAX_VALUE);
  }

  public Comparator<PersistentRecord> ascendingReadComparator()
  {
    return new Comparator<PersistentRecord>() {
      @Override
      public int compare(PersistentRecord record1,
                         PersistentRecord record2)
      {
        int c = Integer.compare(getEncodedTimestamp(record1), getEncodedTimestamp(record2));
        return c == 0 ? Integer.compare(record1.getId(), record2.getId()) : c;
      }
    };
  }

  public Comparator<PersistentRecord> descendingReadComparator()
  {
    return new Comparator<PersistentRecord>() {
      @Override
      public int compare(PersistentRecord record1,
                         PersistentRecord record2)
      {
        int c = Integer.compare(getEncodedTimestamp(record2), getEncodedTimestamp(record1));
        return c == 0 ? Integer.compare(record2.getId(), record1.getId()) : c;
      }
    };
  }

}
