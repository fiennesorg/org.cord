package org.cord.mirror;

import java.util.Comparator;
import java.util.Date;

/**
 * Implementation of Comparator that changes the default ordering of TransientRecords to be sorted
 * alphabetically according to the value of a given field on the records.
 */
public class TransientRecordDateComparator
  implements Comparator<TransientRecord>
{
  private final String __fieldName;

  private final boolean __permitsMultipleTables;

  public TransientRecordDateComparator(String fieldName,
                                       boolean permitsMultipleTables)
  {
    __fieldName = fieldName;
    __permitsMultipleTables = permitsMultipleTables;
  }

  @Override
  public int compare(TransientRecord tr1,
                     TransientRecord tr2)
  {
    if (!__permitsMultipleTables) {
      Table t1 = tr1.getTable();
      Table t2 = tr2.getTable();
      if (!t1.equals(t2)) {
        throw new ClassCastException("Cannot compare two records from different Tables");
      }
    }
    Date d1 = (Date) tr1.get(__fieldName);
    Date d2 = (Date) tr2.get(__fieldName);
    return d1.compareTo(d2);
  }
}