package org.cord.mirror;

import java.util.concurrent.ExecutionException;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;

/**
 * Wrapper around a guava {@link LoadingCache} that is automatically invalidated when messages from
 * given TableListeners are received thereby forcing recalculation. The cache will remain as a
 * TableListener regardless of how many updates are received. If you are going to be updating a lot
 * more records than you are going to be querying the cache then you should maybe consider a
 * {@link LoadingCache_IntermittentTableListener}.
 * 
 * @author alex
 * @param <K>
 *          The class of keys that are used to resolve values from the TableListeningCache.
 * @param <V>
 *          The class of values that are retrieved from the TableListeningCache.
 */
public abstract class LoadingCache_AlwaysTableListener<K, V>
  implements TableListener, Lockable
{
  private final LoadingCache<K, V> __cache;

  /**
   * @param db
   *          The Db that this cache will be utilised with. The constructor will automatically
   *          invoke {@link Db#addLockable(Lockable)} to register this cache with the db.
   */
  public LoadingCache_AlwaysTableListener(Db db)
  {
    __cache = CacheBuilder.newBuilder().softValues().build(new CacheLoader<K, V>() {
      @Override
      public V load(K key)
          throws Exception
      {
        V v = calculate(key);
        return v;
      }

      @Override
      public String toString()
      {
        return LoadingCache_AlwaysTableListener.this + "$CacheLoader";
      }
    });
    db.addLockable(this);
  }

  protected abstract V calculate(K key)
      throws Exception;

  protected abstract ImmutableList<Table> getTableListenerTargets();

  @Override
  public void lock()
  {
    for (Table table : getTableListenerTargets()) {
      table.addTableListener(this);
    }
  }

  /**
   * @return true because we stay registered.
   */
  @Override
  public boolean tableChanged(Table table,
                              int recordId,
                              PersistentRecord persistentRecord,
                              int type)
  {
    // DebugConstants.method(this, "tableChanged", table, recordId, persistentRecord, type);
    __cache.invalidateAll();
    return true;
  }

  public final V get(K key)
      throws ExecutionException
  {
    return __cache.get(key);
  }

}
