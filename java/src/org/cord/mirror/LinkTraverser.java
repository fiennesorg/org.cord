package org.cord.mirror;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkTraverser
  implements Iterator<PersistentRecord>
{
  private PersistentRecord _currentRecord;

  private final String __operationName;

  private final Viewpoint __viewpoint;

  public LinkTraverser(PersistentRecord startingRecord,
                       String operationName,
                       Viewpoint transaction)
  {
    __operationName = operationName;
    _currentRecord = startingRecord;
    __viewpoint = transaction;
    findNextRecord();
  }

  private void findNextRecord()
  {
    try {
      _currentRecord = (PersistentRecord) _currentRecord.get(__operationName, __viewpoint);
    } catch (Exception ex) {
      // 2DO # LinkTraverser currently continues until it hits any error...
      // 2DO # ...and then stops. might be nice to have timeouts permitted.
      _currentRecord = null;
    }
  }

  @Override
  public final boolean hasNext()
  {
    return _currentRecord != null;
  }

  @Override
  public final PersistentRecord next()
  {
    if (hasNext()) {
      PersistentRecord result = _currentRecord;
      findNextRecord();
      return result;
    }
    throw new NoSuchElementException();
  }

  @Override
  public void remove()
  {
    throw new UnsupportedOperationException("LinkTraverser.remove()");
  }
}
