package org.cord.mirror;

public class MirrorAuthorisationException
  extends MirrorRuntimeException
{
  /**
   * 
   */
  private static final long serialVersionUID = -2300680055884673079L;

  /**
   * The amount of time that the invoking thread should sleep for during initialisation of a new
   * MirrorAuthorisationException. This has the effect of forcing brute-force password attacks to
   * become unfeasible due to the time overheads.
   */
  public final static long INITIALISATION_DELAY = 500;

  public MirrorAuthorisationException(String details,
                                      Exception nestedException)
  {
    super(details,
          nestedException);
    try {
      Thread.sleep(INITIALISATION_DELAY);
    } catch (InterruptedException ie) {
    }
  }
}
