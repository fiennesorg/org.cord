package org.cord.mirror;

import org.cord.util.ExceptionUtil;
import org.cord.util.TypedKey;

public class RecordOperationKey<T>
  extends TypedKey<T>
{
  public static <T> RecordOperationKey<T> create(String keyName,
                                                 Class<T> valueClass)
  {
    return new RecordOperationKey<T>(keyName, valueClass);
  }

  public RecordOperationKey(String keyName,
                            Class<T> valueClass)
  {
    super(keyName,
          valueClass);
  }

  public T call(TransientRecord record,
                Viewpoint viewpoint)
  {
    try {
      return record.getRecordOperation(this).getOperation(record, viewpoint);
    } catch (NullPointerException e) {
      throw ExceptionUtil.initCause(new NullPointerException(String.format("Unable to resolve RecordOperation %s on %s",
                                                                           this,
                                                                           record)),
                                    e);
    }
  }

  /**
   * Create a copy of this RecordOperationKey with the same name and value class.
   */
  @Override
  public RecordOperationKey<T> copy()
  {
    return create(getKeyName(), getValueClass());
  }
}
