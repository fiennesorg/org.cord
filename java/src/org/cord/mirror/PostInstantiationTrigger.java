package org.cord.mirror;

import org.cord.node.FeedbackErrorException;
import org.cord.util.Gettable;

/**
 * Extension of InstantiationTrigger that describes objects that wish to perform some operation on a
 * record immediately after it has been made persistent.
 */
public interface PostInstantiationTrigger
  extends InstantiationTrigger
{
  /**
   * Perform whatever action is required on the transient record before it is committed. All
   * InstantiationTriggers will have this method called with respect to a specific record before the
   * database commit is performed. If any of the triggers fail (by throwing an exception), then all
   * previous triggers will have their rollbackTrigger method called.
   * 
   * @param record
   *          The record that is to be instantiated
   * @param params
   *          Any configuration variables hat have been passed in from the original invoication of
   *          makePersistent that lead to this method invocation.
   * @throws MirrorException
   *           If the trigger failed for any reason. Please note that the trigger should <i>not</i>
   *           throw an exception unless it has cleaned up after itself as much as possible as it
   *           will not be touched again in this cycle.
   * @see #rollbackPost(PersistentRecord, Gettable)
   */
  public void triggerPost(PersistentRecord record,
                          Gettable params)
      throws MirrorException, FeedbackErrorException;

  /**
   * Inform this trigger that a later trigger has failed to complete and therefore the operation has
   * to be aborted. Any effects that were created by the previous call to triggerInstantio should be
   * reversed as much as possible. If this is not possible, then an exception should be thrown
   * containing as much relevant information as possible which will be collated into the
   * MirrorInstantiationException that will be being generated from the original error.
   * 
   * @param record
   *          The record who's instantiation is being aborted.
   * @param params
   *          Any configuration variables hat have been passed in from the original invoication of
   *          makePersistent that lead to this method invocation.
   * @throws MirrorException
   *           If the trigger rollback cannot be achieved for whatever reason. Please note that all
   *           earlier triggers will still get asked to rollback even if the exception is thrown.
   */
  public void rollbackPost(PersistentRecord record,
                           Gettable params)
      throws MirrorException;

  /**
   * Tell the trigger that the instantiation has been completed and any rollback information
   * relating to this record can be released.
   * 
   * @param record
   *          The record that has been committed.
   * @param params
   *          Any configuration variables hat have been passed in from the original invoication of
   *          makePersistent that lead to this method invocation.
   */
  public void releasePost(PersistentRecord record,
                          Gettable params);
}
