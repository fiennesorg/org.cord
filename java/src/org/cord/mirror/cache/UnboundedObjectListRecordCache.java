package org.cord.mirror.cache;

import org.cord.mirror.Table;
import org.cord.util.Gettable;

/**
 * RecordCache that stores records in an ObjectList with no expiry implementation. ie any record
 * that gets loaded will stay in memory until some process external to the cache invalidates it.
 * 
 * @author alex
 */
public class UnboundedObjectListRecordCache
  extends ObjectListRecordCache
{
  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public RecordCache createRecordCache(Gettable config,
                                         Table table)
    {
      return new UnboundedObjectListRecordCache(table);
    }
  }

  public UnboundedObjectListRecordCache(Table table)
  {
    super(table);
  }

  @Override
  public int expire()
  {
    return 0;
  }
}
