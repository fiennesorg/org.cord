package org.cord.mirror.cache;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

public abstract class ObjectListRecordCache
  implements RecordCache
{
  private final Table __table;
  private final ObjectList<PersistentRecord> __objectList;
  private int _size = 0;

  public ObjectListRecordCache(Table table)
  {
    __table = table;
    __objectList = new ObjectArrayList<>(100);
    __objectList.size(100);
  }

  protected final ObjectList<PersistentRecord> getObjectList()
  {
    return __objectList;
  }

  private void size(int id)
  {
    if (__objectList.size() <= id) {
      System.err.println(__table + ": " + __objectList.size() + " <= " + id + " --> "
                         + (id * 3 / 2));
      __objectList.size(id * 3 / 2);
    }
  }

  @Override
  public PersistentRecord cacheRecord(PersistentRecord record)
  {
    int id = record.getId();
    try {
      PersistentRecord existingRecord = __objectList.get(id);
      if (existingRecord != null) {
        return existingRecord;
      }
      __objectList.set(id, record);
    } catch (IndexOutOfBoundsException e) {
      size(id);
      __objectList.set(id, record);
    }
    _size++;
    return record;
  }

  @Override
  public PersistentRecord getRecordIfPresent(int id)
  {
    if (__objectList.size() <= id) {
      return null;
    }
    return __objectList.get(id);
  }

  public PersistentRecord missingRecordToNull(PersistentRecord record)
  {
    return record.getId() == TransientRecord.ID_MISSING ? null : record;
  }

  @Override
  public PersistentRecord getRecord(int id)
      throws Exception
  {
    PersistentRecord record;
    try {
      record = __objectList.get(id);
    } catch (IndexOutOfBoundsException e) {
      size(id);
      record = __objectList.get(id);
    }
    if (record == null) {
      record = __table.loadRecord(id);
      __objectList.set(id, record);
      if (record.getId() == TransientRecord.ID_MISSING) {
        return null;
      }
      _size++;
    } else {
      record = record.getId() == TransientRecord.ID_MISSING ? null : record;
    }
    return record;
  }

  @Override
  public boolean hasCachedRecord(int id)
  {
    if (__objectList.size() <= id) {
      return false;
    }
    return __objectList.get(id) != null;
  }

  @Override
  public void invalidateAll()
  {
    __objectList.clear();
    _size = 0;
  }

  @Override
  public void invalidate(int id)
  {
    PersistentRecord record = __objectList.set(id, null);
    if (record != null) {
      _size--;
    }
  }

  @Override
  public Table getTable()
  {
    return __table;
  }

  @Override
  public int size()
  {
    return _size;
  }

  protected void populate(ToStringHelper toStringHelper)
  {
    toStringHelper.add("table", __table);
  }

  public String toString()
  {
    ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
    populate(toStringHelper);
    return toStringHelper.toString();
  }

}
