package org.cord.mirror.cache;

import java.sql.SQLException;
import java.util.Map;

import org.cord.mirror.DbBooter;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.StringUtils;

import com.google.common.base.MoreObjects;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class LoadingCacheRecordCache
  implements RecordCache
{
  public static class Factory
    extends RecordCacheFactory
  {

    @Override
    public RecordCache createRecordCache(Gettable mirrorProperties,
                                         final Table table)
    {
      String spec = StringUtils.trim(mirrorProperties.getString(DbBooter.CONF_RECORDCACHE + "."
                                                                + table.getName()));
      if (spec == null) {
        spec = MoreObjects.firstNonNull(mirrorProperties.getString(DbBooter.CONF_RECORDCACHE),
                                        DbBooter.CONF_RECORDCACHE_DEFAULT);
      }
      CacheBuilderSpec cacheBuilderSpec = CacheBuilderSpec.parse(spec);
      LoadingCache<Integer, PersistentRecord> loadingCache;
      CacheLoader<Integer, PersistentRecord> recordLoader =
          new CacheLoader<Integer, PersistentRecord>() {
            @Override
            public PersistentRecord load(Integer key)
                throws SQLException
            {
              return table.loadRecord(key.intValue());
            }
          };
      if (DebugConstants.DEBUG_CACHE_EVICTION) {
        loadingCache =
            CacheBuilder.from(cacheBuilderSpec)
                        .removalListener(new RemovalListener<Integer, PersistentRecord>() {
                          @Override
                          public void onRemoval(RemovalNotification<Integer, PersistentRecord> notification)
                          {
                            System.err.println(table.getName() + ".Record: "
                                               + notification.getValue().getId()
                                               + " was expired because " + notification.getCause());
                          }
                        })
                        .build(recordLoader);
      } else {
        loadingCache = CacheBuilder.from(cacheBuilderSpec).build(recordLoader);
      }
      return new LoadingCacheRecordCache(table, loadingCache);
    }
  }

  private final Table __table;
  private final LoadingCache<Integer, PersistentRecord> __loadingCache;
  private final Map<Integer, PersistentRecord> __loadingCacheAsMap;

  public LoadingCacheRecordCache(Table table,
                                 LoadingCache<Integer, PersistentRecord> loadingCache)
  {
    __table = table;
    __loadingCache = loadingCache;
    __loadingCacheAsMap = __loadingCache.asMap();
  }

  @Override
  public PersistentRecord cacheRecord(PersistentRecord record)
  {
    __loadingCache.put(Integer.valueOf(record.getId()), record);
    return record;
  }

  @Override
  public PersistentRecord getRecordIfPresent(int id)
  {
    return __loadingCache.getIfPresent(Integer.valueOf(id));
  }

  @Override
  public int expire()
  {
    long size = __loadingCache.size();
    __loadingCache.cleanUp();
    return (int) (__loadingCache.size() - size);
  }

  @Override
  public PersistentRecord getRecord(int id)
      throws Exception
  {
    return __loadingCache.get(Integer.valueOf(id));
  }

  @Override
  public void invalidateAll()
  {
    __loadingCache.invalidateAll();
  }

  @Override
  public void invalidate(int id)
  {
    __loadingCache.invalidate(Integer.valueOf(id));
  }

  @Override
  public boolean hasCachedRecord(int id)
  {
    return __loadingCacheAsMap.containsKey(Integer.valueOf(id));
  }

  @Override
  public Table getTable()
  {
    return __table;
  }

  @Override
  public int size()
  {
    return (int) __loadingCache.size();
  }

}
