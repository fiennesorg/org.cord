package org.cord.mirror.cache;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReferenceArray;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;

public class PagedArrayPersistentRecordCache
{
  public static final int PAGE_BITWIDTH = 8;
  public static final int PAGE_SIZE = 1 << PAGE_BITWIDTH;
  public static final int PAGE_BITMASK = PAGE_SIZE - 1;

  private final Table __table;
  private volatile AtomicReferenceArray<Page> _pages;
  private final Object __resizeSync = new Object();

  public PagedArrayPersistentRecordCache(Table table,
                                         int capacity)
  {
    __table = checkNotNull(table);
    _pages = new AtomicReferenceArray<Page>(capacity >> PAGE_BITWIDTH);
  }

  public PersistentRecord getRecord(int id)
      throws SQLException
  {
    return getPage(id).getPersistentRecord(id);
  }

  private Page getPage(int id)
  {
    int i = id >> PAGE_BITWIDTH;
    try {
      while (true) {
        Page page = _pages.get(i);
        if (page != null) {
          return page;
        }
        synchronized (__resizeSync) {
          page = new Page();
          if (_pages.compareAndSet(i, null, page)) {
            return page;
          }
        }
      }
    } catch (IndexOutOfBoundsException e) {
      if (id < 0) {
        throw e;
      }
      synchronized (__resizeSync) {
        if (_pages.length() < i) {
          return getPage(id);
        }
        AtomicReferenceArray<Page> resizedPages = new AtomicReferenceArray<Page>(i);
        final int l = _pages.length();
        for (int j = 0; j < l; j++) {
          resizedPages.set(j, _pages.get(j));
        }
        _pages = resizedPages;
        return getPage(id);
      }
    }
  }

  public class Page
  {
    private final AtomicReferenceArray<PersistentRecord> __persistentRecords =
        new AtomicReferenceArray<>(PAGE_SIZE);

    protected PersistentRecord getPersistentRecord(int id)
        throws SQLException
    {
      int i = id & PAGE_BITMASK;
      while (true) {
        PersistentRecord cachedRecord = __persistentRecords.get(i);
        if (cachedRecord != null) {
          return cachedRecord;
        }
        PersistentRecord loadedRecord = __table.loadRecord(id);
        if (__persistentRecords.compareAndSet(i, null, loadedRecord)) {
          return loadedRecord;
        }
      }
    }
  }

  public static void main(String[] args)
  {
    long maxInt = (long) Integer.MAX_VALUE - (long) Integer.MIN_VALUE;
    System.out.println(new Date(0));
    System.out.println(new Date(maxInt));
    System.out.println(new Date(maxInt * 1000l));
  }
}
