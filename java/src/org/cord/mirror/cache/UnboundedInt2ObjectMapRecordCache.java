package org.cord.mirror.cache;

import org.cord.mirror.Table;
import org.cord.util.Gettable;

public class UnboundedInt2ObjectMapRecordCache
  extends Int2ObjectMapRecordCache
{
  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public UnboundedInt2ObjectMapRecordCache createRecordCache(Gettable config,
                                                               Table table)
    {
      return new UnboundedInt2ObjectMapRecordCache(table);
    }
  }

  public UnboundedInt2ObjectMapRecordCache(Table table)
  {
    super(table);
  }

  @Override
  public int expire()
  {
    return 0;
  }
}
