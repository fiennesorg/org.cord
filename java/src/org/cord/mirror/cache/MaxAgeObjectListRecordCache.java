package org.cord.mirror.cache;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.ReadTimestamp;
import org.cord.mirror.Table;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

import it.unimi.dsi.fastutil.objects.ObjectList;

public class MaxAgeObjectListRecordCache
  extends ReadTimestampObjectListRecordCache
{
  public static final String CONF_MAXAGEMS = "maxAgeMs";
  public static final Object CONF_MAXAGEMS_DEFAULT = null;

  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public RecordCache createRecordCache(Gettable config,
                                         Table table)
    {
      return new MaxAgeObjectListRecordCache(table,
                                             NumberUtil.toLong(resolveKey(config,
                                                                          table,
                                                                          CONF_MAXAGEMS,
                                                                          CONF_MAXAGEMS_DEFAULT)));
    }
  }

  private final long __maximumAge;

  public MaxAgeObjectListRecordCache(Table table,
                                     long maximumAge)
  {
    super(table);
    __maximumAge = maximumAge;
  }

  public int invalidateOlderThanTimestamp(long timestamp)
  {
    ObjectList<PersistentRecord> records = getObjectList();
    ReadTimestamp readTimestamp = getReadTimestamp();
    try {
      int count = 0;
      int maxSize = records.size();
      for (int i = 0; i < maxSize; i++) {
        PersistentRecord record = records.get(i);
        if (record != null && readTimestamp.isOlderThan(record, timestamp)) {
          invalidate(record.getId());
          count++;
        }
      }
      return count;
    } catch (RuntimeException e) {
      System.err.println(this + ".invalidateOlderThanTimestamp(" + timestamp + ") --> " + e + " on "
                         + records);
      throw e;
    }
  }

  @Override
  public int expire()
  {
    return invalidateOlderThanTimestamp(System.currentTimeMillis() - __maximumAge);
  }

}
