package org.cord.mirror.cache;

import org.cord.mirror.DbBooter;
import org.cord.mirror.Table;
import org.cord.util.Gettable;

public abstract class RecordCacheFactory
{
  public String getTableKey(Table table,
                            String key)
  {
    return DbBooter.CONF_RECORDCACHE + "." + table.getName() + "." + key;
  }

  public String getGlobalKey(String key)
  {
    return DbBooter.CONF_RECORDCACHE + "." + key;
  }

  public Object resolveKey(Gettable gettable,
                           Table table,
                           String key,
                           Object defaultValue)
  {
    Object value = gettable.get(getTableKey(table, key));
    if (value != null) {
      return value;
    }
    value = gettable.get(getGlobalKey(key));
    if (value != null) {
      return value;
    }
    return defaultValue;
  }

  public abstract RecordCache createRecordCache(Gettable config,
                                                Table table);
}
