package org.cord.mirror.cache;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public abstract class Int2ObjectMapRecordCache
  implements RecordCache
{
  private final ReadWriteLock __readWriteLock = new ReentrantReadWriteLock();
  private final Table __table;
  private final Int2ObjectMap<PersistentRecord> __map;

  public Int2ObjectMapRecordCache(Table table)
  {
    __table = table;
    __map = new Int2ObjectOpenHashMap<PersistentRecord>();
  }

  protected Int2ObjectMap<PersistentRecord> getMap()
  {
    return __map;
  }

  protected ReadWriteLock getReadWriteLock()
  {
    return __readWriteLock;
  }

  @Override
  public PersistentRecord cacheRecord(PersistentRecord record)
  {
    Lock writeLock = __readWriteLock.writeLock();
    writeLock.lock();
    try {
      PersistentRecord existingRecord = __map.get(record.getId());
      if (existingRecord != null) {
        existingRecord.updateReadTimestamp();
        return existingRecord;
      }
      __map.put(record.getId(), record);
    } finally {
      writeLock.unlock();
    }
    record.updateReadTimestamp();
    return record;
  }

  @Override
  public PersistentRecord getRecordIfPresent(int id)
  {
    PersistentRecord record;
    __readWriteLock.readLock().lock();
    try {
      record = __map.get(id);
    } finally {
      __readWriteLock.readLock().unlock();
    }
    record.updateReadTimestamp();
    return record;
  }

  @Override
  public PersistentRecord getRecord(int id)
      throws Exception
  {
    PersistentRecord record;
    __readWriteLock.readLock().lock();
    try {
      record = __map.get(id);
    } finally {
      __readWriteLock.readLock().unlock();
    }
    if (record != null) {
      record.updateReadTimestamp();
      return record;
    }
    return cacheRecord(getTable().loadRecord(id));
  }

  @Override
  public boolean hasCachedRecord(int id)
  {
    __readWriteLock.readLock().lock();
    try {
      return __map.containsKey(id);
    } finally {
      __readWriteLock.readLock().unlock();
    }
  }

  @Override
  public void invalidateAll()
  {
    __readWriteLock.writeLock().lock();
    try {
      __map.clear();
    } finally {
      __readWriteLock.writeLock().unlock();
    }
  }

  @Override
  public void invalidate(int id)
  {
    __readWriteLock.writeLock().lock();
    try {
      __map.remove(id);
    } finally {
      __readWriteLock.writeLock().unlock();
    }
  }

  @Override
  public Table getTable()
  {
    return __table;
  }

  @Override
  public int size()
  {
    getReadWriteLock().readLock().lock();
    try {
      return __map.size();
    } finally {
      getReadWriteLock().readLock().unlock();
    }
  }

}
