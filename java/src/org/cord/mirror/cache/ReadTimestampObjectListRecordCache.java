package org.cord.mirror.cache;

import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.ReadTimestamp;
import org.cord.mirror.Table;

public abstract class ReadTimestampObjectListRecordCache
  extends ObjectListRecordCache
{
  private final ReadTimestamp __readTimestamp;

  public ReadTimestampObjectListRecordCache(Table table)
  {
    super(table);
    try {
      __readTimestamp = new ReadTimestamp(table);
    } catch (MirrorTableLockedException e) {
      throw new MirrorLogicException(e);
    }
  }

  protected final ReadTimestamp getReadTimestamp()
  {
    return __readTimestamp;
  }

  public PersistentRecord updateReadTimestamp(PersistentRecord record)
  {
    if (record != null) {
      __readTimestamp.setTimestamp(record, System.currentTimeMillis());
    }
    return record;
  }

  @Override
  public PersistentRecord cacheRecord(PersistentRecord record)
  {
    record = super.cacheRecord(record);
    updateReadTimestamp(record);
    return record;
  }

  @Override
  public PersistentRecord getRecordIfPresent(int id)
  {
    return updateReadTimestamp(super.getRecordIfPresent(id));
  }

  @Override
  public PersistentRecord getRecord(int id)
      throws Exception
  {
    return updateReadTimestamp(super.getRecord(id));
  }

}
