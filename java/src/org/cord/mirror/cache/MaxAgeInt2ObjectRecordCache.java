package org.cord.mirror.cache;

import java.util.Iterator;
import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;

public class MaxAgeInt2ObjectRecordCache
  extends Int2ObjectMapRecordCache
{
  public static final String CONF_MAXAGEMS = "maxAgeMs";
  public static final Object CONF_MAXAGEMS_DEFAULT = null;

  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public RecordCache createRecordCache(Gettable config,
                                         Table table)
    {
      return new MaxAgeInt2ObjectRecordCache(table,
                                             NumberUtil.toLong(resolveKey(config,
                                                                          table,
                                                                          CONF_MAXAGEMS,
                                                                          CONF_MAXAGEMS_DEFAULT)));
    }
  }

  private final long __maxAgeMs;

  public MaxAgeInt2ObjectRecordCache(Table table,
                                     long maxAgeMs)
  {
    super(table);
    __maxAgeMs = maxAgeMs;
  }

  @Override
  public int expire()
  {
    List<PersistentRecord> expiredRecords = null;
    final long now = System.currentTimeMillis();
    Int2ObjectMap<PersistentRecord> map = getMap();
    getReadWriteLock().readLock().lock();
    try {
      Iterator<PersistentRecord> records = map.values().iterator();
      while (records.hasNext()) {
        PersistentRecord record = records.next();
        if (now - record.getReadTimestamp() > __maxAgeMs) {
          if (expiredRecords == null) {
            expiredRecords = Lists.newArrayList();
          }
          expiredRecords.add(record);
        }
      }
    } finally {
      getReadWriteLock().readLock().unlock();
    }
    if (expiredRecords != null) {
      getReadWriteLock().writeLock().lock();
      int c = 0;
      try {
        final int s = expiredRecords.size();
        for (int i = 0; i < s; i++) {
          PersistentRecord record = expiredRecords.get(i);
          if (now - record.getReadTimestamp() > __maxAgeMs) {
            map.remove(record.getId());
            c++;
          }
        }
        return c;
      } finally {
        getReadWriteLock().writeLock().unlock();
      }
    }
    return 0;
  }

}
