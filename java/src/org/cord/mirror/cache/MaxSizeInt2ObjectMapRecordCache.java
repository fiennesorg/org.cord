package org.cord.mirror.cache;

import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.longs.LongArrays;

public class MaxSizeInt2ObjectMapRecordCache
  extends Int2ObjectMapRecordCache
{
  public static final String CONF_MAXSIZE = "maxSize";
  public static final Object CONF_MAXSIZE_DEFAULT = null;

  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public RecordCache createRecordCache(Gettable config,
                                         Table table)
    {
      return new MaxSizeInt2ObjectMapRecordCache(table,
                                                 NumberUtil.toInt(resolveKey(config,
                                                                             table,
                                                                             CONF_MAXSIZE,
                                                                             CONF_MAXSIZE_DEFAULT)));
    }
  }

  private final int __maxSize;

  public MaxSizeInt2ObjectMapRecordCache(Table table,
                                         int maxSize)
  {
    super(table);
    __maxSize = maxSize;
  }

  public int getMaxSize()
  {
    return __maxSize;
  }

  @Override
  public int expire()
  {
    // return 0;
    final Int2ObjectMap<PersistentRecord> cacheMap = getMap();
    // Long2ObjectSortedMap<PersistentRecord> age_record;
    final long[] encodeds;
    final int excessSize;
    getReadWriteLock().readLock().lock();
    try {
      excessSize = cacheMap.size() - getMaxSize();
      if (excessSize <= 0) {
        return 0;
      }
      encodeds = new long[excessSize + 1];
      Iterator<PersistentRecord> records = cacheMap.values().iterator();
      final long nowTimestamp = System.currentTimeMillis();
      populateInitialEncodeds(encodeds, excessSize, records, nowTimestamp);
      populateRemainingEncodeds(cacheMap, encodeds, excessSize, records, nowTimestamp);
    } finally {
      getReadWriteLock().readLock().unlock();
    }
    getReadWriteLock().writeLock().lock();
    try {
      for (int i = 0; i < excessSize; i++) {
        cacheMap.remove(decodeId(encodeds[i]));
      }
    } finally {
      getReadWriteLock().writeLock().unlock();
    }
    return excessSize;
  }

  // private int populateZero(long[] encodeds,
  // Iterator<PersistentRecord> records,
  // long nowTimestamp)
  // {
  // while (records.hasNext()) {
  // PersistentRecord record = records.next();
  // long readTimestamp = record.getReadTimestamp();
  // if (readTimestamp <= nowTimestamp) {
  // encodeds[0] = encode(readTimestamp, nowTimestamp, record.getId());
  // return 1;
  // }
  // }
  // return 0;
  // }
  //
  // private int populateOneToSize(long[] encodeds,
  // Iterator<PersistentRecord> records,
  // long nowTimestamp)
  // {
  // int size = 1;
  // while (size < encodeds.length) {
  //
  // }
  // }
  //
  // private boolean populateTail(long[] encodeds,
  // Iterator<PersistentRecord> records,
  // long nowTimestamp,
  // int size)
  // {
  // while (records.hasNext()) {
  // PersistentRecord record = records.next();
  // long readTimestamp = record.getReadTimestamp();
  // if (readTimestamp <= nowTimestamp) {
  // insertValueIntoSorted(encodeds, size, encode(readTimestamp, nowTimestamp, record.getId()));
  // return true;
  // }
  // }
  // return false;
  // }
  //
  // private void populateEncodeds(long[] encodeds,
  // Iterator<PersistentRecord> records,
  // long nowTimestamp)
  // {
  // while (records.hasNext()) {
  // PersistentRecord record = records.next();
  // long size = 0;
  // long readTimestamp = record.getReadTimestamp();
  // if (readTimestamp <= nowTimestamp) {
  // long relativeTimestamp = nowTimestamp - readTimestamp;
  // long encoded = relativeTimestamp << Integer.SIZE | record.getId();
  // }
  // }
  // }

  private void populateInitialEncodeds(long[] encodeds,
                                       int excessSize,
                                       Iterator<PersistentRecord> records,
                                       long nowTimestamp)
  {
    for (int i = 0; i < excessSize; i++) {
      insertValueIntoSorted(encodeds, i, encode(records.next(), nowTimestamp));
    }
  }

  private void populateRemainingEncodeds(Int2ObjectMap<PersistentRecord> cacheMap,
                                         long[] encodeds,
                                         int excessSize,
                                         Iterator<PersistentRecord> records,
                                         long nowTimestamp)
  {
    int hits = 0;
    int misses = 0;
    long mostRecentRelative = encodeds[excessSize - 1] >> Integer.SIZE;
    while (records.hasNext()) {
      PersistentRecord record = records.next();
      long recordRelative = record.getReadTimestamp() - nowTimestamp;
      if (recordRelative < mostRecentRelative) {
        insertValueIntoSorted(encodeds,
                              excessSize,
                              recordRelative << Integer.SIZE | record.getId());
        mostRecentRelative = encodeds[excessSize - 1] >> Integer.SIZE;
        hits++;
      } else {
        misses++;
      }
    }
    System.err.println(getTable() + " :: hits: " + hits + ", misses: " + misses);
  }

  private static long encode(PersistentRecord record,
                             long nowTimestamp)
  {
    return encode(record.getReadTimestamp(), nowTimestamp, record.getId());
  }

  private static long encode(long readTimestamp,
                             long nowTimestamp,
                             int id)
  {
    return (readTimestamp - nowTimestamp) << Integer.SIZE | id;
  }

  private static final long INT_MASK = (1l << Integer.SIZE) - 1;

  private static int decodeId(long encoded)
  {
    return (int) (encoded & INT_MASK);
  }

  private static long decodeTimestamp(long encoded)
  {
    return encoded >> Integer.SIZE;
  }

  private void insertValueIntoSorted(long[] sorted,
                                     int size,
                                     long encoded)
  {
    if (size == 0) {
      sorted[0] = encoded;
      return;
    }
    int index = LongArrays.binarySearch(sorted, 0, size, encoded);
    if (index >= 0) {
      DebugConstants.method(this,
                            "insertValueIntoSorted",
                            sorted,
                            Integer.valueOf(size),
                            Long.valueOf(encoded));
      throw new IllegalArgumentException(sorted + " already contains " + encoded + " in the first "
                                         + size + " values");
    }
    index = -index - 1;
    if (index < size) {
      try {
        System.arraycopy(sorted, index, sorted, index + 1, size - index);
      } catch (ArrayIndexOutOfBoundsException e) {
        DebugConstants.variable("sorted.length", Integer.valueOf(sorted.length));
        DebugConstants.variable("index", Integer.valueOf(index));
        DebugConstants.variable("size", Integer.valueOf(size));
        DebugConstants.variable("size-index", Integer.valueOf(size - index));
        throw e;
      }
    }
    sorted[index] = encoded;
  }

  public static void main(String[] args)
  {
    long encoded = ((long) (-1)) << Integer.SIZE | 123;
    System.out.println(encoded);
    System.out.println(encoded & INT_MASK);
    System.out.println(encoded >> Integer.SIZE);
    System.out.println(Integer.MAX_VALUE);
  }
}
