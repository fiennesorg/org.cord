package org.cord.mirror.cache;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;

public interface RecordCache
{
  public PersistentRecord cacheRecord(PersistentRecord record);
  public PersistentRecord getRecordIfPresent(int id);
  public int expire();
  public PersistentRecord getRecord(int id)
      throws Exception;
  public boolean hasCachedRecord(int id);
  public void invalidateAll();
  public void invalidate(int id);
  public Table getTable();
  public int size();
}
