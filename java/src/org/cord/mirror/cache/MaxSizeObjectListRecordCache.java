package org.cord.mirror.cache;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

import com.google.common.base.MoreObjects.ToStringHelper;

import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectList;

public class MaxSizeObjectListRecordCache
  extends ReadTimestampObjectListRecordCache
{
  public static final String CONF_MAXSIZE = "maxSize";
  public static final Object CONF_MAXSIZE_DEFAULT = null;

  public static class Factory
    extends RecordCacheFactory
  {
    @Override
    public RecordCache createRecordCache(Gettable config,
                                         Table table)
    {
      return new MaxSizeObjectListRecordCache(table,
                                              NumberUtil.toInt(resolveKey(config,
                                                                          table,
                                                                          CONF_MAXSIZE,
                                                                          CONF_MAXSIZE_DEFAULT)));
    }
  }

  private final int __maxSize;

  public MaxSizeObjectListRecordCache(Table table,
                                      int maxSize)
  {
    super(table);
    __maxSize = maxSize;
  }

  protected void populate(ToStringHelper toStringHelper)
  {
    super.populate(toStringHelper);
    toStringHelper.add("maxSize", __maxSize).toString();
  }

  public int invalidateMaximumSize(int maxSize)
  {
    final int recordCacheSize = this.size();
    if (recordCacheSize <= maxSize) {
      return 0;
    }
    final ObjectAVLTreeSet<PersistentRecord> oldestRecords =
        new ObjectAVLTreeSet<>(getReadTimestamp().descendingReadComparator());
    final int excessSize = recordCacheSize - maxSize;
    final ObjectList<PersistentRecord> objectList = getObjectList();
    final int objectListSize = objectList.size();
    for (int i = 0; i < objectListSize; i++) {
      PersistentRecord record = objectList.get(i);
      if (record != null && record.getId() != TransientRecord.ID_MISSING) {
        oldestRecords.add(record);
        if (oldestRecords.size() > excessSize) {
          oldestRecords.remove(oldestRecords.first());
        }
      }
    }
    for (PersistentRecord record : oldestRecords) {
      invalidate(record.getId());
    }
    return oldestRecords.size();
  }

  @Override
  public int expire()
  {
    return invalidateMaximumSize(__maxSize);
  }

}
