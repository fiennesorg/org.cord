package org.cord.mirror;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;

import org.cord.mirror.recordsource.AbstractRecordSource;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

import com.google.common.collect.Iterators;

/**
 * PersistentRecord represents a direct mirror of the corresponding database record at any
 * particular time. It is a read-only implementation of record. If you wish to change values (and
 * therefore by definition, change the persistent database), then you have to create a Transaction
 * and then check out WritableRecords from the PersistentRecord. This therefore ensures that the
 * general public who are not concerned with your transaction continue to use the previous record
 * while your update is taking place, and only receive the updated data when the Transaction is
 * committed and therefore complete.
 */
public class PersistentRecord
  extends TransientRecord
  implements Iterable<PersistentRecord>
{
  private volatile Transaction _transaction;

  private volatile WritableRecord _writableRecord;

  private volatile boolean _isDeleted = false;

  private volatile long __readTimestamp = 0;

  public void updateReadTimestamp()
  {
    __readTimestamp = System.currentTimeMillis();
  }

  public long getReadTimestamp()
  {
    return __readTimestamp;
  }

  /**
   * Internal method to create a PersistentRecord. You should not attempt to create
   * PersistentRecords in this way, but rather request the Table to give you an existing database
   * record by id, or make a TransientRecord into a PersistentRecord (thereby creating a database
   * record as well).
   * 
   * @param table
   *          The Table that this record is associated with.
   * @param values
   *          The values map that this record is going to be initialised with (note that it does
   *          <i>not</i> clone the map, but uses it directly).
   * @param transaction
   *          The Transaction that is associated with this record, or null if it is not part of a
   *          Transaction (normally null).
   * @see TransientRecord#makePersistent(Gettable)
   */
  protected PersistentRecord(Table table,
                             Object[] values,
                             int[] intValues,
                             Transaction transaction)
  {
    super(table,
          values,
          intValues,
          0,
          0);
    _transaction = transaction;
    _writableRecord = null;
  }

  /**
   * Convert this Record into a RecordReference that can safely be held onto without interfering
   * with the caching mechanism.
   */
  public RecordReference toRecordReference()
  {
    return RecordReference.getInstance(getTable(), getId());
  }

  @Override
  public Iterator<PersistentRecord> iterator()
  {
    return Iterators.singletonIterator(this);
  }

  @Override
  public String toString()
  {
    return getRecordType() + "(" + getTable() + ", #" + getId()
           + (getTable().hasTitleOperationName()
               ? ", " + get(getTable().getTitleOperationName())
               : "")
           + ")" + (_transaction == null ? "" : "-" + _transaction);
  }

  public final static String PERSISTENT_RECORD = "Persistent";

  @Override
  public String getRecordType()
  {
    return PERSISTENT_RECORD;
  }

  /**
   * Internal method to create a PersistentRecord which then gets it's id set to a given value.. You
   * should not attempt to create PersistentRecords in this way, but rather request the Table to
   * give you an existing database record by id, or make a TransientRecord into a PersistentRecord
   * (thereby creating a database record as well). This will also insert the new PersistentRecord
   * into the Table cache automatically so that it is available to other threads immediately without
   * creating another SQL query.
   * 
   * @param table
   *          The Table that this record is associated with.
   * @param values
   *          The values map that this record is going to be initialised with (note that it does
   *          <i>not</i> clone the map, but uses it directly).
   * @param id
   *          The Integer that represents what the unique id of this PersistentRecord is going to
   *          be, which is then inserted into the values map under a key of FIELD_ID.
   * @param transaction
   *          The Transaction that is associated with this record, or null if it is not part of a
   *          Transaction (normally null).
   * @see TransientRecord#makePersistent(Gettable)
   * @see #FIELD_ID
   * @see Table#cachePersistentRecord(PersistentRecord)
   */
  protected PersistentRecord(Table table,
                             Object[] values,
                             int[] intValues,
                             int id,
                             Transaction transaction,
                             boolean shouldCacheRecord)
  {
    super(table,
          values,
          intValues,
          id,
          id);
    _transaction = transaction;
    _writableRecord = null;
    if (shouldCacheRecord) {
      table.cachePersistentRecord(this);
    }
  }

  /**
   * Wrap this PersistentRecord up in a RecordSource interface. The RecordSource will always have a
   * constant contents of just this record.
   * 
   * @return The appropriate RecordSource
   */
  public final RecordSource asRecordSource()
  {
    final ArrayIdList idList = new ArrayIdList(getTable(), 1);
    idList.add(getId());
    return RecordSources.viewedFrom(new AbstractRecordSource(getTable(), null) {
      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        return idList;
      }

      @Override
      public long getLastChangeTime()
      {
        return 0;
      }

      @Override
      public boolean shouldRebuild(Viewpoint viewpoint)
      {
        return false;
      }
    }, defaultIfNull(null));
  }

  /**
   * @return false, because a PersistentRecord is an immutable mirror of a db record. If you want to
   *         update values, then you need to create a Writable record by using the
   *         Transaction.edit(...) command and then manipulate that.
   * @see Transaction#edit(PersistentRecord,long)
   */
  @Override
  public boolean isWriteable()
  {
    return false;
  }

  /**
   * @param table
   *          The Table from which the created PersistentRecord will be found.
   * @param resultSet
   *          The JDBC ResultSet that contains the values that are to be included in the created
   *          PersistentRecord as the next() record in the ResultSet.
   */
  protected static PersistentRecord getInstance(Table table,
                                                ResultSet resultSet)
      throws SQLException
  {
    final int fieldCount = table.getFieldCount();
    Object[] objectValues = table.createObjectValues();
    int[] intValues = table.createIntValues();
    for (int i = 0; i < fieldCount; i++) {
      try {
        table.getField(i).dbToValue(resultSet, objectValues, intValues);
      } catch (SQLException sqlEx) {
        SQLException wrapper =
            new SQLException("Problem validating " + table.getField(i).getName() + " on " + table
                             + " during PersistentRecord creation");
        wrapper.initCause(sqlEx);
        throw wrapper;
      } catch (RuntimeException e) {
        System.err.println("objectValues: " + Arrays.toString(objectValues));
        System.err.println("intValues: " + Arrays.toString(intValues));
      }
    }
    return new PersistentRecord(table, objectValues, intValues, null);
  }

  /**
   * Public authorised method to get the transaction that currently has the write lock on this
   * record.
   * 
   * @param transactionPassword
   *          The plain text authentication password to prove that the client has the rights to
   *          access the transaction layer.
   * @param details
   *          The information as to why this request is needed used to create the exception if
   *          authentication fails.
   * @throws MirrorAuthorisationException
   *           If the supplied password is not correct for accessing the transaction layer.
   * @return The current write lock Transaction, or null if there is no write lock at present.
   * @see #getTransaction()
   */
  public final Transaction getTransaction(String transactionPassword,
                                          String details)
      throws MirrorAuthorisationException
  {
    getTable().getDb().checkTransactionPassword(transactionPassword, details);
    return getTransaction();
  }

  /**
   * @return The current write lock Transaction, or null there is no write lock at present.
   */
  @Override
  protected final Transaction getTransaction()
  {
    return _transaction;
  }

  /**
   * @return {@link Transaction#TRANSACTION_NULL}
   */
  @Override
  public int getTransactionType()
  {
    return Transaction.TRANSACTION_NULL;
  }

  /**
   * Check to see if any transaction currently has the write lock on this object. This is useful
   * because although it doesn't tell you which transaction has the lock (which would be a security
   * risk), it does tell you that there is a lock of some kind.
   * 
   * @return true if there is a transaction that currently has the write lock on this object.
   */
  public final boolean isLocked()
  {
    return _transaction != null;
  }

  /**
   * Check to see if a given transaction is the current owner of the write lock on this transaction.
   * Again, this is a security work around because it provides priviliged information about internal
   * security status of the write lock, but doesn't provide any information that is not readily
   * available to the caller (ie information about any transaction that the caller isn't aware of).
   * 
   * @param viewpoint
   *          The transaction to check if it is the owner
   * @return True if transaction currently has the write lock and false if the record is locked by
   *         another transaction, or the transaction parameter was null.
   */
  public final boolean isLockedBy(Viewpoint viewpoint)
  {
    return viewpoint != null && _transaction == viewpoint;
  }

  protected final String buildDeleteString()
  {
    StringBuilder sql = new StringBuilder();
    sql.append("delete from ").append(getTable().getName()).append(" where id=").append(getId());
    return sql.toString();
  }

  private void validate(Transaction transaction)
      throws MirrorTransactionOwnershipException
  {
    if (_transaction != transaction || _transaction == null) {
      throw new MirrorTransactionOwnershipException(transaction, this, null);
    }
  }

  protected void finaliseTransaction()
  {
    _writableRecord = null;
    _transaction = null;
    getTable().unlockRecord(this);
  }

  protected void setIsDeleted()
  {
    _isDeleted = true;
  }

  public boolean isDeleted()
  {
    return _isDeleted;
  }

  protected void cancel(Transaction transaction)
      throws MirrorTransactionOwnershipException, MirrorNestedException
  {
    validate(transaction);
    try {
      switch (transaction.getType()) {
        case Transaction.TRANSACTION_UPDATE:
          getTable().triggerStateTriggers(this, transaction, StateTrigger.STATE_WRITABLE_CANCELLED);
          break;
        case Transaction.TRANSACTION_DELETE:
          getTable().triggerStateTriggers(this,
                                          transaction,
                                          StateTrigger.STATE_PREDELETED_CANCELLED);
          break;
        default:
          throw new LogicException("Unexpected transaction type:" + transaction, null);
      }
    } finally {
      finaliseTransaction();
    }
  }

  /**
   * Override the <code>TransientRecord</code> version of <code>makePersistent()</code> to merely
   * return itself. <code>PersistentRecord</code> is already persistent, so it makes no sense to do
   * anything else.
   * 
   * @return itself.
   * @see TransientRecord#makePersistent(Gettable)
   */
  @Override
  public final PersistentRecord makePersistent(Gettable params)
  {
    return this;
  }

  /**
   * Extract a list of the PersistentRecords that are dependent on this record with optional
   * recursion into their dependent records as well.
   * 
   * @param allowRecursion
   *          If true then the complete dependency tree will be traversed and returned as a single
   *          list, if false then only records that are immediately dependent on this record are
   *          included.
   * @param transaction
   *          The Transaction of the type whos dependencies are being tracked. Please note that
   *          nothing is actually done with the Transaction other than calling getType() on it.
   * @return Iterator of PersistentRecords containing zero or more elements. This record will not be
   *         included in the list.
   * @see Transaction#getType()
   * @see DependencyTraverser
   */
  public Iterator<PersistentRecord> getDependentRecords(boolean allowRecursion,
                                                        Transaction transaction)
  {
    return getDependentRecords(allowRecursion, transaction.getType());
  }

  /**
   * Extract a list of the PersistentRecords that are dependent on this record with optional
   * recursion into their dependent records as well.
   * 
   * @param allowRecursion
   *          If true then the complete dependency tree will be traversed and returned as a single
   *          list, if false then only records that are immediately dependent on this record are
   *          included.
   * @param transactionType
   *          The type of Transaction whos dependencies are being tracked.
   * @return Iterator of PersistentRecords containing zero or more elements. This record will not be
   *         included in the list.
   * @see Transaction#getType()
   * @see DependencyTraverser
   */
  public Iterator<PersistentRecord> getDependentRecords(boolean allowRecursion,
                                                        int transactionType)
  {
    return DependencyTraverser.getInstance()
                              .traverseDependencies(this, allowRecursion, transactionType);
  }

  /**
   * Internal method to attempt to acquire a write lock on this record. This is called by
   * Transaction.acquireWriteLock(record,timeout) which is the correct way of obtaining a lock on a
   * record. A write lock is required before a PersistentRecord can either be updated or deleted
   * (depending on the style of transaction). In order to obtain a write lock, this record must not
   * be locked by other transactions. If this is the case then the method will repeatedly try and
   * acquire the lock until the timeout has been used up in the hope that the existing transaction
   * either times out or is committed/cancelled during the interim period.
   * 
   * @param transaction
   *          The Transaction that is attempting to acquire the write lock. If the Transaction is of
   *          type TRANSACTION_NULL then the write lock will be rejected and false returned.
   * @param timeout
   *          The maximum amount of time that this method should spend trying to acquire the lock
   *          from an existing transaction (if any).
   * @throws MirrorTransactionTimeoutException
   *           If the lock cannot be acquired during the specified time period.
   * @see Transaction#TRANSACTION_NULL
   */
  protected synchronized boolean acquireWriteLock(Transaction transaction,
                                                  long timeout)
      throws MirrorTransactionTimeoutException
  {
    if (_transaction == transaction) {
      return false;
    }
    if (_transaction == null) {
      if (transaction.getType() == Transaction.TRANSACTION_NULL) {
        return false;
      }
      _transaction = transaction;
      if (transaction.getType() == Transaction.TRANSACTION_UPDATE) {
        _writableRecord = WritableRecord.getInstance(this);
        // _writableRecord = new WritableRecord(this);
      } else {
        _writableRecord = null;
      }
      getTable().lockRecord(this);
      return true;
    }
    if (timeout < 1) {
      throw new MirrorTransactionTimeoutException("Transaction lock timed out",
                                                  transaction,
                                                  this,
                                                  timeout,
                                                  (_transaction == null
                                                      ? null
                                                      : _transaction.getName()),
                                                  null);
    }
    try {
      _transaction.checkValidity();
    } catch (Exception ex) {
      // if this bombs out then it will call cancelTransaction on all
      // source records, including this one --> _transaction == null
      // so we can recurse...
      finaliseTransaction();
      return acquireWriteLock(transaction, timeout);
    }
    // so we have a valid transaction that currently has lock, so we
    // have to wait...
    long startTime = System.currentTimeMillis();
    try {
      wait(timeout);
    } catch (InterruptedException ieEx) {
      // ????
    }
    return acquireWriteLock(transaction, timeout - (System.currentTimeMillis() - startTime));
  }

  /**
   * Request the WritableRecord version of this PersistentRecord, assuming that the client is in
   * possesion of the correct Transaction.
   * 
   * @param viewpoint
   *          The Transaction that the client believes has the lock on this PersistentRecord.
   * @return The WritableRecord that corresponds to the Transaction.
   * @throws MirrorTransactionOwnershipException
   *           If the transaction is not the current owner of the lock on the record (including if
   *           the record is not currently locked - this maybe should be a different exception...)
   */
  public final WritableRecord getWritableRecord(Viewpoint viewpoint)
      throws MirrorTransactionOwnershipException
  {
    if (viewpoint != null && viewpoint.contains(this)) {
      return getWritableRecord();
    }
    throw new MirrorTransactionOwnershipException(viewpoint, this, null);
  }

  /**
   * If this PersistentRecord is locked by viewpoint and viewpoint is an update transaction then
   * return the appropriate WritableRecord otherwise just return this.
   * 
   * @param viewpoint
   *          The transaction that might have a lock on this record.
   * @return Either this, or the equivalent WritableRecord. Not null.
   */
  public final PersistentRecord filterByTransaction(Viewpoint viewpoint)
  {
    if (_transaction != null && _transaction == viewpoint
        && _transaction.getType() == Transaction.TRANSACTION_UPDATE && _transaction.isValid()) {
      WritableRecord writableRecord = getWritableRecord();
      if (writableRecord != null) {
        return writableRecord;
      }
      System.err.println(String.format("%s.filterByTransaction(%s) --> null WritableRecord, _transaction:%s, getWritableRecord():%s",
                                       this,
                                       viewpoint,
                                       _transaction,
                                       getWritableRecord()));
    }
    return getPersistentRecord();
  }

  protected WritableRecord getWritableRecord()
  {
    return _writableRecord;
  }

  /**
   * Compatability method with WritableRecord.getPersistentRecord() that purely returns this
   * PersistentRecord for consistencies sake.
   * 
   * @return this.
   * @see WritableRecord#getPersistentRecord()
   */
  @Override
  public PersistentRecord getPersistentRecord()
  {
    return this;
  }
}
