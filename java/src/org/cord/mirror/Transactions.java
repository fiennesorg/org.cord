package org.cord.mirror;

import com.google.common.base.Preconditions;

public class Transactions
{
  /**
   * Create a new delete Transaction with a default timeout and delete all of the specified records.
   * 
   * @throws MirrorTransactionTimeoutException
   *           If it isn't possible to delete the records within the default timeout.
   */
  public static MirrorNestedException delete(Db db,
                                             String pwd,
                                             PersistentRecord... records)
      throws MirrorTransactionTimeoutException
  {
    Preconditions.checkNotNull(db, "db");
    try (Transaction t =
        db.createTransaction(Db.DEFAULT_TIMEOUT, Transaction.TRANSACTION_DELETE, pwd, null)) {
      for (PersistentRecord record : records) {
        t.delete(record);
      }
      MirrorNestedException mnEx = t.commit();
      return mnEx;
    }
  }
}
