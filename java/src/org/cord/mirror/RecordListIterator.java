package org.cord.mirror;

import com.google.common.collect.AbstractIterator;

public class RecordListIterator
  extends AbstractIterator<PersistentRecord>
{
  private final RecordList __recordList;
  private int _i = 0;
  private int _nextRebuildIndex;

  public RecordListIterator(RecordList recordList)
  {
    __recordList = recordList;
    _nextRebuildIndex = 0;
  }

  @Override
  protected PersistentRecord computeNext()
  {
    if (_i >= __recordList.size()) {
      return endOfData();
    }
    if (_i == _nextRebuildIndex) {
      int precacheSize =
          Math.min(__recordList.size() - _i, __recordList.getTable().getPrecacheSize());
      Table.resolveRecords(__recordList.getIdList(), _i, precacheSize);
      precacheCompleteHook(__recordList, _i, precacheSize);
      _nextRebuildIndex += precacheSize;
    }
    return __recordList.get(_i++);
  }

  /**
   * Hook that is invoked by {@link #computeNext()} once a precache operation has been completed.
   * The default implementation does nothing, but subclasses can override this if there is
   * functionality that should be executed in the knowledge that the Record cache has been primed.
   * 
   * @param recordList
   *          The RecordList that is being iterated across
   * @param startIndex
   *          The first record that was precached
   * @param precacheSize
   *          The number of records that were precached.
   */
  protected void precacheCompleteHook(RecordList recordList,
                                      int startIndex,
                                      int precacheSize)
  {
  }

}
