package org.cord.mirror;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class DbUtils
{
  public static void findFieldOverlaps(Db db)
  {
    SortedMap<String, Set<Field<?>>> overlaps = new TreeMap<String, Set<Field<?>>>();
    for (Table table : db.getTables()) {
      for (Field<?> field : table.getFields()) {
        String name = field.getName();
        Set<Field<?>> fields = overlaps.get(name);
        if (fields == null) {
          fields = new HashSet<Field<?>>();
          overlaps.put(name, fields);
        }
        fields.add(field);
      }
    }
    for (String name : overlaps.keySet()) {
      Set<Field<?>> fields = overlaps.get(name);
      if (fields.size() > 1) {
        System.out.println(name);
        for (Field<?> field : overlaps.get(name)) {
          System.out.println("  " + field.getTable() + ": " + field);
        }
      }
    }
  }
}
