package org.cord.mirror;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * Static utility classes for doing things with Dependencies implementors.
 * 
 * @author alex
 */
public class DependenciesUtils
{
  /**
   * Decode the bits set in bitfield into the List of Strings using the values defined in
   * {@link Dependencies#TAG_NAMES}. The order will be from lowest bit to highest.
   */
  public static List<String> toTags(int bitfield)
  {
    List<String> tags = Lists.newArrayList();
    for (int i = 0; i < Dependencies.TAG_NAMES.size(); i++) {
      if ((bitfield & (1 << i)) != 0) {
        tags.add(Dependencies.TAG_NAMES.get(i));
      }
    }
    return tags;
  }
}
