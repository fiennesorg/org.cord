package org.cord.mirror;

/**
 * Interface used to describe Objects that are capable of taking a Query and transforming it into a
 * new Query across the same Table. There is no way for the interface to enforce the same Table rule
 * so this is up to the developers to play nice.
 * 
 * @deprecated as too Query specific and not used by anyone at all. Will delete once I haven't
 *             decided whether or not it should be used once it has been made RecordSource friendly.
 */
@Deprecated
public interface QueryTransform
{
  /**
   * Perform the appropriate Table consistent transform on the sourceQuery.
   * 
   * @param sourceQuery
   *          The Query that is to be transformed.
   * @return The appropriately transformed Query which must iterator across PersistentRecords from
   *         the same Table as the sourceQuery. If this is not possible due to the sourceQuery being
   *         from a non-supported Table then I think that the default policy should be to return
   *         sourceQuery, or throw a RuntimeException of some kind. Therefore the method should
   *         never return null.
   */
  public Query transformQuery(Query sourceQuery);
}