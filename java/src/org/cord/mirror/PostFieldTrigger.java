package org.cord.mirror;

import org.cord.util.Shutdownable;

/**
 * Interface implemented by triggers that want to be informed of what the previous value of a given
 * field was in a record after a new value has been accepted by any FieldTriggers that where
 * previously implemented on the system. The important thing to remember about them is that they are
 * not really allowed to kick up a fuss - if they wanted to reject a value then they should have
 * been a more conventional FieldTrigger, and that they are allowed to do Bad Things, i.e. they can
 * make changes that cannot be rolled back. As a result, if implementations find it hard to complete
 * their chosen operation, then there are two options:-
 * <ul>
 * <li>re-implement themselves as FieldTriggers and perform enough checks before executing the
 * operation to be able to fail gracefully and cleanly.
 * <li>ensure that the stuff that they can't do leaves the system in as close a state as possible to
 * what it would be in if they had done what they where trying to do and plough on regardless. Don't
 * bail out wimpering and mess it up for any other future PostFieldTriggers because by the point it
 * reaches this stage, it is Normally Too Late!
 * </ul>
 */
public interface PostFieldTrigger<T>
  extends Shutdownable
{
  public FieldKey<T> getFieldName();

  /**
   * Inform the PostFieldTrigger that the information contained within the specified record has been
   * updated.
   * 
   * @param record
   *          The record that has been changed. This will either be a TransientRecord or a
   *          WritableRecord. The data stored in the record will be the <i>updated</i> value.
   * @param fieldName
   *          The name of the field that has completed the setField FieldTrigger operations before
   *          being accepted as a final value.
   * @param originalValue
   *          The data that was stored in the record before the setField operation commenced.
   * @param hasChanged
   *          True if the value of fieldName has been updated, false if the setField operation has
   *          resulted in an unchanged value (either because an identical value was specified or
   *          because a FieldTrigger has munged the data into an identical value).
   */
  public void trigger(TransientRecord record,
                      String fieldName,
                      Object originalValue,
                      boolean hasChanged);
}
