package org.cord.mirror.field;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;
import org.cord.util.StringUtils;

public class HexColourField
  extends StringField
{
  public final static String DEFAULTCOLOUR = "000000";

  public HexColourField(ObjectFieldKey<String> fieldName,
                        String label,
                        String defaultValue)
  {
    super(fieldName,
          label,
          6,
          isValidColour(defaultValue) ? defaultValue : DEFAULTCOLOUR);
  }

  private final static Pattern VALIDCOLOUR = Pattern.compile("[0-9a-f]{6}");

  public static boolean isValidColour(String colour)
  {
    if (colour == null) {
      return false;
    }
    if ("".equals(colour)) {
      return true;
    }
    return VALIDCOLOUR.matcher(colour).matches();
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String value = super.validateDefinedValue(record, obj);
    return isValidColour(value) ? value : DEFAULTCOLOUR;
  }

  private String getColourSwatch(String colour)
  {
    if ("".equals(colour)) {
      return "";
    } else {
      return "<div style=\"width: 100%; height: 2em; background-color: #" + colour
             + "\">&nbsp;</div>\n";
    }
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    String colour = StringUtils.toString(this.getFieldValue(record));
    HtmlUtils.textWidget(null, buf, colour, namePrefix, getName());
    HtmlUtils.value(buf, getColourSwatch(colour), namePrefix, getName());
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    String colour = StringUtils.toString(this.getFieldValue(record));
    HtmlUtils.value(buf, colour, namePrefix, getName());
    HtmlUtils.value(buf, getColourSwatch(colour), namePrefix, getName());
  }
}
