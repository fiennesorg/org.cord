package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.JodaGranularity;
import org.cord.util.ObjectUtil;
import org.joda.time.DateTime;

import com.google.common.base.Optional;

public class OptionalJodaDateTimeField
  extends OptionalField<DateTime>
{
  public static ObjectFieldKey<Optional<DateTime>> createKey(String keyName)
  {
    return ObjectFieldKey.create(keyName,
                                 ObjectUtil.<Optional<DateTime>> castClass(Optional.class));
  }

  public static final FieldValueSupplier<Optional<DateTime>> NULL =
      FieldValueSuppliers.of(Optional.<DateTime> absent());

  private final JodaGranularity __jodaGranularity;

  public OptionalJodaDateTimeField(ObjectFieldKey<Optional<DateTime>> fieldKey,
                                   String label,
                                   JodaGranularity jodaGranularity,
                                   FieldValueSupplier<Optional<DateTime>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
    __jodaGranularity = checkNotNull(jodaGranularity);
  }

  @Override
  public Optional<DateTime> objectToValue(TransientRecord record,
                                          Object obj)
  {
    return toDateTime(obj, __jodaGranularity);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    __jodaGranularity.widget(buf, context, record.comp(getKey()), namePrefix, getName());
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    DateTime value)
  {
    buffer.append(value.getMillis());
  }

  public static Optional<DateTime> toDateTime(Object value,
                                              JodaGranularity jodaGranularity)
  {
    if (value == null) {
      return Optional.absent();
    }
    if (value instanceof Optional) {
      Optional<?> optional = (Optional<?>) value;
      if (optional.isPresent()) {
        return toDateTime(optional.get(), jodaGranularity);
      } else {
        return Optional.absent();
      }
    }
    if (value instanceof DateTime) {
      return Optional.of(jodaGranularity.quantise((DateTime) value));
    }
    if (value instanceof Number) {
      return Optional.of(jodaGranularity.quantise(new DateTime(((Number) value).longValue())));
    }
    if (value instanceof Date) {
      return Optional.of(jodaGranularity.quantise(new DateTime(((Date) value).getTime())));
    }
    String string = value.toString();
    if (string.length() == 0) {
      return Optional.absent();
    }
    return jodaGranularity.quantise(jodaGranularity.parse(string));
  }

  @Override
  public Optional<DateTime> objectToOptionalValue(Object object)
      throws IllegalArgumentException
  {
    return toDateTime(object, __jodaGranularity);
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       DateTime value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.getMillis());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(LongField.ISVALIDSQL, sqlDef);
  }

  @Override
  public Optional<DateTime> dbToValue(Object v,
                                      Object[] values,
                                      int[] intValues)
  {
    return Optional.of(new DateTime((Long) v));
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }

}
