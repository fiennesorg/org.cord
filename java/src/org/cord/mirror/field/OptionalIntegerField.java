package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;

import com.google.common.base.Optional;

public class OptionalIntegerField
  extends OptionalTextInputField<Integer>
{
  public static final ObjectFieldKey<Optional<Integer>> createKey(String name)
  {
    return ObjectFieldKey.create(name, ObjectUtil.<Optional<Integer>> castClass(Optional.class));
  }

  public static final FieldValueSupplier<Optional<Integer>> NULL =
      FieldValueSuppliers.of(Optional.<Integer> absent());

  public OptionalIntegerField(ObjectFieldKey<Optional<Integer>> fieldKey,
                              String label,
                              FieldValueSupplier<Optional<Integer>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    Integer value)
  {
    buffer.append(value);
  }

  @Override
  public Optional<Integer> objectToOptionalValue(Object object)
  {
    return Optional.fromNullable(IntegerField.castValue(object));
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       Integer value)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), value.intValue());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(IntegerField.ISVALIDSQL, sqlDef);
  }

  @Override
  public Optional<Integer> getValue(TransientRecord record,
                                    Gettable params)
  {
    Object value = params.get(getName());
    if (value == null) {
      return params.containsKey(getName()) ? Optional.<Integer> absent() : null;
    }
    return Optional.fromNullable(NumberUtil.toInteger(value, null));
  }

  @Override
  public Optional<Integer> dbToValue(Object v,
                                     Object[] values,
                                     int[] intValues)
  {
    return Optional.of((Integer) v);
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

}
