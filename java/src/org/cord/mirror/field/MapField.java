package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.DebugConstants;
import org.cord.util.HtmlUtils;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class MapField<T>
  extends ObjectField<T>
{
  private final BiMap<String, T> __values = HashBiMap.create();
  private final Map<String, T> __roValues = Collections.unmodifiableMap(__values);
  private final Set<String> __orderedKeys = new LinkedHashSet<String>();
  private final Set<String> __roOrderedKeys = Collections.unmodifiableSet(__orderedKeys);

  public MapField(FieldKey<T> fieldName,
                  String label,
                  Class<T> valueClass,
                  FieldValueSupplier<T> newRecordValue,
                  FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public MapField(FieldKey<T> fieldName,
                  String label,
                  Class<T> valueClass,
                  FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          defaultDefinedValue);
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(StringField.ISVALIDVARCHAR, sqlDef);
  }

  /**
   * @return A readonly view of the mappings defined on this MapField
   */
  public Map<String, T> getValues()
  {
    return __roValues;
  }

  /**
   * @return A readonly view of the keys on this MapField with the iteration order being the order
   *         of registration.
   */
  public Set<String> getOrderedKeys()
  {
    return __roOrderedKeys;
  }

  /**
   * Define a single mapping from the SQL stored value to the T java value.
   * 
   * @return The value that was previously mapped to sql in the system, or null if this is a new
   *         mapping.
   */
  public T addMapping(String sql,
                      T value)
  {
    Preconditions.checkNotNull(sql, "sql");
    Preconditions.checkNotNull(value, "value");
    __orderedKeys.add(sql);
    return __values.put(sql, value);
  }

  /**
   * Add each mapping entry via the {@link #addMapping(String, Object)} method to the MapField.
   */
  public void addMappings(Map<String, T> mappings)
  {
    for (Map.Entry<String, T> entry : mappings.entrySet()) {
      addMapping(entry.getKey(), entry.getValue());
    }
  }

  public T lookupValue(String sql)
  {
    return Preconditions.checkNotNull(__values.get(sql), "Cannot resolve value for %s", sql);
  }

  public String lookupSql(Object value)
  {
    return Preconditions.checkNotNull(__values.inverse().get(value),
                                      "Cannot resolve SQL for %s",
                                      value);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    T value)
  {
    SqlUtil.toSafeSqlString(buffer, lookupSql(value));
  }

  @Override
  public T objectToValue(TransientRecord record,
                         Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (getValueClass().isInstance(obj)) {
      return getValueClass().cast(obj);
    }
    String sql = obj.toString();
    T value = __values.get(sql);
    if (value != null) {
      return value;
    }
    if (sql.length() == 0) {
      return getDefaultDefinedValue(record);
    }
    throw new ClassCastException(String.format("No mapping found for %s to %s",
                                               DebugConstants.argClass(obj),
                                               getValueClass()));
  }

  @Override
  public String valueToNodeRequest(T value)
  {
    if (value == null) {
      return null;
    }
    return __values.inverse().get(value);
  }

  @Override
  public T validateDefinedValue(TransientRecord record,
                                Object obj)
      throws MirrorFieldException, TypeValidationException
  {
    if (getValueClass().isInstance(obj)) {
      return getValueClass().cast(obj);
    }
    String sql = obj.toString();
    T value = __values.get(sql);
    if (value != null) {
      return value;
    }
    throw new TypeValidationException(String.format("Cannot resolve %s into a %s",
                                                    DebugConstants.argClass(obj),
                                                    getValueClass()),
                                      null,
                                      record,
                                      getName(),
                                      obj);
  }

  @Override
  public T dbToValue(Object v,
                     Object[] values,
                     int[] intValues)
  {
    return __values.get(v);
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, getFieldValue(record).toString(), namePrefix, getName());
  }

  public String toOptionContents(T value)
  {
    return value.toString();
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    T currentValue = getFieldValue(record);
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    for (String sql : __orderedKeys) {
      T value = lookupValue(sql);
      HtmlUtils.option(buf, sql, toOptionContents(value), Objects.equal(value, currentValue));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       T value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), lookupSql(value));
  }

  @Override
  protected int getSqlType()
  {
    return Types.VARCHAR;
  }

}
