package org.cord.mirror.field;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.json.ImmutableJSON;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSON;
import org.json.WritableJSONObject;

public class JSONObjectField
  extends AbstractJSONField<JSONObject>
{
  public static RecordOperationKey<JSONObject> createAsJSONKey(String name)
  {
    return RecordOperationKey.create(name, JSONObject.class);
  }

  public JSONObjectField(ObjectFieldKey<String> name,
                         String label,
                         RecordOperationKey<JSONObject> asJSONName,
                         JSONObject defaultValue)
  {
    super(name,
          label,
          asJSONName,
          defaultValue == null ? new WritableJSONObject() : defaultValue,
          JSONObject.class);
  }

  @Override
  protected final JSONObject asJSON(TransientRecord record)
      throws JSONException
  {
    return (record.isWriteable() ? WritableJSON.get() : ImmutableJSON.get()).toJSONObject(record.getString(getName()));
  }
}
