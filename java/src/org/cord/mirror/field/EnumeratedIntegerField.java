package org.cord.mirror.field;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class EnumeratedIntegerField
  extends IntField
{
  public static final RecordOperationKey<Map<Integer, String>> createValueDescriptorKey(String key)
  {
    return RecordOperationKey.create(key, ObjectUtil.<Map<Integer, String>> castClass(Map.class));
  }

  public static final RecordOperationKey<String> createExpandedFieldKey(String key)
  {
    return RecordOperationKey.create(key, String.class);
  }

  private final Map<Integer, String> __validValueNames = new LinkedHashMap<Integer, String>();

  private final Map<Integer, String> __publicValidValueNames =
      Collections.unmodifiableMap(__validValueNames);

  private final RecordOperationKey<Map<Integer, String>> __valueDescriptorName;
  private final RecordSourceOperationKey<Map<Integer, String>> __valueDescriptorRecordSourceKey;

  private final RecordOperationKey<String> __expandedFieldName;

  /**
   * @param valueDescriptorKey
   *          The optional name of the RecordOperation that will resolve to the Map of integer
   *          values to english names that are supported for this field. If this is null or empty
   *          then the value of &lt;fieldName&gt;s will be used instead.
   * @param expandedFieldKey
   *          The optional name of the RecordOperation that will resolve to the name of the value
   *          that is currently stored on the record that it is invoked on. If this is null or empty
   *          then the value of &lt;fieldName&gt;Expanded will be used instead.
   */
  public EnumeratedIntegerField(IntFieldKey fieldKey,
                                String label,
                                RecordOperationKey<Map<Integer, String>> valueDescriptorKey,
                                RecordOperationKey<String> expandedFieldKey)
  {
    super(fieldKey,
          label);
    if (valueDescriptorKey == null) {
      String keyName = fieldKey + "s";
      __valueDescriptorName =
          MoreObjects.firstNonNull(valueDescriptorKey,
                                   RecordOperationKey.create(keyName,
                                                             ObjectUtil.<Map<Integer, String>> castClass(Map.class)));
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(keyName,
                                          ObjectUtil.<Map<Integer, String>> castClass(Map.class));
    } else {
      __valueDescriptorName = valueDescriptorKey;
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(valueDescriptorKey.getKeyName(),
                                          ObjectUtil.<Map<Integer, String>> castClass(Map.class));
    }
    __expandedFieldName =
        MoreObjects.firstNonNull(expandedFieldKey,
                                 RecordOperationKey.create(fieldKey + "Expanded", String.class));
  }

  public void addValueName(int value,
                           String name,
                           boolean isDefault)
  {
    if (getLockStatus() != LO_NEW) {
      throw new IllegalStateException("Cannot add values once item is registered");
    }
    Preconditions.checkNotNull(name, "name");
    __validValueNames.put(Integer.valueOf(value), name);
    if (isDefault) {
      setDefaultDefinedValue(FieldValueSuppliers.ofDefined(Integer.valueOf(value)));
    }
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    Integer currentValue = getFieldValue(record);
    for (Iterator<Map.Entry<Integer, String>> i =
        getValidValues().entrySet().iterator(); i.hasNext();) {
      Map.Entry<Integer, String> entry = i.next();
      HtmlUtils.option(buf, entry.getKey(), entry.getValue(), entry.getKey().equals(currentValue));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, getValidValues().get(getFieldValue(record)), namePrefix, getName());
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    if (__valueDescriptorName != null) {
      getTable().addRecordOperation(new SimpleRecordOperation<Map<Integer, String>>(__valueDescriptorName) {
        @Override
        public Map<Integer, String> getOperation(TransientRecord targetRecord,
                                                 Viewpoint viewpoint)
        {
          return __publicValidValueNames;
        }
      });
      getTable().addRecordSourceOperation(new SimpleRecordSourceOperation<Map<Integer, String>>(__valueDescriptorRecordSourceKey) {
        @Override
        public Map<Integer, String> invokeRecordSourceOperation(RecordSource recordSource,
                                                                Viewpoint viewpoint)
        {
          return __publicValidValueNames;
        }
      });
    }
    if (__expandedFieldName != null) {
      getTable().addRecordOperation(new SimpleRecordOperation<String>(__expandedFieldName) {
        @Override
        public String getOperation(TransientRecord targetRecord,
                                   Viewpoint viewpoint)
        {
          return getValidValues().get(getFieldValue(targetRecord));
        }
      });
    }
  }

  /**
   * @return Collection of EnglishNamed items
   */
  public Map<Integer, String> getValidValues()
  {
    return __publicValidValueNames;
  }

  @Override
  public Integer validateDefinedValue(TransientRecord record,
                                      Object obj)
      throws MirrorFieldException
  {
    Integer proposedValue = super.validateDefinedValue(record, obj);
    if (__validValueNames.containsKey(proposedValue)) {
      return proposedValue;
    }
    return getDefaultDefinedValue(record);
  }
}
