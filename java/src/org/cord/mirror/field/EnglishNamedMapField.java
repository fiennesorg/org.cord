package org.cord.mirror.field;

import java.sql.SQLException;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.ObjectFieldKey;
import org.cord.util.EnglishNamed;

import com.google.common.base.Predicates;

public class EnglishNamedMapField<T extends EnglishNamed>
  extends NamedMapField<T>
{
  public static <T> ObjectFieldKey<T> createKey(String key,
                                                Class<T> valueClass)
  {
    return ObjectFieldKey.create(key, valueClass);
  }

  public EnglishNamedMapField(ObjectFieldKey<T> fieldName,
                              String label,
                              Class<T> valueClass,
                              FieldValueSupplier<T> newRecordValue,
                              FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public EnglishNamedMapField(ObjectFieldKey<T> fieldName,
                              String label,
                              Class<T> valueClass,
                              FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          defaultDefinedValue);
  }

  @Override
  public String toOptionContents(T value)
  {
    return value.getEnglishName();
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(Predicates.equalTo("varchar(255)"), sqlDef);
  }

}
