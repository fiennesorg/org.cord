package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.MyPredicates;

import com.google.common.base.Predicate;

public class LongField
  extends ObjectField<Long>
{
  public static ObjectFieldKey<Long> createKey(String name)
  {
    return ObjectFieldKey.create(name, Long.class);
  }

  public final static Long DEFAULT_VALUE = new Long(0);

  public LongField(ObjectFieldKey<Long> fieldName,
                   String label)
  {
    this(fieldName,
         label,
         DEFAULT_VALUE);
  }

  public LongField(ObjectFieldKey<Long> fieldName,
                   String label,
                   Long defaultValue)
  {
    super(fieldName,
          label,
          Long.class,
          FieldValueSuppliers.ofDefined(defaultValue));
  }

  /**
   * Create an optional LongField
   */
  public LongField(ObjectFieldKey<Long> key,
                   String label,
                   FieldValueSupplier<Long> newRecordValue,
                   FieldValueSupplier<Long> defaultDefinedValue)
  {
    super(key,
          label,
          Long.class,
          newRecordValue,
          defaultDefinedValue);
  }

  /**
   * Create a compulsory LongField
   */
  public LongField(ObjectFieldKey<Long> key,
                   String label,
                   FieldValueSupplier<Long> defaultDefinedValue)
  {
    super(key,
          label,
          Long.class,
          defaultDefinedValue);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Long sqlValue)
  {
    buffer.append(sqlValue);
  }

  @Override
  public Long validateDefinedValue(TransientRecord record,
                                   Object obj)
  {
    try {
      return objectToValue(record, obj);
    } catch (NumberFormatException nfeEx) {
      return getDefaultDefinedValue(record);
    }
  }

  @Override
  public String toString()
  {
    return getName() + "(LONG)";
  }

  public static final Predicate<String> ISVALIDSQL = MyPredicates.startsWith("bigint");

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(ISVALIDSQL, sqlDef);
  }

  @Override
  public Long dbToValue(Object v,
                        Object[] values,
                        int[] intValues)
  {
    return (Long) v;
  }

  @Override
  public Long objectToValue(TransientRecord record,
                            Object obj)
      throws NumberFormatException
  {
    return objectToValue(obj);
  }

  public static Long objectToValue(Object obj)
      throws NumberFormatException
  {
    if (obj instanceof Long) {
      return (Long) obj;
    }
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return Long.valueOf(((Number) obj).longValue());
    }
    return Long.valueOf(obj.toString());
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Long value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.longValue());
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }

}
