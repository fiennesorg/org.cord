package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.BooleanValueType;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.ValueType;

/**
 * FieldFilter that represents a single boolean logical state using an INT field as the SQL backend
 * representation.
 */
public class BooleanField
  extends ObjectField<Boolean>
{
  public static ObjectFieldKey<Boolean> createKey(String name)
  {
    return ObjectFieldKey.create(name, Boolean.class);
  }

  /**
   * Create a named boolean FieldFilter with a given default value.
   * 
   * @param fieldName
   *          The SQL table field that this FieldFilter reflects
   * @param label
   *          The human readable value that should be used when describing this field in a GUI
   * @param defaultValue
   *          The initial value that this field should default to.
   */
  public BooleanField(ObjectFieldKey<Boolean> fieldName,
                      String label,
                      Boolean defaultValue)
  {
    super(fieldName,
          label,
          Boolean.class,
          FieldValues.of(defaultValue == null ? Boolean.FALSE : defaultValue));
  }

  /**
   * Create a named boolean FieldFilter with a default value of false.
   * 
   * @param name
   *          The SQL table field that this FieldFilter reflects.
   * @param label
   *          The human readable value that should be used when describing this field in a GUI
   */
  public BooleanField(ObjectFieldKey<Boolean> name,
                      String label)
  {
    this(name,
         label,
         null);
  }

  public BooleanField(ObjectFieldKey<Boolean> name,
                      String label,
                      Boolean newRecordValue,
                      Boolean defaultDefinedValue)
  {
    super(name,
          label,
          Boolean.class,
          FieldValues.of(newRecordValue),
          FieldValues.ofNotNull(defaultDefinedValue));
  }

  public static Boolean castValue(Object obj)
  {
    if (obj instanceof Number) {
      return Boolean.valueOf(((Number) obj).intValue() != 0);
    }
    if (obj instanceof Boolean) {
      return (Boolean) obj;
    }
    String objString = obj.toString();
    try {
      return Boolean.valueOf(Integer.parseInt(objString) != 0);
    } catch (NumberFormatException nfEx) {
      if (Boolean.TRUE.toString().equals(objString)) {
        return Boolean.TRUE;
      }
      if (Boolean.FALSE.toString().equals(objString)) {
        return Boolean.FALSE;
      }
    }
    return null;
  }

  /**
   * Attempt to convert an object into a valid Boolean value. Tactics are as follows:-
   * <ul>
   * <li>if null, return default value
   * <li>if Boolean, return itself
   * <li>Try and parse as an integer string and the check for value != 0. Parsing failure results in
   * default value.
   * </ul>
   * 
   * @param obj
   *          The object to convert
   * @return The java.lang.Boolean version of obj.
   */
  @Override
  public Boolean validateDefinedValue(TransientRecord record,
                                      Object obj)
  {
    Boolean b = castValue(obj);
    return b == null ? getDefaultDefinedValue(record) : b;
  }

  @Override
  public Boolean dbToValue(Object o,
                           Object[] values,
                           int[] intValues)
  {
    return Boolean.valueOf(((Integer) o).intValue() != 0);
  }

  @Override
  public final void appendDefinedSqlValue(StringBuilder buffer,
                                          Boolean sqlValue)
  {
    buffer.append(sqlValue.booleanValue() ? '1' : '0');
  }

  @Override
  public String toString()
  {
    return getName() + "(BOOLEAN:" + getDefaultDefinedValue(null) + ")";
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.checkboxInput(buf,
                            null,
                            record.comp(getKey()).booleanValue(),
                            shouldReloadOnChange(),
                            false,
                            namePrefix,
                            getName());
  }

  /**
   * @see Gettable#getBoolean(Object)
   */
  @Override
  public Boolean getValue(TransientRecord record,
                          Gettable params)
  {
    return params.getBoolean(getName());
  }

  /**
   * @return the appropriate BooleanValueType
   * @see BooleanValueType
   */
  @Override
  public ValueType getValueType()
  {
    return new BooleanValueType(getName(),
                                getEnglishName(),
                                null,
                                false,
                                getDefaultDefinedValueSupplier());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(IntegerField.ISVALIDSQL, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Boolean value)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), value.booleanValue() ? 1 : 0);
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }
}
