package org.cord.mirror.field;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.util.EnglishNamed;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

/**
 * Extension of StringFieldFilter that constrains fields to only contain values from a defined set
 * of valid values with the values defined as EnglishNamed items and the GUI showing the human
 * readable version.
 */
public class EnglishNamedEnumerationField
  extends StringField
{
  private final Map<String, EnglishNamed> __validEnglishNameds =
      new LinkedHashMap<String, EnglishNamed>();

  private final Map<String, EnglishNamed> __unmodifiableValidEnglishNameds =
      Collections.unmodifiableMap(__validEnglishNameds);

  private final RecordOperationKey<Map<String, EnglishNamed>> __valueDescriptorName;
  private final RecordSourceOperationKey<Map<String, EnglishNamed>> __valueDescriptorRecordSourceKey;

  /**
   * @param label
   *          The humanly readable description of the field that is utilised for building a GUI
   *          representation of the field.
   * @param valueDescriptorName
   *          The name via which the list of valid values may be recalled from the Table, ie
   *          $table.valueDescriptorName --> immutable List of validValues. If null (fieldName)s is
   *          utilised.
   */
  public EnglishNamedEnumerationField(ObjectFieldKey<String> fieldName,
                                      String label,
                                      RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName)
  {
    super(fieldName,
          label,
          LENGTH_VARCHAR,
          null);
    if (valueDescriptorName == null) {
      String name = fieldName + "s";
      __valueDescriptorName =
          RecordOperationKey.create(name,
                                    ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(name,
                                          ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));
    } else {
      __valueDescriptorName = valueDescriptorName;
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(valueDescriptorName.getKeyName(),
                                          ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));

    }
  }

  public RecordOperationKey<Map<String, EnglishNamed>> getValueDescriptorRecordKey()
  {
    return __valueDescriptorName;
  }

  public RecordSourceOperationKey<Map<String, EnglishNamed>> getValueDescriptorRecordSourceKey()
  {
    return __valueDescriptorRecordSourceKey;
  }

  public void addValidValue(EnglishNamed value,
                            boolean isDefault)
  {
    if (getLockStatus() != LO_NEW) {
      throw new IllegalStateException("Cannot add values once item is registered");
    }
    Preconditions.checkNotNull(value, "value");
    String name = value.getName();
    __validEnglishNameds.put(name, value);
    if (isDefault) {
      setDefaultValue(name);
    }
  }

  /**
   * Set which is the default value for this field by name.
   * 
   * @param name
   *          The name of a previously registered value.
   * @throws IllegalArgumentException
   *           if name has not previously been added as a valid value.
   */
  public void setDefaultValue(String name)
  {
    if (!__validEnglishNameds.containsKey(name)) {
      throw new IllegalArgumentException(String.format("%s is not a registered name", name));
    }
    FieldValueSupplier<String> supplier = FieldValueSuppliers.ofDefined(name);
    setDefaultDefinedValue(supplier);
    setNewRecordValue(supplier);
  }

  public void addValidValue(Object name,
                            Object englishName,
                            boolean isDefault)
  {
    addValidValue(new EnglishNamedImpl(StringUtils.toString(name),
                                       StringUtils.toString(englishName)),
                  isDefault);
  }

  public void addValidValue(Object value,
                            boolean isDefault)
  {
    addValidValue(value instanceof EnglishNamed
        ? (EnglishNamed) value
        : new EnglishNamedImpl(StringUtils.toString(value)), isDefault);
  }

  public void addValidValues(Iterable<EnglishNamed> values)
  {
    for (EnglishNamed value : values) {
      addValidValue(value, false);
    }
  }

  /**
   * Add valid values to this field as defined in the given Gettable. The keys will be stored in
   * &lt;namespace&gt;.keys and will have definable keys for the name key and englishName key
   * relative to the namespace.
   */
  public void addValidValues(Gettable gettable,
                             String namespace,
                             String nameKey,
                             String englishNameKey)
  {
    addValidValues(GettableUtils.getValues(gettable,
                                           namespace,
                                           "keys",
                                           GettableUtils.getEnglishNamedGettableSupplier(nameKey,
                                                                                         englishNameKey)));
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    String value = getFieldValue(record);
    for (Iterator<EnglishNamed> i = getValidValues().iterator(); i.hasNext();) {
      EnglishNamed targetValue = i.next();
      HtmlUtils.option(buf,
                       targetValue.getName(),
                       targetValue.getEnglishName(),
                       targetValue.getName().equals(value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    if (__valueDescriptorName != null) {
      getTable().addRecordOperation(new SimpleRecordOperation<Map<String, EnglishNamed>>(__valueDescriptorName) {
        @Override
        public Map<String, EnglishNamed> getOperation(TransientRecord targetRecord,
                                                      Viewpoint viewpoint)
        {
          return __unmodifiableValidEnglishNameds;
        }
      });
      getTable().addRecordSourceOperation(new SimpleRecordSourceOperation<Map<String, EnglishNamed>>(__valueDescriptorRecordSourceKey) {
        @Override
        public Map<String, EnglishNamed> invokeRecordSourceOperation(RecordSource recordSource,
                                                                     Viewpoint viewpoint)
        {
          return __unmodifiableValidEnglishNameds;
        }
      });
    }
  }

  /**
   * @return Collection of EnglishNamed items
   */
  public Collection<EnglishNamed> getValidValues()
  {
    return __unmodifiableValidEnglishNameds.values();
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String proposedValue = super.validateDefinedValue(record, obj);
    if (__validEnglishNameds == null) {
      return proposedValue;
    }
    if (__validEnglishNameds.containsKey(proposedValue)) {
      return proposedValue;
    }
    return getDefaultDefinedValue(record);
  }
}
