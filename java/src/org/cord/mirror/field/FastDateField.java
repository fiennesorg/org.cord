package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.DateFieldExtractor;
import org.cord.mirror.operation.DateFieldRecordOperation;
import org.cord.util.DateQuantiser;
import org.cord.util.DateQuantiser.Granularity;
import org.cord.util.Dates;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringUtils;
import org.cord.util.Today;
import org.json.JSONException;
import org.json.Null;
import org.json.WritableJSONObject;

import com.google.common.base.Preconditions;

/**
 * Field type that stored a time in milliseconds inside a BIGINT value in the mysql database. This
 * has the advantage that conversions to and from the SQL datatypes is considerably faster. The
 * class also supports the optional creation of virtual accessor methods that are utilised to access
 * the various components of the date from the record clas directly. These fields are accessed by
 * the appending the appropriate suffix to the the name of the FastDateFieldFilter, as defined in
 * FastDateConstants. So if we had a field named "publicationDate", then we would be able to access
 * "$record.publicationDate_month" which would resolve to the integer value for the month.
 */
public class FastDateField
  extends ObjectField<Date>
{
  public static ObjectFieldKey<Date> createKey(String name)
  {
    return ObjectFieldKey.create(name, Date.class);
  }

  /**
   * "_millisecond"
   */
  public final static String SUFFIX_MILLISECOND = "_millisecond";

  /**
   * "_second"
   */
  public final static String SUFFIX_SECOND = "_second";

  /**
   * "_minute"
   */
  public final static String SUFFIX_MINUTE = "_minute";

  /**
   * "_hour"
   */
  public final static String SUFFIX_HOUR = "_hour";

  /**
   * "_day"
   */
  public final static String SUFFIX_DAY = "_day";

  /**
   * "_month"
   */
  public final static String SUFFIX_MONTH = "_month";

  /**
   * "_year"
   */
  public final static String SUFFIX_YEAR = "_year";

  private final Granularity __granularity;

  private final DateQuantiser __quantiser = new DateQuantiser();

  private FastDateField(ObjectFieldKey<Date> fieldName,
                        String label,
                        boolean supportsNull,
                        FieldValueSupplier<Date> newRecordValue,
                        FieldValueSupplier<Date> defaultDefinedValue,
                        Granularity granularity)
  {
    super(fieldName,
          label,
          supportsNull,
          Date.class,
          newRecordValue == null
              ? null
              : DateSupplierQuantised.getInstance(newRecordValue, granularity),
          DateSupplierQuantised.getInstance(defaultDefinedValue, granularity));
    __granularity = Preconditions.checkNotNull(granularity, "granularity");
  }

  /**
   * Create a new compulsory FastDateField with definable Granularity and default values.
   */
  public FastDateField(ObjectFieldKey<Date> fieldName,
                       String label,
                       FieldValueSupplier<Date> newRecordValue,
                       Granularity granularity)
  {
    this(fieldName,
         label,
         false,
         newRecordValue,
         newRecordValue,
         granularity);
  }

  /** Create an optional FastDateField **/
  public FastDateField(ObjectFieldKey<Date> fieldName,
                       String label,
                       FieldValueSupplier<Date> newRecordValue,
                       FieldValueSupplier<Date> defaultDefinedValue,
                       Granularity granularity)
  {
    this(fieldName,
         label,
         true,
         newRecordValue,
         defaultDefinedValue,
         granularity);
  }

  /** Create a compulsory FastDateField with a Granularity of ms and a default value of now. **/
  public FastDateField(ObjectFieldKey<Date> fieldName,
                       String label)
  {
    this(fieldName,
         label,
         DateSupplierNow.getInstance(),
         Granularity.MILLISECOND);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openWidgetDiv(buf, HtmlUtils.CSS_W_DATE, namePrefix, getName());
    HtmlUtilsTheme.hiddenInput(buf, "", namePrefix, getName());
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(getFieldValue(record));
    if (__granularity.compareTo(Granularity.DAY) <= 0) {
      HtmlUtils.openSelectWidget(buf,
                                 "fastdate",
                                 false,
                                 false,
                                 false,
                                 namePrefix,
                                 getName() + DateFieldExtractor.FE_DAY);
      int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
      for (int day = 1; day <= 31; day++) {
        String dayString = Integer.toString(day);
        HtmlUtils.option(buf, dayString, dayString, day == currentDay);
      }
      HtmlUtils.closeSelectWidget(buf);
    }
    // Note that the implementation of the Months is slightly quirky with
    // offsets to preserve compatability with the legacy date templates. This
    // may be work-round-able...
    if (__granularity.compareTo(Granularity.MONTH) <= 0) {
      HtmlUtils.openSelectWidget(buf,
                                 "fastdate",
                                 false,
                                 false,
                                 false,
                                 namePrefix,
                                 getName(),
                                 DateFieldExtractor.FE_MONTH);
      int currentMonth = calendar.get(Calendar.MONTH);
      for (int month = 0; month <= 11; month++) {
        HtmlUtils.option(buf,
                         Integer.toString(month + 1),
                         MONTHS.get(month),
                         month == currentMonth);
      }
      HtmlUtils.closeSelectWidget(buf);
    }
    HtmlUtils.openSelectWidget(buf,
                               "fastdate",
                               false,
                               false,
                               false,
                               namePrefix,
                               getName(),
                               DateFieldExtractor.FE_YEAR);
    int currentYear = calendar.get(Calendar.YEAR);
    int minGuiYear = Math.min(getMinGuiYear(), currentYear - 10);
    int maxGuiYear = Math.max(getMaxGuiYear(), currentYear + 10);
    for (int year = minGuiYear; year <= maxGuiYear; year++) {
      String yearString = Integer.toString(year);
      HtmlUtils.option(buf, yearString, yearString, year == currentYear);
    }
    HtmlUtils.closeSelectWidget(buf);
    if (__granularity.compareTo(Granularity.HOUR) <= 0) {
      buf.append("<br />\n");
      HtmlUtils.openSelectWidget(buf,
                                 "fastdate",
                                 false,
                                 false,
                                 false,
                                 namePrefix,
                                 getName(),
                                 DateFieldExtractor.FE_HOUR);
      int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
      for (int hour = 0; hour <= 23; hour++) {
        String hourString = Integer.toString(hour);
        HtmlUtils.option(buf, hourString, hourString, hour == currentHour);
      }
      HtmlUtils.closeSelectWidget(buf);
      if (__granularity.compareTo(Granularity.MINUTE) <= 0) {
        HtmlUtils.openSelectWidget(buf,
                                   "fastdate",
                                   false,
                                   false,
                                   false,
                                   namePrefix,
                                   getName(),
                                   DateFieldExtractor.FE_MINUTE);
        int currentMinute = calendar.get(Calendar.MINUTE);
        for (int minute = 0; minute <= 59; minute++) {
          String minuteString = Integer.toString(minute);
          HtmlUtils.option(buf, minuteString, minuteString, minute == currentMinute);
        }
        HtmlUtils.closeSelectWidget(buf);
        if (__granularity.compareTo(Granularity.SECOND) <= 0) {
          HtmlUtils.openSelectWidget(buf,
                                     "fastdate",
                                     false,
                                     false,
                                     false,
                                     namePrefix,
                                     getName(),
                                     DateFieldExtractor.FE_SECOND);
          int currentSecond = calendar.get(Calendar.SECOND);
          for (int second = 0; second <= 59; second++) {
            String secondString = Integer.toString(second);
            HtmlUtils.option(buf, secondString, secondString, second == currentSecond);
          }
          HtmlUtils.closeSelectWidget(buf);
        }
      }
    }
    HtmlUtilsTheme.closeDiv(buf);
  }

  public static final List<String> MONTHS =
      Collections.unmodifiableList(Arrays.asList(new String[] { "Jan", "Feb", "Mar", "Apr", "May",
          "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }));

  /**
   * Get the smallest year that this FastDateField will present in the drop-down list of years in
   * the edit interface.
   * 
   * @return 10 years in the past. Override this if you want to customise the behaviour.
   */
  public int getMinGuiYear()
  {
    return Today.getInstance().get(Calendar.YEAR) - 10;
  }

  /**
   * Get the largest year that this FastDateField will present in the drop-down list of years in the
   * edit interface.
   * 
   * @return 10 years in the future. Override this if you want to customise the behaviour.
   */
  public int getMaxGuiYear()
  {
    return Today.getInstance().get(Calendar.YEAR) + 10;
  }

  /**
   * Create a compulsory FastDateField with a configurable Granularity and a default value of now.
   */
  public FastDateField(ObjectFieldKey<Date> fieldName,
                       String label,
                       Granularity granularity)
  {
    this(fieldName,
         label,
         DateSupplierNow.getInstance(),
         granularity);
  }

  public Granularity getGranularity()
  {
    return __granularity;
  }

  /**
   * Convert the sqlValue into a java.util.Date object and then the getTime() result to the buffer.
   */
  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Date sqlValue)
  {
    buffer.append(sqlValue.getTime());
  }

  @Override
  public Date validateDefinedValue(TransientRecord record,
                                   Object obj)
  {
    if (obj instanceof Date) {
      return quantise((Date) obj);
    }
    if (obj instanceof Long) {
      return quantise(((Long) obj).longValue());
    }
    final Date fallback = getDefaultDefinedValue(record);
    String str = StringUtils.toString(obj);
    SimpleDateFormat format = getFormat();
    if (format != null) {
      return quantise(Dates.parse(format, str, fallback, fallback));
    }
    try {
      return quantise(Long.parseLong(str));
    } catch (NumberFormatException nfEx) {
      return fallback;
    }
  }

  /**
   * Get the SimpleDateFormat that this field is prepared to accept String value representations in.
   * 
   * @return null to keep the behaviour the same as before. This might be extended in the future. In
   *         the meantime, subclasses that want to be able to accept string representations of dates
   *         should override this method.
   */
  protected SimpleDateFormat getFormat()
  {
    return null;
  }

  @Override
  public Date dbToValue(Object v,
                        Object[] values,
                        int[] intValues)
  {
    return quantise(((Long) v).longValue());
  }

  /**
   * Pass the given date Object through the internal quantiser (if any) that is registered on this
   * field.
   * 
   * @param date
   *          The date to be quantised.
   * @return The quantised version of the date. If the precision is set to PRECISION_MILLISECOND (ie
   *         no quantisation) then the returned value will be the same instance as the parameter
   *         date. If the value of date is null then the returned value will be null.
   */
  public final Date quantise(Date date)
  {
    if (date == null) {
      return null;
    }
    return __quantiser.quantise(date, __granularity);
  }

  public final Date quantise(long ms)
  {
    return new Date(__quantiser.quantise(ms, __granularity));
  }

  private void addAndPreLock(Table table,
                             DateFieldRecordOperation dateFieldRecordOperation)
      throws MirrorTableLockedException
  {
    table.addRecordOperation(dateFieldRecordOperation);
    dateFieldRecordOperation.preLock();
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    final Table table = getTable();
    switch (__granularity) {
      case MILLISECOND:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_MILLISECOND,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.MILLISECOND));
      case SECOND:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_SECOND,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.SECOND));
      case MINUTE:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_MINUTE,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.MINUTE));
      case HOUR:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_HOUR,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.HOUR_OF_DAY));
      case DAY:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName() + SUFFIX_DAY,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.DAY_OF_MONTH));
      case MONTH:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_MONTH,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.MONTH));
      case YEAR:
        addAndPreLock(table,
                      new DateFieldRecordOperation(RecordOperationKey.create(getName()
                                                                             + SUFFIX_YEAR,
                                                                             Integer.class),
                                                   getKey(),
                                                   Calendar.YEAR));
        break;
    }
  }

  private Calendar _calendar = Calendar.getInstance();

  @Override
  public Date getValue(TransientRecord record,
                       Gettable params)
  {
    Object value = params.get(getName());
    if (value == null) {
      return null;
    }
    String year = params.getString(getName() + DateFieldExtractor.FE_YEAR);
    if (year == null) {
      return validateDefinedValue(record, value);
    } else {
      String realMonth = params.getString(getName() + DateFieldExtractor.FE_MONTH);
      String day = params.getString(getName() + DateFieldExtractor.FE_DAY);
      String hour = params.getString(getName() + DateFieldExtractor.FE_HOUR);
      String minute = params.getString(getName() + DateFieldExtractor.FE_MINUTE);
      String second = params.getString(getName() + DateFieldExtractor.FE_SECOND);
      synchronized (_calendar) {
        _calendar.clear();
        setField(_calendar, Calendar.YEAR, year);
        setField(_calendar, Calendar.MONTH, realMonth, -1);
        setField(_calendar, Calendar.DAY_OF_MONTH, day);
        setField(_calendar, Calendar.HOUR_OF_DAY, hour);
        setField(_calendar, Calendar.MINUTE, minute);
        setField(_calendar, Calendar.SECOND, second);
        return _calendar.getTime();
      }
    }
  }

  private static void setField(Calendar calendar,
                               int field,
                               String value)
  {
    setField(calendar, field, value, 0);
  }

  private static void setField(Calendar calendar,
                               int field,
                               String value,
                               int offset)
  {
    if (value != null) {
      try {
        calendar.set(field, Integer.parseInt(value) + offset);
      } catch (NumberFormatException nfEx) {
      }
    }
  }

  @Override
  public String toString(TransientRecord targetRecord)
  {
    Date date = getFieldValue(targetRecord);
    if (date == null) {
      return "null";
    }
    return __granularity.toString(date);
  }

  /**
   * Put the long timestamp in ms into the jObj
   */
  @Override
  public void putJSON(WritableJSONObject jObj,
                      TransientRecord record)
      throws JSONException
  {
    Date date = getFieldValue(record);
    if (date == null) {
      jObj.put(getName(), Null.getInstance());
    } else {
      jObj.put(getName(), date.getTime());
    }
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(LongField.ISVALIDSQL, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Date value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.getTime());
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }

}
