package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Enums;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Strings;

public class OptionalEnumField<T extends Enum<T>>
  extends OptionalField<T>
{
  public static <T extends Enum<T>> ObjectFieldKey<Optional<T>> createKey(String keyName)
  {
    return ObjectFieldKey.create(keyName, ObjectUtil.<Optional<T>> castClass(Optional.class));
  }

  public static final <T extends Enum<T>> FieldValueSupplier<Optional<T>> NULL()
  {
    return FieldValueSuppliers.of(Optional.<T> absent());
  }

  private final Class<T> __enumClass;

  public OptionalEnumField(ObjectFieldKey<Optional<T>> fieldKey,
                           String label,
                           Class<T> enumClass,
                           FieldValueSupplier<Optional<T>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
    __enumClass = checkNotNull(enumClass);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    T currentValue = getFieldValue(record).orNull();
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    HtmlUtils.option(buf, "", "", currentValue == null);
    for (T value : __enumClass.getEnumConstants()) {
      HtmlUtils.option(buf, value.name(), value.toString(), Objects.equal(currentValue, value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    T value)
  {
    SqlUtil.toSafeSqlString(buffer, value.name());
  }

  @Override
  public Optional<T> objectToOptionalValue(Object object)
      throws IllegalArgumentException
  {
    if (__enumClass.isInstance(object)) {
      return Optional.of(__enumClass.cast(object));
    }
    return Enums.getIfPresent(__enumClass, Strings.nullToEmpty(object.toString()));
  }

  @Override
  public Optional<T> objectToValue(TransientRecord record,
                                   Object obj)
  {
    if (obj instanceof Optional) {
      Optional<?> optional = (Optional<?>) obj;
      if (optional.isPresent()) {
        return objectToOptionalValue(optional.get());
      }
      return Optional.absent();
    }
    return objectToOptionalValue(obj);
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       T value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value.name());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(EnumField.ISVALIDSQL, sqlDef);
  }

  @Override
  public Optional<T> dbToValue(Object v,
                               Object[] values,
                               int[] intValues)
  {
    return Enums.getIfPresent(__enumClass, Strings.nullToEmpty(StringUtils.toString(v)));
  }
  @Override
  protected int getSqlType()
  {
    return EnumField.SQLTYPE;
  }

}
