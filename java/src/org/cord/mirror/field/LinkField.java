package org.cord.mirror.field;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DependenciesUtils;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.IdList;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordResolver;
import org.cord.mirror.RecordSource;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.operation.AbstractForwardRecordOperation;
import org.cord.mirror.operation.AbstractReverseRecordOperation;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.LinkFieldTrigger;
import org.cord.mirror.trigger.LinkInstantiationTrigger;
import org.cord.util.Casters;
import org.cord.util.ExceptionUtil;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.NumberUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;

/**
 * FieldFilter that represents an Integer field that holds the id of a PersistentRecord in a given
 * Table.
 */
public class LinkField
  extends IntField
  implements RecordResolver
{
  public final static String LINKENDING = "_id";

  public final static int UNDEFINEDLINK = 0;

  public final static Supplier<Integer> UNDEFINEDLINKSUPPLIER =
      Suppliers.ofInstance(Integer.valueOf(UNDEFINEDLINK));

  public static IntFieldKey createKey(String keyName)
  {
    return IntFieldKey.create(keyName);
  }

  /**
   * Construct the default field name from the name of the target table by dropping the case of the
   * first letter and appending LINKENDING to the result.
   * 
   * @see #LINKENDING
   */
  public static IntFieldKey getLinkFieldName(String targetTableName)
  {
    return IntFieldKey.create(StringUtils.toLowerCaseFirstLetter(targetTableName) + LINKENDING);
  }

  /**
   * construct the default key to resolve the RecordSource of refererring records via
   * {@link Table#getDefaultReferencesName(String)}
   */
  public static RecordOperationKey<RecordSource> getReferencesKey(String sourceTableName)
  {
    return Table.getDefaultReferencesName(sourceTableName);
  }

  /**
   * Utility method to extract the default name of a forward reference from the name of the field it
   * corresponds to. The convention is to convert "abc_id" into "abc".
   * 
   * @param fieldKey
   *          The field name to convert.
   * @return The appropriate linkName. Never null
   * @throws IllegalArgumentException
   *           If linkFieldName is null or if it doesn't end with LINKENDING.
   * @see #LINKENDING
   */
  public final static RecordOperationKey<PersistentRecord> getLinkName(IntFieldKey fieldKey)
  {
    String linkFieldName = fieldKey.getKeyName();
    if (!linkFieldName.endsWith(LINKENDING)) {
      throw new IllegalArgumentException("Cannot convert to LinkName: " + linkFieldName);
    }
    return RecordOperationKey.create(linkFieldName.substring(0,
                                                             linkFieldName.length()
                                                                - LINKENDING.length()),
                                     PersistentRecord.class);
  }

  private final RecordOperationKey<PersistentRecord> __linkName;

  private final String __targetTableName;

  private final int __linkStyle;

  private final String __transactionPassword;

  private RecordOperationKey<RecordSource> _referencesName;

  private final String __reverseOrderClause;

  private Table _targetTable;

  private int _ignoreId = -1;

  private int _defaultId = UNDEFINEDLINK;

  private FieldKey<String> _defaultIdField = null;

  private String _defaultIdValue = null;

  private Gettable _autoCreateInitParams = null;

  /**
   * @param fieldKey
   *          The name of the database field that holds the link. If null, then this will be
   *          auto-generated via getLinkFieldName(targetTableName).
   * @param label
   *          The human readable name of this field. Utilised for automated generation of GUI items.
   * @param linkName
   *          The name by which the forward operation is invoked. If null then this will be
   *          substitued with the result of getLinkName(linkFieldName) at construction time.
   * @param targetTableName
   *          The name of the Table which this LinkFieldFilter points at. This Table does not yet
   *          need to have been initialised as the appropriate operations will be created at the
   *          preLock() stages.
   * @param referencesName
   *          The name by which the reverse operation is invoked. If null then this will be
   *          substituted with the default references name on the target table at linkage time.
   * @param reverseOrderClause
   *          The field name(s) on the source table which will be utilised to sort the reverse
   *          operation. If this is null then it will be substituted with the value of
   *          Table.getDefaultOrdering() at linkage time.
   * @param linkStyle
   *          The bitfield that describes the functionality of the link. This should be a
   *          combination of constants from the Dependencies interface.
   * @see Table#getReferencesName()
   * @see Table#getDefaultOrdering()
   * @see #getLinkFieldName(String)
   * @see #getLinkName(IntFieldKey)
   * @see Dependencies
   */
  public LinkField(final IntFieldKey fieldKey,
                   final String label,
                   final Integer defaultValue,
                   final RecordOperationKey<PersistentRecord> linkName,
                   final String targetTableName,
                   final RecordOperationKey<RecordSource> referencesName,
                   final Object reverseOrderClause,
                   final String pwd,
                   final int linkStyle)
  {
    super(fieldKey == null ? getLinkFieldName(targetTableName) : fieldKey,
          label,
          FieldValueSuppliers.of(NumberUtil.INTEGER_ZERO));
    if (defaultValue != null) {
      _defaultId = defaultValue.intValue();
    }
    __linkName = linkName == null ? getLinkName(getKey()) : linkName;
    __targetTableName = targetTableName;
    _referencesName = referencesName;
    __reverseOrderClause = StringUtils.toString(reverseOrderClause);
    __transactionPassword = pwd;
    __linkStyle = linkStyle;
    FieldValueSupplier<Integer> fieldValueSupplier = new FieldValueSupplier<Integer>() {
      @Override
      public Integer supplyValue(TransientRecord record)
      {
        return Integer.valueOf(calculateDefaultValue());
      }
    };
    setNewRecordValue(fieldValueSupplier);
    setDefaultDefinedValue(fieldValueSupplier);
  }

  /**
   * Get a list of the Dependencies bits set for this LinkField.
   * 
   * @see DependenciesUtils#toTags(int)
   */
  public List<String> getLinkStyleTagNames()
  {
    return DependenciesUtils.toTags(__linkStyle);
  }

  /**
   * Create a LinkFieldFilter that has autogenerated everything. The values utilised for a
   * targetTableName of "TableName" will be:-
   * <dl>
   * <dt>linkFieldName</dt>
   * <dd>tableName_id</dd>
   * <dt>linkName</dt>
   * <dd>tableName</dd>
   * <dt>referencesName</dt>
   * <dd>null (getReferencesName())</dd>
   * <dt>reverseOrderClause</dt>
   * <dd>null (getDefaultOrdering())</dd>
   * </dl>
   * This should provide the correct format for most link usages providing the db is set up to match
   * it.
   * 
   * @param targetTableName
   *          The name of the Table which this LinkFieldFilter points at. This Table does not yet
   *          need to have been initialised as the appropriate operations will be created at the
   *          preLock() stages.
   * @param label
   *          The human readable name of this field. Utilised for automated generation of GUI items.
   * @param linkStyle
   *          The bitfield that describes the functionality of the link. This should be a
   *          combination of constants from the DependenciesConstants interface.
   */
  public LinkField(String targetTableName,
                   String label,
                   String pwd,
                   int linkStyle)
  {
    this(null,
         label,
         null,
         null,
         targetTableName,
         null,
         null,
         pwd,
         linkStyle);
    // Character.toLowerCase(targetTableName.charAt(0)) +
    // targetTableName.substring(1),
  }

  public String getReverseOrderClause()
  {
    return __reverseOrderClause;
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    try {
      if (mayResolveRecord(record, viewpoint)) {
        HtmlUtils.value(buf,
                        resolveRecord(record,
                                      viewpoint).get(getTargetTable().getTitleOperationName()),
                        namePrefix,
                        getName());
        return;
      }
    } catch (MirrorNoSuchRecordException mnsrEx) {
    }
    super.appendHtmlValue(record, viewpoint, namePrefix, buf, context);
  }

  /**
   * Get the RecordSource that contains the PersistentRecord that this LinkField could legitimately
   * point at. This is used to generate the SELECT field for the html widgets in
   * {@link #appendHtmlWidget(TransientRecord, Viewpoint, String, Appendable, Map)} . The default
   * implementation is to return all of the records on the target table and you should override this
   * method if you want a more selective choice of records.
   * 
   * @param source
   *          The record containing the link that we are interested in the potential targets
   * @see #getTargetTable()
   */
  protected RecordSource getValidTargets(TransientRecord source)
  {
    return getTargetTable();
  }

  protected String getTargetTitle(PersistentRecord target)
  {
    return target.getString(getTargetTable().getTitleOperationName());
  }

  protected String getHtmlWidgetSummary(TransientRecord record,
                                        PersistentRecord target)
  {
    return "";
  }

  /**
   * Get the text for the option that should be used for no value by
   * {@link #appendHtmlWidget(TransientRecord, Viewpoint, String, Appendable, Map)} in situations
   * when the LinkField is optional.
   * 
   * @param record
   *          The record which
   *          {@link #appendHtmlWidget(TransientRecord, Viewpoint, String, Appendable, Map)} is
   *          rendering the edit interface for.
   * @return "" by default. Override if required.
   */
  protected String getNoTargetTitle(TransientRecord record)
  {
    return "";
  }

  public boolean isOptional()
  {
    return (__linkStyle & Dependencies.BIT_OPTIONAL) != 0;
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), !isOptional(), namePrefix, getName());
    int value = getIntValue(record);
    if (isOptional()) {
      HtmlUtils.option(buf, "0", getNoTargetTitle(record), false);
    }
    for (PersistentRecord option : getValidTargets(record)) {
      int optionValue = option.getId();
      HtmlUtils.option(buf,
                       Integer.valueOf(optionValue),
                       getTargetTitle(option),
                       optionValue == value);
    }
    HtmlUtils.closeSelect(buf, getHtmlWidgetSummary(record, record.opt(__linkName, viewpoint)));
  }

  /**
   * Override this method if you have custom dependency tracking for the reverse links...
   * 
   * @return true in the default implementation
   */
  public boolean mayHaveDependencies(TransientRecord record,
                                     int transactionType)
  {
    return true;
  }

  public synchronized LinkField setIgnoreId(int id)
  {
    switch (getLockStatus()) {
      case LO_NEW:
      case LO_INITIALISED:
        _ignoreId = id;
        return this;
      default:
        throw new IllegalStateException("Cannot call " + this + ".setIgnoreId(" + id + ") in state "
                                        + LO_NAMES.get(getLockStatus()));
    }
  }

  /**
   * Define the id that this LinkFieldFilter should default to if it has BIT_AUTO_LINK defined on
   * the linkStyle.
   */
  public synchronized LinkField setDefaultTarget(int id)
  {
    switch (getLockStatus()) {
      case LO_NEW:
      case LO_INITIALISED:
        _defaultId = id;
        _defaultIdField = null;
        _defaultIdValue = null;
        return this;
      default:
        throw new IllegalStateException("Cannot call " + this + ".setDefaultTarget(" + id
                                        + ") in state " + LO_NAMES.get(getLockStatus()));
    }
  }

  public synchronized LinkField setAutoCreateInitParams(Gettable params)
  {
    _autoCreateInitParams = params;
    return this;
  }

  /**
   * Define the search terms that should be looked up at instantiation time to determine the id that
   * this LinkFieldFilter should default to if it has BIT_AUTO_LINK defined on the linkStyle.
   */
  public synchronized LinkField setDefaultTarget(FieldKey<String> field,
                                                 String value)
  {
    switch (getLockStatus()) {
      case LO_NEW:
      case LO_INITIALISED:
        _defaultId = 0;
        _defaultIdField = field;
        _defaultIdValue = value;
        return this;
      default:
        throw new IllegalStateException("Cannot call " + this + ".setDefaultTarget(" + field + ","
                                        + value + ") in state " + LO_NAMES.get(getLockStatus()));
    }
  }

  public String getTargetTableName()
  {
    return __targetTableName;
  }

  public RecordOperationKey<PersistentRecord> getLinkName()
  {
    return __linkName;
  }

  public Table getTargetTable()
  {
    return _targetTable;
  }

  /**
   * Get the name of the reverse operation.
   * 
   * @return The name of the reverse operation. This will be generated when the LinkFieldFilter is
   *         registered on its target Table and this method will return null if invoked before this
   *         case.
   */
  public final RecordOperationKey<RecordSource> getReferencesName()
  {
    return _referencesName;
  }

  public PersistentRecord resolveOptRecord(TransientRecord fromRecord,
                                           Viewpoint viewpoint)
  {
    int id = getIntValue(fromRecord);
    return id <= 0 ? null : getTargetTable().getOptRecord(id, viewpoint);
  }

  @Override
  public PersistentRecord resolveRecord(TransientRecord fromRecord,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    int targetRecordId = getIntValue(fromRecord);
    try {
      return getTargetTable().getRecord(targetRecordId, viewpoint);
    } catch (MirrorNoSuchRecordException brokenLinkEx) {
      throw new MirrorNoSuchRecordException("broken link on " + fromRecord + "." + getName(),
                                            brokenLinkEx);
    }
  }

  @Override
  public boolean mayResolveRecord(TransientRecord fromRecord,
                                  Viewpoint viewpoint)
  {
    int targetRecordId = getIntValue(fromRecord);
    if (targetRecordId <= 0) {
      return false;
    }
    return getTargetTable().hasRecord(targetRecordId);
  }

  /**
   * If the LinkFieldFilter is being initialised on its target Table then calculate the
   * referencesName from the Table.
   * 
   * @see Table#getReferencesName()
   */
  @Override
  protected void lockStatusChanged()
  {
    switch (getLockStatus()) {
      case LO_INITIALISED:
        _referencesName =
            _referencesName == null ? getTable().getReferencesName() : _referencesName;
        break;
      default:
    }
  }

  public int calculateDefaultValue()
  {
    if (_defaultId != 0) {
      return _defaultId;
    }
    if ((__linkStyle & Dependencies.BIT_AUTO_LINK) == 0) {
      return 0;
    }
    if (_targetTable == null) {
      return 0;
    }
    if (_defaultIdField == null | _defaultIdValue == null) {
      IdList ids = _targetTable.getIdList();
      return ids.size() == 0 ? 0 : ids.getInt(0);
    }
    try {
      return TableStringMatcher.firstMatch(_targetTable, _defaultIdField, _defaultIdValue).getId();
    } catch (MirrorNoSuchRecordException mnsrEx) {
      IdList ids = _targetTable.getIdList();
      return ids.size() == 0 ? 0 : ids.getInt(0);
    }
  }

  /**
   * Custom prelock operation that registers the various link traversal operations that are
   * associated with the LinkFieldFilter. These are defined as:-
   * <ul>
   * <li>ForwardLinkRecordOperation on the same Table as the field.
   * <li>ReverseLinkRecordOperation on the target Table for this field.
   * <li>If the LinkFieldFilter is defined as enforced, then it will register the following
   * additional triggers:-
   * <ul>
   * <li>LinkFieldTrigger on updates to the field.
   * <li>LinkInstantiationTrigger on the registration Table.
   * </ul>
   * </ul>
   * 
   * @see org.cord.mirror.trigger.LinkFieldTrigger
   * @see org.cord.mirror.trigger.LinkInstantiationTrigger
   */
  @Override
  public final void internalPreLock()
      throws MirrorTableLockedException
  {
    try {
      _targetTable = getTable().getDb().getTable(__targetTableName);
    } catch (NoSuchElementException nsElEx) {
      throw ExceptionUtil.initCause(new NoSuchElementException("Cannot find target table "
                                                               + __targetTableName + " for "
                                                               + getTable() + "." + getName()),
                                    nsElEx);
    }
    RecordOperation<?> forwardLink =
        new FollowLink(_targetTable, getKey(), __linkName, __linkStyle);
    getTable().addRecordOperation(forwardLink);
    forwardLink.preLock();
    RecordOperation<?> reverseLink = new FollowReferences(getTable(),
                                                          getKey(),
                                                          _referencesName,
                                                          __reverseOrderClause,
                                                          __linkStyle);
    _targetTable.addRecordOperation(reverseLink);
    reverseLink.preLock();
    if ((__linkStyle & Dependencies.BIT_ENFORCED) != 0) {
      getTable().addFieldTrigger(new LinkFieldTrigger(getKey(),
                                                      getFieldIndex(),
                                                      _targetTable,
                                                      __linkStyle,
                                                      _ignoreId,
                                                      _autoCreateInitParams));
      getTable().addPreInstantiationTrigger(new LinkInstantiationTrigger(this,
                                                                         getKey(),
                                                                         __linkName,
                                                                         getTable().getDb(),
                                                                         __transactionPassword,
                                                                         Db.DEFAULT_TIMEOUT,
                                                                         __linkStyle));
    }
    try {
      getTable().getRecordOperation(HASTARGET_NAME);
    } catch (IllegalArgumentException iaEx) {
      HasTarget hasTarget = new HasTarget();
      getTable().addRecordOperation(hasTarget);
      hasTarget.preLock();
    }
    if (((__linkStyle & Dependencies.BIT_OPTIONAL_AUTOCLEAR) != 0)
        && (__linkStyle & Dependencies.BIT_OPTIONAL) != 0) {
      _targetTable.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_PREDELETED_DELETED,
                                                            false,
                                                            _targetTable.getName()) {
        @Override
        public void trigger(PersistentRecord record,
                            Transaction deleteTransaction,
                            int state)
            throws Exception
        {
          RecordList records = record.opt(_referencesName, deleteTransaction).getRecordList();
          if (records.size() != 0) {
            try (Transaction t =
                record.getTable().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                            Transaction.TRANSACTION_UPDATE,
                                                            __transactionPassword,
                                                            "OptionalLinkTidy")) {
              for (PersistentRecord ro : records) {
                if (!deleteTransaction.contains(ro)) {
                  WritableRecord rw = t.edit(ro);
                  rw.setField(getName(), Integer.valueOf(UNDEFINEDLINK));
                }
              }
              t.commit();
            }
          }
        }

        @Override
        public String toString()
        {
          return LinkField.this.toString() + ".OptionalLinkTidy";
        }
      });
    }
    postInternalPreLock();
  }

  protected void postInternalPreLock()
      throws MirrorTableLockedException
  {
  }

  @Override
  public String toString()
  {
    return String.format("%s.%s(LINK: %s, %s, %s, %s)",
                         getTable().getName(),
                         getName(),
                         __targetTableName,
                         getNewRecordValue(),
                         getDefaultDefinedValue(null),
                         getLinkStyleTagNames());
  }

  public final static MonoRecordOperationKey<String, Boolean> HASTARGET_NAME =
      MonoRecordOperationKey.create("hasTarget", String.class, Boolean.class);

  public class HasTarget
    extends MonoRecordOperation<String, Boolean>
  {

    public HasTarget()
    {
      super(HASTARGET_NAME,
            Casters.TOSTRING);
    }

    @Override
    public String toString()
    {
      return LinkField.this.toString() + ".HasTarget";
    }

    @Override
    public Boolean calculate(TransientRecord record,
                             Viewpoint viewpoint,
                             String name)
    {
      return Boolean.valueOf(record.mayFollowLink(name, viewpoint));
    }
  }

  public class FollowLink
    extends AbstractForwardRecordOperation<Integer, PersistentRecord>
    implements RecordResolver
  {
    public FollowLink(Table targetTable,
                      FieldKey<Integer> linkFieldName,
                      RecordOperationKey<PersistentRecord> invocationName,
                      int linkStyle)
    {
      super(targetTable,
            linkFieldName,
            invocationName,
            linkStyle);
    }

    /**
     * @return The result of resolve(targetRecord,viewpoint) which is the record pointed to by this
     *         link from targetRecord. If the link is broken then null will be returned rather than
     *         throwing a MirrorNoSuchRecordException.
     * @see #resolveRecord(TransientRecord,Viewpoint)
     */
    @Override
    public final PersistentRecord getOperation(TransientRecord targetRecord,
                                               Viewpoint viewpoint)
    {
      return LinkField.this.resolveOptRecord(targetRecord, viewpoint);
    }

    @Override
    public Iterator<PersistentRecord> getDependentRecords(PersistentRecord fromRecord,
                                                          int transactionType,
                                                          Viewpoint viewpoint)
    {
      if (mayHaveDependencies(fromRecord, transactionType)) {
        PersistentRecord toRecord = LinkField.this.resolveOptRecord(fromRecord, viewpoint);
        if (toRecord != null) {
          return Iterators.singletonIterator(toRecord);
        }
      }
      return ImmutableSet.<PersistentRecord> of().iterator();
    }

    /**
     * @see LinkField#mayResolveRecord(TransientRecord, Viewpoint)
     */
    @Override
    public boolean mayResolveRecord(TransientRecord fromRecord,
                                    Viewpoint viewpoint)
    {
      return LinkField.this.mayResolveRecord(fromRecord, viewpoint);
    }

    @Override
    public String toString()
    {
      return LinkField.this.toString() + ".FollowLink";
    }

    @Override
    public PersistentRecord resolveRecord(TransientRecord fromRecord,
                                          Viewpoint viewpoint)
        throws MirrorNoSuchRecordException
    {
      return LinkField.this.resolveRecord(fromRecord, viewpoint);
    }
  }

  public class FollowReferences
    extends AbstractReverseRecordOperation<Integer>
  {
    private final String __filterHead;

    public FollowReferences(Table linkTable,
                            FieldKey<Integer> linkFieldName,
                            RecordOperationKey<RecordSource> invocationName,
                            String targetOrderClause,
                            int linkStyle)
    {
      super(linkTable,
            linkFieldName,
            invocationName,
            targetOrderClause,
            linkStyle,
            LinkField.this);
      StringBuilder filterHead = new StringBuilder();
      filterHead.append('(')
                .append(getLinkTable().getName())
                .append('.')
                .append(getLinkFieldName())
                .append('=');
      __filterHead = filterHead.toString();
    }

    @Override
    protected String buildFilter(TransientRecord record)
    {
      StringBuilder filter = new StringBuilder(32);
      filter.append(__filterHead).append(record.getId()).append(')');
      return filter.toString();
    }

    @Override
    public String toString()
    {
      return LinkField.this.toString() + ".FollowReferences";
    }
  }

  /**
   * Check to see whether or not the supplied Object is a PersistentRecord from the Table that is
   * being pointed at by this link and if so use the id of the record as the value, otherwise
   * delegate to the parent and look for an Integer as per normal.
   * 
   * @see IntegerField#validateDefinedValue(TransientRecord, Object)
   */
  @Override
  public Integer validateDefinedValue(TransientRecord record,
                                      Object obj)
      throws MirrorFieldException
  {
    if (obj instanceof PersistentRecord) {
      PersistentRecord target = (PersistentRecord) obj;
      if (getTargetTable().equals(target.getTable())) {
        return Integer.valueOf(target.getId());
      }
    }
    return super.validateDefinedValue(record, obj);
  }

  @Override
  public Integer objectToValue(TransientRecord record,
                               Object obj)
  {
    if (obj instanceof PersistentRecord) {
      PersistentRecord target = (PersistentRecord) obj;
      if (getTargetTable().equals(target.getTable())) {
        return Integer.valueOf(target.getId());
      }
      throw new IllegalArgumentException(String.format("%s is not an instance of %s and therefore cannot be used in %s",
                                                       target,
                                                       getTargetTable(),
                                                       this));
    }
    return super.objectToValue(record, obj);
  }

}
