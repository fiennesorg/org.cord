package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;

import com.google.common.base.MoreObjects;

public class TimeField
  extends ObjectField<Long>
{
  private static final SimpleDateFormat FORMAT_HHMMSS = new SimpleDateFormat("HH:mm:ss");
  private static final SimpleDateFormat FORMAT_HHMM = new SimpleDateFormat("HH:mm");

  private static String format(SimpleDateFormat format,
                               Date date)
  {
    if (date == null) {
      return null;
    }
    synchronized (format) {
      return format.format(date);
    }
  }

  public enum Accuracy {
    Minutes {
      @Override
      public String format(Date date)
      {
        return TimeField.format(FORMAT_HHMM, date);
      }
      @Override
      public long calculateTime(long h,
                                long m,
                                long s)
      {
        return TimeField.calculateTime(h, m, 0);
      }
    },
    Seconds {
      @Override
      public String format(Date date)
      {
        return TimeField.format(FORMAT_HHMMSS, date);
      }
      @Override
      public long calculateTime(long h,
                                long m,
                                long s)
      {
        return TimeField.calculateTime(h, m, s);
      }
    };
    public abstract String format(Date date);
    public abstract long calculateTime(long h,
                                       long m,
                                       long s);
  }

  private final Accuracy __accuracy;

  public TimeField(FieldKey<Long> fieldName,
                   String label,
                   Accuracy accuracy,
                   boolean supportsNull,
                   FieldValueSupplier<Long> newRecordValue,
                   FieldValueSupplier<Long> defaultDefinedValue)
  {
    super(fieldName,
          label,
          supportsNull,
          Long.class,
          newRecordValue,
          defaultDefinedValue);
    __accuracy = MoreObjects.firstNonNull(accuracy, Accuracy.Minutes);
  }

  public TimeField(FieldKey<Long> fieldName,
                   String label,
                   Accuracy accuracy,
                   FieldValueSupplier<Long> newRecordValue,
                   FieldValueSupplier<Long> defaultDefinedValue)
  {
    this(fieldName,
         label,
         accuracy,
         true,
         newRecordValue,
         defaultDefinedValue);
  }

  public TimeField(FieldKey<Long> fieldName,
                   String label,
                   Accuracy accuracy,
                   FieldValueSupplier<Long> defaultDefinedValue)
  {
    this(fieldName,
         label,
         accuracy,
         false,
         defaultDefinedValue,
         defaultDefinedValue);
  }

  public Accuracy getAccuracy()
  {
    return __accuracy;
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.textWidget(null,
                         buf,
                         getAccuracy().format(new Date(getFieldValue(record).longValue())),
                         namePrefix,
                         getName());
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Long value)
  {
    buffer.append(value);
  }

  public static final Pattern HHMM = Pattern.compile("([0-2][0-9]):([0-5][0-9])");

  public static final Pattern HHMMSS = Pattern.compile("([0-2][0-9]):([0-5][0-9]):([0-5][0-9])");

  private static long calculateTime(long h,
                                    long m,
                                    long s)
  {
    return ((((h - 1) * 60) + m) * 60 + s) * 1000;
  }

  @Override
  public Long validateDefinedValue(TransientRecord record,
                                   Object obj)
      throws MirrorFieldException, TypeValidationException
  {
    Long value = objectToValue(record, obj);
    Long constrained = constrainValue(value);
    return constrained;
  }

  @Override
  public Long objectToValue(TransientRecord record,
                            Object obj)
  {
    if (obj instanceof Long) {
      return (Long) obj;
    }
    if (obj instanceof Number) {
      return Long.valueOf(((Number) obj).longValue());
    }
    if (obj instanceof Date) {
      return Long.valueOf(((Date) obj).getTime());
    }
    String s = obj.toString();
    Matcher hhmmss = HHMMSS.matcher(s);
    if (hhmmss.matches()) {
      return Long.valueOf(getAccuracy().calculateTime(Long.parseLong(hhmmss.group(1)),
                                                      Long.parseLong(hhmmss.group(2)),
                                                      Long.parseLong(hhmmss.group(3))));

    }
    Matcher hhmm = HHMM.matcher(s);
    if (hhmm.matches()) {
      return Long.valueOf(getAccuracy().calculateTime(Long.parseLong(hhmm.group(1)),
                                                      Long.parseLong(hhmm.group(2)),
                                                      0));
    }
    return Long.valueOf(s);
  }

  public static final long MINVALUE;
  public static final long MAXVALUE;

  public static final int EPOCH_YEAR = 1970;
  public static final int EPOCH_MONTH = Calendar.JANUARY;
  public static final int EPOCH_DAY = 1;

  private static final Calendar c = Calendar.getInstance();

  static {
    c.clear();
    c.set(EPOCH_YEAR, EPOCH_MONTH, EPOCH_DAY, 0, 0, 0);
    MINVALUE = c.getTimeInMillis();
    c.clear();
    c.set(EPOCH_YEAR, EPOCH_MONTH, EPOCH_DAY, 23, 59, 59);
    MAXVALUE = c.getTimeInMillis();
  }

  public static Long constrainValue(Long value)
  {
    final long original = value.longValue();
    long v = original;
    synchronized (c) {
      c.setTimeInMillis(v);
      c.set(EPOCH_YEAR, EPOCH_MONTH, EPOCH_DAY);
      c.set(Calendar.MILLISECOND, 0);
      v = c.getTimeInMillis();
    }
    return Long.valueOf(v);
  }

  @Override
  public Long dbToValue(Object v,
                        Object[] values,
                        int[] intValues)
  {
    return (Long) v;
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(LongField.ISVALIDSQL, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Long value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.longValue());
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }

}
