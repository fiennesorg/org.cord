package org.cord.mirror.field;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.EmailValidator;
import org.cord.util.HtmlUtils;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;

/**
 * FieldFilter that only permits valid email addresses to be stored in it.
 * 
 * @see EmailValidator
 */
public class EmailField
  extends StringField
{

  public EmailField(ObjectFieldKey<String> fieldName,
                    String label,
                    String defaultEmailAddress)
  {
    super(fieldName,
          label,
          StringField.LENGTH_VARCHAR,
          defaultEmailAddress);
  }

  public EmailField(ObjectFieldKey<String> fieldName,
                    String label,
                    String newRecordAddress,
                    String defaultDefinedAddress)
  {
    super(fieldName,
          label,
          StringField.LENGTH_VARCHAR,
          newRecordAddress,
          defaultDefinedAddress);
  }

  private final EmailValidator getEmailValidator()
  {
    return EmailValidator.getDottedInstance();
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    super.appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    EmailValidator validator = getEmailValidator();
    String fieldValue = getFieldValue(record);
    if (Strings.isNullOrEmpty(fieldValue)) {
      HtmlUtils.errorValue(buf, "Email not defined", getName() + "error");
    } else {
      String totalError = null;
      for (String email : Splitter.on(',').split(fieldValue)) {
        String error = validator.getError(email);
        if (error != null) {
          error = '"' + email.trim() + "\" - " + error;
          if (totalError == null) {
            totalError = error;
          } else {
            totalError += ", " + error;
          }
        }
      }
      if (totalError != null) {
        HtmlUtils.errorValue(buf, "Invalid email: " + totalError, getName() + "error");
      }
    }
  }
}
