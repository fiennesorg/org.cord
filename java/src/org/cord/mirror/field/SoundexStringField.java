package org.cord.mirror.field;

import org.cord.mirror.ObjectFieldKey;

public class SoundexStringField
  extends StringField
{
  public SoundexStringField(ObjectFieldKey<String> fieldName)
  {
    this(fieldName,
         4);
  }

  public SoundexStringField(ObjectFieldKey<String> fieldName,
                            int maxLength)
  {
    super(fieldName,
          null,
          maxLength,
          "");
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    String sqlValue)
  {
    buffer.append("SUBSTRING(SOUNDEX(");
    super.appendDefinedSqlValue(buffer, sqlValue);
    buffer.append("),1,").append(getMaxLength()).append(')');
  }
}
