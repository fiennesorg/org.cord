package org.cord.mirror.field;

import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.ForwardBitLinkRecordOperation;
import org.cord.mirror.operation.IsSelectedChoiceFrom;
import org.cord.mirror.operation.ReverseBitLinkRecordOperation;
import org.cord.mirror.operation.SelectedChoicesFrom;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.mirror.trigger.BitLinkFieldTrigger;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntListIterator;

/**
 * Extension of the LongFieldFilter that uses the 63 bits as independant choices referencing a
 * TargetTable.
 * <UL>
 * <LI>$linkTableName
 * <LI>$linkTable.linkFieldName --> Long
 * <LI>$linkTable.linkOperationName --> records from $targetTable ordered by $targetOrder
 * <LI>$targetTableName
 * <LI>$targetTable.targetOperationName --> records from $linkTable ordered by $linkOrder
 * </UL>
 */
public class BitLinkField
  extends LongField
{
  public final static int MINIMUM_CHOICE = 1;

  public final static int MAXIMUM_CHOICE = 63;

  private final static long[] BIT_MASKS = new long[MAXIMUM_CHOICE];
  static {
    for (int i = 0; i < MAXIMUM_CHOICE; i++) {
      BIT_MASKS[i] = 1l << i;
    }
  }

  private final RecordOperationKey<RecordSource> _linkOperationName;

  private final String _targetTableName;

  private final RecordOperationKey<RecordSource> _targetOperationName;

  private final int _linkStyle;

  /**
   * Create a new filter.
   * 
   * @param linkFieldName
   *          The name of the field that contains the bitField.
   * @param targetTableName
   *          The name of the table that the link field references.
   * @param targetOperationName
   *          The name of the operation on records from targetTable that performs the reverse
   *          resolution.
   * @param linkStyle
   *          The dependencies constraints on this link as defined in Dependencies
   * @see Dependencies
   */
  public BitLinkField(ObjectFieldKey<Long> linkFieldName,
                      RecordOperationKey<RecordSource> linkOperationName,
                      String targetTableName,
                      RecordOperationKey<RecordSource> targetOperationName,
                      int linkStyle)
  {
    this(linkFieldName,
         linkOperationName,
         targetTableName,
         targetOperationName,
         linkStyle,
         0l);
  }

  public BitLinkField(ObjectFieldKey<Long> linkFieldName,
                      RecordOperationKey<RecordSource> linkOperationName,
                      String targetTableName,
                      RecordOperationKey<RecordSource> targetOperationName,
                      int linkStyle,
                      long defaultValue)
  {
    super(linkFieldName,
          null,
          Long.valueOf(defaultValue));
    _linkOperationName = linkOperationName;
    _targetTableName = targetTableName;
    _targetOperationName = targetOperationName;
    _linkStyle = linkStyle;
  }

  private Table _targetTable = null;

  /**
   * @return The table referred to by this filter or null if initialise(table) has not yet been
   *         called.
   */
  public final Table getTargetTable()
  {
    return _targetTable;
  }

  /**
   * Perform internal housekeeping. Namely:-
   * <ul>
   * <li>Resolve targetTableName from the creator into a Table reference.
   * </ul>
   */
  @Override
  public void internalPreLock()
      throws MirrorTableLockedException
  {
    _targetTable = getTable().getDb().getTable(_targetTableName);
    RecordOperation<?> forwardOperation =
        new ForwardBitLinkRecordOperation(_targetTable, getKey(), _linkOperationName, _linkStyle);
    getTable().addRecordOperation(forwardOperation);
    forwardOperation.preLock();
    try {
      getTable().addRecordOperation(new IsSelectedChoiceFrom());
      getTable().addRecordOperation(new SelectedChoicesFrom());
    } catch (IllegalArgumentException duplicateInitEx) {
    }
    RecordOperation<?> reverseOperation = new ReverseBitLinkRecordOperation(getTable(),
                                                                            getKey(),
                                                                            _targetOperationName,
                                                                            null,
                                                                            _linkStyle);
    _targetTable.addRecordOperation(reverseOperation);
    reverseOperation.preLock();
    if ((_linkStyle & Dependencies.BIT_ENFORCED) != 0) {
      getTable().addFieldTrigger(new BitLinkFieldTrigger(getKey(), _targetTable));
    }
  }

  /**
   * Get the value for the BitField that corresponds to the given flag index. This is equivalent to
   * 2<sup>(flagIndex-1)</sup>. However, this method utilisises a lookup field to ensure rapid
   * evaluation.
   * 
   * @param flagIndex
   *          The index of the flag whose bit mask is required.
   * @throws ArrayIndexOutOfBoundsException
   *           if ! (MINIMUM_CHOICE <= flagIndex <= MAXIMUM_CHOICE)
   * @return the bit mask for the flagIndex (1 << (flagIndex - 1)).
   */
  public final static long getBitMask(int flagIndex)
  {
    return BIT_MASKS[flagIndex - 1];
  }

  /**
   * Static method to either set or unset a bit in a long int binary field.
   * 
   * @param fieldValue
   *          The binary field.
   * @param flagIndex
   *          The bit that is to be changed. A value of 0 will be ignored.
   * @param flagValue
   *          The target value of the bit.
   * @return The value of fieldValue with the bit updated (if required).
   * @see #getBitMask(int)
   */
  public final static long setFlag(long fieldValue,
                                   int flagIndex,
                                   boolean flagValue)
  {
    if (flagIndex > 0) {
      long flagBit = getBitMask(flagIndex);
      if (flagValue) {
        return fieldValue | flagBit;
      } else {
        return fieldValue & (~flagBit);
      }
    } else {
      return fieldValue;
    }
  }

  /**
   * Static method to get the status of a specific bit in a long int binary field.
   * 
   * @param fieldValue
   *          The binary field.
   * @param flagIndex
   *          The bit that is to be read.
   * @return The value of the bit.
   * @see #getBitMask(int)
   */
  public final static boolean isSet(long fieldValue,
                                    int flagIndex)
  {
    long flagBit = getBitMask(flagIndex);
    return (fieldValue & flagBit) > 0;
  }

  /**
   * Static method to get the status of a specific bit in a given record.
   * 
   * @param targetRecord
   *          The record to retrieve the bitfield from
   * @param fieldName
   *          The name of the bitfield.
   * @param flagIndex
   *          The number of the flag to retrieve from the bit field.
   * @return The value of the bit.
   */
  public final static boolean isSet(TransientRecord targetRecord,
                                    Object fieldName,
                                    int flagIndex)
  {
    return isSet(((Long) targetRecord.get(fieldName)).longValue(), flagIndex);
  }

  /**
   * Static method to set the status of a specified bit in a given record. It is up to the client to
   * ensure that the record is supplied in an updateable state. The setField operation will only be
   * invoked in situations when flagValue is different from the existing value.
   * 
   * @param targetRecord
   *          The record on which the field is to be updated.
   * @param fieldName
   *          The name of the field to be updated
   * @param flagIndex
   *          The index number of the flag (MINIMUM_CHOICE <= flagIndex <= MAXIMUM_CHOICE)
   * @param flagValue
   *          The desired status of the flag.
   * @throws MirrorFieldException
   *           If the targetted record rejects the value.
   * @see #MINIMUM_CHOICE
   * @see #MAXIMUM_CHOICE
   */
  public final static void setFlag(TransientRecord targetRecord,
                                   FieldKey<Long> fieldName,
                                   int flagIndex,
                                   boolean flagValue)
      throws MirrorFieldException
  {
    long currentValue = targetRecord.comp(fieldName).longValue();
    if (flagValue != isSet(currentValue, flagIndex)) {
      targetRecord.setField(fieldName, Long.valueOf(setFlag(currentValue, flagIndex, flagValue)));
    }
  }

  /**
   * Field-based instance method to set the status of the the specified bit in a given record. It is
   * up to the client to ensure that the record is supplied in an updateable state.
   * 
   * @param targetRecord
   *          The record that is to be udpated
   * @param flagIndex
   *          The index of the flag (MINIMUM_CHOICE <= flagIndex <= MAXIMUM_CHOIDE)
   * @param flagValue
   *          The desired value for the flag.
   * @see #setFlag(TransientRecord,FieldKey,int,boolean)
   * @see #MINIMUM_CHOICE
   * @see #MAXIMUM_CHOICE
   */
  public final void setFlag(TransientRecord targetRecord,
                            int flagIndex,
                            boolean flagValue)
      throws MirrorFieldException
  {
    setFlag(targetRecord, getKey(), flagIndex, flagValue);
  }

  /**
   * Static method to get a ListIterator of the ids of the flags that are set to positive in the
   * given value. The ListIterator will be ordered from lowest index to highest.
   * 
   * @param fieldValue
   *          The value which is to be iterated across.
   * @return Iterator of Integer objects
   */
  public final static IntListIterator getSetFlagIds(long fieldValue)
  {
    IntList setFlags = new IntArrayList();
    for (int flagIndex = MINIMUM_CHOICE; flagIndex <= MAXIMUM_CHOICE; flagIndex++) {
      if (isSet(fieldValue, flagIndex)) {
        setFlags.add(flagIndex);
      }
    }
    return setFlags.listIterator();
  }

  /**
   * Static method to get a ListIterator of the set ids in a named field on a given record.
   * 
   * @param sourceRecord
   *          The record containing the field
   * @param fieldName
   *          The name of the field to iterate across.
   * @return Iterator of Integers
   * @see #getSetFlagIds(long)
   */
  public final static IdList getSetFlagIds(TransientRecord sourceRecord,
                                           FieldKey<Long> fieldName)
  {
    long value = sourceRecord.comp(fieldName).longValue();
    ArrayIdList ids = new ArrayIdList(sourceRecord.getTable());
    for (int i = MINIMUM_CHOICE; i <= MAXIMUM_CHOICE; i++) {
      if (isSet(value, i)) {
        ids.add(i);
      }
    }
    return ids;
  }

  /**
   * Static method to get a ListIterator of the target records referenced by a named field in a
   * given record. The action of retrieving these records is performed with Cache.GET_NO_REORDER to
   * optimise the retrieval process as much as possible. This is generally safe because we are
   * limited to a choice of 63 records and as such are allowed to assume that the entire records
   * will be rapidly cached and therefore the benefits of re-ordering are minimal.
   * 
   * @param sourceRecord
   *          The record containing the field to be iterated across
   * @param fieldName
   *          The name of the field in the sourceRecord containing the BitLinkFieldFilter.
   * @param referencedTable
   *          The Table that the referenced records are to be looked up in.
   * @param transaction
   *          The viewpoint onto the operation.
   * @return Iterator of PersistentRecord Objects. Note that this Iterator is in fact a
   *         RecordListIterator which will throw ConcurrentModificationExceptions if the targetted
   *         Records do not exist. As such, if you don't have a LINK_ENFORCED style link then it may
   *         be worth retrieving the records yourself from the Integer FlagIds.
   * @see PersistentRecord
   */
  public final static RecordSource getSetFlags(TransientRecord sourceRecord,
                                               FieldKey<Long> fieldName,
                                               Table referencedTable,
                                               Viewpoint transaction)
  {
    return new ConstantRecordSource(UnmodifiableIdList.getInstance(getSetFlagIds(sourceRecord,
                                                                                 fieldName)),
                                    null);
  }

  /**
   * Instance based message to resolve a given record into its referenced records. This utilises the
   * targetTable and fieldName from the constructor.
   * 
   * @param sourceRecord
   *          The record containing the BitLinkFieldFilter
   * @param transaction
   *          The viewpoint onto the operation.
   * @return Iterator of PersistentRecord Objects. Note that this Iterator is in fact a
   *         RecordListIterator which will throw ConcurrentModificationExceptions if the targetted
   *         Records do not exist. As such, if you don't have a LINK_ENFORCED style link then it may
   *         be worth retrieving the records yourself from the Integer FlagIndss.
   * @see PersistentRecord
   */
  public final RecordSource getSetFlags(TransientRecord sourceRecord,
                                        Viewpoint transaction)
  {
    return getSetFlags(sourceRecord, getKey(), _targetTable, transaction);
  }
}
