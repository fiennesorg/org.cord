package org.cord.mirror.field;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.EnglishNamed;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.IsoCountryCode;

import com.google.common.collect.ImmutableSet;

public class IsoCountryField
  extends StringField
{
  private List<IsoCountryCode> _proposedValues = null;

  private final EnglishNamedImpl __emptyValue;

  public IsoCountryField(ObjectFieldKey<String> fieldName,
                         IsoCountryCode defaultValue,
                         String emptyValueName)
  {
    super(fieldName,
          null,
          IsoCountryCode.LENGTH,
          defaultValue == null ? "" : defaultValue.getName());
    __emptyValue = (emptyValueName == null ? null : new EnglishNamedImpl("", emptyValueName));
  }

  public void addProposedValue(IsoCountryCode countryCode)
  {
    if (_proposedValues == null) {
      _proposedValues = new ArrayList<IsoCountryCode>();
    }
    if (_proposedValues.contains(countryCode)) {
      return;
    }
    _proposedValues.add(countryCode);
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String proposedValue = super.validateDefinedValue(record, obj).toUpperCase();
    if ("".equals(proposedValue)) {
      if (__emptyValue == null) {
        return getDefaultDefinedValue(record);
      } else {
        return "";
      }
    }
    if (IsoCountryCode.getInstance(proposedValue) != null) {
      return proposedValue;
    }
    return getDefaultDefinedValue(record);
  }

  @Override
  public String expand(TransientRecord targetRecord)
  {
    String code = getFieldValue(targetRecord);
    if (code == null) {
      return null;
    }
    return IsoCountryCode.getInstance(code).getEnglishName();
  }

  @Override
  public Iterator<IsoCountryCode> values()
  {
    return IsoCountryCode.getCountryCodes().iterator();
  }

  @Override
  public boolean hasValues()
  {
    return true;
  }

  @Override
  public Iterator<IsoCountryCode> proposedValues()
  {
    return _proposedValues == null
        ? ImmutableSet.<IsoCountryCode> of().iterator()
        : _proposedValues.iterator();
  }

  @Override
  public EnglishNamed getEmptyValue()
  {
    return __emptyValue;
  }
}
