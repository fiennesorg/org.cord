package org.cord.mirror.field;

import java.util.Date;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;
import org.cord.util.DateQuantiser;
import org.cord.util.DateQuantiser.Granularity;

import com.google.common.base.Preconditions;

public class DateSupplierQuantised
  implements FieldValueSupplier<Date>
{
  public static FieldValueSupplier<Date> getInstance(FieldValueSupplier<Date> source,
                                                     Granularity granularity)
  {
    Preconditions.checkNotNull(source, "source");
    if (granularity == null) {
      return source;
    }
    switch (granularity) {
      case MILLISECOND:
        return source;
      default:
        return new DateSupplierQuantised(source, granularity);
    }
  }

  private final FieldValueSupplier<Date> __source;
  private final DateQuantiser __quantiser = new DateQuantiser();
  private final Granularity __granularity;

  public DateSupplierQuantised(FieldValueSupplier<Date> source,
                               Granularity granularity)
  {
    __source = Preconditions.checkNotNull(source, "source");
    __granularity = granularity;
  }

  public Granularity getGranularity()
  {
    return __granularity;
  }

  @Override
  public Date supplyValue(TransientRecord record)
  {
    return __quantiser.quantise(__source.supplyValue(record), __granularity);
  }

}
