package org.cord.mirror.field;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;

/**
 * @deprecated until we get the bugs with initialisation sorted out
 */
@Deprecated
public class MapStringField
  extends MapField<String>
{
  /**
   * Create a new optional MapStringField with the given value as the defaultDefinedValue and null
   * as the newRecordValue.
   * 
   * @param defaultSql
   *          The SQL version of the default value
   * @param defaultValue
   *          The default value.
   */
  public static MapStringField newOptMapStringField(FieldKey<String> fieldName,
                                                    String label,
                                                    String defaultSql,
                                                    String defaultValue)
  {
    MapStringField f = new MapStringField(fieldName,
                                          label,
                                          String.class,
                                          null,
                                          FieldValueSuppliers.ofDefined(defaultValue));
    f.addMapping(defaultSql, defaultValue);
    return f;
  }

  /**
   * Create a new compulsory MapStringField with the given value as the defaultDefinedValue
   * 
   * @param defaultSql
   *          The SQL version of the default value
   * @param defaultValue
   *          The default value.
   */
  public static MapStringField newMapStringField(FieldKey<String> fieldName,
                                                 String label,
                                                 String defaultSql,
                                                 String defaultValue)
  {
    MapStringField f = new MapStringField(fieldName,
                                          label,
                                          String.class,
                                          FieldValueSuppliers.ofDefined(defaultValue));
    f.addMapping(defaultSql, defaultValue);
    return f;
  }

  public MapStringField(FieldKey<String> fieldName,
                        String label,
                        Class<String> valueClass,
                        FieldValueSupplier<String> newRecordValue,
                        FieldValueSupplier<String> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public MapStringField(FieldKey<String> fieldName,
                        String label,
                        Class<String> valueClass,
                        FieldValueSupplier<String> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          defaultDefinedValue);
  }

  @Override
  public String objectToValue(TransientRecord record,
                              Object obj)
  {
    if (obj == null) {
      return null;
    }
    String s = obj.toString();
    String value = getValues().get(s);
    if (value != null) {
      return value;
    }
    if (getValues().values().contains(s)) {
      return s;
    }
    throw new ClassCastException(String.format("No mapping found for %s to %s",
                                               obj,
                                               getValueClass()));
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
      throws MirrorFieldException, TypeValidationException
  {
    String sql = obj.toString();
    if ("".equals(sql)) {
      return getDefaultDefinedValue(record);
    }
    String value = getValues().get(sql);
    if (value != null) {
      return value;
    }
    throw new TypeValidationException(String.format("Cannot resolve %s into a %s",
                                                    obj,
                                                    getValueClass()),
                                      null,
                                      record,
                                      getName(),
                                      obj);
  }

  public static final String GETTABLE_NODE_KEYS = ".node.keys";
  public static final String GETTABLE_APP_KEYS = ".app.keys";
  public static final String GETTABLE_SQL = ".sql";
  public static final String GETTABLE_VALUE = ".value";

  public int addMappings(Gettable gettable,
                         String namespace)
  {
    int c = 0;
    c += addMappings(gettable, namespace, GETTABLE_NODE_KEYS);
    c += addMappings(gettable, namespace, GETTABLE_APP_KEYS);
    return c;
  }

  public int addMappings(Gettable gettable,
                         String namespace,
                         String keysName)
  {
    String keys = gettable.getString(namespace + keysName);
    if (keys == null) {
      return 0;
    }
    String[] splitKeys = keys.split(",");
    int c = 0;
    for (String key : splitKeys) {
      key = key.trim();
      if (key.length() > 0) {
        String keyspace = namespace + "." + key;
        String sql = gettable.getString(keyspace + GETTABLE_SQL);
        String value = gettable.getString(keyspace + GETTABLE_VALUE);
        DebugConstants.DEBUG_OUT.println(String.format("%s.%s: %s --> %s",
                                                       getTable().getName(),
                                                       getName(),
                                                       sql,
                                                       value));
        addMapping(sql, value);
        c++;
      }
    }
    return c;
  }
}
