package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;
import org.cord.util.LogicException;
import org.cord.util.StringUtils;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableBiMap;

/**
 * Field that maps an Enum<T> onto an Integer value.
 * 
 * @author alex
 */
public class EnumIntegerField<T extends Enum<T>>
  extends ObjectField<T>
{
  public static <T extends Enum<T>> ObjectFieldKey<T> createKey(String name,
                                                                Class<T> enumClass)
  {
    return new ObjectFieldKey<T>(name, enumClass);
  }

  /**
   * Check that there is an Integer index for each possible value in the given Enum class and throw
   * an {@link IllegalArgumentException} if not.
   * 
   * @return index if it passes the test
   * @throws IllegalArgumentException
   *           if there are values of T that are not contained in index.
   */
  @SuppressWarnings("unchecked")
  public static <T extends Enum<T>> ImmutableBiMap<T, Integer> validateIndex(Class<T> enumClass,
                                                                             ImmutableBiMap<T, Integer> index)
  {
    Preconditions.checkNotNull(index);
    try {
      for (T t : (T[]) enumClass.getMethod("values").invoke(null)) {
        Preconditions.checkArgument(index.get(t) != null, "No index found for %s", t);
        System.err.println(t);
      }
    } catch (Exception e) {
      throw new LogicException("Cannot get access to " + enumClass + " contents", e);
    }
    return index;
  }

  private final ImmutableBiMap<T, Integer> __index;

  public EnumIntegerField(ObjectFieldKey<T> fieldKey,
                          String label,
                          ImmutableBiMap<T, Integer> index,
                          FieldValueSupplier<T> newRecordValue,
                          FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          fieldKey.getValueClass(),
          newRecordValue,
          defaultDefinedValue);
    __index = validateIndex(fieldKey.getValueClass(), index);
  }

  public static <T extends Enum<T>> EnumIntegerField<T> createOpt(ObjectFieldKey<T> fieldKey,
                                                                  String label,
                                                                  ImmutableBiMap<T, Integer> index,
                                                                  T newRecordValue,
                                                                  T defaultDefinedValue)
  {
    return new EnumIntegerField<T>(fieldKey,
                                   label,
                                   index,
                                   FieldValueSuppliers.of(newRecordValue),
                                   FieldValueSuppliers.ofDefined(defaultDefinedValue));
  }

  public EnumIntegerField(ObjectFieldKey<T> fieldKey,
                          String label,
                          ImmutableBiMap<T, Integer> index,
                          FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          fieldKey.getValueClass(),
          defaultDefinedValue);
    __index = validateIndex(fieldKey.getValueClass(), index);
  }

  public static <T extends Enum<T>> EnumIntegerField<T> createComp(ObjectFieldKey<T> fieldKey,
                                                                   String label,
                                                                   ImmutableBiMap<T, Integer> index,
                                                                   T defaultDefinedValue)
  {
    return new EnumIntegerField<T>(fieldKey,
                                   label,
                                   index,
                                   FieldValueSuppliers.ofDefined(defaultDefinedValue));
  }

  @Override
  public T objectToValue(TransientRecord record,
                         Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (getValueClass().isInstance(obj)) {
      return getValueClass().cast(obj);
    }
    T value = Enum.valueOf(getValueClass(), obj.toString());
    if (value == null) {
      throw new ClassCastException(String.format("It is not possible to convert %s into a %s",
                                                 obj,
                                                 getValueClass()));
    }
    return value;
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    T value)
  {
    buffer.append(__index.get(value));
  }

  @Override
  public T dbToValue(Object v,
                     Object[] values,
                     int[] intValues)
  {
    return __index.inverse().get(v);
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       T value)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), __index.get(value).intValue());
  }

  @Override
  public T validateDefinedValue(TransientRecord record,
                                Object value)
      throws MirrorFieldException, TypeValidationException
  {
    if (getValueClass().isInstance(value)) {
      return getValueClass().cast(value);
    }
    T enumValue = Enum.valueOf(getValueClass(), StringUtils.toString(value));
    if (enumValue == null) {
      enumValue = getDefaultDefinedValue(record);
    }
    return enumValue;
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(IntegerField.ISVALIDSQL, sqlDef);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    T currentValue = getFieldValue(record);
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    for (T value : getDefaultDefinedValue(record).getDeclaringClass().getEnumConstants()) {
      HtmlUtils.option(buf, value.name(), value.toString(), Objects.equal(currentValue, value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

}
