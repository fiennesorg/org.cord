package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * Representation of a java.lang.Double in an SQL field.
 */
public class DoubleField
  extends ObjectField<Double>
{
  // private final Double __defaultValue;

  public static ObjectFieldKey<Double> createKey(String name)
  {
    return ObjectFieldKey.create(name, Double.class);
  }

  public final static Double DEFAULT_VALUE = new Double(0);

  /**
   * Create a named DoubleField with the default value.
   * 
   * @param fieldName
   *          The name of the field filter
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @see #DEFAULT_VALUE
   */
  public DoubleField(ObjectFieldKey<Double> fieldName,
                     String label)
  {
    this(fieldName,
         label,
         DEFAULT_VALUE);
  }

  /**
   * Create a named DoubleField with a given default value.
   * 
   * @param fieldName
   *          The SQL table field that this FieldFilter reflects
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param defaultValue
   *          The initial value that instances of this Field should default to.
   */
  public DoubleField(ObjectFieldKey<Double> fieldName,
                     String label,
                     Double defaultValue)
  {
    super(fieldName,
          label,
          Double.class,
          FieldValues.ofNotNull(defaultValue));
  }

  public DoubleField(ObjectFieldKey<Double> key,
                     String label,
                     FieldValueSupplier<Double> newRecordValue,
                     FieldValueSupplier<Double> defaultDefinedValue)
  {
    super(key,
          label,
          Double.class,
          newRecordValue,
          defaultDefinedValue);
  }

  public DoubleField(ObjectFieldKey<Double> key,
                     String label,
                     Double newRecordValue,
                     Double defaultDefinedValue)
  {
    this(key,
         label,
         FieldValues.of(newRecordValue),
         FieldValues.ofNotNull(defaultDefinedValue));
  }

  public static Double castValue(Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Double) {
      return (Double) obj;
    }
    if (obj instanceof Number) {
      return Double.valueOf(((Number) obj).doubleValue());
    }
    return Double.valueOf(obj.toString());
  }

  @Override
  public Double objectToValue(TransientRecord record,
                              Object obj)
  {
    return castValue(obj);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Double sqlValue)
  {
    buffer.append(sqlValue);
  }

  @Override
  public Double validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    if (obj instanceof Double) {
      return (Double) obj;
    }
    if (obj instanceof Number) {
      return Double.valueOf(((Number) obj).doubleValue());
    }
    try {
      return Double.valueOf(obj.toString());
    } catch (NumberFormatException nfeEx) {
      return getDefaultDefinedValue(record);
    }
  }

  @Override
  public Double dbToValue(Object v,
                          Object[] values,
                          int[] intValues)
  {
    return (Double) v;
  }

  @Override
  public String toString()
  {
    return String.format("%s(DOUBLE, %s)", getName(), getDefaultDefinedValue(null));
  }

  public static final Predicate<String> ISVALIDSQLDEF = Predicates.equalTo("double");

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(ISVALIDSQLDEF, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Double value)
      throws SQLException
  {
    preparedStatement.setDouble(getSqlFieldIndex(), value.doubleValue());
  }

  @Override
  protected int getSqlType()
  {
    return Types.DOUBLE;
  }
}
