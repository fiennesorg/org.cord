package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class OptionalStringField
  extends OptionalTextInputField<String>
{
  private boolean _isTextAreaWidget = false;

  public static ObjectFieldKey<Optional<String>> createKey(String key)
  {
    return ObjectFieldKey.create(key, ObjectUtil.<Optional<String>> castClass(Optional.class));
  }

  public static final FieldValueSupplier<Optional<String>> NULL =
      FieldValueSuppliers.of(Optional.<String> absent());

  private final int __maxLength;

  public OptionalStringField(ObjectFieldKey<Optional<String>> fieldKey,
                             String label,
                             int maxLength,
                             FieldValueSupplier<Optional<String>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
    __maxLength = maxLength;
  }

  public OptionalStringField(ObjectFieldKey<Optional<String>> fieldKey,
                             String label,
                             FieldValueSupplier<Optional<String>> newRecordValue)
  {
    this(fieldKey,
         label,
         StringField.DEFAULT_LENGTH,
         newRecordValue);
  }

  public OptionalStringField(ObjectFieldKey<Optional<String>> fieldKey,
                             String label,
                             int maxLength,
                             Optional<String> newRecordValue)
  {
    this(fieldKey,
         label,
         maxLength,
         FieldValueSuppliers.of(Preconditions.checkNotNull(newRecordValue)));
  }

  public OptionalStringField(ObjectFieldKey<Optional<String>> fieldKey,
                             String label,
                             Optional<String> newRecordValue)
  {
    this(fieldKey,
         label,
         StringField.DEFAULT_LENGTH,
         newRecordValue);
  }

  public int getMaxLength()
  {
    return __maxLength;
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    StringField.validateSqlDefinition(sqlDef, 255, getTable().getName(), this.getName());
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    String value)
  {
    SqlUtil.toSafeSqlString(buffer, value);
  }

  @Override
  public Optional<String> objectToOptionalValue(Object object)
  {
    return Optional.of(StringField.trimLengthIfNeccesary(object.toString(), getMaxLength()));
  }

  /**
   * @return Optional String if is was possible to get a non-empty String out of the Gettable. If
   *         there was either a null value mapped to the key or an empty String mapped to the key
   *         then {@link Optional#absent()} is returned.
   */
  @Override
  public Optional<String> getValue(TransientRecord record,
                                   Gettable params)
  {
    Object value = params.get(getName());
    if (value == null) {
      return params.containsKey(getName()) ? Optional.<String> absent() : null;
    }
    if (value instanceof Optional<?>) {
      Optional<?> optionalValue = (Optional<?>) value;
      if (optionalValue.isPresent()) {
        return toOptionalNotEmptyString(optionalValue.get());
      } else {
        return Optional.absent();
      }
    }
    return toOptionalNotEmptyString(value);
  }

  private Optional<String> toOptionalNotEmptyString(Object obj)
  {
    return Optional.fromNullable(Strings.emptyToNull(StringUtils.toString(obj)));
  }

  @Override
  public Optional<String> dbToValue(Object v,
                                    Object[] values,
                                    int[] intValues)
  {
    return Optional.of(v.toString());
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       String value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value);
  }

  @Override
  protected int getSqlType()
  {
    return Types.VARCHAR;
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    if (_isTextAreaWidget) {
      Optional<String> optValue = record.comp(getKey());
      HtmlUtils.textareaWidget(buf,
                               optValue.isPresent() ? valueToEditableString(optValue.get()) : "",
                               0,
                               8,
                               false,
                               namePrefix,
                               getName());
    } else {
      super.appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    }
  }

  public OptionalStringField isTextAreaWidget(boolean isTextAreaWidget)
  {
    _isTextAreaWidget = isTextAreaWidget;
    return this;
  }
}
