package org.cord.mirror.field;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.json.ImmutableJSON;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.WritableJSON;
import org.json.WritableJSONArray;

public class JSONArrayField
  extends AbstractJSONField<JSONArray>
{
  protected JSONArrayField(ObjectFieldKey<String> name,
                           String label,
                           RecordOperationKey<JSONArray> asJSONName,
                           WritableJSONArray defaultValue)
  {
    super(name,
          label,
          asJSONName,
          defaultValue == null ? new WritableJSONArray() : defaultValue,
          JSONArray.class);
  }

  @Override
  protected JSONArray asJSON(TransientRecord record)
      throws JSONException
  {
    return (record.isWriteable()
        ? WritableJSON.get()
        : ImmutableJSON.get()).toJSONArray(record.getString(getName()));
  }
}
