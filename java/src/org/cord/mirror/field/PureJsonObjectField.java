package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordStateDataFilter;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.sql.SqlUtil;
import org.json.ImmutableJSON;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSON;
import org.json.WritableJSONObject;

public class PureJsonObjectField
  extends ObjectField<JSONObject>
  implements RecordStateDataFilter
{
  public static ObjectFieldKey<JSONObject> createKey(String name)
  {
    return ObjectFieldKey.create(name, JSONObject.class);
  }

  private static FieldValueSupplier<WritableJSONObject> EMPTYFIELDVALUESUPPLIER =
      new FieldValueSupplier<WritableJSONObject>() {
        @Override
        public WritableJSONObject supplyValue(TransientRecord record)
        {
          return new WritableJSONObject();
        }
      };

  public static FieldValueSupplier<WritableJSONObject> emptyFieldValueSupplier()
  {
    return EMPTYFIELDVALUESUPPLIER;
  }

  public PureJsonObjectField(ObjectFieldKey<JSONObject> fieldKey,
                             String label,
                             FieldValueSupplier<WritableJSONObject> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          JSONObject.class,
          defaultDefinedValue);
  }

  @Override
  protected synchronized void register(Table table,
                                       int fieldId)
      throws MirrorTableLockedException
  {
    super.register(table, fieldId);
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_WRITABLE_PREUPDATE,
                                                   false,
                                                   table.getName()) {
      @Override
      public void trigger(PersistentRecord record,
                          Transaction transaction,
                          int state)
          throws Exception
      {
        WritableRecord writable = record.getWritableRecord(transaction);
        boolean isUpdated = !record.comp(getKey()).equals(writable.comp(getKey()));
        if (isUpdated) {
          setIsUpdated(writable);
        }
      }

      @Override
      public String toString()
      {
        return PureJsonObjectField.this + ".isUpdatedTrigger";
      }
    });
  }

  @Override
  public JSONObject dbToValue(Object v,
                              Object[] values,
                              int[] intValues)
  {
    try {
      return ImmutableJSON.getInstance().toJSONObject((String) v);
    } catch (JSONException e) {
      throw new MirrorLogicException(e);
    }
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       JSONObject value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value.toString());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    StringField.validateSqlDefinition(sqlDef,
                                      StringField.LENGTH_TEXT,
                                      getTable().getName(),
                                      this.getName());
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    JSONObject value)
  {
    SqlUtil.toSafeSqlString(buffer, value.toString());
  }

  @Override
  public JSONObject validateDefinedValue(TransientRecord record,
                                         Object value)
      throws MirrorFieldException, TypeValidationException
  {
    if (value instanceof JSONObject) {
      return (JSONObject) value;
    }
    try {
      return WritableJSON.get().toJSONObject(value.toString());
    } catch (JSONException e) {
      if (supportsExceptionForTypeConversion()) {
        throw new TypeValidationException("Unable to parse as JSONObject",
                                          e,
                                          record,
                                          getKey().getKeyName(),
                                          value);
      } else {
        return getDefaultDefinedValue(record);
      }
    }
  }

  @Override
  protected int getSqlType()
  {
    return Types.VARCHAR;
  }

  @Override
  public void persistentToWritable(PersistentRecord persistentRecord,
                                   Object[] writableObjectValues,
                                   int[] writableIntValues)
  {
    writableObjectValues[getObjectValueIndex()] =
        WritableJSON.get().toJSONObject((JSONObject) writableObjectValues[getObjectValueIndex()]);
  }

  @Override
  public void writableToPersistent(WritableRecord writableRecord,
                                   Object[] persistentObjectValues,
                                   int[] persistentIntValues)
  {
    persistentObjectValues[getObjectValueIndex()] =
        ImmutableJSON.getInstance()
                     .toJSONObject((JSONObject) persistentObjectValues[getObjectValueIndex()]);
  }

  @Override
  public void transientToPersistent(TransientRecord transientRecord,
                                    Object[] persistentObjectValues,
                                    int[] persistentIntValues)
  {
    persistentObjectValues[getObjectValueIndex()] =
        ImmutableJSON.getInstance()
                     .toJSONObject((JSONObject) persistentObjectValues[getObjectValueIndex()]);
  }
}
