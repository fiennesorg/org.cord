package org.cord.mirror.field;

import java.util.Date;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Preconditions;

public class DateSupplierConstant
  implements FieldValueSupplier<Date>
{
  private final Date __value;

  public DateSupplierConstant(Date value)
  {
    __value = Preconditions.checkNotNull(value, "value");
  }

  @Override
  public Date supplyValue(TransientRecord record)
  {
    return __value;
  }

}
