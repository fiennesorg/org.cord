package org.cord.mirror.field;

import java.util.Date;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

public class DateSupplierNow
  implements FieldValueSupplier<Date>
{
  private static final DateSupplierNow INSTANCE = new DateSupplierNow();

  public static final DateSupplierNow getInstance()
  {
    return INSTANCE;
  }

  private DateSupplierNow()
  {
  }

  @Override
  public Date supplyValue(TransientRecord record)
  {
    return new Date();
  }

}
