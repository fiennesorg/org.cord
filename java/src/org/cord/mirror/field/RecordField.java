package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.RecordReference;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;

public class RecordField
  extends ObjectField<RecordReference>
{

  protected RecordField(FieldKey<RecordReference> fieldName,
                        String label,
                        boolean supportsNull,
                        Class<RecordReference> valueClass,
                        FieldValueSupplier<RecordReference> newRecordValue,
                        FieldValueSupplier<RecordReference> defaultDefinedValue)
  {
    super(fieldName,
          label,
          supportsNull,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    RecordReference value)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public RecordReference validateDefinedValue(TransientRecord record,
                                              Object value)
      throws MirrorFieldException, TypeValidationException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public RecordReference dbToValue(Object v,
                                   Object[] values,
                                   int[] intValues)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    // TODO Auto-generated method stub

  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       RecordReference value)
      throws SQLException
  {
    // TODO Auto-generated method stub

  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

}
