package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.ObjectUtil;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

public abstract class OptionalField<T>
  extends ObjectField<Optional<T>>
{
  public OptionalField(FieldKey<Optional<T>> fieldKey,
                       String label,
                       FieldValueSupplier<Optional<T>> newRecordValue)
  {
    super(fieldKey,
          label,
          true,
          ObjectUtil.<Optional<T>> castClass(Optional.class),
          newRecordValue,
          newRecordValue);
  }

  @Override
  public final void appendDefinedSqlValue(StringBuilder buffer,
                                          Optional<T> value)
  {
    if (value.isPresent()) {
      appendPresentSqlValue(buffer, value.get());
    } else {
      buffer.append("null");
    }
  }

  public abstract void appendPresentSqlValue(StringBuilder buffer,
                                             T value);

  @Override
  public final Optional<T> validateDefinedValue(TransientRecord record,
                                                Object value)
      throws MirrorFieldException, TypeValidationException
  {
    if (value == null || (value instanceof String && Strings.isNullOrEmpty((String) value))) {
      return Optional.absent();
    }
    if (value instanceof Optional<?>) {
      Optional<?> optional = (Optional<?>) value;
      if (optional.isPresent()) {
        return objectToOptionalValue(optional.get());
      } else {
        return Optional.absent();
      }
    }
    return objectToOptionalValue(value);
  }

  /**
   * @return Optional.absent() always
   */
  @Override
  protected Optional<T> validateNullValue(TransientRecord record)
  {
    return Optional.absent();
  }

  public abstract Optional<T> objectToOptionalValue(Object object)
      throws IllegalArgumentException;

  @Override
  public final Optional<T> dbNullToValue(Object[] values,
                                         int[] intValues)
  {
    return Optional.absent();
  }

  @Override
  public void appendFormElement(final TransientRecord record,
                                final Viewpoint viewpoint,
                                final String namePrefix,
                                final Appendable buf,
                                final Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf, getHtmlLabel(), false, namePrefix, getName());
    if (mayHaveHtmlWidget() && hasHtmlWidget(record)) {
      appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    } else {
      appendHtmlValue(record, viewpoint, namePrefix, buf, context);
    }
    HtmlUtilsTheme.closeDiv(buf);
  }

  @Override
  protected final void populateDefinedInsert(PreparedStatement preparedStatement,
                                             Optional<T> value)
      throws SQLException
  {
    if (value.isPresent()) {
      populatePresentInsert(preparedStatement, value.get());
    } else {
      preparedStatement.setNull(getSqlFieldIndex(), getSqlType());
    }
  }

  protected abstract void populatePresentInsert(PreparedStatement preparedStatement,
                                                T value)
      throws SQLException;
}
