package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.util.JodaGranularity;
import org.joda.time.DateTime;

import com.google.common.base.Optional;

public class JodaDateTimeField
  extends ObjectField<DateTime>
{
  public static ObjectFieldKey<DateTime> createKey(String keyName)
  {
    return ObjectFieldKey.create(keyName, DateTime.class);
  }

  public static final FieldValueSupplier<DateTime> NOW = new FieldValueSupplier<DateTime>() {
    @Override
    public DateTime supplyValue(TransientRecord record)
    {
      return DateTime.now();
    }
  };

  private final JodaGranularity __jodaGranularity;

  public JodaDateTimeField(ObjectFieldKey<DateTime> fieldName,
                           String label,
                           JodaGranularity jodaGranularity,
                           FieldValueSupplier<DateTime> newRecordValue,
                           FieldValueSupplier<DateTime> defaultDefinedValue)
  {
    super(fieldName,
          label,
          DateTime.class,
          newRecordValue,
          defaultDefinedValue);
    __jodaGranularity = checkNotNull(jodaGranularity);
  }

  public JodaDateTimeField(ObjectFieldKey<DateTime> fieldName,
                           String label,
                           JodaGranularity jodaGranularity,
                           FieldValueSupplier<DateTime> defaultDefinedValue)
  {
    super(fieldName,
          label,
          DateTime.class,
          defaultDefinedValue);
    __jodaGranularity = checkNotNull(jodaGranularity);
  }

  @Override
  public DateTime objectToValue(TransientRecord record,
                                Object obj)
  {
    return OptionalJodaDateTimeField.toDateTime(obj, __jodaGranularity)
                                    .or(getDefaultDefinedValue(record));
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    __jodaGranularity.widget(buf,
                             context,
                             Optional.of(record.comp(getKey())),
                             namePrefix,
                             getName());
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    DateTime value)
  {
    buffer.append(value.getMillis());
  }

  @Override
  public DateTime validateDefinedValue(TransientRecord record,
                                       Object value)
      throws MirrorFieldException, TypeValidationException
  {
    Optional<DateTime> optDateTime = OptionalJodaDateTimeField.toDateTime(value, __jodaGranularity);
    if (optDateTime.isPresent()) {
      return optDateTime.get();
    }
    return getDefaultDefinedValue(record);
  }

  @Override
  public DateTime dbToValue(Object v,
                            Object[] values,
                            int[] intValues)
  {
    return new DateTime(v);
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(LongField.ISVALIDSQL, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       DateTime value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.getMillis());
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }

}
