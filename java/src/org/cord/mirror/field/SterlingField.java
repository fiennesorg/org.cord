package org.cord.mirror.field;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.util.Casters;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;

public class SterlingField
  extends IntegerField
{
  /**
   * Create an optional SterlingField
   */
  public SterlingField(ObjectFieldKey<Integer> fieldName,
                       String label,
                       Integer newRecordValue,
                       Integer defaultDefinedValue)
  {
    super(fieldName,
          label,
          newRecordValue,
          defaultDefinedValue);
  }

  /**
   * create a compulsory SterlingField
   */
  public SterlingField(ObjectFieldKey<Integer> fieldName,
                       String label,
                       Integer defaultValue)
  {
    super(fieldName,
          label,
          defaultValue);
  }

  public final static char MINUS = '\u2212';

  public static String toSterlingString(int i)
  {
    if (i == 0) {
      return "£0.00";
    }
    StringBuilder buf = new StringBuilder(8);
    buf.append('£');
    if (i < 0) {
      buf.append(MINUS);
      i = i * -1;
    }
    int pence = i % 100;
    buf.append(i / 100).append('.');
    if (pence == 0) {
      buf.append("00");
    } else if (pence < 10) {
      buf.append('0').append(pence);
    } else {
      buf.append(pence);
    }
    return buf.toString();
  }
  
  public static String toSterlingString(Integer i)
  {
    return toSterlingString(i.intValue());
  }

  public static MonoRecordOperationKey<Object, String> ASSTERLING()
  {
    return MonoRecordOperationKey.create("asSterling", Object.class, String.class);
  }

  public static MonoRecordOperation<Object, String> ASSTERLING(MonoRecordOperationKey<Object, String> key)
  {
    return new MonoRecordOperation<Object, String>(key, Casters.NULL) {
      @Override
      public String calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              Object a)
      {
        return toSterlingString(ObjectUtil.castTo(record.get(a), Integer.class).intValue());
      }
    };
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, toSterlingString(getFieldValue(record).intValue()), namePrefix, getName());
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.textWidget(null,
                         buf,
                         toSterlingString(getFieldValue(record).intValue()),
                         namePrefix,
                         getName());
  }

  /**
   * Try and convert an optional Object into a Sterling Integer in pence with failure generating an
   * exception
   * 
   * @return null --> null, Integers --> Integer, Number --> number.intValue(), String -->
   *         BigDecimal * 100 --> Integer
   */
  public static Integer parseFormattedOptSterling(Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Integer) {
      return (Integer) obj;
    }
    if (obj instanceof Number) {
      return Integer.valueOf(((Number) obj).intValue());
    }
    String s = obj.toString();
    if (s.startsWith("£")) {
      s = s.substring(1);
    }
    if (s.indexOf(MINUS) != -1) {
      s = s.replace(MINUS, '-');
    }
    try {
      BigDecimal bd = new BigDecimal(s);
      return Integer.valueOf(bd.movePointRight(2).intValue());
    } catch (NumberFormatException nfEx) {
      throw new IllegalArgumentException(String.format("Unable to parse value of \"%s\"", obj));
    }
  }

  public static Integer parseOptSterling(Object obj)
  {
    try {
      return parseFormattedOptSterling(obj);
    } catch (Exception ex) {
      return null;
    }
  }

  @Override
  public Integer objectToValue(TransientRecord record,
                               Object obj)
  {
    return parseFormattedOptSterling(obj);
  }

}
