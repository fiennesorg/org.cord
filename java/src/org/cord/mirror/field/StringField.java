package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.StringUtils;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * FieldFilter that implements an optionally fixed length String field using a VARCHAR or TEXT field
 * as the SQL backend representation. The static method of the class attempts to create a Class
 * object to represent java.lang.String and will throw a RuntimException if this is not possible
 * (which should never happen).
 */
public class StringField
  extends ObjectField<String>
{
  public static ObjectFieldKey<String> createKey(String name)
  {
    return ObjectFieldKey.create(name, String.class);
  }

  private final int __maxLength;

  private boolean _mayBeTextArea = false;

  public static final Predicate<String> ISVALIDVARCHAR = Predicates.equalTo("varchar(255)");

  public static final int LENGTH_TEXT = 65535;

  public static final int LENGTH_MEDIUMTEXT = 16777215;

  public static final int LENGTH_VARCHAR = 255;

  /**
   * The default length for StringFieldFilters with unspecified lengths (255).
   */
  public final static int DEFAULT_LENGTH = LENGTH_VARCHAR;

  /**
   * The default starting value for StringFieldFilters ("").
   */
  public final static String DEFAULT_CONTENTS = "";

  /**
   * Create a named String FieldFilter with the default value and the default maximum length.
   * 
   * @param fieldName
   *          The name of the field
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @see #DEFAULT_LENGTH
   * @see #DEFAULT_CONTENTS
   */
  public StringField(ObjectFieldKey<String> fieldName,
                     String label)
  {
    this(fieldName,
         label,
         DEFAULT_LENGTH,
         DEFAULT_CONTENTS);
  }

  /**
   * Create a named String FieldFilter with a given default value and an optional maximum length.
   * 
   * @param fieldName
   *          The SQL table field that this FieldFilter reflects
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param maxLength
   *          The maximum permitted length for Strings stored in this field. If it is unlimited then
   *          UNLIMITED_LENGTH should be used.
   * @param defaultValue
   *          The default value for new instances of this field. The supplied value will be
   *          validated first to ensure that it doesn't contravene the maxLength directive.
   */
  public StringField(ObjectFieldKey<String> fieldName,
                     String label,
                     int maxLength,
                     String defaultValue)
  {
    this(fieldName,
         label,
         false,
         maxLength,
         defaultValue,
         defaultValue);
  }

  /**
   * Create a new StringField that may be optional.
   */
  public StringField(ObjectFieldKey<String> fieldName,
                     String label,
                     int maxLength,
                     String newRecordValue,
                     String defaultDefinedValue)
  {
    this(fieldName,
         label,
         true,
         maxLength,
         newRecordValue,
         defaultDefinedValue);
  }

  protected StringField(ObjectFieldKey<String> fieldName,
                        String label,
                        boolean supportsNull,
                        int maxLength,
                        String newRecordValue,
                        String defaultDefinedValue)
  {
    super(fieldName,
          label,
          supportsNull,
          String.class,
          FieldValueSuppliers.of(newRecordValue),
          FieldValueSuppliers.of(defaultDefinedValue));
    if (maxLength < 0) {
      throw new IllegalArgumentException("Max Length cannot be less than 0:" + maxLength);
    }
    __maxLength = maxLength;
  }

  public final boolean mayBeTextArea()
  {
    return _mayBeTextArea;
  }

  /**
   * Set the value that will be returned by mayBeTextArea. Setting this to true means that
   * isTextArea(record) will be invoked and the default behaviour if isTextArea(record) is not
   * overridden is to return true which means that setting this to true will cause th edit interface
   * to use a textarea rather than an input field.
   * 
   * @return this
   * @see #mayBeTextArea()
   * @see #isTextArea(TransientRecord)
   */
  public final StringField setMayBeTextArea(boolean mayBeTextArea)
  {
    blockOnState(LO_NEW, "Must set TextArea status before locking");
    _mayBeTextArea = mayBeTextArea;
    return this;
  }

  /**
   * Should this StringField be rendered as an HTML textarea? if not then it will be a normal input
   * field which is the default behaviour. Override if you want to change this. This will never get
   * invoked unless setMayBeTextArea(true) has been called during initialisation.
   * 
   * @param record
   * @return true by default, but mayBeTextArea is false by default so this doesn't get invoked.
   * @see #setMayBeTextArea(boolean)
   */
  protected boolean isTextArea(TransientRecord record)
  {
    return true;
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    String sqlValue)
  {
    SqlUtil.toSafeSqlString(buffer, sqlValue);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    if (mayBeTextArea() && isTextArea(record)) {
      HtmlUtils.textareaWidget(buf,
                               StringUtils.toString(this.getFieldValue(record)),
                               0,
                               8,
                               false,
                               namePrefix,
                               getName());
    } else {
      super.appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    }
  }

  /**
   * Convert the object into a valid String value by calling Object.toString() and then trimming the
   * resulting String to fit in with the maximum length requirements (if any).
   * 
   * @return The java.lang.String version of obj.
   */
  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String string = StringUtils.toString(obj);
    if (DebugConstants.DEBUG_MIRROR_STRINGS_TRAILINGSPACES && __maxLength <= LENGTH_VARCHAR
        && string.endsWith(" ")) {
      DebugConstants.DEBUG_OUT.println(String.format("Trailing space on varchar: \"%s\" --> %s.%s",
                                                     string,
                                                     getTable().getName(),
                                                     getName()));
    }
    return trimLengthIfNeccesary(string, __maxLength);
  }

  public static final String trimLengthIfNeccesary(String string,
                                                   int maxLength)
  {
    int stringLength = string.length();
    if (stringLength < maxLength / 3) {
      return string;
    }
    int utf8Length = StringUtils.lengthAsUtf8(string);
    if (utf8Length > maxLength) {
      string = string.substring(0, maxLength - (utf8Length - stringLength));
    }
    return string;
  }

  @Override
  public String toString()
  {
    return getName() + "(STR:" + getDefaultDefinedValue(null) + ")";
  }

  /**
   * Return the maximum length of String that this field will support.
   * 
   * @return The maximum number of characters
   */
  public final int getMaxLength()
  {
    return __maxLength;
  }

  private static final Pattern __digit = Pattern.compile("\\d+");

  public static void validateSqlDefinition(String sqlDef,
                                           int maxLength,
                                           String tableName,
                                           String fieldName)
      throws SQLException
  {
    int sqlLength = 0;
    if ("mediumtext".equals(sqlDef)) {
      sqlLength = LENGTH_MEDIUMTEXT;
    } else if ("text".equals(sqlDef)) {
      sqlLength = LENGTH_TEXT;
    } else {
      if (sqlDef.indexOf("char") == -1) {
        throw new SQLException(String.format("%s.%s is declared as %s rather than as a char based field",
                                             tableName,
                                             fieldName,
                                             sqlDef));
      }
      Matcher m = __digit.matcher(sqlDef);
      if (m.find()) {
        sqlLength = Integer.parseInt(m.group());
      } else {
        throw new SQLException(tableName + "." + fieldName + ": unknown SQL definition: " + sqlDef);
      }
    }
    if (sqlLength > 0 && sqlLength < maxLength) {
      throw new SQLException(tableName + "." + fieldName + " (length " + maxLength
                             + ") is longer than SQL definition of " + sqlDef);
    }
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDefinition(sqlDef, getMaxLength(), getTable().getName(), this.getName());
  }

  /**
   * @see Gettable#getString(Object)
   */
  @Override
  public String getValue(TransientRecord record,
                         Gettable params)
  {
    return params.getString(getName());
  }

  @Override
  public String dbToValue(Object v,
                          Object[] values,
                          int[] intValues)
  {
    return (String) v;
  }

  @Override
  public void dbToValue(ResultSet resultSet,
                        Object[] values,
                        int[] intValues)
      throws SQLException
  {
    values[getObjectValueIndex()] = resultSet.getString(getSqlFieldIndex());
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       String value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value);
  }

  @Override
  protected int getSqlType()
  {
    return Types.VARCHAR;
  }

}
