package org.cord.mirror.field;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.util.Named;

import com.google.common.base.Preconditions;

public class NamedMapField<T extends Named>
  extends MapField<T>
{
  public NamedMapField(FieldKey<T> fieldName,
                       String label,
                       Class<T> valueClass,
                       FieldValueSupplier<T> newRecordValue,
                       FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public NamedMapField(FieldKey<T> fieldName,
                       String label,
                       Class<T> valueClass,
                       FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          defaultDefinedValue);
  }

  @Override
  public T addMapping(String sql,
                      T value)
  {
    Preconditions.checkState(sql.equals(value.getName()),
                             "%s should be named %s not %s",
                             value,
                             value.getName(),
                             sql);
    return super.addMapping(sql, value);
  }

  public void addValue(T value)
  {
    super.addMapping(value.getName(), value);
  }

  public void addValues(Iterable<T> values)
  {
    for (T value : values) {
      addValue(value);
    }
  }
}
