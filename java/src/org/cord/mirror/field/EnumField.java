package org.cord.mirror.field;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.HtmlUtils;
import org.cord.util.StringUtils;

import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * An SQL field that wraps a java enum and stores it as the string declaration of the name of the
 * enum valuies. The value that gets returned from the Record is a fully-fledged enum which means
 * that once can register functionality on the implementation and invoke it directly on the value.
 * 
 * @author alex
 * @param <T>
 *          The type of Enum that this Field represents
 */
public class EnumField<T extends Enum<T>>
  extends ObjectField<T>
{
  public static <T extends Enum<T>> ObjectFieldKey<T> createKey(String name,
                                                                Class<T> enumClass)
  {
    return ObjectFieldKey.create(name, enumClass);
  }

  /**
   * Create an optional EnumField.
   */
  @Deprecated
  public EnumField(ObjectFieldKey<T> fieldName,
                   String label,
                   Class<T> valueClass,
                   FieldValueSupplier<T> newRecordValue,
                   FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public EnumField(ObjectFieldKey<T> fieldKey,
                   String label,
                   FieldValueSupplier<T> newRecordValue,
                   FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          fieldKey.getValueClass(),
          newRecordValue,
          defaultDefinedValue);
  }

  /**
   * Create a compulsory EnumField.
   */
  @Deprecated
  public EnumField(ObjectFieldKey<T> fieldName,
                   String label,
                   Class<T> valueClass,
                   FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          valueClass,
          defaultDefinedValue);
  }

  /**
   * Create a compulsory EnumField.
   */
  public EnumField(ObjectFieldKey<T> fieldName,
                   String label,
                   FieldValueSupplier<T> defaultDefinedValue)
  {
    super(fieldName,
          label,
          fieldName.getValueClass(),
          defaultDefinedValue);
  }

  public static <T extends Enum<T>> EnumField<T> createComp(ObjectFieldKey<T> fieldName,
                                                            String label,
                                                            FieldValueSupplier<T> defaultDefinedValue)
  {
    return new EnumField<T>(fieldName, label, defaultDefinedValue);
  }

  public static <T extends Enum<T>> EnumField<T> createComp(ObjectFieldKey<T> fieldName,
                                                            String label,
                                                            T defaultDefinedValue)
  {
    return createComp(fieldName, label, FieldValues.of(defaultDefinedValue));
  }

  /**
   * Create an optional EnumField where the default state is undefined.
   */
  public static <T extends Enum<T>> EnumField<T> createOpt(ObjectFieldKey<T> fieldKey,
                                                           String label,
                                                           T defaultDefinedValue)
  {
    return new EnumField<T>(fieldKey,
                            label,
                            FieldValues.<T> of(),
                            FieldValues.of(defaultDefinedValue));
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    T value)
  {
    SqlUtil.toSafeSqlString(buffer, value.name());
  }

  @Override
  public T validateDefinedValue(TransientRecord record,
                                Object value)
      throws MirrorFieldException, TypeValidationException
  {
    if (getValueClass().isInstance(value)) {
      return getValueClass().cast(value);
    }
    T enumValue = Enum.valueOf(getValueClass(), StringUtils.toString(value));
    if (enumValue == null) {
      enumValue = getDefaultDefinedValue(record);
    }
    return enumValue;
  }

  @Override
  public T dbToValue(Object value,
                     Object[] values,
                     int[] intValues)
  {
    T enumValue = Enum.valueOf(getValueClass(), (String) value);
    if (enumValue == null) {
      enumValue = getDefaultDefinedValue(null);
    }
    return enumValue;
  }

  @Override
  public T objectToValue(TransientRecord record,
                         Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (getValueClass().isInstance(obj)) {
      return getValueClass().cast(obj);
    }
    T value = Enum.valueOf(getValueClass(), obj.toString());
    if (value == null) {
      throw new ClassCastException(String.format("It is not possible to convert %s into a %s",
                                                 obj,
                                                 getValueClass()));
    }
    return value;
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    T currentValue = getFieldValue(record);
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    for (T value : getDefaultDefinedValue(record).getDeclaringClass().getEnumConstants()) {
      HtmlUtils.option(buf, value.name(), value.toString(), Objects.equal(currentValue, value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  public static final Predicate<String> ISVALIDSQL = Predicates.equalTo("varchar(255)");

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(ISVALIDSQL, sqlDef);
  }

  /**
   * @return the name() method on the enum value as this may be different to the toString() method.
   */
  @Override
  public String valueToNodeRequest(T value)
  {
    return value.name();
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       T value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value.name());
  }

  public static final int SQLTYPE = Types.VARCHAR;

  @Override
  protected int getSqlType()
  {
    return SQLTYPE;
  }

}
