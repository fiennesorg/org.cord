package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

public class FieldValues
{
  /**
   * @throws NullPointerException
   *           if value is null
   */
  public static <T> FieldValueSupplier<T> ofNotNull(T value)
  {
    return of(checkNotNull(value, "FieldValues.ofNotNull(null)"));
  }

  public static <T> FieldValueSupplier<T> of(final T value)
  {
    return new FieldValueSupplier<T>() {
      @Override
      public T supplyValue(TransientRecord record)
      {
        return value;
      }
    };
  }

  private static FieldValueSupplier<?> NULLINSTANCE = of(null);

  @SuppressWarnings("unchecked")
  public static <T> FieldValueSupplier<T> of()
  {
    return (FieldValueSupplier<T>) NULLINSTANCE;
  }
}
