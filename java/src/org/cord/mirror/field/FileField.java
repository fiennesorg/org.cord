package org.cord.mirror.field;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TypeValidationException;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.Assertions;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.IoUtil;

import com.google.common.io.Files;

/**
 * An implementation of Field that stores a reference to a java File and is responsible for deleting
 * Files when they are no longer required. The Field will be responsible for taking copies of the
 * Files as appropriate, with the location that the File will be stored in being given by an
 * external RecordValueFactory that is responible for outputing the absolute directory that will be
 * utilised. SQL will store the path relative to the parentDirectory as a string.
 * 
 * @author alex
 */
public class FileField
  extends ObjectField<File>
{
  private final File __parentDirectory;

  private final int __parentDirectoryLength;

  private final String __baseDirName;

  private final String __localPrefix;
  private final String __urlPrefix;

  private FileField(FieldKey<File> fieldName,
                    String label,
                    boolean supportsNull,
                    FieldValueSupplier<File> newRecordValue,
                    FieldValueSupplier<File> defaultDefinedValue,
                    File parentDirectory,
                    String baseDirName,
                    String localPrefix,
                    String urlPrefix)
  {
    super(fieldName,
          label,
          supportsNull,
          File.class,
          newRecordValue,
          defaultDefinedValue);
    Assertions.isTrue(parentDirectory != null && parentDirectory.isDirectory()
                      && parentDirectory.canWrite(),
                      "%s is not a valid parentDirectory",
                      parentDirectory);
    __parentDirectory = parentDirectory;
    String parentDirectoryPath = __parentDirectory.toString();
    __parentDirectoryLength = parentDirectoryPath.length() + 1;
    __baseDirName = baseDirName;
    if (localPrefix != null) {
      Assertions.isTrue(parentDirectoryPath.startsWith(localPrefix),
                        "%s is not a valid localPrefix because it doesn't match %s",
                        localPrefix,
                        parentDirectoryPath);
      Assertions.notEmpty(urlPrefix, "%s is not a valid urlPrefix", urlPrefix);
    }
    __localPrefix = localPrefix;
    __urlPrefix = urlPrefix;
  }

  /**
   * Create an optional FileField that supports null values.
   */
  public FileField(FieldKey<File> fieldName,
                   String label,
                   FieldValueSupplier<File> newRecordValue,
                   FieldValueSupplier<File> defaultDefinedValue,
                   File parentDirectory,
                   String baseDirName,
                   String localPrefix,
                   String urlPrefix)
  {
    this(fieldName,
         label,
         true,
         newRecordValue,
         defaultDefinedValue,
         parentDirectory,
         baseDirName,
         localPrefix,
         urlPrefix);
  }

  /**
   * Create a FileField that doesn't support null values.
   */
  public FileField(FieldKey<File> fieldName,
                   String label,
                   FieldValueSupplier<File> defaultDefinedValue,
                   File parentDirectory,
                   String baseDirName,
                   String localPrefix,
                   String urlPrefix)
  {
    this(fieldName,
         label,
         false,
         defaultDefinedValue,
         defaultDefinedValue,
         parentDirectory,
         baseDirName,
         localPrefix,
         urlPrefix);
  }

  public final String getLocalPrefix()
  {
    return __localPrefix;
  }

  public final String getUrlPrefix()
  {
    return __urlPrefix;
  }

  /**
   * append the part of the File reference after the parentDirectory definition. There is no
   * checking as to whether or not the File is contained within the parentDirectory as this will
   * have been done previously in the validateDefinedValue method.
   */
  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    File value)
  {
    SqlUtil.toSafeSqlString(buffer, value.toString().substring(__parentDirectoryLength));
  }

  /**
   * Create a new temporary directory within parentDirectory and then copy value as a File into the
   * directory preserving the filename. If this is being invoked coming from a db record then the
   * string is interpretted as a relative path of a File within parentDirectory.
   */
  @Override
  public File validateDefinedValue(TransientRecord record,
                                   Object value)
      throws MirrorFieldException, TypeValidationException
  {
    File sourceFile = null;
    if (value instanceof File) {
      sourceFile = (File) value;
    } else {
      sourceFile = new File(value.toString());
    }
    if (sourceFile.canRead()) {
      throw new MirrorFieldException(String.format("Unable to read %s as a File", sourceFile),
                                     null,
                                     record,
                                     getName());
    }
    String filename = sourceFile.getName();
    File uniqueDir = IoUtil.getTempDir(__parentDirectory, __baseDirName, 20);
    File targetFile = new File(uniqueDir, filename);
    try {
      Files.copy(sourceFile, targetFile);
    } catch (IOException e) {
      targetFile.delete();
      uniqueDir.delete();
      throw new MirrorFieldException(String.format("unable to copy contents of %s to %s",
                                                   sourceFile,
                                                   targetFile),
                                     e,
                                     record,
                                     getName());
    }
    return targetFile;
  }

  @Override
  public File dbToValue(Object v,
                        Object[] values,
                        int[] intValues)
  {
    // should File references from the db be validated to exist when they are
    // retrieved?
    return new File(__parentDirectory, (String) v);
  }

  /**
   * Ask the Gettable to provide the contained file with the same name as this FileField.
   * 
   * @see Gettable#getFile(Object)
   */
  @Override
  public File getValue(TransientRecord record,
                       Gettable params)
  {
    return params.getFile(getName());
  }

  public String getHtmlPath(File file)
  {
    String path = file.toString();
    return __urlPrefix + path.substring(__localPrefix.length());
  }

  /**
   * @see HtmlUtils#fileWidget(Appendable, Object...)
   */
  @Override
  public void appendHtmlWidget(final TransientRecord record,
                               final Viewpoint viewpoint,
                               final String namePrefix,
                               final Appendable buf,
                               final Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.fileWidget(buf, namePrefix, getName());
  }

  @Override
  public void appendHtmlValue(final TransientRecord record,
                              final Viewpoint viewpoint,
                              final String namePrefix,
                              final Appendable buf,
                              final Map<Object, Object> context)
      throws IOException
  {
    final StringBuilder a = new StringBuilder();
    final File file = getFieldValue(record);
    final String path = file.toString();
    if (__localPrefix == null) {
      a.append(path);
    } else {
      a.append("<a href=\"")
       .append(getHtmlPath(file))
       .append("\">")
       .append(file.getName())
       .append("</a>");
    }
    HtmlUtils.value(buf, a, namePrefix, getName());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(StringField.ISVALIDVARCHAR, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       File value)
      throws SQLException
  {
    preparedStatement.setString(getSqlFieldIndex(), value.toString());
  }

  @Override
  protected int getSqlType()
  {
    return Types.VARCHAR;
  }

}
