package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;

public class OptionalBooleanField
  extends OptionalField<Boolean>
{
  public static ObjectFieldKey<Optional<Boolean>> createKey(String key)
  {
    return ObjectFieldKey.create(key, ObjectUtil.<Optional<Boolean>> castClass(Optional.class));
  }

  public static final FieldValueSupplier<Optional<Boolean>> NULL =
      FieldValueSuppliers.of(Optional.<Boolean> absent());

  private final String __nullLabel;
  private final String __falseLabel;
  private final String __trueLabel;

  public OptionalBooleanField(ObjectFieldKey<Optional<Boolean>> fieldKey,
                              String label,
                              FieldValueSupplier<Optional<Boolean>> newRecordValue,
                              String nullLabel,
                              String falseLabel,
                              String trueLabel)
  {
    super(fieldKey,
          label,
          newRecordValue);
    __nullLabel = checkNotNull(nullLabel);
    __falseLabel = checkNotNull(falseLabel);
    __trueLabel = checkNotNull(trueLabel);
  }

  public OptionalBooleanField(ObjectFieldKey<Optional<Boolean>> fieldKey,
                              String label,
                              Optional<Boolean> newRecordValue,
                              String nullLabel,
                              String falseLabel,
                              String trueLabel)
  {
    this(fieldKey,
         label,
         FieldValueSuppliers.of(Preconditions.checkNotNull(newRecordValue)),
         nullLabel,
         falseLabel,
         trueLabel);
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(Predicates.equalTo("int(11)"), sqlDef);
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    Boolean value)
  {
    buffer.append(value.booleanValue() ? '1' : '0');
  }

  @Override
  public Optional<Boolean> objectToOptionalValue(Object object)
  {
    return Optional.fromNullable(BooleanField.castValue(object));
  }

  @Override
  public Optional<Boolean> getValue(TransientRecord record,
                                    Gettable params)
  {
    Boolean value = params.getBoolean(getName());
    if (value == null) {
      return params.containsKey(getName()) ? Optional.<Boolean> absent() : null;
    }
    return Optional.of(value);
  }

  @Override
  public Optional<Boolean> dbToValue(Object v,
                                     Object[] values,
                                     int[] intValues)
  {
    return Optional.of(Boolean.valueOf(((Number) v).intValue() != 0));
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       Boolean value)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), value.booleanValue() ? 1 : 0);
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

  @Override
  public void appendFormElement(final TransientRecord record,
                                final Viewpoint viewpoint,
                                final String namePrefix,
                                final Appendable buf,
                                final Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openRadios(buf, getHtmlLabel());
    if (mayHaveHtmlWidget() && hasHtmlWidget(record)) {
      appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    } else {
      appendHtmlValue(record, viewpoint, namePrefix, buf, context);
    }
    HtmlUtils.closeRadios(buf);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    Optional<Boolean> optValue = record.comp(getKey());
    HtmlUtils.radio(buf, getName(), "", !optValue.isPresent(), __nullLabel, namePrefix, getName());
    HtmlUtils.radio(buf,
                    getName(),
                    "true",
                    optValue.isPresent() && optValue.get().booleanValue(),
                    __trueLabel,
                    namePrefix,
                    getName());
    HtmlUtils.radio(buf,
                    getName(),
                    "false",
                    optValue.isPresent() && !optValue.get().booleanValue(),
                    __falseLabel,
                    namePrefix,
                    getName());
  }

}
