package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;

import com.google.common.base.Optional;

public class OptionalDoubleField
  extends OptionalTextInputField<Double>
{
  public static final ObjectFieldKey<Optional<Double>> createKey(String name)
  {
    return ObjectFieldKey.create(name, ObjectUtil.<Optional<Double>> castClass(Optional.class));
  }

  public static final FieldValueSupplier<Optional<Double>> NULL =
      FieldValueSuppliers.of(Optional.<Double> absent());

  public OptionalDoubleField(ObjectFieldKey<Optional<Double>> fieldKey,
                             String label,
                             FieldValueSupplier<Optional<Double>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    Double value)
  {
    buffer.append(value);
  }

  @Override
  public Optional<Double> objectToOptionalValue(Object object)
      throws IllegalArgumentException
  {
    return Optional.fromNullable(DoubleField.castValue(object));
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       Double value)
      throws SQLException
  {
    preparedStatement.setDouble(getSqlFieldIndex(), value.doubleValue());
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(DoubleField.ISVALIDSQLDEF, sqlDef);
  }

  @Override
  public Optional<Double> getValue(TransientRecord record,
                                   Gettable gettable)
  {
    Object value = gettable.get(getName());
    if (value == null) {
      return gettable.containsKey(getName()) ? Optional.<Double> absent() : null;
    }
    return Optional.fromNullable(NumberUtil.toDouble(value, null));
  }

  @Override
  public Optional<Double> dbToValue(Object v,
                                    Object[] values,
                                    int[] intValues)
  {
    return Optional.of((Double) v);
  }

  @Override
  protected int getSqlType()
  {
    return Types.DOUBLE;
  }
}
