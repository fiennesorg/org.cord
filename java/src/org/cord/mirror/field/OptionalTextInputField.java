package org.cord.mirror.field;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;

import com.google.common.base.Optional;

/**
 * Implementation of OptionalField that expects the value to be edited using a standard input text
 * widget. Null values are represented by empty Strings and defined values are passed through
 * {@link #valueToEditableString(Object)} to enable custom representations to be utilised in the
 * edit interface.
 * 
 * @author alex
 * @param <T>
 */
public abstract class OptionalTextInputField<T>
  extends OptionalField<T>
{

  public OptionalTextInputField(FieldKey<Optional<T>> fieldKey,
                                String label,
                                FieldValueSupplier<Optional<T>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    Optional<T> optValue = record.comp(getKey());
    HtmlUtils.textWidget(null,
                         buf,
                         optValue.isPresent() ? valueToEditableString(optValue.get()) : "",
                         namePrefix,
                         getName());
  }

  public String valueToEditableString(T value)
  {
    return value.toString();
  }

}
