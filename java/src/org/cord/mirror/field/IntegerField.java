package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.MyPredicates;

import com.google.common.base.Predicate;

/**
 * FieldFilter that represents a single integer value using an INT field as the SQL representation.
 * The class will try and initialise an instance of the Class java.lang.Integer during classloading
 * and will through a runtime error if this fails. However, if this fails then you probably have
 * other, more extreme problems to think about...
 */
public class IntegerField
  extends ObjectField<Integer>
{
  public static ObjectFieldKey<Integer> createKey(String name)
  {
    return ObjectFieldKey.create(name, Integer.class);
  }

  public final static Integer ZERO = Integer.valueOf(0);

  public final static Integer DEFAULT_VALUE = ZERO;

  public final static FieldValueSupplier<Integer> DEFAULT_VALUE_SUPPLIER =
      FieldValueSuppliers.ofDefined(DEFAULT_VALUE);

  /**
   * Create a named integer FieldFilter with a given default value.
   * 
   * @param fieldName
   *          The SQL table field that this FieldFilter reflects
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param defaultValue
   *          The initial value that instances of this Field should default to.
   */
  public IntegerField(ObjectFieldKey<Integer> fieldName,
                      String label,
                      FieldValueSupplier<Integer> defaultValue)
  {
    super(fieldName,
          label,
          Integer.class,
          defaultValue);
  }

  /**
   * Create an IntegerField that doesn't support NULL values.
   * 
   * @param defaultValue
   *          The compulsory value that a new record will be initialised with.
   */
  public IntegerField(ObjectFieldKey<Integer> fieldName,
                      String label,
                      Integer defaultValue)
  {
    super(fieldName,
          label,
          Integer.class,
          FieldValueSuppliers.ofDefined(defaultValue));
  }

  public IntegerField(ObjectFieldKey<Integer> fieldName,
                      String label,
                      Integer newRecordValue,
                      Integer defaultDefinedValue)
  {
    this(fieldName,
         label,
         FieldValueSuppliers.of(newRecordValue),
         FieldValueSuppliers.ofDefined(defaultDefinedValue));
  }

  /**
   * Create a new IntegerField that supports NULL values.
   */
  public IntegerField(ObjectFieldKey<Integer> fieldName,
                      String label,
                      FieldValueSupplier<Integer> newRecordValue,
                      FieldValueSupplier<Integer> defaultDefinedValue)
  {
    super(fieldName,
          label,
          Integer.class,
          newRecordValue,
          defaultDefinedValue);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Integer sqlValue)
  {
    buffer.append(sqlValue.intValue());
  }

  @Override
  public Integer validateDefinedValue(TransientRecord record,
                                      Object obj)
      throws MirrorFieldException
  {
    try {
      return objectToValue(record, obj);
    } catch (NumberFormatException nfeEx) {
      nfeEx.printStackTrace();
      return getDefaultDefinedValue(record);
    }
  }

  public static Integer castValue(Object obj)
      throws NumberFormatException
  {
    if (obj instanceof Integer) {
      return (Integer) obj;
    }
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return Integer.valueOf(((Number) obj).intValue());
    }
    return Integer.valueOf(obj.toString());
  }

  @Override
  public Integer objectToValue(TransientRecord record,
                               Object obj)
      throws NumberFormatException
  {
    return castValue(obj);
  }

  @Override
  public Integer dbToValue(Object v,
                           Object[] values,
                           int[] intValues)
  {
    return (Integer) v;
  }

  @Override
  public void dbToValue(ResultSet resultSet,
                        Object[] values,
                        int[] intValues)
      throws SQLException
  {
    if (getSupportsNull()) {
      super.dbToValue(resultSet, values, intValues);
    }
    values[getObjectValueIndex()] = Integer.valueOf(resultSet.getInt(getSqlFieldIndex()));
  }

  @Override
  public String toString()
  {
    return String.format("%s(INT, %s)", getName(), getDefaultDefinedValue(null));
  }

  public static final Predicate<String> ISVALIDSQL = MyPredicates.startsWith("int");

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(ISVALIDSQL, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       Integer value)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), value.intValue());
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

}
