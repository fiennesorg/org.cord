package org.cord.mirror.field;

import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;

/**
 * FieldFilter that extends IntegerFieldFilter to provide an 8 bit constrained byte representation.
 * This continues to use Integer as its core storage medium to permit the use of unsigned byte
 * values rather than the signed bytes provided by the Byte class.
 */
public class ByteField
  extends IntField
{
  public ByteField(IntFieldKey fieldName,
                   int defaultValue)
  {
    super(fieldName,
          null,
          validateInt(defaultValue));
  }

  private static Integer validateInteger(Integer value)
  {
    int validatedValue = validateInt(value.intValue());
    return value.intValue() == validatedValue ? value : Integer.valueOf(validatedValue);
  }

  private final static int validateInt(int value)
  {
    return value & 255;
  }

  @Override
  public Integer validateDefinedValue(TransientRecord record,
                                      Object obj)
      throws MirrorFieldException
  {
    return validateInteger(super.validateDefinedValue(record, obj));
  }

  @Override
  public String toString()
  {
    return getName() + "(BYTE:" + getNewRecordValue() + "," + getDefaultDefinedValue(null) + ")";
  }
}
