package org.cord.mirror.field;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;

import com.google.common.base.Optional;

/**
 * RecordOperation that takes the output of an Optional Boolean RecordOperation (eg
 * {@link OptionalBooleanField} and renders it as a String with custom values for absent, false and
 * true.
 * 
 * @author alex
 */
public class OptionalBooleanOperationFormatter
  extends SimpleRecordOperation<String>
{
  public static RecordOperationKey<String> createKey(String key)
  {
    return RecordOperationKey.create(key, String.class);
  }

  private final RecordOperationKey<Optional<Boolean>> __sourceKey;
  private final String __absentValue;
  private final String __falseValue;
  private final String __trueValue;

  public OptionalBooleanOperationFormatter(RecordOperationKey<String> key,
                                           RecordOperationKey<Optional<Boolean>> sourceKey,
                                           String absentValue,
                                           String falseValue,
                                           String trueValue)
  {
    super(key);
    __sourceKey = checkNotNull(sourceKey, "sourceKey");
    __absentValue = checkNotNull(absentValue, "absentValue");
    __falseValue = checkNotNull(falseValue, "falseValue");
    __trueValue = checkNotNull(trueValue, "trueValue");
  }

  @Override
  public String getOperation(TransientRecord targetRecord,
                             Viewpoint viewpoint)
  {
    Optional<Boolean> value = targetRecord.comp(__sourceKey);
    if (value.isPresent()) {
      return value.get().booleanValue() ? __trueValue : __falseValue;
    }
    return __absentValue;
  }

}
