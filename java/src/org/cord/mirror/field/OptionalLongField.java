package org.cord.mirror.field;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;

import com.google.common.base.Optional;

public class OptionalLongField
  extends OptionalTextInputField<Long>
{
  public static ObjectFieldKey<Optional<Long>> createKey(String name)
  {
    return ObjectFieldKey.create(name, ObjectUtil.<Optional<Long>> castClass(Optional.class));
  }

  public OptionalLongField(ObjectFieldKey<Optional<Long>> fieldKey,
                           String label,
                           FieldValueSupplier<Optional<Long>> newRecordValue)
  {
    super(fieldKey,
          label,
          newRecordValue);
  }

  @Override
  public void appendPresentSqlValue(StringBuilder buffer,
                                    Long value)
  {
    buffer.append(value);
  }

  @Override
  public Optional<Long> objectToOptionalValue(Object object)
      throws IllegalArgumentException
  {
    return Optional.fromNullable(LongField.objectToValue(object));
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(LongField.ISVALIDSQL, sqlDef);
  }

  @Override
  public Optional<Long> getValue(TransientRecord record,
                                 Gettable params)
  {
    Object value = params.get(getName());
    if (value == null) {
      return params.containsKey(getName()) ? Optional.<Long> absent() : null;
    }
    return Optional.fromNullable(NumberUtil.toLongObj(value, null));
  }

  @Override
  public Optional<Long> dbToValue(Object v,
                                  Object[] values,
                                  int[] intValues)
  {
    return Optional.of((Long) v);
  }

  @Override
  protected void populatePresentInsert(PreparedStatement preparedStatement,
                                       Long value)
      throws SQLException
  {
    preparedStatement.setLong(getSqlFieldIndex(), value.longValue());
  }

  @Override
  protected int getSqlType()
  {
    return Types.BIGINT;
  }
}
