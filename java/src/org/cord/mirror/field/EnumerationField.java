package org.cord.mirror.field;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.util.EnglishNamed;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.HtmlUtils;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

/**
 * Extension of StringFieldFilter that constrains fields to only contain values from a defined set
 * of valid values.
 */
public class EnumerationField
  extends StringField
{
  private final Map<String, EnglishNamed> __validEnglishNamedValues =
      new LinkedHashMap<String, EnglishNamed>();

  private final Map<String, EnglishNamed> __publicValidValues =
      Collections.unmodifiableMap(__validEnglishNamedValues);

  private final RecordOperationKey<Map<String, EnglishNamed>> __valueDescriptorName;
  private final RecordSourceOperationKey<Map<String, EnglishNamed>> __valueDescriptorRecordSourceKey;

  private final boolean __hasDefinedNewValue;

  protected EnumerationField(ObjectFieldKey<String> fieldName,
                             String label,
                             boolean supportsNull,
                             int maxLength,
                             String newRecordValue,
                             RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName)
  {
    super(fieldName,
          label,
          supportsNull,
          maxLength,
          newRecordValue,
          newRecordValue);
    __hasDefinedNewValue = newRecordValue != null;
    if (valueDescriptorName == null) {
      String keyName = fieldName + "s";
      __valueDescriptorName =
          RecordOperationKey.create(keyName,
                                    ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(keyName,
                                          ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));
    } else {
      __valueDescriptorName = valueDescriptorName;
      __valueDescriptorRecordSourceKey =
          RecordSourceOperationKey.create(valueDescriptorName.getKeyName(),
                                          ObjectUtil.<Map<String, EnglishNamed>> castClass(Map.class));
    }
  }

  /**
   * Create an optional EnumerationField
   */
  public EnumerationField(ObjectFieldKey<String> fieldName,
                          String label,
                          int maxLength,
                          boolean hasDefinedNewValue,
                          RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName)
  {
    this(fieldName,
         label,
         true,
         maxLength,
         hasDefinedNewValue ? "" : null,
         valueDescriptorName);
  }

  /**
   * Create a compulsory EnumerationField
   * 
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param maxLength
   *          The maximum length of the name of any EnglishNamed values that will be options in this
   *          field. corresponds to the SQL lenght
   * @param valueDescriptorName
   *          The name via which the list of valid values may be recalled from the Table, ie
   *          $table.valueDescriptorName --> immutable List of validValues. If null (fieldName)s is
   *          utilised.
   */
  public EnumerationField(ObjectFieldKey<String> fieldName,
                          String label,
                          int maxLength,
                          RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName)
  {
    this(fieldName,
         label,
         false,
         maxLength,
         "",
         valueDescriptorName);
  }

  /**
   * Boot a compulsory field with a length of LENGTH_VARCHAR
   * 
   * @param valueDescriptorName
   *          The name via which the list of valid values may be recalled from the Table, ie
   *          $table.valueDescriptorName --> immutable List of validValues. If null (fieldName)s is
   *          utilised.
   * @see StringField#LENGTH_VARCHAR
   */
  public EnumerationField(ObjectFieldKey<String> fieldName,
                          String label,
                          RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName)
  {
    this(fieldName,
         label,
         LENGTH_VARCHAR,
         valueDescriptorName);
  }

  public void addValue(EnglishNamed value,
                       boolean isDefaultValue)
  {
    blockOnState(LO_NEW, "Cannot add values once item is registered");
    Preconditions.checkNotNull(value, "value");
    if (value.getName().length() > getMaxLength()) {
      throw new IllegalArgumentException(String.format("%s has a name of %s which is more than %s characters",
                                                       value,
                                                       value.getName(),
                                                       Integer.valueOf(getMaxLength())));
    }
    __validEnglishNamedValues.put(value.getName(), value);
    if (isDefaultValue) {
      FieldValueSupplier<String> nameSupplier = FieldValueSuppliers.ofDefined(value.getName());
      setDefaultDefinedValue(nameSupplier);
      if (__hasDefinedNewValue) {
        setNewRecordValue(nameSupplier);
      }
    }
  }

  public void addValue(String value,
                       String title,
                       boolean isDefaultValue)
  {
    addValue(new EnglishNamedImpl(value, title), isDefaultValue);
  }

  public void addValue(String value,
                       boolean isDefaultValue)
  {
    addValue(value, value, isDefaultValue);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openSelect(buf, false, shouldReloadOnChange(), false, namePrefix, getName());
    String value = getFieldValue(record);
    for (Iterator<EnglishNamed> i = getValidValues().values().iterator(); i.hasNext();) {
      EnglishNamed targetValue = i.next();
      HtmlUtils.option(buf,
                       targetValue.getName(),
                       targetValue.getEnglishName(),
                       targetValue.getName().equals(value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    if (__valueDescriptorName != null) {
      getTable().addRecordOperation(new SimpleRecordOperation<Map<String, EnglishNamed>>(__valueDescriptorName) {
        @Override
        public Map<String, EnglishNamed> getOperation(TransientRecord targetRecord,
                                                      Viewpoint viewpoint)
        {
          return __publicValidValues;
        }
      });
      getTable().addRecordSourceOperation(new SimpleRecordSourceOperation<Map<String, EnglishNamed>>(__valueDescriptorRecordSourceKey) {
        @Override
        public Map<String, EnglishNamed> invokeRecordSourceOperation(RecordSource recordSource,
                                                                     Viewpoint viewpoint)
        {
          return __publicValidValues;
        }
      });
    }
  }

  public Map<String, EnglishNamed> getValidValues()
  {
    return __publicValidValues;
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String proposedValue = super.validateDefinedValue(record, obj);
    if (getValidValues() == null || getValidValues().containsKey(proposedValue)) {
      return proposedValue;
    }
    return getDefaultDefinedValue(record);
  }

  public final static EnumerationField getInstance(ObjectFieldKey<String> fieldName,
                                                   String label,
                                                   String defaultValue,
                                                   String validValues,
                                                   String delim,
                                                   RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName,
                                                   final boolean shouldReloadOnChange)
  {
    EnumerationField field =
        new EnumerationField(fieldName, label, LENGTH_VARCHAR, valueDescriptorName) {
          @Override
          public boolean shouldReloadOnChange()
          {
            return shouldReloadOnChange;
          }
        };
    for (String value : validValues.split(delim)) {
      field.addValue(new EnglishNamedImpl(value, value), value.equals(defaultValue));
    }
    return field;
  }

  public final static EnumerationField getInstance(ObjectFieldKey<String> fieldName,
                                                   String label,
                                                   String defaultValue,
                                                   String validValues,
                                                   String delim,
                                                   String valueDelim,
                                                   RecordOperationKey<Map<String, EnglishNamed>> valueDescriptorName,
                                                   final boolean shouldReloadOnChange)
  {
    EnumerationField field =
        new EnumerationField(fieldName, label, LENGTH_VARCHAR, valueDescriptorName) {
          @Override
          public boolean shouldReloadOnChange()
          {
            return shouldReloadOnChange;
          }
        };
    for (String value : validValues.split(delim)) {
      String[] names = value.split(valueDelim);
      field.addValue(new EnglishNamedImpl(names[0], names[1]), names[0].equals(defaultValue));
    }
    return field;
  }
}
