package org.cord.mirror.field;

import java.util.Date;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Preconditions;

public class DateSupplierOffset
  implements FieldValueSupplier<Date>
{
  public static FieldValueSupplier<Date> getInstance(FieldValueSupplier<Date> source,
                                                     long offset)
  {
    return offset == 0 ? source : new DateSupplierOffset(source, offset);
  }

  private final FieldValueSupplier<Date> __source;
  private final long __offset;

  public DateSupplierOffset(FieldValueSupplier<Date> source,
                            long offset)
  {
    __source = Preconditions.checkNotNull(source, "source");
    __offset = offset;
  }

  @Override
  public Date supplyValue(TransientRecord record)
  {
    return new Date(__source.supplyValue(record).getTime() + __offset);
  }

}
