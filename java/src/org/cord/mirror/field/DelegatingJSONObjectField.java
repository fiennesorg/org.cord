package org.cord.mirror.field;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.util.DelegatingJSONObject;
import org.json.ImmutableJSON;
import org.json.JSONObject;

import com.google.common.base.Preconditions;

public abstract class DelegatingJSONObjectField
  extends JSONObjectField
{
  private final RecordOperationKey<JSONObject> __delegatingJSONName;

  protected DelegatingJSONObjectField(ObjectFieldKey<String> name,
                                      String label,
                                      RecordOperationKey<JSONObject> localJSONName,
                                      RecordOperationKey<JSONObject> delegatingJSONName)
  {
    super(name,
          label,
          localJSONName,
          ImmutableJSON.get().emptyJSONObject());
    __delegatingJSONName = Preconditions.checkNotNull(delegatingJSONName, "delegatingJSONName");
  }

  @Override
  protected void register(Table table,
                          int fieldId)
      throws MirrorTableLockedException
  {
    super.register(table, fieldId);
    table.addRecordOperation(new SimpleRecordOperation<JSONObject>(__delegatingJSONName) {
      @Override
      public DelegatingJSONObject getOperation(TransientRecord record,
                                               Viewpoint viewpoint)
      {
        return new DelegatingJSONObject(record.opt(getAsJSONName()), resolveDefaults(record));
      }
    });
  }

  /**
   * Resolve the JSONObject that contains the default values that this field will use if it doesn't
   * have values defined locally.
   * 
   * @param record
   *          The record that this field is registered on.
   * @return The appropriate set of default values. Should not be null.
   */
  public abstract JSONObject resolveDefaults(TransientRecord record);
}
