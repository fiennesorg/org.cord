package org.cord.mirror.field;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.util.DebugConstants;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;

import com.google.common.base.Preconditions;

public class EnglishNamedImplMapField
  extends EnglishNamedMapField<EnglishNamedImpl>
{
  public static ObjectFieldKey<EnglishNamedImpl> createKey(String key)
  {
    return ObjectFieldKey.create(key, EnglishNamedImpl.class);
  }

  /**
   * Create a new optional MapStringField with the given value as the defaultDefinedValue and null
   * as the newRecordValue.
   */
  public static EnglishNamedImplMapField newOptEnglishNamedImplMapField(ObjectFieldKey<EnglishNamedImpl> fieldName,
                                                                        String label,
                                                                        String defaultName,
                                                                        String defaultEnglishName)
  {
    EnglishNamedImpl def = new EnglishNamedImpl(defaultName, defaultEnglishName);
    EnglishNamedImplMapField f =
        new EnglishNamedImplMapField(fieldName, label, null, FieldValueSuppliers.ofDefined(def));
    f.addValue(def);
    return f;
  }

  /**
   * Create a new compulsory MapStringField with the given value as the defaultDefinedValue
   */
  public static EnglishNamedImplMapField newEnglishNamedImplMapField(ObjectFieldKey<EnglishNamedImpl> fieldName,
                                                                     String label,
                                                                     String defaultName,
                                                                     String defaultEnglishName)
  {
    EnglishNamedImpl def = new EnglishNamedImpl(defaultName, defaultEnglishName);
    EnglishNamedImplMapField f =
        new EnglishNamedImplMapField(fieldName, label, FieldValueSuppliers.ofDefined(def));
    f.addValue(def);
    return f;
  }

  public EnglishNamedImplMapField(ObjectFieldKey<EnglishNamedImpl> fieldName,
                                  String label,
                                  FieldValueSupplier<EnglishNamedImpl> newRecordValue,
                                  FieldValueSupplier<EnglishNamedImpl> defaultDefinedValue)
  {
    super(fieldName,
          label,
          EnglishNamedImpl.class,
          newRecordValue,
          defaultDefinedValue);
  }

  public EnglishNamedImplMapField(ObjectFieldKey<EnglishNamedImpl> fieldName,
                                  String label,
                                  FieldValueSupplier<EnglishNamedImpl> defaultDefinedValue)
  {
    super(fieldName,
          label,
          EnglishNamedImpl.class,
          defaultDefinedValue);
  }

  /**
   * Create a new EnglishNamedImpl and register it as a value.
   * 
   * @return this field so that you can chain invocations.
   */
  public EnglishNamedImplMapField addValue(String sql,
                                           String visibleName)
  {
    EnglishNamedImpl value = new EnglishNamedImpl(sql, visibleName);
    addValue(value);
    return this;
  }

  public static final String GETTABLE_NODE_KEYS = ".node.keys";
  public static final String GETTABLE_APP_KEYS = ".app.keys";
  public static final String GETTABLE_NAME = ".name";
  public static final String GETTABLE_ENGLISHNAME = ".englishName";

  public int addMappings(Gettable gettable,
                         String namespace)
  {
    int c = 0;
    c += addMappings(gettable, namespace, GETTABLE_NODE_KEYS);
    c += addMappings(gettable, namespace, GETTABLE_APP_KEYS);
    return c;
  }

  public int addMappings(Gettable gettable,
                         String namespace,
                         String keysName)
  {
    String keys = gettable.getString(namespace + keysName);
    DebugConstants.DEBUG_OUT.println(String.format("%s --> %s", namespace + keysName, keys));
    if (keys == null) {
      return 0;
    }
    String[] splitKeys = keys.split(",");
    int c = 0;
    for (String key : splitKeys) {
      key = key.trim();
      if (key.length() > 0) {
        String keyspace = namespace + "." + key;
        String name = Preconditions.checkNotNull(gettable.getString(keyspace + GETTABLE_NAME),
                                                 "Unable to resolve %s%s",
                                                 keyspace,
                                                 GETTABLE_NAME);
        String englishName =
            Preconditions.checkNotNull(gettable.getString(keyspace + GETTABLE_ENGLISHNAME),
                                       "Unable to resolve %s%s",
                                       keyspace,
                                       GETTABLE_ENGLISHNAME);
        DebugConstants.DEBUG_OUT.println(String.format("%s: %s --> %s",
                                                       keyspace,
                                                       name,
                                                       englishName));
        addValue(new EnglishNamedImpl(name, englishName));
        c++;
      }
    }
    return c;
  }
}
