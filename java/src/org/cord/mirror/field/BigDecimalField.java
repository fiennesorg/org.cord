package org.cord.mirror.field;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.ObjectField;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Predicate;

/**
 * FieldFilter that utilises the java class BigDecimal to represent the precise value of a floating
 * point field. The BigDecimal field is generated from the toString() method on the default Object
 * representation of the JDBC field so any SQL field type can be utilised that represents a valid
 * floating point number when converted into a String.
 */
public class BigDecimalField
  extends ObjectField<BigDecimal>
{
  public static ObjectFieldKey<BigDecimal> createKey(String name)
  {
    return ObjectFieldKey.create(name, BigDecimal.class);
  }

  public final static BigDecimal ZERO = new BigDecimal("0");

  public final static BigDecimal DEFAULT_VALUE = ZERO;

  public final static int DECIMALPOINTS_VARIABLE = -1;

  public static boolean isZero(BigDecimal value)
  {
    return ZERO.compareTo(value) == 0;
  }

  // private final BigDecimal __defaultValue;

  private final int __decimalPoints;

  /**
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param decimalPoints
   *          The number of decimal points that field values will automatically be constrained to. A
   *          value of DECIMALPOINTS_VARIABLE will not apply any constraints to the values,
   *          permitting any number of decimal points.
   * @see #DECIMALPOINTS_VARIABLE
   */
  public BigDecimalField(ObjectFieldKey<BigDecimal> fieldName,
                         String label,
                         BigDecimal defaultValue,
                         int decimalPoints)
  {
    super(fieldName,
          label,
          BigDecimal.class,
          FieldValues.of(defaultValue));
    __decimalPoints = validateDecimalPoints(decimalPoints);
  }

  /**
   * Create a BigDecimalField that supports NULL values.
   */
  public BigDecimalField(ObjectFieldKey<BigDecimal> fieldName,
                         String label,
                         BigDecimal initValue,
                         BigDecimal defaultValue,
                         int decimalPoints)
  {
    super(fieldName,
          label,
          BigDecimal.class,
          FieldValues.of(initValue),
          FieldValues.of(defaultValue));
    __decimalPoints = validateDecimalPoints(decimalPoints);
  }

  private static int validateDecimalPoints(int decimalPoints)
  {
    return decimalPoints < 0 ? DECIMALPOINTS_VARIABLE : decimalPoints;
  }

  private static BigDecimal validateDecimalPoints(BigDecimal value,
                                                  int decimalPoints)
  {
    if (decimalPoints == DECIMALPOINTS_VARIABLE || value.scale() == decimalPoints) {
      return value;
    } else {
      return value.setScale(decimalPoints, BigDecimal.ROUND_HALF_UP);
    }
  }

  public BigDecimalField(ObjectFieldKey<BigDecimal> fieldName,
                         String label)
  {
    this(fieldName,
         label,
         DEFAULT_VALUE,
         DECIMALPOINTS_VARIABLE);
  }

  @Override
  public BigDecimal objectToValue(TransientRecord record,
                                  Object obj)
      throws NumberFormatException
  {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return validateDecimalPoints((BigDecimal) obj, __decimalPoints);
    }
    return validateDecimalPoints(new BigDecimal(obj.toString()), __decimalPoints);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    BigDecimal sqlValue)
  {
    buffer.append(sqlValue);
  }

  @Override
  public BigDecimal validateDefinedValue(TransientRecord record,
                                         Object obj)
  {
    try {
      return objectToValue(record, obj);
    } catch (NumberFormatException nfeEx) {
      return getDefaultDefinedValue(record);
    }
  }

  @Override
  public BigDecimal dbToValue(Object value,
                              Object[] values,
                              int[] intValues)
  {
    return validateDecimalPoints((BigDecimal) value, __decimalPoints);
  }

  @Override
  public String toString()
  {
    return getName() + "(BigDecimal:" + getDefaultDefinedValue(null) + ")";
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(new Predicate<String>() {
      @Override
      public boolean apply(String input)
      {
        if (!input.startsWith("decimal(")) {
          return false;
        }
        return true;
      }
    }, sqlDef);
  }

  @Override
  protected void populateDefinedInsert(PreparedStatement preparedStatement,
                                       BigDecimal value)
      throws SQLException
  {
    preparedStatement.setBigDecimal(getSqlFieldIndex(), value);
  }

  @Override
  protected int getSqlType()
  {
    return Types.DECIMAL;
  }

}
