package org.cord.mirror.field;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.util.LogicException;
import org.json.JSONException;

import com.google.common.base.Preconditions;

public abstract class AbstractJSONField<J>
  extends StringField
{
  private final RecordOperationKey<J> __asJSONName;
  private final Object __setJSONKey;
  private final Class<J> __jsonClass;

  protected AbstractJSONField(ObjectFieldKey<String> name,
                              String label,
                              RecordOperationKey<J> asJSONName,
                              J defaultValue,
                              Class<J> jsonClass)
  {
    super(name,
          label,
          LENGTH_MEDIUMTEXT,
          defaultValue.toString());
    __asJSONName = Preconditions.checkNotNull(asJSONName, "asJSONName");
    __setJSONKey = new Object();
    __jsonClass = Preconditions.checkNotNull(jsonClass, "jsonClass");
  }

  @Override
  public String validateDefinedValue(TransientRecord record,
                                     Object obj)
  {
    String string = obj.toString();
    String validated = super.validateDefinedValue(record, string);
    if (__jsonClass.isAssignableFrom(obj.getClass()) && string.length() == validated.length()) {
      record.setCachedValue(getAsJSONName(), obj);
      record.setCachedValue(__setJSONKey, Boolean.TRUE);
    }
    return validated;
  }

  /**
   * Parse the value of this field from the serialized form on record into the JSON code represented
   * as a J. The system will do its best to ensure that this isn't called too often and cache the
   * results as appropriate.
   * 
   * @param record
   *          The record whose value is to be converted into JSON
   * @return The appropriate JSON value as an instance of J
   */
  protected abstract J asJSON(TransientRecord record)
      throws JSONException;

  public final RecordOperationKey<J> getAsJSONName()
  {
    return __asJSONName;
  }

  @Override
  protected void register(Table table,
                          int fieldId)
      throws MirrorTableLockedException
  {
    super.register(table, fieldId);
    table.addRecordOperation(new SimpleRecordOperation<J>(__asJSONName) {
      @Override
      public J getOperation(TransientRecord targetRecord,
                            Viewpoint viewpoint)
      {
        @SuppressWarnings("unchecked")
        J value = (J) targetRecord.getCachedValue(__asJSONName);
        if (value == null) {
          try {
            value = asJSON(targetRecord);
          } catch (JSONException e) {
            throw new LogicException(e);
          }
          targetRecord.setCachedValue(__asJSONName, value);
        }
        return value;
      }
    });
    table.addFieldTrigger(new AbstractFieldTrigger<String>(AbstractJSONField.this.getKey(),
                                                           true,
                                                           false) {

      @Override
      protected Object triggerField(TransientRecord record,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        // DebugConstants.method(this, "triggerField", record, fieldName, transaction);
        // DebugConstants.variable("record.__setJSONKey", record.getCachedValue(__setJSONKey));
        if (Boolean.TRUE.equals(record.getCachedValue(__setJSONKey))) {
          record.setCachedValue(__setJSONKey, Boolean.FALSE);
          return null;
        }
        try {
          // System.err.println("...compiling");
          record.setCachedValue(__asJSONName, asJSON(record));
        } catch (JSONException e) {
          throw new MirrorFieldException("Unable to compile JSON", e, record, getFieldName());
        }
        return null;
      }

      @Override
      public String toString()
      {
        return getTable().getName() + "." + getFieldName().getKeyName() + ".JsonCompilerRebuilder";
      }
    });
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_WRITABLE_UPDATED,
                                                   false,
                                                   table.getName()) {
      @Override
      public void trigger(PersistentRecord record,
                          Transaction transaction,
                          int state)
          throws Exception
      {
        // DebugConstants.method(this, "trigger", record, transaction,
        // StateTrigger.STATES.get(state));
        // System.err.println("...clearing compiled value");
        record.removeCachedValue(__asJSONName);
      }

      @Override
      public String toString()
      {
        return getTable().getName() + "." + __asJSONName.getKeyName() + ".JsonCompilerInvalidator";
      }
    });
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_WRITABLE_PREUPDATE,
                                                   false,
                                                   table.getName()) {
      @Override
      public void trigger(PersistentRecord record,
                          Transaction transaction,
                          int state)
          throws Exception
      {
        // DebugConstants.method(this, "trigger", record, transaction,
        // StateTrigger.STATES.get(state));
        // DebugConstants.variable("persistent", record);
        // DebugConstants.variable("persistent." + getKey(), record.comp(getKey()));
        PersistentRecord writable = transaction.getRecord(record);
        // DebugConstants.variable("writable", writable);
        // DebugConstants.variable("writable." + getKey(), writable.comp(getKey()));
        // DebugConstants.variable("writable." + __asJSONName, writable.comp(__asJSONName));
        // DebugConstants.variable("isUpdated",
        // !writable.comp(__asJSONName)
        // .toString()
        // .equals(record.comp(getKey())));
        boolean isUpdated = !writable.comp(__asJSONName).toString().equals(record.comp(getKey()));
        if (isUpdated) {
          writable.setField(getKey(), writable.comp(__asJSONName));
        }
      }

      public String toString()
      {
        return getTable().getName() + "." + __asJSONName.getKeyName() + ".UpdateTracker";
      }
    });
  }
}
