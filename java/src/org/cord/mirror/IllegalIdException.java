package org.cord.mirror;

import org.cord.util.NumberUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

/**
 * A MirrorRuntimeException that is thrown if an Object is passed as a Record id which cannot be
 * converted into a valid id.
 * 
 * @see NumberUtil#toInteger(Object)
 */
public class IllegalIdException
  extends MirrorRuntimeException
{
  private static final long serialVersionUID = -3649826992105988320L;
  private final Table __table;
  private final Object __id;

  public IllegalIdException(String message,
                            Table table,
                            Object id,
                            Throwable cause)
  {
    super(String.format("%s for '%s' on %s",
                        StringUtils.padEmpty(message, "IllegalIdException"),
                        id,
                        table),
          cause);
    __table = Preconditions.checkNotNull(table, "table");
    __id = id;
  }
  public final Table getTable()
  {
    return __table;
  }

  public final Object getId()
  {
    return __id;
  }

}
