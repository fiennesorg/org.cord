package org.cord.mirror;

import org.cord.util.TypedKey;

public class RecordSourceOperationKey<V>
  extends TypedKey<V>
{
  public static <V> RecordSourceOperationKey<V> create(String keyName,
                                                       Class<V> valueClass)
  {
    return new RecordSourceOperationKey<V>(keyName, valueClass);
  }

  public RecordSourceOperationKey(String keyName,
                                  Class<V> valueClass)
  {
    super(keyName,
          valueClass);
  }

  @Override
  public RecordSourceOperationKey<V> copy()
  {
    return create(getKeyName(), getValueClass());
  }

}
