package org.cord.mirror;

/**
 * An Object that wants to be notified when a record is retrieved from the SQL database.
 * 
 * @author alex
 */
public interface PostRetrievalTrigger
{
  /**
   * Notify that the given record has been retrieved from the database. At this point the record
   * will be in the cache.
   */
  public void trigger(PersistentRecord record);
}
