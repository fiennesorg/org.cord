package org.cord.mirror;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.cord.util.Named;

public interface LockableOperation
  extends Named, Lockable
{
  /**
   * Constant for identifying a RecordOperation that has just been created, but has not yet been
   * initialised.
   */
  public final static int LO_NEW = 0;

  /**
   * Constant for identifying a RecordOperation that has just been initialised, but hasn't been
   * prelocked.
   */
  public final static int LO_INITIALISED = 1;

  /**
   * Constant for identifying a RecordOperation that has just been prelocked, but hasn't been
   * locked.
   */
  public final static int LO_PRELOCKED = 2;

  /**
   * Constant for identifying a RecordOperation that has been locked.
   */
  public final static int LO_LOCKED = 3;

  public final static List<String> LO_NAMES =
      Collections.unmodifiableList(Arrays.asList(new String[] { "new", "initialised", "prelocked",
          "locked" }));

  /**
   * Inform the LockableOperation that the database has been initialised and that it should perform
   * any appropriate "side-effects" from the initialisation at this point. If this involves creating
   * new RecordOperations then the responsibility for preLock()ing these RecordOperations lies with
   * the creating class.
   * <p>
   * This may be called multiple times as different sections of the database may initialise
   * themselves at different times as and when they feel that they are now in a position to
   * preLock() themselves. However, only the first call should be honoured and successive calls
   * should be ignored.
   */
  public void preLock()
      throws MirrorTableLockedException;

  /**
   * Inform the FieldFilter that it has entered into the initialisation process for a given Table.
   * 
   * @param table
   *          The Table which this RecordOperation is being stored on.
   */
  public void initialise(Table table);

  /**
   * Get the current lock status of the RecordOperation.
   * 
   * @return The identifier for the lock status of the RecordOperation.
   */
  public int getLockStatus();

  /**
   * Request a non-final Lockable whether it permits itself to be substituted by a new
   * LockableOperation on its registered Table.
   * 
   * @param substitute
   *          The LockableOperation that it is getting replaced by.
   * @return true if the de-initialisation is approved of. False if the substitute should be
   *         rejected and the current LockableOperation left in place.
   */
  public boolean mayDeInitialise(LockableOperation substitute);

  public Object clone()
      throws CloneNotSupportedException;
}
