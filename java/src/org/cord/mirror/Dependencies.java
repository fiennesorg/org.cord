package org.cord.mirror;

import java.util.Iterator;

import com.google.common.collect.ImmutableList;

/**
 * Interface to describe RecordOperations that establish links between the source record and other
 * records that make the other records dependent on the source record for their existence. If the
 * source record is committed into a Transaction for deletion, then all the dependent records (and
 * their dependent records) should also be committed.
 * 
 * @see PersistentRecord#getDependentRecords(boolean,Transaction)
 */
public interface Dependencies
{
  /**
   * ImmutableList of String versions of the tag values in order from smallest to largest.
   */
  public final static ImmutableList<String> TAG_NAMES =
      ImmutableList.of("BIT_T_ON_S_UPDATE",
                       "BIT_T_ON_S_DELETE",
                       "BIT_S_ON_T_UPDATE",
                       "BIT_S_ON_T_DELETE",
                       "BIT_AUTO_CREATE",
                       "BIT_ENFORCED",
                       "BIT_OPTIONAL",
                       "BIT_AUTO_LINK",
                       "BIT_OPTIONAL_AUTOCLEAR",
                       "BIT_OPTIONAL_DELETEIFDEFINED");

  /**
   * Lock the target when the source is locked for an update.
   */
  public final static int BIT_T_ON_S_UPDATE = 1;

  /**
   * Lock the target when the source is locked for a delete.
   */
  public final static int BIT_T_ON_S_DELETE = 2;

  /**
   * Lock the source when the target is locked for an update
   */
  public final static int BIT_S_ON_T_UPDATE = 4;

  /**
   * Lock the source when the target is locked for a delete
   */
  public final static int BIT_S_ON_T_DELETE = 8;

  /**
   * Force the creation of a new target record on instantiation of the source if the link is not
   * completed.
   */
  public final static int BIT_AUTO_CREATE = 16;

  /**
   * Force the registration of a LinkTriggerOperation on the source field that validates any values
   * to ensure that they are pointing to a correct target record. If this is not the case then a
   * MirrorFieldException is thrown.
   * 
   * @see MirrorFieldException
   */
  public final static int BIT_ENFORCED = 32;

  /**
   * Permit a valid value of "no link" or 0 to be stored in the field. This takes preference over
   * BIT_AUTO_LINK and BIT_AUTO_CREATE.
   * 
   * @see #BIT_AUTO_LINK
   * @see #BIT_AUTO_CREATE
   */
  public final static int BIT_OPTIONAL = 64;

  /**
   * If the field is set to NEW_RECORD (0) then auto-initialise it with the first available record
   * in the TargetTable according to the getAllRecordsQuery(). This takes preference over
   * BIT_AUTO_CREATE.
   * 
   * @see #BIT_AUTO_CREATE
   */
  public final static int BIT_AUTO_LINK = 128;

  /**
   * If you delete a record that is pointed at by links that are BIT_OPTIONAL and
   * BIT_OPTIONAL_AUTOCLEAR then an update transaction will automatically set all of the referencing
   * records to 0. Be aware that this may be a lengthy procedure, and in the current model there is
   * no support for pre-testing the Transactional integrity of the operation. ie if there are
   * externally locked 3rd party records then the operation will not update them and may take time
   * to timeout on the references.
   */
  public final static int BIT_OPTIONAL_AUTOCLEAR = 256;

  /**
   * If you delete a record that is pointed at by links that are BIT_OPTIONAL and
   * BIT_OPTIONAL_DELETEIFDEFINED then the referencing record will be added to the delete
   * Transaction.
   */
  public final static int BIT_OPTIONAL_DELETEIFDEFINED = 512;

  /**
   * Compound value that makes a basic enforced link that deletes the source if the target is
   * deleted.
   * 
   * @see #BIT_S_ON_T_DELETE
   * @see #BIT_ENFORCED
   */
  public final static int LINK_ENFORCED = BIT_S_ON_T_DELETE | BIT_ENFORCED;

  /**
   * Compound value that locks the target whenever any kind of lock on the source is acquired.
   * 
   * @see #BIT_T_ON_S_UPDATE
   * @see #BIT_T_ON_S_DELETE
   */
  public final static int LOCK_T_ON_S = BIT_T_ON_S_UPDATE | BIT_T_ON_S_DELETE;

  /**
   * Compound value that locks the source whenever any kind of lock on the target is acquired.
   * 
   * @see #BIT_S_ON_T_UPDATE
   * @see #BIT_S_ON_T_DELETE
   */
  public final static int LOCK_S_ON_T = BIT_S_ON_T_UPDATE | BIT_S_ON_T_DELETE;

  /**
   * Compound value that strongly ties the source and target together with auto-creation. This
   * should be used in situations when the two records are conceptually a single compound data item
   * and as such should be treated as a single atomic object.
   * 
   * @see #LOCK_T_ON_S
   * @see #LOCK_S_ON_T
   * @see #BIT_AUTO_CREATE
   * @see #BIT_ENFORCED
   */
  public final static int LINK_ALL = LOCK_T_ON_S | LOCK_S_ON_T | BIT_AUTO_CREATE | BIT_ENFORCED;

  /**
   * Check to see if the object in question has the potential of having any dependencies for the
   * record in question.
   * 
   * @param record
   *          The record that is being checked to see if it has any dependencies.
   * @param transactionType
   *          The type of transaction that the dependencies are going to be checked for.
   * @return true if it is possible that the record has dependencies, false if it is definately not
   *         possible that the record has dependencies.
   * @see Transaction#TRANSACTION_UPDATE
   * @see Transaction#TRANSACTION_DELETE
   */
  public boolean mayHaveDependencies(PersistentRecord record,
                                     int transactionType);

  /**
   * Get a list of all the records that are dependant on a named record for the purposes of a single
   * transaction type.
   * 
   * @param record
   *          The record which is being dropped into the transaction and whose dependencies are
   *          being tracked.
   * @param transactionType
   *          The transaction type.
   */
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                        int transactionType);

  // throws MirrorTransactionTimeoutException;
  /**
   * Get a list of all the records that are dependant on a named record for the purposes of a single
   * transaction type within the scope of a named transaction. Please note that this transaction may
   * or may not have a lock on record at the time and it may or may not have the same type as
   * transactionType.
   * 
   * @param record
   *          The record which is being dropped into the transaction and whose dependencies are
   *          being tracked.
   * @param transactionType
   *          The transaction type.
   * @param viewpoint
   *          The viewpoint onto the data.
   * @return Iterator of PersistentRecord objects.
   */
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                        int transactionType,
                                                        Viewpoint viewpoint);
  // throws MirrorTransactionTimeoutException;
}
