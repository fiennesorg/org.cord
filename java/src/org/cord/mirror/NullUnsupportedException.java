package org.cord.mirror;

/**
 * MirrorFieldException that is thrown (if enabled) if a null value is set on a field that doesn't
 * support nulls.
 * 
 * @author alex
 * @see Db#CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL
 */
public class NullUnsupportedException
  extends MirrorFieldException
{
  private static final long serialVersionUID = 1L;

  public NullUnsupportedException(String message,
                                  TransientRecord record,
                                  String fieldName)
  {
    super(message,
          null,
          record,
          fieldName,
          null,
          null);
  }
}
