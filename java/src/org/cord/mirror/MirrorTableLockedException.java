package org.cord.mirror;

public class MirrorTableLockedException
  extends MirrorException
{
  private static final long serialVersionUID = -1889749576056512084L;

  private final Table __table;

  private final String __operation;

  private final Object __parameter;

  public MirrorTableLockedException(Table table,
                                    String operation,
                                    Object parameter,
                                    Exception nestedException)
  {
    super(table + "-" + operation + "(" + parameter + ")",
          nestedException);
    __table = table;
    __operation = operation;
    __parameter = parameter;
  }

  public final Table getTable()
  {
    return __table;
  }

  public final Object getParameter()
  {
    return __parameter;
  }

  public final String getOperation()
  {
    return __operation;
  }
}
