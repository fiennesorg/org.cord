package org.cord.mirror;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

import org.cord.mirror.cache.RecordCache;
import org.cord.mirror.cache.RecordCacheFactory;
import org.cord.mirror.operation.DuoRecordOperation;
import org.cord.mirror.operation.DuoRecordOperationKey;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.TrioRecordOperation;
import org.cord.mirror.recordsource.AbstractRecordSource;
import org.cord.mirror.recordsource.EmptyIdList;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.DuoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.DuoRecordSourceOperationKey;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.sql.ConnectionFacade;
import org.cord.sql.SqlUtil;
import org.cord.util.AbstractNamedGettable;
import org.cord.util.Assertions;
import org.cord.util.DebugConstants;
import org.cord.util.DuplicateNamedException;
import org.cord.util.EmptyListIterator;
import org.cord.util.Expirable;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.cord.util.SortedIterator;
import org.cord.util.UndefinedCompException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * Table holds all the information that about a single table in a Db.
 */
public final class Table
  extends AbstractNamedGettable
  implements TableWrapper, RecordSource, Expirable
{
  /**
   * Create a Table FieldValueSupplier that looks up a Table by name. This is useful for situations
   * when you need to create the supplier before the Table has been initialised, but where the Table
   * will be there when the supplier is invoked.
   */
  public static FieldValueSupplier<Table> getFieldValueSupplier(final Db db,
                                                                final String tablename)
  {
    return new FieldValueSupplier<Table>() {
      @Override
      public Table supplyValue(TransientRecord record)
      {
        return db.getTable(tablename);
      }
    };
  }

  public final static int ID_TRANSIENT = 0;

  public final static String FIELDNAME_ID = "id";

  /**
   * The filter ("true") that is used to return all the records in the Table
   * 
   * @see #getAllRecordsQuery()
   */
  public final String FILTER_ALLRECORDS;

  private Db _db;

  private final Int2ObjectMap<PersistentRecord> __lockedRecordMap;

  private volatile String _titleOperationName;

  private volatile String _defaultOrdering;

  private final Map<String, RecordOperation<?>> __recordOperations =
      new HashMap<String, RecordOperation<?>>();

  private final Map<String, RecordSourceOperation<?>> __recordSourceOperations =
      new HashMap<String, RecordSourceOperation<?>>();

  private final List<Field<?>> __fields = new ArrayList<Field<?>>();

  private final List<Field<?>> __roFields = Collections.unmodifiableList(__fields);

  private final Map<String, Field<?>> __name_field = new HashMap<String, Field<?>>();

  private final Set<TableListener> __tableListeners = new CopyOnWriteArraySet<TableListener>();

  private final ListMultimap<String, FieldTrigger<?>> __fieldTriggers = ArrayListMultimap.create();

  private final List<FieldTrigger<?>> __instantiationFieldTriggers = Lists.newArrayList();

  private final ListMultimap<String, PostFieldTrigger<?>> __postFieldTriggers =
      ArrayListMultimap.create();

  private final List<FieldTrigger<?>> __globalFieldTriggers = Lists.newArrayList();

  private final List<PostFieldTrigger<?>> __globalPostFieldTriggers = Lists.newArrayList();

  private final List<PreInstantiationTrigger> __preInstantiationTriggers =
      new ArrayList<PreInstantiationTrigger>();

  private final List<PostInstantiationTrigger> __postInstantiationTriggers =
      new ArrayList<PostInstantiationTrigger>();

  private final List<PostRetrievalTrigger> __postRetrievalTriggers =
      new ArrayList<PostRetrievalTrigger>();

  private final List<List<StateTrigger>> __stateTriggers = new ArrayList<List<StateTrigger>>();

  private final List<RecordStateDataFilter> __recordStateDataFilters = Lists.newArrayList();

  private final List<RecordStateDataFilter> __unmodifiableRecordStateDataFilters =
      Collections.unmodifiableList(__recordStateDataFilters);

  private void addRecordStateDataFilter(RecordStateDataFilter recordStateDataFilter)
  {
    __recordStateDataFilters.add(Preconditions.checkNotNull(recordStateDataFilter));
  }

  public List<RecordStateDataFilter> getUnmodifiableRecordStateDataFilters()
  {
    return __unmodifiableRecordStateDataFilters;
  }

  private volatile long _updateTimestamp = System.currentTimeMillis();

  private int _updateCount = 0;

  private long _updateTime = 0;

  private int _deleteCount = 0;

  private long _deleteTime = 0;

  private int _readCount = 0;

  private long _readTime = 0;

  private int _insertCount = 0;

  private long _insertTime = 0;

  private int _objectValueIndex = 0;

  public int getNextObjectValueIndex(Object requestor)
      throws MirrorTableLockedException
  {
    checkLockedStatus("getNextObjectValueIndex", requestor);
    return _objectValueIndex++;
  }

  public int getObjectValueIndex()
  {
    return _objectValueIndex;
  }

  private int _intValueIndex = 0;

  public int getNextIntValueIndex(Object requestor)
      throws MirrorTableLockedException
  {
    checkLockedStatus("getNextIntValueIndex", requestor);
    return _intValueIndex++;
  }

  public int getIntValueIndex()
  {
    return _intValueIndex;
  }

  private static final Table[] EMPTYTABLEARRAY = new Table[0];

  /**
   * Convert a Collection of Table Objects into an Array of Table Objects. This reuses a single zero
   * length Table array as the template and saves you having to boot one in your client code.
   */
  public static Table[] toArray(Collection<Table> tables)
  {
    return tables.toArray(EMPTYTABLEARRAY);
  }

  private ConcurrentMap<Connection, PreparedStatement> __insertStatements =
      new ConcurrentHashMap<Connection, PreparedStatement>();

  PreparedStatement getInsertPreparedStatement(Connection connection)
      throws SQLException
  {
    PreparedStatement ps = __insertStatements.get(connection);
    if (ps == null) {
      ps = prepareInsertStatement(connection);
      if (DebugConstants.DEBUG_SQL_PREPAREDSTATEMENTS) {
        System.err.println(connection + " --> " + ps);
      }
      __insertStatements.put(connection, ps);
    }
    return ps;
  }

  private ConcurrentMap<Connection, PreparedStatement> __retrieveStatements =
      new ConcurrentHashMap<Connection, PreparedStatement>();

  PreparedStatement getRetrievePreparedStatement(Connection connection)
      throws SQLException
  {
    PreparedStatement ps = __retrieveStatements.get(connection);
    if (ps == null) {
      ps = connection.prepareStatement(_retrievePreparedStatement);
      if (DebugConstants.DEBUG_SQL_PREPAREDSTATEMENTS) {
        System.err.println(connection + " --> " + ps);
      }
      __retrieveStatements.put(connection, ps);
    }
    return ps;
  }

  private void initialiseStateTriggers()
  {
    for (int i = 0; i < StateTrigger.STATE_COUNT; i++) {
      __stateTriggers.add(new ArrayList<StateTrigger>());
    }
  }

  public int getInsertCount()
  {
    return _insertCount;
  }

  public long getInsertTime()
  {
    return _insertTime;
  }

  protected void updateInsertStats(int count,
                                   long time)
  {
    _insertCount += count;
    _insertTime += time;
  }

  public int getUpdateCount()
  {
    return _updateCount;
  }

  public long getUpdateTime()
  {
    return _updateTime;
  }

  protected void updateUpdateStats(int count,
                                   long time)
  {
    _updateCount += count;
    _updateTime += time;
  }

  public int getDeleteCount()
  {
    return _deleteCount;
  }

  public long getDeleteTime()
  {
    return _deleteTime;
  }

  protected void updateDeleteStats(int count,
                                   long time)
  {
    _deleteCount += count;
    _deleteTime += time;
  }

  public int getReadCount()
  {
    return _readCount;
  }

  public long getReadTime()
  {
    return _readTime;
  }

  protected void updateReadStats(int count,
                                 long time)
  {
    _readCount += count;
    _readTime += time;
  }

  private long _queryTime = 0;

  private int _queryCount = 0;

  public long getQueryTime()
  {
    return _queryTime;
  }

  public int getQueryCount()
  {
    return _queryCount;
  }

  public long getQueryAverageTime()
  {
    if (_queryCount == 0) {
      return 0;
    }
    return (_queryTime * 100) / _queryCount;
  }

  protected void updateQueryStats(long queryTime)
  {
    _queryTime += queryTime;
    _queryCount++;
  }

  private void updateTimestamp()
  {
    _updateTimestamp = System.currentTimeMillis();
  }

  /**
   * Get the timestamp of when a Transaction was last invoked against this Table. The Table should
   * not have changed state between then and now.
   * 
   * @return Unix timestamp value
   */
  public long getUpdateTimestamp()
  {
    return _updateTimestamp;
  }

  /**
   * Get the number of StateTrigger Objects of a given type that are currently registered on this
   * Table.
   */
  public int getStateTriggerCount(int stateTriggerType)
  {
    return __stateTriggers.get(stateTriggerType).size();
  }

  private boolean _locked = false;

  private final RecordOperationKey<RecordSource> __referencesName;

  /**
   * Construct the default form of the referencesName from the tableName by dropping the case of the
   * first character and appending an s (TableName --> tableNames)
   */
  public static RecordOperationKey<RecordSource> getDefaultReferencesName(String tableName)
  {
    return RecordOperationKey.create(Character.toLowerCase(tableName.charAt(0))
                                     + tableName.substring(1) + 's',
                                     RecordSource.class);
  }

  /**
   * Replace invalid reference names with the default reference name.
   * 
   * @see #getDefaultReferencesName(String)
   */
  public static RecordOperationKey<RecordSource> getDefaultReferencesName(String tableName,
                                                                          RecordOperationKey<RecordSource> referencesName)
  {
    if (referencesName != null) {
      return referencesName;
    }
    return getDefaultReferencesName(tableName);
  }

  private final TableWrapper __tableWrapper;

  private final RecordCache __recordCache;
  private final Cache<Object, RecordSource> __recordSourceCache;

  public void invalidateCachedRecordSource(Object key)
  {
    __recordSourceCache.invalidate(key);
  }

  public void invalidateCachedRecordSources()
  {
    __recordSourceCache.invalidateAll();
  }

  private final PersistentRecord __missingRecord;

  private final int __precacheSize;

  /**
   * @param referencesName
   *          The name of the default RecordOperation that will resolve to Queries from this Table.
   *          If this is null, then the name will given by
   *          {@link #getDefaultReferencesName(String, RecordOperationKey)}
   */
  protected Table(String name,
                  RecordOperationKey<RecordSource> referencesName,
                  Db db,
                  int precacheSize,
                  CacheBuilderSpec recordSourceCacheSpec,
                  TableWrapper tableWrapper)
  {
    super(name);
    if (tableWrapper == null) {
      __tableWrapper = this;
      if (DebugConstants.DEBUG_TABLES_WITHOUT_TABLEWRAPPERS) {
        DebugConstants.DEBUG_OUT.println(String.format("%s.%s has no TableWrapper",
                                                       db.getName(),
                                                       name));
      }
    } else {
      if (!tableWrapper.getTableName().equals(name)) {
        throw new IllegalArgumentException(String.format("Explicit name of \"%s\" doesn't match TableWrapper \"%s\" name of \"%s\"",
                                                         name,
                                                         tableWrapper,
                                                         tableWrapper.getTableName()));
      }
      __tableWrapper = tableWrapper;
    }
    _db = db;
    __lockedRecordMap = Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<PersistentRecord>());
    _titleOperationName = null;
    __precacheSize = precacheSize;
    __missingRecord =
        new PersistentRecord(this, new Object[0], new int[] { TransientRecord.ID_MISSING }, null);
    __referencesName = getDefaultReferencesName(name, referencesName);
    try {
      addField(new IntField(TransientRecord.FIELD_ID.copy(), null, TransientRecord.ID_TRANSIENT));
    } catch (MirrorTableLockedException mtlEx) {
      throw new LogicException("Table locked during initialiser", mtlEx);
    }
    Gettable mirrorProperties = db.getConfiguration();
    String recordCacheFactoryClassName =
        mirrorProperties.getString(DbBooter.CONF_RECORDCACHEFACTORY + "." + name);
    recordCacheFactoryClassName = recordCacheFactoryClassName != null
        ? recordCacheFactoryClassName
        : MoreObjects.firstNonNull(mirrorProperties.getString(DbBooter.CONF_RECORDCACHEFACTORY),
                                   DbBooter.CONF_RECORDCACHEFACTORY_DEFAULT);
    System.err.println(name + " :: " + recordCacheFactoryClassName);
    try {
      __recordCache = ObjectUtil
                                .castTo(Class.forName(recordCacheFactoryClassName).newInstance(),
                                        RecordCacheFactory.class)
                                .createRecordCache(mirrorProperties, this);
    } catch (Exception e) {
      throw new IllegalArgumentException("Error booting Table " + name + " with "
                                         + recordCacheFactoryClassName + " --> " + e,
                                         e);
    }
    FILTER_ALLRECORDS = name + "." + TransientRecord.FIELD_ID + ">0";
    CacheBuilder<Object, Object> recordSourceCacheBuilder =
        CacheBuilder.from(recordSourceCacheSpec);
    if (DebugConstants.DEBUG_TABLE_CACHEBUILDERSPEC) {
      System.err.println(name + ".recordSourceCache: " + recordSourceCacheSpec);
    }
    if (DebugConstants.DEBUG_CACHE_EVICTION) {
      recordSourceCacheBuilder =
          recordSourceCacheBuilder.removalListener(new RemovalListener<Object, Object>() {
            @Override
            public void onRemoval(RemovalNotification<Object, Object> notification)
            {
              System.err.println(Table.this.getName() + ".RecordSource: " + notification.getValue()
                                 + " was expired because " + notification.getCause());
            }
          });
    }
    __recordSourceCache = recordSourceCacheBuilder.build();

    initialiseStateTriggers();
    setDefaultOrdering(null);
    db.addExpirable(this);
  }

  public String toConfigString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("name", getName())
                      // .add("recordCacheSpec", __recordCacheSpec)
                      .add("preCacheSize", getPrecacheSize())
                      .toString();
  }

  public int getPrecacheSize()
  {
    return __precacheSize;
  }

  /**
   * @return immutable empty RecordIdList from this Table
   */
  public IdList getEmptyIds()
  {
    return __emptyIdList;
  }

  public PersistentRecord loadRecord(int key)
      throws SQLException
  {
    if (DebugConstants.DEBUG_SQL) {
      DebugConstants.method(this, "loadRecord", Integer.valueOf(key));
    }
    PersistentRecord lockedRecord = __lockedRecordMap.get(key);
    if (lockedRecord != null) {
      return lockedRecord;
    }
    ConnectionFacade connectionFacade = getDb().getThreadConnectionFacade().get();
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    try {
      connection = connectionFacade.checkOutConnection();
      preparedStatement = getRetrievePreparedStatement(connection);
      preparedStatement.setInt(1, key);
      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        return PersistentRecord.getInstance(Table.this, resultSet);
      }
      return __missingRecord;
    } catch (SQLException sqlEx) {
      connectionFacade.checkInBrokenConnection(connection);
      connection = null;
      throw sqlEx;
    } finally {
      connectionFacade.checkInWorkingConnection(connection);
    }
  }

  /**
   * Get the TableWrapper that was passed to the constructor of this Table and which conceptually
   * encloses the Table with additional functionality. If there was no TableWrapper then the Table
   * is returned as Table is itself a TableWrapper and can therefore wrap itself.
   * 
   * @return Implementation of TableWrapper. Never null.
   */
  public final TableWrapper getTableWrapper()
  {
    return __tableWrapper;
  }

  /**
   * Check to see if this Table has the given name and throw an IllegalArgumentException if not.
   * 
   * @throws IllegalArgumentException
   *           If the name of this Table doesn't match name
   */
  public void assertIsNamed(String name)
  {
    if (!getName().equals(name)) {
      throw Assertions.printStackTrace(new IllegalArgumentException(this + " is not named \"" + name
                                                                    + "\""));
    }
  }

  /**
   * Get the default references name that should be used for resolved lists of records from this
   * Table. This is either initialised in the constructor or deduced from the Table name.
   * 
   * @return The default references name. Never null. This is a constant within the lifespan of the
   *         Table.
   */
  public RecordOperationKey<RecordSource> getReferencesName()
  {
    return __referencesName;
  }

  /**
   * Set a new value for the default ordering of records from this Table. If orderClause is null or
   * empty then the default value of " <em>tableName.id</em>" will be utilised. This will also clear
   * the QueryCache for the Table to ensure that any Queries that have been built with the previous
   * default ordering are discarded and then rebuilt when next asked for with the new default
   * ordering.
   * 
   * @param orderClause
   *          The ordering that should be applied to future queries. Note that this should include
   *          the Table name as a qualifier to avoid situations when it is used in ambigious
   *          situations involving joins, eg an ordering of "field1, field2" on a table Acme should
   *          be registered as "Acme.field1, Acme.field2". Failure to do this can result in
   *          SQLExceptions being generated on compound queries.
   * @see #getDefaultOrdering()
   */
  public void setDefaultOrdering(String orderClause)
  {
    _defaultOrdering = Strings.emptyToNull(orderClause);
    _recordOrdering = null;
    __recordSourceCache.invalidateAll();
  }

  /**
   * Get the default ordering of records extracted from this Table. If no default ordering is
   * defined, then this returns [table-name].id
   * 
   * @return The default ordering as set by setDefaultOrdering(orderClause) or FIELDNAME_ID if the
   *         clause has not been set. Never null.
   * @see #setDefaultOrdering(String)
   * @see #FIELDNAME_ID
   */
  public String getDefaultOrdering()
  {
    return _defaultOrdering;
  }

  public boolean hasTitleOperationName()
  {
    return _titleOperationName != null;
  }

  /**
   * Get the name of the RecordOperation on this Table that should be used to retrieve a sensible
   * human-readable name for a record.
   * 
   * @return The name of the RecordOperation. This will be FIELDNAME_ID (id) if it hasn't been
   *         explicitly defined
   * @see #FIELDNAME_ID
   */
  public String getTitleOperationName()
  {
    return hasTitleOperationName() ? _titleOperationName : FIELDNAME_ID;
  }

  public void setTitleOperationName(RecordOperationKey<?> titleOperationName)
  {
    _titleOperationName = titleOperationName.getKeyName();
  }

  /**
   * @return The Table name, equivalent to getName()
   * @see #getName()
   */
  @Override
  public String getTableName()
  {
    return getName();
  }

  public String debug()
  {
    try {
      StringBuilder result = new StringBuilder();
      result.append("Table.debug()")
            .append("\n - name:")
            .append(getName())
            .append("\n  - titleFieldName:")
            .append(_titleOperationName)
            .append("\n  - defaultOrdering:")
            .append(getDefaultOrdering())
            .append("\n  - recordOperations:-");
      Iterator<String> recordOperationNameList =
          new SortedIterator<String>(__recordOperations.keySet().iterator());
      while (recordOperationNameList.hasNext()) {
        String recordOperationName = recordOperationNameList.next();
        result.append("\n    - ")
              .append(recordOperationName)
              .append(":")
              .append(__recordOperations.get(recordOperationName));
      }
      result.append("\n  - fieldTriggers:-");
      Iterator<String> fieldTriggerNames =
          new SortedIterator<String>(__fieldTriggers.keySet().iterator());
      while (fieldTriggerNames.hasNext()) {
        String fieldName = fieldTriggerNames.next();
        result.append("\n    - ").append(fieldName);
        for (FieldTrigger<?> fieldTrigger : __fieldTriggers.get(fieldName)) {
          result.append("\n      - ").append(fieldTrigger);
        }
      }
      result.append("\n  - instantiation triggers:-");
      Iterator<PreInstantiationTrigger> instantiationTriggerList =
          __preInstantiationTriggers.iterator();
      while (instantiationTriggerList.hasNext()) {
        result.append("\n    - ").append(instantiationTriggerList.next());
      }
      result.append("\n  - RecordSourceOperations:-");
      Iterator<RecordSourceOperation<?>> recordSourceOperations =
          new SortedIterator<RecordSourceOperation<?>>(getRecordSourceOperations().values()
                                                                                  .iterator());
      while (recordSourceOperations.hasNext()) {
        result.append("\n    - ").append(recordSourceOperations.next());
      }
      result.append("\n - StateTriggers:-");
      for (int i = 0; i < __stateTriggers.size(); i++) {
        if (__stateTriggers.get(i).size() > 0) {
          result.append("\n    - ").append(StateTrigger.STATES.get(i));
          Iterator<StateTrigger> stateTriggers = __stateTriggers.get(i).iterator();
          while (stateTriggers.hasNext()) {
            result.append("\n      - ").append(stateTriggers.next());
          }
        }
      }
      result.append("\n - TableListeners:-");
      for (TableListener tl : __tableListeners) {
        result.append("\n   - ").append(tl);
      }
      return result.toString();
    } catch (Exception ex) {
      ex.printStackTrace();
      return ex.toString();
    }
  }

  /**
   * Get a standard query that maps to all the records in this table sorted by id. The query is
   * cached at startup, but the contents will not be resolved until the first use of the query by a
   * client. Please note: it is not totally advisable to use this in situations when the number of
   * records is extremely high.
   * 
   * @return Query containing all records
   * @see #setDefaultOrdering(String)
   */
  public Query getAllRecordsQuery()
  {
    Query query = getQuery(FILTER_ALLRECORDS, FILTER_ALLRECORDS, null);
    return query;
  }

  /**
   * @see #getAllRecordsQuery()
   */
  public Query getAll()
  {
    return getAllRecordsQuery();
  }

  /**
   * Utility method that gets a list of all the PersistentRecord objects in this current table. This
   * therefore permits webmacro templates to iterate across the table like so:- <BLOCKQUOTE>
   * #foreach $record in $table { some operations on $record } </BLOCKQUOTE> The method is basically
   * just a wrapper around the Query resolution returned by getAllRecordsQuery().
   * 
   * @see #getAllRecordsQuery()
   */
  @Override
  public Iterator<PersistentRecord> iterator()
  {
    return getAllRecordsQuery().getRecordList().iterator();
  }

  private void lock(LockableOperation operation)
      throws MirrorTableLockedException
  {
    operation.lock();
  }

  private void lock(Iterator<? extends LockableOperation> lockableOperations)
      throws MirrorTableLockedException
  {
    while (lockableOperations.hasNext()) {
      lock(lockableOperations.next());
    }
  }

  /**
   * Inform this Table that it is now locked and its fundamental structure is now fixed. Once a
   * table is locked, it cannot be unlocked. This will prevent access to the following operations:-
   * <ul>
   * <li>add(fieldTrigger)
   * <li>add(preInstantiationTrigger)
   * <li>add(postInstantiationTrigger)
   * <li>add(fieldFilter)
   * </ul>
   * This has the effect of being able to analyse the opportunities for MirrorInstantiationException
   * and MirrorFieldExceptions being thrown in the knowledget that you <i>know</i> what has been
   * invoked.
   * 
   * @see #addFieldTrigger(FieldTrigger)
   * @see #addPreInstantiationTrigger(PreInstantiationTrigger)
   * @see #addPostInstantiationTrigger(PostInstantiationTrigger)
   * @see #addField(Field)
   */
  protected void lock()
      throws MirrorTableLockedException
  {
    _locked = true;
    try {
      validateFields();
    } catch (SQLException e) {
      throw new LogicException("Failed to boot " + this + " due to " + e, e);
    }
    lock(getRecordOperations(true));
    lock(getRecordSourceOperations().values().iterator());
  }

  private String _insertPreparedStatement = null;
  private String _retrievePreparedStatement = null;

  private void preLock(LockableOperation operation)
      throws MirrorTableLockedException
  {
    switch (operation.getLockStatus()) {
      case LockableOperation.LO_NEW:
        throw new LogicException(operation + " is in RO_NEW when it should be RO_INITIALISED");
      case LockableOperation.LO_INITIALISED:
        operation.preLock();
        break;
      case LockableOperation.LO_PRELOCKED:
      case LockableOperation.LO_LOCKED:
        // do nothing as it has already been prelocked...
        break;
    }
  }

  private void preLock(Iterator<? extends LockableOperation> lockableOperations)
      throws MirrorTableLockedException
  {
    while (lockableOperations.hasNext()) {
      preLock(lockableOperations.next());
    }
  }

  /**
   * Informed all the registered LockableOperation classes (RecordOperations, TableOperations,
   * QueryOperations) that the Table is moving into the preLocked stage. This will only invoke
   * preLock on each of the registered classes once, enabling the Table.preLock() method to be
   * invoked multiple times without a major performance penalty.
   */
  protected void preLock()
      throws MirrorTableLockedException
  {
    StringBuilder buf = new StringBuilder();
    buf.append("insert into ").append(getName()).append(" values (?");
    for (int i = 1; i < __fields.size(); i++) {
      buf.append(",?");
    }
    buf.append(")");
    _insertPreparedStatement = buf.toString();
    if (_insertId.get() == -1) {
      ConnectionFacade connectionFacade = getDb().getThreadConnectionFacade().get();
      Connection connection = null;
      try {
        connection = connectionFacade.checkOutConnection();
        Statement statement = null;
        try {
          statement = connection.createStatement();
          ResultSet resultSet = statement.executeQuery("select max(id) from " + getName());
          if (resultSet.next()) {
            _insertId.set(Math.max(0, resultSet.getInt(1)));
          } else {
            _insertId.set(0);
          }
        } finally {
          if (statement != null) {
            statement.close();
          }
        }
      } catch (SQLException sqlEx) {
        throw new LogicException("Cannot resolve current id count for " + getName(), sqlEx);
      } finally {
        connectionFacade.checkInWorkingConnection(connection);
      }
    }
    _retrievePreparedStatement = "select * from " + getName() + " where id=?";
    preLock(getRecordOperations(true));
    preLock(getRecordSourceOperations().values().iterator());
  }

  /**
   * Return the current locked status of this Table.
   * 
   * @return true if the table has been locked.
   * @see #lock()
   */
  public boolean isLocked()
  {
    return _locked;
  }

  private void checkLockedStatus(String operation,
                                 Object parameter)
      throws MirrorTableLockedException
  {
    if (_locked) {
      throw new MirrorTableLockedException(this, operation, parameter, null);
    }
  }

  /**
   * Check to see whether the mirror representation of the Table matches the SQL description of the
   * Table and build the list of defaultValues for each fields after it has been deemed appropriate.
   * 
   * @throws SQLException
   *           If the descriptions don't match up.
   */
  public void validateFields()
      throws SQLException
  {
    ConnectionFacade connectionFacade = getDb().getThreadConnectionFacade().get();
    Connection connection = null;
    try {
      connection = connectionFacade.checkOutConnection();
      ResultSet rs = connection.createStatement().executeQuery("describe " + getName());
      if (DebugConstants.DEBUG_SQL_VALIDATEFIELDS) {
        DebugConstants.DEBUG_OUT.println("validateFields: describe " + getName());
      }
      for (int i = 0; i < __fields.size(); i++) {
        Field<?> field = __fields.get(i);
        if (!rs.next()) {
          throw new SQLException("No SQL definition for " + getName() + ":" + field);
        }
        if (DebugConstants.DEBUG_SQL_VALIDATEFIELDS) {
          int cols = rs.getMetaData().getColumnCount();
          DebugConstants.DEBUG_OUT.println("Node: " + field.getName());
          DebugConstants.DEBUG_OUT.print("SQL: ");
          for (int j = 1; j <= cols; ++j) {
            DebugConstants.DEBUG_OUT.print("  [" + rs.getString(j) + "]");
          }
          DebugConstants.DEBUG_OUT.println();
        }
        field.validateSqlDefinition(getName(),
                                    rs.getString(1),
                                    rs.getString(2),
                                    "YES".equals(rs.getString(3)));
      }
      if (rs.next()) {
        String extraField = String.format("(%s:%s)", rs.getString(1), rs.getString(2));
        throw new SQLException(getName() + ": more sql fields than mirror fields: " + extraField);
      }
    } finally {
      connectionFacade.checkInWorkingConnection(connection);
    }
  }

  /**
   * Get the unmodifiable ordered collection of all the field names that have been registered on
   * this Table.
   */
  public Collection<String> getFieldNames()
  {
    return Collections.unmodifiableSet(__name_field.keySet());
  }

  public int getFieldCount()
  {
    return __fields.size();
  }

  public Iterator<FieldTrigger<?>> getFieldTriggers(String fieldName)
  {
    return __fieldTriggers.get(fieldName).iterator();
  }

  protected void uncacheRecord(PersistentRecord record)
  {
    __recordCache.invalidate(record.getId());
  }

  private final AtomicInteger _insertId = new AtomicInteger(-1);

  private final static Object[] ZERO_OBJECTS = new Object[0];
  private final static int[] ZERO_INTS = new int[0];

  /**
   * Create a new TransientRecord with all the values in the record set to the default values as set
   * up by the field filters.
   * 
   * @return A new transient record.
   */
  public TransientRecord createTransientRecord()
  {
    Object[] objectValues = createObjectValues();
    int[] intValues = createIntValues();
    for (Field<?> field : getFields()) {
      field.storeNewRecordValue(objectValues, intValues);
    }
    return new TransientRecord(this, objectValues, intValues, 0, getNextInsertId());
  }

  protected Object[] createObjectValues()
  {
    return getObjectValueIndex() > 0 ? new Object[getObjectValueIndex()] : ZERO_OBJECTS;
  }

  protected int[] createIntValues()
  {
    return getIntValueIndex() > 0 ? new int[getIntValueIndex()] : ZERO_INTS;
  }

  /**
   * Get the next free id that is available on this Table. This will increment the value for the
   * next person to invoke it, so if you invoke it then it is a good idea to use it because you will
   * be the "sole owner" of that id.
   * 
   * @return The next free insert id.
   */
  protected int getNextInsertId()
  {
    return _insertId.incrementAndGet();
  }

  /**
   * Get the id value that was last utilised for generating a TransientRecord. Reading this value
   * does not change the state of the Table.
   * 
   * @return The last insert id as it is currently known.
   */
  public int getLastInsertId()
  {
    return _insertId.get();
  }

  /**
   * Attempt to register a new field trigger on this table.
   * 
   * @param fieldTrigger
   *          The trigger that is to be registered.
   * @throws IllegalArgumentException
   *           If the field name of the field trigger does not have a corresponding field on this
   *           table.
   */
  public <T> FieldTrigger<T> addFieldTrigger(FieldTrigger<T> fieldTrigger)
      throws MirrorTableLockedException
  {
    checkLockedStatus("add", fieldTrigger);
    FieldKey<T> fieldName = fieldTrigger.getFieldName();
    if (fieldName == null) {
      __globalFieldTriggers.add(fieldTrigger);
    } else {
      if (getField(fieldName) == null) {
        throw new LogicException(this + ".add(" + fieldTrigger + ") references unknown field:"
                                 + fieldName,
                                 null);
      }
      __fieldTriggers.put(fieldName.getKeyName(), fieldTrigger);
    }
    if (fieldTrigger.triggerOnInstantiation()) {
      if (!__instantiationFieldTriggers.contains(fieldTrigger)) {
        __instantiationFieldTriggers.add(fieldTrigger);
      }
    }
    getDb().addExpirable(fieldTrigger);
    getDb().addShutdownable(fieldTrigger);
    return fieldTrigger;
  }

  public <T> PostFieldTrigger<T> addPostFieldTrigger(PostFieldTrigger<T> postFieldTrigger)
      throws MirrorTableLockedException
  {
    checkLockedStatus("add", postFieldTrigger);
    FieldKey<T> fieldKey = postFieldTrigger.getFieldName();
    if (fieldKey == null) {
      __globalPostFieldTriggers.add(postFieldTrigger);
    } else {
      if (getField(fieldKey) == null) {
        throw new MirrorLogicException(this + ".add(" + postFieldTrigger
                                       + ") references unknown field:" + fieldKey,
                                       null);
      }
      __postFieldTriggers.put(fieldKey.getKeyName(), postFieldTrigger);
    }
    getDb().addExpirable(postFieldTrigger);
    getDb().addShutdownable(postFieldTrigger);
    return postFieldTrigger;
  }

  /**
   * Add a new PreInstantiationTrigger to this Table that will get invoked on all TransientRecords
   * before they get inserted into the database.
   * 
   * @param trigger
   *          The PreInstantiationTrigger to insert.
   * @throws MirrorTableLockedException
   *           If the Table has been locked via lock().
   * @return The trigger that was inserted into the system.
   * @see #lock()
   */
  public PreInstantiationTrigger addPreInstantiationTrigger(PreInstantiationTrigger trigger)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(trigger, "trigger");
    checkLockedStatus("add", trigger);
    getDb().addExpirable(trigger);
    getDb().addShutdownable(trigger);
    __preInstantiationTriggers.add(trigger);
    return trigger;
  }

  /**
   * Add a new PostInstantiationTrigger to this Table that will get invoked on all PersistentRecords
   * immediately after they are inserted into the database.
   * 
   * @param trigger
   *          The PostInstantiationTrigger to insert.
   * @throws MirrorTableLockedException
   *           If the Table has been locked via lock().
   * @return The trigger that was inserted into the system.
   */
  public PostInstantiationTrigger addPostInstantiationTrigger(PostInstantiationTrigger trigger)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(trigger, "trigger");
    checkLockedStatus("add", trigger);
    getDb().addExpirable(trigger);
    getDb().addShutdownable(trigger);
    __postInstantiationTriggers.add(trigger);
    return trigger;
  }

  public PostRetrievalTrigger addPostRetrievalTrigger(PostRetrievalTrigger trigger)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(trigger, "trigger");
    checkLockedStatus("add", trigger);
    getDb().addExpirable(trigger);
    getDb().addShutdownable(trigger);
    __postRetrievalTriggers.add(trigger);
    return trigger;
  }

  /**
   * Add a StateTrigger to this Table which will trigger on the getDefaultState of the registered
   * StateTrigger.
   * 
   * @see StateTrigger#getDefaultState()
   */
  public StateTrigger addStateTrigger(StateTrigger trigger)
      throws MirrorTableLockedException
  {
    return addStateTrigger(trigger, trigger.getDefaultState());
  }

  /**
   * Add a StateTrigger to this Table which will be triggered on an explicitly defined state. This
   * will fail if the state is not the same as the DefaultState for the StateTrigger and it doesn't
   * have an overridable state.
   * 
   * @see StateTrigger#getDefaultState()
   * @see StateTrigger#isOverridableState()
   */
  public StateTrigger addStateTrigger(StateTrigger trigger,
                                      int state)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(trigger, "trigger");
    if (state == StateTrigger.STATE_UNDEFINED) {
      throw new IllegalArgumentException("Illegal state (StateTrigger.STATE_UNDEFINED)");
    }
    checkLockedStatus("add", trigger);
    String targetTableName = trigger.getTargetTableName();
    if (targetTableName != null && !targetTableName.equals(this.getName())) {
      throw new IllegalArgumentException(trigger + " references wrong targetTableName: "
                                         + targetTableName);
    }
    if (trigger.getDefaultState() != state && !trigger.isOverridableState()) {
      throw new IllegalArgumentException(trigger + " specifies immutable state of "
                                         + trigger.getDefaultState()
                                         + " and cannot be utilised with a state of " + state);
    }
    getDb().addExpirable(trigger);
    getDb().addShutdownable(trigger);
    __stateTriggers.get(state).add(trigger);
    return trigger;
  }

  /**
   * Request any field triggers that are associated with a particular field name to trigger on a
   * particular record. The method automatically works out whether or not the record has a
   * persistent backend and only uses the appropriate set of triggers.
   * 
   * @param record
   *          The record which has been updated
   * @param fieldName
   *          The name of the field that has been changed.
   * @throws MirrorFieldException
   *           If any of the triggers reject the change.
   */
  protected void triggerFieldTriggers(TransientRecord record,
                                      String fieldName)
      throws MirrorFieldException
  {
    boolean isWritableRecord = record instanceof WritableRecord;
    triggerFieldTriggers(record, isWritableRecord, fieldName, __fieldTriggers.get(fieldName));
    triggerFieldTriggers(record, isWritableRecord, fieldName, __globalFieldTriggers);
  }

  protected <T> void triggerPostFieldTriggers(TransientRecord record,
                                              String fieldName,
                                              T originalValue,
                                              boolean hasChanged)
  {
    triggerPostFieldTriggers(record,
                             fieldName,
                             originalValue,
                             hasChanged,
                             __postFieldTriggers.get(fieldName));
    triggerPostFieldTriggers(record,
                             fieldName,
                             originalValue,
                             hasChanged,
                             __globalPostFieldTriggers);
  }

  private <T> void triggerPostFieldTriggers(TransientRecord record,
                                            String fieldName,
                                            T originalValue,
                                            boolean hasChanged,
                                            List<PostFieldTrigger<?>> postFieldTriggers)
  {
    if (postFieldTriggers == null) {
      return;
    }
    int c = postFieldTriggers.size();
    for (int i = 0; i < c; i++) {
      postFieldTriggers.get(i).trigger(record, fieldName, originalValue, hasChanged);
    }
  }

  /**
   * Trigger any registered StateTrigger objects with respect to a given record for a given state
   * change.
   * 
   * @param record
   *          The record to be passed to the StateTriggers
   * @param state
   *          The index of the StateChange.
   * @throws MirrorRecordNestedException
   *           If any of the StateTriggers threw an exception. Please note that all StateTriggers
   *           will be executed even if there are errors, and the errors will be assimilated inside
   *           this compound error.
   */
  protected void triggerStateTriggers(PersistentRecord record,
                                      Transaction transaction,
                                      int state)
      throws MirrorRecordNestedException
  {
    List<StateTrigger> targetList = __stateTriggers.get(state);
    if (targetList.size() > 0) {
      Iterator<StateTrigger> stateTriggers = targetList.iterator();
      MirrorRecordNestedException mrnEx = null;
      while (stateTriggers.hasNext()) {
        StateTrigger trigger = stateTriggers.next();
        if (trigger != null) {
          try {
            trigger.trigger(record, transaction, state);
          } catch (Exception exception) {
            exception.printStackTrace();
            if (mrnEx == null) {
              mrnEx = new MirrorRecordNestedException("Error triggering state triggers:"
                                                      + StateTrigger.STATES.get(state),
                                                      exception,
                                                      record);
            }
            mrnEx.addNestedException(exception);
          }
        }
      }
      if (mrnEx != null) {
        throw mrnEx;
      }
    }
  }

  private void triggerFieldTriggers(TransientRecord record,
                                    boolean isWritableRecord,
                                    String fieldName,
                                    List<FieldTrigger<?>> fieldTriggers)
      throws MirrorFieldException
  {
    if (fieldTriggers == null) {
      return;
    }
    int fieldTriggerCount = fieldTriggers.size();
    for (int i = 0; i < fieldTriggerCount; i++) {
      FieldTrigger<?> fieldTrigger = fieldTriggers.get(i);
      Object filteredValue = null;
      if (isWritableRecord) {
        WritableRecord writableRecord = (WritableRecord) record;
        filteredValue =
            fieldTrigger.triggerField(writableRecord, fieldName, writableRecord.getTransaction());
      } else {
        if (fieldTrigger.handlesTransientRecords()) {
          filteredValue = fieldTrigger.triggerField(record, fieldName);
        }
      }
      if (filteredValue != null) {
        record.setField(fieldName, filteredValue, fieldTrigger);
      }
    }
  }

  private void triggerPre(ListIterator<PreInstantiationTrigger> triggers,
                          TransientRecord record,
                          Gettable params)
      throws MirrorInstantiationException
  {
    PreInstantiationTrigger trigger = null;
    try {
      while (triggers.hasNext()) {
        trigger = triggers.next();
        trigger.triggerPre(record, params);
      }
    } catch (Exception triggerEx) {
      MirrorInstantiationException instantiationEx =
          new MirrorInstantiationException(triggerEx, trigger, record);
      rollbackPre(triggers, record, params, instantiationEx);
      throw instantiationEx;
    }
  }

  private void triggerPost(ListIterator<PostInstantiationTrigger> triggers,
                           PersistentRecord record,
                           Gettable params)
      throws MirrorInstantiationException
  {
    PostInstantiationTrigger trigger = null;
    try {
      while (triggers.hasNext()) {
        trigger = triggers.next();
        trigger.triggerPost(record, params);
      }
    } catch (Exception triggerEx) {
      MirrorInstantiationException instantiationEx =
          new MirrorInstantiationException(triggerEx, trigger, record);
      rollbackPost(triggers, record, params, instantiationEx);
      throw instantiationEx;
    }
  }

  private void rollbackPre(ListIterator<PreInstantiationTrigger> triggers,
                           TransientRecord record,
                           Gettable params,
                           MirrorInstantiationException instantiationEx)
  {
    PreInstantiationTrigger trigger = null;
    try {
      while (triggers.hasPrevious()) {
        trigger = triggers.previous();
        trigger.rollbackPre(record, params);
      }
    } catch (Exception releaseEx) {
      instantiationEx.addTriggerException(trigger, releaseEx);
    }
  }

  private void rollbackPost(ListIterator<PostInstantiationTrigger> triggers,
                            PersistentRecord record,
                            Gettable params,
                            MirrorInstantiationException instantiationEx)
  {
    PostInstantiationTrigger trigger = null;
    try {
      while (triggers.hasPrevious()) {
        trigger = triggers.previous();
        trigger.rollbackPost(record, params);
      }
    } catch (Exception releaseEx) {
      instantiationEx.addTriggerException(trigger, releaseEx);
    }
  }

  private void releasePre(ListIterator<PreInstantiationTrigger> triggers,
                          TransientRecord record,
                          Gettable params)
  {
    PreInstantiationTrigger trigger = null;
    while (triggers.hasPrevious()) {
      trigger = triggers.previous();
      trigger.releasePre(record, params);
    }
  }

  private void releasePost(ListIterator<PostInstantiationTrigger> triggers,
                           PersistentRecord record,
                           Gettable params)
  {
    PostInstantiationTrigger trigger = null;
    while (triggers.hasPrevious()) {
      trigger = triggers.previous();
      trigger.releasePost(record, params);
    }
  }

  /**
   * Convert the given TransientRecord into a PersistentRecord. The sequence of events is as
   * follows:-
   * <ul>
   * <li>Trigger the PreInstantiationTriggers on the TransientRecord. If these fail then they will
   * roll back and the MirrorInstantiationException is thrown nice and cleanly.
   * <li>Move into extended rollback mode:-
   * <ul>
   * <li>Create the backend database record. If this fails due to an SQLException then bomb out with
   * a MirrorInstantiationException with badTrigger set to null (cos it wasnt caused by a trigger
   * explicitly)
   * <li>Trigger the PostInstantiationTriggers on the PersistentRecord. If no
   * MirrorInstantiationTriggers have been thrown by now then we are home and dry...
   * <li>release the PostInstantiationTriggers
   * <li>release the PreInstantiationTriggers
   * <li>notify the TableListeners
   * <li>return
   * </ul>
   * <li>If however at any point in the extended rollback period there was a
   * MirrorInstantiationException thrown then we need to do the following:-
   * <li>The PostInstantiationTriggers will have rolled back before the exception was thrown so we
   * don't have to worry about them...
   * <li>If we've actually created the PersistentRecord, then it needs to be disposed of. So we
   * create a delete Transaction, drop the PersistentRecord (and by definition its dependants) into
   * it and force commit(). If this drops a MirrorNestedException (unlikely) then it is slotted into
   * the MirrorInstantiationException.
   * <li>Rollback the PreInstantiationTriggers, folding any errors into our existing
   * MirrorInstantiationTrigger
   * <li>Throw our (potentially hugely complex) MirrorInstantiationException to whichever class is
   * unlikely enough to want to catch it...
   * </ul>
   */
  protected PersistentRecord makePersistent(TransientRecord transientRecord,
                                            Gettable params,
                                            PreparedStatement preparedStatement,
                                            boolean shouldCacheRecord)
      throws MirrorInstantiationException
  {
    params = GettableUtils.padNull(params);
    ListIterator<PreInstantiationTrigger> preTriggers = null;
    if (__preInstantiationTriggers.size() != 0) {
      preTriggers = __preInstantiationTriggers.listIterator();
      triggerPre(preTriggers, transientRecord, params);
    }
    int count = __instantiationFieldTriggers.size();
    FieldTrigger<?> fieldTrigger = null;
    try {
      for (int i = 0; i < count; i++) {
        fieldTrigger = __instantiationFieldTriggers.get(i);
        fieldTrigger.triggerField(transientRecord, null);
      }
    } catch (MirrorFieldException mfEx) {
      throw new MirrorInstantiationException(mfEx, fieldTrigger, transientRecord);
    }
    Object[] objectValues = transientRecord.getObjectValues();
    int[] intValues = transientRecord.getIntValues();
    for (RecordStateDataFilter writableDataFilter : getUnmodifiableRecordStateDataFilters()) {
      writableDataFilter.transientToPersistent(transientRecord, objectValues, intValues);
    }
    PersistentRecord persistentRecord = null;
    try {
      long time = 0;
      if (DebugConstants.STATS_ENABLED) {
        time = System.currentTimeMillis();
      }
      try {
        Connection connection = null;
        try {
          if (preparedStatement == null) {
            connection = getDb().getThreadConnectionFacade().get().checkOutConnection();
            preparedStatement = getInsertPreparedStatement(connection);
          } else {
            preparedStatement.clearParameters();
          }
          transientRecord.populateInsert(preparedStatement);
          preparedStatement.execute();
        } finally {
          if (connection != null) {
            getDb().getThreadConnectionFacade().get().checkInWorkingConnection(connection);
          }
        }
      } catch (SQLException sqlEx) {
        throw new MirrorInstantiationException(sqlEx, "SQL insert", transientRecord);
      }
      if (DebugConstants.STATS_ENABLED) {
        updateInsertStats(1, System.currentTimeMillis() - time);
      }
      persistentRecord = new PersistentRecord(this,
                                              objectValues,
                                              intValues,
                                              transientRecord.getPendingId(),
                                              null,
                                              shouldCacheRecord);
      if (getDb().flushQueryCaches()) {
        __recordSourceCache.invalidateAll();
      }
      ListIterator<PostInstantiationTrigger> postTriggers = null;
      if (__postInstantiationTriggers.size() != 0) {
        postTriggers = __postInstantiationTriggers.listIterator();
        triggerPost(postTriggers, persistentRecord, params);
        releasePost(postTriggers, persistentRecord, params);
      }
      if (preTriggers != null) {
        releasePre(preTriggers, transientRecord, params);
      }
      notifyTableListeners(persistentRecord, Transaction.TRANSACTION_INSERT);
      return persistentRecord;
    } catch (MirrorInstantiationException instantiationEx) {
      if (persistentRecord != null && !persistentRecord.isDeleted()) {
        Transaction transaction = getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                            Transaction.TRANSACTION_DELETE,
                                                            "rollback on " + transientRecord);
        MirrorNestedException mnEx = null;
        try {
          transaction.delete(persistentRecord, Db.DEFAULT_TIMEOUT);
          mnEx = transaction.commit();
        } catch (MirrorTransactionTimeoutException mttEx) {
          mnEx = new MirrorNestedException("Error rolling back makePersistent operation", mttEx);
          mnEx.addNestedException(mttEx);
        }
        if (mnEx != null) {
          instantiationEx.set(mnEx);
        }
      }
      if (preTriggers != null) {
        rollbackPre(preTriggers, transientRecord, params, instantiationEx);
      }
      throw instantiationEx;
    }
  }

  public PreparedStatement prepareInsertStatement(Connection connection)
      throws SQLException
  {
    return connection.prepareStatement(_insertPreparedStatement);
  }

  protected PersistentRecord makePersistent(TransientRecord transientRecord,
                                            Gettable params,
                                            boolean shouldCacheRecord)
      throws MirrorInstantiationException
  {
    return makePersistent(transientRecord, params, null, shouldCacheRecord);
  }

  /**
   * Register a new TableListener on this Table. The listener will then receive tableChanged calls
   * whenever notifyAllListeners(...) is called on this Table until the listener chooses to
   * deregister itself. This takes a synchronized lock on the internal listener Set to prevent
   * ConcurrentModificationException errors when notifying listeners.
   * 
   * @param listener
   *          The TableListener that is to be added. If the listener already is already registered
   *          then nothing is performed.
   */
  public void addTableListener(TableListener listener)
  {
    __tableListeners.add(listener);
  }

  /**
   * Deregister a given TableListener. This will not normally be required unless the listener is
   * actually being destroyed because a listener can temporarily deregister itself inside the
   * notifyAllListeners(...) method when it has enough information from the Table.
   * 
   * @param listener
   *          The table listener to remove from the list.
   * @see #notifyTableListeners(PersistentRecord,int)
   */
  public void removeTableListener(TableListener listener)
  {
    __tableListeners.remove(listener);
  }

  /**
   * Request this Table to notify all currently registered TableListeners that the specified action
   * has been performed on a particular record. This will also deregister any listeners that so
   * request it by returning false from <code>tableChanged(...)</code>.
   * 
   * @param updatedRecord
   *          The record that has been changed.
   * @param updateType
   *          The style of update that has been performed.
   * @see Transaction#TRANSACTION_DELETE
   * @see Transaction#TRANSACTION_UPDATE
   * @see Transaction#TRANSACTION_INSERT
   * @see Transaction#TRANSACTION_NULL
   */
  protected void notifyTableListeners(PersistentRecord updatedRecord,
                                      int updateType)
  {
    updateTimestamp();
    int recordId = updatedRecord == null ? TransientRecord.ID_TRANSIENT : updatedRecord.getId();
    if (!__tableListeners.isEmpty()) {
      for (TableListener listener : __tableListeners) {
        if (!listener.tableChanged(this, recordId, updatedRecord, updateType)) {
          __tableListeners.remove(listener);
        }
      }
    }
  }

  /**
   * Internal method to enable a Query to force the RecordCache for this Table to contain a
   * PersistentRecord held in the given ResultSet. This is invoked directly by the
   * Query.rebuildInternalState(timeout) method as a way of precaching information and avoiding the
   * need to perform a seperate Db lookup for each record inside the Query. It should not be invoked
   * by any other code without the risk of invalidating the Cache integrity.
   * 
   * @param id
   *          The id of the PersistentRecord from this Table that this resultSet represents. This
   *          information can be got from the resultSet as well, but in the current usage of the
   *          system will already have been extracted during the Query construction stage, so it is
   *          quicker to pass it by reference than resolving the resultSet again. This way a
   *          PersistentRecord that is already inside the recordCache can be skipped without having
   *          to do any resultSet manipulations at all which is considerably faster.
   * @param resultSet
   *          The ResultSet that is currently pointing at the record that has the id of id from the
   *          current Table.
   * @return true if the PersistentRecord was constructed from the resultSet and inserted into the
   *         cache, false if the cache already included a record with the given id. Please note that
   *         if the cache already contains a record with the given id that there is no checks for
   *         equality made between the information inside the resultSet and the PersistentRecord
   *         currently held within the recordCache.
   */
  protected boolean preCacheRecord(int id,
                                   ResultSet resultSet)
      throws SQLException
  {
    if (__recordCache.hasCachedRecord(id) | __lockedRecordMap.containsKey(id)) {
      return false;
    }
    PersistentRecord record;
    try {
      record = PersistentRecord.getInstance(this, resultSet);
    } catch (RuntimeException e) {
      throw new MirrorLogicException(String.format("Error while resolving %s.%s from %s to a PersistentRecord",
                                                   this,
                                                   Integer.toString(id),
                                                   SqlUtil.rowToString(resultSet)),
                                     e);
    }
    __recordCache.cacheRecord(record);
    return true;
  }

  public boolean hasCachedRecord(int id)
  {
    return __recordCache.hasCachedRecord(id);
  }

  public PersistentRecord getCachedRecord(int id)
  {
    return __recordCache.getRecordIfPresent(id);
  }

  // public static void resolveRecords(RecordSource recordSource,
  // Viewpoint viewpoint)
  // {
  // IdList idList = recordSource.getIdList(viewpoint);
  // resolveRecords(idList, 0, idList.size());
  // }

  private static String getPrecacheSql(IdList idList,
                                       int startIndex,
                                       int size)
  {
    StringBuilder buf = null;
    final Table table = idList.getTable();
    size = Math.min(size, table.getPrecacheSize());
    startIndex = Math.max(0, Math.min(startIndex, idList.size()));
    final int maxIndex = Math.min(startIndex + size, idList.size());
    for (int i = startIndex; i < maxIndex; i++) {
      int id = idList.getInt(i);
      if (!table.hasCachedRecord(id)) {
        if (buf == null) {
          buf = new StringBuilder(40 + size * 4);
          buf.append("select * from ").append(table.getName()).append(" where id in (");
        } else {
          buf.append(',');
        }
        buf.append(id);
      }
    }
    if (buf != null) {
      buf.append(")");
      return buf.toString();
    }
    return null;
  }

  /**
   * Try and ensure that all the records contained in a RecordSource are in the cache in an
   * efficient manner.
   */
  public static void resolveRecords(IdList idList,
                                    int startIndex,
                                    int size)
  {
    if (idList.size() <= 1) {
      return;
    }
    final Table table = idList.getTable();
    String precacheSql = getPrecacheSql(idList, startIndex, size);
    int recordCount = 0;
    long t = 0;
    if (DebugConstants.DEBUG_SQL_PRECACHE) {
      t = System.nanoTime();
    }
    if (precacheSql != null) {
      ConnectionFacade connectionFacade = table.getDb().getThreadConnectionFacade().get();
      Connection connection = null;
      try {
        connection = connectionFacade.checkOutConnection();
        Statement statement = null;
        try {
          statement = connection.createStatement();
          statement.setFetchSize(size);
          ResultSet resultSet = statement.executeQuery(precacheSql);
          if (DebugConstants.DEBUG_SQL) {
            System.err.println("-  Precache: " + precacheSql);
          }
          while (resultSet.next()) {
            table.preCacheRecord(resultSet.getInt(1), resultSet);
            if (DebugConstants.DEBUG_SQL_PRECACHE) {
              recordCount++;
            }
          }
        } finally {
          if (statement != null) {
            statement.close();
          }
        }
      } catch (SQLException sqlEx) {
        sqlEx.printStackTrace();
        connectionFacade.checkInBrokenConnection(connection);
        connection = null;
      } finally {
        connectionFacade.checkInWorkingConnection(connection);
      }
    }
    if (DebugConstants.DEBUG_SQL_PRECACHE) {
      double time = (System.nanoTime() - t) / 1000000.0;
      System.err.println(String.format("%s.resolveRecords(...): count: %s/%s, time: %.3fms, average: %.3fms",
                                       table.getName(),
                                       Integer.toString(recordCount),
                                       Integer.toString(idList.size()),
                                       Double.valueOf(time),
                                       Double.valueOf(recordCount == 0
                                           ? 0.0
                                           : time / recordCount)));
    }
  }

  public void shutdown()
  {
    for (PreparedStatement ps : __insertStatements.values()) {
      if (DebugConstants.DEBUG_SQL_PREPAREDSTATEMENTS) {
        System.err.println(ps + ".close()");
      }
      try {
        ps.close();
      } catch (SQLException e) {
        throw new LogicException(e);
      }
    }
    for (PreparedStatement ps : __retrieveStatements.values()) {
      if (DebugConstants.DEBUG_SQL_PREPAREDSTATEMENTS) {
        System.err.println(ps + ".close()");
      }
      try {
        ps.close();
      } catch (SQLException e) {
        throw new LogicException(e);
      }
    }
    __recordCache.invalidateAll();
    __recordSourceCache.invalidateAll();
    __recordOperations.clear();
    __fields.clear();
    __tableListeners.clear();
    __instantiationFieldTriggers.clear();
    __preInstantiationTriggers.clear();
    __postInstantiationTriggers.clear();
    for (int i = 0; i < __stateTriggers.size(); i++) {
      __stateTriggers.get(i).clear();
    }
    _db = null;
  }

  public boolean hasFieldNamed(String fieldName)
  {
    return __name_field.containsKey(fieldName);
    // return __fieldFilterIndex.containsKey(fieldName);
  }

  public Map<String, RecordSourceOperation<?>> getRecordSourceOperations()
  {
    return Collections.unmodifiableMap(__recordSourceOperations);
  }

  /**
   * @return the appropriate RecordOperation or null if it is not found.
   */
  public RecordOperation<?> getRecordOperation(String key)
  {
    return __recordOperations.get(key);
  }

  @SuppressWarnings("unchecked")
  public <T> RecordOperation<T> getRecordOperation(RecordOperationKey<T> key)
  {
    return (RecordOperation<T>) getRecordOperation(key.toString());
  }

  @SuppressWarnings("unchecked")
  public <A, V> MonoRecordOperation<A, V> getRecordOperation(MonoRecordOperationKey<A, V> key)
  {
    return (MonoRecordOperation<A, V>) getRecordOperation(key.toString());
  }

  @SuppressWarnings("unchecked")
  public <A, B, V> DuoRecordOperation<A, B, V> getRecordOperation(DuoRecordOperationKey<A, B, V> key)
  {
    return (DuoRecordOperation<A, B, V>) getRecordOperation(key.toString());
  }

  @SuppressWarnings("unchecked")
  public <A, B, C, V> TrioRecordOperation<A, B, C, V> getRecordOperation(TrioRecordOperationKey<A, B, C, V> key)
  {
    return (TrioRecordOperation<A, B, C, V>) getRecordOperation(key.toString());
  }

  public boolean hasRecordOperation(Object key)
  {
    return __recordOperations.containsKey(key.toString());
  }

  public boolean hasRecordSourceOperation(Object key)
  {
    return __recordSourceOperations.containsKey(key.toString());
  }

  /**
   * @return unmodifiable Map of keys to RecordOperation
   */
  public Map<String, RecordOperation<?>> getRecordOperations()
  {
    return Collections.unmodifiableMap(__recordOperations);
  }

  /**
   * Get an optionally failfast list of the RecordOperations registered on this Table.
   * 
   * @param useCopy
   *          If true then the Iterator will be across a clone of the set of RecordOperations
   *          thereby allowing new RecordOperations to be registered while the Iterator is being
   *          traversed. Of course, there is a performance overhead, so if modifications are not
   *          required then false is more sensible.
   * @return Iterator of RecordOperations. The ordering is undefined.
   */
  public Iterator<RecordOperation<?>> getRecordOperations(boolean useCopy)
  {
    return getOperations(useCopy, __recordOperations);
  }

  /**
   * Resolve the named TableOperation on this Table without a Transactional viewpoint.
   * 
   * @param key
   *          The name of the TableOperation
   * @return The result of the TableOperation
   */
  @Override
  public Object get(Object key)
  {
    return get(key, null);
  }

  /**
   * Resolve the named RecordSourceOperation on this Table (in its role as the RecordSource of all
   * records in the table) from the given Transactional viewpoint. Please note that this is not the
   * same as invoking the RecordSourceOperation on the all records query returned by getAll()
   * because Table is not a Query. If you want to get SQL level optimisations then you should not
   * invoke RecordSourceOperations directly on the Table, but on the all records query instead.
   * 
   * @see #getAllRecordsQuery()
   * @see #getAll()
   */
  @Override
  public Object get(Object name,
                    Viewpoint viewpoint)
  {
    return Preconditions.checkNotNull(getRecordSourceOperation(name.toString()),
                                      "Cannot resolve %s.%s",
                                      this.getName(),
                                      name)
                        .invokeRecordSourceOperation(this, viewpoint);
  }

  @Override
  public <V> V opt(RecordSourceOperationKey<V> key,
                   Viewpoint viewpoint)
  {
    return getRecordSourceOperation(key).invokeRecordSourceOperation(this, viewpoint);
  }

  @Override
  public <V> V opt(RecordSourceOperationKey<V> key)
  {
    return opt(key, null);
  }

  @Override
  public <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                      A a)
  {
    return opt(key, null, a);
  }

  @Override
  public <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                      Viewpoint viewpoint,
                      A a)
  {
    return getRecordSourceOperation(key).calculate(this, viewpoint, a);
  }

  @Override
  public <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                         A a,
                         B b)
  {
    return opt(key, null, a, b);
  }

  @Override
  public <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                         Viewpoint viewpoint,
                         A a,
                         B b)
  {
    return getRecordSourceOperation(key).calculate(this, viewpoint, a, b);
  }

  /**
   * @throws ClassCastException
   *           if the requested TableOperation doesn't resolve to a Gettable.
   */
  public Gettable getGettable(String name,
                              Viewpoint viewpoint)
  {
    Object obj = get(name, viewpoint);
    if (obj instanceof Gettable) {
      return (Gettable) obj;
    }
    throw new ClassCastException(String.format("%s.%s gives %s which is not a Gettable",
                                               this,
                                               name,
                                               obj));
  }

  @Override
  public boolean containsKey(Object key)
  {
    return hasRecordSourceOperation(key);
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    return Collections.unmodifiableSet(__recordSourceOperations.keySet());
  }

  /**
   * Get the Field with a given name.
   * 
   * @param fieldName
   *          The name of the field that is required.
   * @return The requested Field or null if the name is not recognised.
   * @throws ClassCastException
   *           if the field that you have requested is a RecordOperation that is not a Field
   */
  public Field<?> getField(String fieldName)
  {
    RecordOperation<?> recordOperation = getRecordOperation(fieldName);
    try {
      return (Field<?>) recordOperation;
    } catch (ClassCastException ccEx) {
      ClassCastException wrapperEx =
          new ClassCastException(this + ".getFieldFilter(" + fieldName + ") --> "
                                 + recordOperation.getClass().getName());
      wrapperEx.initCause(ccEx);
      throw wrapperEx;
    }
  }

  @SuppressWarnings("unchecked")
  public <T> Field<T> getField(FieldKey<T> fieldKey)
  {
    return (Field<T>) getField(fieldKey.getKeyName());
  }

  public Field<?> getField(int index)
  {
    return __fields.get(index);
  }

  /**
   * @return The field that was registered on the Table. It will have its fieldId set by this point.
   */
  public <T> Field<T> addField(Field<T> field)
      throws MirrorTableLockedException
  {
    checkLockedStatus("add", field);
    if (hasFieldNamed(field.getName())) {
      throw new IllegalArgumentException(field + " already declared in " + this);
    }
    addRecordOperation(field);
    int fieldFilterIndex = __name_field.size();
    field.getKey().register(field);
    __name_field.put(field.getName(), field);
    __fields.add(field);
    field.register(this, fieldFilterIndex);
    if (DebugConstants.DEBUG_FIELDLABELS && field.getEnglishName().equals(field.getName())
        && !"id".equals(field.getName())) {
      System.err.println(this + "." + field.getName() + " needs label defining");
    }
    if (field instanceof RecordStateDataFilter) {
      addRecordStateDataFilter((RecordStateDataFilter) field);
    }
    return field;
  }

  public int getFieldIndex(String fieldName)
  {
    try {
      return __name_field.get(fieldName).getFieldIndex();
      // return __fieldFilterIndex.get(fieldName).intValue();
    } catch (NullPointerException npEx) {
      throw new IllegalArgumentException(this + ".getFieldFilterIndex(" + fieldName
                                         + ") doesn't exist");
    }
  }

  // public int getFieldIndex(FieldKey<?> fieldKey)
  // {
  // return getFieldIndex(fieldKey.getKeyName());
  // }

  /**
   * Get all the Fields that have been registered on this Table in the order that they have been
   * registered.
   * 
   * @return A read-only list of all the Fields that are registered on this Table.
   */
  public List<Field<?>> getFields()
  {
    return __roFields;
  }

  @SuppressWarnings("unchecked")
  private <T extends LockableOperation> T addOperation(T operation,
                                                       Map<String, T> container,
                                                       boolean utiliseClone)
      throws MirrorTableLockedException
  {
    checkLockedStatus("add", operation);
    if (container.containsKey(operation.getName())) {
      throw new DuplicateNamedException(operation.getName(),
                                        String.format("%s.add(%s - %s) is already defined as %s - %s",
                                                      this,
                                                      operation,
                                                      operation.getClass(),
                                                      container.get(operation.getName()),
                                                      container.get(operation.getName())
                                                               .getClass()));
    }
    if (utiliseClone) {
      try {
        operation = (T) operation.clone(); // unchecked
      } catch (CloneNotSupportedException cnsEx) {
        throw new LogicException(operation + " does not clone() cleanly", cnsEx);
      }
    }
    container.put(operation.getName(), operation);
    operation.initialise(this);
    return operation;
  }
  private <T extends Object> Iterator<T> getOperations(boolean useCopy,
                                                       Map<?, T> container)
  {
    return useCopy
        ? new ArrayList<T>(container.values()).iterator()
        : container.values().iterator();
  }

  // /**
  // * Add a TableOperation to this table. Once this has been performed, then the TableOperation can
  // * be invoked by its name directly on the Table itself.
  // *
  // * @deprecated in preference of invoking addRecordSourceOperation as the replacement
  // * @see #addRecordSourceOperation(RecordSourceOperation, boolean)
  // */
  // @Deprecated
  // public RecordSourceOperation<?> addTableOperation(RecordSourceOperation<?> operation)
  // throws MirrorTableLockedException
  // {
  // return addRecordSourceOperation(operation, false);
  // }

  // @Deprecated
  // protected RecordSourceOperation<?> addTableOperation(RecordSourceOperation<?> operation,
  // boolean utiliseClone)
  // throws MirrorTableLockedException
  // {
  // return addRecordSourceOperation(operation, utiliseClone);
  // }

  public RecordOperation<?> addRecordOperation(RecordOperation<?> operation)
      throws MirrorTableLockedException
  {
    return addRecordOperation(operation, false);
  }

  protected RecordOperation<?> addRecordOperation(RecordOperation<?> operation,
                                                  boolean utiliseClone)
      throws MirrorTableLockedException
  {
    return addOperation(operation, __recordOperations, utiliseClone);
  }

  public RecordSourceOperation<?> addRecordSourceOperation(RecordSourceOperation<?> operation)
      throws MirrorTableLockedException
  {
    return addRecordSourceOperation(operation, false);
  }

  protected RecordSourceOperation<?> addRecordSourceOperation(RecordSourceOperation<?> operation,
                                                              boolean utiliseClone)
      throws MirrorTableLockedException
  {
    return addOperation(operation, __recordSourceOperations, utiliseClone);
  }

  /**
   * @return the appropriate RecordSourceOperation or null if it is not found.
   */
  public RecordSourceOperation<?> getRecordSourceOperation(String key)
  {
    return __recordSourceOperations.get(key);
  }

  @SuppressWarnings("unchecked")
  public <V> RecordSourceOperation<V> getRecordSourceOperation(RecordSourceOperationKey<V> key)
  {
    return (RecordSourceOperation<V>) getRecordSourceOperation(key.getKeyName());
  }

  @SuppressWarnings("unchecked")
  public <A, V> MonoRecordSourceOperation<A, V> getRecordSourceOperation(MonoRecordSourceOperationKey<A, V> key)
  {
    return (MonoRecordSourceOperation<A, V>) getRecordSourceOperation(key.getKeyName());
  }

  @SuppressWarnings("unchecked")
  public <A, B, V> DuoRecordSourceOperation<A, B, V> getRecordSourceOperation(DuoRecordSourceOperationKey<A, B, V> key)
  {
    return (DuoRecordSourceOperation<A, B, V>) getRecordSourceOperation(key.getKeyName());
  }

  /**
   * Get the controlling Db for this Table.
   */
  public Db getDb()
  {
    return _db;
  }

  public String debugRecordStatus(int recordId)
  {
    StringBuilder result = new StringBuilder();
    result.append(this);
    result.append(".debugRecordStatus(");
    result.append(recordId);
    result.append(") --> __lockedRecordMap:");
    result.append(__lockedRecordMap.get(recordId));
    result.append(", __recordMap:");
    // result.append(__recordMap.get(recordId));
    result.append(__recordCache.getRecordIfPresent(recordId));
    return result.toString();
  }

  /**
   * @return The requested record. Never null.
   * @throws MirrorNoSuchRecordException
   *           If the id cannot be resolved in the database
   * @throws IllegalStateException
   *           If there is an SQL problem resolving the record against the database.
   */
  public PersistentRecord getRecord(int recordId)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord record = getOptRecord(recordId);
    if (record == null) {
      throw new MirrorNoSuchRecordException(String.format("Failure to resolve %s on %s",
                                                          Integer.toString(recordId),
                                                          this),
                                            this);
    }
    return record;
  }

  /**
   * Get a record from the table with missing records triggering a RuntimeException. This is
   * intended for situations where you are convinced that the record does exist and you are not
   * intending to catch the error that occurs if it does not exist.
   * 
   * @return The requested record. Never null.
   * @throws MissingRecordException
   *           If there is an SQL problem resolving the record against the database.
   */
  public PersistentRecord getCompRecord(int id)
      throws MissingRecordException
  {
    PersistentRecord record = getOptRecord(id);
    if (record == null) {
      throw new MissingRecordException(String.format("Compulsory record #%s was not found",
                                                     Integer.toString(id)),
                                       this,
                                       Integer.valueOf(id),
                                       null);
    }
    return record;
  }

  public PersistentRecord getCompRecord(int id,
                                        Viewpoint viewpoint)
      throws MissingRecordException
  {
    return getCompRecord(id).filterByTransaction(viewpoint);
  }

  /**
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getCompRecord(Object id)
      throws MissingRecordException, IllegalIdException
  {
    return getCompRecord(toIntId(id));
  }

  /**
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getCompRecord(Object id,
                                        Viewpoint viewpoint)
      throws MissingRecordException, IllegalIdException
  {
    return getCompRecord(toIntId(id), viewpoint);
  }

  /**
   * Try and convert an obj into an Integer id
   * 
   * @param obj
   *          The optional Object to try and convert
   * @return The appropriate Integer or null if obj was null or an empty String
   * @throws IllegalIdException
   *           If obj wasn't null but it wasn't able to convert it into an Integer using
   *           NumberUtiul.toInteger. The message will include details as to what was trying to be
   *           converted.
   * @see NumberUtil#toInteger(Object, Integer)
   */
  public int toIntId(Object obj)
      throws IllegalIdException
  {
    if (obj == null | "".equals(obj)) {
      return 0;
    }
    int id = NumberUtil.toInt(obj, -1);
    if (id == -1) {
      throw new IllegalIdException(null, this, obj, null);
    }
    return id;
  }

  /**
   * @return The requested record or null if it doesn't exist.
   */
  public PersistentRecord getOptRecord(int id,
                                       Viewpoint viewpoint)
  {
    PersistentRecord record = getOptRecord(id);
    return viewpoint == null
        ? record
        : record == null ? null : record.filterByTransaction(viewpoint);
  }

  /**
   * @throws MissingRecordException
   *           if it wasn't possible to convert a defined id into an Integer.
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getOptRecord(Object id,
                                       Viewpoint viewpoint)
      throws MissingRecordException, IllegalIdException
  {
    return getOptRecord(toIntId(id), viewpoint);
  }

  public PersistentRecord asPersistentRecord(Object obj,
                                             Viewpoint viewpoint)
  {
    if (obj != null && obj instanceof PersistentRecord) {
      PersistentRecord record = (PersistentRecord) obj;
      if (this.equals(record.getTable())) {
        return record.filterByTransaction(viewpoint);
      }
    }
    return null;
  }

  /**
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getOptRecord(Object id)
      throws IllegalIdException
  {
    return getOptRecord(id, null);
  }

  /**
   * Check to see whether or not a given record actually exists in the backend SQL. This will
   * resolve the record if it is there so if the method returns true then getting the record
   * subsequently should be very fast, but it doesn't throw any exceptions at all internally if the
   * record doesn't exist so should be relatively efficient.
   * 
   * @return true if the record exists in the backend database at present.
   * @throws IllegalStateException
   *           If there is an SQL problem resolving the record against the database.
   */
  public boolean hasRecord(int recordId)
  {
    return getOptRecord(recordId) != null;
  }

  /**
   * Get a record from the table without throwing any exceptions for missing records.
   * 
   * @return The requested record or null if it is not found.
   * @throws MirrorLogicException
   *           If there is a really serious low level problem with the database or the connection
   *           with the database. This is a really unexpected exception and should be treated as
   *           problematic. It will have the SQLException that originally caused it as the nested
   *           exception inside it.
   */
  public PersistentRecord getOptRecord(int recordId)
  {
    PersistentRecord record;
    try {
      record = __recordCache.getRecord(recordId);
    } catch (Exception e) {
      if (recordId <= 0) {
        System.err.println(this + ".getOptRecord(" + recordId + ")");
        e.printStackTrace();
        return null;
      }
      throw new MirrorLogicException("SQLException while resolving " + this + "." + recordId, e);
    }
    if (record == null || record.getId() == TransientRecord.ID_MISSING) {
      return null;
    }
    return record;
  }

  /**
   * Inform this table that a persistent record is now involved in a transaction and therefore
   * shouldn't be discarded from the cache. The record is placed inside a dedicated locked record
   * pool which is always checked before the cache. The object is left in the cache where it may or
   * may not be discarded (depending on the cache usage while the record is locked). Either way, it
   * doesn't matter because the record is safe.
   * 
   * @param record
   *          The PersistentRecord that is due to be inserted into the table.
   */
  protected void lockRecord(PersistentRecord record)
  {
    __lockedRecordMap.put(record.getId(), record);
  }

  /**
   * Resolve an Object into an id using the NumberUtil utilities and grab the record from the
   * database with no Transactional filtering.
   * 
   * @throws MirrorNoSuchRecordException
   *           If the recordId cannot be resolved into an Integer or if the specified id doesn't
   *           exist in the Table.
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getRecord(Object id)
      throws MirrorNoSuchRecordException, IllegalIdException
  {
    return getRecord(id, (Transaction) null);
  }

  /**
   * @deprecated in preference of {@link #getRecord(int)}
   */
  @Deprecated
  public PersistentRecord getRecord(Integer id)
      throws MirrorNoSuchRecordException
  {
    return getRecord(toIntId(id));
  }

  /**
   * Resolve an Object into an id using the NumberUtil utilities and grab the record from the
   * database with the specified Transactional viewpoint.
   * 
   * @throws MirrorNoSuchRecordException
   *           If the recordId cannot be resolved into an Integer or if the specified id doesn't
   *           exist in the Table.
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getRecord(Object id,
                                    Viewpoint viewpoint)
      throws MirrorNoSuchRecordException, IllegalIdException
  {
    return getRecord(toIntId(id), viewpoint);
  }

  /**
   * Get a record from this Table from the perspective of whatever Transaction, if any, holds the
   * lock on the viewpointContainer. This is useful for situations when you know that a record is
   * locked and yet you don't have permissions to get the Transaction but you want to look at other
   * records with the same perspective as the Transaction.
   * 
   * @throws IllegalIdException
   *           if id is defined but it cannot be converted to an Integer id
   * @see #toIntId(Object)
   */
  public PersistentRecord getRecord(Object id,
                                    PersistentRecord viewpointContainer)
      throws MirrorNoSuchRecordException, IllegalIdException
  {
    return getRecord(id, viewpointContainer.getTransaction());
  }

  public PersistentRecord getRecord(int id,
                                    Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return getRecord(id).filterByTransaction(viewpoint);
  }

  protected void cachePersistentRecord(PersistentRecord record)
  {
    __recordCache.cacheRecord(record);
  }

  /**
   * Inform this table that a persistent record is now freed from its Transaction and can be left
   * back at the tender mercy of the cache.
   * 
   * @param record
   *          The record that has now been freed.
   */
  protected void unlockRecord(PersistentRecord record)
  {
    __lockedRecordMap.remove(record.getId());
  }

  /**
   * Look up the given named RecordSource in the RecordSource cache and if it is a Query then return
   * it.
   * 
   * @param key
   *          The name of the Query to look up.
   * @throws IllegalArgumentException
   *           If you ask for a name that resolves into a RecordSource that isn't a Query
   * @return null if the name is not found in the cache otherwise the Query
   * @throws IllegalArgumentException
   *           If the name resolves to a RecordSource that isn't directly resolveable into a Query
   *           (ie not a Query or a QueryWrapper)
   */
  private Query getCachedQuery(Object key)
  {
    RecordSource recordSource = __recordSourceCache.getIfPresent(key);
    if (recordSource == null) {
      return null;
    }
    Query query = RecordSources.asQuery(recordSource);
    if (query != null) {
      return query;
    }
    throw new IllegalArgumentException(String.format("Unable to resolve %s into a Query - already exists as %s",
                                                     key,
                                                     recordSource));
  }

  public RecordSource getCachedRecordSource(Object key)
  {
    return __recordSourceCache.getIfPresent(key);
  }

  public void cacheRecordSource(RecordSource recordSource)
  {
    __recordSourceCache.put(recordSource.getCacheKey(), recordSource);
  }

  /**
   * Get a Query with full control over all the configuration of the Query
   * 
   * @param cacheKey
   *          The value that is used to look up the Query in the query cache for this Table. If this
   *          is null, then the Query will always be constructed and then the resulting SQL query
   *          will be utilised to determine whether the matching records have previously been
   *          calculated. If name is defined then the cache will be queried directly which is
   *          considerably faster in situations where Queries are required more than once. Note that
   *          it is important that the method by which you derive the name of the Query
   *          <em>must</em> not produce identical values to other Queries as otherwise the results
   *          will become unpredictable.
   * @param order
   *          The SQL Ordering to utilise. Passing in null will get {@link #getDefaultOrdering()}
   *          and utilising {@link Query#NOSQLORDERING} will explicitly not utilise any ordering.
   * @param limit
   *          The maximum number of records to be included in the Query, or 0 if there is no limit.
   * @param joinTables
   *          The (optional) Tables that will be included as join Tables in the resulting Query.
   *          Tables will be included up to either the end of the list or the first null Table in
   *          the list. Query has a couple of utility toArray methods for converting items into
   *          suitable arguments if you are generating this list algorithmically.
   * @see Query#toArray(Collection)
   * @see Query#toArray(Query, Table[])
   */
  private Query buildQuery(Object cacheKey,
                           String filter,
                           String order,
                           String groupBy,
                           int limit,
                           String explicitFrom,
                           Table... joinTables)
  {
    if (order == null) {
      order = _defaultOrdering;
    }
    if (cacheKey == null) {
      Query query = new Query(null, this, filter, order, groupBy, limit, explicitFrom, joinTables);
      cacheKey = query.getCacheKey();
      Query cachedQuery = getCachedQuery(cacheKey);
      if (cachedQuery == null) {
        __recordSourceCache.put(cacheKey, query);
        return query;
      } else {
        return cachedQuery;
      }
    }
    Query cachedQuery = getCachedQuery(cacheKey);
    if (cachedQuery == null) {
      Query query =
          new Query(cacheKey, this, filter, order, groupBy, limit, explicitFrom, joinTables);
      __recordSourceCache.put(cacheKey, query);
      return query;
    } else {
      return cachedQuery;
    }
  }

  public Query getQuery(Object cacheKey,
                        String filter,
                        String order,
                        String groupBy,
                        int limit,
                        String explicitFrom,
                        Table... joinTables)
  {
    return buildQuery(cacheKey, filter, order, groupBy, limit, explicitFrom, joinTables);
  }

  /**
   * Get a default Query with default custom parameters. This means no grouping of matching records,
   * no distinct clauses on any fields, no limit on the numnber of records returned and no explicit
   * SQL from method.
   * 
   * @param cacheKey
   *          The value that is used to look up the Query in the query cache for this Table. If this
   *          is null, then the Query will always be constructed and then the resulting SQL query
   *          will be utilised to determine whether the matching records have previously been
   *          calculated. If name is defined then the cache will be queried directly which is
   *          considerably faster in situations where Queries are required more than once. Note that
   *          it is important that the method by which you derive the name of the Query
   *          <em>must</em> not produce identical values to other Queries as otherwise the results
   *          will become unpredictable.
   * @param order
   *          The SQL Ordering to utilise. Passing in null will get {@link #getDefaultOrdering()}
   *          and utilising {@link Query#NOSQLORDERING} will explicitly not utilise any ordering.
   * @param joinTables
   *          The (optional) Tables that will be included as join Tables in the resulting Query.
   *          Tables will be included up to either the end of the list or the first null Table in
   *          the list. Query has a couple of utility toArray methods for converting items into
   *          suitable arguments if you are generating this list algorithmically.
   */
  public Query getQuery(Object cacheKey,
                        String filter,
                        String order,
                        Table... joinTables)
  {
    return buildQuery(cacheKey, filter, order, null, -1, null, joinTables);
  }

  /**
   * @param order
   *          The SQL Ordering to utilise. Passing in null will get {@link #getDefaultOrdering()}
   *          and utilising {@link Query#NOSQLORDERING} will explicitly not utilise any ordering.
   */
  public Query getQuery(Object cacheKey,
                        String filter,
                        String order)
  {
    return buildQuery(cacheKey, filter, order, null, -1, null);
  }

  /**
   * Inform this Table that all information that is locally cached with respect to the database
   * should be dumped in preparation for re-reading from the database. This assumes that all
   * Transactions have been cancelled and therefore no WritableRecords exist. If there are still
   * WritableRecords in the cache then it will attempt to cancel their Transactions.
   */
  protected void flushCaches()
  {
    // while (_recordCache.frozenSize() > 1) {
    // Iterator lockedRecords = _recordCache.frozenValues();
    // try {
    // PersistentRecord lockedRecord = null;
    // try {
    // lockedRecord = (PersistentRecord) lockedRecords.next();
    // Transaction transaction = lockedRecord.getTransaction();
    // if (transaction != null) {
    // transaction.cancel();
    // }
    // } catch (ClassCastException ccEx) {
    // if (lockedRecord == null) {
    // System.err.println("Null record found in " + this + "._recordCache");
    // System.err.println("_recordCache.frozenSize() = " +
    // _recordCache.frozenSize());
    // throw new IllegalStateException("Null record found in RecordCache");
    // }
    // System.err.println("Found " + lockedRecord + " in " + this +
    // "._recordCache");
    // }
    // } catch (NoSuchElementException concurrentIteratorWierdness) {
    // System.err.println(this + ".flushCaches()");
    // concurrentIteratorWierdness.printStackTrace();
    // }
    // }
    // notifyAllListeners();
    // _recordCache.invalidateCache();
  }

  // /**
  // * @deprecated by getEmptyRecordSource()
  // * @see #getEmptyRecordSource()
  // */
  // @Deprecated
  // public Query getNoRecordsQuery()
  // {
  // return getQuery(FILTER_NORECORDS, FILTER_NORECORDS, null);
  // }

  private final IdList __emptyIdList = new EmptyIdList(this);

  private final RecordSource __emptyRecordSource = new AbstractRecordSource(this, null) {
    @Override
    protected IdList buildIdList(Viewpoint viewpoint)
    {
      return __emptyIdList;
    }

    @Override
    public long getLastChangeTime()
    {
      return 0;
    }

    @Override
    public boolean shouldRebuild(Viewpoint viewpoint)
    {
      return false;
    }
  };

  /**
   * Get a RecordSource that contains precisely no records from this Table.
   * 
   * @return An empty RecordSource
   */
  public RecordSource getEmptyRecordSource()
  {
    return __emptyRecordSource;
  }

  @Override
  public Table getTable()
  {
    return this;
  }

  @Override
  public IdList getIdList(Viewpoint viewpoint)
  {
    return getAllRecordsQuery().getIdList(viewpoint);
  }

  @Override
  public RecordList getRecordList(Viewpoint viewpoint)
  {
    return getAllRecordsQuery().getRecordList(viewpoint);
  }

  /**
   * @return The default ordering for the Table. This will remain the same until setDefaultOrdering
   *         is invoked.
   * @see #getDefaultOrdering()
   * @see RecordSourceOrdering#createRecordOrdering(String, String, String)
   * @see #setDefaultOrdering(String)
   */
  @Override
  public RecordOrdering getRecordOrdering()
  {
    if (_recordOrdering == null) {
      _recordOrdering = RecordSourceOrdering.createRecordOrdering(getName(),
                                                                  getDefaultOrdering(),
                                                                  getDefaultOrdering());
    }
    return _recordOrdering;
  }

  private volatile RecordOrdering _recordOrdering;

  @Override
  public IdList getIdList()
  {
    return getIdList(null);
  }

  @Override
  public RecordList getRecordList()
  {
    return getRecordList(null);
  }

  /**
   * Return {@link #getUpdateTimestamp()}
   */
  @Override
  public long getLastChangeTime()
  {
    return getUpdateTimestamp();
  }

  /**
   * @return false because the table will always have the same record ids in it regardless of the
   *         state of update transactions
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return false;
  }

  @Override
  public Object getCacheKey()
  {
    return getName();
  }

  @Deprecated
  public Iterator<PersistentRecord> subsection(int fromIndex,
                                               int maxListSize)
  {
    RecordList allRecords = getRecordList();
    fromIndex = fromIndex < 0 ? 0 : fromIndex;
    if (maxListSize == 0) {
      return allRecords.listIterator(fromIndex);
    }
    int toIndex = Math.min(fromIndex + maxListSize, allRecords.size());
    try {
      return allRecords.subList(fromIndex, toIndex).listIterator();
    } catch (IndexOutOfBoundsException badIndexEx) {
      return EmptyListIterator.<PersistentRecord> getInstance();
    }
  }

  public Object invokeRecordSourceOperation(String key,
                                            Viewpoint viewpoint)
  {
    return getAllRecordsQuery().get(key, viewpoint);
  }

  /**
   * Use direct object equality to check if this Table is equal to another object. It should be
   * impossible to create 2 instances of the same Table from the same database and therefore
   * equality is a valid check. It is also considerably faster.
   */
  @Override
  public boolean equals(Object o)
  {
    return this == o;
  }

  @Override
  public <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                          A a,
                          B b)
      throws UndefinedCompException
  {
    return comp(key, null, a, b);
  }

  @Override
  public <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                          Viewpoint viewpoint,
                          A a,
                          B b)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint, a, b);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this,
                                       key,
                                       getTable().getRecordSourceOperation(key),
                                       null,
                                       a,
                                       b);
    }
    return v;
  }

  @Override
  public <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                       A a)
      throws UndefinedCompException
  {
    return comp(key, null, a);
  }

  @Override
  public <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                       Viewpoint viewpoint,
                       A a)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint, a);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this,
                                       key,
                                       getTable().getRecordSourceOperation(key),
                                       null,
                                       a);
    }
    return v;
  }

  @Override
  public <V> V comp(RecordSourceOperationKey<V> key,
                    Viewpoint viewpoint)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this, key, getTable().getRecordSourceOperation(key), null);
    }
    return v;
  }

  @Override
  public <V> V comp(RecordSourceOperationKey<V> key)
      throws UndefinedCompException
  {
    return comp(key, null);
  }

  /**
   * @return false because we don't want the getAll method to be re-ordered in case of transactions
   *         (or at least I don't think so at present)
   */
  @Override
  public boolean shouldReorder(Viewpoint viewpoint)
  {
    return false;
  }

  @Override
  public int expire()
  {
    int count = __recordCache.expire();
    if (count > 0) {
      System.err.println(this + ".expire() recordCache --> " + count);
    }
    __recordSourceCache.cleanUp();
    return 0;
  }

}
