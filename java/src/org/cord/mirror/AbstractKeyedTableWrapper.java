package org.cord.mirror;

import java.util.concurrent.ExecutionException;

import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.mirror.trigger.RestrictedInstantiation;
import org.cord.util.Casters;
import org.cord.util.Gettable;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * TableWrapper for Tables that have the concept of a unique String key field that is used to lookup
 * records in the table. The class provides a SoftReference cache of key to id values so looking up
 * records by key should be very fast and quite memory efficient. The only caveat is that the key
 * field must be declared before other fields handled by subclasses in the SQL table ordering.
 * 
 * @author alex
 */
public abstract class AbstractKeyedTableWrapper
  extends RestrictedInstantiationTableWrapper
{
  private final LoadingCache<String, Integer> __cache;

  private final StringField __key;

  private final MonoRecordSourceOperationKey<Object, PersistentRecord> __lookupRecordSourceOperationName;

  private final Object __createLock = new Object();

  /**
   * @param tableName
   *          The name of the Table that will be created
   * @param key
   *          The definition of the field that will be utilised as the key. This will be
   *          automatically added to the table definition before initKeyedTable is invoked.
   * @param lookupRecordSourceOperationName
   *          The name of the ParametricTableOperation that will be registered on the Table to
   *          enable lookups by key from webmacro space.
   */
  public AbstractKeyedTableWrapper(String tableName,
                                   StringField key,
                                   CacheBuilderSpec cacheBuilderSpec,
                                   MonoRecordSourceOperationKey<Object, PersistentRecord> lookupRecordSourceOperationName,
                                   String instantiationErrorMessage)
  {
    super(tableName,
          instantiationErrorMessage);
    __key = Preconditions.checkNotNull(key, "key");
    __cache = CacheBuilder.from(cacheBuilderSpec).build(new CacheLoader<String, Integer>() {
      @Override
      public Integer load(String key)
          throws Exception
      {
        return Integer.valueOf(TableStringMatcher.onlyMatch(getTable(), __key.getKey(), key, null)
                                                 .getId());
      }
    });
    __lookupRecordSourceOperationName = lookupRecordSourceOperationName;
  }

  @Override
  protected final void initTableSub(Db db,
                                    String pwd,
                                    Table table)
      throws MirrorTableLockedException
  {
    table.addField(__key);
    table.addFieldTrigger(ImmutableFieldTrigger.create(__key.getKey()));
    table.setTitleOperationName(__key.getKey());
    table.addRecordSourceOperation(new MonoRecordSourceOperation<Object, PersistentRecord>(__lookupRecordSourceOperationName,
                                                                                           Casters.NULL) {
      @Override
      public PersistentRecord calculate(RecordSource recordSource,
                                        Viewpoint viewpoint,
                                        Object a)
      {
        try {
          return resolveRecord(a, viewpoint);
        } catch (MirrorNoSuchRecordException e) {
          throw new IllegalArgumentException("cannot resolve " + a, e);
        }
      }
    });
    initKeyedTable(db, pwd, table);
  }

  protected abstract void initKeyedTable(Db db,
                                         String pwd,
                                         Table table)
      throws MirrorTableLockedException;

  /**
   * Attempt to convert obj into an instance in the Table. The method of resolution will depend on
   * the type of obj, namely:
   * <dl>
   * <dt>PersistentRecord</dt>
   * <dd>If the record is from the wrapped Table then return the transactionally filtered viewpoint
   * onto it. If it is from a different Table then throw an IllegalArgumentException</dd>
   * <dt>Integer</dt>
   * <dd>Look up the record with the given id in the wrapped Table</dd>
   * <dt>Object</dt>
   * <dd>Convert the object to a String and then attempt to match it against a stored key with
   * relatively heavy caching of data.</dd>
   * </dl>
   * 
   * @param obj
   *          The object to try and resolve into an instance, either as an explicit id to the record
   *          or as a key to look the record up.
   * @param viewpoint
   *          The viewpoint through which the returned record will be viewed.
   * @return The requested record. Never null
   * @throws MirrorNoSuchRecordException
   *           If the key or id cannot be resolved to a record in the Table.
   * @see PersistentRecord#filterByTransaction(Viewpoint)
   */
  public final PersistentRecord resolveRecord(Object obj,
                                              Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    if (obj instanceof PersistentRecord) {
      PersistentRecord instance = (PersistentRecord) obj;
      TransientRecord.assertIsTable(instance, this);
      return instance.filterByTransaction(viewpoint);
    }
    if (obj instanceof Integer) {
      return getTable().getRecord((Integer) obj, viewpoint);
    }
    String key = StringUtils.toString(obj);
    if (viewpoint == null) {
      try {
        return getTable().getRecord(__cache.get(key).intValue());
      } catch (ExecutionException e) {
        throw new MirrorNoSuchRecordException(String.format("Unable to resolve %s", obj),
                                              getTable(),
                                              e);
      }
    } else {
      return TableStringMatcher.onlyMatch(getTable(), __key.getKey(), key, viewpoint);
    }
  }

  /**
   * Resolve a record by key from a given viewpoint, and if that is not possible then create a new
   * record with the given initParams.
   * 
   * @param key
   *          The key that the record should be found under
   * @param viewpoint
   *          The transactional viewpoint that existing records should be returned under. Note that
   *          this will not affect new records. Perhaps it should?
   * @param initParams
   *          The params that are to be passed to the makePersistent method in the case that it is
   *          necessary to boot a new record. If the record already exists under key then this is
   *          not used.
   * @throws MirrorFieldException
   *           If there are problems booting the new record
   * @throws MirrorInstantiationException
   *           If there are problems booting the new record
   * @see #resolveRecord(Object, Viewpoint)
   * @see RestrictedInstantiation
   */
  protected PersistentRecord getOrCreateInstance(String key,
                                                 Viewpoint viewpoint,
                                                 Gettable initParams)
      throws MirrorFieldException, MirrorInstantiationException
  {
    try {
      return resolveRecord(key, viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      synchronized (__createLock) {
        TransientRecord tr = getTable().createTransientRecord();
        tr.setField(__key.getName(), key);
        return makePersistent(tr, initParams);
      }
    }
  }

  /**
   * Make the given record Persistent after validating that it doesn't overlap with an existing keys
   * in the database.
   */
  @Override
  protected PersistentRecord makePersistent(TransientRecord record,
                                            Gettable initParams)
      throws MirrorInstantiationException
  {
    TransientRecord.assertIsTable(record, getTable());
    String key = record.getString(__key.getName());
    if (Strings.isNullOrEmpty(key)) {
      throw new MirrorInstantiationException(null,
                                             String.format("No value defined for %s.%s",
                                                           getTableName(),
                                                           __key.getName()),
                                             record);
    }
    synchronized (__createLock) {
      try {
        PersistentRecord existing = resolveRecord(key, null);
        throw new MirrorInstantiationException(null,
                                               String.format("\"%s\" is already defined as a key on %s.%s in %s",
                                                             key,
                                                             getTableName(),
                                                             __key.getName(),
                                                             existing),
                                               record);
      } catch (MirrorNoSuchRecordException e) {
        // continue...
      }
      PersistentRecord result = super.makePersistent(record, initParams);
      __cache.put(key, Integer.valueOf(result.getId()));
      return result;
    }
  }

  /**
   * Resove a record by key, and if that is not possible then create a new record.
   */
  protected PersistentRecord getOrCreateInstance(String key)
      throws MirrorFieldException, MirrorInstantiationException
  {
    return getOrCreateInstance(key, null, null);
  }
}
