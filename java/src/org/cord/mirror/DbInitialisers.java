package org.cord.mirror;

import java.util.Iterator;

import com.google.common.collect.ImmutableSet;

/**
 * Interface that describe Objects that contain one or more DbInitialisers and which are able to
 * supply them to the Db on request. These DbInitialisers are assumed to be constant for the
 * lifecycle of the Object and will only be queried once when the Object is added to the Db.
 * 
 * @see DbInitialiser
 */
public interface DbInitialisers
{
  public Iterator<DbInitialiser> getDbInitialisers();

  public final static DbInitialisers EMPTY = new DbInitialisers() {
    @Override
    public Iterator<DbInitialiser> getDbInitialisers()
    {
      return ImmutableSet.<DbInitialiser> of().iterator();
    }
  };
}