package org.cord.mirror;

import java.util.Comparator;

import org.cord.util.ObjectUtil;

/**
 * A TransientRecord Comparator that works be using the natural ordering of a single Comparable
 * field on the records. This is a more specialised version of
 * {@link RecordOperationValueComparator} which will only work on fields rather than generic
 * operations and which stores the field id rather than looking it up for each invocation, thereby
 * making it more efficient.
 * 
 * @author alex
 */
public class FieldValueComparator
  implements Comparator<TransientRecord>
{
  public static final FieldValueComparator BY_ID =
      new FieldValueComparator(TransientRecord.FIELD_ID);

  private final FieldKey<?> __fieldKey;

  public FieldValueComparator(FieldKey<?> fieldKey)
  {
    __fieldKey = fieldKey;
  }

  /**
   * Compare the value in the field in each record in a case-insensitive manner if the value is a
   * String.
   */
  @Override
  public int compare(TransientRecord record1,
                     TransientRecord record2)
  {
    return ObjectUtil.compareIgnoreCase(__fieldKey.call(record1, null),
                                        __fieldKey.call(record2, null));
  }

  @Override
  public String toString()
  {
    return "FieldValueComparator(" + __fieldKey + ")";
  }
}
