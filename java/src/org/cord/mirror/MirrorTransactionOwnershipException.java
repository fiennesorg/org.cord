package org.cord.mirror;

public class MirrorTransactionOwnershipException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = 5859409987771699822L;

  private final Viewpoint __transaction;

  private final PersistentRecord __targetRecord;

  private final Table __targetTable;

  private MirrorTransactionOwnershipException(String message,
                                              Viewpoint transaction,
                                              PersistentRecord targetRecord,
                                              Table targetTable,
                                              Exception nestedException)
  {
    super(message,
          nestedException);
    __transaction = transaction;
    __targetRecord = targetRecord;
    __targetTable = targetTable;
  }

  public MirrorTransactionOwnershipException(Viewpoint attemptedOwner,
                                             PersistentRecord target,
                                             Exception nestedException)
  {
    this(attemptedOwner.debug() + " is not the current owner of " + target
         + " which is in fact owned by " + target.getTransaction(),
         attemptedOwner,
         target,
         null,
         nestedException);
  }

  public MirrorTransactionOwnershipException(Transaction transaction,
                                             Table table,
                                             Exception nestedException)
  {
    this(transaction.debug() + " is not the current lock owner of " + table,
         transaction,
         null,
         table,
         nestedException);
  }

  protected Viewpoint getTransaction()
  {
    return __transaction;
  }

  protected PersistentRecord getTargetRecord()
  {
    return __targetRecord;
  }

  protected Table getTargetTable()
  {
    return __targetTable;
  }
}
