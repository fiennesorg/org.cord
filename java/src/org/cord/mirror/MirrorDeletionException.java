package org.cord.mirror;

public class MirrorDeletionException
  extends MirrorNestedException
{
  /**
   * 
   */
  private static final long serialVersionUID = 399140606415761150L;

  private final PersistentRecord __record;

  public MirrorDeletionException(String message,
                                 Exception coreNestedException,
                                 PersistentRecord record)
  {
    super(message,
          coreNestedException);
    __record = record;
  }

  public final PersistentRecord getRecord()
  {
    return __record;
  }
}
