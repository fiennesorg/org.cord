package org.cord.mirror;

/**
 * An Object that is capable of resolving a given record into a RecordSource.
 * 
 * @author alex
 */
public interface RecordSourceResolver
{
  public RecordSource resolveRecordSource(TransientRecord targetRecord,
                                          Viewpoint viewpoint);
}
