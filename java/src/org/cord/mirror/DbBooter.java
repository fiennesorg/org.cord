package org.cord.mirror;

import org.cord.sql.ConnectionPool;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;

import com.google.common.base.MoreObjects;
import com.google.common.cache.CacheBuilderSpec;

/**
 * Utility class to create a 'standard' empty db class.
 */
public class DbBooter
{
  public final static String INITPARAM_HOSTNAME = "hostName";

  /**
   * The default value for INITPARAM_HOSTNAME. This is currently defined as "localhost" which means
   * that legacy jserv systems which do not define the up-to-date system will continue to function.
   * Modern tomcat systems should utilise "127.0.0.1".
   * 
   * @see #INITPARAM_HOSTNAME
   */
  public final static String INITPARAM_HOSTNAME_DEFAULT = "127.0.0.1";

  public final static String INITPARAM_DATABASE = "database";

  public final static String INITPARAM_USERNAME = "userName";

  public final static String INITPARAM_PASSWORD = "password";

  public final static String INITPARAM_MINCONNECTIONS = "minConnections";

  public final static int INITPARAM_MINCONNECTIONS_DEFAULT = 1;

  public final static String INITPARAM_MAXCONNECTIONS = "maxConnections";

  public final static int INITPARAM_MAXCONNECTIONS_DEFAULT = 10;

  /**
   * @see #INITPARAM_DB_TRANSACTION_TIMEOUT_DEFAULT
   */
  public final static String INITPARAM_DB_TRANSACTION_TIMEOUT = "db.transaction.timeout";

  /**
   * @see #INITPARAM_DB_TRANSACTION_TIMEOUT
   */
  public final static long INITPARAM_DB_TRANSACTION_TIMEOUT_DEFAULT = 600000;

  /**
   * The number of milliseconds between sweeps of the housekeeping thread.
   * 
   * @see #INITPARAM_HOUSEKEEPINGINTERVAL_DEFAULT
   * @see #INITPARAM_HOUSEKEEPINGINTERVAL_MINIMUM
   */
  public final static String INITPARAM_HOUSEKEEPINGINTERVAL = "housekeepingInterval";

  /**
   * The default housekeeping interval utilised if one is not defined (30 seconds).
   * 
   * @see #INITPARAM_HOUSEKEEPINGINTERVAL
   */
  public final static long INITPARAM_HOUSEKEEPINGINTERVAL_DEFAULT = 30000;

  /**
   * The minimum permitted sensible housekeeping interval that is set up to prevent the system
   * thrashing (5 seconds).
   * 
   * @see #INITPARAM_HOUSEKEEPINGINTERVAL
   */
  public final static long INITPARAM_HOUSEKEEPINGINTERVAL_MINIMUM = 5000;

  public final static String INITPARAM_SETTLERATIO = "settleRatio";

  public final static String INITPARAM_SETTLERATIO_DEFAULT = "0.5";

  public final static String INITPARAM_JDBCDRIVERNAME = "jdbcDriverName";

  public final static String INITPARAM_JDBCDRIVERNAME_DEFAULT = "com.mysql.jdbc.Driver";

  public final static String INITPARAM_PROTOCOL = "protocol";
  public final static String INITPARAM_PROTOCOL_DEFAULT = "mysql";

  public final static String INITPARAM_CHARACTERENCODING = "characterEncoding";

  public final static String INITPARAM_CHARACTERENCODING_DEFAULT = "utf-8";

  public static final String CONF_RECORDCACHE = "Db.RecordCache";
  public static final String CONF_RECORDCACHE_DEFAULT = "concurrencyLevel=4,maximumSize=1000";

  public static final String CONF_RECORDCACHEFACTORY = "Db.RecordCacheFactory";
  public static final String CONF_RECORDCACHEFACTORY_DEFAULT =
      "org.cord.mirror.cache.LoadingCacheRecordCache$Factory";

  public static final String CONF_PRECACHESIZE = "Db.PreCacheSize";
  public static final int CONF_PRECACHESIZE_DEFAULT = 500;

  public static final String CONF_RECORDSOURCECACHE = "Db.RecordSourceCache";
  public static final String CONF_RECORDSOURCECACHE_DEFAULT = "concurrencyLevel=4,maximumSize=1000";

  /**
   * @param namespace
   *          The name that is to be prepended onto the standard INITPARAM_xxx names when recalling
   *          parameters from the Gettable. This will be separated by a ".", ie if "namespace" is
   *          "abc" then the name of the INITPARAM_HOSTNAME looked for in the system will be
   *          "abc.hostName". This therefore lets a single Gettable source store the information to
   *          boot several different dbs in parallel.
   */
  public final static Db getDb(Gettable source,
                               String namespace,
                               String transactionPassword,
                               Gettable configuration)
  {
    return getDb(GettableUtils.get(source,
                                   addNamespace(INITPARAM_JDBCDRIVERNAME, namespace),
                                   INITPARAM_JDBCDRIVERNAME_DEFAULT),
                 GettableUtils.get(source,
                                   addNamespace(INITPARAM_PROTOCOL, namespace),
                                   INITPARAM_PROTOCOL_DEFAULT),
                 GettableUtils.get(source,
                                   addNamespace(INITPARAM_HOSTNAME, namespace),
                                   INITPARAM_HOSTNAME_DEFAULT),
                 GettableUtils.get(source, addNamespace(INITPARAM_DATABASE, namespace), null),
                 GettableUtils.get(source, addNamespace(INITPARAM_USERNAME, namespace), null),
                 GettableUtils.get(source, addNamespace(INITPARAM_PASSWORD, namespace), null),
                 transactionPassword,
                 GettableUtils.getInt(source,
                                      addNamespace(INITPARAM_MINCONNECTIONS, namespace),
                                      INITPARAM_MINCONNECTIONS_DEFAULT),
                 GettableUtils.getInt(source,
                                      addNamespace(INITPARAM_MAXCONNECTIONS, namespace),
                                      INITPARAM_MAXCONNECTIONS_DEFAULT),
                 GettableUtils.getLong(source,
                                       addNamespace(INITPARAM_HOUSEKEEPINGINTERVAL, namespace),
                                       INITPARAM_HOUSEKEEPINGINTERVAL_DEFAULT),
                 Float.parseFloat(GettableUtils.get(source,
                                                    addNamespace(INITPARAM_SETTLERATIO, namespace),
                                                    INITPARAM_SETTLERATIO_DEFAULT)),
                 GettableUtils.getLong(source,
                                       INITPARAM_DB_TRANSACTION_TIMEOUT,
                                       INITPARAM_DB_TRANSACTION_TIMEOUT_DEFAULT),
                 getCharacterEncoding(source, namespace),
                 configuration);
  }

  public static String getCharacterEncoding(Gettable source,
                                            String namespace)
  {
    return GettableUtils.get(source,
                             addNamespace(INITPARAM_CHARACTERENCODING, namespace),
                             INITPARAM_CHARACTERENCODING_DEFAULT);
  }

  public static String addNamespace(String parameterName,
                                    String namespace)
  {
    return (namespace == null ? parameterName : namespace + '.' + parameterName);
  }

  // /**
  // * Legacy compatability method that does its best to boot a jserv compatible Db.
  // *
  // * @see #INITPARAM_JDBCDRIVERNAME_DEFAULT
  // * @see #INITPARAM_MINCONNECTIONS_DEFAULT
  // * @see #INITPARAM_HOUSEKEEPINGINTERVAL_DEFAULT
  // * @see #INITPARAM_SETTLERATIO_DEFAULT
  // * @see #getDb(String, String, String, String, String, String, int, int, long, float, long,
  // * String, Gettable)
  // */
  // public final static Db getDb(String hostname,
  // String database,
  // String userName,
  // String password,
  // String transactionPassword,
  // String characterEncoding,
  // Gettable configuration)
  // {
  // return getDb(INITPARAM_JDBCDRIVERNAME_DEFAULT,
  // hostname,
  // database,
  // userName,
  // password,
  // transactionPassword,
  // INITPARAM_MINCONNECTIONS_DEFAULT,
  // INITPARAM_MAXCONNECTIONS_DEFAULT,
  // INITPARAM_HOUSEKEEPINGINTERVAL_DEFAULT,
  // Float.parseFloat(INITPARAM_SETTLERATIO_DEFAULT),
  // INITPARAM_DB_TRANSACTION_TIMEOUT_DEFAULT,
  // characterEncoding,
  // configuration);
  // }

  public final static Db getDb(String jdbcDriverName,
                               String protocol,
                               String hostname,
                               String database,
                               String userName,
                               String password,
                               String transactionPassword,
                               int minConnections,
                               int maxConnections,
                               long housekeepingInterval,
                               float settleRatio,
                               long defaultTransactionTimeout,
                               String characterEncoding,
                               Gettable configuration)
  {
    minConnections = Math.max(minConnections, 2);
    maxConnections = Math.max(minConnections, maxConnections);
    housekeepingInterval = Math.max(INITPARAM_HOUSEKEEPINGINTERVAL_MINIMUM, housekeepingInterval);
    ConnectionPool connectionPool = null;
    try {
      String connectionString = ConnectionPool.getConnectionRequest(protocol,
                                                                    hostname,
                                                                    database,
                                                                    userName,
                                                                    password,
                                                                    characterEncoding);
      System.err.println(connectionString);
      connectionPool = new ConnectionPool(minConnections,
                                          maxConnections,
                                          housekeepingInterval,
                                          settleRatio,
                                          Thread.NORM_PRIORITY - 1,
                                          false,
                                          true,
                                          jdbcDriverName,
                                          connectionString,
                                          database);
    } catch (ClassNotFoundException cnfEx) {
      throw new LogicException("booting up connection pool", cnfEx);
    }
    if (DebugConstants.DEBUG_BOOTUP) {
      DebugConstants.DEBUG_OUT.println(INITPARAM_DB_TRANSACTION_TIMEOUT + ":"
                                       + defaultTransactionTimeout);
    }
    return new Db(database,
                  transactionPassword,
                  connectionPool,
                  defaultTransactionTimeout,
                  CacheBuilderSpec.parse(MoreObjects.firstNonNull(configuration.getString(CONF_RECORDCACHE),
                                                                  CONF_RECORDCACHE_DEFAULT)),
                  MoreObjects.firstNonNull(NumberUtil.toInteger(configuration.getString(CONF_PRECACHESIZE),
                                                                null),
                                           Integer.valueOf(CONF_PRECACHESIZE_DEFAULT))
                             .intValue(),
                  CacheBuilderSpec.parse(MoreObjects.firstNonNull(configuration.getString(CONF_RECORDSOURCECACHE),
                                                                  CONF_RECORDSOURCECACHE_DEFAULT)),
                  configuration);
  }
}
