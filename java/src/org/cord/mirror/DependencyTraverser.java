package org.cord.mirror;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.cord.util.DebugConstants;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

import com.google.common.collect.ImmutableSet;

/**
 * Utility class that works extracts lists of dependent records from an initial starting record.
 * This class is a Singleton with the instance made accessible via <code>getInstance()</code>. It
 * also implements RecordOperation and is automatically registered in all tables under the name
 * DEPENDENCIES ("<code>dependencies"</code> ).
 * 
 * @see #DEPENDENCIES
 * @see #getInstance()
 */
public final class DependencyTraverser
  implements SingletonShutdownable
{
  /**
   * The name that the DependencyTraverser is registered under in all Tables as a RecordOperation,
   * with a value of "<code>dependencies</code>".
   */
  public final static String DEPENDENCIES = "dependencies";

  private static DependencyTraverser _instance = new DependencyTraverser(DEPENDENCIES);

  /**
   * Get a reference to the only instance permitted of DependencyTraverser (it's a Singleton).
   * 
   * @return Instance of DependencyTraverser
   */
  public static DependencyTraverser getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private final String _name;

  private DependencyTraverser(String name)
  {
    _name = name;
    SingletonShutdown.registerSingleton(this);
  }

  /**
   * @return The value of DEPENDENCIES ("dependencies").
   * @see #DEPENDENCIES
   */
  public String getName()
  {
    return _name;
  }

  private Set<PersistentRecord> traverseDependencies(PersistentRecord originalRecord,
                                                     PersistentRecord currentRecord,
                                                     Set<PersistentRecord> dependents,
                                                     boolean allowRecursion,
                                                     int transactionType)
  {
    Iterator<RecordOperation<?>> recordOperations =
        currentRecord.getTable().getRecordOperations().values().iterator();
    while (recordOperations.hasNext()) {
      Object nextObject = recordOperations.next();
      if (nextObject instanceof Dependencies) {
        Dependencies dependencies = (Dependencies) nextObject;
        Iterator<PersistentRecord> dependentRecords =
            dependencies.getDependentRecords(currentRecord, transactionType);
        while (dependentRecords.hasNext()) {
          PersistentRecord nextRecord = dependentRecords.next();
          if (!nextRecord.equals(originalRecord)) {
            if (dependents == null) {
              dependents = new HashSet<PersistentRecord>();
            }
            if (!dependents.contains(nextRecord)) {
              if (DebugConstants.DEBUG_DEPENDENCIES) {
                System.err.println(String.format("DEPENDENCY: %s#%s :: %s --> %s.%s",
                                                 currentRecord.getTableName(),
                                                 Integer.toString(currentRecord.getId()),
                                                 dependencies,
                                                 nextRecord.getTableName(),
                                                 Integer.toString(nextRecord.getId())));
              }
              dependents.add(nextRecord);
              if (allowRecursion) {
                traverseDependencies(originalRecord,
                                     nextRecord,
                                     dependents,
                                     allowRecursion,
                                     transactionType);
              }
            }
          }
        }
      }
    }
    return dependents;
  }

  /**
   * Request the DependencyTraverser to generate a list of all dependent records from an initial
   * record with the option to automatically include the dependent records of these dependent
   * records.
   * 
   * @param startRecord
   *          The initial record from which the dependency tree is going to be generated.
   * @param allowRecursion
   *          If true then a complete recursion tree will be generated, otherwise only records that
   *          are <i>immediately</i> dependent on the startRecord will be included.
   * @return Iterator of PersistentRecord objects. This Iterator will <i>not</i> contain the
   *         original startRecord and wil have zero or more entries.
   */
  public Iterator<PersistentRecord> traverseDependencies(PersistentRecord startRecord,
                                                         boolean allowRecursion,
                                                         int transactionType)
  {
    Set<PersistentRecord> dependents =
        traverseDependencies(startRecord, startRecord, null, allowRecursion, transactionType);
    return dependents == null
        ? ImmutableSet.<PersistentRecord> of().iterator()
        : dependents.iterator();
  }
  /**
   * RecordOperation access to the dependency list which returns a complete list of dependent
   * records including indirect dependencies. This enables the use of webmacro constructs along the
   * lines of:- <blockquote> <code>#foreach $dependentRecord in
   * $record.dependencies {<BR>
   * $dependentRecord.someOperation<BR>
   * }</code></blockquote>
   * 
   * @param record
   *          The starting point for the dependency traversal.
   * @return Iterator of PersistentRecord objects.
   * @see #DEPENDENCIES
   */
  // public Object getOperation(TransientRecord record)
  // throws MirrorTransactionTimeoutException
  // {
  // return getOperation(record,
  // (Transaction) null);
  // }
  // public Object getOperation(TransientRecord record,
  // Transaction transaction)
  // throws MirrorTransactionTimeoutException
  // {
  // return traverseDependencies(record,
  // true,
  // transaction);
  // }
  // 2DO ## Restore DependencyTraverser to being a RecordOperation, will
  // need generation of two instances for update and delete and
  // registration. Some issues concerning the transactional view on the
  // returned data as well.
}
