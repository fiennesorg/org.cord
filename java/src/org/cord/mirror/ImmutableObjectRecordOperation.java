package org.cord.mirror;

import org.cord.mirror.operation.SimpleRecordOperation;

public abstract class ImmutableObjectRecordOperation<T>
  extends SimpleRecordOperation<T>
{
  public ImmutableObjectRecordOperation(RecordOperationKey<T> key)
  {
    super(key);
  }

  private int _objectValuesIndex;

  @Override
  protected void lockStatusChanged()
  {
    switch (getLockStatus()) {
      case LO_INITIALISED:
        try {
          _objectValuesIndex = getTable().getNextObjectValueIndex(this);
        } catch (MirrorTableLockedException e) {
          throw new IllegalStateException(e);
        }
        break;
      default:
    }
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public T getOperation(TransientRecord targetRecord,
                        Viewpoint viewpoint)
  {
    Object[] objectValues = targetRecord.getObjectValues();
    synchronized (objectValues) {
      @SuppressWarnings("unchecked")
      T cachedValue = (T) objectValues[_objectValuesIndex];
      if (cachedValue == null) {
        cachedValue = calculate(targetRecord);
        objectValues[_objectValuesIndex] = cachedValue;
      }
      return cachedValue;
    }
  }

  protected abstract T calculate(TransientRecord record);
}
