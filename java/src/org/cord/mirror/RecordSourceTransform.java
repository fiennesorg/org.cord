package org.cord.mirror;

import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;

/**
 * An implementation of {@link SimpleRecordSourceOperation} that returns a {@link RecordSource} and
 * which supports caching of the result.
 * 
 * @author alex
 */
public abstract class RecordSourceTransform
  extends SimpleRecordSourceOperation<RecordSource>
{
  private final String __cacheKeyPostfix;

  public RecordSourceTransform(RecordSourceOperationKey<RecordSource> key)
  {
    super(key);
    __cacheKeyPostfix = "." + key;
  }

  public Object getCacheKey(RecordSource recordSource)
  {
    Object cacheKey = recordSource.getCacheKey();
    if (cacheKey == null) {
      return null;
    }
    return cacheKey + __cacheKeyPostfix;
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource source,
                                                  Viewpoint viewpoint)
  {
    if (viewpoint != null) {
      return calculateRecordSource(source, null, viewpoint);
    }
    Object cacheKey = getCacheKey(source);
    if (cacheKey != null) {
      RecordSource cached = getTable().getCachedRecordSource(cacheKey);
      if (cached != null) {
        return cached;
      }
    }
    RecordSource calculated = calculateRecordSource(source, cacheKey, null);
    getTable().cacheRecordSource(calculated);
    return calculated;
  }

  protected abstract RecordSource calculateRecordSource(RecordSource source,
                                                        Object cacheKey,
                                                        Viewpoint viewpoint);
}
