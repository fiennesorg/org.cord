package org.cord.mirror;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.cord.util.NumberUtil;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

public final class TableOrderings
  implements SingletonShutdownable
{
  public static final String NAME = "name";

  public static final String R_SIZE = "r_size";

  public static final String R_HITS = "r_hits";

  public static final String R_MISSES = "r_misses";

  public static final String R_EXPIRES = "r_expires";

  public static final String R_EFFICIENCY = "r_efficiency";

  public static final String Q_SIZE = "q_size";

  public static final String Q_HITS = "q_hits";

  public static final String Q_MISSES = "q_misses";

  public static final String Q_EXPIRES = "q_expires";

  public static final String Q_EFFICIENCY = "q_efficiency";

  public static final String Q_SQLTIME = "q_sqlTime";

  public static final String Q_SQLCOUNT = "q_sqlCount";

  public static final String Q_SQLAVERAGETIME = "q_sqlAverageTime";

  public static final String R_READCOUNT = "r_readCount";

  public static final String R_READTIME = "r_readTime";

  public static final String R_UPDATECOUNT = "r_updateCount";

  public static final String R_UPDATETIME = "r_updateTime";

  public static final String R_INSERTCOUNT = "r_insertCount";

  public static final String R_INSERTTIME = "r_insertTime";

  public static final String R_DELETECOUNT = "r_deleteCount";

  public static final String R_DELETETIME = "r_deleteTime";

  private static TableOrderings _instance = new TableOrderings();

  public static TableOrderings getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private final Map<String, Comparator<Table>> __orderings =
      new HashMap<String, Comparator<Table>>();

  private TableOrderings()
  {
    SingletonShutdown.registerSingleton(this);
    __orderings.put(NAME, new Comparator<Table>() {
      @Override
      public int compare(Table arg0,
                         Table arg1)
      {
        return (arg0).getName().compareTo((arg1).getName());
      }
    });
    // __orderings.put(R_SIZE, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordCache().size(), t1.getRecordCache().size())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(R_HITS, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordCache().getHitCount(), t1.getRecordCache()
    // .getHitCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(R_MISSES, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordCache().getMissCount(), t1.getRecordCache()
    // .getMissCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(R_EXPIRES, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordCache().getExpireCount(), t1.getRecordCache()
    // .getExpireCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(R_EFFICIENCY, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordCache().getEfficiency(), t1.getRecordCache()
    // .getEfficiency())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(Q_SIZE, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordSourceCache().size(), t1.getRecordSourceCache()
    // .size())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(Q_HITS, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordSourceCache().getHitCount(),
    // t1.getRecordSourceCache().getHitCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(Q_MISSES, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordSourceCache().getMissCount(),
    // t1.getRecordSourceCache().getMissCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(Q_EXPIRES, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordSourceCache().getExpireCount(),
    // t1.getRecordSourceCache().getExpireCount())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    // __orderings.put(Q_EFFICIENCY, new Comparator<Table>() {
    // public int compare(Table t0,
    // Table t1)
    // {
    // switch (NumberUtil.compare(t0.getRecordSourceCache().getEfficiency(),
    // t1.getRecordSourceCache().getEfficiency())) {
    // case 1:
    // return -1;
    // case -1:
    // return 1;
    // default:
    // return t0.getName().compareTo(t1.getName());
    // }
    // }
    // });
    __orderings.put(Q_SQLTIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getQueryTime(), t1.getQueryTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(Q_SQLCOUNT, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getQueryCount(), t1.getQueryCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(Q_SQLAVERAGETIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getQueryAverageTime(), t1.getQueryAverageTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_READCOUNT, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getReadCount(), t1.getReadCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_READTIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getReadTime(), t1.getReadTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_UPDATECOUNT, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getUpdateCount(), t1.getUpdateCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_UPDATETIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getUpdateTime(), t1.getUpdateTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_INSERTCOUNT, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getInsertCount(), t1.getInsertCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_INSERTTIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getInsertTime(), t1.getInsertTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_DELETECOUNT, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getDeleteCount(), t1.getDeleteCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
    __orderings.put(R_DELETETIME, new Comparator<Table>() {
      @Override
      public int compare(Table t0,
                         Table t1)
      {
        switch (NumberUtil.compare(t0.getDeleteTime(), t1.getDeleteTime())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return t0.getName().compareTo(t1.getName());
        }
      }
    });
  }

  public Comparator<Table> getOrdering(String name)
  {
    return __orderings.get(name);
  }
}
