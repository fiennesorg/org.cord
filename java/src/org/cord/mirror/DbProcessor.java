package org.cord.mirror;

/**
 * Interface that describes an Object that is capable of doing some process to a Db normally in the
 * context of the ProcessDb class.
 * 
 * @author alex
 * @see ProcessDb
 */
public interface DbProcessor
{
  /**
   * Does this DbProcessor have any work to do on a specific Table? This will get invoked once per
   * Table.
   * 
   * @return true if this DbProcessor wants to process the given Table
   */
  public boolean shouldProcess(Table table);

  public void process(WritableRecord record)
      throws MirrorFieldException;
}
