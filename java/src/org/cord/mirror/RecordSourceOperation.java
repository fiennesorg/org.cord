package org.cord.mirror;

/**
 * An Object that is capable of invoking an operation on a RecordSource from a given Viewpoint.
 * 
 * @author alex
 */
public interface RecordSourceOperation<V>
  extends LockableOperation
{
  /**
   * Invoke the operation on the given RecordSource.
   * 
   * @param recordSource
   *          The RecordSource to target with the operation. Should not be null.
   * @param viewpoint
   *          The Transactional viewpoint to utilise while performing the operation. May well be
   *          null (ie no viewpoint)
   * @return The result of the operation. The class of this will depend on what the implementation
   *         is.
   */
  public V invokeRecordSourceOperation(RecordSource recordSource,
                                       Viewpoint viewpoint);
}
