package org.cord.mirror;

/**
 * A RecordValueFactory represents an Object that is capable of generating a value given a record.
 * 
 * @author alex
 * @param <T>
 *          The type of value that this RecordValueFactory will produce
 */
public interface RecordValueFactory<T>
{
  /**
   * Generate the value appropriate to the given record.
   * 
   * @param record
   *          The compulsory record that should be utilised to generate the value
   * @throws NullPointerException
   *           if record is null.
   * @return The appropriate value. This might be null depending on the implementation.
   */
  public T getRecordValue(TransientRecord record);
}
