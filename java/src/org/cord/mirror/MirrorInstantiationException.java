package org.cord.mirror;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.cord.util.LogicException;

public class MirrorInstantiationException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = 3593548344921524312L;

  private final TransientRecord __record;

  private final ArrayList<InstantiationTrigger> __triggers = new ArrayList<InstantiationTrigger>(8);

  private final HashMap<InstantiationTrigger, Exception> __exceptions =
      new HashMap<InstantiationTrigger, Exception>(8);

  private MirrorNestedException __mirrorNestedException = null;

  public MirrorInstantiationException(Exception nestedException,
                                      Object cause,
                                      TransientRecord record)
  {
    super(cause + " failed on " + record,
          nestedException);
    __record = record;
  }

  public void set(MirrorNestedException mnEx)
  {
    if (__mirrorNestedException != null) {
      throw new LogicException("Second MirrorNestedException after " + __mirrorNestedException,
                               mnEx);
    }
    __mirrorNestedException = mnEx;
  }

  public MirrorNestedException getMirrorNestedException()
  {
    return __mirrorNestedException;
  }

  public final void addTriggerException(InstantiationTrigger trigger,
                                        Exception rollbackException)
  {
    if (__triggers.contains(trigger)) {
      throw new LogicException("duplicateTriggers:" + this + ":" + trigger + ":"
                               + rollbackException);
    }
    __triggers.add(trigger);
    __exceptions.put(trigger, rollbackException);
  }

  public final Exception getException(InstantiationTrigger trigger)
  {
    return __exceptions.get(trigger);
  }

  public final Iterator<InstantiationTrigger> getBadRollbackTriggers()
  {
    return __triggers.iterator();
  }

  public final TransientRecord getRecord()
  {
    return __record;
  }
}