package org.cord.mirror;

/**
 * Interface that should be implemented by classes that wish to be informed when a given Table has
 * the state of one of its Records updated.
 * <p>
 * The expected mode of behaviour for a TableListener on receiving this notification should be that
 * it tags itself as having received this action, but does not perform any other steps itself under
 * the invoking thread. The reasons for this are as follows:-
 * <ul>
 * <li>When the method is called, the Table that calls the method will currently be locked down
 * because it is in the process of handling a Transaction that created the change. This will block
 * Queries that reference the Table until the completion of the Transaction. Therefore if a
 * TableListener invokes Queries in rebuilding its internal state based on the notification then
 * these will automatically fail due to timeout exceptions.
 * <li>A TableListener may get notified multiple times of updates before its internal state is next
 * required. It is therefore considerably more efficient to only rebuild the internal state once
 * (when needed) rather than on every invocation of tableChanged().
 * <li>In a situation when large numbers of TableListeners are registered on a Table but are invoked
 * infrequently, the apparent performance will be much greater if on-demand recalculation of data is
 * performed rather than immediate.
 * </ul>
 * The TableListener can make life easier for the Table by choosing to 'opt out' of future updates
 * by returning false to this call. In this case, they will be automatically removed from the list
 * of TableListeners and therefore will not receive further updates. Please note that this is the
 * responsibility of the TableListener to re-register themselves as listeners if and when the
 * processing from the previous update has been completed.
 * <p>
 * This is particularly useful when the recalculation of the state of the TableListener is performed
 * "on-demand" because then as soon as the first tableChanged(...) message comes in, the listener
 * deregisters itself from the table and stays that way until the state is rebuilt. In this way the
 * number of tableChanged messages passed is kept to an absolute minimum.
 */
public interface TableListener
{
  /**
   * Inform the TableListener that the given Table has updated a given record.
   * 
   * @param table
   *          The Table that owns the updated record.
   * @param recordId
   *          The id of the updated record.
   * @param type
   *          Either TRANSACTION_DELETE or TRANSACTION_UPDATE. Please note that in the case of the
   *          deleted option, the record will no longer be available and in the updated state then
   *          the value in the PersistentRecord will now have it's new updated values.
   * @return True if the listener wants to remain connected to the Table as a listener, false if the
   *         listener doesn't wish to receive any more updates.
   * @see Transaction#TRANSACTION_DELETE
   * @see Transaction#TRANSACTION_UPDATE
   */
  public boolean tableChanged(Table table,
                              int recordId,
                              PersistentRecord persistentRecord,
                              int type);
}
