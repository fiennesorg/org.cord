package org.cord.mirror;

import java.sql.PreparedStatement;

import org.cord.mirror.trigger.RestrictedInstantiation;
import org.cord.util.Gettable;
import org.cord.util.StringUtils;

/**
 * A TableWrapper that only permits instantiation of records via the protected makePersistent
 * method. It is up to subclasses to decide if and how this method should be made available to
 * clients.
 * 
 * @author alex
 */
public abstract class RestrictedInstantiationTableWrapper
  extends AbstractTableWrapper
{
  private final RestrictedInstantiation __restrictedInstantiation;

  public RestrictedInstantiationTableWrapper(String tableName,
                                             String instantiationErrorMessage)
  {
    super(tableName);
    instantiationErrorMessage =
        StringUtils.padEmpty(instantiationErrorMessage,
                             String.format("%s does not support public instantiation of records",
                                           tableName));
    __restrictedInstantiation = new RestrictedInstantiation(instantiationErrorMessage);
  }

  /**
   * @see RestrictedInstantiation
   * @see #initTableSub(Db, String, Table)
   */
  @Override
  protected final void initTable(Db db,
                                 String pwd,
                                 Table table)
      throws MirrorTableLockedException
  {
    table.addPreInstantiationTrigger(__restrictedInstantiation);
    initTableSub(db, pwd, table);
  }

  protected abstract void initTableSub(Db db,
                                       String pwd,
                                       Table table)
      throws MirrorTableLockedException;

  protected PersistentRecord makePersistent(TransientRecord record,
                                            Gettable initParams,
                                            PreparedStatement preparedStatement,
                                            boolean shouldCacheRecord)
      throws MirrorInstantiationException
  {
    TransientRecord.assertIsTable(record, getTable());
    initParams = __restrictedInstantiation.addInstantiationPermission(initParams);
    return record.makePersistent(initParams, preparedStatement, shouldCacheRecord);
  }

  protected PersistentRecord makePersistent(TransientRecord record,
                                            Gettable initParams)
      throws MirrorInstantiationException
  {
    return makePersistent(record, initParams, null, true);
  }
}
