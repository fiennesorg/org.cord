package org.cord.mirror;

public class MirrorRuntimeException
  extends RuntimeException
{
  /**
   * An exception that is caused by the mirror system that should not be expected under normal
   * circumstances. Genereally clients should not expect to catch MirrorRuntimeExceptions except in
   * specifix circumstances.
   */
  private static final long serialVersionUID = 1L;

  public MirrorRuntimeException(String message,
                                Throwable cause)
  {
    super(message);
    initCause(cause);
  }
}