package org.cord.mirror;

import org.cord.util.Gettable;

/**
 * Interface that describe Objects that are capable of processing a record with the information
 * contained in a set of Gettable params. This will commonly include updating the state of the
 * record with the information in params.
 * 
 * @author alex
 */
public interface GettableRecordProcessor
{
  /**
   * Do whatever work is defined in the params file on the given record.
   */
  public void process(TransientRecord record,
                      Gettable params,
                      Transaction transaction)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException;
}
