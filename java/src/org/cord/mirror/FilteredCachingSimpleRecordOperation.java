package org.cord.mirror;

import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * BaseClass for RecordOperations that cache a value which is then transformed into a V each time
 * that the RecordOperation is invoked.
 * 
 * @author alex
 * @param <C>
 *          The class of the cached value
 * @param <V>
 *          The class of the final transformed value that is returned to the user.
 */
public abstract class FilteredCachingSimpleRecordOperation<C, V>
  extends CachingSimpleRecordOperation<V>
{
  private final Object __cacheKey;

  public FilteredCachingSimpleRecordOperation(RecordOperationKey<V> name,
                                              Object cacheKey)
  {
    super(name);
    __cacheKey = Preconditions.checkNotNull(cacheKey, "cacheKey");
  }

  public FilteredCachingSimpleRecordOperation(RecordOperationKey<V> name)
  {
    this(name,
         name);
  }

  /**
   * Check to see whether or not there is already a cached value,
   * {@link #calculate(TransientRecord, Viewpoint)} if not, and then finally pass the cached value
   * through {@link #filter(TransientRecord, Viewpoint, Object)}
   * 
   * @param viewpoint
   *          The Viewpoint to resolve this operation against. If this is not null then the cache
   *          will be bypassed and {@link #calculate(TransientRecord, Viewpoint)} and
   *          {@link #filter(TransientRecord, Viewpoint, Object)} will always be invoked to avoid us
   *          caching temporary values.
   */
  @SuppressWarnings("unchecked")
  @Override
  protected final V getOperation(TransientRecord record,
                                 Viewpoint viewpoint,
                                 Map<Object, Object> cache)
  {
    if (viewpoint != null) {
      return filter(record, viewpoint, calculate(record, viewpoint));
    }
    C cached = (C) cache.get(__cacheKey);
    if (cached == null) {
      cached = calculate(record, viewpoint);
      cache.put(__cacheKey, cached);
    }
    return filter(record, viewpoint, cached);
  }

  /**
   * calculate the C value of invoking this RecordOperation because there isn't a cached value.
   */
  public abstract C calculate(TransientRecord record,
                              Viewpoint viewpoint);

  /**
   * filter the cached C value into a public V value. This will be invoked every time that the
   * RecordOperation is invoked.
   */
  public abstract V filter(TransientRecord record,
                           Viewpoint viewpoint,
                           C cached);

}
