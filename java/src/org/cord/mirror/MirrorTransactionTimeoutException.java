package org.cord.mirror;

public class MirrorTransactionTimeoutException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = 4898545985046724463L;

  private final PersistentRecord __record;

  private final Transaction __transaction;

  private final long __timeout;

  private final String __blockingTransactionName;

  /**
   * @param blockingTransactionName
   *          the name of the Transaction that blocked the request causing this exception.
   */
  public MirrorTransactionTimeoutException(String message,
                                           Transaction transaction,
                                           PersistentRecord record,
                                           long timeout,
                                           String blockingTransactionName,
                                           Exception nestedException)
  {
    super(message,
          nestedException);
    __blockingTransactionName = blockingTransactionName;
    __transaction = transaction;
    __record = record;
    __timeout = timeout;
  }

  /**
   * Get the name of the Transaction that blocked the request causing this exception. This may then
   * be utilised to get access to the Transaction if the client has the appropriate
   * transactionPassword knowledge.
   * 
   * @see Db#getTransaction(String,String,String)
   * @see Db#getTransaction(String)
   */
  public final String getBlockingTransactionName()
  {
    return __blockingTransactionName;
  }

  public final Transaction getTransaction()
  {
    return __transaction;
  }

  public final PersistentRecord getRecord()
  {
    return __record;
  }

  public final long getTimeout()
  {
    return __timeout;
  }

  @Override
  public String toString()
  {
    return String.format("MirrorTransactionTimeoutException(\"%s\", on %s, timeout: %s, blocked by: %s)",
                         getMessage(),
                         __record,
                         Long.toString(__timeout),
                         __blockingTransactionName);
  }
}
