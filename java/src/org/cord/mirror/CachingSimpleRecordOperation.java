package org.cord.mirror;

import java.util.Map;

import org.cord.mirror.operation.AbstractLockableOperation;

/**
 * Base class for RecordOperations that want access to the caching infrastructure for values.
 * 
 * @param <V>
 *          The class of Value that the RecordOperation ultimately returns
 */
public abstract class CachingSimpleRecordOperation<V>
  extends AbstractLockableOperation
  implements RecordOperation<V>
{
  private final RecordOperationKey<V> __key;

  public CachingSimpleRecordOperation(RecordOperationKey<V> key)
  {
    super(key.getKeyName());
    __key = key;
  }

  @Override
  public final RecordOperationKey<V> getKey()
  {
    return __key;
  }

  /**
   * Create or resolve the cachedValues Map and invoke
   * {@link #getOperation(TransientRecord, Viewpoint, Map)}
   */
  @Override
  public final V getOperation(TransientRecord targetRecord,
                              Viewpoint viewpoint)
  {
    return getOperation(targetRecord, viewpoint, targetRecord.createCachedValues());
  }

  /**
   * Resolve the value of this RecordOperation using the cache as necessary
   */
  protected abstract V getOperation(TransientRecord record,
                                    Viewpoint viewpoint,
                                    Map<Object, Object> cache);

  @Override
  public void shutdown()
  {
  }

}
