package org.cord.mirror;

import org.cord.util.Gettable;

/**
 * Describes objects that wish to perform some operation that is triggered by the transition of a
 * record from Transient to Persistent state. These objects should be registered on the appropriate
 * table via Table.add(instantiationTrigger).
 * 
 * @see Table#addPreInstantiationTrigger(PreInstantiationTrigger)
 */
public interface PreInstantiationTrigger
  extends InstantiationTrigger
{
  /**
   * Perform whatever action is required on the transient record before it is committed. All
   * InstantiationTriggers will have this method called with respect to a specific record before the
   * database commit is performed. If any of the triggers fail (by throwing an exception), then all
   * previous triggers will have their rollbackTrigger method called.
   * 
   * @param record
   *          The record that is to be instantiated
   * @param params
   *          The Gettable parameters that were passed to the makePersistent method that triggered
   *          this trigger.
   * @throws MirrorException
   *           If the trigger failed for any reason. Please note that the trigger should <i>not</i>
   *           throw an exception unless it has cleaned up after itself as much as possible as it
   *           will not be touched again in this cycle.
   * @see #rollbackPre(TransientRecord, Gettable)
   * @see TransientRecord#makePersistent(Gettable)
   */
  public void triggerPre(TransientRecord record,
                         Gettable params)
      throws MirrorException;

  /**
   * Inform this trigger that a later trigger has failed to complete and therefore the operation has
   * to be aborted. Any effects that were created by the previous call to triggerInstantio should be
   * reversed as much as possible. If this is not possible, then an exception should be thrown
   * containing as much relevant information as possible which will be collated into the
   * MirrorInstantiationException that will be being generated from the original error.
   * 
   * @param record
   *          The record who's instantiation is being aborted.
   * @param params
   *          The Gettable parameters that were passed to the makePersistent method that triggered
   *          this trigger.
   * @throws MirrorException
   *           If the trigger rollback cannot be achieved for whatever reason. Please note that all
   *           earlier triggers will still get asked to rollback even if the exception is thrown.
   */
  public void rollbackPre(TransientRecord record,
                          Gettable params)
      throws MirrorException;

  /**
   * Tell the trigger that the instantiation has been completed and any rollback information
   * relating to this record can be released.
   * 
   * @param record
   *          The record that has been committed.
   * @param params
   *          The Gettable parameters that were passed to the makePersistent method that triggered
   *          this trigger.
   */
  public void releasePre(TransientRecord record,
                         Gettable params);
}
