package org.cord.mirror;

import java.sql.SQLException;
import java.sql.Statement;

public final class WritableRecord
  extends PersistentRecord
{
  protected static WritableRecord getInstance(PersistentRecord persistentRecord)
  {
    Object[] objectValues = persistentRecord.getObjectValues().clone();
    int[] intValues = persistentRecord.getIntValues().clone();
    for (RecordStateDataFilter writableDataFilter : persistentRecord.getTable()
                                                                    .getUnmodifiableRecordStateDataFilters()) {
      writableDataFilter.persistentToWritable(persistentRecord, objectValues, intValues);
    }
    return new WritableRecord(persistentRecord, objectValues, intValues);
  }

  private final PersistentRecord __sourceRecord;

  private WritableRecord(PersistentRecord sourceRecord,
                         Object[] objectValues,
                         int[] intValues)
  {
    super(sourceRecord.getTable(),
          objectValues,
          intValues,
          sourceRecord.getTransaction());
    __sourceRecord = sourceRecord;
  }

  public boolean isChangedField(Field<?> field)
  {
    return !field.isEqualValues(this, __sourceRecord);
  }

  public boolean isChangedField(FieldKey<?> fieldKey)
  {
    return isChangedField(getTable().getField(fieldKey));
  }

  @Override
  public Object setField(Object fieldKey,
                         Object value)
      throws MirrorFieldException, IllegalArgumentException, IllegalStateException
  {
    try {
      getTransaction().checkValidity();
    } catch (MirrorTransactionTimeoutException mttEx) {
      throw new IllegalStateException(this
                                      + " has timed out (this _should_ be MirrorTransactionTimeoutException)",
                                      mttEx);
    }
    return super.setField(fieldKey, value);
  }

  /**
   * @return true, because WritableRecords are writable!
   */
  @Override
  public boolean isWriteable()
  {
    return true;
  }

  public final static String WRITABLE_RECORD = "Writable";

  @Override
  public String getRecordType()
  {
    return WRITABLE_RECORD;
  }

  /**
   * Internal method called by PersistentRecord during commitTransaction(transaction) that updates
   * the database (if necessary) and returns the current value map. The method does not inform the
   * Table of the change that has taken place, rather deferring the responsibility to the
   * PersistentRecord so that the state of the PersistentRecord is in sync with the db before the
   * notification is instigated.
   * 
   * @return true if the record had changed and the SQL was updated, false if there was no
   *         operation.
   * @throws java.sql.SQLException
   *           If the update of the backend db failed in some way. This will happen before any table
   *           listeners have been informed of any change and it is up to the client to decide what
   *           to do about this (normally it will mean that the update has completed failed which is
   *           OK.
   */
  protected boolean performUpdate(Statement statement)
      throws SQLException
  {
    if (getIsUpdated()) {
      String sql = buildUpdateString();
      if (sql != null) {
        Db.executeUpdate(statement, buildUpdateString());
        return true;
      }
    }
    return false;
  }

  private String buildUpdateString()
  {
    Table table = getTable();
    PersistentRecord persistentRecord = getPersistentRecord();
    StringBuilder sql = new StringBuilder(128);
    sql.append("update ").append(getTable().getName()).append(" set ");
    int count = table.getFieldCount();
    boolean hasFirst = false;
    for (int i = 1; i < count; i++) {
      Field<?> fieldFilter = table.getField(i);
      if (!fieldFilter.isEqualValues(this, persistentRecord)) {
        if (hasFirst) {
          sql.append(',');
        } else {
          hasFirst = true;
        }
        sql.append(fieldFilter.getName()).append('=');
        fieldFilter.appendSqlObjectValue(sql, this, fieldFilter.getFieldValue(this));
      }
    }
    sql.append(" where id=").append(getId());
    return hasFirst ? sql.toString() : null;
  }
  /**
   * Get the PersistentRecord that holds the original information that is used to create this
   * WritableRecord. By definition, the data in the persistent record will be consistent with that
   * in the database and therefore this permits before/after comparisons between the two states.
   * 
   * @return PersistentRecord that was used to create this WritableRecord.
   */
  @Override
  public PersistentRecord getPersistentRecord()
  {
    return __sourceRecord;
  }

  /**
   * Delegate the attempt to acquire the write lock on this WritableRecord to the PersistentRecord
   * that it was created from.
   * 
   * @see PersistentRecord#acquireWriteLock(Transaction,long)
   * @see #getPersistentRecord()
   */
  @Override
  protected boolean acquireWriteLock(Transaction transaction,
                                     long timeout)
      throws MirrorTransactionTimeoutException
  {
    return getPersistentRecord().acquireWriteLock(transaction, timeout);
  }

  /**
   * @return this
   */
  @Override
  protected WritableRecord getWritableRecord()
  {
    return this;
  }

  /**
   * @return The optional viewpoint or the Transaction which currently has the lock on this
   *         WritableRecord if viewpoint is null.
   */
  @Override
  protected Viewpoint defaultIfNull(Viewpoint viewpoint)
  {
    return viewpoint == null ? getTransaction() : viewpoint;
  }

  public <V> V opt(RecordOperationKey<V> key,
                   Viewpoint viewpoint)
  {
    return key.call(this, defaultIfNull(viewpoint));
  }

  /**
   * @return the type of the Transaction that currently has the lock on this WritableRecord. Either
   *         {@link Transaction#TRANSACTION_DELETE} or {@link Transaction#TRANSACTION_UPDATE}
   */
  @Override
  public int getTransactionType()
  {
    return getTransaction().getType();
  }
}
