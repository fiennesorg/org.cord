package org.cord.mirror;

import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

/**
 * A MirrorRuntimeException to indicate that a requested record that has been requested is not
 * available when requested explicitly by id. This should only be thrown in situations where the
 * client would normally expect that the id should be resolveable. One example would be is if the
 * record has been deleted while it is in the process of being resoved, and on a higher level then
 * invoking a method that is expected to work under normal circumstances should throw a
 * MissingRecordException if it fails. If it is "acceptable" for the method to fail, then a checked
 * MirrorNoSuchRecordException should be thrown instead.
 * 
 * @see MirrorNoSuchRecordException
 * @author alex
 */
public class MissingRecordException
  extends MirrorRuntimeException
{
  private static final long serialVersionUID = 1L;

  private final Table __table;

  private final Integer __id;

  /**
   * @param table
   *          The Table that has been modified. Must be defined
   * @param id
   *          The id that is missing (if known)
   */
  public MissingRecordException(String message,
                                Table table,
                                Integer id,
                                Throwable cause)
  {
    super(String.format("%s for %s#%s",
                        StringUtils.padEmpty(message, "MissingRecordException"),
                        table,
                        id),
          cause);
    __table = Preconditions.checkNotNull(table, "table");
    __id = id;
  }

  public final Table getTable()
  {
    return __table;
  }

  public final Integer getId()
  {
    return __id;
  }
}
