package org.cord.mirror;

import java.io.IOException;
import java.lang.ref.Reference;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.DuoRecordOperationKey;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.util.AbstractGettable;
import org.cord.util.Assertions;
import org.cord.util.ExceptionUtil;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.LogicException;
import org.cord.util.NamedComparator;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.SortedIterator;
import org.cord.util.StringUtils;
import org.cord.util.TypedKey;
import org.cord.util.UndefinedCompException;
import org.cord.util.ValueType;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.Null;
import org.json.WritableJSONObject;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.objects.Object2ObjectMaps;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

/**
 * TransientRecord is the simplest of the record objects and represents a new record that has not
 * yet been reflected in the database.
 */
public class TransientRecord
  extends AbstractGettable
  implements TransactionGettable, Comparable<TransientRecord>, Settable, TableWrapper
{
  public final static Comparator<TransientRecord> BYID = new Comparator<TransientRecord>() {
    @Override
    public int compare(TransientRecord o1,
                       TransientRecord o2)
    {
      return Integer.compare(o1.getId(), o2.getId());
    }
  };

  /**
   * The name of the standard field for storing the unique integer that identifies all records
   * reflected by the mirror system, with a value of " <code>id</code>".
   */
  public final static IntFieldKey FIELD_ID = new IntFieldKey("id");

  public final static int FIELD_ID_INDEX = 0;

  /**
   * The default value for FIELD_ID when the record does not have a persistent reflection in the
   * database, value 0.
   */
  public final static int ID_TRANSIENT = 0;

  public final static int ID_MISSING = -1;

  private Object[] _objectValues;

  private int[] _intValues;

  private final Table __table;

  private boolean _isUpdated;

  private boolean _hasBeenCloned = false;

  private volatile Map<Object, Object> _cachedValues = null;

  private int _pendingId;

  /**
   * Internal method to create a new TransientRecord object with a known db id. This method is
   * called by PersistentRecord when initialising itself as a newly serialized database record and
   * it wants to insert the id over the existing value.
   * 
   * @param table
   *          The Table that this TransientRecord will belong to when it is committed.
   * @param persistentId
   *          The persistent id that this record is associated with in the database or null if this
   *          really is a TransientRecord as opposed to some form of PersistentRecord and therefore
   *          doesn't have an SQL representation yet.
   */
  protected TransientRecord(Table table,
                            Object[] objectValues,
                            int[] intValues,
                            int persistentId,
                            int pendingId)
  {
    __table = table;
    setFields(objectValues, intValues);
    _isUpdated = false;
    if (persistentId > 0) {
      _intValues[FIELD_ID_INDEX] = persistentId;
    } else {
      persistentId = _intValues[FIELD_ID_INDEX];
    }
    _pendingId = pendingId > 0 ? pendingId : persistentId;
  }

  /**
   * Get the id that this record will utilise in the SQL database if you make it Persistent. If it
   * is already persistent then it will return the existing SQL id.
   * 
   * @return the appropriate id, never null
   */
  public final int getPendingId()
  {
    return _pendingId;
  }

  /**
   * Check to see if this record is from a Table with the given name and throw an
   * IllegalArgumentException if not.
   * 
   * @throws IllegalArgumentException
   *           if record is not from a table named tableName
   * @see Table#assertIsNamed(String)
   */
  public final void assertIsTableNamed(String tableName)
  {
    getTable().assertIsNamed(tableName);
  }

  /**
   * @throws IllegalArgumentException
   *           if this record is not from the given table
   */
  public final void assertIsTable(Table table)
  {
    if (getTable() != table) {
      throw Assertions.printStackTrace(new IllegalArgumentException(String.format("%s is not from %s",
                                                                                  this,
                                                                                  table)));
    }
  }

  /**
   * Check to see that a record is not null and that it is from tableName.
   * 
   * @throws NullPointerException
   *           if record is null
   * @throws IllegalArgumentException
   *           if record is not from a table named tableName
   */
  public final static <T extends TransientRecord> T assertIsTableNamed(T record,
                                                                       String tableName)
  {
    if (record == null) {
      throw new NullPointerException("null is not from " + tableName);
    }
    record.assertIsTableNamed(tableName);
    return record;
  }

  /**
   * Check to see if the given record is from the given Table.
   * 
   * @param <T>
   *          The type of the record
   * @param record
   *          The record to check
   * @param table
   *          The table to check that the record is from
   * @return record
   * @throws NullPointerException
   *           if record is null
   * @throws IllegalArgumentException
   *           if the record is not from the table
   * @see TransientRecord#assertIsTable(Table)
   */
  public final static <T extends TransientRecord> T assertIsTable(T record,
                                                                  Table table)
  {
    if (record == null) {
      throw Assertions.printStackTrace(new NullPointerException(String.format("null is not from "
                                                                              + table)));
    }
    record.assertIsTable(table);
    return record;
  }

  public boolean is(RecordOperationKey<Boolean> fieldKey)
  {
    return comp(fieldKey).booleanValue();
  }

  public int compInt(IntFieldKey intFieldKey)
  {
    return intFieldKey.getInt(this);
  }

  /**
   * Check to see if the given record is from the Table that the tableWrapper encapsulates
   * 
   * @param <T>
   *          The type of the record
   * @param record
   *          The record to check
   * @param tableWrapper
   *          The TableWrapper that contains the Table to check that the record is from
   * @return record
   * @throws NullPointerException
   *           if record is null
   * @throws IllegalArgumentException
   *           if the record is not from the table
   * @see TableWrapper#getTable()
   */
  public final static <T extends TransientRecord> T assertIsTable(T record,
                                                                  TableWrapper tableWrapper)
  {
    return assertIsTable(record, tableWrapper.getTable());
  }

  protected Map<Object, Object> createCachedValues()
  {
    Map<Object, Object> cachedValues = _cachedValues;
    if (cachedValues == null) {
      synchronized (this) {
        cachedValues = _cachedValues;
        if (cachedValues == null) {
          _cachedValues =
              cachedValues = Object2ObjectMaps.synchronize(new Object2ObjectOpenHashMap<>());
        }
      }
    }
    return cachedValues;
  }

  @Override
  public int compareTo(TransientRecord that)
  {
    if (this == that) {
      return 0;
    }
    if (!getTable().equals(that.getTable())) {
      throw new ClassCastException("Cannot compareTo between records in different tables");
    }
    return Integer.compare(this.getId(), that.getId());
  }

  @Override
  public String toString()
  {
    return TRANSIENT_RECORD + "(" + __table + ", #" + getPendingId() + ")";
  }

  public final static String TRANSIENT_RECORD = "Transient";

  public String getRecordType()
  {
    return TRANSIENT_RECORD;
  }

  /**
   * Check to see if this Record has had the values of any of its fields changed since it was
   * initialised. Please note that setting a field with a value that satisfies the
   * <code>Object.equals(Object)</code> test does not count as updated.
   * 
   * @return true if the record has been changed since initialisation.
   */
  public final boolean getIsUpdated()
  {
    return _isUpdated;
  }

  protected final boolean setIsUpdated()
  {
    if (_isUpdated) {
      return false;
    }
    _isUpdated = true;
    Transaction t = getTransaction();
    if (t != null) {
      t.addUpdatedTable(getTable());
    }
    return true;
  }

  /**
   * Return the value of the id field of this record, identified by FIELD_ID.
   * 
   * @return The id of this record which will either be the corresponding value from the database or
   *         ID_TRANSIENT if the record hasn't been made persistent yet.
   * @see #FIELD_ID
   * @see #ID_TRANSIENT
   */
  public final int getId()
  {
    return _intValues[TransientRecord.FIELD_ID_INDEX];
  }

  /**
   * Check to see if this record is currently in a writeable state. If not, then calls to
   * <code>setField(fieldFilter, value)</code> will throw an exception if called.
   */
  public boolean isWriteable()
  {
    return !_hasBeenCloned;
  }

  /**
   * Does the Table that contains this record have the given tablename?
   * 
   * @return true if the tablename equals the Table's name. false if they are different or if
   *         tablename is null.
   */
  public final boolean isTable(String tablename)
  {
    return getTable().getName().equals(tablename);
  }

  /**
   * Does the Table that contains this record equal the given Table?
   * 
   * @return true if the Tables are equal. false if they are different or if table is null.
   */
  public final boolean isTable(Table table)
  {
    return getTable().equals(table);
  }

  /**
   * Get the FieldFilter that is responsible for the handling of fieldName. This delegates to
   * getFieldFilter(fieldName) in the appropriate Table for this TransientRecord.
   * 
   * @param fieldName
   *          The name of the field that is being requested.
   * @return The FieldFilter or null if the name is not recognised, or is an instance of some other
   *         RecordOperation.
   * @see Table#getField(String)
   */
  public final Field<?> getFieldFilter(String fieldName)
  {
    return getTable().getField(fieldName);
  }

  /**
   * Attempt to set the value of the named field in this record. The method first looks up the
   * FieldFilter associated with the fieldName using <code>Table.getFieldFilter(fieldName)</code>
   * and then validates the supplied value against the FieldFilter. If the value is different from
   * the existing value then the record is tagged as having been updated.
   * 
   * @param fieldKey
   *          The name of the field that is to be updated.
   * @param value
   *          The Object that represents the value that should be stored. Note that if you pass a
   *          value of null in as the value then the field will revert to the default value of
   *          FieldFilter.validateValue(null) which is normally the default value for the field. If
   *          you want null values to have no effect then you should use setFieldIfDefined
   * @return The validated version of value that has been stored in the record.
   * @throws java.lang.IllegalArgumentException
   *           If the fieldName is not associated with a valid field on the Table or if the user
   *           does not have permission to write to the field.
   * @throws IllegalStateException
   *           If the record is not a writeable record.
   * @throws NullUnsupportedException
   *           If the Db has been configured to throw the exception and you are trying to set a
   *           field to null that doesn't support null values. If the Db has not been configured to
   *           behave in this manner (ie the legacy behaviour) then this action will cause the
   *           default defined value to be utilised instead.
   * @see Table#getField(String)
   * @see Db#CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL
   */
  public Object setField(Object fieldKey,
                         Object value)
      throws MirrorFieldException, NullUnsupportedException
  {
    String fieldName = fieldKey.toString();
    if (!isWriteable()) {
      Transaction t = this.getTransaction();
      if (t == null) {
        System.err.println("No Transaction lock");
      } else {
        System.err.println("???" + t.debug());
      }
      throw new IllegalStateException(String.format("%s.setField(%s, %s):  not in writeable state",
                                                    this,
                                                    fieldName,
                                                    value));
    }
    Table table = getTable();
    int fieldIndex = table.getFieldIndex(fieldName);
    if (fieldIndex == 0) {
      throw new IllegalArgumentException("You do not have permission to set id on " + this);
    }
    return table.getField(fieldIndex).setRecordToValue(this, value);
  }

  /**
   * Set a Field to an int value. This currently just invokes {@link #setField(Object, Object)}
   * using {@link Integer#valueOf(int)} to wrap up value, but this may be optimised in the future to
   * avoid Integer creation and therefore should be utilised when possible.
   */
  public Object setField(Object fieldKey,
                         int value)
      throws NullUnsupportedException, MirrorFieldException
  {
    return setField(fieldKey, Integer.valueOf(value));
  }

  /**
   * Set the named field to the id of the given record. This is equivalent to doing
   * record.setField(fieldName, targetRecord.getId()) but easier to type and easier to read. If
   * fieldName doesn't resolve to a LinkField then targetRecord is passed through to the
   * conventional setField method where it will be treated as any other Object.
   * 
   * @param targetRecord
   *          The record which you want the field to point at. If this is null then the default
   *          value will be utilised instead. If the LinkField is compulsory then this will result
   *          in an error being generated.
   * @throws IllegalArgumentException
   *           If the supplied targetRecord is not from the Table that the LinkField fieldName is
   *           expecting to point at. If fieldName is not a LinkField then the id is just set
   *           without performing the check.
   * @see #setField(Object, Object)
   */
  public Object setField(Object fieldKey,
                         PersistentRecord targetRecord)
      throws MirrorFieldException
  {
    String fieldName = fieldKey.toString();
    if (targetRecord == null) {
      return setField(fieldName, (Object) null);
    }
    Field<?> field = getFieldFilter(fieldName);
    if (field instanceof LinkField) {
      LinkField linkField = (LinkField) field;
      if (!linkField.getTargetTable().equals(targetRecord.getTable())) {
        throw new MirrorFieldException(String.format("Cannot set the value of %s.%s to %s because it is not a %s record",
                                                     this,
                                                     fieldName,
                                                     targetRecord,
                                                     linkField.getTargetTable()),
                                       null,
                                       this,
                                       fieldName,
                                       targetRecord,
                                       null);
      }
      return setField(fieldName, Integer.valueOf(targetRecord.getId()));
    } else {
      return setField(fieldName, (Object) targetRecord);
    }
  }

  /**
   * Utility method that sets a field with the contents of the Optional maybeValue if defined,
   * otherwise sets the field to null.
   */
  public <V> Object setField(Object fieldKey,
                             Optional<V> maybeValue)
      throws NullUnsupportedException, MirrorFieldException
  {
    return (maybeValue.isPresent())
        ? setField(fieldKey, maybeValue.get())
        : setField(fieldKey, (Object) null);
  }

  /**
   * Set a value on this record if the value is defined (ie not null) otherwise do nothing. This is
   * different from the behavious of setField(fieldName, value) because this will set the field to
   * the default value if null is passed in. If the field is optional then you might want to utilise
   * {@link #setFieldIfDefined(Object, Gettable, Object)} if you are getting data out of a Gettable.
   * 
   * @param fieldKey
   *          The name of the field that is to be set
   * @param value
   *          The value that we would like it to be set to if it isn't null.
   * @return The result of invoking setField if value is defined, otherwise null so that one knows
   *         whether or not the field was updated at all.
   * @see #setField(Object,Object)
   * @throws MirrorFieldException
   *           If the value was rejected in some way.
   */
  public final Object setFieldIfDefined(Object fieldKey,
                                        Object value)
      throws MirrorFieldException
  {
    String fieldName = fieldKey.toString();
    return value == null ? null : setField(fieldName, value);
  }

  /**
   * If gettable contains the given key then set fieldName with the value that key maps to. This is
   * useful for conditional updating of optional values which is harder with
   * {@link #setFieldIfDefined(Object, Object)} because you cannot distinguish between an explicit
   * value of null and no value.
   * 
   * @return true if gettable contained the field and the field was updated, or false if gettable
   *         didn't contain the key and no action was taken.
   */
  public final boolean setFieldIfDefined(Object fieldKey,
                                         Gettable gettable,
                                         Object gettableKey)
      throws MirrorFieldException
  {
    String gettableName = gettableKey.toString();
    if (gettable.containsKey(gettableName)) {
      setField(fieldKey.toString(), gettable.get(gettableName));
      return true;
    }
    return false;
  }

  /**
   * Protected method to permit filtering FieldTriggers that are caught by the invokoking Table to
   * update the value in this record without invoking all the field triggers. The proposed value
   * will be passed through the appropriate FieldFilter and then stored in the map.
   * 
   * @param fieldKey
   *          The key of the field to set.
   * @param filteredValue
   *          The Object that is to be associated with the fieldName.
   * @param filter
   *          The FieldTrigger that is responsible for the filtering request.
   * @return The filteredValue as it was stored in the values map after being validated by the
   *         FieldFilter
   * @throws NullUnsupportedException
   *           If the Db has been configured to throw the exception and you are trying to set a
   *           field to null that doesn't support null values. If the Db has not been configured to
   *           behave in this manner (ie the legacy behaviour) then this action will cause the
   *           default defined value to be utilised instead.
   */
  protected final Object setField(Object fieldKey,
                                  Object filteredValue,
                                  FieldTrigger<?> filter)
      throws MirrorFieldException, NullUnsupportedException
  {
    return getFieldFilter(fieldKey.toString()).setRecordToFilteredValue(this,
                                                                        filteredValue,
                                                                        filter);
  }

  /**
   * Set a field that you are confident is not going to throw a MirrorFieldException and where you
   * don't want to handle the MirrorFieldException. I'm not convinced that this is a good idea, but
   * I'll need to think more on this before removing the method.
   * 
   * @see #setField(Object, Object)
   * @throws LogicException
   *           if the setField operation generated a MirrorFieldException. The initial exception
   *           will be the cause of the LogicException.
   */
  public final Object setSafeField(Object fieldKey,
                                   Object value)
  {
    String fieldName = fieldKey.toString();
    try {
      return setField(fieldName, value);
    } catch (MirrorFieldException mfEx) {
      throw new LogicException(this + ".setSafeField(" + fieldName + "," + value + "):" + mfEx,
                               mfEx);
    }
  }

  /**
   * Get the Table that this record is associated with.
   * 
   * @return The Table!
   */
  @Override
  public final Table getTable()
  {
    return __table;
  }

  @Override
  public final String getTableName()
  {
    return getTable().getName();
  }

  /**
   * Internal method to directly set the contents of the internal value map. This should not be
   * called by client software in any situation (if at all possible).
   * 
   * @param objectValues
   *          The new map that is going to be used as the internal value map for this record.
   */
  protected final void setFields(Object[] objectValues,
                                 int[] intValues)
  {
    Preconditions.checkNotNull(objectValues, "TransientRecord.setFields()");
    _objectValues = objectValues;
    _intValues = intValues;
  }

  /**
   * Internal method to get a reference to the values Map from this record. Any changes to this Map
   * will be reflected in the record, but without the validation offered by the correct methods.
   * This should therefore be avoided.
   * 
   * @return the internal value HashMap.
   */
  protected final Object[] getObjectValues()
  {
    return _objectValues;
  }

  protected final int[] getIntValues()
  {
    return _intValues;
  }

  public int getIntValue(int index)
  {
    try {
      return _intValues[index];
    } catch (RuntimeException e) {
      System.err.println(this + ".getIntValue(" + index + ") on " + Arrays.toString(_intValues)
                         + " with " + getTable().getIntValueIndex() + " --> " + e);
      throw e;
    }
  }

  /**
   * @param linkName
   *          The name of the operation that may represents the link that we are validating. Common
   *          examples are the name of a LinkField (eg target_id) or the name of the operation to
   *          follow a LinkField (eg target)
   */
  public final boolean mayFollowLink(String linkName,
                                     Viewpoint viewpoint)
  {
    viewpoint = defaultIfNull(viewpoint);
    RecordOperation<?> recordOperation = getRecordOperation(linkName);
    if (recordOperation instanceof RecordResolver) {
      return ((RecordResolver) recordOperation).mayResolveRecord(this, viewpoint);
    } else {
      throw new LogicException(this + ".mayFollowLink(" + linkName + ") is not a RecordResolver: "
                               + recordOperation);
    }
  }

  /**
   * @param key
   *          The name of the operation that may represents the link that we are validating. Common
   *          examples are the name of a LinkField (eg target_id) or the name of the operation to
   *          follow a LinkField (eg target)
   */
  public final boolean mayFollowLink(RecordOperationKey<?> key)
  {
    return mayFollowLink(key.getKeyName(), null);
  }

  /**
   * Get the Transaction that currently has a lock on this Record.
   * 
   * @return null because TransientRecords cannot have a Transactional lock by definition.
   */
  protected Transaction getTransaction()
  {
    return null;
  }

  /**
   * If the supplied viewpoint is null then utilise the default viewpoint for this record.
   * 
   * @param viewpoint
   *          The optional viewpoint
   * @return The optional viewpoint or null if the viewpoint is not defined.
   */
  protected Viewpoint defaultIfNull(Viewpoint viewpoint)
  {
    return viewpoint;
  }

  /**
   * Get the result of applying the named RecordOperation on this record.
   * 
   * @param key
   *          The object that resolves to the name of the required RecordOperation via
   *          <code>.toString()</code>.
   * @return The result of calling <code>getOperation(this)</code> on the targetted RecordOperation.
   */
  @Override
  public Object get(Object key)
  {
    return get(key, null);
  }

  @Override
  public final Collection<String> getPublicKeys()
  {
    return __table.getRecordOperations().keySet();
  }

  /**
   * Get the cached value that is stored under the recordOperationName.
   * 
   * @return The cached value or null if there is either no cache or if there is not a value cached
   *         for the given key.
   */
  public Object getCachedValue(Object key)
  {
    Map<Object, Object> cachedValues = _cachedValues;
    if (cachedValues != null) {
      Object value = null;
      value = cachedValues.get(key);
      if (value instanceof Reference<?>) {
        value = ((Reference<?>) value).get();
        if (value == null) {
          cachedValues.remove(key);
        }
      }
      return value;
    }
    return null;
  }

  /**
   * Get an unmodifiable Map view of the cached values on this record.
   */
  public Map<Object, Object> getCachedValues()
  {
    return _cachedValues == null ? null : Collections.unmodifiableMap(_cachedValues);
  }

  /**
   * Inject a value into the value cache.
   * 
   * @return the previous cached value associated with cacheKey, or null if there was no value
   *         stored
   */
  public final Object setCachedValue(Object cacheKey,
                                     Object value)
  {
    Preconditions.checkNotNull(cacheKey, "key");
    Preconditions.checkNotNull(value, "value");
    Map<Object, Object> cachedValues = createCachedValues();
    return cachedValues.put(cacheKey, value);
  }

  /**
   * Remove a cached value from the cached data store. If no cached data store has been created or
   * if a value has yet to be assigned then no operation is performed.
   * 
   * @return The value that was removed or null if no change was made.
   */
  public final Object removeCachedValue(Object cacheKey)
  {
    Map<Object, Object> cachedValues = _cachedValues;
    if (cachedValues != null) {
      return cachedValues.remove(cacheKey);
    }
    return null;
  }

  /**
   * @return The RecordOperation or null if it can't be resolved.
   */
  public final <T> RecordOperation<T> getRecordOperation(final RecordOperationKey<T> key)
  {
    return __table.getRecordOperation(key);
  }

  public final RecordOperation<?> getRecordOperation(final String name)
  {
    return __table.getRecordOperation(name);
  }

  public final RecordOperation<?> getTablePrefixedRecordOperation(final Object key)
  {
    if (key instanceof String) {
      String skey = (String) key;
      String tablePrefix = getTable().getName();
      if (skey.startsWith(tablePrefix) && skey.length() > tablePrefix.length()
          && skey.charAt(tablePrefix.length()) == '.') {
        return __table.getRecordOperation(skey.substring(tablePrefix.length() + 1));
      }
    }
    return null;
  }

  @Override
  public final Object get(Object key,
                          Viewpoint viewpoint)
  {
    String keyName = key.toString();
    viewpoint = defaultIfNull(viewpoint);
    RecordOperation<?> recordOpp = getRecordOperation(keyName);
    if (recordOpp == null) {
      recordOpp = getTablePrefixedRecordOperation(keyName);
    }
    if (recordOpp != null) {
      return recordOpp.getOperation(this, viewpoint);
    }
    int dotIndex = keyName.indexOf('.');
    if (dotIndex != -1) {
      recordOpp = getRecordOperation(keyName.substring(0, dotIndex));
      if (recordOpp == null) {
        return null;
      }
      Object result = recordOpp.getOperation(this, viewpoint);
      if (result == null) {
        return null;
      }
      String postdot = keyName.substring(dotIndex + 1);
      if (result instanceof TransactionGettable) {
        return ((TransactionGettable) result).get(postdot, viewpoint);
      } else if (result instanceof Gettable) {
        return ((Gettable) result).get(postdot);
      }
      return result;
    }
    return null;
  }

  public <V> V opt(RecordOperationKey<V> key)
  {
    return opt(key, null);
  }

  public <A, V> V opt(MonoRecordOperationKey<A, V> key,
                      Viewpoint viewpoint,
                      A a)
  {
    return __table.getRecordOperation(key).calculate(this, viewpoint, a);
  }

  public <A, V> V comp(MonoRecordOperationKey<A, V> key,
                       Viewpoint viewpoint,
                       A a)
      throws UndefinedCompException
  {
    return comp(key, null, a);
  }

  public <A, V> V opt(MonoRecordOperationKey<A, V> key,
                      A a)
  {
    return __table.getRecordOperation(key).calculate(this, null, a);
  }

  public <A, B, V> V opt(DuoRecordOperationKey<A, B, V> key,
                         A a,
                         B b)
  {
    return opt(key, null, a, b);
  }

  public <A, B, V> V opt(DuoRecordOperationKey<A, B, V> key,
                         Viewpoint viewpoint,
                         A a,
                         B b)
  {
    return __table.getRecordOperation(key).calculate(this, viewpoint, a, b);
  }

  public <A, B, C, V> V opt(TrioRecordOperationKey<A, B, C, V> key,
                            Viewpoint viewpoint,
                            A a,
                            B b,
                            C c)
  {
    return __table.getRecordOperation(key).calculate(this, viewpoint, a, b, c);
  }

  public <A, B, C, V> V opt(TrioRecordOperationKey<A, B, C, V> key,
                            A a,
                            B b,
                            C c)
  {
    return __table.getRecordOperation(key).calculate(this, null, a, b, c);
  }

  public <A, B, C, V> V comp(TrioRecordOperationKey<A, B, C, V> key,
                             Viewpoint viewpoint,
                             A a,
                             B b,
                             C c)
      throws UndefinedCompException
  {
    V result = opt(key, viewpoint, a, b, c);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(this, key, getRecordOperation(key), null, a, b, c);
    }
    return result;
  }

  public <A, B, C, V> V comp(TrioRecordOperationKey<A, B, C, V> key,
                             A a,
                             B b,
                             C c)
      throws UndefinedCompException
  {
    V result = opt(key, null, a, b, c);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(this, key, getRecordOperation(key), null, a, b, c);
    }
    return result;
  }

  /**
   * @throws UndefinedCompException
   *           if the DuoRecordOperation returned null
   */
  public <A, B, V> V comp(DuoRecordOperationKey<A, B, V> key,
                          Viewpoint viewpoint,
                          A a,
                          B b)
      throws UndefinedCompException
  {
    V result = opt(key, viewpoint, a, b);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(this, key, getRecordOperation(key), null, a, b);
    }
    return result;
  }

  /**
   * @throws UndefinedCompException
   *           if the DuoRecordOperation returned null
   */
  public <A, B, V> V comp(DuoRecordOperationKey<A, B, V> key,
                          A a,
                          B b)
      throws UndefinedCompException
  {
    return comp(key, null, a, b);
  }

  /**
   * @throws UndefinedCompException
   *           if the MonoRecordOperation returned null
   */
  public <A, V> V comp(MonoRecordOperationKey<A, V> key,
                       A a)
      throws UndefinedCompException
  {
    V result = opt(key, a);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(this, key, getRecordOperation(key), null, a);
    }
    return result;
  }

  public <V> V comp(RecordOperationKey<V> key)
      throws UndefinedCompException
  {
    return comp(key, null);
  }

  /**
   * @deprecated in preference of {@link #compInt(IntFieldKey)}
   */
  @Deprecated
  public Integer comp(IntFieldKey intFieldKey)
  {
    return Integer.valueOf(compInt(intFieldKey));
  }

  public <V> V opt(RecordOperationKey<V> key,
                   Viewpoint viewpoint)
  {
    return key.call(this, viewpoint);
  }

  /**
   * @throws UndefinedCompException
   *           if it was not possible to resolve the ReocrdOperation into a defined value.
   */
  public <V> V comp(RecordOperationKey<V> key,
                    Viewpoint viewpoint)
      throws UndefinedCompException
  {
    V result = opt(key, viewpoint);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(this, key, getRecordOperation(key), null);
    }
    return result;
  }

  /**
   * Resolve a RecordOperation to a non null value, and if that is not possible then throw an
   * {@link UndefinedCompException} with the specified error message.
   */
  public <V> V comp(RecordOperationKey<V> key,
                    Viewpoint viewpoint,
                    String errorMessage,
                    Object... params)
      throws UndefinedCompException
  {
    V result = opt(key, viewpoint);
    if (Db.ENABLE_COMP & result == null) {
      throw new UndefinedCompException(String.format(errorMessage, params),
                                       this,
                                       key,
                                       getRecordOperation(key),
                                       null);
    }
    return result;
  }

  /**
   * Resolve a RecordOperation to a non null value, and if that is not possible then throw an
   * {@link UndefinedCompException} with the specified error message.
   */
  public <V> V comp(RecordOperationKey<V> key,
                    String errorMessage,
                    Object... params)
      throws UndefinedCompException
  {
    return comp(key, null, errorMessage, params);
  }

  public boolean hasRecordOperation(RecordOperationKey<?> key)
  {
    return containsKey(key.getKeyName());
  }

  /**
   * Delegate to hasRecordOperation
   * 
   * @see #hasRecordOperation(Object)
   */
  @Override
  public boolean containsKey(Object key)
  {
    return hasRecordOperation(key);
  }

  /**
   * Check to see whether this record has given named RecordOperation
   */
  public final boolean hasRecordOperation(Object key)
  {
    return __table.hasRecordOperation(key);
  }

  protected void populateInsert(PreparedStatement preparedStatement)
      throws SQLException
  {
    Table table = getTable();
    final int fieldCount = table.getFieldCount();
    preparedStatement.setInt(1, getPendingId());
    for (int i = 1; i < fieldCount; i++) {
      table.getField(i).populateInsert(this, preparedStatement);
    }
  }

  /**
   * Attempt to generate a PersistentRecord out of this TransientRecord. The sequence of steps
   * followed are thus:-
   * <ul>
   * <li>Trigger any instantiation triggers on the table for this record. This will have the effect
   * of throwing a MirrorInstantiationException if the record is rejected by one of the triggers.
   * <li>Commit this TransientRecord to the db.
   * <li>Extract the id number assigned by the db to the new db record.
   * <li>Ask the Table to retrieve the specifed PersistentRecord
   * </ul>
   * Points to note:-
   * <ul>
   * <li>The state of the TransientRecord is not affected by this operation. This means that you can
   * call <code>makePersistent()</code> on the same <code>TransientRecord</code> multiple times
   * (with updates to the values in between if required). However, this will create a <i>new</i>
   * persistent Record on each invocation.
   * <li>The new <code>PersistentRecord</code> will automatically be cached in the Table (if Record
   * caching is enabled).
   * </ul>
   * 
   * @param params
   *          Optional Gettable that contains information that will preinitialise fields in this
   *          record. If you don't pass a Gettable or if the Gettable doesn't contain any relevant
   *          data then all field values will contain default values.
   * @throws MirrorInstantiationException
   *           if there is either an SQLException generated during the actual commit to the db or if
   *           the instantiation triggers fail in some way. Either way, there will not be a db
   *           object created to reflect this record.
   */
  public PersistentRecord makePersistent(Gettable params)
      throws MirrorInstantiationException
  {
    return getTable().makePersistent(this, params, true);
  }

  public synchronized PersistentRecord makePersistent(Gettable params,
                                                      PreparedStatement preparedStatement,
                                                      boolean shouldCacheRecord)
      throws MirrorInstantiationException
  {
    return getTable().makePersistent(this, params, preparedStatement, shouldCacheRecord);
  }

  public PersistentRecord makePersistent(Gettable params,
                                         PreparedStatement preparedStatement)
      throws MirrorInstantiationException
  {
    return makePersistent(params, preparedStatement, true);
  }

  public String debugFields()
  {
    StringBuilder result = new StringBuilder();
    result.append(this).append('\n');
    for (Field<?> field : getTable().getFields()) {
      result.append(" - ")
            .append(field.getName())
            .append('=')
            .append(field.getFieldValue(this))
            .append('\n');
    }
    return result.toString();
  }

  public String debug()
  {
    StringBuilder result = new StringBuilder();
    result.append(this).append('\n');
    try {
      Iterator<RecordOperation<?>> recordOperations =
          new SortedIterator<RecordOperation<?>>(getTable().getRecordOperations()
                                                           .values()
                                                           .iterator(),
                                                 NamedComparator.getInstance());
      while (recordOperations.hasNext()) {
        RecordOperation<?> recordOperation = recordOperations.next();
        result.append(" - ").append(recordOperation.getName()).append('=');
        try {
          result.append(this.get(recordOperation.getName()));
        } catch (RuntimeException rEx) {
          result.append(ExceptionUtil.printStackTrace(rEx));
        }
        result.append('\n');
      }
      result.append(" - Transaction=").append(getTransaction()).append('\n');
      if (_cachedValues != null) {
        result.append(" - Cached Values:-\n");
        for (Object key : _cachedValues.keySet()) {
          result.append("   - ")
                .append(key)
                .append('=')
                .append(_cachedValues.get(key))
                .append('\n');
        }
      }
      return result.toString();
    } catch (Exception e) {
      e.printStackTrace();
      return e.toString();
    }
  }

  /**
   * Get the value of the appropriate title field on this record. This will resolve the result of
   * invoking the Table's getTitleOperationName() against this record and then convert the result to
   * a String.
   * 
   * @see Table#getTitleOperationName()
   */
  public final String getTitle()
  {
    return StringUtils.toString(get(getTable().getTitleOperationName()));
  }

  public final void appendFormElement(String namePrefix,
                                      String name,
                                      Appendable buf,
                                      Map<Object, Object> context)
      throws IOException
  {
    RecordOperation<?> obj = this.getRecordOperation(name);
    if (obj instanceof HtmlGuiSupplier) {
      HtmlGuiSupplier gui = (HtmlGuiSupplier) obj;
      gui.appendFormElement(this, getTransaction(), namePrefix, buf, context);
    } else {
      HtmlUtils.openLabelWidget(buf, name, false, namePrefix, name);
      HtmlUtils.value(buf, this.get(name), namePrefix, name);
      HtmlUtilsTheme.closeDiv(buf);
    }
  }

  public final void appendFormValue(String namePrefix,
                                    String name,
                                    Appendable buf,
                                    Map<Object, Object> context)
      throws IOException
  {
    RecordOperation<?> obj = this.getRecordOperation(name);
    if (obj instanceof HtmlGuiSupplier) {
      HtmlGuiSupplier gui = (HtmlGuiSupplier) obj;
      HtmlUtils.openLabelWidget(buf,
                                gui.getHtmlLabel(),
                                HtmlUtilsTheme.CSS_W_VALUE,
                                false,
                                namePrefix,
                                name);
      gui.appendHtmlValue(this, getTransaction(), namePrefix, buf, context);
      HtmlUtilsTheme.closeDiv(buf);
    } else {
      HtmlUtils.openLabelWidget(buf, name, HtmlUtilsTheme.CSS_W_VALUE, false, namePrefix, name);
      HtmlUtils.value(buf, this.get(name), namePrefix, name);
      HtmlUtilsTheme.closeDiv(buf);
    }
  }

  /**
   * Wrapper around the setField method to satisfy the Settable interface and which wraps up nested
   * MirrorFieldExceptions in SettableExceptions.
   * 
   * @see #setField(Object, Object)
   */
  @Override
  public final Object set(String key,
                          Object value)
      throws SettableException
  {
    try {
      Object oldVal = get(key);
      setField(StringUtils.toString(key), value);
      return oldVal;
    } catch (MirrorFieldException e) {
      throw new SettableException("Error setting " + this + "." + key + " to " + value,
                                  this,
                                  key,
                                  value,
                                  e);
    }
  }

  @Override
  public final Object set(TypedKey<?> key,
                          Object value)
      throws SettableException
  {
    return set(key.getKeyName(), value);
  }

  /**
   * @see Table#getFieldNames()
   */
  @Override
  public final Collection<String> getSettableKeys()
  {
    return getTable().getFieldNames();
  }

  /**
   * @return the ValueType as defined by the Field (which could conceivably be null) or null if
   *         there is a problem defining the Field.
   * @see Field#getValueType()
   */
  @Override
  public final ValueType getValueType(String key)
  {
    Field<?> field;
    try {
      field = getTable().getField(StringUtils.toString(key));
    } catch (RuntimeException rEx) {
      return null;
    }
    return field.getValueType();
  }

  /**
   * Create a new WritableJSONObject that contains a copy of the current state of this record.
   * Please note that changes to this representation are not reflected back into the record.
   */
  public WritableJSONObject asJSON()
      throws JSONException
  {
    WritableJSONObject jObj = new WritableJSONObject();
    for (Field<?> field : getTable().getFields()) {
      field.putJSON(jObj, this);
    }
    return jObj;
  }

  /**
   * Attempt to set the values of this record to the data contained within the JSONObject.
   */
  public void setFields(JSONObject jObj)
      throws MirrorFieldException
  {
    for (Iterator<String> i = jObj.keys(); i.hasNext();) {
      String name = i.next();
      Object value = jObj.opt(name);
      // TODO: jObj.opt(name) can't return Null so check is redundant?
      if (Null.getInstance().equals(value)) {
        setField(name, (Object) null);
      } else {
        setField(name, value);
      }
    }
  }

  /**
   * Get the Transaction Type index of the current lock of this record.
   * 
   * @return {@link Transaction#TRANSACTION_INSERT}
   */
  public int getTransactionType()
  {
    return Transaction.TRANSACTION_INSERT;
  }

  /**
   * Compatability method with WritableRecord.getPersistentRecord() that throws an
   * IllegalStateException if invoked because it is not permitted.
   */
  public PersistentRecord getPersistentRecord()
  {
    throw new IllegalStateException(this + " has yet to be committed to the database.");
  }

}