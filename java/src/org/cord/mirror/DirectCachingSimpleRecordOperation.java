package org.cord.mirror;

import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * Base class for RecordOperations that cache the same value that is returned to the client when the
 * RecordOperation is invoked.
 */
public abstract class DirectCachingSimpleRecordOperation<V>
  extends CachingSimpleRecordOperation<V>
{
  private final Object __cacheKey;

  public DirectCachingSimpleRecordOperation(RecordOperationKey<V> name,
                                            Object cacheKey)
  {
    super(name);
    __cacheKey = Preconditions.checkNotNull(cacheKey, "cacheKey");
  }

  public DirectCachingSimpleRecordOperation(RecordOperationKey<V> name)
  {
    this(name,
         name);
  }

  /**
   * Check to see whether or not there is already a cached value and return that otherwise invoke
   * {@link #calculate(TransientRecord, Viewpoint)} and cache the result before returning it.
   */
  @SuppressWarnings("unchecked")
  @Override
  protected V getOperation(TransientRecord record,
                           Viewpoint viewpoint,
                           Map<Object, Object> cache)
  {
    if (viewpoint != null) {
      return calculate(record, viewpoint);
    }
    V value = (V) cache.get(__cacheKey);
    if (value == null) {
      value = calculate(record, viewpoint);
      cache.put(__cacheKey, value);
    }
    return value;
  }

  /**
   * calculate the result of invoking this RecordOperation because there isn't a cached value.
   */
  public abstract V calculate(TransientRecord record,
                              Viewpoint viewpoint);

}
