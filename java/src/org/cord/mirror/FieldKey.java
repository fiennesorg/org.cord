package org.cord.mirror;

public abstract class FieldKey<V>
  extends RecordOperationKey<V>
{
  // /**
  // * If keyName is defined then create a FieldKey otherwise return null
  // *
  // * @param keyName
  // * The optional keyName
  // * @param valueClass
  // * The compulsory value class
  // * @return the appropriate FieldKey if keyName was defined.
  // */
  // public static <V> FieldKey<V> opt(String keyName,
  // Class<V> valueClass)
  // {
  // if (keyName == null) {
  // return null;
  // }
  // return create(keyName, valueClass);
  // }

  private static final int FIELDINDEX_INIT = -1;

  private Table _table = null;
  private int _fieldIndex = FIELDINDEX_INIT;

  public FieldKey(String keyName,
                  Class<V> valueClass)
  {
    super(keyName,
          valueClass);
  }

  protected void register(Field<V> field)
  {
    int fieldIndex = field.getFieldIndex();
    if (this._fieldIndex != FIELDINDEX_INIT && !(this._fieldIndex == fieldIndex)) {
      throw new IllegalStateException(String.format("%s.register(%s) has already been registered as %s.%s",
                                                    field.getTable(),
                                                    field,
                                                    _table.getName(),
                                                    Integer.toString(this._fieldIndex)));
    }
    _fieldIndex = fieldIndex;
  }

  /**
   * Create a new FieldKey that has the same name and class as this FieldKey. It will not be bound
   * to any table yet and will have no ongoing relationship with this FieldKey.
   */
  @Override
  public abstract FieldKey<V> copy();
}
