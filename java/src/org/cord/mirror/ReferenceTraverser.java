package org.cord.mirror;

import java.util.ArrayList;
import java.util.Iterator;

public class ReferenceTraverser
  implements Iterable<Object>
{
  private ArrayList<Object> _contents = new ArrayList<Object>();

  public ReferenceTraverser(TransientRecord startingRecord,
                            String key)
  {
    Object value = startingRecord.get(key);
    if (value instanceof RecordSource) {
      RecordSource recordSource = (RecordSource) value;
      for (PersistentRecord record : recordSource) {
        _contents.add(record);
        _contents.add(new ReferenceTraverser(record, key));
      }
    } else {
      _contents.add(value);
    }
    // if (operationResult instanceof QueryWrapper) {
    // Query query = ((QueryWrapper) operationResult).getQuery();
    // Iterator<PersistentRecord> recordList =
    // query.getRecordList().iterator();
    // while (recordList.hasNext()) {
    // PersistentRecord nextRecord = recordList.next();
    // _contents.add(nextRecord);
    // _contents.add(new ReferenceTraverser(nextRecord, operationName));
    // }
    // } else {
    // _contents.add(operationResult);
    // }
  }

  @Override
  public Iterator<Object> iterator()
  {
    return _contents.iterator();
  }
}
