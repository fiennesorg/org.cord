package org.cord.mirror;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.NodeRequest;
import org.cord.util.EnglishNamed;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;
import org.cord.util.StringValueType;
import org.cord.util.ValueType;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.Null;
import org.json.WritableJSONObject;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;

/**
 * The abstract base class of all Objects that represent a field in an SQL table.
 * <p>
 * A Field should not contain data that is {@link Viewpoint} sensitive, ie the value on a record
 * should be constant until the record itself is edited and then the value might be updated on the
 * {@link WritableRecord}. If you want to have {@link Viewpoint} sensitive data then you should
 * store the immutable part of the value in the {@link Field}, and then create a
 * {@link RecordOperation} that resolves this into the {@link Viewpoint} sensitive component. For
 * example a Field that refers to another record (eg {@link LinkField} would store the immutable
 * Integer id of the record, and then there would be a {@link RecordOperation} that resolves this
 * into a {@link PersistentRecord} or {@link WritableRecord} depending on the perspective that it is
 * invoked with.
 * </p>
 */
public abstract class Field<T>
  extends SimpleRecordOperation<T>
  implements EnglishNamed, HtmlGuiSupplier
{
  private final String __label;

  private int _fieldIndex;
  private int _sqlFieldIndex;

  private boolean _mayHaveHtmlWidget = true;

  private final boolean __supportsNull;

  private FieldValueSupplier<? extends T> _newRecordValue;

  /**
   * Get the value T that will be stored in the Field when a new Record is created.
   */
  public abstract void storeNewRecordValue(Object[] objectValues,
                                           int[] intValues);

  public T getNewRecordValue()
  {
    return _newRecordValue.supplyValue(null);
  }

  private FieldValueSupplier<? extends T> _defaultDefinedValue;

  /**
   * Get the value T the will be stored in the Field when it is transformed from being NULL to
   * not-NULL. If the Field does not support NULL values then this should be the same value as
   * getNewRecordValue
   */
  public final T getDefaultDefinedValue(TransientRecord record)
  {
    return _defaultDefinedValue.supplyValue(record);
  }

  public final FieldValueSupplier<? extends T> getDefaultDefinedValueSupplier()
  {
    return _defaultDefinedValue;
  }

  public final boolean getSupportsNull()
  {
    return __supportsNull;
  }

  protected T setRecordToFilteredValue(TransientRecord record,
                                       Object filteredValue,
                                       FieldTrigger<?> filter)
      throws NullUnsupportedException, MirrorFieldException
  {
    T validFilteredValue = this.validateOptionalValue(record, filteredValue);
    setRecordToValueStore(record, validFilteredValue);
    return validFilteredValue;
  }

  /**
   * Constant representing the position of a FieldFilter that has not yet been register with a value
   * of -1.
   * 
   * @see #getFieldIndex()
   * @see #register(Table,int)
   */
  public final static int NOT_INITIALISED = -1;

  private final Class<T> __valueClass;

  /**
   * Create a generic FieldFilter in the NOT_INITIALISED state.
   */
  protected Field(FieldKey<T> fieldKey,
                  String label,
                  boolean supportsNull,
                  Class<T> valueClass,
                  FieldValueSupplier<? extends T> newRecordValue,
                  FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    super(fieldKey);
    __label = Strings.isNullOrEmpty(label) ? fieldKey.getKeyName() : label;
    _fieldIndex = NOT_INITIALISED;
    if (!supportsNull && (newRecordValue == null || defaultDefinedValue == null)) {
      System.err.println(String.format("%s initialised with defaults of (%s, %s) and supportsNull as false",
                                       this,
                                       newRecordValue,
                                       defaultDefinedValue));
    }
    if (newRecordValue == null) {
      newRecordValue = FieldValueSuppliers.nullSupplier();
    }
    __valueClass = fieldKey.getValueClass();
    __supportsNull = supportsNull;
    _newRecordValue = newRecordValue;
    _defaultDefinedValue = defaultDefinedValue;
  }

  /**
   * Create a new Field that supports NULL values
   * 
   * @param newRecordValue
   *          The value that a field will take when the record is initially created. May be null
   * @param defaultDefinedValue
   *          The value that a field will assume if it is switched from being NULL to being
   *          not-NULL. This must not be null.
   */
  public Field(FieldKey<T> fieldKey,
               String label,
               Class<T> valueClass,
               FieldValueSupplier<? extends T> newRecordValue,
               FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    this(fieldKey,
         label,
         true,
         valueClass,
         newRecordValue,
         defaultDefinedValue);
  }

  /**
   * Create a new Field the doesn't support NULL values
   * 
   * @param defaultDefinedValue
   *          The value that a field will take when the record is initially created. Must not be
   *          null.
   */
  public Field(FieldKey<T> fieldKey,
               String label,
               Class<T> valueClass,
               FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    this(fieldKey,
         label,
         false,
         valueClass,
         defaultDefinedValue,
         defaultDefinedValue);
  }

  public Field<T> setMayHaveHtmlWidget(boolean mayHaveHtmlWidget)
  {
    blockOnState(LO_NEW, "Must set HTML widget status before locking");
    _mayHaveHtmlWidget = mayHaveHtmlWidget;
    return this;
  }

  public final void validateSqlDefinition(String table,
                                          String sqlName,
                                          String sqlDef,
                                          boolean permitsNulls)
      throws SQLException
  {
    if (!sqlName.equals(getName())) {
      throw new SQLException(table + ": mirror:" + getName() + " != sql:" + sqlName);
    }
    if (permitsNulls != getSupportsNull()) {
      String error = String.format("%s.%s.permitsNulls: SQL:%s != mirror:%s",
                                   table,
                                   sqlName,
                                   Boolean.toString(permitsNulls),
                                   Boolean.toString(getSupportsNull()));
      if (getSupportsNull()) {
        throw new SQLException(error);
      }
      System.err.println(error);
    }
    validateSqlDefinition(sqlDef);
  }

  /**
   * Check to see if the given SQL definition of the field matches up with the requirements of the
   * implementation. This should be overriden by implementations. The default behaviour is to print
   * out a string to the debug output detailing which field is failing to validate the data so that
   * it is easy to perform the implementation. This will get invoked by
   * {@link #validateSqlDefinition(String, String, String, boolean)} once the name and optional
   * status of the field has been validated.
   */
  protected abstract void validateSqlDefinition(String sqlDef)
      throws SQLException;

  @Override
  public final String getHtmlLabel()
  {
    return __label;
  }

  @Override
  public final String getEnglishName()
  {
    return getHtmlLabel();
  }

  @Override
  public FieldKey<T> getKey()
  {
    return (FieldKey<T>) super.getKey();
  }

  /**
   * Inform this Field that it has been registered on a given Table with a given fieldId. This is
   * invoked once during the Table.addField operation.
   * 
   * @param table
   * @param fieldId
   * @throws MirrorTableLockedException
   * @see Table#addField(Field)
   */
  protected synchronized void register(Table table,
                                       int fieldId)
      throws MirrorTableLockedException
  {
    if (_fieldIndex != NOT_INITIALISED) {
      throw new IllegalStateException("Cannot call setFieldId() twice on " + this);
    }
    _fieldIndex = fieldId;
    _sqlFieldIndex = _fieldIndex + 1;
    _setField_exceptionForInvalidNull =
        MoreObjects.firstNonNull(table.getDb()
                                      .getConfiguration()
                                      .getBoolean(Db.CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL),
                                 Boolean.FALSE)
                   .booleanValue();
    _setField_exceptionForTypeConversion =
        MoreObjects.firstNonNull(table.getDb()
                                      .getConfiguration()
                                      .getBoolean(Db.CONFIG_FIELD_SETFIELD_EXCEPTIONFORTYPECONVERSION),
                                 Boolean.FALSE)
                   .booleanValue();
  }

  private boolean _setField_exceptionForInvalidNull;

  private boolean _setField_exceptionForTypeConversion;

  public static final String CSS_W_ISDEFINED = "w_isDefined";

  protected final boolean supportsExceptionForTypeConversion()
  {
    return _setField_exceptionForTypeConversion;
  }

  /**
   * Get the index of this Field in the Table that it is currently registered on. This can be used
   * to get the value of the Field via the TransientRecord.getField(fieldIndex) method.
   */
  public final int getFieldIndex()
  {
    return _fieldIndex;
  }

  /**
   * Get the index of this Field which should be utilised when communicating with SQL operations.
   * This is 1 higher than {@link #getFieldIndex()} because SQL numbering starts at 1 rather than 0.
   */
  public final int getSqlFieldIndex()
  {
    return _sqlFieldIndex;
  }

  public final void appendSqlValue(StringBuilder buffer,
                                   TransientRecord targetRecord)
  {
    appendSqlValue(buffer, getFieldValue(targetRecord));
  }

  /**
   * Request the implementation of FieldFilter to append the SQL representation of a given value to
   * the StringBuilder. Classes should implement this method to provide the necessary translation
   * from the java Object that is used to store the value in the field to the correct String
   * representation to generate an update string for the database. If the value is expected to be
   * identified as a string in the SQL statement, then it is the responsibility of this method to
   * include the bounding single quotes around the String.
   * 
   * @param buffer
   *          The StringBuilder to append the value onto.
   * @param value
   *          The value that is to be converted. Never null.
   */
  public abstract void appendDefinedSqlValue(StringBuilder buffer,
                                             T value);

  /**
   * Serialize the value that this Field has on the given TransientRecord into a JSONObject.
   */
  public void putJSON(WritableJSONObject jObj,
                      TransientRecord record)
      throws JSONException
  {
    jObj.put(getName(), MoreObjects.firstNonNull(getFieldValue(record), Null.getInstance()));
  }

  /**
   * Restore the serialized value of this Field to a given TransientRecord.
   * 
   * @param jObj
   *          The compulsory JSONObject to get the value out of
   * @param record
   *          The compulsory record which must be from the same Table that this Field is registered
   *          on
   * @return true if setField was invoked on record, otherwise false if jObj didn't contain any
   *         information relevant to this Field
   * @throws MirrorFieldException
   *           if the record rejected the value contained in jObj
   * @throws JSONException
   *           if jObj didn't contain data that was compulsory for this Field on the given record.
   *           The base implementation never throws this error, but subclasses might.
   */
  public boolean getJSON(JSONObject jObj,
                         TransientRecord record)
      throws MirrorFieldException, JSONException
  {
    Preconditions.checkNotNull(jObj, "jObj");
    Preconditions.checkState(record != null && record.getTable() == getTable(),
                             "%s is not a valid record for %s",
                             record,
                             this);
    Object obj = jObj.opt(getName());
    if (obj == null) {
      return false;
    }
    record.setField(getKey(), obj instanceof Null ? null : obj);
    return true;
  }

  public final void appendSqlValue(StringBuilder buf,
                                   T value)
  {
    if (value == null) {
      buf.append("null");
    } else {
      appendDefinedSqlValue(buf, value);
    }
  }

  /**
   * Convert the value into a T using objectToValue and then invoke appendSqlValue with the result.
   * 
   * @see #appendSqlValue(StringBuilder, Object)
   * @see #objectToValue(TransientRecord, Object)
   */
  public final void appendSqlObjectValue(StringBuilder buf,
                                         TransientRecord record,
                                         Object value)
  {
    appendSqlValue(buf, objectToValue(record, value));
  }

  /**
   * Convert a generic Object into a T value. The default implementation of this is to just cast the
   * Object to the appropriate target class, but if your Field implementation has other methods of
   * performing the conversion then this method should be overridden appropriately.
   * 
   * @param obj
   *          The optional obj to be converted.
   * @return The T representation of obj or null if obj was not defined.
   * @throws ClassCastException
   *           if it isn't possible to cast obj into a T
   */
  public T objectToValue(TransientRecord record,
                         Object obj)
  {
    return ObjectUtil.castTo(obj, __valueClass);
  }

  /**
   * Request the implementation of FieldFilter to state which Class it uses for internal storage of
   * values. If implementations use multiple Classes (no idea why they should but...) then they
   * should return the common sub-class of the multiple classes.
   * 
   * @return The Class used for internal data storage. This value should be constant and immutable.
   */
  public final Class<T> getValueClass()
  {
    return __valueClass;
  }

  /**
   * Return a default StringValueType as most fields will be able to parse the string version of
   * their data type. There is no validation and the type is not compulsory. Fields which want a
   * more specific GUI behaviour than this should override this method. There is currently no
   * caching on this and the StringValueType is created each time it is invoked.
   * 
   * @return The default StringValueType for this field.
   */
  public ValueType getValueType()
  {
    return new StringValueType(getName(), getHtmlLabel(), null, null, _defaultDefinedValue, false);
  }

  /**
   * Get the value that this FieldFilter has on the given TransientRecord.
   */
  public abstract T getFieldValue(TransientRecord targetRecord);

  public abstract boolean isEqualValues(TransientRecord record1,
                                        TransientRecord record2);

  public abstract int compareValues(TransientRecord record1,
                                    TransientRecord record2);

  public abstract int compareValuesIgnoreCase(TransientRecord record1,
                                              TransientRecord record2);

  /**
   * Format the value that this Field holds on the given record according to any appropriate
   * internal rules.
   * 
   * @return The result of calling toString() on the given value. If the value is null then "null"
   *         is returned. subclasses should override this functionality if more bespoke
   *         functionality is required.
   */
  public String toString(TransientRecord targetRecord)
  {
    T value = getFieldValue(targetRecord);
    if (value == null) {
      return "null";
    }
    return value.toString();
  }

  /**
   * @return the result of getFieldValue(targetRecord)
   * @see #getFieldValue(TransientRecord)
   */
  @Override
  public final T getOperation(TransientRecord targetRecord,
                              Viewpoint transaction)
  {
    return getFieldValue(targetRecord);
  }

  /**
   * Convert an abstract Object into a T that this Field uses to represent values. If the value is
   * null then the method will either use the default defined value or it will throw a
   * NullUnsupportedException depending on the state of the
   * CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL value in the Db configuration. Over time, this
   * will move towards always throwing the exception, but choice is available at present to preserve
   * backwards compatability. If the value is defined then it is passed onto validateDefinedValue
   * for further implementation-specific processing.
   * 
   * @param value
   *          The optional value to be validated
   * @return The validated value.
   * @throws NullUnsupportedException
   *           If the Db has been configured to throw the exception and you are trying to set a
   *           field to null that doesn't support null values. If the Db has not been configured to
   *           behave in this manner (ie the legacy behaviour) then this action will cause the
   *           default defined value to be utilised instead.
   * @see Db#CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL
   * @see #validateDefinedValue(TransientRecord, Object)
   * @see #getDefaultDefinedValue(TransientRecord)
   */
  public final T validateOptionalValue(TransientRecord record,
                                       Object value)
      throws MirrorFieldException, NullUnsupportedException
  {
    if (value == null) {
      return validateNullValue(record);
    }
    return validateDefinedValue(record, value);
  }

  protected T validateNullValue(TransientRecord record)
      throws NullUnsupportedException
  {
    if (getSupportsNull()) {
      return null;
    }
    if (_setField_exceptionForInvalidNull) {
      throw new NullUnsupportedException(String.format("%s.%s does not support null values",
                                                       getTable(),
                                                       getName()),
                                         record,
                                         getName());
    }
    return getDefaultDefinedValue(record);
  }

  /**
   * Convert the proposed not null Object value into a T so that it is suitable for processing by
   * this Field.
   * 
   * @throws TypeValidationException
   *           If the value supplied cannot be converted into a T for this Field. Note that this
   *           should only be thrown if the CONFIG_FIELD_SETFIELD_EXCEPTIONFORTYPECONVERSION is set
   *           to true in the Db configuration. If not then the default defined value will be
   *           returned instead. It the exception is thrown the state of the record should not be
   *           changed.
   * @see Db#CONFIG_FIELD_SETFIELD_EXCEPTIONFORTYPECONVERSION
   * @see #supportsExceptionForTypeConversion()
   */
  public abstract T validateDefinedValue(TransientRecord record,
                                         Object value)
      throws MirrorFieldException, TypeValidationException;

  /**
   * Resolve to the appropriate Object or int from the ResultSet and store it in objectValues or
   * intValues as appropriate.
   * 
   * @param objectValues
   *          the array of values that have been resolved from the resultSet up until the point when
   *          this Field is requested. Any Fields with lower id numbers will have been resolved and
   *          the data will be available. You should not change this data.
   * @param intValues
   *          the array of primitive int values that have been resolved from the resultSet up until
   *          the point when this Field is requested. Any fields with lower id numbers will have
   *          been resolved and the data will be available. You should not change this data.
   * @throws MirrorLogicException
   *           if {@link #dbToValue(ResultSet, Object[], int[])} fails to make the conversion. This
   *           will have additional debugging as to what type of Object triggered the failure..
   */
  public abstract void dbToValue(ResultSet resultSet,
                                 Object[] objectValues,
                                 int[] intValues)
      throws SQLException;

  /**
   * Request the FieldFilter to provide an expanded view of the supplied data (if any is defined).
   * The default implementation of this merely returns the value itself. This may be overridden if
   * required by subclasses that encapsulate the idea of a short internal data format and an
   * expanded human readable format. This is utilised by the ExpandField operation.
   * 
   * @param targetRecord
   *          The TransientRecord on which this value is to be expanded.
   * @return The expanded value which is currently the same as getFieldValue(targetRecord). This
   *         will currently be of class T, but this may be overrulled by elements that return
   *         Strings at the discretion of sub-classes.
   * @see #getFieldValue(TransientRecord)
   * @see org.cord.mirror.operation.ExpandField
   */
  public String expand(TransientRecord targetRecord)
  {
    return StringUtils.toString(getFieldValue(targetRecord));
  }

  /**
   * Get an Iterator of EnglishNamed Objects that represent the valid choices that this FieldFilter
   * accepts, assuming that this is a meaningful concept.
   * 
   * @return An EmptyIterator<EnglishNamed> unless overriden
   * @see org.cord.util.EnglishNamed
   */
  public Iterator<? extends EnglishNamed> values()
  {
    return ImmutableSet.<EnglishNamed> of().iterator();
  }

  /**
   * Get the EnglishNamed Objects that represent any proposed values for this FieldFilter. These
   * values are intended to be included ahead of the values() listing in a selection box. However,
   * they should be a subset of the complete values() listing rather than extra values.
   * 
   * @return An EmptyIterator<EnglishNamed> unless overriden
   * @see org.cord.util.EnglishNamed
   */
  public Iterator<? extends EnglishNamed> proposedValues()
  {
    return ImmutableSet.<EnglishNamed> of().iterator();
  }

  /**
   * Get the default EnglishNamed value that represents no choice for this FieldFilter. The
   * getName() on the EnglishNamed should be "" and the getEnglishName() should contain the
   * instruction to the user about what the values() selection entails.
   * 
   * @return The appropriate EnglishNamed object or null if either this FieldFilter doesn't support
   *         values (hasValues() == false) or if this FieldFilter doesn't support null values.
   */
  public EnglishNamed getEmptyValue()
  {
    return null;
  }

  /**
   * Check to see if this FieldFilter supports the concept of multi-valued choices.
   * 
   * @return True if values are a valid concept. If false then all of the values commands will not
   *         return anything particularly useful.
   * @see #getEmptyValue()
   * @see #proposedValues()
   * @see #values()
   */
  public boolean hasValues()
  {
    return false;
  }

  public final boolean mayHaveHtmlWidget()
  {
    return _mayHaveHtmlWidget;
  }

  @Override
  public boolean hasHtmlWidget(TransientRecord record)
  {
    return true;
  }

  /**
   * Convert an optional T into a String that if supplied in a NodeRequest would generate the
   * original value.
   * 
   * @return value.toString() or null if value is null. Override if your implementation of Field
   *         utilises different representations in communication with NodeRequests.
   */
  public String valueToNodeRequest(T value)
  {
    return StringUtils.toString(value);
  }

  /**
   * Append a hidden form element containing the default value of this Field. The name of the hidden
   * field should be the same as the default name for this Field with
   * {@link NodeRequest#DEFAULTDEFINEDVALUE_POSTFIX} appended on the end. This is invoked in
   * situations when an optional Field is currently disabled and sets the value that will be
   * utilised for the Field if the checkbox to re-enable it is clicked.
   * <p>
   * The default implementation is to create a hidden form with a value of
   * {@link #getDefaultDefinedValue(TransientRecord)} passed through
   * {@link #valueToNodeRequest(Object)}.
   */
  public void appendDefaultDefinedWidget(TransientRecord record,
                                         Viewpoint viewpoint,
                                         String namePrefix,
                                         Appendable buf,
                                         Map<Object, Object> context)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(buf,
                               valueToNodeRequest(getDefaultDefinedValue(record)),
                               namePrefix,
                               getName(),
                               NodeRequest.DEFAULTDEFINEDVALUE_POSTFIX);
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, getFieldValue(record), namePrefix, getName());
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.textWidget(null, buf, StringUtils.toString(getFieldValue(record)), namePrefix, getName());
  }

  @Override
  public void appendFormElement(final TransientRecord record,
                                final Viewpoint viewpoint,
                                final String namePrefix,
                                final Appendable buf,
                                final Map<Object, Object> context)
      throws IOException
  {
    final T value = this.getFieldValue(record);
    if (value == null) {
      HtmlUtils.openLabelWidget(buf,
                                getHtmlLabel(),
                                false,
                                NodeRequest.ISDEFINED_PREFIX,
                                namePrefix,
                                getName());
    } else {
      HtmlUtils.openLabelWidget(buf, getHtmlLabel(), false, namePrefix, getName());
    }
    if (mayHaveHtmlWidget() && hasHtmlWidget(record)) {
      if (getSupportsNull()) {
        final boolean isDefined = value != null;
        HtmlUtils.openWidgetDiv(buf, Field.CSS_W_ISDEFINED);
        if (isDefined && !value.equals(getDefaultDefinedValue(record))) {
          HtmlUtils.checkboxInputWidget(buf,
                                        null,
                                        isDefined,
                                        "Are you sure? Disabling a field will lose the value that it contains.",
                                        true,
                                        false,
                                        NodeRequest.ISDEFINED_PREFIX,
                                        namePrefix,
                                        getName());
        } else {
          HtmlUtils.checkboxInputWidget(buf,
                                        null,
                                        isDefined,
                                        true,
                                        false,
                                        NodeRequest.ISDEFINED_PREFIX,
                                        namePrefix,
                                        getName());
        }
        HtmlUtilsTheme.closeDiv(buf);
        if (isDefined) {
          appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
        } else {
          appendDefaultDefinedWidget(record, viewpoint, namePrefix, buf, context);
        }
      } else {
        appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
      }
    } else {
      appendHtmlValue(record, viewpoint, namePrefix, buf, context);
    }
    HtmlUtilsTheme.closeDiv(buf);
  }

  /**
   * @return false;
   */
  @Override
  public boolean shouldReloadOnChange()
  {
    return false;
  }

  /**
   * Extract the value from the supplied Gettable utilising any appropriate methods of encoding the
   * value required. The default implementation just calls get(name) on the Gettable and then passes
   * the resulting Object through objectToValue to perform the necessary transform. You should
   * override this method if you want to use a different Gettable method to extract the value or if
   * the value is stored in multiple components within the Gettable.
   * 
   * @param gettable
   *          The compulsory Gettable to get the value out of
   * @return The value that was in the params or null if there was no contained value.
   * @see Gettable#get(Object)
   * @see #objectToValue(TransientRecord, Object)
   */
  public T getValue(TransientRecord record,
                    Gettable gettable)
  {
    Object obj = gettable.get(getName());
    if (obj == null) {
      return null;
    }
    try {
      return objectToValue(record, obj);
    } catch (RuntimeException e) {
      throw new IllegalArgumentException(String.format("Error while resolving %s.%s from %s as %s (%s) into %s",
                                                       getTable(),
                                                       getName(),
                                                       gettable,
                                                       obj,
                                                       obj.getClass(),
                                                       getValueClass()),
                                         e);
    }
  }

  public Field<T> setDefaultDefinedValue(FieldValueSupplier<T> defaultDefinedValueSupplier)
  {
    blockOnState(LO_NEW, "Cannot set defaultDefinedValueSupplier after registration");
    _defaultDefinedValue =
        Preconditions.checkNotNull(defaultDefinedValueSupplier, "defaultDefinedValueSupplier");
    return this;
  }

  public Field<T> setNewRecordValue(FieldValueSupplier<T> newRecordValueSupplier)
  {
    blockOnState(LO_NEW, "Cannot set newRecordValueSupplier after registration");
    _newRecordValue = Preconditions.checkNotNull(newRecordValueSupplier, "newRecordValueSupplier");
    return this;
  }

  /**
   * Utility method to test an sqlDef against a Predicate and throw a sensibly formatted
   * SQLException if it fails.
   */
  protected void validateSqlDef(Predicate<String> isValid,
                                String sqlDef)
      throws SQLException
  {
    if (!isValid.apply(sqlDef)) {
      throw new SQLException(String.format("%s.%s of class %s is defined as \"%s\" which fails %s",
                                           getTable(),
                                           this,
                                           this.getClass().getName(),
                                           sqlDef,
                                           isValid));
    }
  }

  /**
   * Insert the data represented by this Field into the PreparedStatement to add the Field to the
   * database.
   */
  protected abstract void populateInsert(TransientRecord record,
                                         PreparedStatement preparedStatement)
      throws SQLException;

  /**
   * Get the SQL {@link Types} constant that represents the format of data that this Field will be
   * used for SQL storage. This will be utilised by
   * {@link #populateInsert(TransientRecord, PreparedStatement)} to automatically handle null values
   * when inserting data.
   */
  protected abstract int getSqlType();

  protected final T setRecordToValue(TransientRecord record,
                                     Object value)
      throws NullUnsupportedException, MirrorFieldException
  {
    boolean isUpdated = false;
    final T existingValue = getFieldValue(record);
    T validValue = this.validateOptionalValue(record, value);
    MirrorFieldException mfEx = null;
    do {
      mfEx = null;
      setRecordToValueStore(record, validValue);
      try {
        getTable().triggerFieldTriggers(record, getName());
      } catch (MirrorFieldException caughtMfEx) {
        mfEx = caughtMfEx;
        if (mfEx.getProposedValue() != null && mfEx.getRecord().equals(record)) {
          validValue = this.validateOptionalValue(record, mfEx.getProposedValue());
        } else {
          validValue = null;
        }
      }
    } while (mfEx != null && mfEx.getProposedValue() != null);
    if (validValue == null && !this.getSupportsNull()) {
      setRecordToValueStore(record, existingValue);
      throw new MirrorFieldException(record + ".setField(" + getName() + "," + value + ")",
                                     mfEx,
                                     record,
                                     getName());
    }
    isUpdated = !isEqualValue(record, existingValue);
    getTable().triggerPostFieldTriggers(record, getName(), existingValue, isUpdated);
    if (isUpdated) {
      record.setIsUpdated();
    }
    return validValue;
  }

  protected abstract boolean isEqualValue(TransientRecord record,
                                          T value);

  protected abstract void setRecordToValueStore(TransientRecord record,
                                                T value);

  /**
   * Inform a WritableRecord that its state is updated. This is intended for implementations of
   * Field that change state by mutating the values rather than assigning new values with
   * {@link TransientRecord#setField(Object, Object)} and therefore the record might not realise
   * that a change has occurred. This will invoke {@link TransientRecord#setIsUpdated()} which would
   * otherwise not be visible to Field implementations in other packages.
   * 
   * @param writableRecord
   *          The not-null record to update.
   */
  protected void setIsUpdated(WritableRecord writableRecord)
  {
    writableRecord.setIsUpdated();
  }
}
