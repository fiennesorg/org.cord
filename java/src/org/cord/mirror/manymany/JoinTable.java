package org.cord.mirror.manymany;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.util.Assertions;

import com.google.common.base.Strings;

public class JoinTable
  extends AbstractTableWrapper
{
  public static final ObjectFieldKey<Boolean> ISDEFINED = BooleanField.createKey("isDefined");

  private final String __defaultJoinOrdering;

  private final String __t1Name;

  private final IntFieldKey __t1LinkField;

  private final String __t1LinkFieldLabel;

  private final Integer __t1LinkFieldDefault;

  private final RecordOperationKey<PersistentRecord> __t1LinkName;

  private final RecordOperationKey<RecordSource> __t1JoinReferencesName;

  private final String __t1JoinReferencesOrdering;

  private final int __t1LinkStyle;

  private final String __t2Name;

  private final IntFieldKey __t2LinkField;

  private final String __t2LinkFieldLabel;

  private final Integer __t2LinkFieldDefault;

  private final RecordOperationKey<PersistentRecord> __t2LinkName;

  private final RecordOperationKey<RecordSource> __t2JoinReferencesName;

  private final String __t2JoinReferencesOrdering;

  private final int __t2LinkStyle;

  public JoinTable(String joinTableName,
                   String defaultJoinOrdering,
                   String t1Name,
                   IntFieldKey t1LinkField,
                   String t1LinkFieldLabel,
                   Integer t1LinkFieldDefault,
                   RecordOperationKey<PersistentRecord> t1LinkName,
                   RecordOperationKey<RecordSource> t1JoinReferencesName,
                   String t1JoinReferencesOrdering,
                   int t1LinkStyle,
                   String t2Name,
                   IntFieldKey t2LinkField,
                   String t2LinkFieldLabel,
                   Integer t2LinkFieldDefault,
                   RecordOperationKey<PersistentRecord> t2LinkName,
                   RecordOperationKey<RecordSource> t2JoinReferencesName,
                   String t2JoinReferencesOrdering,
                   int t2LinkStyle)
  {
    super(joinTableName);
    __defaultJoinOrdering = defaultJoinOrdering;
    __t1Name = Assertions.notEmpty(t1Name, "t1Name");
    __t1LinkField = t1LinkField;
    __t1LinkFieldLabel = t1LinkFieldLabel;
    __t1LinkFieldDefault = t1LinkFieldDefault;
    __t1LinkName = t1LinkName;
    __t1JoinReferencesName = t1JoinReferencesName;
    __t1JoinReferencesOrdering = t1JoinReferencesOrdering;
    __t1LinkStyle = t1LinkStyle;
    __t2Name = Assertions.notEmpty(t2Name, "t2Name");
    __t2LinkField = t2LinkField;
    __t2LinkFieldLabel = t2LinkFieldLabel;
    __t2LinkFieldDefault = t2LinkFieldDefault;
    __t2LinkName = t2LinkName;
    __t2JoinReferencesName = t2JoinReferencesName;
    __t2JoinReferencesOrdering = t2JoinReferencesOrdering;
    __t2LinkStyle = t2LinkStyle;
  }

  public JoinTable(String joinTableName,
                   String t1Name,
                   boolean lockOnT1Edit,
                   String t2Name,
                   boolean lockOnT2Edit)
  {
    this(joinTableName,
         null,
         t1Name,
         null,
         null,
         null,
         null,
         null,
         null,
         Dependencies.LINK_ENFORCED | (lockOnT1Edit ? Dependencies.BIT_S_ON_T_UPDATE : 0),
         t2Name,
         null,
         null,
         null,
         null,
         null,
         null,
         Dependencies.LINK_ENFORCED | (lockOnT2Edit ? Dependencies.BIT_S_ON_T_UPDATE : 0));
  }

  @Override
  protected void initTable(Db db,
                           String transactionPassword,
                           Table joinTable)
      throws MirrorTableLockedException
  {
    addJoinTableFields(transactionPassword,
                       joinTable,
                       __defaultJoinOrdering,
                       __t1Name,
                       __t1LinkField,
                       __t1LinkFieldLabel,
                       __t1LinkFieldDefault,
                       __t1LinkName,
                       __t1JoinReferencesName,
                       __t1JoinReferencesOrdering,
                       __t1LinkStyle,
                       __t2Name,
                       __t2LinkField,
                       __t2LinkFieldLabel,
                       __t2LinkFieldDefault,
                       __t2LinkName,
                       __t2JoinReferencesName,
                       __t2JoinReferencesOrdering,
                       __t2LinkStyle);
  }

  public static void addJoinTableFields(String transactionPassword,
                                        Table joinTable,
                                        String defaultJoinOrdering,
                                        String t1Name,
                                        IntFieldKey t1LinkField,
                                        String t1LinkFieldLabel,
                                        Integer t1LinkFieldDefault,
                                        RecordOperationKey<PersistentRecord> t1LinkName,
                                        RecordOperationKey<RecordSource> t1JoinReferencesName,
                                        String t1JoinReferencesOrdering,
                                        int t1LinkStyle,
                                        String t2Name,
                                        IntFieldKey t2LinkField,
                                        String t2LinkFieldLabel,
                                        Integer t2LinkFieldDefault,
                                        RecordOperationKey<PersistentRecord> t2LinkName,
                                        RecordOperationKey<RecordSource> t2JoinReferencesName,
                                        String t2JoinReferencesOrdering,
                                        int t2LinkStyle)
      throws MirrorTableLockedException
  {
    joinTable.addField(new LinkField(t1LinkField,
                                     t1LinkFieldLabel,
                                     t1LinkFieldDefault,
                                     t1LinkName,
                                     t1Name,
                                     t1JoinReferencesName,
                                     t1JoinReferencesOrdering,
                                     transactionPassword,
                                     t1LinkStyle | Dependencies.LINK_ENFORCED));
    joinTable.addField(new LinkField(t2LinkField,
                                     t2LinkFieldLabel,
                                     t2LinkFieldDefault,
                                     t2LinkName,
                                     t2Name,
                                     t2JoinReferencesName,
                                     t2JoinReferencesOrdering,
                                     transactionPassword,
                                     t2LinkStyle | Dependencies.LINK_ENFORCED));
    joinTable.addField(new BooleanField(ISDEFINED, null));
    if (!Strings.isNullOrEmpty(defaultJoinOrdering)) {
      joinTable.setDefaultOrdering(defaultJoinOrdering);
    }
  }
}
