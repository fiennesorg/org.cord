package org.cord.mirror;

public class MirrorRecordNestedException
  extends MirrorNestedException
{
  /**
   * 
   */
  private static final long serialVersionUID = -5235789244043694597L;

  private final PersistentRecord _record;

  public MirrorRecordNestedException(String message,
                                     Exception coreNestedException,
                                     PersistentRecord record)
  {
    super(message,
          coreNestedException);
    _record = record;
  }

  public PersistentRecord getRecord()
  {
    return _record;
  }
}
