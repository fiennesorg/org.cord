package org.cord.mirror;

import java.util.Comparator;

/**
 * Implementation of Comparator that changes the default ordering of TransientRecords to be sorted
 * alphabetically according to the value of a given field on the records.
 * 
 * @deprecated in preference of {@link FieldValueComparator}
 */
@Deprecated
public class TransientRecordStringComparator
  implements Comparator<TransientRecord>
{
  private final FieldKey<String> __fieldName;

  private final boolean __permitsMultipleTables;

  public TransientRecordStringComparator(FieldKey<String> fieldName,
                                         boolean permitsMultipleTables)
  {
    __fieldName = fieldName;
    __permitsMultipleTables = permitsMultipleTables;
  }

  @Override
  public int compare(TransientRecord tr1,
                     TransientRecord tr2)
  {
    if (!__permitsMultipleTables) {
      Table t1 = tr1.getTable();
      Table t2 = tr2.getTable();
      if (!t1.equals(t2)) {
        throw new ClassCastException("Cannot compare two records from different Tables");
      }
    }
    String v1 = tr1.comp(__fieldName);
    String v2 = tr2.comp(__fieldName);
    return v1.compareTo(v2);
  }
}