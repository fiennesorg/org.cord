package org.cord.mirror;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.operation.MonoRecordOperationKey;
import org.webmacro.Macro;

/**
 * Describes an Object that is capable of rendering its state (and optionally a GUI) in an HTML
 * form.
 * 
 * @author alex
 */
public interface HtmlGuiSupplier
{
  public static final MonoRecordOperationKey<String, Macro> ASFORMELEMENT =
      MonoRecordOperationKey.create("asFormElement", String.class, Macro.class);

  public static final MonoRecordOperationKey<String, Macro> ASFORMVALUE =
      MonoRecordOperationKey.create("asFormValue", String.class, Macro.class);

  /**
   * Append the HTML representation of this item's value for the given record onto the
   * StringBuilder.
   * 
   * @param context
   *          The optional source of additional information about the context within which this
   *          rendering operation is going to takes place.
   */
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException;

  /**
   * Append the HTML representation of this item's controller for the given record onto the
   * StrinBuffer. The widget should be wrapped in the appropriate divs as required. In general using
   * the HtmlUtils methods will guarantee consistent results.
   * 
   * @param context
   *          The optional source of additional information about the context within which this
   *          rendering operation is going to takes place.
   * @see org.cord.util.HtmlUtils
   */
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException;

  /**
   * Render this object as either a view or a controller as appropriate onto the given
   * StringBuilder.
   * 
   * @param context
   *          The optional source of additional information about the context within which this
   *          rendering operation is going to takes place.
   */
  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Map<Object, Object> context)
      throws IOException;

  /**
   * Should the current record have a controller or a read view in its current state?
   */
  public boolean hasHtmlWidget(TransientRecord record);

  /**
   * What should the text of the label be for the controller for this object?
   */
  public String getHtmlLabel();

  /**
   * Should changing the value of this HtmlGuiSupplier force the GUI to revalidate itself
   * immediately.
   */
  public boolean shouldReloadOnChange();
}
