package org.cord.mirror;

/**
 * An object that is capable of generating a String out of the state contained within a given Record
 * 
 * @author alex
 */
public interface RecordStringFactory
{
  /**
   * @return The appropriate String
   */
  public String getValue(TransientRecord record);
}
