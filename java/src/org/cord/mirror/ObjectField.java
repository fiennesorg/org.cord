package org.cord.mirror;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.cord.util.ObjectUtil;

import com.google.common.base.Objects;

public abstract class ObjectField<T>
  extends Field<T>
{
  private int _objectValueIndex = -1;

  protected ObjectField(FieldKey<T> fieldKey,
                        String label,
                        boolean supportsNull,
                        Class<T> valueClass,
                        FieldValueSupplier<? extends T> newRecordValue,
                        FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          supportsNull,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public ObjectField(FieldKey<T> fieldKey,
                     String label,
                     Class<T> valueClass,
                     FieldValueSupplier<? extends T> newRecordValue,
                     FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          valueClass,
          newRecordValue,
          defaultDefinedValue);
  }

  public ObjectField(FieldKey<T> fieldKey,
                     String label,
                     Class<T> valueClass,
                     FieldValueSupplier<? extends T> defaultDefinedValue)
  {
    super(fieldKey,
          label,
          valueClass,
          defaultDefinedValue);
  }

  public void storeNewRecordValue(Object[] objectValues,
                                  int[] intValues)

  {
    objectValues[getObjectValueIndex()] = getNewRecordValue();
  }

  @Override
  protected synchronized void register(Table table,
                                       int fieldId)
      throws MirrorTableLockedException
  {
    super.register(table, fieldId);
    _objectValueIndex = table.getNextObjectValueIndex(this);
  }

  protected final int getObjectValueIndex()
  {
    return _objectValueIndex;
  }

  public final Object getFieldValueObject(TransientRecord record)
  {
    return record.getObjectValues()[getObjectValueIndex()];
  }

  public final T getFieldValue(TransientRecord record)
  {
    return ObjectUtil.castTo(getFieldValueObject(record), getValueClass());
  }

  public void dbToValue(ResultSet resultSet,
                        Object[] objectValues,
                        int[] intValues)
      throws SQLException
  {
    Object o = resultSet.getObject(getSqlFieldIndex());
    try {
      objectValues[getObjectValueIndex()] = o == null
          ? dbNullToValue(objectValues, intValues)
          : dbToValue(o, objectValues, intValues);
    } catch (RuntimeException e) {
      throw new MirrorLogicException(String.format("Error converting %s.%s value of \"%s\" (%s) to %s",
                                                   getTable(),
                                                   getName(),
                                                   o,
                                                   o.getClass(),
                                                   getValueClass()),
                                     e);
    }
  }

  /**
   * Resolve the SQL Object representation of the data for this Field into the appropriate T.
   * 
   * @param values
   *          the array of values that have been resolved from the resultSet up until the point when
   *          this Field is requested. Any Fields with lower id numbers will have been resolved and
   *          the data will be available. You should not change this data.
   */
  public abstract T dbToValue(Object v,
                              Object[] values,
                              int[] intValues);

  public T dbNullToValue(Object[] values,
                         int[] intValues)
  {
    return null;
  }

  protected final void populateInsert(TransientRecord record,
                                      PreparedStatement preparedStatement)
      throws SQLException
  {
    T value = getFieldValue(record);
    if (value == null) {
      preparedStatement.setNull(getSqlFieldIndex(), getSqlType());
    } else {
      populateDefinedInsert(preparedStatement, value);
    }
  }

  /**
   * Add the defined value for this Field on the given record into the SQL PreparedStatement that
   * will insert the data into the database.
   * 
   * @param preparedStatement
   * @param value
   *          The value that is to be inserted into the PreparedStatement. This will never be null.
   * @throws SQLException
   */
  protected abstract void populateDefinedInsert(PreparedStatement preparedStatement,
                                                T value)
      throws SQLException;

  @Override
  protected boolean isEqualValue(TransientRecord record,
                                 T value)
  {
    return Objects.equal(getFieldValueObject(record), value);
  }

  @Override
  protected void setRecordToValueStore(TransientRecord record,
                                       Object value)
  {
    record.getObjectValues()[getObjectValueIndex()] = value;
  }

  @Override
  public boolean isEqualValues(TransientRecord record1,
                               TransientRecord record2)
  {
    return Objects.equal(getFieldValueObject(record1), getFieldValueObject(record2));
  }

  @Override
  public int compareValues(TransientRecord record1,
                           TransientRecord record2)
  {
    return ObjectUtil.compare(getFieldValueObject(record1), getFieldValueObject(record2));
  }

  @Override
  public int compareValuesIgnoreCase(TransientRecord record1,
                                     TransientRecord record2)
  {
    return ObjectUtil.compareIgnoreCase(getFieldValueObject(record1), getFieldValueObject(record2));
  }

}
