package org.cord.mirror;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.cord.sql.ConnectionFacade;
import org.cord.sql.ConnectionPool;
import org.cord.sql.ConnectionPoolFacade;
import org.cord.sql.SingleConnectionFacade;
import org.cord.sql.ThreadConnectionFacade;
import org.cord.util.DebugConstants;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Expirable;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.Maps;
import org.cord.util.NumberUtil;
import org.cord.util.Shutdownable;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * The Db class represents the top level of the Mirror library and it represents a complete MySql
 * database.
 */
public final class Db
{
  public static final boolean ENABLE_COMP = true;

  /**
   * Configuration value to set whether or not Transactions should clear Query caches in Tables that
   * have updated Queries when they commit. This is currently false and I think that it should stay
   * that way because the presence of named Queries makes it more likely that people will know what
   * they are looking for in the caches and therefore clearing them even if they are redundant will
   * be a waste of time.
   */
  public static final String CONFIG_DB_FLUSHQUERYCACHES = "db.flushQueryCaches";
  public static final boolean CONFIG_DB_FLUSHQUERYCACHES_DEFAULT = true;
  private final boolean __flushQueryCaches;

  public final boolean flushQueryCaches()
  {
    return __flushQueryCaches;
  }

  public static final String CONFIG_FIELD_SETFIELD_EXCEPTIONFORINVALIDNULL =
      "field.setField.exceptionForInvalidNull";

  public static final String CONFIG_FIELD_SETFIELD_EXCEPTIONFORTYPECONVERSION =
      "field.setField.exceptionForTypeConversion";

  /*
   * If set in the configuration, then this will force mirror to use Java-space transforms in
   * preference to SQL queries when the situation arises, instead of only using it when a
   * transaction is involved. Intended for debugging use: should not be true in production.
   */
  public static final String CONFIG_DISABLE_QUERY_BRANCH = "recordSource.disableQueryBranch";
  public static final boolean CONFIG_DISABLE_QUERY_BRANCH_DEFAULT = false;

  private final boolean __disableQueryBranch;

  public final boolean disableQueryBranch()
  {
    return __disableQueryBranch;
  }

  /**
   * Value that should be used in timeout operations to signify that the most appropriate default
   * timeout value should be used.
   */
  public final static long DEFAULT_TIMEOUT = -1;

  public final static long DEFAULT_CACHE_TIMEOUT = 30000l;

  private final String __name;

  private final String __transactionPassword;

  private final ConnectionPool __connectionPool;
  private final ThreadConnectionFacade __threadConnectionFacade;

  // String --> Table
  private final Map<String, Table> __tables = new HashMap<String, Table>();

  private final Map<String, Table> __roTables = Collections.unmodifiableMap(__tables);

  private final ConcurrentHashMap<String, Transaction> __transactions = Maps.newConcurrentHashMap();

  private int _transactionCounter;

  private long _defaultTransactionTimeout;

  private final Map<String, DbInitialiser> __dbInitialisers = new HashMap<String, DbInitialiser>();

  private final Map<String, DbInitialiser> __immutableDbInitialisers =
      Collections.unmodifiableMap(__dbInitialisers);

  private final ExpireTrigger __expireTrigger;

  private Set<Expirable> __expirables = Sets.newHashSet();

  private Set<Shutdownable> __shutdownables = Sets.newHashSet();

  private final Gettable __configuration;

  private final Set<Lockable> __lockables = Sets.newHashSet();

  private final CacheBuilderSpec __recordCacheSpec;
  private final CacheBuilderSpec __recordSourceCacheSpec;

  private final int __defaultDbPrecacheSize;

  /**
   * Create a new Db with a given name and a given connection to the physical database. The database
   * is created empty and with no Tables.
   * <p>
   * The caching templates are to enable users to define default models for the caching behaviour of
   * tables. This enables people to define Db objects that are optimised with their preferred access
   * routines and then pass them to a population method that will automatically use these
   * parameters.
   * 
   * @param name
   *          The name of the db. This should be the same as the name of the physical database.
   * @param pwd
   *          The unencrypted password that should be used for all methods that create a Transaction
   *          in the future. This password is not retrievable by any public methods again, so it is
   *          up to the client to keep track of it. This therefore stops unauthorized clients (ie
   *          template authors) from introducing code that manipulates the values in the database
   *          without first being given explicit permission to do so by the servlet programmer.
   * @param connectionPool
   *          The initialised ConnectionPool object that is ready to connect to the given database.
   */
  public Db(String name,
            String pwd,
            ConnectionPool connectionPool,
            long defaultTransactionTimeout,
            CacheBuilderSpec recordCacheSpec,
            int defaultDbPrecacheSize,
            CacheBuilderSpec recordSourceCacheSpec,
            Gettable configuration)
  {
    __name = name;
    __transactionPassword = Preconditions.checkNotNull(pwd, "pwd");
    __connectionPool = connectionPool;
    __threadConnectionFacade =
        new ThreadConnectionFacade(new ConnectionPoolFacade(__connectionPool));
    _defaultTransactionTimeout = defaultTransactionTimeout;
    _transactionCounter = 0;
    __expireTrigger = new ExpireTrigger(5000);
    __expireTrigger.setPriority(Thread.NORM_PRIORITY - 1);
    __recordCacheSpec = Preconditions.checkNotNull(recordCacheSpec, "recordCacheSpec");
    __recordSourceCacheSpec =
        Preconditions.checkNotNull(recordSourceCacheSpec, "recordSourceCacheSpec");
    __defaultDbPrecacheSize = defaultDbPrecacheSize;
    __configuration = Preconditions.checkNotNull(configuration, "Db.configuration");

    Boolean disableQueryBranch = __configuration.getBoolean(CONFIG_DISABLE_QUERY_BRANCH);
    __disableQueryBranch = disableQueryBranch == null
        ? CONFIG_DISABLE_QUERY_BRANCH_DEFAULT
        : disableQueryBranch.booleanValue();
    Boolean flushQueryCaches = __configuration.getBoolean(CONFIG_DB_FLUSHQUERYCACHES);
    __flushQueryCaches = flushQueryCaches == null
        ? CONFIG_DB_FLUSHQUERYCACHES_DEFAULT
        : flushQueryCaches.booleanValue();
  }

  public int getDbDefaultPrecacheSize()
  {
    return __defaultDbPrecacheSize;
  }

  public ThreadConnectionFacade getThreadConnectionFacade()
  {
    return __threadConnectionFacade;
  }

  /**
   * Return the Gettable configuration that was passed to the constructor.
   * 
   * @return The appropriate Gettable. Never null, although may be empty.
   */
  public final Gettable getConfiguration()
  {
    return __configuration;
  }

  public boolean addExpirable(Object object)
  {
    if (object instanceof Expirable) {
      return addExpirable((Expirable) object);
    }
    return false;
  }

  public boolean addExpirable(Expirable expirable)
  {
    if (expirable != null) {
      synchronized (__expirables) {
        __expirables.add(expirable);
        return true;
      }
    }
    return false;
  }

  public boolean addShutdownable(Object object)
  {
    if (object != null && object instanceof Shutdownable) {
      synchronized (__shutdownables) {
        __shutdownables.add((Shutdownable) object);
        return true;
      }
    }
    return false;
  }

  /**
   * Create a new random transactionPassword. Although not guaranteed to be unique, the odds are
   * really pretty high that this is the case.
   */
  public static String createTransactionPassword()
  {
    return Double.toString(Math.random());
  }

  private final List<RecordSourceOperation<?>> __globalRecordSourceOperations =
      new LinkedList<RecordSourceOperation<?>>();

  public RecordSourceOperation<?> addRecordSourceOperation(RecordSourceOperation<?> operation)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(operation, "operation");
    synchronized (__globalRecordSourceOperations) {
      if (__globalRecordSourceOperations.contains(operation)) {
        return __globalRecordSourceOperations.get(__globalRecordSourceOperations.indexOf(operation));
      }
      addExpirable(operation);
      addShutdownable(operation);
      __globalRecordSourceOperations.add(operation);
      for (Table table : getTables()) {
        table.addRecordSourceOperation(operation, true);
      }
    }
    return operation;
  }

  public String debug()
  {
    StringBuilder result = new StringBuilder();
    for (Table table : getTables()) {
      result.append(table.debug()).append('\n');
    }
    return result.toString();
  }

  private void registerLegacyRecordSourceOperations(Table newTable)
      throws MirrorTableLockedException
  {
    synchronized (__globalRecordSourceOperations) {
      for (RecordSourceOperation<?> op : __globalRecordSourceOperations) {
        newTable.addRecordSourceOperation(op, true);
      }
    }
  }

  private final List<RecordOperation<?>> __globalRecordOperations = Lists.newLinkedList();

  /**
   * @return The RecordOperation that was added to the Db or the existing RecordOperation if there
   *         was already one registered that tested equal to operation.
   */
  public RecordOperation<?> add(RecordOperation<?> operation)
      throws MirrorTableLockedException
  {
    Preconditions.checkNotNull(operation, "operation");
    synchronized (__globalRecordOperations) {
      if (__globalRecordOperations.contains(operation)) {
        return __globalRecordOperations.get(__globalRecordOperations.indexOf(operation));
      }
      addExpirable(operation);
      addShutdownable(operation);
      __globalRecordOperations.add(operation);
      for (Table table : getTables()) {
        table.addRecordOperation(operation, true);
      }
    }
    return operation;
  }

  private void registerLegacyRecordOperations(Table newTable)
      throws MirrorTableLockedException
  {
    synchronized (__globalRecordOperations) {
      Iterator<RecordOperation<?>> recordOperations = __globalRecordOperations.iterator();
      while (recordOperations.hasNext()) {
        newTable.addRecordOperation(recordOperations.next(), true);
      }
    }
  }

  public void preLock()
  {
    try {
      for (Table table : getTables()) {
        table.preLock();
      }
    } catch (MirrorTableLockedException mtlEx) {
      throw new LogicException("Tables should not be locked during Db.preLock()", mtlEx);
    }
  }

  public final void lock()
  {
    try {
      for (Table table : getTables()) {
        table.lock();
      }
    } catch (MirrorTableLockedException mtlEx) {
      throw new LogicException("Table structure should not be updated during Db.lock()", mtlEx);
    }
    for (Lockable lockable : __lockables) {
      lockable.lock();
    }
    __expireTrigger.start();
  }

  public void addLockable(Lockable lockable)
  {
    __lockables.add(Preconditions.checkNotNull(lockable, "lockable"));
  }

  public int add(DbInitialisers dbInitialisers)
      throws MirrorTableLockedException
  {
    int count = 0;
    if (dbInitialisers != null) {
      Iterator<DbInitialiser> i = dbInitialisers.getDbInitialisers();
      while (i.hasNext()) {
        count += add(i.next()) == null ? 1 : 0;
      }
    }
    return count;
  }

  /**
   * Add a new DbInitialiser to this Db. This will only call the init method if the initialiser
   * hasn't been used before on this db otherwise it will be ignored. It is therefore safe to invoke
   * the same DbInitialiser multiple times without fear of repercussions. Once the DbInitialiser has
   * been registered then {@link DbInitialiser#init(Db, String)} will be invoked on it.
   * 
   * @param dbInitialiser
   *          The initialiser to invoke on this Db.
   * @return the DbInitialiser that was registerd under the name of dbInitialiser. If the
   *         DbInitialiser was previously registered and was the same Class as dbInitialiser then
   *         the original instance is returned.
   * @throws MirrorTableLockedException
   *           If the database in question has been locked before initialisation.
   * @throws DuplicateNamedException
   *           If you have already registered a DbInitialiser with the same name and it is of a
   *           different Class to this dbInitialiser.
   */
  @SuppressWarnings("unchecked")
  public <T extends DbInitialiser> T add(T dbInitialiser)
      throws MirrorTableLockedException
  {
    if (dbInitialiser == null) {
      return null;
    }
    synchronized (__dbInitialisers) {
      DbInitialiser existingItem = __dbInitialisers.get(dbInitialiser.getName());
      if (existingItem != null) {
        try {
          return (T) existingItem;
        } catch (ClassCastException e) {
          throw new DuplicateNamedException(dbInitialiser.getName(),
                                            String.format("%s (%s) was already registered as %s (%s)",
                                                          dbInitialiser,
                                                          dbInitialiser.getClass(),
                                                          existingItem,
                                                          existingItem.getClass()));
        }
      }
      // 2DO # possible security hole: transactionPassword is given
      // out quite freely to "untrusted" DbInitialiser not really a
      // solution to this, other than locking the db while it is still
      // within scope for your application and then rejecting all
      // future DbInitialisers. This will help matters greatly.
      if (DebugConstants.DEBUG_DBINITIALISERS) {
        System.err.println(this + ".add(" + dbInitialiser + ")");
      }
      dbInitialiser.init(this, __transactionPassword);
      __dbInitialisers.put(dbInitialiser.getName(), dbInitialiser);
      addShutdownable(dbInitialiser);
    }
    return dbInitialiser;
  }

  /**
   * Get the names of all used DbInitialiser Objects.
   * 
   * @return Immutable Map of String to DbInitialiser objects.
   * @see DbInitialiser
   */
  public Map<String, DbInitialiser> getDbInitialisers()
  {
    return __immutableDbInitialisers;
  }

  public void checkTransactionPassword(String password,
                                       String details,
                                       Object... detailsParams)
      throws MirrorAuthorisationException
  {
    if (!__transactionPassword.equals(password)) {
      throw new MirrorAuthorisationException(String.format(details, detailsParams), null);
    }
  }

  /**
   * Get the default Transaction timeout that will be used when an explicit argument is not given.
   * 
   * @return The default timeout in milliseconds.
   */
  public final long getDefaultTransactionTimeout()
  {
    return _defaultTransactionTimeout;
  }

  /**
   * Create a new Table object and insert it into the Db. The Table is created in state
   * DBSTATE_DEFINETABLES.
   * 
   * @param name
   *          The name of the Table. This should be the same name as the table has in the backend db
   *          otherwise queries will not function correctly.
   * @param referencesName
   *          The name of the default RecordOperation that will resolve to Queries from the Table.
   *          If this is null, then the name will be calculated by making the first letter of the
   *          name lower case and appending an s to the end of it (ie ActiveDog --> activeDogs).
   * @return A new Table object.
   * @throws java.lang.IllegalStateException
   *           If the Db is not in state DBSTATE_DEFINETABLES
   * @throws DuplicateNamedException
   *           If there is already a Table that has been declared with the same name.
   */
  public synchronized Table addTable(TableWrapper tableWrapper,
                                     String name,
                                     RecordOperationKey<RecordSource> referencesName,
                                     Integer optPrecacheSize,
                                     CacheBuilderSpec recordSourceCacheSpec)
      throws DuplicateNamedException, MirrorTableLockedException
  {
    if (__tables.containsKey(name)) {
      throw new DuplicateNamedException(name, "Table already exists");
    }
    optPrecacheSize = optPrecacheSize == null
        ? NumberUtil.toInteger(getConfiguration().getString(DbBooter.CONF_PRECACHESIZE + "."
                                                            + name),
                               null)
        : optPrecacheSize;
    int precacheSize =
        optPrecacheSize == null ? getDbDefaultPrecacheSize() : optPrecacheSize.intValue();
    if (recordSourceCacheSpec == null) {
      String spec = getConfiguration().getString(DbBooter.CONF_RECORDSOURCECACHE + "." + name);
      recordSourceCacheSpec = spec == null ? __recordSourceCacheSpec : CacheBuilderSpec.parse(spec);
    }
    Table table =
        new Table(name, referencesName, this, precacheSize, recordSourceCacheSpec, tableWrapper);
    __tables.put(name, table);
    registerLegacyRecordSourceOperations(table);
    registerLegacyRecordOperations(table);
    return table;
  }

  public Table addSoftTable(TableWrapper tableWrapper,
                            String name,
                            RecordOperationKey<RecordSource> referencesName)
      throws MirrorTableLockedException
  {
    return addTable(tableWrapper, name, referencesName, null, null);
  }

  public Table addTable(TableWrapper tableWrapper,
                        String name)
      throws DuplicateNamedException, MirrorTableLockedException
  {
    return addTable(tableWrapper, name, null, null, null);
  }

  /**
   * Get the name of this Db which should correspond to the name of the backend database.
   * 
   * @return The name of the db.
   */
  public final String getName()
  {
    return __name;
  }

  /**
   * Get the current time in milliseconds. This is currently purely a wrapper around
   * System.currentTimeMillis(), but ensuring that all classes in the mirror package use this method
   * will permit more efficient (although less accurate) time measurements to be slotted into place
   * at a later date if required,
   * 
   * @return The time in milliseconds.
   */
  public long currentTimeMillis()
  {
    return System.currentTimeMillis();
  }

  /**
   * Get the Table with the requested tableName.
   * 
   * @param tableName
   *          The name of the desired Table
   * @return The desired Table
   * @throws NoSuchElementException
   *           if the tableName refers to a non-existent Table.
   */
  public Table getTable(String tableName)
      throws NoSuchElementException
  {
    Table table = __tables.get(tableName);
    if (table == null) {
      throw new NoSuchElementException("Db.getTable(" + tableName + ") not found");
    }
    return table;
  }

  public boolean hasTable(String tableName)
  {
    return __tables.containsKey(tableName);
  }

  /**
   * Webmacro utility method that acts as a wrapper around getTable(tableName). This permits the
   * access to Table objects under webmacro via $db.tableName
   * 
   * @param objectName
   *          objectName.toString() should give the name of the table.
   * @return The desired Table.
   */
  public Object get(Object objectName)
  {
    return getTable(objectName.toString());
  }

  /**
   * @return unmodifiable Collection of Tables
   */
  public Collection<Table> getTables()
  {
    return __roTables.values();
  }

  public Set<Table> getTables(String orderingName)
  {
    TreeSet<Table> treeSet =
        new TreeSet<Table>(TableOrderings.getInstance().getOrdering(orderingName));
    treeSet.addAll(__tables.values());
    return Collections.unmodifiableSet(treeSet);
  }

  /**
   * Get the ConnectionPool object that is used by this Db to connect to the backend db.
   * 
   * @return The ConnectionPool object!
   */
  @Deprecated
  public ConnectionPool getConnectionPool()
  {
    return __connectionPool;
  }

  @Override
  public String toString()
  {
    return "DB(" + __name + ")";
  }

  /**
   * Create a new (currently empty) explicitly named transaction that is synced to this db with the
   * specified timeout after which it will fail to function. This can be used to make Singleton
   * Transaction processes by always using the same name for the Transaction associated with a
   * particular operation, thereby guaranteeing that the transaction will never be duplicated at the
   * same time.
   * 
   * @param name
   *          The name to call the Transaction. If there is already a valid Transaction with this
   *          name then the operation will fail.
   * @param timeout
   *          The timeout in milliseconds for this timeout to complete. To use a default value
   *          supply DEFAULT_TIMEOUT.
   * @param transactionType
   *          Either TRANSACTION_DELETE or TRANSACTION_UPDATE depending on the requirements.
   * @param details
   *          The information as to why this transaction is required which will be stored in the
   *          Transaction and used for toString and and accessible via getDetails()
   * @return a new Transaction object or null if the there is already a valid Transaction with the
   *         given name.
   * @see Transaction#TRANSACTION_DELETE
   * @see Transaction#TRANSACTION_UPDATE
   * @see #DEFAULT_TIMEOUT
   * @see Transaction#getDetails()
   */
  protected Transaction createTransaction(String name,
                                          long timeout,
                                          int transactionType,
                                          String details)
  {
    Transaction newTransaction = new Transaction(name,
                                                 this,
                                                 transactionType,
                                                 currentTimeMillis(),
                                                 handleDefaultTransactionTimeout(timeout),
                                                 details);
    Transaction oldTransaction = __transactions.putIfAbsent(name, newTransaction);
    if (oldTransaction == null) {
      _transactionCounter++;
      return newTransaction;
    }
    return null;
  }

  public final long handleDefaultTransactionTimeout(long timeout)
  {
    return (timeout == DEFAULT_TIMEOUT ? _defaultTransactionTimeout : timeout);
  }

  /**
   * Calculate the DB name from the MYSQL connection URL as supplied from the metadata in the
   * Connection. This isn't particularly efficient and if you want to call it a lot then you should
   * probably find an alternative method.
   */
  public static String getDbName(Connection connection)
      throws SQLException
  {
    String url = connection.getMetaData().getURL();
    return url.substring(url.lastIndexOf('/') + 1, url.indexOf('?'));
  }

  /**
   * Create a new (currently empty) automatically named Transaction. The name is an Integer string
   * and the system will continue to try ever increasing numbers until it finds one that fits. If
   * people do not explicitly check in Transactions with Integer values as names, then the name will
   * also correspond to the total number of Transactions created so far in this session of the Db
   * (including explicitly named Transactions).
   * 
   * @param timeout
   *          The time from creation that this Transaction will remain valid.
   * @param transactionType
   *          Either TRANSACTION_DELETE or TRANSACTION_UPDATE depending on the requirements.
   * @param details
   *          The information as to why this transaction is required for use in creating
   *          authentication exceptions if the password fails.
   * @return A new Transaction. This should <i>always</i> return a new Transaction because it
   *         recurses until it finds one. If it doesn't return, then you have other problems to
   *         think about...
   */
  protected Transaction createTransaction(long timeout,
                                          int transactionType,
                                          String details)
  {
    Transaction transaction =
        createTransaction(Integer.toString(_transactionCounter), timeout, transactionType, details);
    return (transaction == null
        ? createTransaction(timeout, transactionType, details)
        : transaction);
  }

  /**
   * Create a new (currently empty) explicitly named authenticated transaction that is synced to
   * this db with the specified timeout after which it will fail to function. This can be used to
   * make Singleton Transaction processes by always using the same name for the Transaction
   * associated with a particular operation, thereby guaranteeing that the transaction will never be
   * duplicated at the same time.
   * 
   * @param name
   *          The name to call the Transaction. If there is already a valid Transaction with this
   *          name then the operation will fail.
   * @param timeout
   *          The timeout in milliseconds for this timeout to complete.
   * @param transactionType
   *          Either TRANSACTION_DELETE or TRANSACTION_UPDATE depending on the requirements.
   * @return a new Transaction object or null if the there is already a valid Transaction with the
   *         given name.
   * @param transactionPassword
   *          The plain text authentication password to prove that the client has the rights to
   *          create a transaction.
   * @param details
   *          The information as to why this transaction is required for use in creating
   *          authentication exceptions if the password fails.
   * @throws MirrorAuthorisationException
   *           If the supplied transaction password is not correct.
   * @see Transaction#TRANSACTION_DELETE
   * @see Transaction#TRANSACTION_UPDATE
   */
  public Transaction createTransaction(String name,
                                       long timeout,
                                       int transactionType,
                                       String transactionPassword,
                                       String details)
      throws MirrorAuthorisationException
  {
    checkTransactionPassword(transactionPassword, details);
    return createTransaction(name, timeout, transactionType, details);
  }

  /**
   * Create a new (currently empty) automatically named authenticated Transaction. The name is an
   * Integer string and the system will continue to try ever increasing numbers until it finds one
   * that fits. If people do not explicitly check in Transactions with Integer values as names, then
   * the name will also correspond to the total number of Transactions created so far in this
   * session of the Db (including explicitly named Transactions).
   * 
   * @param timeout
   *          The time from creation that this Transaction will remain valid.
   * @param transactionType
   *          Either TRANSACTION_DELETE or TRANSACTION_UPDATE depending on the requirements.
   * @param transactionPassword
   *          The plain text authentication password to prove that the client has the rights to
   *          create a transaction.
   * @param details
   *          The information as to why this transaction is required for use in creating
   *          authentication exceptions if the password fails.
   * @throws MirrorAuthorisationException
   *           If the supplied transaction password is not correct.
   * @return A new Transaction. This should <i>always</i> return a new Transaction because it
   *         recurses until it finds one. If it doesn't return, then you have other problems to
   *         think about...
   */
  public Transaction createTransaction(long timeout,
                                       int transactionType,
                                       String transactionPassword,
                                       String details)
      throws MirrorAuthorisationException
  {
    checkTransactionPassword(transactionPassword, details);
    return createTransaction(timeout, transactionType, details);
  }

  /**
   * Remove the named Transaction from the list of currently active Transaction. This only takes the
   * Transaction out of the current set of Transactions and has no side effects.
   * 
   * @param name
   *          The name of the Transaction to remove.
   * @return true if the Transaction was found and removed.
   */
  protected boolean removeTransaction(String name)
  {
    return __transactions.remove(name) != null;
  }

  protected boolean removeTransaction(Transaction transaction)
  {
    return removeTransaction(transaction.getName());
  }

  /**
   * Request a named Transaction from this the list of currently active Transactions with
   * authentication for untrusted clients. A Transaction is active until it is completed or until it
   * times out at some point. This method will:-
   * <ul>
   * <li>Check to see if the named Transaction exists
   * <li>Check that the Transaction is valid (at the time of issue - it may well timeout between
   * returning the transaction and the first usage of the transaction)
   * </ul>
   * 
   * @param name
   *          The name of the desired Transaction
   * @param transactionPassword
   *          The plain text authentication password to prove that the client has the rights to
   *          create a transaction.
   * @param details
   *          The information as to why this transaction is required for use in creating
   *          authentication exceptions if the password fails.
   * @throws MirrorAuthorisationException
   *           If the supplied transaction password is not correct.
   * @return The currently valid Transaction where (Transaction.getName()==name), or null if no such
   *         transaction exists.
   * @see Transaction#getName()
   */
  public final Transaction getTransaction(String name,
                                          String transactionPassword,
                                          String details)
      throws MirrorAuthorisationException
  {
    checkTransactionPassword(transactionPassword, details);
    return getTransaction(name);
  }

  /**
   * Internal method without authentication to get a named Transaction from this the list of
   * currently active Transactions. A Transaction is active until it is completed or until it times
   * out at some point. This method will:-
   * <ul>
   * <li>Check to see if the named Transaction exists
   * <li>Check that the Transaction is valid (at the time of issue - it may well timeout between
   * returning the transaction and the first usage of the transaction)
   * </ul>
   * 
   * @param name
   *          The name of the desired Transaction
   * @return The currently valid Transaction where (Transaction.getName()==name), or null if no such
   *         transaction exists.
   * @see Transaction#getName()
   */
  protected final Transaction getTransaction(String name)
  {
    Transaction transaction = __transactions.get(name);
    if (transaction != null) {
      try {
        transaction.checkValidity();
        return transaction;
      } catch (MirrorTransactionTimeoutException mtteEx) {
      }
    }
    return null;
  }

  public final void shutdown(boolean shouldKillConnectionPool)
  {
    // _connectionPool = null;
    flushCaches();
    for (Shutdownable shutdownable : __shutdownables) {
      shutdownable.shutdown();
    }
    __shutdownables.clear();
    __expireTrigger.shutdown();
    __expirables.clear();
    for (Table table : getTables()) {
      table.shutdown();
    }
    __tables.clear();
    if (shouldKillConnectionPool && __connectionPool != null) {
      __connectionPool.shutdown();
    }
  }

  protected void flushCaches()
  {
    while (__transactions.size() > 0) {
      Iterator<Transaction> transactions = __transactions.values().iterator();
      Transaction transaction = transactions.next();
      transaction.cancel();
    }
    for (Table table : getTables()) {
      table.flushCaches();
    }
  }

  public void flushCaches(String transactionPassword)
      throws MirrorAuthorisationException
  {
    checkTransactionPassword(transactionPassword,
                             this + ".flushCaches(" + transactionPassword + ")");
    flushCaches();
  }

  /**
   * Utility method that executes a given SQL update string on a Statement while integrating into
   * the DEBUG_SQL logging facility, thereby making client code more readable.
   * 
   * @param statement
   *          The Statement under which the command is to be executed
   * @param sql
   *          The SQL command that is to be executed.
   * @return The integer return code.
   * @throws SQLException
   *           If there is an error performing the update.
   * @see Statement#executeUpdate(String)
   * @see DebugConstants#DEBUG_SQL
   */
  public static int executeUpdate(Statement statement,
                                  String sql)
      throws SQLException
  {
    try {
      int result = statement.executeUpdate(sql);
      if (DebugConstants.DEBUG_SQL) {
        DebugConstants.DEBUG_OUT.println(ConnectionPool.generateDebugString(statement.getConnection(),
                                                                            sql)
                                         + " --> " + result);
      }
      return result;
    } catch (SQLException sqlEx) {
      SQLException wrapperEx = new SQLException("faulty SQL:" + sql);
      wrapperEx.initCause(sqlEx);
      throw wrapperEx;
    }
  }

  /**
   * Execute the supplied SQL string as an update and return the record count.
   * 
   * @param SQL
   *          the SQL command to execute.
   * @return the record count of updated objects.
   * @throws java.sql.SQLException
   *           In times of SQL hassle...
   */
  public int executeUpdate(String SQL)
      throws SQLException
  {
    ConnectionFacade connectionFacade = getThreadConnectionFacade().get();
    Connection connection = null;
    Statement statement = null;
    try {
      connection = connectionFacade.checkOutConnection();
      try {
        statement = connection.createStatement();
        int result = Db.executeUpdate(statement, SQL);
        return result;
      } finally {
        if (statement != null) {
          statement.close();
        }
      }
    } catch (SQLException e) {
      if (connection != null) {
        connectionFacade.checkInBrokenConnection(connection);
      }
      SQLException wrapperEx = new SQLException("faulty SQL:" + SQL);
      wrapperEx.initCause(e);
      throw wrapperEx;
    } finally {
      connectionFacade.checkInWorkingConnection(connection);
    }
  }

  public void lockConnectionToThread()
  {
    getThreadConnectionFacade().set(new SingleConnectionFacade(getConnectionPool().checkOutConnection("Db.lockConnectionToThread")));
  }

  public void releaseConnectionFromThread()
  {
    getConnectionPool().checkInConnection(getThreadConnectionFacade().get().checkOutConnection(),
                                          "Db.releaseConnectionFromThread");
    getThreadConnectionFacade().remove();
  }

  public class ExpireTrigger
    extends Thread
  {
    private final long __sleepDuration;

    private boolean _isShutdown = false;

    private final Object __waitLock = new Object();

    public ExpireTrigger(long sleepDuration)
    {
      super(Db.this.getName() + "(ExpireTrigger)");
      __sleepDuration = sleepDuration;
    }

    @SuppressWarnings("unused")
    @Override
    public void run()
    {
      synchronized (__waitLock) {
        while (!_isShutdown) {
          try {
            __waitLock.wait(__sleepDuration);
          } catch (InterruptedException iEx) {
            // not fussed...
          }
          if (!_isShutdown) {
            int expireCount = 0;
            Iterator<Expirable> expirables = __expirables.iterator();
            Expirable expirable = null;
            try {
              while (expirables.hasNext()) {
                expirable = expirables.next();
                try {
                  expireCount += expirable.expire();
                } catch (Exception ex) {
                  System.err.println("Caught expire error in " + expirable);
                  ex.printStackTrace();
                  // continue from here because we are not responsible
                  // for the excentricities of 3rd party expire()
                  // implementations...
                }
              }
              if (DebugConstants.DEBUG_EXPIRABLE && expireCount > 0) {
                System.err.println(getName() + ": EXPIRE: " + expireCount);
              }
            } catch (ConcurrentModificationException iteratingEx) {
              System.err.println("Error in traversing expirable list - abandoning sweep");
              iteratingEx.printStackTrace();
            }
          }
        }
      }
      System.err.println(this + " shutting down...");
    }

    /**
     * Tell the ExpireTrigger thread to stop running. If the thread is currently in the process of
     * expiring objects then the request will block until it is completed, otherwise it will
     * complete instantly.
     */
    public void shutdown()
    {
      synchronized (__waitLock) {
        _isShutdown = true;
        __waitLock.notifyAll();
      }
    }
  }
}
