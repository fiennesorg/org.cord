package org.cord.mirror;

/**
 * RuntimeException that signifies that something has gone wrong with the internal integrity of the
 * database. You would not normally expect to catch this method as it would normally never get
 * thrown.
 * 
 * @author alex
 */
public class MirrorLogicException
  extends MirrorRuntimeException
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public MirrorLogicException(String message,
                              Throwable cause)
  {
    super(message,
          cause);
  }

  public MirrorLogicException(String message)
  {
    this(message,
         null);
  }

  public MirrorLogicException(Exception e)
  {
    super("MirrorLogicException",
          e);
  }
}
