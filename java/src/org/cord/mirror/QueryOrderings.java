package org.cord.mirror;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.cord.util.NumberUtil;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

/**
 * @author alex
 * @deprecated will be replaced by a collection of RecordOrdering implementations at some point in
 *             the future.
 */
@Deprecated
public final class QueryOrderings
  implements SingletonShutdownable
{
  public static final String SQLQUERY = "sqlQuery";

  public static final String REBUILDDURATION = "rebuildDuration";

  public static final String RECORDCOUNT = "recordCount";

  public static final String RESULTLISTCOUNT = "resultListCount";

  private static QueryOrderings _instance = new QueryOrderings();

  public static QueryOrderings getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private final Map<String, Comparator<Query>> __orderings =
      new HashMap<String, Comparator<Query>>();

  private QueryOrderings()
  {
    SingletonShutdown.registerSingleton(this);
    __orderings.put(SQLQUERY, new Comparator<Query>() {
      @Override
      public int compare(Query q0,
                         Query q1)
      {
        return q0.getSqlQuery().compareTo(q1.getSqlQuery());
      }
    });
    __orderings.put(REBUILDDURATION, new Comparator<Query>() {
      @Override
      public int compare(Query q0,
                         Query q1)
      {
        switch (NumberUtil.compare(q0.getLastRebuildDurationNs(), q1.getLastRebuildDurationNs())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return q0.getSqlQuery().compareTo(q1.getSqlQuery());
        }
      }
    });
    __orderings.put(RECORDCOUNT, new Comparator<Query>() {
      @Override
      public int compare(Query q0,
                         Query q1)
      {
        switch (NumberUtil.compare(q0.getSizeIfKnown(), q1.getSizeIfKnown())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return q0.getSqlQuery().compareTo(q1.getSqlQuery());
        }
      }
    });
    __orderings.put(RESULTLISTCOUNT, new Comparator<Query>() {
      @Override
      public int compare(Query q0,
                         Query q1)
      {
        switch (NumberUtil.compare(q0.getResultListCount(), q1.getResultListCount())) {
          case 1:
            return -1;
          case -1:
            return 1;
          default:
            return q0.getSqlQuery().compareTo(q1.getSqlQuery());
        }
      }
    });
  }

  public Comparator<Query> getOrdering(String name)
  {
    return __orderings.get(name);
  }
}