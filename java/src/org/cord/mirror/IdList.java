package org.cord.mirror;

import it.unimi.dsi.fastutil.ints.IntList;

/**
 * An Object that represents an ordered List of Integer record ids from a given Table. An IdList
 * should not contain null elements, although some of the Integer ids that it contains may no longer
 * refer to valid records on the Table if the Table has changed state since it was generated. An
 * IdList may or may not be mutable.
 * 
 * @author alex
 */
public interface IdList
  extends IntList
{
  /**
   * Get the Table that this IdList represents ids from. The ids are not guaranteed to exist in the
   * Table as the Table may have changed state since the IdList was last generated.
   * 
   * @return The appropriate Table. Never null.
   */
  public Table getTable();
}
