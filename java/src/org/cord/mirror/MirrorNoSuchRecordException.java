package org.cord.mirror;

import org.cord.util.OptionalInt;

/**
 * Checked Exception that is thrown to show that a client has requested a record from a table that
 * does not exist. This should be used for situations when not finding the record is something that
 * you would want the invoking method to do something about.
 * 
 * @see MissingRecordException
 */
public class MirrorNoSuchRecordException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = 850968373865739866L;

  private OptionalInt __id;

  private Table __table;

  public MirrorNoSuchRecordException(String details,
                                     Table table,
                                     OptionalInt id,
                                     Exception exception)
  {
    super(details,
          exception);
    __id = id;
    __table = table;
  }

  public MirrorNoSuchRecordException(String details,
                                     MirrorNoSuchRecordException cause)
  {
    this(details,
         cause.getTable(),
         cause.getId(),
         cause);
  }

  public MirrorNoSuchRecordException(String details,
                                     Table table)
  {
    this(details,
         table,
         OptionalInt.absent(),
         null);
  }

  public MirrorNoSuchRecordException(String details,
                                     Table table,
                                     Exception cause)
  {
    this(details,
         table,
         OptionalInt.absent(),
         cause);
  }

  public OptionalInt getId()
  {
    return __id;
  }

  public Table getTable()
  {
    return __table;
  }
}
