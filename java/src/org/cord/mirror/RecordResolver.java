package org.cord.mirror;

/**
 * Interface that describes objects that are capable of being invoked on a source record and will
 * resolve into a target record.
 */
public interface RecordResolver
{
  /**
   * Perform the necessary steps to transform the requested targetRecord into the appropriate target
   * record.
   * 
   * @param sourceRecord
   *          The record from which the targetRecord is to be extracted.
   * @param viewpoint
   *          The transactional viewpoint from which the operation is to be performed.
   * @throws MirrorNoSuchRecordException
   *           if it is not possible to resolve the target record for whatever reason.
   */
  public PersistentRecord resolveRecord(TransientRecord sourceRecord,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  /**
   * Check to see whether or not requesting resolveRecord would result in a
   * MirrorNoSuchRecordException if it was invoked on the given sourceRecord. This should be
   * implemented in as efficient a manner as possible as it is intended to provide a short-cut
   * method for resolveRecord without the overhead of unnecessary exception handling.
   * 
   * @param sourceRecord
   *          The record from which the targetRecord would be extracted.
   * @param viewpoint
   *          The transactional viewpoint from which the operation would be performed.
   */
  public boolean mayResolveRecord(TransientRecord sourceRecord,
                                  Viewpoint viewpoint);
}
