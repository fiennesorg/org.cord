package org.cord.mirror;

/**
 * An Object that is capable of supplying a value of class T for a Field
 * 
 * @see org.cord.mirror.Field
 */
public interface FieldValueSupplier<T>
{
  /**
   * Supply the value for the given record.
   * 
   * @param record
   *          The record that the value is to be associated with. If the value is to be used outside
   *          the context of a record (eg when creating a set of default values for a new record)
   *          then record will be null.
   */
  public T supplyValue(TransientRecord record);
}
