package org.cord.mirror;

import org.cord.util.EnglishNamed;

/**
 * An Object that is capable of representing an ordering onto a set of records.
 * 
 * @author alex
 */
public interface RecordOrdering
  extends EnglishNamed
{
  /**
   * The comparable String representation of the ordering. If two RecordOrderings have the same
   * value for this then they should represent the same order of records regardless of whether they
   * represent an SQL or a java implementation. If the ordering does have an SQL meaning then it is
   * probably safest to have this as the SQL component for the Query. If it is a pure java
   * representation then it is up to user implementation as to what should be returned.
   * 
   * @return The comparable order representation. Not null.
   */
  @Override
  public String getName();

  /**
   * Get the SQL representation (if any) of this ordering.
   * 
   * @return The SQL ordering or null if it is not possible to execute this ordering in SQL.
   */
  public String getSqlOrdering();

  /**
   * Apply the appropriate ordering to the given collection of record ids.
   * 
   * @param ids
   *          The record ids to sort. Not null.
   * @param viewpoint
   *          The viewpoint to use when looking at the ids. Potentially null.
   * @return The appropriate ordering. If null then the ordering is assumed to be the same as the
   *         original ordering. However it is more polite to just return ids in this case.
   */
  public IdList applyJavaOrdering(IdList ids,
                                  Viewpoint viewpoint);
}
