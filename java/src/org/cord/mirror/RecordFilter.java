package org.cord.mirror;

/**
 * Interface that describes Objects that are capable of accepting or rejecting records defined on
 * some unspecified criteria. Note that the record passed by its id to the filter may not exist in
 * the Table.
 */
public interface RecordFilter
{
  /**
   * Test the supplied recordId on the specified Table to see if it is accepted. Note that the
   * recordId may not refer to a record that actually exists, but the RecordFilter should not throw
   * a MirrorNoSuchRecordException in this case, but rather just return false (assuming that this is
   * a failure condition).
   * 
   * @param table
   *          The Table which the recordId is relative to
   * @param recordId
   *          The id of the record from table.
   * @return true if the filter accepts the record, false if it rejects it.
   */
  public boolean accepts(Table table,
                         int recordId);

  /**
   * Test the supplied concrete record to see if it is accepted. Any implementation should be
   * prepared to process records from any Table to ensure that it is transparently pluggable.
   */
  public boolean accepts(PersistentRecord record);
}
