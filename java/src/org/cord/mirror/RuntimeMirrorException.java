package org.cord.mirror;

/**
 * Parent class for any unchecked runtime exceptions generated by the mirror system.
 * 
 * @author alex
 */
public class RuntimeMirrorException
  extends RuntimeException
{
  /**
   * default
   */
  private static final long serialVersionUID = 1L;

  public RuntimeMirrorException(String message,
                                Throwable cause)
  {
    super(message,
          cause);
  }
}
