package org.cord.mirror.trigger;

import java.util.HashSet;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * Implementation of both PreInstantiationTrigger and PostInstantiationTrigger that provides
 * advances rollback and delayed committing of resources.
 * <p>
 * The order of invocation will be:-
 * </p>
 * <ol>
 * <li>Invoke triggerPre</li>
 * <li>Create database record</li>
 * <li>Invoke triggerPost</li>
 * <li>Release triggerPost</li>
 * <li>Release triggerPre</li>
 * </ol>
 * <p>
 * The choice of when to do the work (ie in triggerPre or triggerPost) depends on a) whether you
 * need the record to actually exist in order to do the work (triggerPost) and b) whether you want
 * the record to be prevented from being created if you can't do the work (triggerPre).
 * </p>
 * <p>
 * Either way, the work is divided up into an optional delete and an optional update transaction and
 * an optional Set of created PersistentRecords. The PersistentRecords will obviously have been
 * created at the point when they are created, but the transactions will not get committed until the
 * releasePre or releasePost method (choice configured in constructore) is invoked and all the
 * components have approved the creation of the new record. If the record creation fails for any
 * reason then all the transactions will be automatically cancelled and all the newly created
 * records are deleted.
 * </p>
 * 
 * @see #releasePre(TransientRecord, Gettable)
 * @see #rollbackPre(TransientRecord, Gettable)
 * @author alex
 */
public abstract class TransactionalInstantiationTrigger
  implements PreInstantiationTrigger, PostInstantiationTrigger
{
  private final Int2ObjectMap<Transaction> __deleteTransactions =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<Transaction>());

  private final Int2ObjectMap<Transaction> __updateTransactions =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<Transaction>());

  private final Int2ObjectMap<Set<PersistentRecord>> __createdRecords =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<Set<PersistentRecord>>());

  private final Db __db;

  private final String __pwd;

  private final boolean __commitAsPre;

  /**
   * @param commitAsPre
   *          If true then any pending transactions will be committed as part of the release phase
   *          of the PreInstantiationTrigger cycle, and if false then they will be invoked as part
   *          of the PostInstantiationTrigger cycle. I suspect that false (PostInstantiationTrigger
   *          style) will probably be the most sensible time to do this.
   */
  public TransactionalInstantiationTrigger(Db db,
                                           String pwd,
                                           boolean commitAsPre)
  {
    __db = db;
    __pwd = pwd;
    __commitAsPre = commitAsPre;
  }

  protected final Db getDb()
  {
    return __db;
  }

  protected final String getPwd()
  {
    return __pwd;
  }

  private int getKey(TransientRecord record)
  {
    return record.getPendingId();
  }

  private final Transaction createDeleteTransaction()
  {
    return __db.createTransaction(Db.DEFAULT_TIMEOUT,
                                  Transaction.TRANSACTION_DELETE,
                                  __pwd,
                                  this.toString());
  }

  /**
   * Create or resolve the delete Transaction that is bound to the given TransientRecord. This
   * Transaction will be automatically committed once the record has been made persistent.
   */
  protected final Transaction bootDeleteTransactionFor(TransientRecord record)
  {
    int key = getKey(record);
    Transaction t = __deleteTransactions.get(key);
    if (t == null) {
      t = createDeleteTransaction();
      __deleteTransactions.put(key, t);
    }
    return t;
  }

  /**
   * Create or resolve the update Transaction that is bound to the given TransientRecord. This
   * Transaction will be automatically commited once the record is made persistent.
   */
  protected final Transaction bootUpdateTransactionFor(TransientRecord record)
  {
    int key = getKey(record);
    Transaction t = __updateTransactions.get(key);
    if (t == null) {
      t = __db.createTransaction(Db.DEFAULT_TIMEOUT,
                                 Transaction.TRANSACTION_UPDATE,
                                 __pwd,
                                 this.toString());
      __updateTransactions.put(key, t);
    }
    return t;
  }

  protected final Set<PersistentRecord> bootCreatedRecords(TransientRecord record)
  {
    int key = getKey(record);
    Set<PersistentRecord> records = __createdRecords.get(key);
    if (records == null) {
      records = new HashSet<PersistentRecord>();
      __createdRecords.put(key, records);
    }
    return records;
  }

  private void cancelTransaction(TransientRecord record,
                                 Int2ObjectMap<Transaction> transactions)
  {
    int id = getKey(record);
    Transaction t = transactions.get(id);
    if (t != null) {
      t.cancel();
      transactions.remove(id);
    }
  }

  private void rollback(TransientRecord record,
                        Gettable params)
      throws MirrorTransactionTimeoutException
  {
    beforeCancel(record, params);
    cancelTransaction(record, __deleteTransactions);
    cancelTransaction(record, __updateTransactions);
    Set<PersistentRecord> records = __createdRecords.get(getKey(record));
    if (records != null) {
      try (Transaction t = createDeleteTransaction()) {
        t.delete(records, Db.DEFAULT_TIMEOUT);
        t.commit();
      }
    }
    afterCancel(record, params);
  }

  /**
   * Cancel any associated update or delete Transactions and then invoke afterRollback.
   * 
   * @see #afterCancel(TransientRecord, Gettable)
   */
  @Override
  public final void rollbackPost(PersistentRecord record,
                                 Gettable params)
      throws MirrorException
  {
    rollback(record, params);
  }

  /**
   * Cancel any associated update or delete Transactions and then invoke postCancelRollback.
   * 
   * @see #afterCancel(TransientRecord, Gettable)
   */
  @Override
  public final void rollbackPre(TransientRecord record,
                                Gettable params)
      throws MirrorException
  {
    rollback(record, params);
  }

  /**
   * Method that is invokved before all the Transactions have been cancelled. Empty by default,
   * override to attach additional functionality.
   */
  protected void beforeCancel(TransientRecord record,
                              Gettable params)
  {
  }

  /**
   * Method that is invokved by rollback after all the Transactions have been cancelled. Empty by
   * default, override to attach additional functionality.
   */
  protected void afterCancel(TransientRecord record,
                             Gettable params)
  {
  }

  private void commitTransaction(TransientRecord record,
                                 Int2ObjectMap<Transaction> transactions)
      throws MirrorTransactionTimeoutException
  {
    int key = getKey(record);
    Transaction t = transactions.get(key);
    if (t != null) {
      t.commit();
      transactions.remove(key);
    }
  }

  private void commit(TransientRecord record,
                      Gettable params)
  {
    try {
      beforeCommit(record, params);
      commitTransaction(record, __deleteTransactions);
      commitTransaction(record, __updateTransactions);
      __createdRecords.remove(getKey(record));
      afterCommit(record, params);
    } catch (MirrorException mEx) {
      cancelTransaction(record, __deleteTransactions);
      cancelTransaction(record, __updateTransactions);
      throw new LogicException("Unexpected MirrorException", mEx);
    }
  }

  /**
   * Method that is invoked immediately before the commit process is started. Empty by default,
   * override to attach additional functionality.
   */
  protected void beforeCommit(TransientRecord record,
                              Gettable params)
  {
  }

  /**
   * Method that is invoked by release after all the Transactions have been committed. Empty by
   * default, override to attach additional functionality.
   */
  protected void afterCommit(TransientRecord record,
                             Gettable params)
  {
  }

  @Override
  public final void releasePost(PersistentRecord record,
                                Gettable params)
  {
    if (!__commitAsPre) {
      commit(record, params);
    }
  }

  /**
   * Commit any associated update or delete Transactions and then invoke postCommitRelease.
   * 
   * @see #afterCommit(TransientRecord, Gettable)
   * @throws LogicException
   *           If the Transactions have timed out during the makePersistent cycle (which is very
   *           unlikely)
   */
  @Override
  public final void releasePre(TransientRecord record,
                               Gettable params)
  {
    if (__commitAsPre) {
      commit(record, params);
    }
  }

  /**
   * Do nothing. Override this if an action is required.
   */
  @Override
  public void shutdown()
  {
  }

  /**
   * Empty implementation to be overridden if the class wants to do work after the record has been
   * created.
   */
  @Override
  public void triggerPost(PersistentRecord record,
                          Gettable params)
      throws MirrorException
  {
  }

  /**
   * Empty implementation to be overridden if the class wants to do work before the record has been
   * created. Note that the work will not become actually commited until after the records has been
   * created, but preparing the work before can provide useful clues about things.
   */
  @Override
  public void triggerPre(TransientRecord record,
                         Gettable params)
      throws MirrorException
  {
  }
}
