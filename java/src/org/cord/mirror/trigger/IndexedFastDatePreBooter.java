package org.cord.mirror.trigger;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

public class IndexedFastDatePreBooter
  extends PreInstantiationTriggerImpl
{
  private String _fieldName;

  public IndexedFastDatePreBooter(String fieldName)
  {
    _fieldName = fieldName;
  }

  @Override
  public void shutdown()
  {
    _fieldName = null;
  }

  @Override
  public void triggerPre(TransientRecord record,
                         Gettable params)
      throws MirrorFieldException
  {
    record.setField(_fieldName, record.get(_fieldName));
  }

  /**
   * @return true
   */
  public boolean triggerOnInstantiation()
  {
    return true;
  }
}
