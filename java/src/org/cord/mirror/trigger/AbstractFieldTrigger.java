package org.cord.mirror.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.util.LogicException;

/**
 * Utility class that means that people only need to implement one method -
 * triggerField(record,fieldName) to make a functioning FieldTrigger. The following methods must be
 * implemented by subclasses:-
 * <ul>
 * <li>public Object triggerField(record, fieldName)
 * </ul>
 * 
 * @see FieldTrigger#triggerField(TransientRecord,String)
 */
public abstract class AbstractFieldTrigger<T>
  implements FieldTrigger<T>
{
  private final FieldKey<T> __fieldName;

  private final boolean __handlesTransientRecords;

  private final boolean __triggerOnInstantiation;

  /**
   * @param fieldName
   *          The name of the field that this FieldTrigger should listen to. If null, then the value
   *          of FieldTrigger.ANY_FIELD is utilised to make it listen to all available triggers.
   */
  public AbstractFieldTrigger(FieldKey<T> fieldName,
                              boolean handlesTransientRecords,
                              boolean triggerOnInstantiation)
  {
    if (fieldName == null) {
      throw new LogicException("FieldTriggers no longer handle Global FieldTrigger actions");
    }
    __fieldName = fieldName;
    __handlesTransientRecords = handlesTransientRecords;
    __triggerOnInstantiation = triggerOnInstantiation;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public final FieldKey<T> getFieldName()
  {
    return __fieldName;
  }

  @Override
  public final boolean handlesTransientRecords()
  {
    return __handlesTransientRecords;
  }

  @Override
  public final boolean triggerOnInstantiation()
  {
    return __triggerOnInstantiation;
  }

  @Override
  public final Object triggerField(TransientRecord record,
                                   String fieldName)
      throws MirrorFieldException
  {
    if (!(triggerOnInstantiation() || handlesTransientRecords())) {
      throw new LogicException("Doesn't handle Transient records");
    } else {
      return triggerField(record, fieldName, null);
    }
  }

  @Override
  public final Object triggerField(WritableRecord record,
                                   String fieldName,
                                   Transaction transaction)
      throws MirrorFieldException
  {
    return triggerField((TransientRecord) record, fieldName, transaction);
  }

  /**
   * Implement this method to which will be invoked by the appropriate income trigger. This should
   * not be invoked directly, as it will be implemented by the implementation of the FieldTrigger's
   * triggerField(...) methods.
   * 
   * @param record
   *          Either a TransientRecord or a WritableRecord depending on status of transaction.
   * @param fieldName
   *          The name of the field that the trigger is being invoked upon.
   * @param transaction
   *          If record is writable then this will contains the Transaction controlling the record,
   *          otherwise record will be transient and transaction will be null.
   */
  protected abstract Object triggerField(TransientRecord record,
                                         String fieldName,
                                         Transaction transaction)
      throws MirrorFieldException;
}
