package org.cord.mirror.trigger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Strings;

/**
 * FieldTrigger that matches the String representation of a given field against a regular expression
 * and then substitutes the matching segments of the String with a constant replacement value. If
 * this replacement value is an empty String then this will have the effect of stripping out the
 * offending sections.
 */
public class RegexFieldTrigger
  extends AbstractFieldTrigger<String>
{
  private final Pattern __pattern;

  private final String __replacementValue;

  private final boolean __isRecursive;

  /**
   * @param searchPattern
   *          The regular expression that should match the segment of the field value that should be
   *          targetted by this trigger.
   * @param replacementValue
   *          The String that should replace the segments of the value that match the searchPattern
   *          regular expression. If this is null then it will be padded to empty String and will
   *          result in the matching patterns being stripped from the source value.
   * @param isRecursive
   *          If true then any proposed substitution resulting from the regex parsing will trigger a
   *          MirrorFieldException. This will result in the substituted value being resubmitted to
   *          all the FieldTriggers, including this one, a second time. If false, then this
   *          FieldTrigger will act as a pass-through FieldTrigger and will not throw an Exception
   *          but will pass the updated value onto the next FieldTrigger in the chain. The recursive
   *          facilities should be utilised with caution when you are substituting with a value that
   *          is itself matching the searchPattern, and is intended for a future refactoring that
   *          permits parametric subsitution values to be generated thereby making recursive
   *          resolution into a useful tool.
   */
  public RegexFieldTrigger(FieldKey<String> fieldName,
                           boolean handlesTransientRecords,
                           boolean triggerOnInstantiation,
                           String searchPattern,
                           String replacementValue,
                           boolean isRecursive)
  {
    super(fieldName,
          handlesTransientRecords,
          triggerOnInstantiation);
    __pattern = Pattern.compile(searchPattern);
    __replacementValue = Strings.nullToEmpty(replacementValue);
    __isRecursive = isRecursive;
  }

  /**
   * Static utility method that exposes the replacement functionality of the RegexFieldTrigger to
   * other classes.
   * 
   * @return The updated String if it has been altered, or null if the original value was
   *         sufficient.
   */
  public static String process(Pattern pattern,
                               String value,
                               String replacementValue)
  {
    Matcher matcher = pattern.matcher(value);
    if (!matcher.find()) {
      return null;
    }
    String substitutedValue = matcher.replaceAll(replacementValue);
    if (substitutedValue.equals(value)) {
      return null;
    }
    return substitutedValue;
  }

  @Override
  protected Object triggerField(TransientRecord record,
                                String fieldName,
                                Transaction transaction)
      throws MirrorFieldException
  {
    String value = record.opt(getFieldName());
    String substitutedValue = process(__pattern, value, __replacementValue);
    if (substitutedValue == null) {
      return null;
    }
    if (__isRecursive) {
      throw new MirrorFieldException("RegexFieldTrigger has updated values",
                                     null,
                                     record,
                                     getFieldName(),
                                     value,
                                     substitutedValue);
    } else {
      return substitutedValue;
    }
  }
}
