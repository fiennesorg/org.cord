package org.cord.mirror.trigger;

import java.util.HashMap;

import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.TransientRecord;
import org.cord.util.Assertions;
import org.cord.util.DelegatingGettable;
import org.cord.util.Gettable;
import org.cord.util.GettableMapWrapper;

/**
 * PreInstantiation trigger that rejects any record that doesn't contain an internal secret in the
 * Gettable initParams. The secret can be added to the initParams via the addInstantiationPermission
 * method and this should be invoked during the "approved" approach for creating records. Any other
 * attempt to create a record in the table will therefore fail thereby letting you preserve whatever
 * bootup process that you like in situations that doing all the work via PreInstantiation and
 * PostInstantiation triggers is not appropriate.
 * 
 * @author alex
 * @see #addInstantiationPermission(Gettable)
 */
public final class RestrictedInstantiation
  extends PreInstantiationTriggerImpl
{
  private final String __key = new Object().toString();

  private final Object __secret = new Object();

  private final String __message;

  private final Gettable __initParams;

  public RestrictedInstantiation(String message)
  {
    __message = Assertions.notEmpty(message, "message");
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put(__key, __secret);
    __initParams = new GettableMapWrapper(map);
  }

  @Override
  public void triggerPre(TransientRecord record,
                         Gettable params)
      throws MirrorException
  {
    if (params == null || params.get(__key) != __secret) {
      throw new MirrorInstantiationException(null, __message, record);
    }
  }

  /**
   * Decorate the given initParams via a DelegatingGettable so that it contains the appropriate
   * secret to permit it to be used to instantiate a record in the restricted Table.
   * 
   * @param initParams
   *          The bootup parameters that want to be decorated with the secret. If null then a
   *          Gettable that only contains the secret will be returned.
   * @return Either the decorated initParams or a constant Gettable containing the secret key
   */
  public Gettable addInstantiationPermission(Gettable initParams)
  {
    if (initParams == null) {
      return __initParams;
    }
    DelegatingGettable wrapper = new DelegatingGettable(false);
    wrapper.addGettable(__initParams);
    wrapper.addGettable(initParams);
    return initParams;
  }
}
