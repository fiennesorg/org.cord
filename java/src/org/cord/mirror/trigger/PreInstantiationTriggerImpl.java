package org.cord.mirror.trigger;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

/**
 * Partial implementation of PreInstantiationTrigger that gives default empty implementation for all
 * methods apart from trigger making it an ideal parent class for implementations without side
 * effects.
 * 
 * @author alex
 */
public abstract class PreInstantiationTriggerImpl
  implements PreInstantiationTrigger
{
  @Override
  public void rollbackPre(TransientRecord record,
                          Gettable params)
      throws MirrorException
  {
  }

  @Override
  public void releasePre(TransientRecord record,
                         Gettable params)
  {
  }

  @Override
  public void shutdown()
  {
  }
}
