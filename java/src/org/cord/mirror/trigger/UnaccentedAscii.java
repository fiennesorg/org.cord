package org.cord.mirror.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.StringUtils;

/**
 * FieldTrigger that normalizes unicode strings to only include non-accented ascii characters.
 * 
 * @see org.cord.util.StringUtils#removeAccents(String)
 */
public class UnaccentedAscii
  extends AbstractFieldTrigger<String>
{
  private final boolean __isRecursive;

  public UnaccentedAscii(FieldKey<String> fieldName,
                         boolean handlesTransientRecords,
                         boolean triggerOnInstantiation,
                         boolean isRecursive)
  {
    super(fieldName,
          handlesTransientRecords,
          triggerOnInstantiation);
    __isRecursive = isRecursive;
  }

  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    String value = record.opt(getFieldName());
    String strippedValue = StringUtils.removeAccents(value);
    if (value.equals(strippedValue)) {
      return null;
    }
    if (__isRecursive) {
      throw new MirrorFieldException("Stripped non-UnaccentedAscii",
                                     null,
                                     record,
                                     getFieldName(),
                                     value,
                                     strippedValue);
    } else {
      return strippedValue;
    }
  }
}
