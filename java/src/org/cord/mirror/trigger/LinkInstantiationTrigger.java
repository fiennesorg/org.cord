package org.cord.mirror.trigger;

import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNestedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.LinkField;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.Maps;

/**
 * Instantiation trigger that ensures that strong links between records are enforced in
 * un-initialised referenced fields.
 */
public class LinkInstantiationTrigger
  implements PreInstantiationTrigger
{
  private IntFieldKey __fieldName;

  private final RecordOperationKey<PersistentRecord> __linkName;

  private Db _db;

  private final long __timeout;

  private final Map<TransientRecord, PersistentRecord> __createdRecords =
      Maps.newConcurrentHashMap();

  private final boolean __autoCreateLink;

  private final boolean __autoLink;

  private final boolean __isOptional;

  private final String __transactionPassword;

  // private final int __defaultValue;
  private final LinkField __linkFieldFilter;

  public LinkInstantiationTrigger(LinkField linkFieldFilter,
                                  IntFieldKey fieldName,
                                  RecordOperationKey<PersistentRecord> linkName,
                                  Db db,
                                  String transactionPassword,
                                  long timeout,
                                  int linkStyle)
  {
    __linkFieldFilter = linkFieldFilter;
    __fieldName = fieldName;
    __linkName = linkName;
    _db = db;
    __transactionPassword = transactionPassword;
    __timeout = timeout;
    __autoCreateLink = (linkStyle & Dependencies.BIT_AUTO_CREATE) != 0;
    __autoLink = (linkStyle & Dependencies.BIT_AUTO_LINK) != 0;
    __isOptional = (linkStyle & Dependencies.BIT_OPTIONAL) != 0;
    // __defaultValue = defaultValue;
  }

  @Override
  public void shutdown()
  {
    __createdRecords.clear();
    _db = null;
  }

  @Override
  public String toString()
  {
    return "LinkInstantiationTrigger(" + "field:" + __fieldName + ",link:" + __linkName
           + (__autoCreateLink ? ",auto" : ",no-auto") + ")";
  }

  /**
   * @throws MirrorFieldException
   *           If the field in question refuses to permit a value of NEW_RECORD (in order to trigger
   *           the creation of a new record), or if the record is currently pointing to an explicit,
   *           yet illegal record id.
   */
  @Override
  public void triggerPre(TransientRecord record,
                         Gettable params)
      throws MirrorFieldException
  {
    synchronized (record) {
      int value = record.compInt(__fieldName);
      if (value == LinkFieldTrigger.NEW_RECORD) {
        if (__autoCreateLink) {
          record.setField(__fieldName, Integer.valueOf(LinkFieldTrigger.NEW_RECORD));
          PersistentRecord newRecord = null;
          newRecord = record.opt(__linkName);
          if (newRecord == null) {
            throw new LogicException("Failure to create linked record named " + __linkName + " on "
                                     + record);
          }
          __createdRecords.put(record, newRecord);
        } else if (__autoLink) {
          record.setField(__fieldName, Integer.valueOf(__linkFieldFilter.calculateDefaultValue()));
        } else {
          if (!__isOptional) {
            throw new MirrorFieldException(String.format("%s.%s has not been initialised and is not optional",
                                                         record,
                                                         __fieldName),
                                           null,
                                           record,
                                           __fieldName,
                                           Integer.valueOf(value),
                                           null);
          }
        }
      }
    }
  }

  @Override
  public void rollbackPre(TransientRecord record,
                          Gettable params)
      throws MirrorTransactionTimeoutException, MirrorNestedException
  {
    synchronized (record) {
      try {
        PersistentRecord newRecord = __createdRecords.get(record);
        if (newRecord != null) {
          Transaction transaction = null;
          transaction = _db.createTransaction(__timeout,
                                              Transaction.TRANSACTION_DELETE,
                                              __transactionPassword,
                                              "rollback of " + this);
          transaction.delete(newRecord, __timeout);
          MirrorNestedException mnEx = transaction.commit();
          if (mnEx != null) {
            throw mnEx;
          }
        }
      } finally {
        releasePre(record, null);
      }
    }
  }

  @Override
  public void releasePre(TransientRecord record,
                         Gettable params)
  {
    __createdRecords.remove(record);
  }
}
