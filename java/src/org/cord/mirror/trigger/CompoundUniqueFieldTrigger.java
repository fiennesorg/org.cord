package org.cord.mirror.trigger;

import java.util.ArrayList;
import java.util.Iterator;

import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorRuntimeException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RelatedMirrorFieldException;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.recordsource.RecordSources;

/**
 * FieldTrigger that ensures that a range of values in a record are unique when taken as a whole.
 */
public class CompoundUniqueFieldTrigger<T>
  extends AbstractFieldTrigger<T>
{
  private final ArrayList<Field<?>> __fields = new ArrayList<Field<?>>();

  private final FieldKey<?> __targetFieldName;

  private final Table __targetTable;

  private boolean _shouldCacheQuery = true;

  public CompoundUniqueFieldTrigger(Table targetTable,
                                    FieldKey<T> targetFieldName,
                                    boolean triggerWhileTransient)
  {
    super(targetFieldName,
          triggerWhileTransient,
          true);
    __targetTable = targetTable;
    __targetFieldName = targetFieldName;
    addField(__targetFieldName);
  }

  public void setShouldCacheQuery(boolean flag)
  {
    _shouldCacheQuery = flag;
  }

  @Override
  public String toString()
  {
    StringBuilder result = new StringBuilder();
    result.append("CompoundUniqueFieldTrigger(").append(__targetTable).append(".(");
    Iterator<Field<?>> fields = __fields.iterator();
    while (fields.hasNext()) {
      result.append(fields.next().getName());
      if (fields.hasNext()) {
        result.append(',');
      }
    }
    result.append("))");
    return result.toString();
  }

  /**
   * Add another target field to the compound unique trigger. The target Table will be queried to
   * check that it has a field by the specified name. Please note that the targetFieldName from the
   * constructor will have been automatically registered by the constructor.
   * 
   * @param fieldName
   *          The field that is to be added to the system.
   * @return true if the fieldName was new and has been added, false if it was already registered.
   * @throws IllegalArgumentException
   *           if the target Table does not have a FieldFilter named fieldName registered on it.
   * @throws NullPointerException
   *           if fieldName is null
   */
  public final CompoundUniqueFieldTrigger<T> addField(FieldKey<?> fieldName)
  {
    Field<?> field = __targetTable.getField(fieldName);
    if (field == null) {
      throw new IllegalArgumentException("No such Field named:" + fieldName);
    }
    if (!__fields.contains(field)) {
      __fields.add(field);
    }
    return this;
  }

  /**
   * @throws MirrorRuntimeException
   *           if there is more than one match for the so-called unique requirements
   */
  @Override
  public Object triggerField(TransientRecord triggeringRecord,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    StringBuilder buf = new StringBuilder();
    int count = __fields.size();
    Field<?> fieldFilter = __fields.get(0);
    buf.append(fieldFilter.getName()).append('=');
    fieldFilter.appendSqlValue(buf, triggeringRecord);
    for (int i = 1; i < count; i++) {
      buf.append(" and ");
      fieldFilter = __fields.get(i);
      buf.append(fieldFilter.getName()).append('=');
      fieldFilter.appendSqlValue(buf, triggeringRecord);
    }
    String filter = buf.toString();
    Query uniqueQuery = triggeringRecord.getTable().getQuery(filter, filter, Query.NOSQLORDERING);
    IdList uniqueRecordIds = uniqueQuery.getIdList(transaction);
    try {
      switch (uniqueRecordIds.size()) {
        case 0:
          return null;
        case 1:
          // try {
          if (triggeringRecord.getId() == uniqueRecordIds.getInt(0)) {
            return null;
          }
          // if
          // (triggeringRecord.getId().equals(uniqueQuery.getFirstRecordId()))
          // {
          // return null;
          // }
          PersistentRecord clashingRecord = uniqueQuery.getRecordList(transaction).get(0);
          throw new RelatedMirrorFieldException("Duplicate value in compound unique field: "
                                                + triggeringRecord + " clashes with "
                                                + clashingRecord + " using " + uniqueQuery,
                                                null,
                                                triggeringRecord,
                                                getFieldName(),
                                                clashingRecord);
          // } catch (MirrorNoSuchRecordException mnsrEx) {
          // return null;
          // }
        default:
          throw new MirrorRuntimeException(this + " fails on " + triggeringRecord + " with "
                                           + uniqueQuery,
                                           null);
      }
    } finally {
      if (!_shouldCacheQuery) {
        RecordSources.uncache(uniqueQuery);
      }
    }
  }
}
