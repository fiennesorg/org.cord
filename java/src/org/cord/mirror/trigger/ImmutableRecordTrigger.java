package org.cord.mirror.trigger;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;

/**
 * FieldTrigger that rejects any change to any field in the system once the record has been made
 * persistent thereby making it immutable.
 * 
 * @author alex
 */
public class ImmutableRecordTrigger
  extends AbstractFieldTrigger<Object>
{

  public ImmutableRecordTrigger()
  {
    super(ANY_FIELD,
          false,
          false);
  }

  @Override
  protected Object triggerField(TransientRecord record,
                                String fieldName,
                                Transaction transaction)
      throws MirrorFieldException
  {
    throw new MirrorFieldException("Immutable Record",
                                   null,
                                   record,
                                   fieldName,
                                   record.get(fieldName),
                                   null);
  }
}
