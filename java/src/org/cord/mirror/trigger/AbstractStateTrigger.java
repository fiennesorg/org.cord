package org.cord.mirror.trigger;

import org.cord.mirror.StateTrigger;

/**
 * Base class for implementing StateTriggers that provides the methodology for the StateTriggers to
 * describe how they would like to perform.
 */
public abstract class AbstractStateTrigger
  implements StateTrigger
{
  private final int _defaultState;

  private final boolean _isOverridableState;

  private final String _targetTableName;

  public AbstractStateTrigger(int defaultState,
                              boolean isOverridableState,
                              String targetTableName)
  {
    _defaultState = defaultState;
    _isOverridableState = isOverridableState;
    _targetTableName = targetTableName;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public final int getDefaultState()
  {
    return _defaultState;
  }

  @Override
  public final boolean isOverridableState()
  {
    return _isOverridableState;
  }

  @Override
  public final String getTargetTableName()
  {
    return _targetTableName;
  }
}
