package org.cord.mirror.trigger;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.util.Gettable;

/**
 * Minimal implementation of PostInstantiationTrigger that provides empty methods for everything
 * except the trigger method. This is sufficient for 99% of the cases.
 * 
 * @author alex
 */
public abstract class PostInstantiationTriggerImpl
  implements PostInstantiationTrigger
{
  @Override
  public void rollbackPost(PersistentRecord record,
                           Gettable params)
      throws MirrorException
  {
  }

  @Override
  public void releasePost(PersistentRecord record,
                          Gettable params)
  {
  }

  @Override
  public void shutdown()
  {
  }
}
