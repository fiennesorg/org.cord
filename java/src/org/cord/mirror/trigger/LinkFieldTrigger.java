package org.cord.mirror.trigger;

// import java.sql.*;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.util.Gettable;

/**
 * LinkTriggerOperation is a FieldTrigger that listens for changes to an integer field that is
 * pointing to another record and validates that the target record actually exists. This class will
 * be automatically created and registered on the appropriate table during the creation of a hard
 * link via makeLink(...).
 */
public class LinkFieldTrigger
  implements FieldTrigger<Integer>
{
  private final int __ignoreId;

  public final static int NEW_RECORD_INT = 0;

  /**
   * The value (0) that you should set the field to if you want a new target record to be created.
   * Note that this only works if this field filter has been declared with autoCreateTargetRecord as
   * true.
   */
  public final static int NEW_RECORD = NEW_RECORD_INT;

  private final IntFieldKey __sourceFieldName;

  private final int __sourceFieldIndex;

  private final boolean __isOptional;

  private final boolean __autoCreateTargetRecord;

  private final boolean __autoLink;

  private final Table __targetTable;

  private final Gettable __initParams;

  public LinkFieldTrigger(IntFieldKey sourceFieldName,
                          int sourceFieldIndex,
                          Table targetTable,
                          int linkStyle,
                          int ignoreId,
                          Gettable initParams)
  {
    __targetTable = targetTable;
    __sourceFieldName = sourceFieldName;
    __sourceFieldIndex = sourceFieldIndex;
    __isOptional = ((linkStyle & Dependencies.BIT_OPTIONAL) != 0);
    __autoCreateTargetRecord = ((linkStyle & Dependencies.BIT_AUTO_CREATE) != 0);
    __autoLink = ((linkStyle & Dependencies.BIT_AUTO_LINK) != 0);
    __ignoreId = ignoreId;
    __initParams = initParams;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String toString()
  {
    return "LinkTriggerOperation(targetTable:" + __targetTable.getName() + ",sourceField:"
           + __sourceFieldName + (__autoCreateTargetRecord ? ",auto" : ",no-auto") + ")";
  }

  /**
   * Get the id number of the record that is ignored by this link. If the trigger is activated on
   * this record then it will not validate the link. This is useful in tree style situations when
   * you want validated links, but you need a top level node that has a 'broken link'.
   * 
   * @return The id of the record to ignore. If the trigger doesn't ignore any records, then the
   *         value will normally be 0.
   */
  public final int getIgnoreId()
  {
    return __ignoreId;
  }

  public final boolean getAutoCreateTargetRecord()
  {
    return __autoCreateTargetRecord;
  }

  @Override
  public final FieldKey<Integer> getFieldName()
  {
    return __sourceFieldName;
  }

  /**
   * @return true
   */
  @Override
  public final boolean handlesTransientRecords()
  {
    return true;
  }

  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
      throws MirrorFieldException
  {
    return internalTrigger(record, null);
  }

  @Override
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    return internalTrigger(record, transaction);
  }

  public static final Object validate(TransientRecord record,
                                      Table targetTable,
                                      Transaction transaction,
                                      int ignoreId,
                                      IntFieldKey sourceFieldName,
                                      // TODO - ditch LinkFieldTrigger.validate - sourceFieldIndex
                                      int sourceFieldIndex,
                                      boolean isOptional,
                                      boolean autoLink,
                                      boolean autoCreateTargetRecord,
                                      Gettable initParams)
      throws MirrorFieldException
  {
    if (ignoreId > 0 && record.getId() == ignoreId) {
      return null;
    }
    final int currentValue = record.compInt(sourceFieldName);
    if (currentValue == ignoreId) {
      return null;
    }
    if (currentValue == NEW_RECORD_INT) {
      if (isOptional) {
        return null;
      }
      if (autoLink) {
        IdList allIds = targetTable.getIdList();
        if (allIds.size() == 0) {
          throw new MirrorFieldException("unable to auto-link record (no targets for "
                                         + sourceFieldName + ")",
                                         null,
                                         record,
                                         sourceFieldName,
                                         Integer.valueOf(currentValue),
                                         null);
        } else {
          throw new MirrorFieldException("auto-linked value",
                                         null,
                                         record,
                                         sourceFieldName,
                                         Integer.valueOf(currentValue),
                                         Integer.valueOf(allIds.getInt(0)));
        }
      }
      if (autoCreateTargetRecord) {
        TransientRecord newTargetRecord = targetTable.createTransientRecord();
        try {
          int newValue = newTargetRecord.makePersistent(initParams).getId();
          throw new MirrorFieldException("auto-built new record",
                                         null,
                                         record,
                                         sourceFieldName,
                                         Integer.valueOf(currentValue),
                                         Integer.valueOf(newValue));
        } catch (MirrorInstantiationException miEx) {
          throw new MirrorFieldException("Unable to create new record",
                                         miEx,
                                         record,
                                         sourceFieldName,
                                         Integer.valueOf(currentValue),
                                         null);
        }
      }
    }
    if (!targetTable.hasRecord(currentValue)) {
      throw new MirrorFieldException("Failed link: " + record + "." + sourceFieldName + "="
                                     + currentValue,
                                     null,
                                     record,
                                     sourceFieldName,
                                     Integer.valueOf(currentValue),
                                     null);
    }
    return null;
  }

  private Object internalTrigger(TransientRecord record,
                                 Transaction transaction)
      throws MirrorFieldException
  {
    return validate(record,
                    __targetTable,
                    transaction,
                    __ignoreId,
                    __sourceFieldName,
                    __sourceFieldIndex,
                    __isOptional,
                    __autoLink,
                    __autoCreateTargetRecord,
                    __initParams);
  }

  /**
   * @return true
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return true;
  }
}
