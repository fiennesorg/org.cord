package org.cord.mirror.trigger;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;

import com.google.common.base.Objects;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * StateTrigger that watches the result of invoking a RecordOperation and if the result changes when
 * a Transaction is committed then it invokes {@link #triggerUpdatedValue(PersistentRecord, Object)}
 * . The point at which this functionality is invoked (#STATE_TRANSACTION_COMMITTED) will be after
 * the SQL database has been updated and all of the triggers associated with that have been run.
 * 
 * @author alex
 * @param <T>
 */
public abstract class PostCommitRecordOperationTrigger<T>
  extends AbstractStateTrigger
{
  private final RecordOperationKey<T> __key;

  private final Int2ObjectMap<T> __id_previousValue =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<T>());

  public PostCommitRecordOperationTrigger(String targetTableName,
                                          RecordOperationKey<T> key)
  {
    super(STATE_WRITABLE_PREUPDATE,
          true,
          targetTableName);
    __key = checkNotNull(key);
  }

  public void register(Table table)
      throws MirrorTableLockedException
  {
    table.addStateTrigger(this, STATE_WRITABLE_PREUPDATE);
    table.addStateTrigger(this, STATE_TRANSACTION_COMMITTED);
  }

  @Override
  public void trigger(PersistentRecord persistentRecord,
                      Transaction transaction,
                      int state)
      throws Exception
  {
    switch (state) {
      case STATE_WRITABLE_PREUPDATE:
        __id_previousValue.put(persistentRecord.getId(), persistentRecord.opt(__key));
        break;
      case STATE_TRANSACTION_COMMITTED:
        if (__id_previousValue.containsKey(persistentRecord.getId())) {
          T previousValue = __id_previousValue.remove(persistentRecord.getId());
          if (!Objects.equal(persistentRecord.opt(__key), previousValue)) {
            triggerUpdatedValue(persistentRecord, previousValue);
          }
        }
        break;
    }
  }

  protected abstract void triggerUpdatedValue(PersistentRecord persistentRecord,
                                              T previousValue);
}
