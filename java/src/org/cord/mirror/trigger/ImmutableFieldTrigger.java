package org.cord.mirror.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;

/**
 * FieldTrigger that permits the creation of fields or tables that are immutable once instantiated.
 */
public class ImmutableFieldTrigger<T>
  implements FieldTrigger<T>
{
  public static <T> ImmutableFieldTrigger<T> create(FieldKey<T> fieldKey)
  {
    return new ImmutableFieldTrigger<T>(fieldKey);
  }

  private FieldKey<T> _fieldName;

  /**
   * Create an ImmutableFieldTrigger that locks the named field into a readonly state once the
   * record has been committed to the database.
   * 
   * @param fieldName
   *          The name of the field that is to be locked. Use ANY_FIELD if all the fields are to be
   *          readonly, thereby locking the complete table.
   * @see FieldTrigger#ANY_FIELD
   */
  public ImmutableFieldTrigger(FieldKey<T> fieldName)
  {
    _fieldName = fieldName;
  }

  @Override
  public void shutdown()
  {
    _fieldName = null;
  }

  @Override
  public final FieldKey<T> getFieldName()
  {
    return _fieldName;
  }

  /**
   * @return false because ImmutableFieldTrigger only functions on Instantiated records.
   */
  @Override
  public final boolean handlesTransientRecords()
  {
    return false;
  }

  /**
   * Do nothing as the ImmutableFieldTrigger doesn't operate on TransientRecord objects.
   */
  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
  {
    return null;
  }

  /**
   * Always throw a MirrorFieldException with no proposed value causing the set to fail.
   */
  @Override
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    throw new MirrorFieldException("Immutable Field",
                                   null,
                                   record,
                                   getFieldName(),
                                   record.opt(getFieldName()),
                                   null);
  }

  /**
   * @return "ImmutableFieldTrigger(fieldName)"
   */
  @Override
  public final String toString()
  {
    return "ImmutableFieldTrigger(" + getFieldName() + ")";
  }

  /**
   * @return false
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
