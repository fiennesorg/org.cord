package org.cord.mirror.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BitLinkField;

import it.unimi.dsi.fastutil.ints.IntListIterator;

/**
 * FieldTrigger that ensures that all the set bits in the BitLinkFieldFilter that this is watching
 * point to valid records in the target Table.
 * 
 * @see org.cord.mirror.field.BitLinkField
 */
public class BitLinkFieldTrigger
  implements FieldTrigger<Long>
{
  private final FieldKey<Long> __linkFieldName;

  private Table _targetTable;

  public BitLinkFieldTrigger(FieldKey<Long> linkFieldName,
                             Table targetTable)
  {
    __linkFieldName = linkFieldName;
    _targetTable = targetTable;
  }

  @Override
  public void shutdown()
  {
    _targetTable = null;
  }

  /**
   * @return linkFieldName from the constructor.
   */
  @Override
  public final FieldKey<Long> getFieldName()
  {
    return __linkFieldName;
  }

  /**
   * @return true
   */
  @Override
  public boolean handlesTransientRecords()
  {
    return true;
  }

  /**
   * @throws MirrorFieldException
   *           if any of the set flags do not have corresponding target records. The proposed
   *           replacement value will be the same as the original with the offending bit turned off.
   *           This means that it will get re-presented and passed (at least as far as that flag is
   *           concerned) the second time round.
   */
  @Override
  public Object triggerField(TransientRecord linkRecord,
                             String fieldName)
      throws MirrorFieldException
  {
    if (linkRecord.comp(__linkFieldName).longValue() > 0) {
      IntListIterator targetRecordIds =
          BitLinkField.getSetFlagIds(linkRecord, __linkFieldName).iterator();
      int id = 0;
      try {
        while (targetRecordIds.hasNext()) {
          id = targetRecordIds.nextInt();
          _targetTable.getRecord(id);
        }
      } catch (MirrorNoSuchRecordException badPositiveBitEx) {
        Long badValue = linkRecord.opt(__linkFieldName);
        Long correctedValue = new Long(BitLinkField.setFlag(badValue.longValue(), id, false));
        throw new MirrorFieldException("org.cord.mirror.trigger.BitLinkFieldTrigger",
                                       badPositiveBitEx,
                                       linkRecord,
                                       __linkFieldName,
                                       badValue,
                                       correctedValue);
      }
    }
    return null;
  }

  @Override
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    return triggerField(record, fieldName);
  }

  /**
   * @return false
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
