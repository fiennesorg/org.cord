package org.cord.mirror.trigger;

import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Query;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;

import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Field trigger that ensures that unique fields are maintained.
 */
public class UniqueFieldTrigger
  implements FieldTrigger<String>
{
  private final FieldKey<String> __fieldName;

  private final Field<String> __field;

  private final Table __table;

  public UniqueFieldTrigger(Table table,
                            FieldKey<String> fieldName)
  {
    __table = table;
    __fieldName = fieldName;
    __field = table.getField(fieldName);
  }

  @Override
  public void shutdown()
  {
  }

  /**
   * Create a query that checks that there are no existing persistent records that contain the same
   * value for the declared field and then verify that it contains no records.
   */
  @Override
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    return triggerField(record, fieldName);
  }

  /**
   * Create a query that checks that there are no existing persistent records that contain the same
   * value for the declared field and then verify that it contains no records.
   */
  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
      throws MirrorFieldException
  {
    StringBuilder buf = new StringBuilder(32);
    buf.append(__fieldName).append('=');
    __field.appendSqlValue(buf, record);
    String filter = buf.toString();
    Query findDuplicatesQuery = __table.getQuery(filter, filter, null);
    int targetId = record.getId();
    for (IntIterator i = findDuplicatesQuery.getIdList().iterator(); i.hasNext();) {
      int id = i.nextInt();
      if (id != targetId) {
        throw new MirrorFieldException(String.format("Duplicate value of \"%s\" in %s.%s",
                                                     __field.getFieldValue(record),
                                                     record,
                                                     __fieldName),
                                       null,
                                       record,
                                       __fieldName,
                                       record.opt(__fieldName),
                                       null);
      }
    }
    return null;
  }

  @Override
  public FieldKey<String> getFieldName()
  {
    return __fieldName;
  }

  /**
   * @return true
   */
  @Override
  public boolean handlesTransientRecords()
  {
    return true;
  }

  /**
   * @return true
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return true;
  }
}
