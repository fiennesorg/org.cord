package org.cord.mirror.trigger;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

/**
 * Implementation of a {@link StateTrigger#STATE_TRANSACTION_COMMITTED} and
 * {@link StateTrigger#STATE_TRANSACTION_CANCELLED} {@link StateTrigger} that checks to see whether
 * or not each {@link PersistentRecord} in the {@link Transaction} passes a given {@link Predicate}
 * and if so then it deletes it. This is intended primarily as a way of tidying up loose records
 * that utilise an isDefined or isActive flag to state whether or not they are in active use, and
 * therefore preserve transactional transparency with the ability to undo changes and restore us
 * back to the original state.
 * 
 * @author alex
 */
public class PostTransactionDeleteTrigger
  extends AbstractStateTrigger
{
  /**
   * Create a new instance and register it on the appropriate state triggers on the given Table.
   */
  public static PostTransactionDeleteTrigger register(Table table,
                                                      Predicate<PersistentRecord> isDeletable,
                                                      String pwd,
                                                      String transactionDetails)
      throws MirrorTableLockedException
  {
    PostTransactionDeleteTrigger instance = new PostTransactionDeleteTrigger(table.getTableName(),
                                                                             isDeletable,
                                                                             pwd,
                                                                             transactionDetails);
    table.addStateTrigger(instance, STATE_TRANSACTION_COMMITTED);
    table.addStateTrigger(instance, STATE_TRANSACTION_CANCELLED);
    return instance;
  }

  /**
   * Create a new instance with a Predicate that looks for a boolean field that is not defined, and
   * register it on the appropriate state triggers on the given Table. This will be the most
   * commonly required configuration and is the most concise usage pattern.
   * 
   * @param isDefined
   *          the FieldKey for the compulsory Boolean field that we want to delete records that have
   *          a value of false for this field.
   */
  public static PostTransactionDeleteTrigger register(Table table,
                                                      final FieldKey<Boolean> isDefined,
                                                      String pwd,
                                                      String transactionDetails)
      throws MirrorTableLockedException
  {
    return register(table, new Predicate<PersistentRecord>() {
      @Override
      public boolean apply(PersistentRecord input)
      {
        return !input.is(isDefined);
      }
    }, pwd, transactionDetails);
  }

  private final Predicate<PersistentRecord> __isDeletable;
  private final String __pwd;
  private final String __transactionDetails;

  private PostTransactionDeleteTrigger(String targetTableName,
                                       Predicate<PersistentRecord> isDeletable,
                                       String pwd,
                                       String transactionDetails)
  {
    super(StateTrigger.STATE_UNDEFINED,
          true,
          targetTableName);
    __isDeletable = Preconditions.checkNotNull(isDeletable, "isDeletable");
    __pwd = Preconditions.checkNotNull(pwd, "pwd");
    __transactionDetails = Preconditions.checkNotNull(transactionDetails, "transactionDetails");
  }

  @Override
  public void trigger(PersistentRecord record,
                      Transaction transaction,
                      int state)
      throws Exception
  {
    if (transaction.getType() == Transaction.TRANSACTION_UPDATE & __isDeletable.apply(record)) {
      try (Transaction t = record.getTable()
                                 .getDb()
                                 .createTransaction(Db.DEFAULT_TIMEOUT,
                                                    Transaction.TRANSACTION_DELETE,
                                                    __pwd,
                                                    __transactionDetails)) {
        t.delete(record, Db.DEFAULT_TIMEOUT);
        t.commit();
      }
    }
  }
}
