package org.cord.mirror.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.util.StringValidator;
import org.cord.util.StringValidatorException;

public class StringValidatorFieldTrigger
  implements FieldTrigger<String>
{
  private FieldKey<String> _fieldName;

  private StringValidator _stringValidator;

  public StringValidatorFieldTrigger(FieldKey<String> fieldName,
                                     StringValidator stringValidator)
  {
    _fieldName = fieldName;
    _stringValidator = stringValidator;
  }

  @Override
  public void shutdown()
  {
    _fieldName = null;
    _stringValidator = null;
  }

  @Override
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    return triggerField(record, fieldName);
  }

  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
      throws MirrorFieldException
  {
    try {
      _stringValidator.validate(record.opt(_fieldName).toString());
    } catch (StringValidatorException svEx) {
      throw new MirrorFieldException(svEx.getMessage(),
                                     svEx,
                                     record,
                                     _fieldName,
                                     record.opt(_fieldName),
                                     svEx.getProposedSubstitution());
    }
    return null;
  }

  /**
   * @return true
   */
  @Override
  public final boolean handlesTransientRecords()
  {
    return true;
  }

  @Override
  public final FieldKey<String> getFieldName()
  {
    return _fieldName;
  }

  public StringValidator getStringValidator()
  {
    return _stringValidator;
  }

  /**
   * @return "StringValidatorFieldTrigger(stringValidator@fieldName)"
   */
  @Override
  public String toString()
  {
    return "StringValidatorFieldTrigger(" + _stringValidator + "@" + _fieldName + ")";
  }

  /**
   * @return true
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return true;
  }
}
