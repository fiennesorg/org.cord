package org.cord.mirror.trigger;

import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.util.EnumerationWrapper;

/**
 * Unique field trigger that provides crude namespace support by giving additional field checks to
 * the unique clause.
 */
public class MultiUniqueFieldTrigger
  extends UniqueFieldTrigger
{
  private final HashSet<Field<?>> __fields = new HashSet<Field<?>>();

  public MultiUniqueFieldTrigger(Table table,
                                 FieldKey<String> targetField,
                                 Iterator<String> fieldNames)
  {
    super(table,
          targetField);
    while (fieldNames.hasNext()) {
      addFieldFilter(fieldNames.next(), table);
    }
    if (targetField != null) {
      addFieldFilter(targetField.getKeyName(), table);
    }
  }

  private final void addFieldFilter(String name,
                                    Table table)
  {
    Field<?> field = table.getField(name);
    if (field == null) {
      throw new IllegalArgumentException(String.format("Cannot resolve %s.%s", table, name));
    }
    __fields.add(table.getField(name));
  }

  protected String getWhereClause(TransientRecord record)
  {
    StringBuilder whereClause = new StringBuilder();
    Iterator<Field<?>> i = __fields.iterator();
    while (i.hasNext()) {
      Field<?> field = i.next();
      whereClause.append(field.getName()).append('=');
      field.appendSqlObjectValue(whereClause, record, record.get(field.getName()));
      if (i.hasNext()) {
        whereClause.append(" AND ");
      }
    }
    return whereClause.toString();
  }

  public final static String FIELD_SEPERATOR = ",";

  public final static MultiUniqueFieldTrigger getInstance(Table table,
                                                          FieldKey<String> targetField,
                                                          String fieldNamesString)
  {
    HashSet<String> fieldNames = new HashSet<String>();
    Iterator<Object> fieldNameList =
        new EnumerationWrapper<Object>(new StringTokenizer(fieldNamesString, FIELD_SEPERATOR));
    while (fieldNameList.hasNext()) {
      fieldNames.add((String) fieldNameList.next());
    }
    return new MultiUniqueFieldTrigger(table, targetField, fieldNames.iterator());
  }
}
