package org.cord.mirror;

/**
 * Base class of exceptions thrown by the Mirror system. All other custom exceptions extends from
 * this so it is easy to make catch all clauses.
 */
public class MirrorException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = -1383488810997248195L;

  public MirrorException(String s,
                         Exception nestedException)
  {
    super(s,
          nestedException);
  }
}
