package org.cord.mirror;

import org.cord.util.Shutdownable;

/**
 * FieldTrigger describes objects that wish to perform some kind of operation upon the updating of a
 * particular field in a record on a particular Table.
 */
public interface FieldTrigger<T>
  extends Shutdownable
{
  public final static FieldKey<Object> ANY_FIELD = null;

  /**
   * Inform the field trigger that the field has been changed during the course of a persistent
   * record update transaction. The record in question will contain the updated value of the field.
   * 
   * @param record
   *          The record that has had its field updated in.
   * @param fieldName
   *          The name of the field that has been updated and which has therefore triggered this
   *          trigger.
   * @param transaction
   *          The transaction that currently has the write lock on the writable record. This
   *          therefore bypasses the security that prevent untrusted clients on obtaining the
   *          transaction and therefore lets the field trigger have full access to the updated
   *          records in the transaction.
   * @return null if the value has been accepted, an updated value if the FieldFilter expects to
   *         operate as a pipe-line filter. This should only be the case when filters are incapable
   *         of distuinguishing between original and filtered versions of the data which would
   *         therefore result in a recusive loop if the correction was suggested via the
   *         MirrorFieldException.
   * @throws MirrorFieldException
   *           If the value of the record is not acceptable to the field trigger in some way. If
   *           this occurs, then the record will automatically roll back to the previous value of
   *           the field before passing on the exception to the calling client.
   */
  public Object triggerField(WritableRecord record,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException;

  /**
   * Inform the field trigger that the field has been changed during the initialisation phase of a
   * TransientRecord. The record will contain the updated value of the field. This method will only
   * get called if the field trigger in question returns true to handlesTransientRecords().
   * 
   * @param record
   *          The TransientRecord that has had it's value set.
   * @param fieldName
   *          The name of the field that has been updated and which has therefore triggered this
   *          trigger.
   * @throws MirrorFieldException
   *           If the field trigger rejects this updated value for the field.
   * @return null if the value has been accepted, an updated value if the FieldFilter expects to
   *         operate as a pipe-line filter. This should only be the case when filters are incapable
   *         of distuinguishing between original and filtered versions of the data which would
   *         therefore result in a recusive loop if the correction was suggested via the
   *         MirrorFieldException.
   */
  public Object triggerField(TransientRecord record,
                             String fieldName)
      throws MirrorFieldException;

  /**
   * Get the name of the field to which this field trigger wants to be attached. This is used by
   * Table when registering the field trigger.
   * 
   * @return The name of the field.
   */
  public FieldKey<T> getFieldName();

  /**
   * Request the field trigger to state whether or not it wishes to be invoked on transient records
   * which don't yet have a persistent backend.
   * 
   * @return True if the trigger wants to be evoked on <i>both</i> transient and writable records,
   *         false if only on writable records.
   */
  public boolean handlesTransientRecords();

  /**
   * Check to see if the FieldTrigger wishes to be invoked across uninitialised fields in a
   * TransientRecord immediately before the insertion into the database. This enables the
   * FieldTrigger to guarantee that any PersistentRecord will conform to any filters that have been
   * defined. Note that this will get invoked even if the handlesTransientRecords() method returns
   * false, but the record that is passed to the FieldTrigger will still be a TransientRecord.
   * 
   * @return true if the triggerField(...) method is to be invoked before the Object is to be made
   *         Persistent.
   */
  public boolean triggerOnInstantiation();
}
