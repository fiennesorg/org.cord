package org.cord.mirror;

import java.util.Collection;
import java.util.Set;

import com.google.common.base.Preconditions;

/**
 * A wrapper around another Viewpoint that only exposes the Viewpoint interface to the world at
 * large. This permits you to use implementations of Viewpoint that contain other functionality that
 * you don't want people to utilise (even if they "guess" and cast your Viewpoint to the class that
 * they think is). You just make sure that when you pass it to any functionality that needs the
 * public API interface that they get a ViewpointWrapper rather than the potentially dangerous
 * original implementation of Viewpoint.
 * 
 * @author alex
 */
public class ViewpointWrapper
  implements Viewpoint
{
  private final Viewpoint __viewpoint;

  public ViewpointWrapper(Viewpoint viewpoint)
  {
    __viewpoint = Preconditions.checkNotNull(viewpoint, "viewpoint");
  }

  @Override
  public int getType()
  {
    return __viewpoint.getType();
  }

  @Override
  public boolean isUpdated()
  {
    return __viewpoint.isUpdated();
  }

  @Override
  public boolean isUpdatedTable(Table table)
  {
    return __viewpoint.isUpdatedTable(table);
  }

  @Override
  public Set<Table> getUpdatedTables()
  {
    return __viewpoint.getUpdatedTables();
  }

  @Override
  public boolean contains(PersistentRecord record)
  {
    return __viewpoint.contains(record);
  }

  @Override
  public boolean contains(Table table,
                          int id)
  {
    return __viewpoint.contains(table, id);
  }

  @Override
  public int getRecordCount()
  {
    return __viewpoint.getRecordCount();
  }

  @Override
  public int getRecordCount(Table table)
  {
    return __viewpoint.getRecordCount(table);
  }

  @Override
  public Collection<PersistentRecord> getRecords(Table table)
  {
    return __viewpoint.getRecords(table);
  }

  @Override
  public Set<Table> getTables()
  {
    return __viewpoint.getTables();
  }

  @Override
  public String debug()
  {
    return __viewpoint.debug();
  }

  @Override
  public PersistentRecord getRecord(Table table,
                                    Integer id)
      throws MirrorNoSuchRecordException
  {
    return __viewpoint.getRecord(table, id);
  }

  @Override
  public PersistentRecord getRecord(PersistentRecord record)
  {
    return __viewpoint.getRecord(record);
  }
}
