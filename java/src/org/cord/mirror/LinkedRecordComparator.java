package org.cord.mirror;

import java.util.Comparator;

import com.google.common.base.Preconditions;

public class LinkedRecordComparator
  implements Comparator<TransientRecord>
{
  private final RecordOperationKey<? extends TransientRecord> __linkKey;
  private final Comparator<TransientRecord> __linkOrdering;

  public LinkedRecordComparator(RecordOperationKey<? extends TransientRecord> linkKey,
                                Comparator<TransientRecord> linkOrdering)
  {
    __linkKey = Preconditions.checkNotNull(linkKey, "linkKey");
    __linkOrdering = Preconditions.checkNotNull(linkOrdering, "linkOrdering");
  }

  @Override
  public int compare(TransientRecord arg0,
                     TransientRecord arg1)
  {
    return __linkOrdering.compare(arg0.opt(__linkKey), arg1.opt(__linkKey));
  }

}
