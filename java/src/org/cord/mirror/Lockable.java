package org.cord.mirror;

/**
 * Lockable indicates that an Object wants to be informed that the database is ready for use.
 * 
 * @author alex
 */
public interface Lockable
{
  public void lock();
}
