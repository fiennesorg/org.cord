package org.cord.mirror;

import java.util.Comparator;

import org.cord.util.ObjectUtil;

/**
 * A PersistentRecord Comparator that orders according to the natural ordering of the result of
 * invoking a named RecordOperation on the records. The type of the value that the RecordOperation
 * is not specified, but providing it is comparable to the result of invoking the same
 * RecordOperation on both the records being compared then it will order accordingly.
 * 
 * @author alex
 */
public class RecordOperationValueComparator<V>
  implements Comparator<TransientRecord>
{
  /**
   * Create a new instance which will be a {@link FieldValueComparator} if possible or a
   * {@link RecordOperationValueComparator} if the recordOperationName doesn't resolve to a Field.
   */
  public static <V extends Comparable<V>> Comparator<TransientRecord> newInstance(RecordOperationKey<V> recordOperationKey)
  {
    if (recordOperationKey instanceof FieldKey<?>) {
      return new FieldValueComparator((FieldKey<?>) recordOperationKey);
    }
    return new RecordOperationValueComparator<V>(recordOperationKey);
  }

  private final RecordOperationKey<?> __recordOperationName;

  /**
   * @param recordOperationName
   *          The RecordOperation that is to be invoked on the PersistentRecords that are to be
   *          ordered. This must resolve to a result that implements Comparable<Object>.
   */
  public RecordOperationValueComparator(RecordOperationKey<V> recordOperationName)
  {
    __recordOperationName = recordOperationName;
  }

  /**
   * Compare the value resulted in invoking the RecordOperation on both records in a
   * case-insensitive manner if the value is a String.
   */
  @Override
  public int compare(TransientRecord o1,
                     TransientRecord o2)
  {
    return ObjectUtil.compareIgnoreCase(o1.opt(__recordOperationName),
                                        o2.opt(__recordOperationName));
  }

  @Override
  public String toString()
  {
    return "RecordOperationValueComparator(" + __recordOperationName + ")";
  }
}
