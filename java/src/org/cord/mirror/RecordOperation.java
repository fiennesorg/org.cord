package org.cord.mirror;

import org.cord.util.Shutdownable;

/**
 * Interface that describes objects that are capable of performing an operation on a specific
 * Record.
 * 
 * @param <V>
 *          The class of the Objects that invoking this RecordOperation will return.
 */
public interface RecordOperation<V>
  extends LockableOperation, Shutdownable
{
  /**
   * Request the RecordOperation to perform it's task on the given Record and then return the result
   * of the operation from the viewpoint of a particular transaction.
   * 
   * @param targetRecord
   *          The record that is going to be targetted by this operation.
   * @param viewpoint
   *          The viewpoint that may be used to filter the return results to include updated records
   *          if necessary.
   * @return The result of the operation (if any). If there is an error in performing the operation,
   *         then null should be returned.
   */
  public V getOperation(TransientRecord targetRecord,
                        Viewpoint viewpoint);

  public RecordOperationKey<V> getKey();
}
