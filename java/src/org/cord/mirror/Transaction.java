package org.cord.mirror;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cord.node.search.AdvancedSearch;
import org.cord.sql.ConnectionFacade;
import org.cord.util.DebugConstants;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.collect.Maps;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Represents a single atomic operation on a number of records. The storage of Transaction objects
 * is permitted because no sensitive objects are stored inside the transaction. All records are
 * stored as references via id rather than as the records themselves.
 */
public class Transaction
  extends NamedImpl
  implements Viewpoint, AutoCloseable
{
  /**
   * Invoke {@link #cancel()} and print out any resulting {@link MirrorNestedException} to
   * {@link System#err}. This will have no effect if the Transaction has previously been committed.
   */
  @Override
  public void close()
  {
    MirrorNestedException mne = cancel();
    if (mne != null) {
      System.err.println(mne);
    }
  }

  public final static Transaction NULL = null;

  /**
   * Transaction style to denote that all records held inside this transaction are going to be
   * updated when the transaction is committed.
   */
  public final static int TRANSACTION_UPDATE = 0;

  /**
   * Transaction style to denote that all records held inside this transaction are going to be
   * deleted when the transaction is committed.
   */
  public final static int TRANSACTION_DELETE = 1;

  /**
   * Slight cop-out to provide an index for notifyAllListeners to inform them that there is a new
   * record inserted into a specific table. This shouldn't really be here, and will be moved later
   * but for now it is the most logical place to have it.
   */
  public final static int TRANSACTION_INSERT = 2;

  // 2DO # tidy up insertion transactions.
  /**
   * Another slight cop-out to provide the concept of a null transaction that represents no
   * transaction at all. This can then be dropped into webmacro templates without causing null
   * problems. However, all Transactions with this state should be taken as being equivalent to null
   * whereever possible.
   */
  public final static int TRANSACTION_NULL = 3;

  /**
   * Debugging string holding readable values of the types of transactions.
   * 
   * @see #TRANSACTION_UPDATE
   * @see #TRANSACTION_DELETE
   * @see #TRANSACTION_INSERT
   * @see #TRANSACTION_NULL
   */
  public final static List<String> TRANSACTION_TYPES =
      Collections.unmodifiableList(Arrays.asList(new String[] { "Update", "Delete", "Insert",
          "Null" }));

  /**
   * Utility method that transforms Transactions of type TRANSACTION_NULL into null values and
   * passes through all other Transactions untouched.
   * 
   * @param viewpoint
   *          The transaction to be filtered
   * @return The original transaction or null if the original transaction was null or was of type
   *         TRANSACTION_NULL.
   * @see #TRANSACTION_NULL
   */
  public final static <T extends Viewpoint> T stripNullTransaction(T viewpoint)
  {
    return (viewpoint != null && viewpoint.getType() != TRANSACTION_NULL) ? viewpoint : null;
  }

  // time this Transaction was completed
  private long _transactionCreationTime;

  // milliseconds that this Transaction remains valid
  private final long __timeoutPeriod;

  // Db owning this Transaction
  private final Db __db;

  private final Map<Table, Int2ObjectMap<PersistentRecord>> __lockedRecords = Maps.newHashMap();

  private final Set<Table> __updatedTables = new HashSet<Table>();

  protected void addUpdatedTable(Table table)
  {
    __updatedTables.add(table);
  }

  @Override
  public boolean isUpdatedTable(Table table)
  {
    return __updatedTables.contains(table);
  }

  @Override
  public Set<Table> getUpdatedTables()
  {
    return Collections.unmodifiableSet(__updatedTables);
  }

  private int _recordCount = 0;

  public enum State {
    ACTIVE,
    COMMITTED,
    CANCELLED
  }

  private State _state;

  // TRANSACTION_UPDATE or TRANSACTION_DELETE
  private int _style;

  private final String __details;

  protected Transaction(String name,
                        Db db,
                        int style,
                        long creationTime,
                        long timeoutPeriod,
                        String details)
  {
    super(name);
    _transactionCreationTime = creationTime;
    __timeoutPeriod = timeoutPeriod;
    __db = db;
    _style = style;
    _state = State.ACTIVE;
    __details = details;
  }

  /**
   * Reset the creation time of the transaction to now, thereby providing it with the full
   * timeoutPeriod again before the transaction becomes invalid. The time is delivered by the
   * Db.currentTimeMillis() function.
   * 
   * @throws MirrorTransactionTimeoutException
   *           If the transaction has already timed out or been committed. It is not possible to
   *           'reincarnate' a dead transaction.
   */
  public void resetCreationTime()
      throws MirrorTransactionTimeoutException
  {
    checkValidity();
    _transactionCreationTime = __db.currentTimeMillis();
  }

  @Override
  public String debug()
  {
    StringBuilder result = new StringBuilder();
    result.append("Transaction(")
          .append(getName())
          .append(":")
          .append(TRANSACTION_TYPES.get(getType()))
          .append(":");
    Iterator<Table> tables = getTables().iterator();
    while (tables.hasNext()) {
      Table table = tables.next();
      result.append(table.getName()).append("(");
      IntIterator ids = getTableRecords(table, false).keySet().iterator();
      while (ids.hasNext()) {
        result.append(ids.nextInt());
        if (ids.hasNext()) {
          result.append(", ");
        }
      }
      result.append(")");
      if (tables.hasNext()) {
        result.append(", ");
      }
    }
    result.append(")");
    return result.toString();
  }

  /**
   * Create a String representation of this Transaction that includes the number of records locked
   * in each table. This is a good overview of what is going to happen in a relatively compact
   * format which is useful for larger Transactions where {@link #debug()} would be far too verbose.
   */
  public String toStringRecordCounts()
  {
    ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
    for (Table table : getTables()) {
      toStringHelper.add(table.getName(), getTableRecords(table, false).size());
    }
    return toStringHelper.toString();
  }

  public String getDetails()
  {
    return __details;
  }

  @Override
  public String toString()
  {
    return String.format("Transaction(%s: %s, %s, %s, %sms)",
                         getName(),
                         __details,
                         TRANSACTION_TYPES.get(getType()),
                         getState(),
                         Long.toString(System.currentTimeMillis() - _transactionCreationTime));
  }

  /**
   * Get the number of records that are locked by this Transaction.
   * 
   * @return The number of locked records (>= 0)!
   */
  @Override
  public int getRecordCount()
  {
    return _recordCount;
  }

  /**
   * Get the number of records that are locked by this Transaction in a particular Table.
   * 
   * @param table
   *          The Table that we are interested in.
   * @return The number of locked records in that Table (>=0).
   */
  @Override
  public int getRecordCount(Table table)
  {
    Int2ObjectMap<PersistentRecord> mapset = getTableRecords(table, false);
    return mapset == null ? 0 : mapset.size();
  }

  /**
   * Get the current {@link State} of this Transaction.
   * 
   * @return current status of this Transaction, never null.
   */
  public final State getState()
  {
    return _state;
  }

  private Int2ObjectMap<PersistentRecord> getTableRecords(Table table,
                                                          boolean shouldAutocreate)
  {
    Int2ObjectMap<PersistentRecord> mapset = __lockedRecords.get(table);
    if (mapset == null && shouldAutocreate) {
      mapset = new Int2ObjectOpenHashMap<PersistentRecord>();
      __lockedRecords.put(table, mapset);
    }
    return mapset;
  }

  /**
   * Get a Collection of the PersistentRecord from the specified Table that this Transaction has a
   * lock on.
   * 
   * @param table
   *          The table for whom we want the id list.
   * @return The appropriate immutable collection of Records. This will never be null, but may be
   *         empty.
   */
  @Override
  public final Collection<PersistentRecord> getRecords(Table table)
  {
    Int2ObjectMap<PersistentRecord> records = getTableRecords(table, false);
    if (records == null) {
      return Collections.emptySet();
    }
    return Collections.unmodifiableCollection(records.values());
  }

  /**
   * Return a list of all the records that are locked by this Transaction. The records will be
   * returned from the viewpoint of this Transaction so Update Transactions will return a list of
   * WritableRecords.
   * 
   * @param tableName
   *          The name of the Table from which the Records should belong. If the Transaction has no
   *          locks on Records from this Table then an empty collection will be returned.
   * @see PersistentRecord
   */
  public final Collection<PersistentRecord> getRecords(String tableName)
  {
    return getRecords(__db.getTable(tableName));
  }

  /**
   * Get a list of all Tables that one or more records that are included in this Transaction. Each
   * Table will only appear once in the list and the ordering on the list is unspecified.
   * 
   * @return The locked Tables, never null
   */
  @Override
  public final Set<Table> getTables()
  {
    return Collections.unmodifiableMap(__lockedRecords).keySet();
  }

  /**
   * Check to see if the Transaction currently has the lock on a specific record.
   * 
   * @param record
   *          The record to check for.
   * @return true if the record is currently in the locked records data structure, or null if the
   *         record is either null or not locked.
   */
  @Override
  public final boolean contains(PersistentRecord record)
  {
    return record == null ? false : contains(record.getTable(), record.getId());
  }

  /**
   * Check to see if the Transaction currently has the lock on a record specified by its id number.
   * 
   * @param table
   *          The table containing the targetted record.
   * @param id
   *          The id of the targetted record
   * @return true if this Transaction currently has the lock on the targetted record.
   */
  @Override
  public final boolean contains(Table table,
                                int id)
  {
    Int2ObjectMap<PersistentRecord> records = getTableRecords(table, false);
    return records == null ? false : records.containsKey(id);
  }

  /**
   * Get the timeout for the lifespan of this Transaction.
   * 
   * @return The maximum functioning age of this Transaction in milliseconds.
   */
  public long getTimeout()
  {
    return __timeoutPeriod;
  }

  /**
   * Get the number of milliseconds that have elapsed since the Transaction was created or the
   * resetCreationTime() was last called. This may then be utilised to calculate the vitality of the
   * Transaction and therefore the likelyhood that it will expire in the near future.
   * 
   * @return Age of the Transaction in milliseconds.
   * @see #resetCreationTime()
   */
  public final long getTransactionAge()
  {
    return getDb().currentTimeMillis() - _transactionCreationTime;
  }

  /**
   * Get the integer flag that denotes the style of this Transaction. This will either be one of
   * TRANSACTION_DELETE or TRANSACTION_UPDATE depending on how the Transaction was initialised.
   * 
   * @return The style of this Transaction
   * @see #TRANSACTION_DELETE
   * @see #TRANSACTION_UPDATE
   */
  @Override
  public int getType()
  {
    return _style;
  }

  /**
   * Get the Db that this Transaction is registered with.
   * 
   * @return The parent Db.
   */
  public final Db getDb()
  {
    return __db;
  }

  /**
   * Check to see if this Transaction is still valid. The properties required for a valid
   * Transaction are:-
   * <ul>
   * <li>The Transaction is still within its valid Timeout period.
   * <li>The Transaction hasn't been committed or cancelled.
   * </ul>
   * If the Transaction is no longer valid then it will be automatically cancelled. This will cancel
   * any write locks that are currently owned on records, and remove the Transaction from the list
   * of currently executing Transactions on the Db.
   * 
   * @return This transaction if the transaction is still valid.
   * @throws MirrorTransactionTimeoutException
   *           If the Transaction has timed out, or has already been committed or cancelled.
   */
  public final Transaction checkValidity()
      throws MirrorTransactionTimeoutException
  {
    switch (_state) {
      case COMMITTED:
        throw new MirrorTransactionTimeoutException("Transaction committed",
                                                    this,
                                                    null,
                                                    0,
                                                    null,
                                                    null);
      case CANCELLED:
        throw new MirrorTransactionTimeoutException("Transaction cancelled",
                                                    this,
                                                    null,
                                                    0,
                                                    null,
                                                    null);
      default:
        if (getTransactionAge() > __timeoutPeriod) {
          cancel();
          throw new MirrorTransactionTimeoutException(this + " transaction expired ("
                                                      + getTransactionAge() + ">" + __timeoutPeriod,
                                                      this,
                                                      null,
                                                      0,
                                                      null,
                                                      null);
        }
        return this;
    }
  }

  /**
   * Quick check to see if a Transaction is still valid, ie isn't committed or cancelled or timed
   * out that doesn't throw any exceptions. Calling this method doesn't affect the timeout status of
   * the Transaction.
   * 
   * @return true if the Transaction is still valid.
   */
  public boolean isValid()
  {
    switch (_state) {
      case COMMITTED:
      case CANCELLED:
        return false;
      default:
        return getTransactionAge() < __timeoutPeriod;
    }
  }

  /**
   * Tell this Transaction that it should perform whatever accumulated actions have been defined in
   * it. This is performed by:-
   * <ul>
   * <li>Remove this Transaction from the register of active Transactions on the Db. Once a commit
   * begins, it cannot be stopped...
   * <li>Checking that the Transaction is still valid. Invalid Transactions throw
   * MirrorTransactionTimeoutExceptions.
   * <li>Lock all the associated Tables that are related to all the records that are currently
   * contained in the transaction. We are now guaranteed that no new Querys will be generated until
   * the Transaction is complete.
   * <li>Call commitTransaction(this) on every PersistentRecord held in this Transaction, thereby
   * forcing the database to be updated.
   * <li>Release the lock on all the Tables Once this method returns, the Transaction will be in its
   * completed state and all future calls to the Transaction and any WritableRecords associated with
   * it will fail.
   * </ul>
   * 
   * @throws MirrorTransactionTimeoutException
   *           If the transaction has timed out before the commit() method was called. In this case,
   *           it will have released all the records in the same fashion as cancel() does.
   * @return a MirrorNestedException ff any of the triggers that are registered on the Table that
   *         are invoked during the course of the commit have thrown Exceptions of any type, then
   *         they will all be wrapped up in this compound Exception. This is, however, a bad case
   *         and it generally means that things are in an unstable position. If everything has been
   *         OK, then return null.
   * @see #checkValidity()
   */
  public final MirrorNestedException commit()
      throws MirrorTransactionTimeoutException
  {
    checkValidity();
    if (_state != State.ACTIVE) {
      return null;
    }
    ConnectionFacade connectionFacade = getDb().getThreadConnectionFacade().get();
    Connection connection = null;
    Statement statement = null;
    try {
      connection = connectionFacade.checkOutConnection();
      statement = connection.createStatement();
      // TODO: (#249) decide what is happening to these nested commit errors
      MirrorNestedException mnEx = commit(statement);
      if (mnEx != null) {
        System.err.println(mnEx);
      }
      return mnEx;
    } catch (Exception exception) {
      exception.printStackTrace();
      throw new MirrorLogicException("Unexpected Exception during Transaction.commit()", exception);
    } finally {
      if (statement != null) {
        try {
          statement.close();
        } catch (SQLException sqlEx) {
          System.err.println(this + ".commit() failed to close Statement --> " + sqlEx);
          connectionFacade.checkInBrokenConnection(connection);
          connection = null;
        }
      }
      connectionFacade.checkInWorkingConnection(connection);
    }
  }

  private MirrorNestedException triggerStateTriggers(int stateTriggerType,
                                                     Map<Table, Int2ObjectMap<PersistentRecord>> tableToRecordSet,
                                                     MirrorNestedException mirrorNestedException)
      throws MirrorTransactionOwnershipException
  {
    for (Table table : tableToRecordSet.keySet()) {
      for (PersistentRecord record : tableToRecordSet.get(table).values()) {
        WritableRecord writableRecord = null;
        try {
          switch (stateTriggerType) {
            case StateTrigger.STATE_PREDELETED_DELETED:
              table.notifyTableListeners(record, getType());
              table.triggerStateTriggers(record, this, stateTriggerType);
              record.finaliseTransaction();
              record.setIsDeleted();
              break;
            case StateTrigger.STATE_WRITABLE_PREUPDATE:
              writableRecord = record.getWritableRecord(this);
              // if (writableRecord.getIsUpdated()) {
              table.triggerStateTriggers(record, this, stateTriggerType);
              // }
              break;
            case StateTrigger.STATE_WRITABLE_UPDATED:
              writableRecord = record.getWritableRecord(this);
              if (writableRecord.getIsUpdated()) {
                Object[] objectValues = writableRecord.getObjectValues();
                int[] intValues = writableRecord.getIntValues();
                for (RecordStateDataFilter writableDataFilter : table.getUnmodifiableRecordStateDataFilters()) {
                  writableDataFilter.writableToPersistent(writableRecord, objectValues, intValues);
                }
                record.setFields(objectValues, intValues);
                table.notifyTableListeners(record, getType());
                table.triggerStateTriggers(record, this, stateTriggerType);
              }
              record.finaliseTransaction();
              break;
            case StateTrigger.STATE_TRANSACTION_COMMITTED:
              table.triggerStateTriggers(record, this, stateTriggerType);
              if (getType() == TRANSACTION_DELETE) {
                table.uncacheRecord(record);
              }
              break;
            case StateTrigger.STATE_TRANSACTION_CANCELLED:
              table.triggerStateTriggers(record, this, stateTriggerType);
              break;
            default:
              throw new LogicException("wierdy stateTriggerType: "
                                       + StateTrigger.STATES.get(stateTriggerType));
          }
        } catch (MirrorRecordNestedException mrnEx) {
          mirrorNestedException =
              MirrorNestedException.getInstance(mirrorNestedException, "stateTrigger", mrnEx);
        }
      }
    }
    return mirrorNestedException;
  }

  private boolean executeUpdateSql(Statement statement,
                                   Map<Table, Int2ObjectMap<PersistentRecord>> tableToRecordSet)
      throws SQLException, MirrorTransactionOwnershipException
  {
    boolean hasChanged = false;
    for (Table table : tableToRecordSet.keySet()) {
      int changeCount = 0;
      long time = 0;
      if (DebugConstants.STATS_ENABLED) {
        System.currentTimeMillis();
      }
      for (PersistentRecord record : tableToRecordSet.get(table).values()) {
        boolean recordHasChanged = record.getWritableRecord(this).performUpdate(statement);
        if (recordHasChanged) {
          changeCount++;
        }
      }
      if (changeCount > 0) {
        hasChanged = true;
        if (getDb().flushQueryCaches()) {
          table.invalidateCachedRecordSources();
        }
        if (DebugConstants.STATS_ENABLED) {
          table.updateUpdateStats(changeCount, System.currentTimeMillis() - time);
        }
      }
    }
    return hasChanged;
  }

  protected MirrorNestedException commit(Statement statement)
      throws SQLException, MirrorTransactionOwnershipException, MirrorNoSuchRecordException
  {
    __db.removeTransaction(this);
    MirrorNestedException mirrorNestedException = null;
    switch (getType()) {
      case TRANSACTION_UPDATE:
        mirrorNestedException = triggerStateTriggers(StateTrigger.STATE_WRITABLE_PREUPDATE,
                                                     __lockedRecords,
                                                     mirrorNestedException);
        _isUpdated = Boolean.valueOf(executeUpdateSql(statement, __lockedRecords));
        _state = State.COMMITTED;
        mirrorNestedException = triggerStateTriggers(StateTrigger.STATE_WRITABLE_UPDATED,
                                                     __lockedRecords,
                                                     mirrorNestedException);
        break;
      case TRANSACTION_DELETE:
        executeDeleteSql(statement);
        _state = State.COMMITTED;
        mirrorNestedException = triggerStateTriggers(StateTrigger.STATE_PREDELETED_DELETED,
                                                     __lockedRecords,
                                                     mirrorNestedException);
        break;
      default:
        throw new MirrorLogicException("Unexpected Transaction state of " + getType());
    }
    mirrorNestedException = triggerStateTriggers(StateTrigger.STATE_TRANSACTION_COMMITTED,
                                                 __lockedRecords,
                                                 mirrorNestedException);
    return mirrorNestedException;
  }

  private Boolean _isUpdated = null;

  @Override
  public boolean isUpdated()
  {
    if (_isUpdated != null) {
      return _isUpdated.booleanValue();
    }
    return getUpdatedTables().size() > 0;
  }

  /**
   * Inform this Transaction that the action that was being prepared is to be cancelled and any
   * potential updates destroyed. This will have the following effects:-
   * <ul>
   * <li>Remove the Transaction from the register of active Transactions in the Db
   * <li>Release all write locks held on PersistentRecords in the Db
   * <li>Inform any WritableRecords that have been created for the Transaction that they are no
   * longer valid.
   * </ul>
   * 
   * @return a MirrorNestedException ff any of the triggers that are registered on the Table that
   *         are invoked during the course of the commit have thrown Exceptions of any type, then
   *         they will all be wrapped up in this compound Exception. This is, however, a bad case
   *         and it generally means that things are in an unstable position. If everything has been
   *         OK, then return null.
   */
  public final MirrorNestedException cancel()
  {
    if (_state != State.ACTIVE) {
      return null;
    }
    __db.removeTransaction(this);
    MirrorNestedException mirrorNestedException = null;
    for (Table table : getTables()) {
      Int2ObjectMap<PersistentRecord> records = getTableRecords(table, false);
      for (PersistentRecord sourceRecord : records.values()) {
        try {
          sourceRecord.cancel(this);
        } catch (MirrorNestedException mirrorRecordNestedException) {
          mirrorNestedException = MirrorNestedException.getInstance(mirrorNestedException,
                                                                    "Error in ending transaction",
                                                                    mirrorRecordNestedException);
        } catch (MirrorTransactionOwnershipException mtoEx) {
          PersistentRecord targetRecord = mtoEx.getTargetRecord();
          if (targetRecord != null && targetRecord.getTransaction() == null) {
            System.err.println("null Transaction in finalisation for " + targetRecord);
            mtoEx.printStackTrace();
          } else {
            throw new LogicException("Record lost ownership", mtoEx);
          }
        }
      }
    }
    try {
      mirrorNestedException = triggerStateTriggers(StateTrigger.STATE_TRANSACTION_CANCELLED,
                                                   __lockedRecords,
                                                   mirrorNestedException);
    } catch (MirrorTransactionOwnershipException e) {
      throw new MirrorLogicException("Unexpected exception during Transaction.cancel()", e);
    }
    _state = State.CANCELLED;
    return mirrorNestedException;
  }

  /**
   * Lock the targetted tables, and then execute the appropriate delete commands to remove all data
   * from the backend database. This will take 1 SQL statement per Table currently locked by the
   * Transaction.
   */
  private void executeDeleteSql(Statement statement)
      throws SQLException
  {
    Set<Table> tables = getTables();
    if (tables.size() > 0) {
      for (Table table : tables) {
        Int2ObjectMap<PersistentRecord> records = getTableRecords(table, false);
        if (records.size() != 0) {
          long time = 0;
          if (DebugConstants.STATS_ENABLED) {
            time = System.currentTimeMillis();
          }
          deleteSqlRecords(table, records, statement);
          if (DebugConstants.STATS_ENABLED) {
            table.updateDeleteStats(records.size(), System.currentTimeMillis() - time);
          }
        }
      }
    }
  }

  /**
   * Take the given Set of recordIds and create a single compound SQL statement to delete all of
   * them from table and commit it in the context of the given statement. This is SQL transaction
   * safe as it doesn't perform any look ups and creates the SQL statement purely from the list of
   * ids.
   */
  private void deleteSqlRecords(Table table,
                                Int2ObjectMap<PersistentRecord> records,
                                Statement statement)
      throws SQLException
  {
    StringBuilder sql = new StringBuilder();
    sql.append("delete from ").append(table.getName()).append(" where id in (");
    IntIterator i = records.keySet().iterator();
    while (i.hasNext()) {
      sql.append(i.nextInt());
      if (i.hasNext()) {
        sql.append(",");
      }
    }
    sql.append(")");
    Db.executeUpdate(statement, sql.toString());
    if (getDb().flushQueryCaches()) {
      table.invalidateCachedRecordSources();
    }
  }

  /**
   * Add a given PersistentRecord to this Transaction. This is the only "clean" way of acquiring a
   * write lock on a record as it ensures that the record is cleanly inserted into the Transaction.
   * 
   * @param record
   *          The PersistentRecord that is going to be inserted into this transaction.
   * @param includeDependencies
   *          If true then the method will try and acquire the locks on all records that are
   *          declared as dependent on the original record. Please note that if the Transaction is a
   *          deletion transaction then this will get overridden to a state of true always.
   * @param timeout
   *          The maximum amount of time that this method is allowed to wait on a single write lock
   *          acquisition operation.
   */
  private void lockPersistentRecord(PersistentRecord record,
                                    boolean includeDependencies,
                                    long timeout)
      throws MirrorTransactionTimeoutException
  {
    timeout = __db.handleDefaultTransactionTimeout(timeout);
    acquireSingleRecordLock(record, timeout);
    if (includeDependencies || _style == TRANSACTION_DELETE) {
      Iterator<PersistentRecord> dependentRecords = record.getDependentRecords(true, this);
      while (dependentRecords.hasNext()) {
        acquireSingleRecordLock(dependentRecords.next(), timeout);
      }
    }
  }

  private void acquireSingleRecordLock(PersistentRecord record,
                                       long timeout)
      throws MirrorTransactionTimeoutException
  {
    if (record instanceof WritableRecord) {
      RuntimeException rEx =
          new IllegalStateException(String.format("Warning: %s Attempted to lock %s, will try and gain lock",
                                                  this,
                                                  record));
      rEx.printStackTrace();
      record = record.getPersistentRecord();
    }
    record.acquireWriteLock(this, timeout);
    boolean result = getTableRecords(record.getTable(), true).put(record.getId(), record) == null;
    if (result) {
      try {
        switch (getType()) {
          case TRANSACTION_UPDATE:
            record.getTable()
                  .triggerStateTriggers(record, this, StateTrigger.STATE_PERSISTENT_WRITABLE);
            break;
          case TRANSACTION_DELETE:
            record.getTable()
                  .triggerStateTriggers(record, this, StateTrigger.STATE_PERSISTENT_PREDELETED);
            break;
          default:
            throw new LogicException("Transactions of type " + TRANSACTION_TYPES.get(getType())
                                     + " should not be locking records");
        }
      } catch (MirrorNestedException triggerEx) {
        cancel();
        throw new MirrorTransactionTimeoutException("Error while executing StateTriggers",
                                                    this,
                                                    null,
                                                    timeout,
                                                    null,
                                                    triggerEx);
      }
      _recordCount++;
    }
  }

  /**
   * Request the Transaction to obtain a write lock on a given PersistentRecord and get a
   * WritableRecord as the result. The process of acquiring the write lock on the record depends on
   * whether or not there is a Transaction that currently holds a write lock on the record (only one
   * Transaction is permitted to hold a write lock at a time). Acquiring the lock will succeed if:-
   * <ul>
   * <li>The given Transaction already owns the write lock on this record and therefore gets another
   * reference to the existing WritableRecord. This means that the only threads who can have access
   * to the WritableRecord at a given time are ones that have access to a reference to the
   * Transaction that owns it. If this reference is held privately to the Thread that is executing
   * the operation, then it is guaranteed to be thread safe.
   * <li>If no Transaction has a write lock at the moment, then the lock is given to the passed
   * Transaction, and a new WritableRecord is returned.
   * <li>If an existing Transaction has the write lock then the Thread will wait until either:-
   * <ul>
   * <li>The Transaction with the lock either commits or cancels and therefore releases the lock.
   * <li>The Transaction with the lock becomes invalid (by timing out) then is automatically
   * cancelled and releases the lock.
   * <li>The request for the writable record times out itself, in which case the operation is
   * aborted nd a MirrorTransactionTimeout is thrown.
   * </ul>
   * Always assuming that the operation didn't abort, then a new WritableRecord is created and the
   * write lock given over to the requesting Transaction.
   * </ul>
   * The method will always lock any dependent records of the source record.
   * 
   * @param sourceRecord
   *          The record that is to be edited.
   * @param timeout
   *          The maximum amount of time to spend attempting to take the record lock.
   * @return The Writable version of the Persistent record.
   * @throws MirrorTransactionTimeoutException
   *           If the transaction lock cannot be acquired within the specified length of time.
   */
  public WritableRecord edit(PersistentRecord sourceRecord,
                             long timeout)
      throws MirrorTransactionTimeoutException
  {
    return edit(sourceRecord, true, timeout);
  }

  /**
   * Acquire the edit lock on the given sourceRecord using a timeout of Db.DEFAULT_TIMEOUT.
   * 
   * @see #edit(PersistentRecord,long)
   * @see Db#DEFAULT_TIMEOUT
   */
  public WritableRecord edit(PersistentRecord sourceRecord)
      throws MirrorTransactionTimeoutException
  {
    return edit(sourceRecord, Db.DEFAULT_TIMEOUT);
  }

  /**
   * Demand the Transaction obtains a write lock on the given PersistentRecord and get a
   * WritableRecord as a result. This method will recursively attempt to acquire the lock on the
   * sourceRecord, cancelling <i>all</i> existing Transactions that are part of the sourceRecords
   * dependent tree. Use with lots of caution. This method recurses, but I see no valid reason why
   * it should not terminate (sooner or later in the worst case it will lock the entire db and then
   * it has to)
   * 
   * @param sourceRecord
   *          The record that is to be edited.
   * @param timeout
   *          The maximum amount of time to wait before attempting to cancel the Transaction. Note
   *          that this method may well wait for longer than this if multiple Transactions may need
   *          to be cancelled as it will need to timeout between each one. Choosing a longer value
   *          than 1 may be "polite", but 100 should be sufficient for avoiding messing with most
   *          automated processes.
   * @param transactionPassword
   *          The admin password that is required to be able to extract the Transaction from the
   *          locked record.
   * @throws MirrorTransactionTimeoutException
   *           If there is a timeout thrown inside the system that is not record associated. Very
   *           unlikely.
   * @throws MirrorAuthorisationException
   *           if transactionPassword is duff.
   * @see #edit(PersistentRecord,long)
   */
  public WritableRecord editWithLockStealing(PersistentRecord sourceRecord,
                                             long timeout,
                                             String transactionPassword)
      throws MirrorTransactionTimeoutException, MirrorAuthorisationException
  {
    try {
      return edit(sourceRecord, timeout);
    } catch (MirrorTransactionTimeoutException timeoutException) {
      PersistentRecord lockedRecord = timeoutException.getRecord();
      if (lockedRecord != null) {
        Transaction lockingTransaction =
            lockedRecord.getTransaction(transactionPassword, "Transaction.editWithLockStealing");
        lockingTransaction.cancel();
        return editWithLockStealing(sourceRecord, timeout, transactionPassword);
      }
      throw timeoutException;
    }
  }

  /**
   * Demand the Transaction obtains a delete lock on the given PersistentRecord. This method will
   * recursively attempt to acquire the lock on the sourceRecord, cancelling <i>all</i> existing
   * Transactions that are part of the sourceRecords dependent tree. Use with lots of caution. This
   * method recurses, but I see no valid reason why it should not terminate (sooner or later in the
   * worst case it will lock the entire db and then it has to)
   * 
   * @param sourceRecord
   *          The record that is to be deleted.
   * @param timeout
   *          The maximum amount of time to wait before attempting to cancel the Transaction. Note
   *          that this method may well wait for longer than this if multiple Transactions may need
   *          to be cancelled as it will need to timeout between each one. Choosing a longer value
   *          than 1 may be "polite", but 100 should be sufficient for avoiding messing with most
   *          automated processes.
   * @param transactionPassword
   *          The admin password that is required to be able to extract the Transaction from the
   *          locked record.
   * @throws MirrorTransactionTimeoutException
   *           If there is a timeout thrown inside the system that is not record associated. Very
   *           unlikely.
   * @throws MirrorAuthorisationException
   *           if transactionPassword is duff.
   * @see #edit(PersistentRecord,long)
   */
  public void deleteWithLockStealing(PersistentRecord sourceRecord,
                                     long timeout,
                                     String transactionPassword)
      throws MirrorTransactionTimeoutException, MirrorAuthorisationException
  {
    try {
      delete(sourceRecord, timeout);
    } catch (MirrorTransactionTimeoutException timeoutException) {
      PersistentRecord lockedRecord = timeoutException.getRecord();
      if (lockedRecord != null) {
        Transaction lockingTransaction =
            lockedRecord.getTransaction(transactionPassword, "Transaction.deleteWithLockStealing");
        lockingTransaction.cancel();
        deleteWithLockStealing(sourceRecord, timeout, transactionPassword);
        return;
      }
      throw timeoutException;
    }
  }

  /**
   * Attempt to acquire an edit lock on a given record with the option as to whether or not you also
   * lock the dependent records. If you choose not to lock the dependent records then you should be
   * sure that you don't need to lock them to complete the work that you are doing. If your changes
   * have side effects and these side effects expect the dependent records to be locked then you may
   * get MirrorFieldExceptions as things try to update PersistentRecords in the believe that they
   * "should" be WritableRecords. Use with caution and if you are unsure, use the conventional edit
   * lock which is much safer (although less performant in some situations).
   * <p>
   * If your Transaction is working on records in the Node infrastructure relating to a particular
   * Node (eg TableContentCompiler instances), and you are making changes to fields that are
   * indexed, then be aware that triggering the rebuild of the index is dependent on the Node itself
   * being lock. You should therefore either not use this method and lock then entire infrastructure
   * for the Node, or you explicitly trigger a rebuild of the Node index once your Transaction has
   * been committed via {@link AdvancedSearch#rebuildIndex(PersistentRecord)}
   * </p>
   * 
   * @see #edit(PersistentRecord)
   */
  public WritableRecord edit(PersistentRecord sourceRecord,
                             boolean includeDependentRecords,
                             long timeout)
      throws MirrorTransactionTimeoutException
  {
    checkValidity();
    if (getType() != TRANSACTION_UPDATE) {
      throw new IllegalStateException("Cannot get a writable record from a "
                                      + TRANSACTION_TYPES.get(getType()) + " transaction");
    }
    sourceRecord = sourceRecord.getPersistentRecord();
    lockPersistentRecord(sourceRecord, includeDependentRecords, timeout);
    try {
      return sourceRecord.getWritableRecord(this);
    } catch (MirrorTransactionOwnershipException mtoEx) {
      throw new LogicException(this + " has lost ownership of freshly locked record "
                               + sourceRecord,
                               mtoEx);
    }
  }

  /**
   * Attempt to acquire an edit lock on a list of PersistentRecord objects.
   * 
   * @param records
   *          The Records that we want to obtain the edit lock on.
   * @param timeout
   *          The time to spend per record on acquiring the transaction lock.
   * @throws MirrorTransactionTimeoutException
   *           If the record lock cannot be acquired on one of the records.
   */
  public void edit(Collection<PersistentRecord> records,
                   long timeout)
      throws MirrorTransactionTimeoutException
  {
    for (PersistentRecord record : records) {
      edit(record, timeout);
    }
  }

  /**
   * Attempt to acquire an edit lock on all the records that currently match the contents of a given
   * Query.
   * 
   * @param recordSource
   *          The RecordSource that contains the records to be deleted.
   * @param timeout
   *          The maximum amount of time to spend per record on acquiring the transaction lock.
   * @throws MirrorTransactionTimeoutException
   *           If the lock cannot be obtained on one of the records.
   */
  public void edit(RecordSource recordSource,
                   long timeout)
      throws MirrorTransactionTimeoutException
  {
    edit(recordSource.getRecordList(this), timeout);
  }

  /**
   * Attempt to include a given persistent record (and all it's dependent records) into a delete
   * transaction. This will attempt to gain a write lock on the complete dependency tree from the
   * stated record. Failure will result in a cancellation of the transaction. Upon success of the
   * operation, once the transaction is committed, all the records in the dependency tree will be
   * deleted.
   * 
   * @param record
   *          The PersistentRecord that wishes to be deleted.
   * @param timeout
   *          The maximum amount of time to spend acquiring the write lock on the record.
   * @throws MirrorTransactionTimeoutException
   *           If the write lock could not be achieved. In order to prevent the transaction from
   *           being left in a partially incorrect status (ie only part of the write lock held on a
   *           dependency tree), the transaction is automatically cancelled if the write lock is not
   *           fully taken and then the timeout exception is rethrown.
   */
  public void delete(PersistentRecord record,
                     long timeout)
      throws MirrorTransactionTimeoutException
  {
    checkValidity();
    if (getType() != TRANSACTION_DELETE) {
      throw new IllegalStateException("Cannot call delete on an " + TRANSACTION_TYPES.get(getType())
                                      + " transaction");
    }
    try {
      lockPersistentRecord(record, true, timeout);
    } catch (MirrorTransactionTimeoutException mttEx) {
      // # 2DO do we really want to cancel the entire Tranaction if
      // # a single record times out? Probably yes because
      // # otherwise the structure of compound deletes will be
      // # unpredictable if the lock fails in the centre at an
      // # unspecified point.
      cancel();
      throw (MirrorTransactionTimeoutException) mttEx.fillInStackTrace();
    }
  }

  /**
   * Attempt to include a given persistent record (and all it's dependent records) into a delete
   * transaction with a timeout of Db.DEFAULT_TIMEOUT.
   * 
   * @see #delete(PersistentRecord,long)
   * @see Db#DEFAULT_TIMEOUT
   */
  public void delete(PersistentRecord record)
      throws MirrorTransactionTimeoutException
  {
    delete(record, Db.DEFAULT_TIMEOUT);
  }

  /**
   * Attempt to acquire a deletion lock on a list of PersistentRecord objects.
   * 
   * @param records
   *          Iterator of PersistentRecord objects.
   * @param timeout
   *          The time to spend per record on acquiring the transaction lock.
   * @throws MirrorTransactionTimeoutException
   *           If the record lock cannot be acquired on one of the records.
   */
  public void delete(Collection<PersistentRecord> records,
                     long timeout)
      throws MirrorTransactionTimeoutException
  {
    for (PersistentRecord record : records) {
      delete(record, timeout);
    }
  }

  /**
   * Attempt to acquire the deletion lock on all the records that currently match the contents of a
   * given Query.
   * 
   * @param recordSource
   *          The RecordSource that contains the records to be deleted.
   * @param timeout
   *          The maximum amount of time to spend per record on acquiring the transaction lock.
   * @throws MirrorTransactionTimeoutException
   *           If the lock cannot be obtained on one of the records.
   */
  public void delete(RecordSource recordSource,
                     long timeout)
      throws MirrorTransactionTimeoutException
  {
    delete(recordSource.getRecordList(), timeout);
  }

  @Override
  public PersistentRecord getRecord(Table table,
                                    Integer id)
      throws MirrorNoSuchRecordException
  {
    return table.getRecord(id, this);
  }

  @Override
  public PersistentRecord getRecord(PersistentRecord record)
  {
    if (record == null) {
      return null;
    }
    try {
      return record.isLockedBy(this) ? record.getWritableRecord(this) : record;
    } catch (MirrorTransactionOwnershipException e) {
      throw new MirrorLogicException("Impossible", e);
    }
  }

}
