package org.cord.mirror;

/**
 * Exception that is thrown to indicate that the value which was supplied as a candidate for a Field
 * could not be converted into a type T and therefore the setField method could not continue. The
 * state of the record should not have been changed if this exception is thrown.
 * 
 * @author alex
 */
public class TypeValidationException
  extends MirrorFieldException
{
  private static final long serialVersionUID = 1L;

  public TypeValidationException(String message,
                                 Exception exception,
                                 TransientRecord record,
                                 String fieldName,
                                 Object badValue)
  {
    super(message,
          exception,
          record,
          fieldName,
          badValue,
          null);
  }
}
