package org.cord.mirror;

import org.cord.util.Shutdownable;

public interface InstantiationTrigger
  extends Shutdownable
{
}
