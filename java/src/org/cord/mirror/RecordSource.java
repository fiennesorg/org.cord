package org.cord.mirror;

import java.util.Iterator;

import org.cord.mirror.recordsource.operation.DuoRecordSourceOperationKey;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.util.Cachable;
import org.cord.util.UndefinedCompException;

/**
 * An Object that wraps an ordered list of Records from a given Table. The choice of records in the
 * list may or may not be affected by the Transactional viewpoint that you are asking for the
 * RecordSource.
 * 
 * @author alex
 */
public interface RecordSource
  extends Iterable<PersistentRecord>, Cachable<RecordSource>, TransactionGettable
{
  /**
   * Get the Table that this RecordSource contains records from.
   * 
   * @return The Table. Should never be null.
   */
  public Table getTable();

  /**
   * Get the ids that are currently contained in this RecordSource from the perspective of the given
   * Transaction.
   * 
   * @return Unmodifiable IdList. Should not be null.
   * @see #shouldRebuild(Viewpoint)
   */
  public IdList getIdList(Viewpoint viewpoint);

  /**
   * Get the list of record ids that this RecordSource contains from the perspective of no
   * Transaction. This is a readable shortcut for getIdList(null).
   * 
   * @return Unmodifiable IdList. Should not be null.
   * @see #getIdList(Viewpoint)
   */
  public IdList getIdList();

  /**
   * Get the List of all PersistentRecords from getTable() that this RecordSource contains from the
   * given transactional viewpoint. If any of the records are locked by viewpoint then they will be
   * automatically resolved into the appropriate WritableRecords amongst the non-locked
   * PersistentRecords. This is equivalent to invoking:
   * <code> new RecordList(getIdList(viewpoint), viewpoint)</code> The implementation may cache the
   * value if it deems it safe rather than creating a new RecordList with each invocation.
   * 
   * @return Unmodifiable RecordList. Should not be null.
   * @see #getRecordList(Viewpoint)
   */
  public RecordList getRecordList(Viewpoint viewpoint);

  /**
   * Get the List of all PersistentRecords from getTable() that this RecordSource contains. This is
   * equivalent to invoking: <code>new RecordList(getIdList(), null)</code> The implementation may
   * cache the value if it deems it safe rather than creating a new RecordList with each invocation.
   * 
   * @return The unmodifiable RecordList. Should not be null.
   * @see #getIdList()
   */
  public RecordList getRecordList();

  /**
   * Get the most recent time when this RecordSource might have changed the records that it
   * contains. This is intended primarily for when you are chaining RecordSources and you want to
   * know when you need to rebuild cached versions of your data on the grounds that it may no longer
   * be valid. If you are not caching derived results of the contained data, then you should just
   * call getIdList() or getRecordList() and leave it up to the implementation of the RecordSource
   * to rebuild this information when needed. Please note that the value returned by this method
   * doesn't mean that the contents <em>have</em> changed, just that they <em>might</em> have
   * changed.
   * 
   * @return The timestamp in milliseconds for the last time that something happened which may have
   *         caused the contents of this RecordSource to update itself.
   */
  public long getLastChangeTime();

  /**
   * Get the Iterator of PersistentRecords as contained within the RecordList supplied by
   * getRecordList. This method is there primarily so that it becomes cleaner to iterate acros
   * RecordSource.
   * 
   * @return The appropriate Iterator. Never null
   */
  @Override
  public Iterator<PersistentRecord> iterator();

  /**
   * Is it possible that examining this RecordSource from the perspective of the supplied
   * Transaction may return different ids? This will get invoked relatively frequently and should
   * therefore be relatively lightweight. Please note that it would not be a problem if you return
   * true and yet still return the same set of ids, but it would be a problem if you returned false
   * and then did change the IdList. If there is an element of doubt as to the result then you
   * should return true.
   * 
   * @param viewpoint
   *          The Transaction to determine whether it has an effect. If you are only reading
   *          Transactionally aware data from a selection of Tables and the supplied viewpoint
   *          doesn't lock any records from these Tables, then it will not have an effect on this
   *          RecordSource.
   * @return true if there is a possibility that the ids in the RecordIdList may be affected by
   *         which Transaction is the current viewpoint onto the RecordSource.
   */
  public boolean shouldRebuild(Viewpoint viewpoint);

  /**
   * @return The required ordering or null if it is not possible to define the ordering.
   */
  public RecordOrdering getRecordOrdering();

  /**
   * Might this RecordSource require reordering according to it's RecordOrdering in the presence of
   * this Transaction? Most RecordSources will answer false to this, but if you are a Query and if
   * the Viewpoint has updated Tables that impact on your ordering then you might want to return
   * true. If this is the case then the {@link RecordOrdering#applyJavaOrdering(IdList, Viewpoint)}
   * will be invoked on the ids that your RecordSource returns. Note that this will not add or
   * remove records from the RecordSource, but may impose a different ordering on them depending on
   * the state of the locked records.
   */
  public boolean shouldReorder(Viewpoint viewpoint);

  public <V> V opt(RecordSourceOperationKey<V> key);
  public <V> V opt(RecordSourceOperationKey<V> key,
                   Viewpoint viewpoint);
  public <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                      A a);
  public <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                      Viewpoint viewpoint,
                      A a);
  public <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                         A a,
                         B b);
  public <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                         Viewpoint viewpoint,
                         A a,
                         B b);

  public <V> V comp(RecordSourceOperationKey<V> key)
      throws UndefinedCompException;
  public <V> V comp(RecordSourceOperationKey<V> key,
                    Viewpoint viewpoint)
      throws UndefinedCompException;
  public <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                       A a)
      throws UndefinedCompException;
  public <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                       Viewpoint viewpoint,
                       A a)
      throws UndefinedCompException;
  public <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                          A a,
                          B b)
      throws UndefinedCompException;
  public <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                          Viewpoint viewpoint,
                          A a,
                          B b)
      throws UndefinedCompException;

}
