package org.cord.mirror;

import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * A caching RecordOperation that will cache values in WritableRecords as well as PersistentRecords.
 * Most of the time you don't want this behaviour and you probably want to utilise a
 * {@link DirectCachingSimpleRecordOperation} as otherwise updates to your data will not get
 * reflected in the cached values unless you are not expecting to manipulate the data in this way,
 * or you clear the cache when the data changes.
 * 
 * @author alex
 * @param <V>
 *          The class of value that is returned by the RecordOperation
 */
public abstract class AlwaysCachingSimpleRecordOperation<V>
  extends CachingSimpleRecordOperation<V>
{
  private final Object __cacheKey;

  public AlwaysCachingSimpleRecordOperation(RecordOperationKey<V> key,
                                            Object cacheKey)
  {
    super(key);
    __cacheKey = Preconditions.checkNotNull(cacheKey, "cacheKey");
  }

  /**
   * Create an instance that uses the RecordOperationKey as the cache key.
   */
  public AlwaysCachingSimpleRecordOperation(RecordOperationKey<V> key)
  {
    this(key,
         key);
  }

  @Override
  protected V getOperation(TransientRecord record,
                           Viewpoint viewpoint,
                           Map<Object, Object> cache)
  {
    @SuppressWarnings("unchecked")
    V value = (V) cache.get(__cacheKey);

    if (value == null) {
      value = calculate(record, viewpoint);
      cache.put(__cacheKey, value);
    }
    return value;
  }

  /**
   * calculate the result of invoking this RecordOperation because there isn't a cached value.
   */
  public abstract V calculate(TransientRecord record,
                              Viewpoint viewpoint);

}
