package org.cord.mirror;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ParametricQueryFactory is class that enables you to construct Query object that take a variable
 * number of parameters. These Queries are either attached to Table or Record level objects and
 * permit the generation of complex joins that take more than one source Record (or other parameter
 * sources)
 */
@Deprecated
public class ParametricQueryFactory
{
  private final Table __targetTable;

  private final List<Table> __affectedTables;

  private final List<String> __filterSegments;

  private final String __order;

  public ParametricQueryFactory(String name,
                                Table targetTable,
                                Iterator<Table> affectedTableList,
                                Iterator<String> filterSegmentList,
                                String order,
                                long defaultTimeout)
  {
    __targetTable = targetTable;
    __affectedTables = new ArrayList<Table>();
    __affectedTables.add(targetTable);
    if (affectedTableList != null) {
      while (affectedTableList.hasNext()) {
        addAffectedTable(affectedTableList.next());
      }
    }
    __filterSegments = new ArrayList<String>();
    while (filterSegmentList.hasNext()) {
      __filterSegments.add(filterSegmentList.next().toString());
    }
    __order = order;
  }

  private void addAffectedTable(Object potentialTable)
  {
    Table table = null;
    if (potentialTable instanceof Table) {
      table = (Table) potentialTable;
    } else {
      table = __targetTable.getDb().getTable(potentialTable.toString());
    }
    if (!__affectedTables.contains(table)) {
      __affectedTables.add(table);
    }
  }

  private void addNextParameter(StringBuilder filter,
                                Iterator<String> filterSegmentList,
                                Iterator<String> parameterList)
  {
    filter.append(filterSegmentList.next());
    filter.append(parameterList.next());
  }

  private void addNextParameter(StringBuilder filter,
                                Iterator<String> filterSegmentList)
  {
    filter.append(filterSegmentList.next());
    if (filterSegmentList.hasNext()) {
      throw new IllegalArgumentException("Insufficient number of parameters");
    }
  }

  public Query buildQuery(Iterator<String> parameterList)
  {
    StringBuilder filter = new StringBuilder();
    Iterator<String> filterSegmentList = __filterSegments.iterator();
    while (parameterList.hasNext()) {
      addNextParameter(filter, filterSegmentList, parameterList);
    }
    addNextParameter(filter, filterSegmentList);
    return __targetTable.getQuery(null,
                                  filter.toString(),
                                  __order,
                                  Query.toArray(__affectedTables));
  }
}
