package org.cord.mirror;

/**
 * Extension of MirrorFieldException that holds information about a related record that is
 * responsible for the MirrorFieldException being thrown.
 */
public class RelatedMirrorFieldException
  extends MirrorFieldException
{
  /**
   * 
   */
  private static final long serialVersionUID = -8791823787446778542L;

  private final PersistentRecord __relatedRecord;

  public RelatedMirrorFieldException(String message,
                                     Exception exception,
                                     TransientRecord record,
                                     FieldKey<?> fieldName,
                                     PersistentRecord relatedRecord)
  {
    super(message,
          exception,
          record,
          fieldName);
    __relatedRecord = relatedRecord;
  }

  public RelatedMirrorFieldException(String message,
                                     Exception exception,
                                     TransientRecord record,
                                     String fieldName,
                                     Object badValue,
                                     Object proposedValue,
                                     PersistentRecord relatedRecord)
  {
    super(message,
          exception,
          record,
          fieldName,
          badValue,
          proposedValue);
    __relatedRecord = relatedRecord;
  }

  public final PersistentRecord getRelatedRecord()
  {
    return __relatedRecord;
  }

  /**
   * Try and resolve the given Throwable or its causal ancestors into a RelatedMirrorFieldException
   * and return the appropriate related record.
   */
  public static PersistentRecord getRelatedRecord(Throwable throwable)
  {
    if (throwable != null && throwable instanceof RelatedMirrorFieldException) {
      return ((RelatedMirrorFieldException) throwable).getRelatedRecord();
    }
    Throwable cause = throwable.getCause();
    if (cause != null) {
      return getRelatedRecord(cause);
    }
    return null;
  }
}