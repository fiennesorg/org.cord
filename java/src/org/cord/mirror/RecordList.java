package org.cord.mirror;

import java.util.AbstractList;
import java.util.Iterator;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/**
 * Wrapper around an IdList that provides a view onto the list that exposes PersistentRecords rather
 * than Integer ids. If the wrapped IdList is modifiable then this will also be modifiable otherwise
 * it will be immutable.
 * <p>
 * If the Table has been modified since the wrapped IdList was generated and some of the ids are no
 * longer valid (ie they are pointing at records that have since been deleted) then attempts to get
 * these records will result in a MissingRecordException. This is a RuntimeException and clients are
 * not expected to catch the failure, but rather to fail gracefully. If the failure route includes
 * retrying the operation then the exception should be allowed to bubble up far enought that the
 * presupposition that made it valid to invoke the operation can be checked again against the
 * updated state of the database. Please note that if the Table has had records deleted and these
 * records were not contained in the nested IdList then it is safe to iterate() across this
 * RecordList without any exceptions being raised.
 * </p>
 * <p>
 * In order to minimise the chances of issues with backend modifications of Tables taking place
 * while you are using a RecordList, you should not hold onto a reference for any longer than the
 * immediate requirements when it is first created. If you want to cache something then you should
 * cache the RecordSource that produces the RecordList as this will automatically rebuild a new
 * RecordList when necessary as the backend Table gets updated.
 * </p>
 * 
 * @author alex
 * @see IdList
 * @see RecordSource
 * @see org.cord.mirror.MissingRecordException
 */
public final class RecordList
  extends AbstractList<PersistentRecord>
{
  private final IdList __idList;

  private final Viewpoint __viewpoint;

  /**
   * @param recordIdList
   *          The compulsory IdList that this RecordList is wrapping.
   * @param viewpoint
   *          The optional viewpoint that this RecordList will apply to all PersistentRecords that
   *          it presents
   */
  public RecordList(IdList recordIdList,
                    Viewpoint viewpoint)
  {
    __idList = Preconditions.checkNotNull(recordIdList, "recordIdList");
    __viewpoint = viewpoint;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("table", getTable()).add("size", size()).toString();
  }

  /**
   * Get the IdList that this RecordList is wrapping.
   * 
   * @return The wrapped IdList. Never null
   */
  public IdList getIdList()
  {
    return __idList;
  }

  /**
   * @return The transactional viewpoint. Possibly null
   */
  public Viewpoint getViewpoint()
  {
    return __viewpoint;
  }

  /**
   * @return The Table that this RecordList contains records from. Never null.
   */
  public Table getTable()
  {
    return __idList.getTable();
  }

  /**
   * Get a record from the viewpoint of the initialising transaction.
   * 
   * @return The record, never null.
   * @throws IndexOutOfBoundsException
   *           if index lies outside the permissable range of ids defined in the wrapped IdList
   * @throws MissingRecordException
   *           if the id in the wrapped IdList no longer exists on the Table.
   */
  @Override
  public PersistentRecord get(int index)
  {
    int id = __idList.getInt(index);
    PersistentRecord record = getTable().getOptRecord(id, __viewpoint);
    if (record == null) {
      throw new MissingRecordException(null, getTable(), Integer.valueOf(id), null);
    }
    return record;
  }

  @Override
  public Iterator<PersistentRecord> iterator()
  {
    return new RecordListIterator(this);
  }

  @Override
  public int size()
  {
    return __idList.size();
  }

  /**
   * @throws UnsupportedOperationException
   *           if the nested IdList is not read/write.
   * @throws IllegalArgumentException
   *           if element is not from the same Table that this RecordList is using.
   * @throws NullPointerException
   *           if element is null
   */
  @Override
  public void add(int index,
                  PersistentRecord element)
  {
    TransientRecord.assertIsTable(element, __idList.getTable());
    __idList.add(index, element.getId());
  }

  /**
   * @throws UnsupportedOperationException
   *           if the nested IdList is not read/write.
   */
  @Override
  public PersistentRecord remove(int index)
  {
    return __idList.getTable().getOptRecord(__idList.removeInt(index), __viewpoint);
  }

  /**
   * @throws UnsupportedOperationException
   *           if the nested IdList is not read/write.
   * @throws IllegalArgumentException
   *           if element is not from the same Table that this RecordList is using.
   * @throws NullPointerException
   *           if element is null
   */
  @Override
  public PersistentRecord set(int index,
                              PersistentRecord element)
  {
    TransientRecord.assertIsTable(element, __idList.getTable());
    try {
      return __idList.getTable().getRecord(__idList.set(index, element.getId()), __viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      return null;
    }
  }
}
