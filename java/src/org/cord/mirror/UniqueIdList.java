package org.cord.mirror;

/**
 * Marker interface that should be implemented by implementations of IdList that don't permit
 * repeating ids. They don't throw an error if you add a duplicate, but they don't add it either.
 * 
 * @author alex
 */
public interface UniqueIdList
  extends IdList
{
}
