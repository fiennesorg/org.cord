package org.cord.mirror;

/**
 * A MirrorException that is thrown during the assignment of a value to a field that signifies that
 * the value is rejected for some reason.
 */
public class MirrorFieldException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = 5297607189908178197L;

  private final TransientRecord __record;

  private final String __fieldName;

  private final Object __badValue;

  private final Object __proposedValue;

  public MirrorFieldException(String message,
                              Exception exception,
                              TransientRecord record,
                              Object fieldName,
                              Object badValue,
                              Object proposedValue)
  {
    super(message,
          exception);
    __record = record;
    __fieldName = fieldName.toString();
    __badValue = badValue;
    __proposedValue = proposedValue;
  }

  /**
   * Create a MirrorFieldException that is a rebranded version of an existing MirrorFieldException.
   * All of the meta-data in the internalException will be identical in this MirrorFieldException
   * with the only difference being the message. This is to enable more client-side applications to
   * provide more friendly versions of the occasionally scary debugging messages that mirror can
   * produce as a result of nested compound errors.
   */
  public MirrorFieldException(String message,
                              MirrorFieldException internalException)
  {
    this(message,
         internalException,
         internalException.getRecord(),
         internalException.getFieldName(),
         internalException.getBadValue(),
         internalException.getProposedValue());
  }

  /**
   * Create a MirrorFieldException that doesn't supply another value as an alternative.
   */
  public MirrorFieldException(String message,
                              Exception exception,
                              TransientRecord record,
                              Object fieldName)
  {
    this(message,
         exception,
         record,
         fieldName,
         record == null ? null : record.get(fieldName),
         null);
  }

  public final TransientRecord getRecord()
  {
    return __record;
  }

  public final String getFieldName()
  {
    return __fieldName;
  }

  public final Object getBadValue()
  {
    return __badValue;
  }

  public final Object getProposedValue()
  {
    return __proposedValue;
  }

  public final boolean hasProposedValue()
  {
    return __proposedValue != null;
  }
}
