package org.cord.mirror;

/**
 * Interface that describes Objects that contain a single Table and which are able to describe this
 * Table to the world at large.
 */
public interface TableWrapper
{
  /**
   * Get the SQL name of the Table
   */
  public String getTableName();

  /**
   * Get the Table that this TableWrapper contains assuming that it has been initialised.
   * 
   * @return The appropriate Table, or null if the Table is yet to be initialised
   */
  public Table getTable();
}