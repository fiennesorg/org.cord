package org.cord.mirror;

import org.cord.util.ObjectUtil;

public class IntFieldKey
  extends FieldKey<Integer>
{
  public static IntFieldKey create(String keyName)
  {
    return new IntFieldKey(keyName);
  }

  public IntFieldKey(String keyName)
  {
    super(keyName,
          Integer.class);
  }

  @Override
  public IntFieldKey copy()
  {
    return new IntFieldKey(getKeyName());
  }

  private int _intValueIndex = -1;

  @Override
  protected void register(Field<Integer> field)
  {
    super.register(field);
    _intValueIndex = ObjectUtil.castTo(field, IntField.class).getIntValueIndex();
  }

  @Override
  public Integer call(TransientRecord record,
                      Viewpoint viewpoint)
  {
    return Integer.valueOf(getInt(record));
  }

  public int getInt(TransientRecord record)
  {
    try {
      return record.getIntValues()[_intValueIndex];
    } catch (ArrayIndexOutOfBoundsException e) {
      register(record.getTable().getField(this));
      return getInt(record);
    }
  }

}
