package org.cord.mirror;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MirrorNestedException
  extends MirrorException
{
  /**
   * 
   */
  private static final long serialVersionUID = -486189875271954582L;

  /**
   * Utility method to add a nestedException to a mirrorNestedException that will automatically
   * create a new MirrorNestedException to hold the it if the passed value is null. This therefore
   * frees the client from having to check whether or not it is necessary to create the master
   * exception container on each catch method, thereby making the code considerably more readable.
   * 
   * @param mirrorNestedException
   *          The exception to which the nestedException is to be added. If this is null then a new
   *          exception will be created.
   * @param message
   *          The master message that will be utilised to create the MirrorNestedException. This is
   *          ignored if the mirrorNestedException parameter is not null.
   * @param nestedException
   *          The exception that will be added to the mirrorNestedException.
   * @return The resulting MirrorNestedException which will either be the mirrorNestedException
   *         passed to the method or a new MirrorNestedException if the value was null.
   */
  public static MirrorNestedException getInstance(MirrorNestedException mirrorNestedException,
                                                  String message,
                                                  Exception nestedException)
  {
    if (mirrorNestedException == null) {
      mirrorNestedException = new MirrorNestedException(message, nestedException);
    } else {
      mirrorNestedException.addNestedException(nestedException);
    }
    return mirrorNestedException;
  }

  private final List<Exception> _nestedExceptions = new LinkedList<Exception>();

  public MirrorNestedException(String message,
                               Exception coreNestedException)
  {
    super(message,
          coreNestedException);
  }

  public void addNestedException(Exception exception)
  {
    if (exception != null) {
      _nestedExceptions.add(exception);
    }
  }

  public Iterator<Exception> getNestedExceptions()
  {
    return _nestedExceptions.iterator();
  }

  @Override
  public String toString()
  {
    StringBuilder result = new StringBuilder();
    result.append("MirrorNestedException(");
    Iterator<Exception> nestedExceptions = getNestedExceptions();
    while (nestedExceptions.hasNext()) {
      result.append(nestedExceptions.next());
      if (nestedExceptions.hasNext()) {
        result.append(',');
      }
    }
    result.append(')');
    return result.toString();
  }
}
