package org.cord.mirror;

public class ImpossibleIdException
  extends MissingRecordException
{
  /**
   * MissingRecordException that signifies that you have asked for an id that is outside the range
   * of possible ids for the Table
   */
  private static final long serialVersionUID = 1L;

  public ImpossibleIdException(String message,
                               Table table,
                               Integer id,
                               Throwable cause)
  {
    super(message,
          table,
          id,
          cause);
  }
}
