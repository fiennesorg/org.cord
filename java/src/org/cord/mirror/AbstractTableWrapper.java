package org.cord.mirror;

import org.cord.util.NamedImpl;

/**
 * Basic implementation of TableWrapper that deals with the construction and reporting of the empty
 * Table.
 * 
 * @author alex
 */
public abstract class AbstractTableWrapper
  extends NamedImpl
  implements DbInitialiser, TableWrapper
{
  /**
   * @see TransientRecord#FIELD_ID
   */
  public final static IntFieldKey ID = TransientRecord.FIELD_ID.copy();

  private Db _db;

  private Table _table;

  private String _pwd;

  public AbstractTableWrapper(String tableName)
  {
    super(tableName);
  }

  @Override
  public final String getTableName()
  {
    return getName();
  }

  @Override
  public final Table getTable()
  {
    return _table;
  }

  /**
   * Create the Table if it doesn't already exist and then invokde the subclass initTable method. If
   * you want a non-standard table creation then override createTable.
   * 
   * @param pwd
   *          The transaction password for the database
   * @see #createTable(Db, String)
   * @see #initTable(Db, String, Table)
   */
  @Override
  public final void init(Db db,
                         String pwd)
      throws MirrorTableLockedException
  {
    _db = db;
    _pwd = pwd;
    if (db.hasTable(getTableName())) {
      _table = db.getTable(getTableName());
    } else {
      _table = createTable(db, pwd);
    }
    initTable(db, pwd, _table);
  }

  protected final Db getDb()
  {
    return _db;
  }

  protected final String getTransactionPassword()
  {
    return _pwd;
  }

  /**
   * Create the Table that this TableWrapper wraps. This will invoke the default addTable method. If
   * a more specific form of Table initialisation is required then this method should be overridden
   * in sub-classes.
   * 
   * @param pwd
   *          The transaction password for the database
   * @see Db#addTable(TableWrapper, String)
   * @see #getTableName()
   */
  protected Table createTable(Db db,
                              String pwd)
      throws MirrorTableLockedException
  {
    return db.addTable(this, getTableName());
  }

  /**
   * @param pwd
   *          The transaction password for the database
   * @param table
   *          The table that was created by createTable and which this TableWrapper is wrapping
   * @see #createTable(Db, String)
   */
  protected abstract void initTable(Db db,
                                    String pwd,
                                    Table table)
      throws MirrorTableLockedException;

  /**
   * Shutdown resources associated with this Object. If you are going to override this method then
   * be sure to invoke super.shutdown() after you have done whatever work you need to do.
   */
  @Override
  public void shutdown()
  {
    _table = null;
  }
}