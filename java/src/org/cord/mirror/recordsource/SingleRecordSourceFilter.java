package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * A SingleRecordSourceFilter is a RecordSource that wraps a single RecordSource and provides a
 * filtered view onto it. When it is asked to rebuild the id list, it iterates through the filtered
 * RecordSource and if accepts(record) likes the record then the id is included in the list. Please
 * note that this can be innefficient on large queries where most of the records are to be discarded
 * because it will mean retrieving (possibly from the database) a lot of records that will then just
 * be discarded.
 * 
 * @author alex
 * @see #accepts(PersistentRecord)
 */
public abstract class SingleRecordSourceFilter
  extends RecordSourceWrapper
{
  public SingleRecordSourceFilter(RecordSource wrappedSource,
                                  Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
  }

  /**
   * Iterate through all the records in the filtered RecordSource and return a RecordIdList of all
   * the items that pass the accepts(record) filter.
   * 
   * @see #accepts(PersistentRecord)
   */
  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    RecordList records = getWrappedRecordSource().getRecordList(viewpoint);
    ArrayIdList ids = new ArrayIdList(getTable(), records.size());
    for (PersistentRecord record : records) {
      if (accepts(record)) {
        ids.add(record.getId());
      }
    }
    return ids;
  }

  /**
   * Should the given record that was provided by the filtered RecordSource be part of the contents
   * of this RecordSource?
   * 
   * @return true if the given record is acceptable, false if it should be ommitted
   */
  protected abstract boolean accepts(PersistentRecord record);

  @Override
  public String toString()
  {
    return String.format("SingleRecordSourceFilter(%s)", getWrappedRecordSource());
  }

}
