package org.cord.mirror.recordsource;

import org.cord.mirror.TableWrapper;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Implementation of AbstractRecordSource that provides implements of {@link #getLastChangeTime()}
 * and {@link #shouldRebuild(Viewpoint)} by querying a set of {@link TableWrapper}s.
 * 
 * @author alex
 */
public abstract class DependentTableWrappersRecordSource
  extends AbstractRecordSource
{
  private final Iterable<TableWrapper> __dependentTableWrappers;

  public DependentTableWrappersRecordSource(TableWrapper tableWrapper,
                                            Object cacheKey,
                                            Iterable<TableWrapper> dependentTableWrappers)
  {
    super(tableWrapper,
          cacheKey);
    __dependentTableWrappers =
        Preconditions.checkNotNull(dependentTableWrappers, "dependentTableWrappers");
  }

  public DependentTableWrappersRecordSource(TableWrapper tableWrapper,
                                            Object cacheKey,
                                            TableWrapper... dependentTableWrappers)
  {
    super(tableWrapper,
          cacheKey);
    __dependentTableWrappers = ImmutableList.copyOf(dependentTableWrappers);
  }

  /**
   * Return the most recent lastChangeTime from the set of dependentTableWrappers.
   */
  @Override
  public long getLastChangeTime()
  {
    return RecordSources.getLastChangeTime(__dependentTableWrappers.iterator());
  }

  /**
   * Return true if viewpoint has any locked records from any of the dependentTableWrappers.
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return RecordSources.containsRecordsFrom(viewpoint, __dependentTableWrappers.iterator());
  }

}
