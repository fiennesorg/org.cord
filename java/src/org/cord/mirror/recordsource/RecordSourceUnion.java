package org.cord.mirror.recordsource;

import java.util.List;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * RecordSource that contains the set union of the contained RecordSources.
 * 
 * @author alex
 */
public class RecordSourceUnion
  extends MergedRecordSource
{
  /**
   * @param table
   *          The Table that this RecordSource wraps records from
   * @param cacheKey
   *          The cacheKey that this RecordSource will identify itself by. If this information is
   *          not available at initialisation then use null and then override getCacheKey
   *          appropriately.
   * @param recordSources
   *          The RecordSources that are going to contribute to the union. These must all be from
   *          the same Table as table.
   */
  public RecordSourceUnion(Table table,
                           Object cacheKey,
                           RecordSource... recordSources)
  {
    super(table,
          cacheKey,
          recordSources);
  }

  /**
   * Iterate through the contained RecordSources and construct a Set of the sum of the unique ids
   * present in all of their getIdList(viewpoint) methods.
   * 
   * @see #getRecordSources()
   * @see #getIdList(Viewpoint)
   */
  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    List<RecordSource> recordSources = getRecordSources();
    switch (recordSources.size()) {
      case 0:
        return getTable().getEmptyIds();
      case 1:
        return recordSources.get(0).getIdList(viewpoint);
      default:
        IntSet ids = new IntOpenHashSet();
        for (RecordSource recordSource : recordSources) {
          ids.addAll(recordSource.getIdList(viewpoint));
        }
        return new ArrayIdList(getTable(), ids);
    }
  }
}
