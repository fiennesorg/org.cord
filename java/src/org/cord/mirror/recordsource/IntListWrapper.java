package org.cord.mirror.recordsource;

import java.util.Collection;
import java.util.List;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntListIterator;

public abstract class IntListWrapper
  implements IntList
{
  private final IntList __delegate;

  public IntListWrapper(IntList delegate)
  {
    __delegate = delegate;
  }

  protected abstract void mutatingMethodInvoked();

  protected abstract IntList wrapSubList(IntList intList);

  @Override
  public boolean rem(int key)
  {
    mutatingMethodInvoked();
    return __delegate.rem(key);
  }

  @Override
  public int size()
  {
    return __delegate.size();
  }

  @Override
  public boolean isEmpty()
  {
    return __delegate.isEmpty();
  }

  @Override
  public boolean contains(Object o)
  {
    return __delegate.contains(o);
  }

  @Override
  public Object[] toArray()
  {
    return __delegate.toArray();
  }

  @Override
  public <T> T[] toArray(T[] a)
  {
    return __delegate.toArray(a);
  }

  @Override
  @Deprecated
  public boolean add(Integer e)
  {
    mutatingMethodInvoked();
    return __delegate.add(e);
  }

  @Override
  public boolean remove(Object o)
  {
    mutatingMethodInvoked();
    return __delegate.remove(o);
  }

  @Override
  public boolean containsAll(Collection<?> c)
  {
    return __delegate.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends Integer> c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(c);
  }

  @Override
  public boolean addAll(int index,
                        Collection<? extends Integer> c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(index, c);
  }

  @Override
  public boolean removeAll(Collection<?> c)
  {
    mutatingMethodInvoked();
    return __delegate.removeAll(c);
  }

  @Override
  public boolean retainAll(Collection<?> c)
  {
    mutatingMethodInvoked();
    return __delegate.retainAll(c);
  }

  @Override
  public void clear()
  {
    mutatingMethodInvoked();
    __delegate.clear();
  }

  @Override
  @Deprecated
  public Integer get(int index)
  {
    return __delegate.get(index);
  }

  @Override
  @Deprecated
  public Integer set(int index,
                     Integer element)
  {
    mutatingMethodInvoked();
    return __delegate.set(index, element);
  }

  @Override
  @Deprecated
  public void add(int index,
                  Integer element)
  {
    mutatingMethodInvoked();
    __delegate.add(index, element);
  }

  /**
   * @Deprecated in preference of {@link #removeInt(int)}
   */
  @Override
  @Deprecated
  public Integer remove(int index)
  {
    mutatingMethodInvoked();
    return __delegate.remove(index);
  }

  @Override
  @Deprecated
  public int indexOf(Object o)
  {
    return __delegate.indexOf(o);
  }

  @Override
  @Deprecated
  public int lastIndexOf(Object o)
  {
    return __delegate.indexOf(o);
  }

  @Override
  public int compareTo(List<? extends Integer> o)
  {
    return __delegate.compareTo(o);
  }

  @Override
  @Deprecated
  public IntIterator intIterator()
  {
    final IntIterator i = __delegate.intIterator();
    return new IntIterator() {
      @Override
      public boolean hasNext()
      {
        return i.hasNext();
      }

      @Override
      public Integer next()
      {
        return i.next();
      }

      @Override
      public void remove()
      {
        mutatingMethodInvoked();
        i.remove();
      }

      @Override
      public int nextInt()
      {
        return i.nextInt();
      }

      @Override
      public int skip(int n)
      {
        return skip(n);
      }
    };
  }

  @Override
  public boolean contains(int key)
  {
    return __delegate.contains(key);
  }

  @Override
  public int[] toIntArray()
  {
    return __delegate.toIntArray();
  }

  @Override
  @Deprecated
  public int[] toIntArray(int[] a)
  {
    return __delegate.toIntArray(a);
  }

  @Override
  public int[] toArray(int[] a)
  {
    return __delegate.toArray(a);
  }

  @Override
  public boolean addAll(IntCollection c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(c);
  }

  @Override
  public boolean containsAll(IntCollection c)
  {
    return __delegate.containsAll(c);
  }

  @Override
  public boolean removeAll(IntCollection c)
  {
    mutatingMethodInvoked();
    return __delegate.removeAll(c);
  }

  @Override
  public boolean retainAll(IntCollection c)
  {
    mutatingMethodInvoked();
    return __delegate.removeAll(c);
  }

  @Override
  public IntListIterator iterator()
  {
    return new IntListIteratorWrapper(__delegate.iterator());
  }

  @Override
  @Deprecated
  public IntListIterator intListIterator()
  {
    return new IntListIteratorWrapper(__delegate.intListIterator());
  }

  @Override
  @Deprecated
  public IntListIterator intListIterator(int index)
  {
    return new IntListIteratorWrapper(__delegate.intListIterator(index));
  }

  @Override
  public IntListIterator listIterator()
  {
    return new IntListIteratorWrapper(__delegate.listIterator());
  }

  @Override
  public IntListIterator listIterator(int index)
  {
    return new IntListIteratorWrapper(__delegate.listIterator(index));
  }

  class IntListIteratorWrapper
    implements IntListIterator
  {
    private final IntListIterator __delegate;

    IntListIteratorWrapper(IntListIterator delegate)
    {
      __delegate = delegate;
    }

    @Override
    public boolean hasNext()
    {
      return __delegate.hasNext();
    }

    @Override
    public Integer next()
    {
      return __delegate.next();
    }

    @Override
    public boolean hasPrevious()
    {
      return __delegate.hasPrevious();
    }

    @Override
    public Integer previous()
    {
      return __delegate.previous();
    }

    @Override
    public int nextIndex()
    {
      return __delegate.nextIndex();
    }

    @Override
    public int previousIndex()
    {
      return __delegate.previousIndex();
    }

    @Override
    public void remove()
    {
      mutatingMethodInvoked();
      __delegate.remove();
    }

    @Override
    @Deprecated
    public void set(Integer e)
    {
      mutatingMethodInvoked();
      __delegate.set(e);
    }

    @Override
    @Deprecated
    public void add(Integer e)
    {
      mutatingMethodInvoked();
      __delegate.add(e);
    }

    @Override
    public int previousInt()
    {
      return __delegate.previousInt();
    }

    @Override
    public int back(int n)
    {
      return __delegate.back(n);
    }

    @Override
    public int nextInt()
    {
      return __delegate.nextInt();
    }

    @Override
    public int skip(int n)
    {
      return __delegate.skip(n);
    }

    @Override
    public void set(int k)
    {
      mutatingMethodInvoked();
      __delegate.set(k);
    }

    @Override
    public void add(int k)
    {
      mutatingMethodInvoked();
      __delegate.add(k);
    }

  }

  @Override
  @Deprecated
  public IntList intSubList(int from,
                            int to)
  {
    return wrapSubList(__delegate.intSubList(from, to));
  }

  @Override
  public IntList subList(int from,
                         int to)
  {
    return wrapSubList(__delegate.subList(from, to));
  }

  @Override
  public void size(int size)
  {
    mutatingMethodInvoked();
    __delegate.size(size);
  }

  @Override
  public void getElements(int from,
                          int[] a,
                          int offset,
                          int length)
  {
    __delegate.getElements(from, a, offset, length);
  }

  @Override
  public void removeElements(int from,
                             int to)
  {
    mutatingMethodInvoked();
    __delegate.removeElements(from, to);
  }

  @Override
  public void addElements(int index,
                          int[] a)
  {
    mutatingMethodInvoked();
    __delegate.addElements(index, a);
  }

  @Override
  public void addElements(int index,
                          int[] a,
                          int offset,
                          int length)
  {
    mutatingMethodInvoked();
    __delegate.addElements(index, a, offset, length);
  }

  @Override
  public boolean add(int key)
  {
    mutatingMethodInvoked();
    return __delegate.add(key);
  }

  @Override
  public void add(int index,
                  int key)
  {
    mutatingMethodInvoked();
    __delegate.add(index, key);
  }

  @Override
  public boolean addAll(int index,
                        IntCollection c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(index, c);
  }

  @Override
  public boolean addAll(int index,
                        IntList c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(index, c);
  }

  @Override
  public boolean addAll(IntList c)
  {
    mutatingMethodInvoked();
    return __delegate.addAll(c);
  }

  @Override
  public int getInt(int index)
  {
    return __delegate.getInt(index);
  }

  @Override
  public int indexOf(int k)
  {
    return __delegate.indexOf(k);
  }

  @Override
  public int lastIndexOf(int k)
  {
    return __delegate.lastIndexOf(k);
  }

  @Override
  public int removeInt(int index)
  {
    mutatingMethodInvoked();
    return __delegate.removeInt(index);
  }

  @Override
  public int set(int index,
                 int k)
  {
    mutatingMethodInvoked();
    return __delegate.set(index, k);
  }

}
