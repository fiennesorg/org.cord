package org.cord.mirror.recordsource;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;

import com.google.common.base.Preconditions;

/**
 * A RecordSource that is driven by a number of sub-RecordSources all of which point at the same
 * Table thereby letting you merge the contents of the sub-RecordSources.
 * 
 * @author alex
 */
public abstract class MergedRecordSource
  extends CompoundRecordSource
{
  public MergedRecordSource(Table table,
                            Object cacheKey,
                            RecordSource... recordSources)
  {
    super(table,
          cacheKey);
    for (RecordSource recordSource : recordSources) {
      addRecordSource(recordSource);
    }
  }

  /**
   * Check that the supplied RecordSource points at the same Table as this and if so then add it as
   * a RecordSource.
   * 
   * @throws IllegalArgumentException
   *           if recordSource points at the wrong Table
   * @see CompoundRecordSource#addRecordSource(RecordSource)
   */
  @Override
  protected boolean addRecordSource(RecordSource recordSource)
  {
    Preconditions.checkNotNull(recordSource, "recordSource");
    Preconditions.checkState(recordSource.getTable().equals(this.getTable()),
                             "Cannot add %s to %s as it targets %s rather than %s",
                             recordSource,
                             this,
                             recordSource.getTable(),
                             this.getTable());
    return super.addRecordSource(recordSource);
  }
}
