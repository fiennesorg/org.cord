package org.cord.mirror.recordsource;

import java.util.Comparator;
import java.util.TreeSet;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Assertions;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

/**
 * Basic implementation of RecordOrdering that utilises a Comparator to implement the java element
 * of the orderin
 * 
 * @author alex
 */
public class AbstractRecordOrdering
  extends EnglishNamedImpl
  implements RecordOrdering
{
  private final String __sqlOrdering;

  private final Comparator<TransientRecord> __javaOrdering;

  /**
   * @param name
   *          the compulsory comparable ordering representation
   * @param englishName
   *          the optional humanly readable version of the name of the RecordOrdering. If
   *          englishName is not supplied then name will be used instead.
   * @param sqlOrdering
   *          the optional SQL implementation of the ordering. If null, then the RecordOrdering will
   *          always utilise the Comparator in java space.
   * @param javaOrdering
   *          the compulsory java implementation of the ordering.
   */
  public AbstractRecordOrdering(String name,
                                String englishName,
                                String sqlOrdering,
                                Comparator<TransientRecord> javaOrdering)
  {
    super(Assertions.notEmpty(name, "name"),
          StringUtils.padEmpty(englishName, name));
    __sqlOrdering = sqlOrdering;
    __javaOrdering = Preconditions.checkNotNull(javaOrdering, "javaOrdering");
  }

  @Override
  public String getSqlOrdering()
  {
    return __sqlOrdering;
  }

  public Comparator<TransientRecord> getJavaOrdering()
  {
    return __javaOrdering;
  }

  @Override
  public IdList applyJavaOrdering(IdList ids,
                                  Viewpoint viewpoint)
  {
    TreeSet<PersistentRecord> orderedRecords = new TreeSet<PersistentRecord>(__javaOrdering);
    orderedRecords.addAll(new RecordList(ids, viewpoint));
    ArrayIdList orderedIds = new ArrayIdList(ids.getTable(), ids.size());
    for (PersistentRecord record : orderedRecords) {
      orderedIds.add(record.getId());
    }
    return orderedIds;
  }
}
