package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public class IdLists
{
  private IdLists()
  {
  }

  public static String join(String separator,
                            IntCollection ids)
  {
    if (ids.size() == 0) {
      return "";
    }
    StringBuilder buf = new StringBuilder();
    join(separator, ids, buf);
    return buf.toString();
  }

  public static void join(String separator,
                          IntCollection ids,
                          StringBuilder buf)
  {
    IntIterator i = ids.iterator();
    if (i.hasNext()) {
      buf.append(i.nextInt());
      while (i.hasNext()) {
        buf.append(separator).append(i.nextInt());
      }
    }
  }

  public static IdList intersection(Table table,
                                    Iterable<IdList> idLists)
  {
    IntSet intersectionIds = null;
    for (IdList idList : idLists) {
      if (intersectionIds == null) {
        intersectionIds = new IntOpenHashSet(idList);
      } else {
        IntSet nextIds = new IntOpenHashSet(intersectionIds.size());
        final int idListSize = idList.size();
        for (int i = 0; i < idListSize; i++) {
          int id = idList.getInt(i);
          if (intersectionIds.contains(id)) {
            nextIds.add(id);
          }
        }
        intersectionIds = nextIds;
      }
      if (intersectionIds.size() == 0) {
        break;
      }
    }
    return new ArrayIdList(table, intersectionIds);
  }

  public static void assertIsTable(IdList idList,
                                   Table table)
  {
    if (idList == null || !idList.getTable().equals(table)) {
      throw new IllegalArgumentException(String.format("%s is not an IdList from %s",
                                                       idList,
                                                       table));
    }
  }

  public static void assertIsTable(IdList idList,
                                   String tableName)
  {
    if (idList == null || !idList.getTable().getName().equals(tableName)) {
      throw new IllegalArgumentException(String.format("%s is not an IdList from %s",
                                                       idList,
                                                       tableName));
    }
  }
}
