package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Viewpoint;

/**
 * A ManualRecordSource is backed by a TimestampedArrayIdList that is under external control and
 * therefore can contain any combination of records and be updated at will. The getLastChangeTime
 * method defers to the TimestampedArrayIdList so whenever the container is updated even after the
 * ManualRecordSource is create then it will automatically force downstream recalculation of derived
 * RecordSources.
 * 
 * @author alex
 */
public class ManualRecordSource
  extends AbstractRecordSource
{
  private final TimestampedIdList __idList;

  private final IdList __roIdList;

  public ManualRecordSource(TimestampedIdList idList,
                            Object cacheKey)
  {
    super(idList.getTable(),
          cacheKey);
    __idList = idList;
    __roIdList = new UnmodifiableIdList(__idList);
  }

  /**
   * @return unmodifiable view of the idList that was passed to the creator.
   */
  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    return __roIdList;
  }

  /**
   * @return the time that the idList passed to the creator reports that it last changed its state
   * @see TimestampedIdList#getLastChangeTime()
   */
  @Override
  public long getLastChangeTime()
  {
    return __idList.getLastChangeTime();
  }

  /**
   * @return false because the TimestampedArrayIdList isn't interested in a Transactional viewpoint
   *         but rather just a static list of ids that stays the same regardless of how you look at
   *         it.
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return false;
  }
}
