package org.cord.mirror.recordsource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.cord.mirror.IdList;
import org.cord.mirror.RuntimeMirrorException;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.sql.ConnectionPool;
import org.cord.util.Assertions;
import org.cord.util.Named;

/**
 * An ExplicitQuery is a RecordSource wrapper around an externally specified SQL Query. The Query
 * will only ever get invoked against the backend database when one of the wrapped Tables has been
 * updated.
 * 
 * @author alex
 */
public class ExplicitQuery
  extends CompoundRecordSource
  implements Named
{
  private final String __sql;

  /**
   * @param table
   *          The table which the query will return records from
   * @param sql
   *          The SQL which when run will generate a ResultSet with the first column being the ids
   *          of records from table that match this query
   * @param joinTables
   *          The Tables (if any) that the sql references beyond table. Updates to any of these
   *          tables will cause the id list to be recalculated when the results are next asked for.
   */
  public ExplicitQuery(Table table,
                       String sql,
                       Table... joinTables)
  {
    super(table,
          sql);
    addRecordSource(table);
    __sql = Assertions.notEmpty(sql, "sql cannot be empty");
    for (Table joinTable : joinTables) {
      addRecordSource(joinTable);
    }
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
      throws RuntimeMirrorException
  {
    ArrayIdList ids = new ArrayIdList(getTable());
    ConnectionPool connectionPool = getTable().getDb().getConnectionPool();
    Connection connection = null;
    try {
      connection = connectionPool.checkOutConnection("ExplicitQuery");
      Statement statement = null;
      try {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(__sql);
        while (resultSet.next()) {
          ids.add(resultSet.getInt(1));
        }
      } finally {
        if (statement != null) {
          statement.close();
        }
      }
      return ids;
    } catch (SQLException sqlEx) {
      connectionPool.checkInBroken(connection, "ExplicitQuery");
      connection = null;
      throw new RuntimeMirrorException("Unable to resolve query", sqlEx);
    } finally {
      if (connection != null) {
        connectionPool.checkInConnection(connection, "ExplicitQuery");
      }
    }
  }

  @Override
  public String getName()
  {
    return __sql;
  }

  @Override
  public int compareTo(Named other)
  {
    return getName().compareTo(other.getName());
  }

  /**
   * @return false because SQL databases don't know anything about java transactions and therefore
   *         will always give the same result.
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return false;
  }
}
