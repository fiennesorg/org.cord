package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

/**
 * A RecordSource that provides a Transactional viewpoint onto another RecordSource. If you look at
 * this RecordSource without providing a viewpoint, then you will be provided with the data from
 * viewpoint in the constructor. The RecordSourceViewpoint will contain records from the same Table
 * as the wrappedSource, and will not be cachable due to the transient nature of Transaction based
 * viewpoints.
 * 
 * @author alex
 */
public class RecordSourceViewpoint
  extends RecordSourceWrapper
{
  private final Viewpoint __viewpoint;

  public RecordSourceViewpoint(RecordSource wrappedSource,
                               Viewpoint viewpoint)
  {
    super(wrappedSource.getTable(),
          null,
          wrappedSource);
    __viewpoint = viewpoint;
  }

  /**
   * @return If viewpoint is explicitly defined then preserve the explicit viewpoint. If viewpoint
   *         is null then substitute the viewpoint passed to the constructor.
   */
  @Override
  protected final Viewpoint transformViewpoint(Viewpoint viewpoint)
  {
    return viewpoint == null ? __viewpoint : viewpoint;
  }

  @Override
  protected final IdList buildIdList(Viewpoint viewpoint)
  {
    return getWrappedRecordSource().getIdList(transformViewpoint(viewpoint));
  }

  /**
   * @return null because Transaction viewpoints should not be cachable. The wrapped RecordSource
   *         may well be cachable, but it is not an efficient use of caching resources to wrap
   *         viewpoints onto this data.
   */
  @Override
  public final Object getCacheKey()
  {
    return null;
  }

  public final Viewpoint getTransaction()
  {
    return __viewpoint;
  }

  @Override
  public String toString()
  {
    return String.format("RecordSourceViewpoint(%s, %s)", getWrappedRecordSource(), __viewpoint);
  }
}
