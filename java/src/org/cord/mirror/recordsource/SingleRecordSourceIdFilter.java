package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * A SingleRecordSourceIdFilter wraps a RecordSource and only lets through ids that are accepted by
 * the implementing class. This is an optimised version of {@link SingleRecordSourceFilter} which
 * doesn't instantiate the records during filtering and is therefore more efficient in situations
 * where you can perform the filter on ids alone.
 */
public abstract class SingleRecordSourceIdFilter
  extends RecordSourceWrapper
{

  public SingleRecordSourceIdFilter(RecordSource wrappedSource,
                                    Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    IdList origIds = getWrappedRecordSource().getIdList(viewpoint);
    final int origIdsSize = origIds.size();
    ArrayIdList filteredIds = new ArrayIdList(getTable(), origIdsSize);
    for (int i = 0; i < origIdsSize; i++) {
      int id = origIds.getInt(i);
      if (accepts(id)) {
        filteredIds.add(id);
      }
    }
    return filteredIds;
  }

  protected abstract boolean accepts(int id);

}
