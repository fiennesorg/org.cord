package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntLists;

/**
 * Unmodifiable IdList that contains a single defined id.
 */
public class SingletonIdList
  extends IntLists.UnmodifiableList
  implements IdList
{
  private static final long serialVersionUID = 8385829276991134912L;
  private final Table __table;

  public SingletonIdList(Table table,
                         int id)
  {
    super(IntLists.singleton(id));
    __table = table;
  }

  @Override
  public Table getTable()
  {
    return __table;
  }
}
