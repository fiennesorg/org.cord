package org.cord.mirror.recordsource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * A CompoundRecordSource is a RecordSource that derives its contents from a number of other
 * RecordSources. The implementation caches the IdList as supplied implemented by the subclass in
 * buildIds and discards it getLastChangeTime() moves past the last update time. If you know that
 * you are only going to be wrapping a single RecordSource then it is more efficient to utilise a
 * RecordSourceWrapper
 * 
 * @author alex
 * @see org.cord.mirror.recordsource.RecordSourceWrapper
 */
public abstract class CompoundRecordSource
  extends AbstractRecordSource
{
  private final List<RecordSource> __recordSources = new ArrayList<RecordSource>();

  private final List<RecordSource> __roRecordSources =
      Collections.unmodifiableList(__recordSources);

  /**
   * Create a new CompoundRecordSource that produces records from a given Table. This does not
   * register table as one of the RecordSources that feeds into rebuild requests so subclasses
   * should invoke addRecordSource(table) if this is required (which it probably is).
   * 
   * @param table
   *          The compulsory Table that this RecordSource will return records from and which will be
   *          returned by getTable().
   */
  public CompoundRecordSource(Table table,
                              Object cacheKey)
  {
    super(table,
          cacheKey);
  }

  /**
   * Get an immutable List of the RecordSources that are contained within this CompoundRecordSource
   * 
   * @return immutable List of RecordSources
   */
  public List<RecordSource> getRecordSources()
  {
    return __roRecordSources;
  }

  /**
   * Add a RecordSource to become part of the shouldRebuild and buildIds structure. The method is
   * protected so that subclasses can decide how / if it should be exposed. No validation of the
   * recordSource is performed other than it must be defined and not already added. It is up to the
   * sub-class to decide whether or not it has to be a RecordSource pointing at the same Table or
   * not.
   * 
   * @param recordSource
   *          The recordSource. This must not be null
   */
  protected boolean addRecordSource(RecordSource recordSource)
  {
    synchronized (__recordSources) {
      if (__recordSources.contains(recordSource)) {
        return false;
      }
      __recordSources.add(Preconditions.checkNotNull(recordSource, "recordSource"));
    }
    return true;
  }

  /**
   * Return the most recent value of getLastChangeTime() for any of the nested RecordSources.
   * Subclasses should override this method if there is additional information to be added into the
   * calculation.
   */
  @Override
  public long getLastChangeTime()
  {
    long time = 0;
    final int s = __recordSources.size();
    for (int i = 0; i < s; i++) {
      time = Math.max(__recordSources.get(i).getLastChangeTime(), time);
    }
    return time;
  }

  /**
   * Return true if any of the contained RecordSources are transactionally sensitive. If the
   * subclass has any knowledge about this beyond the status of the wrapped sources, then it should
   * override this method (remembering to call super as appropriate)
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    for (RecordSource recordSource : getRecordSources()) {
      if (recordSource.shouldRebuild(viewpoint)) {
        return true;
      }
    }
    return false;
  }
}
