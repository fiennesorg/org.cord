package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

/**
 * A RecordSourceWrapper that implements a view of a subsection of an existing RecordSource. The
 * fromIndex and toIndex are automatically constrained such that the viewpoint offered will never
 * thrown an indexing exception (although it may end up with zero records in it)
 * 
 * @author alex
 */
public class SubRecordSource
  extends RecordSourceWrapper
{
  private int __fromIndex;

  private int __toIndex;

  public SubRecordSource(Object cacheKey,
                         RecordSource wrappedSource,
                         int fromIndex,
                         int toIndex)
  {
    super(wrappedSource.getTable(),
          cacheKey,
          wrappedSource);
    __fromIndex = Math.max(0, fromIndex);
    __toIndex = Math.max(__fromIndex, toIndex);
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    IdList ids = getWrappedRecordSource().getIdList(viewpoint);
    return new ArrayIdList(getTable(),
                           ids.subList(Math.min(ids.size(), __fromIndex),
                                       Math.min(ids.size(), __toIndex)));
  }
}
