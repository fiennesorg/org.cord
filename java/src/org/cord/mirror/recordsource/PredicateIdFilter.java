package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.util.PredicateInt;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * RecordSource filter that uses a Predicate Integer to decide whether a given id should be
 * included.
 * 
 * @author alex
 */
public class PredicateIdFilter
  extends RecordSourceWrapper
{
  private final PredicateInt __filter;

  public PredicateIdFilter(RecordSource wrappedSource,
                           PredicateInt filter,
                           Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
    __filter = Preconditions.checkNotNull(filter, "filter");
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    IdList wrappedIds = getWrappedRecordSource().getIdList(viewpoint);
    ArrayIdList filteredIds = new ArrayIdList(getTable(), wrappedIds.size());
    for (IntIterator i = wrappedIds.iterator(); i.hasNext();) {
      int id = i.nextInt();
      if (__filter.apply(id)) {
        filteredIds.add(id);
      }
    }
    return filteredIds;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper("PredicateIdFilter")
                      .addValue(getWrappedRecordSource())
                      .addValue(__filter)
                      .toString();
  }

}
