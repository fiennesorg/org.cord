package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntLists;

/**
 * Unmodifiable IdList that contains no ids at all. You shouldn't need to create new instances of
 * this, but can instead just use {@link Table#getEmptyIds()}
 */
public class EmptyIdList
  extends IntLists.EmptyList
  implements IdList
{
  private static final long serialVersionUID = 4584852701127863578L;
  private final Table __table;

  public EmptyIdList(Table table)
  {
    __table = table;
  }

  @Override
  public Table getTable()
  {
    return __table;
  }
}
