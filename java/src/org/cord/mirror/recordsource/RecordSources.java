package org.cord.mirror.recordsource;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.util.Assertions;
import org.cord.util.CollectionUtil;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntLinkedOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Utility methods for doing useful things with a RecordSource
 * 
 * @author alex
 */
public class RecordSources
{
  /**
   * Get the first record in any given RecordSource. This will automatically handle
   * MissingRecordException problems with parallel deletes by repeatedly trying until successfull or
   * the RecordSource is empty.
   * 
   * @param recordSource
   *          The compulsory RecordSource whose first record we want
   * @param viewpoint
   *          The optional viewpoint to use when resolving the contents of the recordSource
   * @return The first record or null if the recordSource is empty.
   * @throws MissingRecordException
   *           if the state of the first record changes during the operation.
   */
  public static PersistentRecord getOptFirstRecord(RecordSource recordSource,
                                                   Viewpoint viewpoint)
      throws MissingRecordException
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    if (recordList.size() == 0) {
      return null;
    }
    return recordList.get(0);
  }

  public static Optional<PersistentRecord> optFirstRecord(RecordSource recordSource,
                                                          Viewpoint viewpoint)
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    if (recordList.size() == 0) {
      return Optional.absent();
    }
    return Optional.of(recordList.get(0));
  }

  /**
   * Get the first record from the given recordSource and throw a MirrorNoSuchRecordException if it
   * cannot be found. This is equivalent to the legacy Query.getFirstRecord(viewpoint) behaviour.
   * This is useful in situations when it is possible that there isn't any records in the
   * recordSource, and you want someone to deal with this eventuality, but you are not able to or
   * are not prepared to deal with it in the invoking method.
   * 
   * @param recordSource
   *          The compulsory recordSource whose first record we are interested in
   * @param viewpoint
   *          The optional transactional viewpoint onto the recordSource
   * @return The appropriate first record. Never null
   * @throws MissingRecordException
   *           If there is a runtime problem resolving the contents of the recordSource which
   *           normally implies that the data has been changed in another thread. This would not
   *           normally be caught
   * @throws MirrorNoSuchRecordException
   *           If the recordSource is empty from the transactional viewpoint
   * @see #getOptFirstRecord(RecordSource, Viewpoint)
   */
  public static PersistentRecord getFirstRecord(RecordSource recordSource,
                                                Viewpoint viewpoint)
      throws MissingRecordException, MirrorNoSuchRecordException
  {
    PersistentRecord record = getOptFirstRecord(recordSource, viewpoint);
    if (record == null) {
      throw new MirrorNoSuchRecordException(recordSource
                                            + " has 0 records and therefore has no firstRecord",
                                            recordSource.getTable());
    }
    return record;
  }

  /**
   * @param errorMessage
   *          The optional error message that will be used in the MissingRecordException if there
   *          are no records in the recordSource.
   * @param errorMessageParams
   *          The optional params that will be used to format errorMessage if defined and required.
   * @return The first record. Never null
   * @throws MissingRecordException
   *           If there was not a first record.
   * @see String#format(java.lang.String, java.lang.Object[])
   */
  public static PersistentRecord getCompFirstRecord(RecordSource recordSource,
                                                    Viewpoint viewpoint,
                                                    String errorMessage,
                                                    Object... errorMessageParams)
      throws MissingRecordException
  {
    PersistentRecord record = getOptFirstRecord(recordSource, viewpoint);
    if (record != null) {
      return record;
    }
    throw new MissingRecordException(StringUtils.format(errorMessage,
                                                        errorMessageParams,
                                                        "%s has 0 records and therefore has no firstRecord",
                                                        recordSource),
                                     recordSource.getTable(),
                                     null,
                                     null);
  }

  /**
   * Get the first record in the given RecordSource and throw a {@link MissingRecordException} with
   * an auto-generated error message if the RecordSource is empty.
   * 
   * @return the first record, never null
   */
  public static PersistentRecord getCompFirstRecord(RecordSource recordSource,
                                                    Viewpoint viewpoint)
      throws MissingRecordException
  {
    return getCompFirstRecord(recordSource, viewpoint, null);
  }

  /**
   * Get the last record in any given RecordSource. This will automatically handle
   * MissingRecordException problems with parallel deletes by repeatedly trying until successfull or
   * the RecordSource is empty.
   * 
   * @param recordSource
   *          The compulsory RecordSource whose first record we want
   * @param viewpoint
   *          The optional viewpoint to use when resolving the contents of the recordSource
   * @return The last record or null if the recordSource is empty.
   * @throws MissingRecordException
   *           If the state of the last record changes during the operation
   */
  public static PersistentRecord getOptLastRecord(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    if (recordList.size() == 0) {
      return null;
    }
    return recordList.get(recordList.size() - 1);
  }

  public static PersistentRecord getLastRecord(RecordSource recordSource,
                                               Viewpoint viewpoint)
      throws MirrorNoSuchRecordException, MissingRecordException
  {
    PersistentRecord record = getOptLastRecord(recordSource, viewpoint);
    if (record != null) {
      return record;
    }
    throw new MirrorNoSuchRecordException(String.format("%s has 0 records and therefore has no last Record",
                                                        recordSource),
                                          recordSource.getTable());
  }

  public static PersistentRecord getCompLastRecord(RecordSource recordSource,
                                                   Viewpoint viewpoint)
  {
    return getCompLastRecord(recordSource, viewpoint, null);
  }

  /**
   * @param errorMessage
   *          The optional error message that will be used in the MissingRecordException if there
   *          are no records in the recordSource.
   * @param errorMessageParams
   *          The optional params that will be used to format errorMessage if defined and required.
   * @see String#format(java.lang.String, java.lang.Object[])
   * @return The last record. Never null
   * @throws MissingRecordException
   *           If there was not a last record.
   */
  public static PersistentRecord getCompLastRecord(RecordSource recordSource,
                                                   Viewpoint viewpoint,
                                                   String errorMessage,
                                                   Object... errorMessageParams)
      throws MissingRecordException
  {
    PersistentRecord record = getOptLastRecord(recordSource, viewpoint);
    if (record == null) {
      throw new MissingRecordException(StringUtils.format(errorMessage,
                                                          errorMessageParams,
                                                          "%s has 0 records and therefore has no last Record",
                                                          recordSource),
                                       recordSource.getTable(),
                                       null,
                                       null);
    }
    return record;
  }

  /**
   * Get the only record that is in the RecordSource or throw a MirrorLogicException if there is not
   * exactly one record.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain exactly 1 record
   * @param viewpoint
   *          The optional viewpoint to resolve the record.
   * @return The only record in the recordSource. Never null;
   * @throws MissingRecordException
   *           if the recordSource's only record is deleted in parallel with resolving it. This
   *           wouldn't normally be caught as it is a "runtime" issue.
   * @throws MirrorLogicException
   *           if the recordSource doesn't contain exactly 1 resolveable record. You wouldn't
   *           normally expect to catch this as you would expect the recordSource to contain the
   *           "correct" number of records.
   */
  public static PersistentRecord getOnlyRecord(RecordSource recordSource,
                                               Viewpoint viewpoint)
      throws MirrorLogicException, MissingRecordException
  {
    return getOnlyRecord(recordSource, viewpoint, null);
  }

  /**
   * Get the only record that is in the RecordSource or throw an annotated MirrorLogicException if
   * there is not exactly one record.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain exactly 1 record
   * @param viewpoint
   *          The optional viewpoint to resolve the record.
   * @param errorMessage
   *          The optional error message that will be used in the MirrorLogicException if there is
   *          not exactly 1 record in the recordSource.
   * @param errorMessageParams
   *          The optional params that will be used to format errorMessage if defined and required.
   * @return The only record in the recordSource. Never null;
   * @throws MissingRecordException
   *           if the recordSource's only record is deleted in parallel with resolving it. This
   *           wouldn't normally be caught as it is a "runtime" issue.
   * @throws MirrorLogicException
   *           if the recordSource doesn't contain exactly 1 resolveable record. You wouldn't
   *           normally expect to catch this as you would expect the recordSource to contain the
   *           "correct" number of records.
   */
  public static PersistentRecord getOnlyRecord(RecordSource recordSource,
                                               Viewpoint viewpoint,
                                               String errorMessage,
                                               Object... errorMessageParams)
      throws MirrorLogicException, MissingRecordException
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    if (recordList.size() == 1) {
      return recordList.get(0);
    }
    throw new MirrorLogicException(StringUtils.format(errorMessage,
                                                      errorMessageParams,
                                                      "getOnlyRecord: %s should only have 1 record but it contains %s records: %s",
                                                      recordSource,
                                                      Integer.valueOf(recordList.size()),
                                                      recordList),
                                   null);
  }

  /**
   * Get the only record id that is in the RecordSource or throw an annotated MirrorLogicException
   * if there is not exactly one record.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain exactly 1 record
   * @param viewpoint
   *          The optional viewpoint to resolve the record.
   * @param errorMessage
   *          The optional error message that will be used in the MirrorLogicException if there is
   *          not exactly 1 record in the recordSource.
   * @param errorMessageParams
   *          The optional params that will be used to format errorMessage if defined and required.
   * @return The only record id in the recordSource. Never null;
   * @throws MirrorLogicException
   *           if the recordSource doesn't contain exactly 1 resolveable record. You wouldn't
   *           normally expect to catch this as you would expect the recordSource to contain the
   *           "correct" number of records.
   */
  public static int getOnlyId(RecordSource recordSource,
                              Viewpoint viewpoint,
                              String errorMessage,
                              Object... errorMessageParams)
      throws MirrorLogicException
  {
    IdList idList = recordSource.getIdList(viewpoint);
    if (idList.size() == 1) {
      return idList.getInt(0);
    }
    throw new MirrorLogicException(StringUtils.format(errorMessage,
                                                      errorMessageParams,
                                                      "getOnlyRecord: %s should only have 1 record but it contains %s records: %s",
                                                      recordSource,
                                                      Integer.valueOf(idList.size()),
                                                      idList),
                                   null);
  }

  /**
   * Get the only record id that is in the RecordSource or throw an annotated MirrorLogicException
   * if there is not exactly one record.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain exactly 1 record
   * @param viewpoint
   *          The optional viewpoint to resolve the record.
   * @return The only record id in the recordSource.;
   * @throws MirrorLogicException
   *           if the recordSource doesn't contain exactly 1 resolveable record. You wouldn't
   *           normally expect to catch this as you would expect the recordSource to contain the
   *           "correct" number of records.
   */
  public static int getOnlyId(RecordSource recordSource,
                              Viewpoint viewpoint)
      throws MirrorLogicException, MissingRecordException
  {
    return getOnlyId(recordSource, viewpoint, null);
  }

  /**
   * Either get the only record that is in the RecordSource, or return null if there are no records
   * or throw a MirrorLogicException if there is more than 1 record. This will use the default error
   * message from getOptOnlyRecord(recordSource, viewpoint, logicErrorMessage) for the
   * MirrorLogicException if required.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain zero or one record.
   * @param viewpoint
   *          The optional viewpoint to resolve the record
   * @return The only record or null if there are no records.
   * @throws MirrorLogicException
   *           If there is more than 1 record in the recordSource
   * @throws MissingRecordException
   *           If the only record is deleted in parallel with resolving it.
   * @see #getOptOnlyRecord(RecordSource, Viewpoint, String, Object[])
   */
  public static PersistentRecord getOptOnlyRecord(RecordSource recordSource,
                                                  Viewpoint viewpoint)
      throws MirrorLogicException, MissingRecordException
  {
    return getOptOnlyRecord(recordSource, viewpoint, null);
  }

  /**
   * Either get the only record that is in the RecordSource, or return null if there are no records
   * or throw a MirrorLogicException if there is more than 1 record.
   * 
   * @param recordSource
   *          The compulsory recordSource that should contain zero or one record.
   * @param viewpoint
   *          The optional viewpoint to resolve the record
   * @param errorMessage
   *          The optional error message that will be used in the MirrorLogicException if there is
   *          more than 1 record in the recordSource.
   * @param errorMessageParams
   *          The optional params that will be used to format errorMessage if defined and required.
   * @see String#format(java.lang.String, java.lang.Object[])
   * @return The only record or null if there are no records.
   * @throws MirrorLogicException
   *           If there is more than 1 record in the recordSource
   * @throws MissingRecordException
   *           If the only record is deleted in parallel with resolving it.
   */
  public static PersistentRecord getOptOnlyRecord(RecordSource recordSource,
                                                  Viewpoint viewpoint,
                                                  String errorMessage,
                                                  Object... errorMessageParams)
      throws MirrorLogicException, MissingRecordException
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    switch (recordList.size()) {
      case 0:
        return null;
      case 1:
        return recordList.get(0);
    }
    throw new MirrorLogicException(StringUtils.format(errorMessage,
                                                      errorMessageParams,
                                                      "getOptOnlyRecord: %s should have 0 to 1 records but it contains %s records: %s",
                                                      recordSource,
                                                      Integer.valueOf(recordList.size()),
                                                      recordList),
                                   null);
  }

  /**
   * Return an Optional wrapper around the only PersistentRecord in the RecordSource with a
   * customisable MirrorLogicException thrown if there is more than one record.
   * 
   * @throws MirrorLogicException
   *           If there is more than 1 record in recordSource
   * @throws MissingRecordException
   *           If the referenced record in RecordSource has disappeared when we try and resolve it.
   */
  public static Optional<PersistentRecord> optOnlyRecord(RecordSource recordSource,
                                                         Viewpoint viewpoint,
                                                         String errorMessage,
                                                         Object... errorMessageParams)
      throws MirrorLogicException, MissingRecordException
  {
    RecordList recordList = recordSource.getRecordList(viewpoint);
    switch (recordList.size()) {
      case 0:
        return Optional.absent();
      case 1:
        return Optional.of(recordList.get(0));
    }
    throw new MirrorLogicException(StringUtils.format(errorMessage,
                                                      errorMessageParams,
                                                      "optOnlyRecord: %s should have 0 to 1 records but it contains %s records: %s",
                                                      recordSource,
                                                      Integer.valueOf(recordList.size()),
                                                      recordList),
                                   null);
  }

  /**
   * Return an Optional wrapper around the only PersistentRecord in the RecordSource with a
   * MirrorLogicException thrown if there is more than one record.
   * 
   * @throws MirrorLogicException
   *           If there is more than 1 record in recordSource
   * @throws MissingRecordException
   *           If the referenced record in RecordSource has disappeared when we try and resolve it.
   */
  public static Optional<PersistentRecord> optOnlyRecord(RecordSource recordSource,
                                                         Viewpoint viewpoint)
      throws MirrorLogicException, MissingRecordException
  {
    return optOnlyRecord(recordSource, viewpoint, null);
  }

  /**
   * If the given recordSource is either a Query or RecordSourceViewpoint around a Query then return
   * the Query component otherwise return null.
   * 
   * @return The Query that the recordSource represents or null if it doesn't represent a Query.
   */
  public static Query asQuery(RecordSource recordSource)
  {
    if (recordSource instanceof RecordSourceViewpoint) {
      return asQuery(((RecordSourceViewpoint) recordSource).getWrappedRecordSource());
    }
    if (recordSource instanceof Query) {
      return (Query) recordSource;
    }
    return null;
  }

  /**
   * Force any RecordSource into a Query that can be resolved against the database. This is done by
   * creating a Query that matches "tablename.id in ( a,b,c...)" where the ids are supplied by the
   * recordSource parameter. If all you want to do is resolve the contents of the RecordSource then
   * this is an utterly pointless operation and you should just resolve the original recordSource.
   * However, if you want to further manipulate the Query using the SQL database to perform the
   * transform then the resulting QueryWrapper can be utilised for this purpose.
   * <p>
   * The only issue to be aware of is that at present the resulting Query is only sensitive to
   * changes in the Table which it is returning records from. Therefore if you generate a Query from
   * a RecordSource that is sensitive to multiple tables then there is a chance that the resulting
   * Query will miss updates to Tables and would therefore contain stale data. It is therefore
   * unwise to cache the result of this call rather than resolving it whenever you need it.
   * 
   * @return A new Query around the recordSource, or the recordSource itself if it is already
   *         directly resolveable into a QueryWrapper
   * @see #asQuery(RecordSource)
   */
  public static Query convertToQuery(RecordSource recordSource)
  {
    Query query = asQuery(recordSource);
    if (query != null) {
      return query;
    }
    Table table = recordSource.getTable();
    IdList ids = recordSource.getIdList();
    StringBuilder buf = new StringBuilder(table.getName().length() + 10 + ids.size() * 5);
    buf.append(table.getName()).append(".id ");
    if (ids.isEmpty()) {
      buf.append("is null");
    } else {
      buf.append("in (");
      for (IntIterator i = ids.iterator(); i.hasNext();) {
        buf.append(i.nextInt());
        if (i.hasNext()) {
          buf.append(',');
        }
      }
      buf.append(")");
    }
    query = table.getQuery(null, buf.toString(), null);
    uncache(query);
    return query;
  }

  /**
   * Check to see if a recordSource contains a given id from a given perspective.
   * 
   * @param recordSource
   *          If null then returns false (because nothing can't contain anything)
   * @param id
   *          If null then resturns false (because anything can't contain nothing)
   * @param viewpoint
   *          Optional viewpoint
   * @return true if the id is contained within recordSource
   */
  public static boolean contains(RecordSource recordSource,
                                 Integer id,
                                 Viewpoint viewpoint)
  {
    if (recordSource == null || id == null) {
      return false;
    }
    return recordSource.getIdList(viewpoint).contains(id);
  }

  /**
   * Check to see if a recordSource contains a given record from a given perspective. Note that it
   * doesn't pay any attention as to whether the record you pass in is Writable or not - it is only
   * the id and the containing Table which is important.
   * 
   * @param recordSource
   *          If null then returns false (because nothing can't contain anything)
   * @param record
   *          If null then resturns false (because anything can't contain nothing). If from a
   *          different Table then returns false.
   * @param viewpoint
   *          Optional viewpoint
   * @return true if the record is contained within recordSource
   */
  public static boolean contains(RecordSource recordSource,
                                 PersistentRecord record,
                                 Viewpoint viewpoint)
  {
    if (recordSource == null || record == null) {
      return false;
    }
    if (!recordSource.getTable().equals(record.getTable())) {
      return false;
    }
    return recordSource.getIdList(viewpoint).contains(record.getId());
  }

  /**
   * Check to see whether the two given RecordSources are equal with both being null considered not
   * equal.
   * 
   * @param rs0
   *          The RecordSource to compare. Possibly null
   * @param rs1
   *          The RecordSource to compare. Possibly null
   * @return true if both RecordSources are defined and if they have the same value for getName()
   * @see RecordOrdering#getName()
   */
  public static boolean isEqualOrdering(RecordOrdering rs0,
                                        RecordOrdering rs1)
  {
    if (rs0 == null || rs1 == null) {
      return false;
    }
    return rs0.getName().equals(rs1.getName());
  }

  /**
   * Apply the specified RecordOdering to the given RecordSource if necessary
   * 
   * @param recordSource
   *          The compulsory RecordSource to reorder
   * @param recordOrdering
   *          The optional ordering. null will return the recordSource unchanged
   * @return Either the original recordSource (if recordOrdering is either null or equal to the
   *         existing RecordOrdering on recordSource) or the re-ordered RecordSource.
   * @see #isEqualOrdering(RecordOrdering, RecordOrdering)
   * @see RecordSourceOrdering
   */
  public static RecordSource applyRecordOrdering(RecordSource recordSource,
                                                 RecordOrdering recordOrdering)
  {
    Preconditions.checkNotNull(recordSource, "cannot reorder a null recordSource");
    RecordOrdering existingRecordOrdering = recordSource.getRecordOrdering();
    if (recordOrdering == null || isEqualOrdering(existingRecordOrdering, recordOrdering)) {
      return recordSource;
    }
    return new RecordSourceOrdering(recordSource.getTable(), null, recordSource, recordOrdering);
  }

  /**
   * If it is possible that the RecordSource may be transaction sensitive then does the RecordSource
   * reference Tables that have also been updated by the Viewpoint? This is evaluated for
   * CompoundRecordSources as to whether any of the related sub-RecordSources (eg in the case of a
   * Query whether any of the referenced Tables) intersect with the updated Tables, or for all other
   * RecordSources whether getTable() is in the list of updated Tables.
   * 
   * @param mayBeTransactionSensitive
   *          Is it theoretically possible that the recordSource may be sensitive? If false then the
   *          result will always be false. If true then the method will try and determine whether it
   *          is.
   * @param recordSource
   *          Compulsory RecordSource that we are interesting in evaluating.
   * @param viewpoint
   *          Optional viewpoint that want to know if it is sensitive to. If null then it isn't and
   *          will therefore return false.
   * @return true if there is a possibility that the Viewpoint has updated records that have an
   *         effect on the contents of recordSource.
   * @see CompoundRecordSource#getRecordSources()
   * @see RecordSource#getTable()
   * @see Viewpoint#getUpdatedTables()
   */
  public static boolean isTransactionSensitive(boolean mayBeTransactionSensitive,
                                               RecordSource recordSource,
                                               Viewpoint viewpoint)
  {
    if (mayBeTransactionSensitive & viewpoint != null) {
      Preconditions.checkNotNull(recordSource, "recordSource");
      if (recordSource instanceof CompoundRecordSource) {
        return CollectionUtil.hasIntersection(((CompoundRecordSource) recordSource).getRecordSources(),
                                              viewpoint.getUpdatedTables());
      }
      return viewpoint.getUpdatedTables().contains(recordSource.getTable());
    }
    return false;
  }

  /**
   * Return a RecordSource that represents a given RecordSource from a persistent given Viewpoint.
   * This will be via a RecordSourceViewpoint unless the requested viewpoint is null in which case
   * it is OK to return the original recordSource, or if the original recordSource was already a
   * RecordSourceViewpoint that already had the required viewpoint in which case the original
   * recordSource is still returned.
   * 
   * @param recordSource
   *          The recordSource to transform. Not null
   * @param viewpoint
   *          The optional Viewpoint to transform recordSource
   * @return The appropriate RecordSource or RecordSourceViewpoint. Never null
   */
  public static RecordSource viewedFrom(RecordSource recordSource,
                                        Viewpoint viewpoint)
  {
    Preconditions.checkNotNull(recordSource, "recordSource");
    if (viewpoint == null) {
      return recordSource;
    }
    if (recordSource instanceof RecordSourceViewpoint
        && viewpoint.equals(((RecordSourceViewpoint) recordSource).getTransaction())) {
      return recordSource;
    }
    return new RecordSourceViewpoint(recordSource, viewpoint);
  }

  /**
   * Get the encapsulated Viewpoint from the given RecordSource. If the RecordSource is a
   * RecordSourceViewpoint then grab the nested Viewpoint. We'll extend this method in the future if
   * there are alternative ways of nesting a Viewpoint in future development.
   * 
   * @param recordSource
   *          The RecordSource to process. If null then the result will be null.
   * @return The Viewpoint if it is possible to extract, or null if this is not the case.
   */
  public static Viewpoint getViewpoint(RecordSource recordSource)
  {
    if (recordSource != null) {
      if (recordSource instanceof RecordSourceViewpoint) {
        return ((RecordSourceViewpoint) recordSource).getTransaction();
      }
    }
    return null;
  }

  /**
   * Return the requestedViewpoint unless it is not defined in which case get the encapsulated
   * Viewpoint inside the recordSource.
   * 
   * @param recordSource
   *          The compulsory RecordSource whose Viewpoint will be returned if requestedViewpoint is
   *          not defined.
   * @param requestedViewpoint
   *          The Viewpoint that will be returned if it is defined.
   * @return The appropriate Viewpoint or null if requestedViewpoint was not defined and
   *         recordSource didn't encapsulate a Viewpoint
   * @see #getViewpoint(RecordSource)
   */
  public static Viewpoint getViewpoint(RecordSource recordSource,
                                       Viewpoint requestedViewpoint)
  {
    Preconditions.checkNotNull(recordSource, "recordSource");
    if (requestedViewpoint != null) {
      return requestedViewpoint;
    }
    return getViewpoint(recordSource);
  }

  /**
   * Check that the given RecordSource is from the given Table.
   * 
   * @param recordSource
   *          The compulsory RecordSource
   * @param table
   *          The compulsory Table
   * @return recordSource
   * @throws RuntimeException
   *           if the Assertion is not satisfied
   */
  public static RecordSource assertIsTable(RecordSource recordSource,
                                           Table table)
  {
    if (Assertions.ASSERTIONS_ENABLED) {
      if (recordSource == null) {
        throw new NullPointerException(String.format("null is not a valid RecordSource from %s",
                                                     table));
      }
      Table recordSourceTable = recordSource.getTable();
      Assertions.isTrue(recordSourceTable.equals(table),
                        "%s.getTable() is %s rather than %s",
                        recordSource,
                        recordSourceTable,
                        table);
    }
    return recordSource;
  }

  /**
   * Check that the given RecordSource is from a Table with the given name.
   * 
   * @param recordSource
   *          The compulsory RecordSource
   * @param tableName
   *          The compulsory Table name
   * @return recordSource
   * @throws RuntimeException
   *           if the Assertion is not satisfied
   */
  public static RecordSource assertIsTableNamed(RecordSource recordSource,
                                                String tableName)
  {
    if (Assertions.ASSERTIONS_ENABLED) {
      if (recordSource == null) {
        throw new NullPointerException(String.format("null is not a valid RecordSource from %s",
                                                     tableName));
      }
      String recordSourceTableName = recordSource.getTable().getName();
      Assertions.isTrue(recordSourceTableName.equals(tableName),
                        "%s.getTable().getName() is %s rather than %s",
                        recordSource,
                        recordSourceTableName,
                        tableName);
    }
    return recordSource;
  }

  /**
   * If the recordSource is defined and if it has a cacheKey then remove it from the RecordSource
   * cache on the appropriate Table
   * 
   * @param recordSource
   *          The optional RecordSource to uncache.
   */
  public static void uncache(RecordSource recordSource)
  {
    if (recordSource == null) {
      return;
    }
    Object cacheKey = recordSource.getCacheKey();
    if (cacheKey == null) {
      return;
    }
    recordSource.getTable().invalidateCachedRecordSource(cacheKey);
  }

  /**
   * Generate a RecordSource that is the result of following the named link on all of the records in
   * the original RecordSource. The resulting records will be in the same order as the original
   * source, and may include duplicates depending on the logic behind the link field that is being
   * followed. The resulting RecordSource will be rebuilt whenever the original RecordSource becomes
   * invalid.
   */
  public static RecordSource followLink(final RecordSource recordSource,
                                        final FieldKey<Integer> linkFieldName)
  {
    return followLink(recordSource,
                      ObjectUtil.castTo(recordSource.getTable().getField(linkFieldName),
                                        LinkField.class));
  }

  public static RecordSource followLink(final RecordSource recordSource,
                                        String linkFieldName)
  {
    return followLink(recordSource,
                      ObjectUtil.castTo(recordSource.getTable().getField(linkFieldName),
                                        LinkField.class));
  }

  private static RecordSource followLink(final RecordSource recordSource,
                                         final LinkField linkField)
  {
    return new RecordSourceWrapper(linkField.getTargetTable(), null, recordSource) {
      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        RecordList wrappedRecords = getWrappedRecordSource().getRecordList(viewpoint);
        ArrayIdList targetIds = new ArrayIdList(getTable(), wrappedRecords.size());
        for (PersistentRecord wrappedRecord : wrappedRecords) {
          targetIds.add(linkField.getIntValue(wrappedRecord));
        }
        return targetIds;
      }

      @Override
      public String toString()
      {
        return String.format("FollowLink(%s, %s)", recordSource, linkField);
      }
    };

  }

  /**
   * Resolve a multi-parameter RecordSourceOperation against the given RecordSource.
   */
  public static Object get(RecordSource recordSource,
                           String name,
                           Object... params)
  {
    Object result = recordSource.get(name);
    if (!(result instanceof Gettable)) {
      throw new IllegalArgumentException(String.format("%s.%s is %s which is not Gettable",
                                                       recordSource,
                                                       name,
                                                       result));
    }
    for (int i = 0; i < params.length; i++) {
      result = ((Gettable) result).get(params[i].toString());
      if (i == params.length - 1) {
        return result;
      }
      if (!(result instanceof Gettable)) {
        throw new IllegalArgumentException(String.format("%s.%s.%s gives %s which is not Gettable",
                                                         recordSource,
                                                         name,
                                                         Arrays.toString(params),
                                                         result));
      }
    }
    throw new LogicException(String.format("%s.%s.%s should have terminated",
                                           recordSource,
                                           name,
                                           Arrays.toString(params)));
  }

  /**
   * Get the most recently changed timestamp from a list of TableWrappers. This can be useful for
   * calculating the getLastChangedTime() for a RecordSource implementation.
   */
  public static long getLastChangeTime(TableWrapper... tableWrappers)
  {
    long time = 0;
    for (TableWrapper tableWrapper : tableWrappers) {
      time = Math.max(time, tableWrapper.getTable().getLastChangeTime());
    }
    return time;
  }

  public static long getLastChangeTime(Iterator<TableWrapper> tableWrappers)
  {
    long time = 0;
    while (tableWrappers.hasNext()) {
      time = Math.max(time, tableWrappers.next().getTable().getLastChangeTime());
    }
    return time;
  }

  public static long getLastChangeTime(RecordSource... recordSources)
  {
    long time = 0;
    for (RecordSource recordSource : recordSources) {
      time = Math.max(time, recordSource.getLastChangeTime());
    }
    return time;
  }

  public static boolean containsRecordsFrom(Viewpoint viewpoint,
                                            TableWrapper... tableWrappers)
  {
    Set<Table> tables = viewpoint.getTables();
    for (TableWrapper tableWrapper : tableWrappers) {
      if (tables.contains(tableWrapper.getTable())) {
        return true;
      }
    }
    return false;
  }

  public static boolean containsRecordsFrom(Viewpoint viewpoint,
                                            Iterator<TableWrapper> tableWrappers)
  {
    Set<Table> tables = viewpoint.getTables();
    while (tableWrappers.hasNext()) {
      if (tables.contains(tableWrappers.next().getTable())) {
        return true;
      }
    }
    return false;
  }

  public static boolean shouldRebuild(Viewpoint viewpoint,
                                      RecordSource... recordSources)
  {
    for (RecordSource recordSource : recordSources) {
      if (recordSource.shouldRebuild(viewpoint)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Calculate the union between two RecordSources.
   */
  public static RecordSource union(final RecordSource firstRecordSource,
                                   final Viewpoint viewpoint,
                                   final RecordSource secondRecordSource)
  {
    Preconditions.checkNotNull(firstRecordSource, "firstRecordSource");
    Preconditions.checkNotNull(secondRecordSource, "secondRecordSource");
    final Table table = firstRecordSource.getTable();
    Preconditions.checkState(table.equals(secondRecordSource.getTable()),
                             "%s and %s are from different Tables",
                             firstRecordSource,
                             secondRecordSource);
    return new AbstractRecordSource(table, null) {
      @Override
      public boolean shouldRebuild(Viewpoint viewpoint)
      {
        return firstRecordSource.shouldRebuild(viewpoint)
               | secondRecordSource.shouldRebuild(viewpoint);
      }

      @Override
      public long getLastChangeTime()
      {
        return Math.max(firstRecordSource.getLastChangeTime(),
                        secondRecordSource.getLastChangeTime());
      }

      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        IdList firstIds = firstRecordSource.getIdList(viewpoint);
        IdList secondIds = secondRecordSource.getIdList(viewpoint);
        if (firstIds.size() == 0) {
          return secondIds;
        }
        if (secondIds.size() == 0) {
          return firstIds;
        }
        IntSet ids = new IntOpenHashSet();
        ids.addAll(firstIds);
        ids.addAll(secondIds);
        return new ArrayIdList(table, ids);
      }
    };
  }

  /**
   * Calculate the union between an arbitrary number of RecordSources.
   */
  public static RecordSource union(final RecordSource firstRecordSource,
                                   final Viewpoint viewpoint,
                                   final RecordSource... recordSources)
  {
    final Table table =
        Preconditions.checkNotNull(firstRecordSource, "firstRecordSource").getTable();
    switch (recordSources.length) {
      case 0:
        return viewedFrom(firstRecordSource, viewpoint);
      default:
        return new AbstractRecordSource(table, null) {

          @Override
          protected IdList buildIdList(Viewpoint viewpoint)
          {
            IntSet ids = new IntOpenHashSet();
            ids.addAll(firstRecordSource.getIdList(viewpoint));
            for (RecordSource recordSource : recordSources) {
              recordSource = assertIsTable(recordSource, table);
              ids.addAll(recordSource.getIdList(viewpoint));
            }
            return new ArrayIdList(table, ids);
          }

          @Override
          public long getLastChangeTime()
          {
            return Math.max(firstRecordSource.getLastChangeTime(),
                            RecordSources.getLastChangeTime(recordSources));
          }

          @Override
          public boolean shouldRebuild(Viewpoint viewpoint)
          {
            return firstRecordSource.shouldRebuild(viewpoint)
                   | RecordSources.shouldRebuild(viewpoint, recordSources);
          }

        };
    }
  }

  /**
   * Create a RecordSource that contains only the specified number of records from the start of the
   * given RecordSource. If there are less records than the requested records then only the records
   * present are returned. Note that this work takes place entirely in java space so if your
   * RecordSource is an SQL query then all of the ids will still be retrieved from the database.
   * 
   * @param recordSource
   *          The RecordSource to truncate
   * @param size
   *          The maximum number of Records in the limited RecordSource
   */
  public static RecordSource limit(final RecordSource recordSource,
                                   final int size)
  {
    return new RecordSourceWrapper(recordSource.getTable(), null, recordSource) {
      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        IdList wrappedIdList = getWrappedRecordSource().getIdList(viewpoint);
        if (wrappedIdList.size() <= size) {
          return wrappedIdList;
        }
        return new ArrayIdList(recordSource.getTable(), wrappedIdList.subList(0, size));
      }

      @Override
      public String toString()
      {
        return MoreObjects.toStringHelper("RecordSources.limit")
                          .add("source", recordSource)
                          .add("size", size)
                          .toString();
      }
    };
  }

  public static RecordSource stripDuplicates(RecordSource originalRecordSource,
                                             boolean preserveOrdering)
  {
    IdList originalIds = originalRecordSource.getIdList();
    final int originalIdsSize = originalIds.size();
    switch (originalIdsSize) {
      case 0:
      case 1:
        return originalRecordSource;
      default:
        IntSet uniqueIds = preserveOrdering
            ? new IntLinkedOpenHashSet(originalIds)
            : new IntOpenHashSet(originalIds);
        return uniqueIds.size() == originalIds.size()
            ? originalRecordSource
            : ConstantRecordSource.fromIds(originalRecordSource.getTable(), null, uniqueIds);
    }
  }
}
