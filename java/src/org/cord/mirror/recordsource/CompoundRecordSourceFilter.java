package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * A CompoundRecordSourceFilter is a RecordSource that wraps another RecordSource and provides a
 * filtered view onto it, with potentially input from other RecordSources as well. If further
 * RecordSources are added by subclasses then they will be included in the calculation as to when
 * this RecordSource needs rebuilding as defined in CompoundRecordSource.
 * 
 * @author alex
 * @see #accepts(PersistentRecord)
 * @see org.cord.mirror.recordsource.SingleRecordSourceFilter
 */
public abstract class CompoundRecordSourceFilter
  extends CompoundRecordSource
{
  private final RecordSource __filteredSource;

  /**
   * @param filteredSource
   *          The RecordSource that is going to be filtered by this RecordSourceFilter. Not null.
   */
  public CompoundRecordSourceFilter(RecordSource filteredSource,
                                    Object cacheKey)
  {
    super(Preconditions.checkNotNull(filteredSource, "filteredSource").getTable(),
          cacheKey);
    __filteredSource = filteredSource;
    addRecordSource(filteredSource);
  }

  /**
   * Get the nested RecordSource that is used as the source of the records to be filtered.
   * 
   * @return The RecordSource to be filtered. Not null
   */
  protected final RecordSource getFilteredSource()
  {
    return __filteredSource;
  }

  /**
   * Iterate through all the records in the filtered RecordSource and return a RecordIdList of all
   * the items that pass the accepts(record) filter.
   * 
   * @see #accepts(PersistentRecord)
   */
  @Override
  protected final IdList buildIdList(Viewpoint viewpoint)
  {
    RecordList records = getFilteredSource().getRecordList(viewpoint);
    ArrayIdList ids = new ArrayIdList(getTable(), records.size());
    for (PersistentRecord record : records) {
      if (accepts(record)) {
        ids.add(record.getId());
      }
    }
    return ids;
  }

  /**
   * Should the given record that was provided by the filtered RecordSource be part of the contents
   * of this RecordSource?
   * 
   * @return true if the given record is acceptable, false if it should be ommitted
   */
  protected abstract boolean accepts(PersistentRecord record);
}