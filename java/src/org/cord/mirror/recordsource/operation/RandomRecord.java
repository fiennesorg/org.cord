package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;

public class RandomRecord
  extends SimpleRecordSourceOperation<PersistentRecord>
{
  public static final RecordSourceOperationKey<PersistentRecord> NAME =
      RecordSourceOperationKey.create("randomRecord", PersistentRecord.class);

  public RandomRecord()
  {
    super(NAME);
  }

  public static PersistentRecord getRecord(RecordSource recordSource,
                                           Viewpoint viewpoint)
  {
    RecordList records = recordSource.getRecordList(viewpoint);
    if (records.size() == 0) {
      return null;
    }
    return records.get((int) (Math.random() * records.size()));
  }

  @Override
  public PersistentRecord invokeRecordSourceOperation(RecordSource recordSource,
                                                      Viewpoint viewpoint)
  {
    return getRecord(recordSource, viewpoint);
  }
}
