package org.cord.mirror.recordsource.operation;

import java.util.Random;

import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSourceWrapper;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntLists;

public class RandomOrder
  extends SimpleQueryOrRecordSourceOperation<RecordSource>
{
  public static final RecordSourceOperationKey<RecordSource> NAME =
      RecordSourceOperationKey.create("randomOrder", RecordSource.class);

  private final Random __random = new Random();

  public RandomOrder()
  {
    super(NAME,
          false);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(final RecordSource recordSource,
                                                  final Viewpoint viewpoint)
  {
    return new RecordSourceWrapper(recordSource.getTable(), null, recordSource) {
      private final long __creationTime = System.currentTimeMillis();

      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        return new ArrayIdList(getTable(),
                               IntLists.shuffle(new IntArrayList(recordSource.getIdList(viewpoint)),
                                                __random));
      }

      @Override
      public long getLastChangeTime()
      {
        return Math.max(__creationTime, super.getLastChangeTime());
      }

    };
  }

  @Override
  public boolean isTransactionSensitive(Query query,
                                        Viewpoint viewpoint)
  {
    return true;
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint)
  {
    throw new IllegalStateException(String.format("QueryBranch should never be invoked by %s on %s",
                                                  this,
                                                  query));
  }
}
