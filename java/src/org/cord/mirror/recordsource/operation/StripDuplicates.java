package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.StripDuplicatesWrapper;

/**
 * $recordSource.stripDuplicates removes any duplicate ids from $recordSource
 * 
 * @see StripDuplicatesWrapper
 * @author alex
 */
public class StripDuplicates
  extends SimpleRecordSourceOperation<RecordSource>
{
  public static final RecordSourceOperationKey<RecordSource> KEY =
      RecordSourceOperationKey.create("stripDuplicates", RecordSource.class);

  public StripDuplicates()
  {
    super(KEY);
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    return new StripDuplicatesWrapper(recordSource, null);
  }

}
