package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;

/**
 * $recordSource.numericalEquals.field.value
 * 
 * @author alex
 */
public class NumericalEquals
  extends NumericalComparisonFilter
{
  public static final DuoRecordSourceOperationKey<String, Object, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("numericalEquals",
                                         String.class,
                                         Object.class,
                                         RecordSource.class);

  public NumericalEquals()
  {
    super(NAME,
          "=");
  }

  @Override
  protected boolean accepts(int recordValue,
                            int paramValue)
  {
    return recordValue == paramValue;
  }

}
