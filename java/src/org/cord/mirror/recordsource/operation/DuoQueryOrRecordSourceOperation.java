package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Caster;

public abstract class DuoQueryOrRecordSourceOperation<A, B, V>
  extends DuoRecordSourceOperation<A, B, V>
{
  private final boolean __mayBeTransactionSensitive;

  public DuoQueryOrRecordSourceOperation(DuoRecordSourceOperationKey<A, B, V> key,
                                         Caster<A> casterA,
                                         Caster<B> casterB,
                                         boolean mayBeTransactionSensitive)
  {
    super(key,
          casterA,
          casterB);
    __mayBeTransactionSensitive = mayBeTransactionSensitive;
  }

  public boolean isTransactionSensitive(Query query,
                                        Viewpoint viewpoint)
  {
    return RecordSources.isTransactionSensitive(__mayBeTransactionSensitive, query, viewpoint);
  }

  @Override
  public V calculate(RecordSource recordSource,
                     Viewpoint viewpoint,
                     A a,
                     B b)
  {
    viewpoint = RecordSources.getViewpoint(recordSource, viewpoint);
    if (!recordSource.getTable().getDb().disableQueryBranch()) {
      Query query = RecordSources.asQuery(recordSource);
      if (query != null && !isTransactionSensitive(query, viewpoint)) {
        return invokeQueryBranch(query, viewpoint, a, b);
      }
    }
    return invokeRecordSourceBranch(recordSource, viewpoint, a, b);
  }

  protected abstract V invokeRecordSourceBranch(RecordSource recordSource,
                                                Viewpoint viewpoint,
                                                A a,
                                                B b);
  protected abstract V invokeQueryBranch(Query query,
                                         Viewpoint viewpoint,
                                         A a,
                                         B b);

}
