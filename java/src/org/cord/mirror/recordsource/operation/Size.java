package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;

public class Size
  extends SimpleRecordSourceOperation<Integer>
{
  public static final RecordSourceOperationKey<Integer> NAME =
      RecordSourceOperationKey.create("Size", Integer.class);

  public Size()
  {
    super(NAME);
  }

  @Override
  public Integer invokeRecordSourceOperation(RecordSource recordSource,
                                             Viewpoint viewpoint)
  {
    return Integer.valueOf(recordSource.getIdList(viewpoint).size());
  }
}
