package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Casters;

/**
 * RecordSourceOperation that permits the addition of a new SQL filter component to the existing
 * Query filter. If this is invoked on a RecordSource that is not resolveable to a Query then it
 * will be turned into a Query for he purpose of this transform. This operation may be quite
 * expensive and it is encouraged that it is only used as a "proof of concept". Once the
 * functionality is identified, then a dedicated RecordSourceOperation should be crafter that
 * provides a sensible implementation of the filter in java space as well as RecordSource space.
 * 
 * @author alex
 */
public class SqlFilter
  extends MonoQueryOrRecordSourceOperation<String, RecordSource>
{
  public final static MonoRecordSourceOperationKey<String, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("filter", String.class, RecordSource.class);

  public final static int P0_SQLFILTER = 0;

  public SqlFilter()
  {
    super(NAME,
          Casters.TOSTRING,
          false);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint,
                                                  String sqlFilter)
  {
    return Query.transformQuery(RecordSources.convertToQuery(recordSource),
                                sqlFilter,
                                Query.TYPE_AND,
                                null);
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String sqlFilter)
  {
    return Query.transformQuery(query, sqlFilter, Query.TYPE_AND, null);
  }
}
