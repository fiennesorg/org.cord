package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;

/**
 * A RecordSourceOperation that only ever makes a transform on a Query, automatically converting
 * non-Query RecordSources if invoked upon them. This can be very useful for converting legacy
 * QueryOperations that don't have an easy equivalent in java space. Care should be taken when using
 * it on large RecordSource arguments because it may prove to be innefficient.
 * 
 * @author alex
 * @see RecordSources#convertToQuery(RecordSource)
 */
@Deprecated
public abstract class AlwaysQueryRecordSourceOperation
  extends QueryOrRecordSourceOperation
{
  public AlwaysQueryRecordSourceOperation(String name,
                                          int paramCount)
  {
    super(name,
          paramCount,
          false);
  }

  /**
   * Convert the recordSource into a Query and then invoke invokeQueryBranch with the result.
   * 
   * @return the result of invoking inokeQueryBranch
   * @see RecordSources#convertToQuery(RecordSource)
   */
  @Override
  protected final Object invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint,
                                                  Object... p)
  {
    return invokeQueryBranch(RecordSources.convertToQuery(recordSource), viewpoint, p);
  }
}
