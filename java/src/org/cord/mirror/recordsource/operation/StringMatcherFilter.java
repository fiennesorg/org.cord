package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;

import com.google.common.base.Objects;

/**
 * FilterOperation of the form $recordSource.equals.fieldName.fieldValue that performs a String
 * comparison on the Field.
 */
public class StringMatcherFilter
  extends DuoFilterOperation<String, String>
{
  public final static DuoRecordSourceOperationKey<String, String, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("equals", String.class, String.class, RecordSource.class);

  public final static int P0_FIELDNAME = 0;

  public final static int P1_VALUE = 1;

  public StringMatcherFilter()
  {
    super(NAME,
          Casters.TOSTRING,
          Casters.TOSTRING,
          true);
  }

  @Override
  protected boolean accepts(PersistentRecord record,
                            String fieldName,
                            String value)
  {
    return Objects.equal(record.getString(fieldName), value);
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String fieldName,
                                           String value)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(fieldName).append('=');
    SqlUtil.toSafeSqlString(buf, value);
    return Query.transformQuery(query, buf.toString(), Query.TYPE_AND, null);
  }
}
