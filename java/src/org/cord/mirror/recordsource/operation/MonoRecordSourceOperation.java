package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperation;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractLockableOperation;
import org.cord.util.AbstractGettable;
import org.cord.util.Caster;

import com.google.common.base.Preconditions;

public abstract class MonoRecordSourceOperation<A, V>
  extends AbstractLockableOperation
  implements RecordSourceOperation<MonoRecordSourceOperation<A, V>.Invoker>
{
  private final MonoRecordSourceOperationKey<A, V> __key;
  private final Caster<A> __casterA;

  public MonoRecordSourceOperation(MonoRecordSourceOperationKey<A, V> key,
                                   Caster<A> casterA)
  {
    super(key.getKeyName());
    __key = key;
    __casterA = Preconditions.checkNotNull(casterA, "casterA");
  }

  public final MonoRecordSourceOperationKey<A, V> getKey()
  {
    return __key;
  }

  @Override
  public final Invoker invokeRecordSourceOperation(RecordSource recordSource,
                                                   Viewpoint viewpoint)
  {
    return new Invoker(recordSource, viewpoint);
  }

  public abstract V calculate(RecordSource recordSource,
                              Viewpoint viewpoint,
                              A a);

  public class Invoker
    extends AbstractGettable
  {
    private final RecordSource __recordSource;
    private final Viewpoint __viewpoint;

    protected Invoker(RecordSource recordSource,
                      Viewpoint viewpoint)
    {
      __recordSource = recordSource;
      __viewpoint = viewpoint;
    }

    public V call(A a)
    {
      return calculate(__recordSource, __viewpoint, a);
    }

    @Override
    public Object get(Object key)
    {
      A a;
      try {
        a = __casterA.cast(key);
      } catch (RuntimeException e) {
        throw new IllegalArgumentException(String.format("%s.%s was rejected by %s",
                                                         MonoRecordSourceOperation.this,
                                                         key,
                                                         __casterA),
                                           e);
      }
      return calculate(__recordSource, __viewpoint, a);
    }
  }

}
