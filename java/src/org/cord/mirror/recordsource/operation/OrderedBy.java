package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.util.Casters;

/**
 * RecordSourceOperation that provides $recordsSource.orderedBy.orderingClause reordering of items.
 * 
 * @author alex
 */
public class OrderedBy
  extends MonoQueryOrRecordSourceOperation<String, RecordSource>
{
  public static final MonoRecordSourceOperationKey<String, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("orderedBy", String.class, RecordSource.class);

  public static final int P0_ORDERINGCLAUSE = 0;

  public OrderedBy()
  {
    super(NAME,
          Casters.TOSTRING,
          true);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint,
                                                  String ordering)
  {
    Table table = recordSource.getTable();
    return new RecordSourceOrdering(table,
                                    null,
                                    recordSource,
                                    RecordSourceOrdering.createRecordOrdering(table.getName(),
                                                                              ordering,
                                                                              ordering));
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String ordering)
  {
    return Query.transformQuery(query, null, Query.TYPE_ORDEREDBY, ordering);
  }
}
