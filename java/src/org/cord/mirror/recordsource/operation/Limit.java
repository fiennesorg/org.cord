package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SubRecordSource;
import org.cord.util.Casters;

/**
 * Webmacro targetted operation that gives you $records.limit.n which returns a RecordSource that
 * contains the first n elements of the target RecordSource.
 * 
 * @author alex
 */
public class Limit
  extends MonoQueryOrRecordSourceOperation<Integer, RecordSource>
{
  public static final MonoRecordSourceOperationKey<Integer, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("limit", Integer.class, RecordSource.class);

  public static final int P0_RECORDCOUNT = 0;

  public Limit()
  {
    super(NAME,
          Casters.TOINT,
          false);
  }

  /**
   * Get a viewpoint wrapping of {@link #limitRecordSource(RecordSource, int)}
   */
  @Override
  protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint,
                                                  Integer recordCount)
  {
    return RecordSources.viewedFrom(limitRecordSource(recordSource, recordCount.intValue()),
                                    viewpoint);
  }

  /**
   * Create a limited view onto an existing RecordSource using a {@link SubRecordSource}. This will
   * resolve all the records in the original recordSource in order to generate the sublist.
   */
  public static RecordSource limitRecordSource(RecordSource recordSource,
                                               int recordCount)
  {
    return new SubRecordSource(null, recordSource, 0, recordCount);
  }

  /**
   * Invoke {@link #limitQuery(Query, int)} or {@link #limitRecordSource(RecordSource, int)} as
   * appropriate with the given recordSource.
   */
  public static RecordSource limit(RecordSource recordSource,
                                   int recordCount)
  {
    if (recordSource instanceof Query) {
      return limitQuery((Query) recordSource, recordCount);
    }
    return limitRecordSource(recordSource, recordCount);
  }

  /**
   * Return a viewpoint wrapping of {@link #limitQuery(Query, int)}
   */
  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           Integer recordCount)
  {
    return RecordSources.viewedFrom(limitQuery(query, recordCount.intValue()), viewpoint);
  }

  /**
   * Create a new Query from the given Query that adds a LIMIT clause onto the SQL.
   */
  public static Query limitQuery(Query query,
                                 int recordCount)
  {
    if (recordCount == query.getLimit()) {
      return query;
    }
    StringBuilder buf = new StringBuilder();
    buf.append(query.getCacheKey()).append(recordCount);
    String name = buf.toString();
    return query.getTable().getQuery(name,
                                     query.getFilter(),
                                     query.getOrder(),
                                     query.getGroupBy(),
                                     recordCount,
                                     query.getExplicitFrom(),
                                     query.getTableArray());
  }
}
