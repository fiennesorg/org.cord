package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;

public class NumericalGreaterThan
  extends NumericalComparisonFilter
{
  public final static DuoRecordSourceOperationKey<String, Object, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("numericalGreaterThan",
                                         String.class,
                                         Object.class,
                                         RecordSource.class);

  public NumericalGreaterThan()
  {
    super(NAME,
          ">");
  }

  @Override
  protected boolean accepts(int recordValue,
                            int paramValue)
  {
    return recordValue > paramValue;
  }
}
