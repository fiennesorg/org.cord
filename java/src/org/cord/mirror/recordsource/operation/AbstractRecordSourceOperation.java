package org.cord.mirror.recordsource.operation;

import java.util.Arrays;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperation;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractLockableOperation;
import org.cord.util.Assertions;
import org.cord.util.ParamsAccumulator;

/**
 * Generic parametric base class implementation of RecordSourceOperation that leaves subclasses with
 * only the task of implementing invokeRecordSourceOperation.
 * 
 * @author alex
 * @deprecated in preference of {@link SimpleRecordSourceOperation} and
 *             {@link MonoRecordSourceOperation}
 */
@Deprecated
public abstract class AbstractRecordSourceOperation
  extends AbstractLockableOperation
  implements RecordSourceOperation<Object>
{
  private final int __paramCount;

  /**
   * @param name
   *          The name by which this operation will be invoked
   * @param paramCount
   *          The number of parameters that this operation requires. This must not be negative. It
   *          will set the number of params that will be passed to invokeWithParams.
   */
  public AbstractRecordSourceOperation(String name,
                                       int paramCount)
  {
    super(name);
    Assertions.isTrue(paramCount >= 0, "paramCount must be >= 0");
    __paramCount = paramCount;
  }

  /**
   * @return If paramCount is 0 then the result of invoking invokeWithParams with no params,
   *         otherwise a Params object that will gather the appropriate number of params via the
   *         Gettable interface whereupon invokeWithParams will be invoked.
   */
  @Override
  public final Object invokeRecordSourceOperation(final RecordSource recordSource,
                                                  final Viewpoint viewpoint)
  {
    if (__paramCount == 0) {
      return invokeWithParams(recordSource, viewpoint, ParamsAccumulator.NOPARAMS);
    }
    return ParamsAccumulator.accumulate(__paramCount, new ParamsAccumulator.Invoker<Object>() {
      @Override
      public Object invoke(Object[] params)
      {
        return invokeWithParams(recordSource, viewpoint, params);
      }

      @Override
      public String toString()
      {
        return String.format("%s.%s", recordSource, getName());
      }
    });
  }

  /**
   * Get the number of params that this RecordSourceOperation expects to receive to be able to
   * invoke its functionality.
   * 
   * @return The required number of params. zero or more.
   */
  public final int getParamCount()
  {
    return __paramCount;
  }

  /**
   * Check that the number of params passed is equal to the number of params that this object is
   * expecting and call invokeWithParams. This is intended as a way of invoking the functionality
   * without having to jump through the webmacro-enforced ParamsAccumulator hoops and is more
   * effecient for direct java invocations.
   * 
   * @param recordSource
   *          The RecordSource that the operation is to be invoked on.
   * @param viewpoint
   *          The viewpoint to use when invoking the operation
   * @param params
   *          The parameters to apply to the operation. There should be paramCount of these.
   * @return The result of invoking the operation.
   * @see #invokeWithParams(RecordSource, Viewpoint, Object[])
   * @throws IllegalArgumentException
   *           if there are the wrong number of params.
   */
  public final Object invoke(RecordSource recordSource,
                             Viewpoint viewpoint,
                             Object... params)
  {
    if (params.length != __paramCount) {
      throw new IllegalArgumentException(String.format("%s expects %s params but received %s",
                                                       this,
                                                       Integer.toString(__paramCount),
                                                       Arrays.toString(params)));
    }
    return invokeWithParams(recordSource, viewpoint, params);
  }

  /**
   * Do whatever work is required to perform the functionality of this RecordSourceOperation.
   * 
   * @param recordSource
   *          The RecordSource that the operation is to be invoked on.
   * @param viewpoint
   *          The viewpoint to use when invoking the operation
   * @param params
   *          The parameters to apply to the operation. There should be paramCount of these.
   * @return The result of invoking the operation.
   */
  protected abstract Object invokeWithParams(RecordSource recordSource,
                                             Viewpoint viewpoint,
                                             Object... params);
}
