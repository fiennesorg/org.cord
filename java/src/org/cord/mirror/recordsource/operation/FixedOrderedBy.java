package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.util.StringUtils;

public class FixedOrderedBy
  extends SimpleQueryOrRecordSourceOperation<RecordSource>
{
  private final String __ordering;

  public FixedOrderedBy(RecordSourceOperationKey<RecordSource> name,
                        String ordering)
  {
    super(name,
          true);
    __ordering = ordering;
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    Table table = recordSource.getTable();
    return new RecordSourceOrdering(table,
                                    null,
                                    recordSource,
                                    RecordSourceOrdering.createRecordOrdering(table.getName(),
                                                                              __ordering,
                                                                              __ordering));
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint)
  {
    return Query.transformQuery(query,
                                null,
                                Query.TYPE_ORDEREDBY,
                                StringUtils.toString(__ordering));
  }
}
