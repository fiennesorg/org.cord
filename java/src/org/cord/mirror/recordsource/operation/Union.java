package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Casters;

public class Union
  extends MonoRecordSourceOperation<RecordSource, RecordSource>
{
  public static final MonoRecordSourceOperationKey<RecordSource, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("unionWith", RecordSource.class, RecordSource.class);

  public Union()
  {
    super(NAME,
          Casters.<RecordSource> CAST());
  }

  /**
   * @return the result of the {@link RecordSources#union(RecordSource, Viewpoint, RecordSource...)}
   *         between the invoking RecordSource and the first parameter.
   */
  @Override
  public RecordSource calculate(RecordSource recordSource,
                                Viewpoint viewpoint,
                                RecordSource a)
  {
    return RecordSources.union(recordSource, viewpoint, a);
  }
}
