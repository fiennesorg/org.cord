package org.cord.mirror.recordsource.operation;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Casters;

/**
 * $records.isnt.booleanFieldKey
 * 
 * @author alex
 */
public class BooleanNotFilter
  extends MonoFilterOperation<String>
{
  /**
   * "isnt"
   */
  public final static MonoRecordSourceOperationKey<String, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("isnt", String.class, RecordSource.class);

  private String _tableNameDot = null;

  public BooleanNotFilter()
  {
    super(NAME,
          Casters.TOSTRING,
          true);
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    _tableNameDot = getTable().getName() + ".";
  }

  @Override
  protected boolean accepts(PersistentRecord record,
                            String a)
  {
    return !((Boolean) record.get(a)).booleanValue();
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String a)
  {
    StringBuilder buf = new StringBuilder(40);
    buf.append(_tableNameDot).append(a).append("=0");
    return RecordSources.viewedFrom(Query.transformQuery(query,
                                                         buf.toString(),
                                                         Query.TYPE_AND,
                                                         null),
                                    viewpoint);
  }
}
