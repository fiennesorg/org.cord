package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;

public abstract class SimpleQueryOrRecordSourceOperation<V>
  extends SimpleRecordSourceOperation<V>
{
  private final boolean __mayBeTransactionSensitive;

  public SimpleQueryOrRecordSourceOperation(RecordSourceOperationKey<V> key,
                                            boolean mayBeTransactionSensitive)
  {
    super(key);
    __mayBeTransactionSensitive = mayBeTransactionSensitive;
  }

  /**
   * Is there a possibility that this operation may invoke the RecordSource branch on a Query if
   * invoked with the correct Transaction viewpoint?
   * 
   * @return The value passed to the constructor
   */
  public final boolean mayBeTransactionSensitive()
  {
    return __mayBeTransactionSensitive;
  }

  public boolean isTransactionSensitive(Query recordSource,
                                        Viewpoint viewpoint)
  {
    return RecordSources.isTransactionSensitive(__mayBeTransactionSensitive,
                                                recordSource,
                                                viewpoint);
  }

  @Override
  public V invokeRecordSourceOperation(RecordSource recordSource,
                                       Viewpoint viewpoint)
  {
    viewpoint = RecordSources.getViewpoint(recordSource, viewpoint);
    if (!recordSource.getTable().getDb().disableQueryBranch()) {
      Query query = RecordSources.asQuery(recordSource);
      if (query != null && !isTransactionSensitive(query, viewpoint)) {
        return invokeQueryBranch(query, viewpoint);
      }
    }
    return invokeRecordSourceBranch(recordSource, viewpoint);
  }

  protected abstract V invokeRecordSourceBranch(RecordSource recordSource,
                                                Viewpoint viewpoint);
  protected abstract V invokeQueryBranch(Query query,
                                         Viewpoint viewpoint);
}
