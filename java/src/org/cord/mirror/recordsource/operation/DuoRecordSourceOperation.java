package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperation;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractLockableOperation;
import org.cord.util.AbstractGettable;
import org.cord.util.Caster;

import com.google.common.base.Preconditions;

public abstract class DuoRecordSourceOperation<A, B, V>
  extends AbstractLockableOperation
  implements RecordSourceOperation<DuoRecordSourceOperation<A, B, V>.Invoker>
{
  private final DuoRecordSourceOperationKey<A, B, V> __key;
  private final Caster<A> __casterA;
  private final Caster<B> __casterB;

  public DuoRecordSourceOperation(DuoRecordSourceOperationKey<A, B, V> key,
                                  Caster<A> casterA,
                                  Caster<B> casterB)
  {
    super(key.getKeyName());
    __key = key;
    __casterA = Preconditions.checkNotNull(casterA, "casterA");
    __casterB = Preconditions.checkNotNull(casterB, "casterB");
  }

  public DuoRecordSourceOperationKey<A, B, V> getKey()
  {
    return __key;
  }

  public abstract V calculate(RecordSource recordSource,
                              Viewpoint viewpoint,
                              A a,
                              B b);

  @Override
  public Invoker invokeRecordSourceOperation(RecordSource recordSource,
                                             Viewpoint viewpoint)
  {
    return new Invoker(recordSource, viewpoint);
  }

  public class Invoker
    extends AbstractGettable
  {
    private final RecordSource __recordSource;
    private final Viewpoint __viewpoint;

    private Invoker(RecordSource recordSource,
                    Viewpoint viewpoint)
    {
      __recordSource = recordSource;
      __viewpoint = viewpoint;
    }

    @Override
    public Second get(Object key)
    {
      return new Second(__casterA.cast(key));
    }

    public class Second
      extends AbstractGettable
    {
      private final A __a;

      private Second(A a)
      {
        __a = a;
      }

      @Override
      public V get(Object key)
      {
        return calculate(__recordSource, __viewpoint, __a, __casterB.cast(key));
      }

    }
  }

}
