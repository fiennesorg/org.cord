package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;

/**
 * Filter that accepts records that have a given field that starts with a given value with the
 * comparison made in a case insensitive manner.
 * 
 * @author alex
 */
public class StartsWithFilter
  extends DuoFilterOperation<String, String>
{
  public static final DuoRecordSourceOperationKey<String, String, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("startsWith",
                                         String.class,
                                         String.class,
                                         RecordSource.class);

  public StartsWithFilter()
  {
    super(NAME,
          Casters.TOSTRING,
          Casters.TOSTRING,
          true);
  }

  @Override
  protected boolean accepts(PersistentRecord record,
                            String fieldName,
                            String startMatch)
  {
    return record.getString(fieldName).regionMatches(true, 0, startMatch, 0, startMatch.length());
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String fieldName,
                                           String startMatch)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(fieldName).append(" LIKE '");
    SqlUtil.escapeQuotes(buf, startMatch);
    buf.append("%'");
    return Query.transformQuery(query, buf.toString(), Query.TYPE_AND, null);
  }
}
