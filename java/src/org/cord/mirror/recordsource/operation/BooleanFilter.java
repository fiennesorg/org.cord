package org.cord.mirror.recordsource.operation;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Casters;

/**
 * ParametricFilterOperation that passes records which have a positive value for a given boolean
 * field.
 * 
 * @author alex
 */
public class BooleanFilter
  extends MonoFilterOperation<Object>
{
  public final static MonoRecordSourceOperationKey<Object, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("is", Object.class, RecordSource.class);

  private String _tableNameDot = null;

  public BooleanFilter()
  {
    super(NAME,
          Casters.NULL,
          true);
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    _tableNameDot = getTable().getName() + ".";
  }

  @Override
  protected boolean accepts(PersistentRecord record,
                            Object a)
  {
    return ((Boolean) record.get(a)).booleanValue();
  }

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           Object a)
  {
    StringBuilder buf = new StringBuilder(40);
    buf.append(_tableNameDot).append(a).append("!=0");
    return RecordSources.viewedFrom(Query.transformQuery(query,
                                                         buf.toString(),
                                                         Query.TYPE_AND,
                                                         null),
                                    viewpoint);
  }
}
