package org.cord.mirror.recordsource.operation;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSourceWrapper;

import it.unimi.dsi.fastutil.ints.IntArrays;

public class Reverse
  extends SimpleRecordSourceOperation<RecordSource>
{
  public static final RecordSourceOperationKey<RecordSource> NAME =
      RecordSourceOperationKey.create("reverse", RecordSource.class);

  public Reverse()
  {
    super(NAME);
  }

  @Override
  public RecordSource invokeRecordSourceOperation(final RecordSource recordSource,
                                                  final Viewpoint viewpoint)
  {
    return new RecordSourceWrapper(recordSource.getTable(), null, recordSource) {
      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        return new ArrayIdList(getTable(),
                               IntArrays.reverse(recordSource.getIdList(viewpoint).toIntArray()));
      }
    };
  }
}
