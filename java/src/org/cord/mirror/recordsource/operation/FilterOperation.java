package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;

/**
 * Abstract QueryOrRecordSourceOperation that either transforms a Query into another RecordSource or
 * passes each record through a java filter to decide whether it is eligible for inclusion in the
 * result. This therefore enables you to provide transactionally aware behaviour as to which records
 * in the original RecordSource pass through the filter.
 * 
 * @author alex
 */
@Deprecated
public abstract class FilterOperation
  extends QueryOrRecordSourceOperation
{
  public FilterOperation(String name,
                         int paramCount,
                         boolean mayBeTransactionSensitive)
  {
    super(name,
          paramCount,
          mayBeTransactionSensitive);
  }

  /**
   * @return A SingleRecordSourceFilter that only passes records that pass the accepts method
   *         defined in the subclass.
   * @see SingleRecordSourceFilter
   * @see #accepts(PersistentRecord, Object[])
   */
  @Override
  protected final RecordSource invokeRecordSourceBranch(final RecordSource recordSource,
                                                        final Viewpoint viewpoint,
                                                        final Object... p)
  {
    RecordSource result = new SingleRecordSourceFilter(recordSource, null) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        return FilterOperation.this.accepts(record, p);
      }
    };
    result = RecordSources.applyRecordOrdering(result, getRecordOrdering());
    return RecordSources.viewedFrom(result, viewpoint);
  }

  public final RecordSource applyFilter(RecordSource recordSource,
                                        Viewpoint viewpoint,
                                        Object... p)
  {
    return (RecordSource) invokeWithParams(recordSource, viewpoint, p);
  }

  /**
   * Does this FilterOperation accept the given record from the RecordSource that it is being
   * invoked on? This will get invoked for every record when invoked on a non-Query based
   * RecordSource or if it is transaction sensitive and invoked on a viewpoint based RecordSource.
   */
  protected abstract boolean accepts(PersistentRecord record,
                                     Object... p);
  /**
   * Get the RecordOrdering (if any) that should be applied to java transforms of this
   * FilterOperation. If the value is null then no re-ordering will take place and you will just
   * have the filtered view of the original RecordOrdering. If it is defined then
   * {@link #invokeRecordSourceBranch(RecordSource, Viewpoint, Object...)} will use the value to
   * re-order the filtered RecordSource. Please note that this has no value on the
   * {@link #invokeQueryBranch(Query, Viewpoint, Object...)} by default, although there is nothing
   * to stop implementations from using it if required.
   */
  protected RecordOrdering getRecordOrdering()
  {
    return null;
  }

  @Override
  protected final RecordSource invokeQueryBranch(Query query,
                                                 Viewpoint viewpoint,
                                                 Object... p)
  {
    return transformQuery(query, viewpoint, p);
  }

  /**
   * Perform whichever transformation this FilterOperation represents on the Query. This will be
   * called by invokeQueryBranch.
   * 
   * @return The transformed Query. This may be another Query or a RecordSource if the
   *         transformation is hard to express in Query terms.
   * @see #invokeQueryBranch(Query, Viewpoint, Object[])
   */
  protected abstract RecordSource transformQuery(Query query,
                                                 Viewpoint viewpoint,
                                                 Object... p);
}
