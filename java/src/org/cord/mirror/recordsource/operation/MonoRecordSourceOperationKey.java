package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSourceOperationKey;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

public class MonoRecordSourceOperationKey<A, V>
  extends RecordSourceOperationKey<MonoRecordSourceOperation<A, V>.Invoker>
{
  public static <A, V> MonoRecordSourceOperationKey<A, V> create(String name,
                                                                 Class<A> classA,
                                                                 Class<V> valueClass)
  {
    return new MonoRecordSourceOperationKey<A, V>(name, classA, valueClass);
  }

  private final Class<A> __classA;
  private final Class<V> __classV;

  public MonoRecordSourceOperationKey(String keyName,
                                      Class<A> classA,
                                      Class<V> classV)
  {
    super(keyName,
          ObjectUtil.<MonoRecordSourceOperation<A, V>
                    .Invoker> castClass(MonoRecordSourceOperation.Invoker.class));
    __classA = Preconditions.checkNotNull(classA, "classA");
    __classV = classV;
  }

  public final Class<A> getClassA()
  {
    return __classA;
  }

  public final Class<V> getClassV()
  {
    return __classV;
  }

  @Override
  public MonoRecordSourceOperationKey<A, V> copy()
  {
    return create(getKeyName(), __classA, getClassV());
  }
}
