package org.cord.mirror.recordsource.operation;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.AbstractRecordSource;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.UnmodifiableIdList;

import com.google.common.base.Preconditions;

/**
 * RecordSourceOperation that returns a RecordSource that is formed by using a RecordOperation to
 * return a PersistentRecord on each PersistentRecord in the original RecordSource.
 * 
 * @author alex
 */
public class RelatedRecords
  extends SimpleRecordSourceOperation<RecordSource>
{
  private final RecordOperationKey<PersistentRecord> __relatedRecordKey;
  private final TableWrapper __targetTableWrapper;

  /**
   * @param key
   *          The key to invoke this operation on the source RecordSource
   * @param relatedRecordKey
   *          The RecordOperationKey that is used to transform one of the source PersistentRecords
   *          into a target PersistentRecord. This doesn't have to always return a value - null
   *          results will be automatically discarded.
   * @param targetTableWrapper
   *          The TableWrapper that the relatedRecordKey will return PersistentRecords from.
   */
  public RelatedRecords(RecordSourceOperationKey<RecordSource> key,
                        RecordOperationKey<PersistentRecord> relatedRecordKey,
                        TableWrapper targetTableWrapper)
  {
    super(key);
    __relatedRecordKey = Preconditions.checkNotNull(relatedRecordKey, "relatedRecordKey");
    __targetTableWrapper = Preconditions.checkNotNull(targetTableWrapper, "targetTableWrapper");
  }

  @Override
  public RecordSource invokeRecordSourceOperation(final RecordSource sourceRecordSource,
                                                  Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(new AbstractRecordSource(__targetTableWrapper, null) {
      @Override
      protected IdList buildIdList(Viewpoint viewpoint)
      {
        RecordList sourceRecordList = sourceRecordSource.getRecordList(viewpoint);
        ArrayIdList targetIdList = new ArrayIdList(__targetTableWrapper, sourceRecordList.size());
        for (PersistentRecord record : sourceRecordList) {
          PersistentRecord targetRecord = record.opt(__relatedRecordKey);
          if (targetRecord != null) {
            targetIdList.add(targetRecord.getId());
          }
        }
        return new UnmodifiableIdList(targetIdList);
      }

      @Override
      public long getLastChangeTime()
      {
        return sourceRecordSource.getLastChangeTime();
      }

      @Override
      public boolean shouldRebuild(Viewpoint viewpoint)
      {
        return sourceRecordSource.shouldRebuild(viewpoint);
      }
    }, viewpoint);
  }
}
