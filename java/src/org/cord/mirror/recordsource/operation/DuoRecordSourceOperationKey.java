package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSourceOperationKey;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

public class DuoRecordSourceOperationKey<A, B, V>
  extends RecordSourceOperationKey<DuoRecordSourceOperation<A, B, V>.Invoker>

{
  public static <A, B, V> DuoRecordSourceOperationKey<A, B, V> create(String name,
                                                                      Class<A> classA,
                                                                      Class<B> classB,
                                                                      Class<V> valueClass)
  {
    return new DuoRecordSourceOperationKey<A, B, V>(name, classA, classB, valueClass);
  }

  private final Class<A> __classA;
  private final Class<B> __classB;

  public DuoRecordSourceOperationKey(String keyName,
                                     Class<A> classA,
                                     Class<B> classB,
                                     Class<V> valueClass)
  {
    super(keyName,
          ObjectUtil.<DuoRecordSourceOperation<A, B, V>
                    .Invoker> castClass(DuoRecordSourceOperation.Invoker.class));
    __classA = Preconditions.checkNotNull(classA, "classA");
    __classB = Preconditions.checkNotNull(classB, "classB");
  }

  public final Class<A> getClassA()
  {
    return __classA;
  }

  public final Class<B> getClassB()
  {
    return __classB;
  }
}
