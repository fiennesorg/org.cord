package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Casters;

/**
 * RecordSourceOperation that creates a new RecordSource by following references in the original
 * RecordSource. $recordSource.followLink.linkFieldName
 * 
 * @see RecordSources#followLink(RecordSource, String)
 */
public class FollowLink
  extends MonoRecordSourceOperation<String, RecordSource>
{
  public static final MonoRecordSourceOperationKey<String, RecordSource> NAME =
      MonoRecordSourceOperationKey.create("followLink", String.class, RecordSource.class);

  public FollowLink()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public RecordSource calculate(RecordSource recordSource,
                                Viewpoint viewpoint,
                                String a)
  {
    return RecordSources.viewedFrom(RecordSources.followLink(recordSource, a), viewpoint);
  }

}
