package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;

public class NumericalNotEquals
  extends NumericalComparisonFilter
{
  public final static DuoRecordSourceOperationKey<String, Object, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("notEquals",
                                         String.class,
                                         Object.class,
                                         RecordSource.class);

  public NumericalNotEquals()
  {
    super(NAME,
          "!=");
  }

  @Override
  protected boolean accepts(int recordValue,
                            int paramValue)
  {
    return recordValue != paramValue;
  }
}
