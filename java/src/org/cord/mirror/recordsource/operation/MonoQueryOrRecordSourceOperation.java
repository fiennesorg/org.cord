package org.cord.mirror.recordsource.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.Caster;

public abstract class MonoQueryOrRecordSourceOperation<A, V>
  extends MonoRecordSourceOperation<A, V>
{
  private final boolean __mayBeTransactionSensitive;

  public MonoQueryOrRecordSourceOperation(MonoRecordSourceOperationKey<A, V> key,
                                          Caster<A> casterA,
                                          boolean mayBeTransactionSensitive)
  {
    super(key,
          casterA);
    __mayBeTransactionSensitive = mayBeTransactionSensitive;
  }

  public boolean isTransactionSensitive(Query query,
                                        Viewpoint viewpoint)
  {
    return RecordSources.isTransactionSensitive(__mayBeTransactionSensitive, query, viewpoint);
  }

  @Override
  public V calculate(RecordSource recordSource,
                     Viewpoint viewpoint,
                     A a)
  {
    viewpoint = RecordSources.getViewpoint(recordSource, viewpoint);
    if (!recordSource.getTable().getDb().disableQueryBranch()) {
      Query query = RecordSources.asQuery(recordSource);
      if (query != null && !isTransactionSensitive(query, viewpoint)) {
        return invokeQueryBranch(query, viewpoint, a);
      }
    }
    return invokeRecordSourceBranch(recordSource, viewpoint, a);
  }

  protected abstract V invokeRecordSourceBranch(RecordSource recordSource,
                                                Viewpoint viewpoint,
                                                A a);
  protected abstract V invokeQueryBranch(Query query,
                                         Viewpoint viewpoint,
                                         A a);

}
