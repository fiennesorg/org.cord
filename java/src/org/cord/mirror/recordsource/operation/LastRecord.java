package org.cord.mirror.recordsource.operation;

import org.cord.mirror.MissingRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;

/**
 * $recordSource.LastRecord
 * 
 * @author alex
 */
public class LastRecord
  extends SimpleRecordSourceOperation<PersistentRecord>
{
  public static final RecordSourceOperationKey<PersistentRecord> NAME =
      RecordSourceOperationKey.create("LastRecord", PersistentRecord.class);

  public LastRecord()
  {
    super(NAME);
  }

  /**
   * @return The last record in the RecordSource or null if there are no records.
   * @throws MissingRecordException
   *           If the record is deleted in parallel with the resolution of the request.
   * @see RecordSources#getOptLastRecord(RecordSource, Viewpoint)
   */
  @Override
  public PersistentRecord invokeRecordSourceOperation(RecordSource recordSource,
                                                      Viewpoint viewpoint)
  {
    return RecordSources.getOptLastRecord(recordSource, viewpoint);
  }
}
