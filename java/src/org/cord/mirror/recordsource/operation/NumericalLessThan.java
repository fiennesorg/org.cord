package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSource;

public class NumericalLessThan
  extends NumericalComparisonFilter
{
  public final static DuoRecordSourceOperationKey<String, Object, RecordSource> NAME =
      DuoRecordSourceOperationKey.create("numericalLessThan",
                                         String.class,
                                         Object.class,
                                         RecordSource.class);

  public NumericalLessThan()
  {
    super(NAME,
          "<");
  }

  @Override
  protected boolean accepts(int recordValue,
                            int paramValue)
  {
    return recordValue < paramValue;
  }
}
