package org.cord.mirror.recordsource.operation;

import java.util.Comparator;

import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.AbstractRecordOrdering;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.mirror.recordsource.RecordSources;

import com.google.common.base.Preconditions;

public class OrderByComparator
  extends SimpleRecordSourceOperation<RecordSource>
{
  private final RecordOrdering __recordOrdering;

  public OrderByComparator(RecordSourceOperationKey<RecordSource> key,
                           Comparator<TransientRecord> comparator)
  {
    super(key);
    __recordOrdering =
        new AbstractRecordOrdering(key.getKeyName(),
                                   null,
                                   null,
                                   Preconditions.checkNotNull(comparator, "comparator"));
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(new RecordSourceOrdering(recordSource.getTable(),
                                                             null,
                                                             recordSource,
                                                             __recordOrdering),
                                    viewpoint);
  }
}
