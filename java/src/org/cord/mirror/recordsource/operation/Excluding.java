package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceIdFilter;
import org.cord.util.Casters;
import org.cord.util.NumberUtil;

import com.google.common.base.Optional;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * RecordSourceOperation that filters out the given record(s) or id from its contents invoked as
 * either $records.excluding.get($record), $records.excluding.get($id),
 * $records.excluding.get($recordSource) or $records.excluding.get($optional) where $optional is an
 * Optional wrapper around either $record, $id or $recordSource
 * 
 * @author alex
 */
public class Excluding
  extends MonoRecordSourceOperation<Object, RecordSource>
{
  public static final MonoRecordSourceOperationKey<Object, RecordSource> EXCLUDING =
      MonoRecordSourceOperationKey.create("excluding", Object.class, RecordSource.class);

  public Excluding()
  {
    super(EXCLUDING,
          Casters.NULL);
  }

  @Override
  public RecordSource calculate(RecordSource recordSource,
                                Viewpoint viewpoint,
                                Object a)
  {
    if (a instanceof PersistentRecord) {
      return excludeSingleRecord(recordSource, viewpoint, ((PersistentRecord) a).getId());
    }
    if (a instanceof RecordSource) {
      return excludeRecordSource(recordSource, viewpoint, ((RecordSource) a));
    }
    if (a instanceof Optional<?>) {
      return excludeOptional(recordSource, viewpoint, ((Optional<?>) a));
    }
    return excludeSingleRecord(recordSource, viewpoint, NumberUtil.toInt(a));
  }

  private RecordSource excludeOptional(RecordSource recordSource,
                                       Viewpoint viewpoint,
                                       Optional<?> optional)
  {
    if (optional.isPresent()) {
      return calculate(recordSource, viewpoint, optional.get());
    }
    return RecordSources.viewedFrom(recordSource, viewpoint);
  }

  private RecordSource excludeSingleRecord(RecordSource recordSource,
                                           Viewpoint viewpoint,
                                           final int excludedId)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceIdFilter(recordSource, viewpoint) {
      @Override
      protected boolean accepts(int id)
      {
        return id != excludedId;
      }
    }, viewpoint);
  }

  private RecordSource excludeRecordSource(RecordSource recordSource,
                                           Viewpoint viewpoint,
                                           RecordSource excludedRecords)
  {
    final IntList excludedIds = excludedRecords.getIdList(viewpoint);
    switch (excludedIds.size()) {
      case 0:
        return RecordSources.viewedFrom(recordSource, viewpoint);
      case 1:
        return excludeSingleRecord(recordSource, viewpoint, excludedIds.getInt(0));
      default:
        return excludeIntCollection(recordSource, viewpoint, excludedIds);
    }
  }

  private RecordSource excludeIntCollection(RecordSource recordSource,
                                            Viewpoint viewpoint,
                                            final IntCollection excludedIds)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceIdFilter(recordSource, viewpoint) {
      @Override
      protected boolean accepts(int id)
      {
        return !excludedIds.contains(id);
      }
    }, viewpoint);
  }

}
