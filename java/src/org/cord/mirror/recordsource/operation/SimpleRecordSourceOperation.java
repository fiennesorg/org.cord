package org.cord.mirror.recordsource.operation;

import org.cord.mirror.RecordSourceOperation;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.operation.AbstractLockableOperation;

public abstract class SimpleRecordSourceOperation<V>
  extends AbstractLockableOperation
  implements RecordSourceOperation<V>
{
  private final RecordSourceOperationKey<V> __key;

  public SimpleRecordSourceOperation(RecordSourceOperationKey<V> key)
  {
    super(key.getKeyName());
    __key = key;
  }

  public final RecordSourceOperationKey<V> getKey()
  {
    return __key;
  }

}
