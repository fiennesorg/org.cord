package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;

public abstract class SimpleFilterOperation
  extends SimpleQueryOrRecordSourceOperation<RecordSource>
{
  public SimpleFilterOperation(RecordSourceOperationKey<RecordSource> key,
                               boolean mayBeTransactionSensitive)
  {
    super(key,
          mayBeTransactionSensitive);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceFilter(recordSource, null) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        return SimpleFilterOperation.this.accepts(record);
      }
    }, viewpoint);
  }

  protected abstract boolean accepts(PersistentRecord record);
}
