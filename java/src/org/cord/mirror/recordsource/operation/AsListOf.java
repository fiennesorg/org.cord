package org.cord.mirror.recordsource.operation;

import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.ObjectUtil;

import com.google.common.collect.Lists;

public class AsListOf
  extends MonoRecordSourceOperation<String, List<?>>
{
  public static final MonoRecordSourceOperationKey<String, List<?>> NAME =
      MonoRecordSourceOperationKey.create("asListOf",
                                          String.class,
                                          ObjectUtil.<List<?>> castClass(List.class));

  public AsListOf()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public List<?> calculate(RecordSource recordSource,
                           Viewpoint viewpoint,
                           String key)
  {
    RecordList records = recordSource.getRecordList(viewpoint);
    List<Object> list = Lists.newArrayListWithCapacity(records.size());
    for (PersistentRecord record : records) {
      list.add(record.get(key));
    }
    return list;
  }

}
