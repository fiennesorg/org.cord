package org.cord.mirror.recordsource.operation;

import java.util.Arrays;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.util.DebugConstants;

/**
 * Abstract RecordSourceOperation that delegates the implementation down different branches
 * depending on whether it is invoked on something that can be resolved as a Query or a generic
 * RecordSource.
 * 
 * @author alex
 * @deprecated in preference of {@link SimpleQueryOrRecordSourceOperation} and
 *             {@link MonoQueryOrRecordSourceOperation}
 */
@Deprecated
public abstract class QueryOrRecordSourceOperation
  extends AbstractRecordSourceOperation
{
  /**
   * If true then any time that the QueryOrRecordSourceOperation chooses to invoke a RecordSource
   * branch then it will document this decision to DEBUG_OUT. Currently set to false (ie don't debug
   * RecordSource branches)
   * 
   * @see DebugConstants#DEBUG_OUT
   */
  public final static boolean DEBUG_RECORDSOURCEBRANCH = false;

  private final boolean __mayBeTransactionSensitive;

  /**
   * @param mayBeTransactionSensitive
   *          Flag to state whether this operation may take a different course of action if it is
   *          invoked from a transactional viewpoint. If this is true and
   *          isTransactionSensitive(query,viewpoint) returns true then the invokeRecordSourceBranch
   *          will be invoked.
   * @see #mayBeTransactionSensitive()
   * @see #isTransactionSensitive(Query, Viewpoint)
   */
  public QueryOrRecordSourceOperation(String name,
                                      int paramCount,
                                      boolean mayBeTransactionSensitive)
  {
    super(name,
          paramCount);
    __mayBeTransactionSensitive = mayBeTransactionSensitive;
  }

  /**
   * Is there a possibility that this operation may invoke the RecordSource branch on a Query if
   * invoked with the correct Transaction viewpoint?
   * 
   * @return The value passed to the constructor
   */
  public final boolean mayBeTransactionSensitive()
  {
    return __mayBeTransactionSensitive;
  }

  /**
   * If we want to invoke this operation on the given Query then is it sensitive to the given
   * Transaction? This is invoked by invokeWithParams if we are given a Query to process and we want
   * to know whether or not to invoke the Query or the RecordSource branch. The default
   * implementation will only return true if the operation was initialised with
   * mayBeTransactionSensitive set to true and the given viewpoint has updated a WritableRecord in
   * one of the Tables that the (via the appropriate method in RecordSources). If this logic is not
   * sufficient then the method may be overriden (although I am at a loss to think of a situation
   * where this may be the case).
   * 
   * @param query
   *          Compulsory
   * @param viewpoint
   *          Optional
   * @return true if the operation is transaction sensitive and the RecordSource branch should
   *         therefore be invoked on the Query. false if the given Transaction has no effect on the
   *         Query and it is safe to invoke the query branch.
   * @see RecordSources#isTransactionSensitive(boolean, RecordSource, Viewpoint)
   */
  public boolean isTransactionSensitive(Query query,
                                        Viewpoint viewpoint)
  {
    return RecordSources.isTransactionSensitive(__mayBeTransactionSensitive, query, viewpoint);
  }

  /**
   * If the recordSource is either a Query or a QueryWrapper then invokeQueryBranch otherwise
   * invokeRecordSourceBranch. If invokeQueryBranch is being invoked on a QueryWrapper and the
   * viewpoint passed into this method is null, then the viewpoint contained within the QueryWrapper
   * will be utilised instead. If the operation isTransactionSensitive and viewpoint is defined then
   * invokeRecordSourceBranch will be defined regardless of whether the incoming request is
   * resolveable to a Query.
   * 
   * @see #invokeQueryBranch(Query, Viewpoint, Object[])
   * @see #invokeRecordSourceBranch(RecordSource, Viewpoint, Object[])
   */
  @Override
  protected final Object invokeWithParams(RecordSource recordSource,
                                          Viewpoint viewpoint,
                                          Object... p)
  {
    viewpoint = RecordSources.getViewpoint(recordSource, viewpoint);
    Query query = RecordSources.asQuery(recordSource);
    if (!recordSource.getTable().getDb().disableQueryBranch()) {
      if (query != null && !isTransactionSensitive(query, viewpoint)) {
        return invokeQueryBranch(query, viewpoint, p);
      }
    }
    if (DEBUG_RECORDSOURCEBRANCH) {
      DebugConstants.DEBUG_OUT.println(String.format("%s.invokeWithParams(%s,%s,%s) --> RecordSource branch",
                                                     this.getClass(),
                                                     recordSource,
                                                     viewpoint,
                                                     Arrays.toString(p)));
    }
    return invokeRecordSourceBranch(recordSource, viewpoint, p);
  }

  /**
   * Perform the necessary operation on the given RecordSource. This will be invoked when
   * invokeRecordSourceOperation is invoked with a non-Query based RecordSource or if it is invoked
   * with a viewpoint defined and it isTransactionSensitive.
   * 
   * @param recordSource
   *          The RecordSource to operate on. Not null
   * @param viewpoint
   *          The viewpoint. Possibly null
   * @see #invokeRecordSourceOperation(RecordSource, Viewpoint)
   */
  protected abstract Object invokeRecordSourceBranch(RecordSource recordSource,
                                                     Viewpoint viewpoint,
                                                     Object... p);

  /**
   * Perform the necessary operation on the given Query. This will only be invoked if
   * invokeRecordSourceOperation is called with either a Query or a QueryWrapper and no transaction
   * if it is declared as isTransactionSensitive.
   * 
   * @param query
   *          The Query to operate on. Not null
   * @param viewpoint
   *          The viewpoint to utilise. Possibly null. If invokeRecordSourceOperation was invoked
   *          with a QueryWrapper containing a viewpoint and an explicit null viewpoint, then this
   *          will be the QueryWrapper viewpoint. However, if the explicit viewpoint is defined then
   *          this is what will be used.
   * @see #invokeRecordSourceOperation(RecordSource, Viewpoint)
   */
  protected abstract Object invokeQueryBranch(Query query,
                                              Viewpoint viewpoint,
                                              Object... p);
}
