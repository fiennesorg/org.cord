package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.util.Assertions;
import org.cord.util.Casters;
import org.cord.util.NumberUtil;

public abstract class NumericalComparisonFilter
  extends DuoFilterOperation<String, Object>
{
  public static final int P0_FIELDNAME = 0;

  public static final int P1_NUMERICALVALUE = 1;

  private final String __sqlOperator;

  public NumericalComparisonFilter(DuoRecordSourceOperationKey<String, Object, RecordSource> name,
                                   String sqlOperator)
  {
    super(name,
          Casters.TOSTRING,
          Casters.NULL,
          true);
    __sqlOperator = Assertions.notEmpty(sqlOperator, "sqlOperator");
  }

  @Override
  protected final boolean accepts(PersistentRecord record,
                                  String fieldName,
                                  Object value)
  {
    Number recordValue = (Number) record.get(fieldName);
    int paramValue = NumberUtil.toInt(value);
    return accepts(recordValue.intValue(), paramValue);
  }

  protected abstract boolean accepts(int recordValue,
                                     int paramValue);

  @Override
  protected RecordSource invokeQueryBranch(Query query,
                                           Viewpoint viewpoint,
                                           String fieldName,
                                           Object value)
  {
    return Query.transformQuery(query, fieldName + __sqlOperator + value, Query.TYPE_AND, null);
  }
}
