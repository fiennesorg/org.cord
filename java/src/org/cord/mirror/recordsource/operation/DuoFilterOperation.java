package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.util.Caster;

public abstract class DuoFilterOperation<A, B>
  extends DuoQueryOrRecordSourceOperation<A, B, RecordSource>
{
  public DuoFilterOperation(DuoRecordSourceOperationKey<A, B, RecordSource> key,
                            Caster<A> casterA,
                            Caster<B> casterB,
                            boolean mayBeTransactionSensitive)
  {
    super(key,
          casterA,
          casterB,
          mayBeTransactionSensitive);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(final RecordSource recordSource,
                                                  final Viewpoint viewpoint,
                                                  final A a,
                                                  final B b)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceFilter(recordSource, null) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        return DuoFilterOperation.this.accepts(record, a, b);
      }
    }, viewpoint);
  }

  protected abstract boolean accepts(PersistentRecord record,
                                     A a,
                                     B b);

}
