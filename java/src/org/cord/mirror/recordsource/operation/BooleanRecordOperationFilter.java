package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;

import com.google.common.base.Preconditions;

/**
 * RecordSourceOperation that strips out any records that don't return true when the
 * booleanOperationKey is invoked on them.
 * 
 * @author alex
 * @deprecated because nobody appears to be using this and I can't think of a sensible use case.
 */
@Deprecated
public class BooleanRecordOperationFilter
  extends SimpleRecordSourceOperation<RecordSource>
{
  private final RecordOperationKey<Boolean> __booleanOperationKey;

  public BooleanRecordOperationFilter(RecordSourceOperationKey<RecordSource> key,
                                      RecordOperationKey<Boolean> booleanOperationKey)
  {
    super(key);
    __booleanOperationKey = Preconditions.checkNotNull(booleanOperationKey, "booleanOperationKey");
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    return new SingleRecordSourceFilter(recordSource, null) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        return record.is(__booleanOperationKey);
      }
    };
  }
}
