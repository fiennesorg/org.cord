package org.cord.mirror.recordsource.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.util.Caster;

public abstract class MonoFilterOperation<A>
  extends MonoQueryOrRecordSourceOperation<A, RecordSource>
{

  public MonoFilterOperation(MonoRecordSourceOperationKey<A, RecordSource> key,
                             Caster<A> casterA,
                             boolean mayBeTransactionSensitive)
  {
    super(key,
          casterA,
          mayBeTransactionSensitive);
  }

  @Override
  protected RecordSource invokeRecordSourceBranch(final RecordSource recordSource,
                                                  final Viewpoint viewpoint,
                                                  final A a)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceFilter(recordSource, null) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        return MonoFilterOperation.this.accepts(record, a);
      }
    }, viewpoint);
  }

  protected abstract boolean accepts(PersistentRecord record,
                                     A a);

}
