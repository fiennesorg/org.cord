package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntLists;

/**
 * IdList wrapper class that provides an immutable view onto a nested IdList. Please note that if
 * other classes have access to the contained IdList then they can still change the contents so the
 * contents of this IdList are not guaranteed never to change.
 * 
 * @author alex
 */
public class UnmodifiableIdList
  extends IntLists.UnmodifiableList
  implements IdList
{
  private static final long serialVersionUID = 8344925804984554116L;

  /**
   * Get an UnmodifiableIdList wrapper around idList with a shortcut if idList is already an
   * UnmodifiableIdList.
   * 
   * @return The appropriate UnmodifiableIdList wrapper, or idList itself if it is already an
   *         UnmodifiableIdList
   */
  public static UnmodifiableIdList getInstance(IdList idList)
  {
    if (idList instanceof UnmodifiableIdList) {
      return (UnmodifiableIdList) idList;
    }
    return new UnmodifiableIdList(idList);
  }

  private final Table __table;

  /**
   * @param ids
   *          The IdList that this is going to provide an immutable view of. Must be defined.
   */
  public UnmodifiableIdList(IdList ids)
  {
    this(ids.getTable(),
         ids);
  }

  public UnmodifiableIdList(Table table,
                            IntList ids)
  {
    super(ids);
    __table = table;
  }

  @Override
  public Table getTable()
  {
    return __table;
  }
}
