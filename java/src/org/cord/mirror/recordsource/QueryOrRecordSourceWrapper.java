package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.util.DebugConstants;

/**
 * Implementation of RecordSourceWrapper that chooses between a RecordSource or a Query branch for
 * the implementation depending on the Transactional sensitivity to the state of the wrapped
 * RecordSource.
 * 
 * @author alex
 */
public abstract class QueryOrRecordSourceWrapper
  extends RecordSourceWrapper
{
  public final static boolean DEBUG_RECORDSOURCEBRANCH = false;

  private final boolean __mayBeTransactionSensitive;

  public QueryOrRecordSourceWrapper(Table table,
                                    Object cacheKey,
                                    RecordSource wrappedSource,
                                    boolean mayBeTransactionSensitive)
  {
    super(table,
          cacheKey,
          wrappedSource);
    __mayBeTransactionSensitive = mayBeTransactionSensitive;
  }

  @Override
  protected final IdList buildIdList(Viewpoint viewpoint)
  {
    Query query = RecordSources.asQuery(getWrappedRecordSource());
    if (!getTable().getDb().disableQueryBranch()) {
      if (query != null && !isTransactionSensitive(query, viewpoint)) {
        return buildQueryIdList(query, viewpoint);
      }
    }
    if (DEBUG_RECORDSOURCEBRANCH) {
      DebugConstants.DEBUG_OUT.println(String.format("%s.buildIdList(%s) --> buildRecordSourceIdList(%s, %s)",
                                                     this,
                                                     viewpoint,
                                                     getWrappedRecordSource(),
                                                     viewpoint));
    }
    return buildRecordSourceIdList(getWrappedRecordSource(), viewpoint);
  }

  public boolean isTransactionSensitive(RecordSource recordSource,
                                        Viewpoint viewpoint)
  {
    return RecordSources.isTransactionSensitive(__mayBeTransactionSensitive,
                                                recordSource,
                                                viewpoint);
  }

  protected abstract IdList buildQueryIdList(Query query,
                                             Viewpoint viewpoint);

  protected abstract IdList buildRecordSourceIdList(RecordSource recordSource,
                                                    Viewpoint viewpoint);
}
