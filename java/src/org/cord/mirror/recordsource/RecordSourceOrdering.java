package org.cord.mirror.recordsource;

import java.util.Collections;
import java.util.Comparator;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueComparator;
import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationValueComparator;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.DelegatingComparator;

import com.google.common.base.Strings;

/**
 * A RecordSourceWrapper that takes a Comparator that can order PersistentRecords and which applies
 * this ordering to the wrapped RecordSource. This is potentially quite an expensive operation on
 * large RecordSources because it will involve creating a temporary ordered collection of all the
 * PersistentRecords in the wrapped RecordSource which can then be acted on by the Comparator. It
 * should therefore only be used when either the ordering is dependent on the Transactional
 * viewpoint of the data or when the ordering is hard to express in SQL terms.
 * 
 * @author alex
 */
public class RecordSourceOrdering
  extends QueryOrRecordSourceWrapper
{
  private final RecordOrdering __recordOrdering;

  public RecordSourceOrdering(Table table,
                              Object cacheKey,
                              RecordSource wrappedSource,
                              RecordOrdering recordOrdering)
  {
    super(table,
          cacheKey,
          wrappedSource,
          true);
    __recordOrdering = recordOrdering;
  }

  @Override
  public String toString()
  {
    return String.format("RecordSourceOrdering(%s, %s, %s, %s)",
                         getTable(),
                         getCacheKey(),
                         getWrappedRecordSource(),
                         getRecordOrdering());
  }

  /**
   * Construct the sorted IdList from the records in the Query. This will normally invoke
   * transformQuery to result in a new SQL Query unless the RecordOrdering cannot be represented in
   * SQL space (ie getSqlOrdering returns an empty string) in which case it will delegate to
   * buildRecordSourceIdList
   * 
   * @see Query#transformQuery(Query, String, int, String, Table[])
   * @see RecordOrdering#getSqlOrdering()
   * @see #buildRecordSourceIdList(RecordSource, Viewpoint)
   */
  @Override
  protected IdList buildQueryIdList(Query query,
                                    Viewpoint viewpoint)
  {
    String sqlOrdering = getRecordOrdering().getSqlOrdering();
    if (Strings.isNullOrEmpty(sqlOrdering)) {
      return buildRecordSourceIdList(query, viewpoint);
    }
    return Query.transformQuery(query, null, Query.TYPE_ORDEREDBY, sqlOrdering)
                .getIdList(viewpoint);
  }

  @Override
  protected IdList buildRecordSourceIdList(RecordSource recordSource,
                                           Viewpoint viewpoint)
  {
    return __recordOrdering.applyJavaOrdering(recordSource.getIdList(viewpoint), viewpoint);
  }

  @Override
  public RecordOrdering getRecordOrdering()
  {
    return __recordOrdering;
  }

  /**
   * Parse an SQL ordered by clause into a Comparator<PersistentRecord> so that the same effect can
   * be got in java space rather than in SQL space. This will only work with ordering constructs
   * that target operations that can be invoked on the record itself, ie it is not possible to
   * supply an ordering such as "TargetTable.field1, JoinTable.field2" because the comparator will
   * not know how to resolve the join table in java space.
   * 
   * @param tableName
   *          The name of the Table that this Comparator is expected to order across. This will be
   *          used to strip out the "tableName." component of qualified field names as they are not
   *          required when invoking the appropriate RecordOperations.
   * @param sqlOrderedBy
   *          The SQL ordering string. This may be comma separated, and may also include "asc" or
   *          "desc" after each element.
   * @return The appropriate comparator. This will either be a DelegatingComparator of
   *         RecordOperationValueComparators or a single RecordOperationValueComparator as
   *         appropriate.
   * @see DelegatingComparator
   * @see RecordOperationValueComparator
   */
  public static Comparator<TransientRecord> createComparator(String tableName,
                                                             String sqlOrderedBy)
  {
    // if (sqlOrderedBy.indexOf(',') == -1) {
    // return singleOrderingClause(tableName, sqlOrderedBy, sqlOrderedBy);
    // }
    if (Strings.isNullOrEmpty(sqlOrderedBy)) {
      return FieldValueComparator.BY_ID;
    }
    String[] clauses = sqlOrderedBy.split(",");
    DelegatingComparator.Builder<TransientRecord> builder =
        DelegatingComparator.of(singleOrderingClause(tableName, sqlOrderedBy, clauses[0]));
    for (int i = 1; i < clauses.length; i++) {
      builder.of(singleOrderingClause(tableName, sqlOrderedBy, clauses[i]));
    }
    builder.of(FieldValueComparator.BY_ID);
    return builder.build();
  }

  /**
   * A RecordOrdering that imposes no definition of the SQL ordering of records and which utilises
   * ORDERBYID for java ordering.
   */
  public final static RecordOrdering NOSQLORDERING =
      new AbstractRecordOrdering("NoSqlOrdering",
                                 "No SQL Ordering",
                                 null,
                                 FieldValueComparator.BY_ID);

  /**
   * @return The appropriate parsed RecordOrdering or {@link #NOSQLORDERING} if sqlOrderBy is not
   *         defined.
   */
  public static RecordOrdering createRecordOrdering(final String tableName,
                                                    final String sqlOrderBy,
                                                    final String englishName)
  {
    if (sqlOrderBy == null || sqlOrderBy.length() == 0) {
      return NOSQLORDERING;
    }
    final Comparator<TransientRecord> comparator = createComparator(tableName, sqlOrderBy);
    return new AbstractRecordOrdering(sqlOrderBy, englishName, sqlOrderBy, comparator);
  }

  private static Comparator<TransientRecord> singleOrderingClause(String tableName,
                                                                  String sqlOrderedBy,
                                                                  String clause)
  {
    clause = clause.trim();
    boolean isAscending = true;
    String[] elements = clause.split(" +");
    switch (elements.length) {
      case 1:
        break;
      case 2:
        String sqlOrdering = elements[1].toLowerCase();
        if (sqlOrdering.equals("desc")) {
          isAscending = false;
          break;
        }
        if (sqlOrdering.equals("asc")) {
          break;
        }
        throw new IllegalArgumentException(String.format("Unable to parse ordering of \"%s\" in \"%s\"",
                                                         elements[1],
                                                         sqlOrderedBy));
      default:
        throw new IllegalArgumentException(String.format("Unable to parse \"%s\" in \"%s\"",
                                                         clause,
                                                         sqlOrderedBy));
    }
    if (elements[0].indexOf('.') != -1) {
      if (elements[0].startsWith(tableName)) {
        elements[0] = elements[0].substring(tableName.length() + 1);
      } else {
        throw new IllegalArgumentException(String.format("\"%s\" does not reference a field from %s and therefore cannot be used to construct a Comparator",
                                                         clause,
                                                         tableName));
      }
    }
    // FIXME
    Comparator<TransientRecord> comparator =
        new RecordOperationValueComparator<Object>(FieldKey.create(elements[0], Object.class));
    return isAscending ? comparator : Collections.reverseOrder(comparator);
  }
}
