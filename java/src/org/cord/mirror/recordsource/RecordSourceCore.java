package org.cord.mirror.recordsource;

import java.util.Iterator;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperation;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.operation.DuoRecordSourceOperationKey;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.util.UndefinedCompException;

import com.google.common.base.Preconditions;

/**
 * RecordSourceCore provides default implementations for all parts of RecordSource that don't
 * actually deal with where the ids came from and whether they are up to date. It is intended as a
 * base level reference implementation for the majority of implementations of RecordSource and any
 * non-RecordSource implementations should probably use a copy of these implementations.
 * 
 * @author alex
 */
public abstract class RecordSourceCore
  implements RecordSource
{
  private final Table __table;

  public RecordSourceCore(Table table)
  {
    __table = Preconditions.checkNotNull(table, "table");
  }

  /**
   * Invoke {@link #comp(RecordSourceOperationKey, Viewpoint)} with a Viewpoint of null. The null
   * Viewpoint will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <V> V comp(RecordSourceOperationKey<V> key)
      throws UndefinedCompException
  {
    return comp(key, null);
  }

  /**
   * Invoke {@link #opt(RecordSourceOperationKey, Viewpoint)} and throw an
   * {@link UndefinedCompException} if the result is null.
   */
  @Override
  public final <V> V comp(RecordSourceOperationKey<V> key,
                          Viewpoint viewpoint)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this, key, getTable().getRecordSourceOperation(key), null);
    }
    return v;
  }

  /**
   * Invoke {@link #comp(MonoRecordSourceOperationKey, Viewpoint, Object)} with a Viewpoint of null.
   * The null Viewpoint will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                             A a)
      throws UndefinedCompException
  {
    return comp(key, null, a);
  }

  /**
   * Invoke {@link #opt(MonoRecordSourceOperationKey, Viewpoint, Object)} and throw an
   * {@link UndefinedCompException} if the result is null.
   */
  @Override
  public final <A, V> V comp(MonoRecordSourceOperationKey<A, V> key,
                             Viewpoint viewpoint,
                             A a)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint, a);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this,
                                       key,
                                       getTable().getRecordSourceOperation(key),
                                       null,
                                       a);
    }
    return v;
  }

  /**
   * Invoke {@link #comp(DuoRecordSourceOperationKey, Viewpoint, Object, Object)} with a Viewpoint
   * of null. The null Viewpoint will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                                A a,
                                B b)
      throws UndefinedCompException
  {
    return comp(key, null, a, b);
  }

  /**
   * Invoke {@link #opt(DuoRecordSourceOperationKey, Viewpoint, Object, Object)} and throw an
   * {@link UndefinedCompException} if the result is null.
   */
  @Override
  public final <A, B, V> V comp(DuoRecordSourceOperationKey<A, B, V> key,
                                Viewpoint viewpoint,
                                A a,
                                B b)
      throws UndefinedCompException
  {
    V v = opt(key, viewpoint, a, b);
    if (Db.ENABLE_COMP & v == null) {
      throw new UndefinedCompException(this,
                                       key,
                                       getTable().getRecordSourceOperation(key),
                                       null,
                                       a,
                                       b);
    }
    return v;
  }

  /**
   * Invoke {@link #getRecordList(Viewpoint)} with a Viewpoint of null.
   */
  @Override
  public final RecordList getRecordList()
  {
    return getRecordList(null);
  }

  /**
   * @return The Table that was passed to the constructor. Never null.
   */
  @Override
  public final Table getTable()
  {
    return __table;
  }

  @Override
  public final Iterator<PersistentRecord> iterator()
  {
    return getRecordList(null).iterator();
  }

  /**
   * Invoke {@link #opt(RecordSourceOperationKey, Viewpoint)} with a Viewpoint of null
   */
  @Override
  public final <V> V opt(RecordSourceOperationKey<V> key)
  {
    return opt(key, null);
  }

  /**
   * @param viewpoint
   *          optional Viewpoint that will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <V> V opt(RecordSourceOperationKey<V> key,
                         Viewpoint viewpoint)
  {
    return getTable().getRecordSourceOperation(key)
                     .invokeRecordSourceOperation(this, transformViewpoint(viewpoint));
  }

  @Override
  public final <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                            A a)
  {
    return opt(key, null, a);
  }

  /**
   * @param viewpoint
   *          optional Viewpoint that will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <A, V> V opt(MonoRecordSourceOperationKey<A, V> key,
                            Viewpoint viewpoint,
                            A a)
  {
    return getTable().getRecordSourceOperation(key)
                     .calculate(this, transformViewpoint(viewpoint), a);
  }

  @Override
  public final <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                               A a,
                               B b)
  {
    return opt(key, null, a, b);
  }

  /**
   * @param viewpoint
   *          optional Viewpoint that will be passed through {@link #transformViewpoint(Viewpoint)}
   */
  @Override
  public final <A, B, V> V opt(DuoRecordSourceOperationKey<A, B, V> key,
                               Viewpoint viewpoint,
                               A a,
                               B b)
  {
    return getTable().getRecordSourceOperation(key)
                     .calculate(this, transformViewpoint(viewpoint), a, b);
  }

  /**
   * Invoke {@link #get(Object, Viewpoint)} with a Viewpoint of null.
   */
  @Override
  public final Object get(Object key)
  {
    return get(key, null);
  }

  @Override
  public final Object get(Object key,
                          Viewpoint viewpoint)
  {
    viewpoint = transformViewpoint(viewpoint);
    RecordSourceOperation<?> rso = getTable().getRecordSourceOperation(key.toString());
    if (rso == null) {
      throw new IllegalArgumentException(String.format("Cannot resolve RecordSourceOperation named %s on %s",
                                                       key,
                                                       this));
    }
    return rso.invokeRecordSourceOperation(this, viewpoint);
  }

  @Override
  public final boolean containsKey(Object key)
  {
    return getTable().getRecordSourceOperation(key.toString()) != null;
  }

  /**
   * Apply any viewpoint transformations that are necessary. This will be used to preprocess any
   * incoming viewpoint requests (getIdList, getRecordList) so if you wish to make an implementation
   * of AbstractRecordSource that enforces a particular policy as to which view is enforced (eg
   * RecordSourceViewpoint) then you should override this method. In addition, if your subclasses
   * contains methods that take viewpoints as parameters then you should pass the parameters through
   * this method so that the viewpoint handling is consistent across your class.
   * 
   * @param viewpoint
   * @return viewpoint without any transformation. ie no change at all and you get the viewpoint you
   *         asked for. subclasses should override this as appropriate.
   * @see #getIdList(Viewpoint)
   * @see #getRecordList(Viewpoint)
   * @see RecordSourceViewpoint#transformViewpoint(Viewpoint)
   */
  protected Viewpoint transformViewpoint(Viewpoint viewpoint)
  {
    return viewpoint;
  }

  /**
   * Invoke {@link #getIdList(Viewpoint)} with a Viewpoint of null.
   */
  @Override
  public final IdList getIdList()
  {
    return getIdList(null);
  }

}
