package org.cord.mirror.recordsource;

import it.unimi.dsi.fastutil.ints.IntList;

public class TimestampedIntList
  extends IntListWrapper
{
  private volatile long _lastChangeTime = System.currentTimeMillis();

  public TimestampedIntList(IntList source)
  {
    super(source);
  }

  public long getLastChangeTime()
  {
    return _lastChangeTime;
  }

  @Override
  protected void mutatingMethodInvoked()
  {
    _lastChangeTime = System.currentTimeMillis();
  }

  @Override
  protected IntList wrapSubList(IntList tIntList)
  {
    return new TimestampedIntList(tIntList);
  }

}
