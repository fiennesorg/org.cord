package org.cord.mirror.recordsource;

import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * RecordSourceWrapper is a RecordSource that has a single wrapped RecordSource that is responsible
 * for update timing information. If you require more than one wrapped RecordSource then you should
 * utilise a CompoundRecordSource.
 * 
 * @author alex
 * @see org.cord.mirror.recordsource.CompoundRecordSource
 */
public abstract class RecordSourceWrapper
  extends AbstractRecordSource
{
  private final RecordSource __wrappedSource;

  /**
   * Create a new CompoundRecordSource that produces records from a given Table. This does not
   * register table as one of the RecordSources that feeds into rebuild requests so subclasses
   * should invoke addRecordSource(table) if this is required (which it probably is).
   * 
   * @param table
   *          The compulsory Table that this RecordSource will return records from and which will be
   *          returned by getTable().
   */
  public RecordSourceWrapper(Table table,
                             Object cacheKey,
                             RecordSource wrappedSource)
  {
    super(table,
          cacheKey);
    __wrappedSource = Preconditions.checkNotNull(wrappedSource, "wrappedSource");
  }

  public final RecordSource getWrappedRecordSource()
  {
    return __wrappedSource;
  }

  /**
   * Return the most recent value of getLastChangeTime() of the wrapped RecordSource. Subclasses
   * should override this method if additional functionality is required.
   */
  @Override
  public long getLastChangeTime()
  {
    return __wrappedSource.getLastChangeTime();
  }

  /**
   * Return true if the wrapped RecordSource is transactionally sensitive. If the subclass has more
   * functionality than this then they should override this method (remembering to invoke super as
   * appropriate)
   */
  @Override
  public final boolean shouldRebuild(Viewpoint viewpoint)
  {
    return getWrappedRecordSource().shouldRebuild(viewpoint);
  }

  @Override
  public String toString()
  {
    return String.format("RecordSourceWrapper(%s)", __wrappedSource);
  }

  /**
   * @return The RecordOrdering defined by {@link #getWrappedRecordSource()}
   */
  @Override
  public RecordOrdering getRecordOrdering()
  {
    return getWrappedRecordSource().getRecordOrdering();
  }
}
