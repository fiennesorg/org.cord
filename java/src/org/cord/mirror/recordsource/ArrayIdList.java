package org.cord.mirror.recordsource;

import java.util.Collection;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;

public final class ArrayIdList
  extends IntArrayList
  implements IdList
{
  /**
   * 
   */
  private static final long serialVersionUID = -6996948346425777571L;
  private final Table __table;

  public ArrayIdList(TableWrapper tableWrapper,
                     int initialCapacity)
  {
    super(initialCapacity);
    __table = tableWrapper.getTable();
  }

  public ArrayIdList(TableWrapper tableWrapper)
  {
    super();
    __table = tableWrapper.getTable();
  }

  public ArrayIdList(TableWrapper tableWrapper,
                     Collection<? extends Integer> ids)
  {
    super(ids);
    __table = tableWrapper.getTable();
  }

  public ArrayIdList(TableWrapper tableWrapper,
                     IntCollection ids)
  {
    super(ids);
    __table = tableWrapper.getTable();
  }

  public ArrayIdList(TableWrapper tableWrapper,
                     int[] ids)
  {
    super(ids);
    __table = tableWrapper.getTable();
  }

  @Override
  public final Table getTable()
  {
    return __table;
  }
}
