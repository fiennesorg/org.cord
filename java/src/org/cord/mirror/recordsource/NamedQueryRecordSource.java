package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;

/**
 * Abstract RecordSource that wraps a Query, but which doesn't hold a strong reference to the Query
 * thereby allowing it to be garbage collected if necessary. The name of the Query is held, and when
 * the data is required by this RecordSource then the Query will be retrieved from the appropriate
 * RecordSource cache on the target Table. If the Query is not in the cache, or if the Query has
 * never been resolved before (and therefore the CachedQuery doesn't know the name of the referenced
 * Query yet) then it will invoke buildQuery that must be implemented by a subclass.
 * <p>
 * The cacheKey for this RecordSource is null, ie it is not cached. It may end up in a cache, but
 * contained within a RecordSource that is itself cachable.
 * </p>
 * 
 * @author alex
 * @deprecated As it is bound into Query too tightly, isn't getting used by anyone, and to be honest
 *             I'm not sure of an example as to when I would use it. I suspect that it doesn't have
 *             long for this world...
 */
@Deprecated
public abstract class NamedQueryRecordSource
  extends AbstractRecordSource
{
  private Object _cacheKey;

  public NamedQueryRecordSource(Table table)
  {
    super(table,
          null);
  }

  @Override
  protected final IdList buildIdList(Viewpoint viewpoint)
  {
    return getRecordSource().getIdList(viewpoint);
  }

  @Override
  public final long getLastChangeTime()
  {
    return getRecordSource().getLastChangeTime();
  }

  @Override
  public final boolean shouldRebuild(Viewpoint viewpoint)
  {
    return getRecordSource().shouldRebuild(viewpoint);
  }

  protected final RecordSource getRecordSource()
  {
    RecordSource query = null;
    if (_cacheKey != null) {
      query = getTable().getCachedRecordSource(_cacheKey);
      if (query != null) {
        return query;
      }
    }
    query = buildQuery();
    _cacheKey = query.getCacheKey();
    return query;
  }

  /**
   * Build the Query that this CachedQuery is wrapping. This should be invoked quite infrequently as
   * the rest of the time the caching infrastructure will take care of resolving the Query.
   * 
   * @return The appropriate Query. Not null
   */
  protected abstract Query buildQuery();
}
