package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * RecordSourceWrapper that only lets through one of each id in the original RecordSource, while
 * preserving the order as much as possible.
 * 
 * @author alex
 */
public class StripDuplicatesWrapper
  extends RecordSourceWrapper
{

  public StripDuplicatesWrapper(RecordSource wrappedSource,
                                Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    // Alternative guava based approach:-
    // return new ArrayIdList(getTable(),
    // ImmutableSet.copyOf(getWrappedRecordSource().getIdList(viewpoint)));
    IdList origIds = getWrappedRecordSource().getIdList(viewpoint);
    final int len = origIds.size();
    switch (len) {
      case 0:
      case 1:
        return origIds;
      case 2:
        if (origIds.getInt(0) == origIds.getInt(1)) {
          return new SingletonIdList(getTable(), origIds.getInt(0));
        }
        return origIds;
    }
    IntSet uniqueIds = new IntOpenHashSet();
    ArrayIdList filteredIds = new ArrayIdList(getTable(), len);
    for (int i = 0; i < len; i++) {
      int id = origIds.getInt(i);
      if (uniqueIds.add(id)) {
        filteredIds.add(id);
      }
    }
    return filteredIds;
  }

}
