package org.cord.mirror.recordsource;

import java.util.Collection;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Viewpoint;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntCollection;

/**
 * Implementation of RecordSource that holds an immutable copy of an externally defined set of ids
 * on a given Table.
 * 
 * @author alex
 */
public class ConstantRecordSource
  extends AbstractRecordSource
{
  public static ConstantRecordSource fromIds(TableWrapper tableWrapper,
                                             Object cacheKey,
                                             Collection<Integer> ids)
  {
    return new ConstantRecordSource(new UnmodifiableIdList(new ArrayIdList(tableWrapper.getTable(),
                                                                           ids)),
                                    cacheKey);
  }

  public static ConstantRecordSource fromIds(TableWrapper tableWrapper,
                                             Object cacheKey,
                                             IntCollection ids)
  {
    return new ConstantRecordSource(new UnmodifiableIdList(new ArrayIdList(tableWrapper.getTable(),
                                                                           ids)),
                                    cacheKey);
  }

  public static ConstantRecordSource fromRecords(TableWrapper tableWrapper,
                                                 Object cacheKey,
                                                 Collection<PersistentRecord> records)
  {
    ArrayIdList ids = new ArrayIdList(tableWrapper.getTable());
    for (PersistentRecord record : records) {
      ids.add(record.getId());
    }
    return new ConstantRecordSource(new UnmodifiableIdList(ids), cacheKey);

  }

  public static ConstantRecordSource fromRecordSource(Object cacheKey,
                                                      RecordSource recordSource)
  {
    if (recordSource instanceof ConstantRecordSource) {
      return (ConstantRecordSource) recordSource;
    }
    return new ConstantRecordSource(new UnmodifiableIdList(recordSource.getIdList()), cacheKey);
  }

  private final UnmodifiableIdList __idList;

  public ConstantRecordSource(UnmodifiableIdList unmodifiableIdList,
                              Object cacheKey)
  {
    super(unmodifiableIdList.getTable(),
          cacheKey);
    __idList = Preconditions.checkNotNull(unmodifiableIdList);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("table", getTable())
                      .add("ids", __idList)
                      .toString();
  }

  // @Deprecated
  // public ConstantRecordSource(TableWrapper tableWrapper,
  // Object cacheKey,
  // Collection<Integer> ids)
  // {
  // super(tableWrapper,
  // cacheKey);
  // __idList = new UnmodifiableIdList(new ArrayIdList(tableWrapper.getTable(), ids));
  // }
  //
  // public ConstantRecordSource(IdList idList,
  // Object cacheKey)
  // {
  // this(idList.getTable(),
  // cacheKey,
  // idList);
  // }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    return __idList;
  }

  @Override
  public long getLastChangeTime()
  {
    return 0;
  }

  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return false;
  }

}
