package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * An implementation of IdList that keeps a record of the timestamp of the last change to the
 * contents via add, remove or set.
 * 
 * @author alex
 * @see ManualRecordSource
 */
public class TimestampedIdList
  extends TimestampedIntList
  implements IdList
{
  private final Table __table;

  public TimestampedIdList(TableWrapper tableWrapper,
                           IntList source)
  {
    super(source);
    __table = tableWrapper.getTable();
  }

  public TimestampedIdList(TableWrapper tableWrapper)
  {
    this(tableWrapper,
         new IntArrayList());
  }

  @Override
  public Table getTable()
  {
    return __table;
  }
}
