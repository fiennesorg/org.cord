package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

/**
 * RecordSource filter that uses a Predicate ? extends PersistentRecord to decide whether or not a
 * record should be included.
 * 
 * @author alex
 */
public class PredicateRecordFilter
  extends RecordSourceWrapper
{
  private final Predicate<? super PersistentRecord> __filter;

  public PredicateRecordFilter(RecordSource wrappedSource,
                               Predicate<? super PersistentRecord> filter,
                               Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
    __filter = Preconditions.checkNotNull(filter, "filter");
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    RecordList records = getWrappedRecordSource().getRecordList(viewpoint);
    ArrayIdList ids = new ArrayIdList(getTable(), records.size());
    for (PersistentRecord record : records) {
      if (__filter.apply(record)) {
        ids.add(record.getId());
      }
    }
    return ids;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper("PredicateRecordFilter")
                      .addValue(getWrappedRecordSource())
                      .addValue(__filter)
                      .toString();
  }

}
