package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

/**
 * An AbstractRecordSource is responsible for handling the caching and rebuilding of the RecordList
 * and IdList. It delegates to subclasses for the actual generation of the IdList (when required)
 * and for the handling of update invalidations and any storage of sub-RecordSources that may be
 * required.
 * <p>
 * 
 * @author alex
 * @see #buildIdList(Viewpoint)
 * @see #getCacheKey()
 */
public abstract class AbstractRecordSource
  extends RecordSourceCore
{
  private IdList _ids;

  private RecordList _records;

  private volatile long _lastBuildTime = 0;

  private final Object __cacheKey;

  /**
   * @param tableWrapper
   *          The TableWrapper that this RecordSource contains records from. Not null.
   * @param cacheKey
   *          The cacheKey that this RecordSource will identify itself by. If this information is
   *          not available at initialisation then use null and override getCacheKey()
   *          appropriately. The getCacheKey() method will not be invoked until the subclasses
   *          creator has been initialised.
   * @see #getCacheKey()
   */
  public AbstractRecordSource(TableWrapper tableWrapper,
                              Object cacheKey)
  {
    super(Preconditions.checkNotNull(tableWrapper, "tableWrapper").getTable());
    __cacheKey = cacheKey;
  }

  /**
   * @return the value of immutable cacheKey from the creator. Very possibly null
   */
  @Override
  public Object getCacheKey()
  {
    return __cacheKey;
  }

  // private volatile boolean _hasResolvedRecords = false;

  /**
   * Get the IdList as defined by buildIdList, rebuilding if necessary. The rebuilding will take
   * place if this RecordSource is transactionally sensitive and viewpoint is not null (because it
   * may have changed due to the transaction), if it has never been built before, or if
   * getLastChangeTime() is more recent than the last time that it was rebuilt. If it doesn't need
   * rebuilding then the existing cached version will be returned.
   */
  @Override
  public final IdList getIdList(Viewpoint viewpoint)
  {
    viewpoint = transformViewpoint(viewpoint);
    if (viewpoint != null && shouldRebuild(viewpoint)) {
      IdList ids = new UnmodifiableIdList(buildIdList(viewpoint));
      // System.err.println("REBUILD " + this + " --> " + ids);
      return ids;
    }
    if (_ids == null | getLastChangeTime() >= _lastBuildTime) {
      synchronized (this) {
        if (_ids == null | getLastChangeTime() >= _lastBuildTime) {
          _ids = new UnmodifiableIdList(buildIdList(null));
          _records = new RecordList(_ids, null);
          // _hasResolvedRecords = false;
          _lastBuildTime = System.currentTimeMillis();
        }
      }
    }
    if (_ids.size() > 1 & viewpoint != null && shouldReorder(viewpoint)) {
      RecordOrdering recordOrdering = getRecordOrdering();
      if (recordOrdering != null) {
        // System.err.print("REORDER " + this + " from " + _ids + " --> ");
        IdList ids = new UnmodifiableIdList(recordOrdering.applyJavaOrdering(_ids, viewpoint));
        // System.err.println(ids);
        return ids;
      }
    }
    return _ids;
  }

  /**
   * @return false because the majority of RecordSources don't reorder their contained values.
   *         Override as necessary.
   */
  @Override
  public boolean shouldReorder(Viewpoint viewpoint)
  {
    return false;
  }

  public final long getLastBuildTime()
  {
    return _lastBuildTime;
  }

  /**
   * Get the IdList that was last built for the no-viewpoint perspective if any. This is primarily
   * intended for implementations that want to make decisions about the previous results for a
   * RecordSource when they are forced to rebuild the IdList as it is not unreasonable to assume
   * that the size may be similar between the two builds...
   * 
   * @return The appropriate immutable IdList or null if the IdList has not yet been built for this
   *         RecordSource.
   */
  protected final IdList getCachedIdList()
  {
    return _ids;
  }

  /**
   * Get the Record view perspective onto the current value of getIds(viewpoint). If the transformed
   * viewpoint is null then this may well return a cached result, but requests that have a
   * Transactional viewpoint will recreate the view each time on the principle that caching
   * transactional information is not a good use of resources.
   * 
   * @see #getIdList(Viewpoint)
   * @see #transformViewpoint(Viewpoint)
   */
  @Override
  public final RecordList getRecordList(Viewpoint viewpoint)
  {
    viewpoint = transformViewpoint(viewpoint);
    IdList idList = getIdList(viewpoint);
    if (viewpoint != null) {
      return new RecordList(idList, viewpoint);
    }
    // Table.resolveRecords(idList, 0, getTable().getPrecacheSize());
    // if (!_hasResolvedRecords) {
    // Table.resolveRecords(idList, 0, idList.size());
    // _hasResolvedRecords = true;
    // }
    return _records;
  }

  /**
   * @return null - ie undefined ordering. If your implementation of AbstractRecordSource has an
   *         ordering then it should override this method.
   */
  @Override
  public RecordOrdering getRecordOrdering()
  {
    return null;
  }

  /**
   * Rebuild the IdList that this RecordSource encapsulates from the viewpoint of the given
   * Transaction. This will get invoked by getIdList(viewpoint) when it determines that the
   * previously cached value is redundant and needs rebuilding.
   * 
   * @param viewpoint
   *          The Transactional perspective that should be utilised when rebuilding the IdList. If
   *          this RecordSource returns false from isTransactionSensitive(viewpoint) then the
   *          generated IdList should be the same for buildIdList(viewpoint) as it is for
   *          buildIdList(null).
   * @return The IdList (which is also a List<Integer>). Should not be null.
   */
  protected abstract IdList buildIdList(Viewpoint viewpoint);

  /**
   * Has the id list ever been built?
   * 
   * @return true if getRecordList() or getIdList() has been invoked
   */
  public final boolean isBuilt()
  {
    return _ids != null;
  }
}
