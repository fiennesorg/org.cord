package org.cord.mirror.recordsource;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.util.Gettable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

/**
 * RecordSource filter that uses a Predicate Gettable to decide whether or not a record should be
 * included.
 * 
 * @author alex
 */
public class PredicateGettableFilter
  extends RecordSourceWrapper
{

  private final Predicate<Gettable> __filter;

  public PredicateGettableFilter(RecordSource wrappedSource,
                                 Predicate<Gettable> filter,
                                 Object cacheKey)
  {
    super(Preconditions.checkNotNull(wrappedSource, "wrappedSource").getTable(),
          cacheKey,
          wrappedSource);
    __filter = Preconditions.checkNotNull(filter, "filter");
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    RecordList records = getWrappedRecordSource().getRecordList(viewpoint);
    ArrayIdList ids = new ArrayIdList(getTable(), records.size());
    for (PersistentRecord record : records) {
      if (__filter.apply(record)) {
        ids.add(record.getId());
      }
    }
    return ids;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper("PredicateGettableFilter")
                      .addValue(getWrappedRecordSource())
                      .addValue(__filter)
                      .toString();
  }

}
