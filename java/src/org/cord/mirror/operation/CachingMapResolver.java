package org.cord.mirror.operation;

import java.util.Map;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Caster;

/**
 * An implementation of a Parametric RecordOperation that gets items out of a given CachingMap with
 * optional filtering of the values as they are retrieved.
 * 
 * @author alex
 * @param <K>
 *          The class of the keys in the CachingMap
 * @param <V>
 *          The class of the values in the CachingMap
 * @see org.cord.mirror.operation.CachingMap
 */
public class CachingMapResolver<K, V>
  extends MonoRecordOperation<K, V>
{
  private final String __cachingMapKey;

  public CachingMapResolver(MonoRecordOperationKey<K, V> name,
                            String cachingMapKey,
                            Caster<K> casterK)
  {
    super(name,
          casterK);
    __cachingMapKey = cachingMapKey;
  }

  @SuppressWarnings("unchecked")
  @Override
  public V calculate(TransientRecord record,
                     Viewpoint viewpoint,
                     K key)
  {
    Map<K, V> map = (Map<K, V>) record.get(__cachingMapKey);
    key = filterMapKey(map, record, viewpoint, key);
    return filterMapValue(map, record, viewpoint, key, map.get(key));
  }

  /**
   * Perform any transformations that are necessary to convert the key passed in as a parameter into
   * a key suitable to query the map.
   * 
   * @return key, ie no transform at all. override to provide alternative implementations
   */
  protected K filterMapKey(Map<K, V> map,
                           TransientRecord record,
                           Viewpoint viewpoint,
                           K key)
  {
    return key;
  }

  /**
   * Perform any transformations that are necessary to convert the value stored in the CachingMap
   * into the value returned to the user.
   * 
   * @return value, ie no transform at all. override to provide alternative implementations
   */
  protected V filterMapValue(Map<K, V> map,
                             TransientRecord record,
                             Viewpoint viewpoint,
                             K var,
                             V value)
  {
    return value;
  }
}