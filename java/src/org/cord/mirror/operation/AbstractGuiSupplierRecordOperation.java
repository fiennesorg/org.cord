package org.cord.mirror.operation;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;

public abstract class AbstractGuiSupplierRecordOperation
  extends SimpleRecordOperation<String>
  implements HtmlGuiSupplier
{
  private final String __htmlLabel;

  public AbstractGuiSupplierRecordOperation(RecordOperationKey<String> name,
                                            String htmlLabel)
  {
    super(name);
    __htmlLabel = htmlLabel;
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, getOperation(record, viewpoint), namePrefix, getName());
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    appendHtmlValue(record, viewpoint, namePrefix, buf, context);
  }

  @Override
  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf, getHtmlLabel(), false, namePrefix, getName());
    if (hasHtmlWidget(record)) {
      appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    } else {
      appendHtmlValue(record, viewpoint, namePrefix, buf, context);
    }
    HtmlUtilsTheme.closeDiv(buf);
  }

  @Override
  public boolean hasHtmlWidget(TransientRecord record)
  {
    return false;
  }

  @Override
  public String getHtmlLabel()
  {
    return __htmlLabel;
  }

  @Override
  public boolean shouldReloadOnChange()
  {
    return false;
  }
}
