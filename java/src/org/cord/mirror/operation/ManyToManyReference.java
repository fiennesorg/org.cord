package org.cord.mirror.operation;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.GettableRecordProcessor;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceResolver;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.CompoundRecordSourceFilter;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.PostTransactionDeleteTrigger;
import org.cord.util.Casters;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * ManyToManyReference provides a way of resolving the records that are linked via an intermediate
 * link Table without having to reference the Table and then reference the resolving links.
 */
public class ManyToManyReference
  extends SimpleRecordOperation<RecordSource>
  implements RecordSourceResolver, HtmlGuiSupplier
{
  /**
   * @deprecated in preference of {@link #ISDEFINED()} as we might have different index values for
   *             different tables...
   */
  @Deprecated
  public final static ObjectFieldKey<Boolean> ISDEFINED = BooleanField.createKey("isDefined");

  public final static ObjectFieldKey<Boolean> ISDEFINED()
  {
    return BooleanField.createKey("isDefined");
  }

  public final static String LINKS = "Links";

  public final static String INCLUDES = "Includes";

  // public final static String JOIN = "Join";
  public final static String IDSUFFIX = "_" + TransientRecord.FIELD_ID;

  private final Table __linkTable;

  // `linkTableName
  // tableOne_id (tableOne) --> TableOne
  // tableTwo_id (tableTwo) --> TableTwo
  private final String __localLinkName;

  private final String __referenceLinkName;

  private final Table __referenceTable;

  private final String __order;

  private final String __label;

  private final RecordOperationKey<RecordSource> __linksKey;

  /**
   * @param name
   *          The name by which the operation is invoked.
   * @param label
   *          The label used in the appendFormElement HTML
   * @param linkTableName
   *          The name of the Table that is to contain the many to many reference information.
   * @param localLinkName
   *          The name of the field in the linkTableName Table that contains the reference back to
   *          the local Table that this RecordOperation is to be registered on.
   * @param referenceLinkName
   *          The name of the field in the linkTableName Table that contains the reference to the
   *          referenced Table that this will resolve to a Query across.
   * @param referenceTableName
   *          The name of the referenced Table.
   */
  public ManyToManyReference(Db db,
                             RecordOperationKey<RecordSource> name,
                             String label,
                             String linkTableName,
                             String localLinkName,
                             RecordOperationKey<RecordSource> linksKey,
                             String referenceLinkName,
                             String referenceTableName,
                             String order)
  {
    super(name);
    __label = StringUtils.padEmpty(label, name.toString());
    __linkTable = db.getTable(linkTableName);
    __localLinkName = localLinkName;
    __linksKey = linksKey;
    __referenceLinkName = referenceLinkName;
    __referenceTable = db.getTable(referenceTableName);
    __order = order;
  }

  /**
   * Append the appropriate filter to an existing StringBuilder to generate a search for defined
   * many:many relationships. The structure generated will be as follows:- <blockquote>
   * (&lt;linkTableName&gt;.&lt;sourceLinkName&gt;=&lt;sourceTableName&gt;.id) and
   * (&lt;linkTableName&gt;.&lt;referenceLinkName&gt;=&lt;referenceTableName &gt;.id) [and
   * (&lt;linkTableName&gt;.&lt;isDefinedField&gt;=1)] </blockquote> with the isDefinedField clause
   * ommitted if isDefinedField is null.
   * 
   * @param referenceId
   *          If greater than 0 then the target table is not joined in directly but instead matched
   *          against the referenceId.
   */
  public static void appendIsDefinedLinkFilter(String sourceTableName,
                                               String referenceTableName,
                                               String linkTableName,
                                               String sourceLinkName,
                                               String referenceLinkName,
                                               int referenceId,
                                               String isDefinedField,
                                               StringBuilder filter)
  {
    filter.append('(')
          .append(linkTableName)
          .append('.')
          .append(sourceLinkName)
          .append('=')
          .append(sourceTableName)
          .append("." + TransientRecord.FIELD_ID + ") and (")
          .append(linkTableName)
          .append('.')
          .append(referenceLinkName)
          .append('=');
    if (referenceId > 0) {
      filter.append(referenceId);
    } else {
      filter.append(referenceTableName).append("." + TransientRecord.FIELD_ID);
    }
    filter.append(')');
    if (isDefinedField != null) {
      filter.append(" and (")
            .append(linkTableName)
            .append('.')
            .append(isDefinedField)
            .append("=1)");
    }
  }

  public static void appendIsDefinedFilter(String linkTableName,
                                           String referenceTableName,
                                           String sourceLinkName,
                                           String referenceLinkName,
                                           String isDefinedName,
                                           int sourceId,
                                           StringBuilder filter)
  {
    filter.append('(')
          .append(linkTableName)
          .append('.')
          .append(sourceLinkName)
          .append('=')
          .append(sourceId)
          .append(" AND ")
          .append(linkTableName)
          .append('.')
          .append(referenceLinkName)
          .append('=')
          .append(referenceTableName)
          .append("." + TransientRecord.FIELD_ID + " AND ")
          .append(linkTableName)
          .append('.')
          .append(isDefinedName)
          .append("=1)");
  }

  /**
   * Append the appropriate filter for performing a simple, non-compound join filter for a many:many
   * reference. The structure generated will be as follows:- <blockquote>
   * &lt;linkTableName&gt;.&lt;sourceLinkName&gt;=&lt;sourceId&gt; and
   * &lt;linkTabletName&gt;.&lt;&gt; and &lt;linkTableName&gt;.isDefined=1 </blockquote> where
   * <blockquote> &lt;linkTableName&gt;=linkTable.getName() </blockquote>
   */
  public static void appendIsDefinedFilter(Table linkTable,
                                           Table referenceTable,
                                           String sourceLinkName,
                                           String referenceLinkName,
                                           int sourceId,
                                           StringBuilder filter)
  {
    filter.append('(')
          .append(linkTable.getName())
          .append('.')
          .append(sourceLinkName)
          .append('=')
          .append(sourceId)
          .append(" AND ")
          .append(linkTable.getName())
          .append('.')
          .append(referenceLinkName)
          .append('=')
          .append(referenceTable.getName())
          .append("." + TransientRecord.FIELD_ID + " AND ")
          .append(linkTable.getName())
          .append("." + ISDEFINED + "=1)");
  }

  /**
   * Create the query that models all the target records that have a relation with the source
   * record.
   * 
   * @param linkTable
   *          The Table that is being used as the link Table between the many:many records
   * @param referenceTable
   *          The Table from which the target records are going to be returned.
   * @param sourceLinkName
   *          The field name on the linkTable that points at the source record.
   * @param referenceLinkName
   *          The field name on the linkTable that points at the target record
   * @param sourceId
   *          The id of the source record from which the query is being run.
   */
  public static Query getIsDefinedQuery(Table linkTable,
                                        Table referenceTable,
                                        String sourceLinkName,
                                        String referenceLinkName,
                                        int sourceId,
                                        String order)
  {
    StringBuilder filter = new StringBuilder();
    appendIsDefinedFilter(linkTable,
                          referenceTable,
                          sourceLinkName,
                          referenceLinkName,
                          sourceId,
                          filter);
    return referenceTable.getQuery(null, filter.toString(), order, linkTable);
  }

  @Override
  public RecordSource resolveRecordSource(TransientRecord targetRecord,
                                          Viewpoint viewpoint)
  {
    return getTargetRecords(targetRecord, viewpoint);
  }

  /**
   * @return RecordSource of PersistentRecords from the reference Table.
   * @see RecordSource
   */
  @Override
  public RecordSource getOperation(TransientRecord localRecord,
                                   Viewpoint viewpoint)
  {
    return getTargetRecords(localRecord, viewpoint);
  }

  protected RecordSource getTargetsRecordSource(TransientRecord localRecord,
                                                Viewpoint viewpoint)
  {
    if (viewpoint != null && viewpoint.getRecordCount(__linkTable) > 0) {
      StringBuilder filter = new StringBuilder();
      filter.append(__localLinkName).append('=').append(localRecord.getId());
      Query links = __linkTable.getQuery(null, filter.toString(), null);
      return new CompoundRecordSourceFilter(links, null) {
        @Override
        protected boolean accepts(PersistentRecord record)
        {
          return record.is(ISDEFINED);
        }

        /**
         * @return true if either the viewpoint has a locked record in the link table or if the
         *         parent implementation says it is.
         */
        @Override
        public boolean shouldRebuild(Viewpoint viewpoint)
        {
          return viewpoint.getRecordCount(__linkTable) > 0 || super.shouldRebuild(viewpoint);
        }
      };
    } else {
      return RecordSources.viewedFrom(getIsDefinedQuery(__linkTable,
                                                        __referenceTable,
                                                        __localLinkName,
                                                        __referenceLinkName,
                                                        localRecord.getId(),
                                                        __order),
                                      viewpoint);
    }
  }

  protected RecordSource getTargetRecords(TransientRecord localRecord,
                                          Viewpoint viewpoint)
  {
    if (viewpoint != null && viewpoint.getRecordCount(__linkTable) > 0) {
      StringBuilder filter = new StringBuilder();
      filter.append(__localLinkName).append('=').append(localRecord.getId());
      Query links = __linkTable.getQuery(null, filter.toString(), null);
      filter = new StringBuilder();
      filter.append(__linkTable.getName()).append(".id in (");
      boolean hasMatches = false;
      Iterator<PersistentRecord> i = links.getRecordList(viewpoint).iterator();
      while (i.hasNext()) {
        PersistentRecord link = i.next();
        if (link.is(ISDEFINED)) {
          if (hasMatches) {
            filter.append(',');
          } else {
            hasMatches = true;
          }
          filter.append(link.getId());
        }
      }
      if (!hasMatches) {
        filter.append("-666");
      }
      filter.append(')');
      filter.append(" and (")
            .append(__referenceTable.getName())
            .append(".id=")
            .append(__linkTable.getName())
            .append('.')
            .append(__referenceLinkName)
            .append(')');
      return RecordSources.viewedFrom(__referenceTable.getQuery(null,
                                                                filter.toString(),
                                                                __order,
                                                                __linkTable),
                                      viewpoint);
    } else {
      return RecordSources.viewedFrom(getIsDefinedQuery(__linkTable,
                                                        __referenceTable,
                                                        __localLinkName,
                                                        __referenceLinkName,
                                                        localRecord.getId(),
                                                        __order),
                                      viewpoint);
    }
  }

  /**
   * Assign a given Table as a link Table in a many-to-many relationship. This will have the
   * following effects:-
   * <ul>
   * <li>creation of a Table named linkTableName if one doesn't already exist.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableOneName&gt;_id to tableOne.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableTwoName&gt;_id to tableTwo.
   * <li>creation of a BooleanFieldFilter on the linkTable named isDefined.
   * <li>creation of a ManyToManyReference on tableOne named tableTwoReferencesName.
   * <li>creation of a ManyToManyReference on tableTwo named tableOneReferencesName.
   * </ul>
   * <p>
   * If you want to have your Table bound to a TableWrapper (which you should) then you should
   * create the Table accordingly before invoking registerComponents and then it should all just
   * work.
   * </p>
   * <p>
   * This link table should be empty when you call this - ie have no fields added yet other than the
   * automatic id field. If there are then the operation will fail.
   * </p>
   * 
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param linkTableName
   *          The name of the Table that is going to store the relationship between tableOne and
   *          tableTwo. If this Table doesn't already exist then it will be created. If the value is
   *          null then the value will be &lt;tableOneName&gt;&lt;tableTwoName&gt;
   * @param linkTableOrder
   *          The default ordering clause that is going to be applied to the linkTable if it is
   *          created.
   * @param tableOneName
   *          The name of tableOne. This must resolve to a valid created Table in db.
   * @param tableOneOrder
   *          The order in which records from tableOne should be returned in. null utilises the
   *          default table ordering from tableOne.
   * @param tableTwoReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableOne to
   *          retrieve linked records in tableTwo. If null, then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableOneEdit
   *          If true then taking an edit lock on a record from tableOne will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @param tableTwoName
   *          The name of tableTwo. This must resolve to a valid created Table in db.
   * @param tableTwoOrder
   *          The order in which the records from tableTwo should be returned in. null utilises the
   *          default table ordering from tableTwo.
   * @param tableOneReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableTwo to
   *          retrieve linked records in tableOne. If null then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableTwoEdit
   *          If true then taking an edit lock on a record from tableTwo will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @return The Table that holds the linkage between tableOne and tableTwo. This may or may not
   *         have been created by the method or may be an existing Table.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         String label,
                                         String linkTableName,
                                         String linkTableOrder,
                                         String tableOneName,
                                         String tableOneOrder,
                                         RecordOperationKey<RecordSource> tableTwoReferencesName,
                                         boolean lockOnTableOneEdit,
                                         String tableTwoName,
                                         String tableTwoOrder,
                                         RecordOperationKey<RecordSource> tableOneReferencesName,
                                         boolean lockOnTableTwoEdit,
                                         boolean isOptionalLinks)
      throws MirrorTableLockedException
  {
    return registerComponents(db,
                              transactionPassword,
                              label,
                              linkTableName,
                              linkTableOrder,
                              tableOneName,
                              tableOneOrder,
                              tableTwoReferencesName,
                              lockOnTableOneEdit,
                              false,
                              tableTwoName,
                              tableTwoOrder,
                              tableOneReferencesName,
                              lockOnTableTwoEdit,
                              false,
                              isOptionalLinks);
  }

  /**
   * Assign a given Table as a link Table in a many-to-many relationship. This will have the
   * following effects:-
   * <ul>
   * <li>creation of a Table named linkTableName if one doesn't already exist.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableOneName&gt;_id to tableOne.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableTwoName&gt;_id to tableTwo.
   * <li>creation of a BooleanFieldFilter on the linkTable named isDefined.
   * <li>creation of a ManyToManyReference on tableOne named tableTwoReferencesName.
   * <li>creation of a ManyToManyReference on tableTwo named tableOneReferencesName.
   * </ul>
   * <p>
   * If you want to have your Table bound to a TableWrapper (which you should) then you should
   * create the Table accordingly before invoking registerComponents and then it should all just
   * work.
   * </p>
   * <p>
   * This link table should be empty when you call this - ie have no fields added yet other than the
   * automatic id field. If there are then the operation will fail.
   * </p>
   * 
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param linkTableName
   *          The name of the Table that is going to store the relationship between tableOne and
   *          tableTwo. If this Table doesn't already exist then it will be created. If the value is
   *          null then the value will be &lt;tableOneName&gt;&lt;tableTwoName&gt;
   * @param linkTableOrder
   *          The default ordering clause that is going to be applied to the linkTable if it is
   *          created.
   * @param tableOneName
   *          The name of tableOne. This must resolve to a valid created Table in db.
   * @param tableOneOrder
   *          The order in which records from tableOne should be returned in. null utilises the
   *          default table ordering from tableOne.
   * @param tableTwoReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableOne to
   *          retrieve linked records in tableTwo. If null, then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableOneEdit
   *          If true then taking an edit lock on a record from tableOne will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @param deleteTableOneWithTwo
   *          if true then if a referenced record in table two is scheduled for deletion then this
   *          will extend to the linked record in table one as well as the link record.
   * @param tableTwoName
   *          The name of tableTwo. This must resolve to a valid created Table in db.
   * @param tableTwoOrder
   *          The order in which the records from tableTwo should be returned in. null utilises the
   *          default table ordering from tableTwo.
   * @param tableOneReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableTwo to
   *          retrieve linked records in tableOne. If null then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableTwoEdit
   *          If true then taking an edit lock on a record from tableTwo will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @param deleteTableTwoWithOne
   *          if true then if a referenced record in table one is scheduled for deletion then this
   *          will extend to the linked record in table two as well as the link record.
   * @return The Table that holds the linkage between tableOne and tableTwo. This may or may not
   *         have been created by the method or may be an existing Table.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         String label,
                                         String linkTableName,
                                         String linkTableOrder,
                                         String tableOneName,
                                         String tableOneOrder,
                                         RecordOperationKey<RecordSource> tableTwoReferencesName,
                                         boolean lockOnTableOneEdit,
                                         boolean deleteTableOneWithTwo,
                                         String tableTwoName,
                                         String tableTwoOrder,
                                         RecordOperationKey<RecordSource> tableOneReferencesName,
                                         boolean lockOnTableTwoEdit,
                                         boolean deleteTableTwoWithOne,
                                         boolean isOptionalLinks)
      throws MirrorTableLockedException
  {
    return registerComponents(db,
                              transactionPassword,
                              label,
                              linkTableName,
                              linkTableOrder,
                              tableOneName,
                              tableOneOrder,
                              tableTwoReferencesName,
                              lockOnTableOneEdit,
                              deleteTableOneWithTwo,
                              tableTwoName,
                              tableTwoOrder,
                              tableOneReferencesName,
                              lockOnTableTwoEdit,
                              deleteTableTwoWithOne,
                              isOptionalLinks,
                              isOptionalLinks);
  }

  /**
   * Assign a given Table as a link Table in a many-to-many relationship. This will have the
   * following effects:-
   * <ul>
   * <li>creation of a Table named linkTableName if one doesn't already exist.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableOneName&gt;_id to tableOne.
   * <li>creation of a LinkFieldFilter on the linkTable named &lt;tableTwoName&gt;_id to tableTwo.
   * <li>creation of a BooleanFieldFilter on the linkTable named isDefined.
   * <li>creation of a ManyToManyReference on tableOne named tableTwoReferencesName.
   * <li>creation of a ManyToManyReference on tableTwo named tableOneReferencesName.
   * </ul>
   * <p>
   * If you want to have your Table bound to a TableWrapper (which you should) then you should
   * create the Table accordingly before invoking registerComponents and then it should all just
   * work.
   * </p>
   * <p>
   * This link table should be empty when you call this - ie have no fields added yet other than the
   * automatic id field. If there are then the operation will fail.
   * </p>
   * 
   * @param label
   *          The visible label for the field as used in edit interfaces.
   * @param linkTableName
   *          The name of the Table that is going to store the relationship between tableOne and
   *          tableTwo. If this Table doesn't already exist then it will be created. If the value is
   *          null then the value will be &lt;tableOneName&gt;&lt;tableTwoName&gt;
   * @param linkTableOrder
   *          The default ordering clause that is going to be applied to the linkTable if it is
   *          created.
   * @param tableOneName
   *          The name of tableOne. This must resolve to a valid created Table in db.
   * @param tableOneOrder
   *          The order in which records from tableOne should be returned in. null utilises the
   *          default table ordering from tableOne.
   * @param tableTwoReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableOne to
   *          retrieve linked records in tableTwo. If null, then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableOneEdit
   *          If true then taking an edit lock on a record from tableOne will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @param deleteTableOneWithTwo
   *          if true then if a referenced record in table two is scheduled for deletion then this
   *          will extend to the linked record in table one as well as the link record.
   * @param tableTwoName
   *          The name of tableTwo. This must resolve to a valid created Table in db.
   * @param tableTwoOrder
   *          The order in which the records from tableTwo should be returned in. null utilises the
   *          default table ordering from tableTwo.
   * @param tableOneReferencesName
   *          The name of the ManyToManyReference operation that will be registered on tableTwo to
   *          retrieve linked records in tableOne. If null then the defaultReferencesName will be
   *          utilised.
   * @param lockOnTableTwoEdit
   *          If true then taking an edit lock on a record from tableTwo will result in all the
   *          associated records from linkTable being locked for updating as well.
   * @param deleteTableTwoWithOne
   *          if true then if a referenced record in table one is scheduled for deletion then this
   *          will extend to the linked record in table two as well as the link record.
   * @return The Table that holds the linkage between tableOne and tableTwo. This may or may not
   *         have been created by the method or may be an existing Table.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         String label,
                                         String linkTableName,
                                         String linkTableOrder,
                                         String tableOneName,
                                         String tableOneOrder,
                                         RecordOperationKey<RecordSource> tableTwoReferencesName,
                                         boolean lockOnTableOneEdit,
                                         boolean deleteTableOneWithTwo,
                                         String tableTwoName,
                                         String tableTwoOrder,
                                         RecordOperationKey<RecordSource> tableOneReferencesName,
                                         boolean lockOnTableTwoEdit,
                                         boolean deleteTableTwoWithOne,
                                         boolean isOptionalLinkOne,
                                         boolean isOptionalLinkTwo)
      throws MirrorTableLockedException
  {
    linkTableName = linkTableName == null ? tableOneName + tableTwoName : linkTableName;
    tableTwoReferencesName = Table.getDefaultReferencesName(tableTwoName, tableTwoReferencesName);
    tableOneReferencesName = Table.getDefaultReferencesName(tableOneName, tableOneReferencesName);
    Table tableOne = db.getTable(tableOneName);
    Table tableTwo = db.getTable(tableTwoName);
    Table linkTable = null;
    try {
      linkTable = db.getTable(linkTableName);
    } catch (RuntimeException nonInitialisedLinkTableEx) {
      linkTable = db.addSoftTable(null, linkTableName, null);
      linkTable.setDefaultOrdering(linkTableOrder);
    }
    Preconditions.checkState(linkTable.getFields().size() == 1,
                             "ManyToManyReference cannot be used with %s after %s",
                             linkTableName,
                             linkTable.getFields());
    RecordOperationKey<RecordSource> tableTwoLinks = getLinksKey(tableTwoReferencesName);
    String tableOneLinkName =
        linkTable.addField(new LinkField(null,
                                         "Referenced " + tableOneName,
                                         null,
                                         null,
                                         tableOneName,
                                         tableTwoLinks,
                                         null,
                                         transactionPassword,
                                         Dependencies.LINK_ENFORCED
                                                              | (lockOnTableOneEdit
                                                                  ? Dependencies.BIT_S_ON_T_UPDATE
                                                                  : 0)
                                                              | (isOptionalLinkOne
                                                                  ? Dependencies.BIT_OPTIONAL
                                                                  : 0)
                                                              | (deleteTableOneWithTwo
                                                                  ? Dependencies.BIT_T_ON_S_DELETE
                                                                  : 0)))
                 .getName();
    RecordOperationKey<RecordSource> tableOneLinks = getLinksKey(tableOneReferencesName);
    String tableTwoLinkName =
        linkTable.addField(new LinkField(null,
                                         "Referenced " + tableTwoName,
                                         null,
                                         null,
                                         tableTwoName,
                                         tableOneLinks,
                                         null,
                                         transactionPassword,
                                         Dependencies.LINK_ENFORCED
                                                              | (lockOnTableTwoEdit
                                                                  ? Dependencies.BIT_S_ON_T_UPDATE
                                                                  : 0)
                                                              | (isOptionalLinkTwo
                                                                  ? Dependencies.BIT_OPTIONAL
                                                                  : 0)
                                                              | (deleteTableTwoWithOne
                                                                  ? Dependencies.BIT_T_ON_S_DELETE
                                                                  : 0)))
                 .getName();
    final ObjectFieldKey<Boolean> isDefined = ISDEFINED();
    linkTable.addField(new BooleanField(isDefined, "Link is defined?"));
    tableOne.addRecordOperation(new ManyToManyReference(db,
                                                        tableTwoReferencesName,
                                                        label,
                                                        linkTableName,
                                                        tableOneLinkName,
                                                        tableTwoLinks,
                                                        tableTwoLinkName,
                                                        tableTwoName,
                                                        tableTwoOrder));
    tableOne.addRecordOperation(new Includes(MonoRecordOperationKey.create(tableTwoReferencesName
                                                                           + INCLUDES,
                                                                           Object.class,
                                                                           Boolean.class),
                                             linkTable,
                                             tableTwo));
    tableTwo.addRecordOperation(new ManyToManyReference(db,
                                                        tableOneReferencesName,
                                                        null,
                                                        linkTableName,
                                                        tableTwoLinkName,
                                                        tableOneLinks,
                                                        tableOneLinkName,
                                                        tableOneName,
                                                        tableOneOrder));
    tableTwo.addRecordOperation(new Includes(MonoRecordOperationKey.create(tableOneReferencesName
                                                                           + INCLUDES,
                                                                           Object.class,
                                                                           Boolean.class),
                                             linkTable,
                                             tableOne));
    PostTransactionDeleteTrigger.register(linkTable,
                                          isDefined,
                                          transactionPassword,
                                          linkTable.getName() + "." + ISDEFINED + " cleanup");
    return linkTable;
  }

  /**
   * Construct the RecordOperationKey to resolve all instances of the join table from a target
   * record.
   * 
   * @param referencesName
   *          The name of the operation to resolve the opposite end of the relationship
   * @return RecordSource key with the name being referencesName + {@link #LINKS}
   */
  public static RecordOperationKey<RecordSource> getLinksKey(String referencesName)
  {
    return RecordOperationKey.create(referencesName + LINKS, RecordSource.class);
  }

  /**
   * Construct the RecordOperationKey to resolve all instances of the join table from a target
   * record.
   * 
   * @param referencesKey
   *          The name of the operation to resolve the opposite end of the relationship
   * @return RecordSource key with the name being referencesName + {@link #LINKS}
   */
  public static RecordOperationKey<RecordSource> getLinksKey(RecordOperationKey<RecordSource> referencesKey)
  {
    return getLinksKey(referencesKey.getKeyName());
  }

  /**
   * @param label
   *          The visible label for the field as used in edit interfaces.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         String label,
                                         String tableOneName,
                                         boolean lockOnTableOneEdit,
                                         String tableTwoName,
                                         boolean lockOnTableTwoEdit)
      throws MirrorTableLockedException
  {
    return registerComponents(db,
                              transactionPassword,
                              label,
                              null,
                              null,
                              tableOneName,
                              null,
                              null,
                              lockOnTableOneEdit,
                              tableTwoName,
                              null,
                              null,
                              lockOnTableTwoEdit,
                              false);
  }

  /**
   * Register the components for a ManyToManyReference with default everything, but a custom link
   * Table name.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         String label,
                                         String linkTableName,
                                         String tableOneName,
                                         boolean lockOnTableOneEdit,
                                         String tableTwoName,
                                         boolean lockOnTableTwoEdit)
      throws MirrorTableLockedException
  {
    return registerComponents(db,
                              transactionPassword,
                              label,
                              linkTableName,
                              null,
                              tableOneName,
                              null,
                              null,
                              lockOnTableOneEdit,
                              tableTwoName,
                              null,
                              null,
                              lockOnTableTwoEdit,
                              false);
  }

  /**
   * Get the Query that resolves to the record from the given linkTable that models the behaviour
   * between a pair of records. Please note that this does not pay attention to the ISDEFINED field.
   * 
   * @param linkTable
   *          the Table that contains the relationship
   * @param recordOne
   *          The first record in the desired relationship tuplet.
   * @param recordTwo
   *          The second record in the desired relationship tuplet.
   * @return The Query that should resolve to the record from the targetted linkTable. This Query
   *         should be of length 0 or 1. If length 0, then there is no relationship between
   *         recordOne and recordTwo. If of length 1, then the relationship is defined by the
   *         ISDEFINED field in the reference link record.
   */
  public static Query getRelationshipQuery(Table linkTable,
                                           TransientRecord recordOne,
                                           TransientRecord recordTwo)
  {
    Preconditions.checkNotNull(recordOne, "recordOne");
    Preconditions.checkNotNull(recordTwo, "recordTwo");
    StringBuilder filter = new StringBuilder();
    filter.append(toLinkName(recordOne))
          .append('=')
          .append(recordOne.getId())
          .append(" AND ")
          .append(toLinkName(recordTwo))
          .append('=')
          .append(recordTwo.getId());
    return linkTable.getQuery(null, filter.toString(), null);
  }

  /**
   * Get the Query that resolves to all link records that relate to the given record.
   * 
   * @param linkTable
   *          The Table that holds the relationship from record to another set of records.
   * @param record
   *          The record from which we wish to find the relationships.
   * @return The appropriate Query. This will contain records from linkTable and will have length
   *         0..n.
   */
  public static Query getLinkQuery(Table linkTable,
                                   TransientRecord record)
  {
    return linkTable.getQuery(null, toLinkName(record) + '=' + record.getId(), null);
  }

  /**
   * Get the relationship that is modelled between two records via a link table.
   * 
   * @param linkTable
   *          The Table that holds the link relationship between recordOne and recordTwo.
   * @param recordOne
   *          The first record...
   * @param recordTwo
   *          The second record...
   * @param viewpoint
   *          Transactional filtering
   * @return true if recordOne and recordTwo have a defined relationship from the given trnasaction
   *         viewpoint.
   */
  public static boolean getRelationship(Table linkTable,
                                        TransientRecord recordOne,
                                        TransientRecord recordTwo,
                                        Viewpoint viewpoint)
  {
    if (recordOne == null || recordTwo == null) {
      return false;
    }
    PersistentRecord link =
        RecordSources.getOptOnlyRecord(getRelationshipQuery(linkTable, recordOne, recordTwo),
                                       viewpoint);
    return link == null ? false : link.is(ISDEFINED);
  }

  /**
   * Attempt to define the links from a given record through a linkTable as given by an Iterator of
   * ids. The method will initially clear all of the links defined to record and then attempt to
   * re-create the appropriate links to the given ids. The method is transactionally clear with all
   * of the operations taking place inside the given updateTransaction. The only possible
   * side-effect is the creation of some "new" non-defined link records that are functionally
   * invisible. There is a wide range of errors that can potentially be thrown, but the majority of
   * these are either due to invalid data inside the recordTwoIds Iterator or from 3rd party
   * registered operations on the link table. Either way, cancelling the updateTransaction will
   * roll-back from the operation if so required. Note that if the operation is sucessfull, then all
   * of the information will still be held inside the updateTransaction and as such will not get
   * committed until the client decides that it is now time.
   * 
   * @param linkTable
   *          The link table that is going to hold the relationships.
   * @param recordOne
   *          The record from which all of the links are to be defined.
   * @param tableTwo
   *          The Table that contains the records that are to be linked to recordOne via the
   *          linkTable.
   * @param recordTwos
   *          Iterator of Objects which are either Integers, TransientRecords or when converted to
   *          Strings via .toString() may then be parsed as record ids under tableTwo.
   * @param updateTransaction
   *          The Transaction under which the entire operation is carried out.
   * @param initialiser
   *          The optional GettableRecordProcessor that will be used to maintain additional metadata
   *          on the join records (if any)
   * @throws NumberFormatException
   *           if it is not possible to convert one of the Objects in recordTwoIds into an Integer
   *           after applying .toString() to the it.
   * @see Integer#valueOf(String)
   * @throws MirrorNoSuchRecordException
   *           If one of the integer ids in recordTwoIds does not translate to a PersistentRecord
   *           inside tableTwo.
   */
  public static boolean defineLinks(Gettable params,
                                    Table linkTable,
                                    IntFieldKey tableOneLinkKey,
                                    IntFieldKey tableTwoLinkKey,
                                    TransientRecord recordOne,
                                    Table tableTwo,
                                    Iterator<?> recordTwos,
                                    boolean ignoreBadIds,
                                    Transaction updateTransaction,
                                    long timeout,
                                    Statement statement,
                                    GettableRecordProcessor initialiser)
      throws MirrorNoSuchRecordException, MirrorFieldException, MirrorTransactionTimeoutException,
      MirrorInstantiationException
  {
    boolean isUpdated = false;
    // build a mapping of all of the existing records first of all...
    tableOneLinkKey = tableOneLinkKey == null
        ? LinkField.getLinkFieldName(recordOne.getTable().getName())
        : tableOneLinkKey;
    tableTwoLinkKey =
        tableTwoLinkKey == null ? LinkField.getLinkFieldName(tableTwo.getName()) : tableTwoLinkKey;
    Query linkQuery = getLinkQuery(linkTable, recordOne);
    Int2ObjectMap<PersistentRecord> idToLink = new Int2ObjectOpenHashMap<PersistentRecord>();
    for (PersistentRecord link : linkQuery.getRecordList(updateTransaction)) {
      idToLink.put(link.compInt(tableTwoLinkKey), link);
    }
    while (recordTwos.hasNext()) {
      Object next = recordTwos.next();
      int recordTwoId = 0;
      PersistentRecord recordTwo = null;
      try {
        if (next instanceof Integer) {
          recordTwoId = ((Integer) next).intValue();
          recordTwo = tableTwo.getRecord(recordTwoId, updateTransaction);
        } else if (next instanceof PersistentRecord) {
          recordTwo = (PersistentRecord) next;
          // TODO: Validate that this the PersistentRecords passed to
          // ManyToManyReference.defineLinks are for the correct table.
          recordTwoId = recordTwo.getId();
        } else {
          try {
            recordTwoId = Integer.parseInt(StringUtils.toString(next));
            recordTwo = tableTwo.getRecord(recordTwoId, updateTransaction);
          } catch (NumberFormatException nfEx) {
            if (!ignoreBadIds) {
              throw nfEx;
            }
          }
        }
      } catch (MirrorNoSuchRecordException badIdEx) {
        if (!ignoreBadIds) {
          throw badIdEx;
        }
      }
      if (recordTwo != null) {
        PersistentRecord link = idToLink.remove(recordTwoId);
        if (link == null) {
          TransientRecord tLink = linkTable.createTransientRecord();
          tLink.setField(tableTwoLinkKey, Integer.valueOf(recordTwoId));
          tLink.setField(tableOneLinkKey, Integer.valueOf(recordOne.getId()));
          link = tLink.makePersistent(params);
        }
        WritableRecord wLink = updateTransaction.edit(link);
        updateLinkState(params, wLink, updateTransaction, Boolean.TRUE, initialiser);
        isUpdated = isUpdated | wLink.getIsUpdated();
      }
    }
    Iterator<PersistentRecord> deadLinks = idToLink.values().iterator();
    while (deadLinks.hasNext()) {
      PersistentRecord link = deadLinks.next();
      WritableRecord wLink = updateTransaction.edit(link);
      updateLinkState(params, wLink, updateTransaction, Boolean.FALSE, initialiser);
      isUpdated = isUpdated | wLink.getIsUpdated();
    }
    return isUpdated;
  }

  /**
   * Create a link record (if necessary) between recordOne and recordTwo, check it into the
   * updateTransaction and set isDefined to true.
   * 
   * @return The link record that has been locked by the updateTransaction.
   */
  public static WritableRecord defineLink(Gettable params,
                                          Table linkTable,
                                          TransientRecord recordOne,
                                          TransientRecord recordTwo,
                                          Boolean isDefined,
                                          GettableRecordProcessor initialiser,
                                          Transaction updateTransaction,
                                          long timeout,
                                          PreparedStatement preparedStatement)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException
  {
    PersistentRecord link = initialiseLink(params,
                                           linkTable,
                                           recordOne.getTable().getName(),
                                           recordOne.getId(),
                                           recordTwo.getTable().getName(),
                                           recordTwo.getId(),
                                           updateTransaction,
                                           getRelationshipQuery(linkTable, recordOne, recordTwo),
                                           preparedStatement);
    WritableRecord wLink = updateTransaction.edit(link, timeout);
    updateLinkState(params, wLink, updateTransaction, isDefined, initialiser);
    return wLink;
  }

  /**
   * Update the values in a single locked link record according to the isDefined flag and the
   * optional initialiser.
   * 
   * @param initialiser
   *          The GettableRecordProcessor that will perform any additional work on the link record.
   *          If null then this stage is skipped. Note that this stage is updated after the
   *          ISDEFINED flag is updated so you can overrule this definition if required.
   */
  public static void updateLinkState(Gettable params,
                                     WritableRecord wLink,
                                     Transaction transaction,
                                     Boolean isDefined,
                                     GettableRecordProcessor initialiser)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException
  {
    if (isDefined != null) {
      wLink.setField(ISDEFINED, isDefined);
    }
    if (initialiser != null) {
      initialiser.process(wLink, params, transaction);
    }
  }

  private static PersistentRecord initialiseLink(Gettable params,
                                                 Table linkTable,
                                                 String tableOneName,
                                                 int recordOneId,
                                                 String tableTwoName,
                                                 int recordTwoId,
                                                 Transaction updateTransaction,
                                                 Query relationshipQuery,
                                                 PreparedStatement preparedStatement)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException
  {
    PersistentRecord link = RecordSources.getOptOnlyRecord(relationshipQuery, updateTransaction);
    if (link != null) {
      return link;
    }
    TransientRecord newLink = linkTable.createTransientRecord();
    newLink.setField(toLinkName(tableOneName), Integer.valueOf(recordOneId));
    newLink.setField(toLinkName(tableTwoName), Integer.valueOf(recordTwoId));
    return newLink.makePersistent(params, preparedStatement);
  }

  /**
   * Clear all of the links from record defined by linkTable . This is achieved by finding all of
   * the link records and setting the isDefined field to false. The operation takes place entirely
   * within the transaction space of updateTransaction, and therefore is reversible. If any errors
   * are thrown, then it is advised to cancel the updateTransaction to be sure of a clean roll-back.
   * 
   * @param linkTable
   *          The link Table between recordOne and recordTwo.
   * @param record
   *          the record whose links to clear.
   * @param updateTransaction
   *          The existing updateTransaction that will be utilised to hold the results of this
   *          operation.
   * @param timeout
   *          The maximum amount of time to wait while attempting to gain the lock on any contested
   *          link records.
   * @throws MirrorTransactionTimeoutException
   *           If there is a contested lock situation when acquiring the link records.
   * @throws MirrorFieldException
   *           If the value of false is rejected by the field isDefined. This will not happen unless
   *           there is a 3rd party FieldTrigger defined on the system.
   */
  public static void clearLinks(Table linkTable,
                                TransientRecord record,
                                Transaction updateTransaction,
                                long timeout)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    Query linkQuery = getLinkQuery(linkTable, record);
    clearLinks(linkTable, linkQuery, updateTransaction, timeout);
  }

  public static void clearLinks(Table linkTable,
                                RecordSource links,
                                Transaction updateTransaction,
                                long timeout)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    if (updateTransaction != null) {
      updateTransaction.edit(links, timeout);
      Iterator<PersistentRecord> linkRecords = links.getRecordList(updateTransaction).iterator();
      while (linkRecords.hasNext()) {
        PersistentRecord linkRecord = linkRecords.next();
        if (linkRecord.is(ISDEFINED)) {
          linkRecord.setField(ISDEFINED, Boolean.FALSE);
        }
      }
    }
  }

  /**
   * Convert a TableName (capitalized first letter) into an equivalent fieldName (lower case first
   * letter).
   * 
   * @param tableName
   *          the TableName to be converted.
   * @return The converted TableName or the same string if the first character was not an upper case
   *         character.
   * @deprecated in preference of LinkField properties
   */
  @Deprecated
  private final static String toFieldName(String tableName)
  {
    if (tableName.length() == 0) {
      return tableName;
    }
    char firstChar = tableName.charAt(0);
    if (Character.isUpperCase(firstChar)) {
      return Character.toLowerCase(firstChar) + tableName.substring(1);
    }
    return tableName;
  }

  /**
   * Convert a Table name into a field name for the reference to that table (TableName -->
   * tableName_id).
   * 
   * @deprecated in preference of version in LinkField
   */
  @Deprecated
  private final static String toLinkName(String tableName)
  {
    return toFieldName(tableName) + IDSUFFIX;
  }

  /**
   * @deprecated in preference of version in LinkField
   */
  @Deprecated
  private final static String toLinkName(Table table)
  {
    return toLinkName(table.getName());
  }

  /**
   * @deprecated in preference of version in LinkField
   */
  @Deprecated
  private final static String toLinkName(TransientRecord record)
  {
    return toLinkName(record.getTable());
  }

  public static class Includes
    extends MonoRecordOperation<Object, Boolean>
  {
    public static final int P0_RECORDTWO = 0;

    private final Table __linkTable;

    private final Table __tableTwo;

    protected Includes(MonoRecordOperationKey<Object, Boolean> name,
                       Table linkTable,
                       Table tableTwo)
    {
      super(name,
            Casters.NULL);
      __linkTable = linkTable;
      __tableTwo = tableTwo;
    }

    @Override
    public Boolean calculate(TransientRecord recordOne,
                             Viewpoint viewpoint,
                             Object recordTwoObj)
    {
      TransientRecord recordTwo = null;
      if (recordTwoObj instanceof TransientRecord) {
        recordTwo = (TransientRecord) recordTwoObj;
      } else {
        try {
          recordTwo = __tableTwo.getRecord(Integer.valueOf(recordTwoObj.toString()), viewpoint);
        } catch (NumberFormatException badFormattedIdEx) {
          return Boolean.FALSE;
        } catch (MirrorNoSuchRecordException noSuchTargetEx) {
          return Boolean.FALSE;
        }
      }
      return ManyToManyReference.getRelationship(__linkTable, recordOne, recordTwo, viewpoint)
          ? Boolean.TRUE
          : Boolean.FALSE;
    }
  }

  // public static class QueryJoin
  // extends AbstractQueryTransform
  // {
  // private final String __sourceTableName;
  //
  // private final Table __referenceTable;
  //
  // private final Table __linkTable;
  //
  // private final String __sourceLinkName;
  //
  // private final String __referenceLinkName;
  //
  // private final String __filter;
  //
  // public QueryJoin(String name,
  // Table sourceTable,
  // Table referenceTable,
  // Table linkTable,
  // String sourceLinkName,
  // String referenceLinkName)
  // {
  // super(name);
  // __sourceTableName = sourceTable.getName();
  // __referenceTable = referenceTable;
  // __linkTable = linkTable;
  // __sourceLinkName = sourceLinkName;
  // __referenceLinkName = referenceLinkName;
  // StringBuilder filter = new StringBuilder();
  // appendIsDefinedLinkFilter(__sourceTableName, referenceTable.getName(),
  // __linkTable
  // .getName(), sourceLinkName, referenceLinkName, 0, ISDEFINED, filter);
  // __filter = filter.toString();
  // }
  //
  // public Query transformQuery(Query sourceQuery,
  // String referenceTableName,
  // String linkTableName)
  // {
  // StringBuilder filter = new StringBuilder();
  // appendIsDefinedLinkFilter(__sourceTableName,
  // referenceTableName,
  // linkTableName,
  // __sourceLinkName,
  // __referenceLinkName,
  // 0,
  // ISDEFINED,
  // filter);
  // return ConstantQueryTransform.transformQuery(sourceQuery,
  // filter.toString(),
  // ConstantQueryTransform.TYPE_AND,
  // null,
  // __linkTable,
  // __referenceTable);
  // }
  //
  // public Query transformQuery(Query sourceQuery)
  // {
  // return ConstantQueryTransform.transformQuery(sourceQuery,
  // __filter,
  // ConstantQueryTransform.TYPE_AND,
  // null,
  // __linkTable,
  // __referenceTable);
  // }
  // }
  // public static class QueryIncludes
  // extends AbstractParametricQueryTransform
  // {
  // private final Table __linkTable;
  //
  // private final Table __sourceTable;
  //
  // private final Table __referenceTable;
  //
  // private final String __sourceLinkName;
  //
  // private final String __referenceLinkName;
  //
  // protected QueryIncludes(String name,
  // Table linkTable,
  // Table sourceTable,
  // Table referenceTable,
  // String sourceLinkName,
  // String referenceLinkName)
  // {
  // super(name);
  // __linkTable = linkTable;
  // __referenceTable = referenceTable;
  // __sourceLinkName = sourceLinkName;
  // __sourceTable = sourceTable;
  // __referenceLinkName = referenceLinkName;
  // }
  //
  // public Query transformQuery(Query sourceQuery,
  // int targetId)
  // {
  // StringBuilder filter = new StringBuilder();
  // appendIsDefinedFilter(__linkTable,
  // __referenceTable,
  // __sourceLinkName,
  // __referenceLinkName,
  // targetId,
  // filter);
  // return ConstantQueryTransform.transformQuery(sourceQuery,
  // filter.toString(),
  // ConstantQueryTransform.TYPE_AND,
  // null,
  // __linkTable,
  // __sourceTable);
  // }
  //
  // public Query transformQuery(Query sourceQuery,
  // Object parameter)
  // {
  // return transformQuery(sourceQuery, NumberUtil.toInt(parameter, 0));
  // }
  // }
  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openWidgetDiv(buf, HtmlUtilsTheme.CSS_W_VALUE, namePrefix, __referenceLinkName);
    for (Iterator<PersistentRecord> i =
        getTargetRecords(record, viewpoint).getRecordList().iterator(); i.hasNext();) {
      PersistentRecord targetRecord = i.next();
      buf.append(targetRecord.getTitle());
      if (i.hasNext()) {
        buf.append(", ");
      }
    }
    HtmlUtilsTheme.closeDiv(buf);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtilsTheme.activeIndicator(buf, namePrefix, __referenceLinkName);
    HtmlUtils.openSelect(buf, true, false, false, namePrefix, __referenceLinkName);
    IdList targetRecordIds = getTargetRecords(record, viewpoint).getIdList();
    for (PersistentRecord targetRecord : __referenceTable) {
      HtmlUtils.option(buf,
                       Integer.toString(targetRecord.getId()),
                       targetRecord.getTitle(),
                       targetRecordIds.contains(targetRecord.getId()));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  public boolean hasHtmlWidget(TransientRecord record)
  {
    return true;
  }

  @Override
  public String getHtmlLabel()
  {
    return getName();
  }

  /**
   * @return false
   */
  @Override
  public boolean shouldReloadOnChange()
  {
    return false;
  }

  @Override
  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf, __label, false, namePrefix, getName());
    appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    HtmlUtilsTheme.closeDiv(buf);
  }
}
