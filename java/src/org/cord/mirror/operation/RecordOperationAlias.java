package org.cord.mirror.operation;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

/**
 * RecordOperation that acts as a bridge between an existing RecordOperation and a new name for the
 * operation. This is intended primarily as a compatability layer for providing backwards
 * compatability with refactored methods.
 */
public class RecordOperationAlias<V>
  extends SimpleRecordOperation<V>
{
  private final RecordOperationKey<V> __aliasedKey;

  /**
   * @param key
   *          The name that this RecordOperation is registered as
   * @param aliasedKey
   *          The name of the RecordOperation that will be invoked when this RecordOperation is
   *          triggered.
   */
  public RecordOperationAlias(RecordOperationKey<V> key,
                              RecordOperationKey<V> aliasedKey)
  {
    super(key);
    if (key.equals(aliasedKey)) {
      throw new IllegalArgumentException(key + " and " + aliasedKey
                                         + " are identical which will cause a recursive loop");
    }
    __aliasedKey = aliasedKey;
  }

  @Override
  public V getOperation(TransientRecord targetRecord,
                        Viewpoint transaction)
  {
    return targetRecord.opt(__aliasedKey, transaction);
  }
}