package org.cord.mirror.operation;

import org.cord.mirror.RecordOperationKey;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

public class MonoRecordOperationKey<A, V>
  extends RecordOperationKey<MonoRecordOperation<A, V>.Invoker>
{
  public static final <A, V> MonoRecordOperationKey<A, V> create(String keyName,
                                                                 Class<A> classA,
                                                                 Class<V> classV)
  {
    return new MonoRecordOperationKey<A, V>(keyName, classA, classV);
  }

  private final Class<A> __classA;
  private final Class<V> __classV;

  public MonoRecordOperationKey(String keyName,
                                Class<A> classA,
                                Class<V> classV)
  {
    super(keyName,
          ObjectUtil.<MonoRecordOperation<A, V>
                    .Invoker> castClass(MonoRecordOperation.Invoker.class));
    __classA = Preconditions.checkNotNull(classA, "classA");
    __classV = Preconditions.checkNotNull(classV, "classV");
  }

  public final Class<A> getClassA()
  {
    return __classA;
  }

  public final Class<V> getClassV()
  {
    return __classV;
  }

  @Override
  public MonoRecordOperationKey<A, V> copy()
  {
    return new MonoRecordOperationKey<A, V>(getKeyName(), getClassA(), getClassV());
  }
}
