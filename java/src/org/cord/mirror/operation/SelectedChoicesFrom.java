package org.cord.mirror.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BitLinkField;
import org.cord.util.Casters;

/**
 * ParametricRecordOperation that permits Webmacro templates to list the range of choices in a
 * ChoiceFieldFilter field. Usage is as follows:- <blockquote> $record.listChoices.fieldName
 * </blockquote>
 */
public class SelectedChoicesFrom
  extends MonoRecordOperation<String, RecordSource>
{
  /**
   * "listChoices"
   */
  public final static MonoRecordOperationKey<String, RecordSource> NAME =
      MonoRecordOperationKey.create("selectedChoicesFrom", String.class, RecordSource.class);

  public final static int P0_FIELDNAME = 0;

  public SelectedChoicesFrom()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public RecordSource calculate(TransientRecord record,
                                Viewpoint viewpoint,
                                String bitLinkFieldName)
  {

    return ((BitLinkField) record.getFieldFilter(bitLinkFieldName)).getSetFlags(record, viewpoint);
  }
}
