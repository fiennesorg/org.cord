package org.cord.mirror.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;

public abstract class SimpleRecordOperation<V>
  extends AbstractLockableOperation
  implements RecordOperation<V>
{
  private final RecordOperationKey<V> __key;

  public SimpleRecordOperation(RecordOperationKey<V> key)
  {
    super(key.getKeyName());
    __key = key;
  }

  @Override
  public RecordOperationKey<V> getKey()
  {
    return __key;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String toString()
  {
    Table table = getTable();
    if (table == null) {
      return "<table>." + getName();
    }
    return table.getName() + "." + getName();
  }

}
