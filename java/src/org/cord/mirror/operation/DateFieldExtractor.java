package org.cord.mirror.operation;

import java.util.Calendar;
import java.util.Date;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

/**
 * RecordOperation that extracts a given field from a Date field registered on a given record.
 */
public class DateFieldExtractor
  extends SimpleRecordOperation<Integer>
{
  private final Calendar __calendar = Calendar.getInstance();

  private final FieldKey<Date> __dateFieldName;

  private final int __calendarField;

  public DateFieldExtractor(RecordOperationKey<Integer> name,
                            FieldKey<Date> dateFieldName,
                            int calendarField)
  {
    super(name);
    __dateFieldName = dateFieldName;
    __calendarField = calendarField;
  }

  public DateFieldExtractor(FieldKey<Date> dateFieldName,
                            int calendarField)
  {
    this(getDefaultKey(dateFieldName, calendarField),
         dateFieldName,
         calendarField);
  }

  public final static String FE_YEAR = "Year";

  public final static String FE_MONTH = "Month";

  public final static String FE_DAY = "Day";

  public final static String FE_HOUR = "Hour";

  public final static String FE_MINUTE = "Minute";

  public final static String FE_SECOND = "Second";

  public final static String FE_MILLISECOND = "Millisecond";

  private final static int[] FE_CALENDARFIELDS =
      { Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE,
          Calendar.SECOND, Calendar.MILLISECOND };

  public static RecordOperationKey<Integer> getDefaultKey(FieldKey<Date> dateFieldName,
                                                          int calendarField)
  {
    return RecordOperationKey.create(getDefaultName(dateFieldName, calendarField), Integer.class);
  }

  /**
   * Get the default name for the DateFieldExtractor given a known dateFieldName and calendarField.
   * This is constructed by appending one of the standard suffixes onto the dateFieldName.
   */
  public static String getDefaultName(FieldKey<Date> dateFieldName,
                                      int calendarField)
  {
    switch (calendarField) {
      case Calendar.YEAR:
        return dateFieldName + FE_YEAR;
      case Calendar.MONTH:
        return dateFieldName + FE_MONTH;
      case Calendar.DAY_OF_MONTH:
        return dateFieldName + FE_DAY;
      case Calendar.HOUR_OF_DAY:
        return dateFieldName + FE_HOUR;
      case Calendar.MINUTE:
        return dateFieldName + FE_MINUTE;
      case Calendar.SECOND:
        return dateFieldName + FE_SECOND;
      case Calendar.MILLISECOND:
        return dateFieldName + FE_MILLISECOND;
      default:
        return dateFieldName.toString() + calendarField;
    }
  }

  public static void register(Table table,
                              FieldKey<Date> dateFieldName,
                              int calendarField)
      throws MirrorTableLockedException
  {
    table.addRecordOperation(new DateFieldExtractor(getDefaultKey(dateFieldName, calendarField),
                                                    dateFieldName,
                                                    calendarField));
  }

  public static void registerSmaller(Table table,
                                     FieldKey<Date> dateFieldName,
                                     int largestCalendarField)
      throws MirrorTableLockedException
  {
    boolean validTarget = false;
    for (int i = 0; i < FE_CALENDARFIELDS.length; i++) {
      if (FE_CALENDARFIELDS[i] == largestCalendarField) {
        validTarget = true;
      }
      if (validTarget) {
        register(table, dateFieldName, FE_CALENDARFIELDS[i]);
      }
    }
  }

  public static void registerLarger(Table table,
                                    FieldKey<Date> dateFieldName,
                                    int smallestCalendarField)
      throws MirrorTableLockedException
  {
    boolean validTarget = true;
    for (int i = 0; i < FE_CALENDARFIELDS.length; i++) {
      if (FE_CALENDARFIELDS[i] == smallestCalendarField) {
        validTarget = false;
      }
      if (validTarget) {
        register(table, dateFieldName, FE_CALENDARFIELDS[i]);
      }
    }
  }

  @Override
  public Integer getOperation(TransientRecord targetRecord,
                              Viewpoint viewpoint)
  {
    synchronized (__calendar) {
      __calendar.setTime(targetRecord.opt(__dateFieldName));
      return Integer.valueOf(__calendar.get(__calendarField));
    }
  }
}
