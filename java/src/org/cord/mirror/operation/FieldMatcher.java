package org.cord.mirror.operation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.cord.mirror.FieldKey;
import org.cord.mirror.Table;
import org.cord.sql.ConnectionFacade;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public abstract class FieldMatcher<T>
{
  private final Table __table;
  private final FieldKey<T> __fieldKey;
  private final String __preparedSQL;

  public FieldMatcher(Table table,
                      FieldKey<T> fieldKey)
  {
    __table = Preconditions.checkNotNull(table);
    __fieldKey = Preconditions.checkNotNull(fieldKey);
    __preparedSQL = "select id from " + table + " where " + __fieldKey + "=?";
  }

  public final Table getTable()
  {
    return __table;
  }

  public final FieldKey<T> getFieldKey()
  {
    return __fieldKey;
  }

  protected abstract void setValue(PreparedStatement preparedStatement,
                                   T value)
      throws SQLException;

  public final Optional<Integer> optId(T value)
  {
    if (value != null) {
      ConnectionFacade connectionFacade = __table.getDb().getThreadConnectionFacade().get();
      Connection connection = null;
      PreparedStatement preparedStatement = null;
      ResultSet resultSet = null;
      try {
        connection = connectionFacade.checkOutConnection();
        preparedStatement = connection.prepareStatement(__preparedSQL);
        setValue(preparedStatement, value);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
          return Optional.of(Integer.valueOf(resultSet.getInt(1)));
        }
      } catch (Exception e) {
      } finally {
        if (resultSet != null) {
          try {
            resultSet.close();
          } catch (SQLException e) {
          }
        }
        if (preparedStatement != null) {
          try {
            preparedStatement.close();
          } catch (SQLException e) {
          }
        }
        connectionFacade.checkInWorkingConnection(connection);
      }
    }
    return Optional.absent();
  }

}
