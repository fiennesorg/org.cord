package org.cord.mirror.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;

public class ExpandField
  extends MonoRecordOperation<String, String>
{
  /**
   * "expand"
   */
  public final static MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("expand", String.class, String.class);

  public ExpandField()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String fieldName)
  {
    return record.getFieldFilter(fieldName).expand(record);
  }
}
