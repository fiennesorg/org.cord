package org.cord.mirror.operation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.ThreadLocalSimpleDateFormat;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * ParametricRecordOperation that permits the user to filter either a RecordOperation that returns a
 * {@link Date} or {@link DateTime} object, or a raw Date, DateTime or Number (treated as a ms long
 * timestamp) into a specified format. This can either be invoked as: <blockquote>
 * 
 * <pre>
 * $record.dateFormatterName.dateOperationName
 * $record.dateFormatterName.get($dateOperationName)
 * $record.dateFormatterName.get($date)
 * $record.dateFormatterName.get($dateTime)
 * $record.dateFormatterName.get($number)
 * </pre>
 * 
 * </blockquote>
 * 
 * @see java.text.SimpleDateFormat
 */
public class DateFormatter
  extends MonoRecordOperation<Object, String>
{
  public static MonoRecordOperationKey<Object, String> createKey(String keyName)
  {
    return MonoRecordOperationKey.create(keyName, Object.class, String.class);
  }

  private final ThreadLocalSimpleDateFormat __simpleDateFormat;
  private final DateTimeFormatter __dateTimeFormat;
  private final boolean __hasNth;
  private final String __dateFormat;

  public static final int P0_DATEOPERATIONNAME = 0;

  public DateFormatter(MonoRecordOperationKey<Object, String> name,
                       String dateFormat)
  {
    super(name,
          Casters.NULL);
    __dateFormat = Preconditions.checkNotNull(dateFormat, "dateFormat for %s", name);
    try {
      __simpleDateFormat = new ThreadLocalSimpleDateFormat(dateFormat);
    } catch (IllegalArgumentException iaEx) {
      throw new IllegalArgumentException(String.format("%s is an invalid SimpleDateFormat for %s",
                                                       dateFormat,
                                                       name),
                                         iaEx);
    }
    try {
      __dateTimeFormat = DateTimeFormat.forPattern(dateFormat);
    } catch (IllegalArgumentException iaEx) {
      throw new IllegalArgumentException(String.format("%s is an invalid DateTimeFormat for %s",
                                                       dateFormat,
                                                       name),
                                         iaEx);
    }
    __hasNth = dateFormat.indexOf(DATE_POSTFIX) != -1;
  }

  @Override
  public String toString()
  {
    return "DateFormatter(" + __dateFormat + ")";
  }

  public static final String DATE_POSTFIX = "XX";

  public static final List<String> DATE_POSTFIXES;

  static {
    String[] p = new String[32];
    for (int i = 0; i <= 31; i++) {
      switch (i) {
        case 1:
        case 21:
        case 31:
          p[i] = "st";
          break;
        case 2:
        case 22:
          p[i] = "nd";
          break;
        case 3:
        case 23:
          p[i] = "rd";
          break;
        default:
          p[i] = "th";
      }
    }
    DATE_POSTFIXES = ImmutableList.copyOf(p);
  }

  /**
   * Apply the internal SimpleDateFormat to the given Date in a thread-safe manner.
   * 
   * @see SimpleDateFormat#format(Date)
   */
  public String format(Date date)
  {
    SimpleDateFormat simpleDateFormat = __simpleDateFormat.get();
    String result = simpleDateFormat.format(date);
    if (__hasNth) {
      result = result.replace(DATE_POSTFIX,
                              DATE_POSTFIXES.get(simpleDateFormat.getCalendar()
                                                                 .get(Calendar.DAY_OF_MONTH)));
    }
    return result;
  }

  public String format(DateTime dateTime)
  {
    String result = __dateTimeFormat.print(dateTime);
    if (__hasNth) {
      result = result.replace(DATE_POSTFIX, DATE_POSTFIXES.get(dateTime.getDayOfMonth()));
    }
    return result;
  }

  /**
   * Utilise the internal SimpleDateFormat to parse the given source into a Date in a thread-safe
   * manner.
   * 
   * @see SimpleDateFormat#parse(String)
   */
  public Date parse(String source)
      throws ParseException
  {
    return __simpleDateFormat.get().parse(source);
  }

  /**
   * Utility method that resolves the given named DateFormatter and then invokes it on the given
   * operationName.
   */
  public static String format(final TransientRecord record,
                              final String dateFormatName,
                              final String operationName)
  {
    final RecordOperation<?> recordOperation = record.getRecordOperation(dateFormatName);
    if (!(recordOperation instanceof DateFormatter)) {
      throw new IllegalArgumentException(String.format("%s.%s resolves to %s which is not a DateFormatter",
                                                       record,
                                                       dateFormatName,
                                                       recordOperation));
    }
    final DateFormatter dateFormatter = (DateFormatter) recordOperation;
    return dateFormatter.format(record, operationName);
  }

  /**
   * @throws IllegalArgumentException
   *           If the value of record.get(operationName) doesn't resolve to a Date object.
   */
  public String format(TransientRecord record,
                       String operationName)
  {
    Object object = record.get(operationName);
    return format(object);
  }

  private String format(Object object)
  {
    if (object == null) {
      return "";
    }
    if (object instanceof DateTime) {
      return format((DateTime) object);
    }
    if (object instanceof Date) {
      return format((Date) object);
    }
    if (object instanceof Optional) {
      Optional<?> optional = (Optional<?>) object;
      if (optional.isPresent()) {
        return format(optional.get());
      } else {
        return "";
      }
    }
    throw new IllegalArgumentException(String.format("DateFormatter cannot format %s of class %s into a String",
                                                     object,
                                                     object.getClass()));
  }
  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          Object param)
  {
    if (param instanceof String) {
      return format(record, (String) param);
    }
    if (param instanceof Date) {
      return format((Date) param);
    }
    if (param instanceof DateTime) {
      return format((DateTime) param);
    }
    if (param instanceof Number) {
      return format(new DateTime(((Number) param).longValue()));
    }
    return format(record, param.toString());
  }

}
