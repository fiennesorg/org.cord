package org.cord.mirror.operation;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.cord.mirror.FieldKey;
import org.cord.mirror.Table;

public class StringFieldMatcher
  extends FieldMatcher<String>
{

  public StringFieldMatcher(Table table,
                            FieldKey<String> fieldKey)
  {
    super(table,
          fieldKey);
  }

  @Override
  protected void setValue(PreparedStatement preparedStatement,
                          String value)
      throws SQLException
  {
    preparedStatement.setString(1, value);
  }

}
