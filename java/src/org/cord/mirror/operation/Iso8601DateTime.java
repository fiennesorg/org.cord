package org.cord.mirror.operation;

import java.util.Date;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class Iso8601DateTime
  extends MonoRecordOperation<Object, String>
{
  public static final MonoRecordOperationKey<Object, String> NAME =
      MonoRecordOperationKey.create("ISO8601_dateTime", Object.class, String.class);

  private static final DateTimeFormatter __formatter = ISODateTimeFormat.dateTimeNoMillis();

  public Iso8601DateTime()
  {
    super(NAME,
          Casters.NULL);
  }

  public String format(Date time)
  {
    return __formatter.print(time.getTime());
  }

  public String format(ReadableInstant time)
  {
    return __formatter.print(time);
  }

  public String format(Object o)
  {
    if (o instanceof Date) {
      return format((Date) o);
    }
    if (o instanceof ReadableInstant) {
      return format((ReadableInstant) o);
    }
    throw new IllegalArgumentException(String.format("Cannot print %s as ISO 8601", o));
  }

  public String format(TransientRecord record,
                       String operationName)
  {
    return format(record.get(operationName));
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          Object p)
  {
    if (p instanceof Date) {
      return format((Date) p);
    }
    return format(record, p.toString());
  }

}
