package org.cord.mirror.operation;

import java.util.Arrays;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Assertions;
import org.cord.util.ParamsAccumulator;
import org.cord.util.ParamsAccumulator.Params;
import org.cord.util.ParamsAccumulator.ParamsTail;

/**
 * Basic implementation of RecordOperation that means that sub-classes only have to implement a
 * single method to comply with the RecordOperation interface.
 */
public abstract class ParametricRecordOperation<V>
  extends AbstractLockableOperation
  implements RecordOperation<Params<V>>
{
  private final int __paramCount;

  /**
   * Initialise a new AbstractRecordOperation
   * 
   * @param name
   *          The name by which the RecordOperation will be invoked.
   * @param paramCount
   *          the number of parameters that this RecordOperation will gather before it invokes the
   *          logic. Must be at least 1.
   */
  public ParametricRecordOperation(String name,
                                   int paramCount)
  {
    super(name);
    Assertions.isTrue(paramCount > 0, "paramCount must be > 0");
    switch (paramCount) {
      case 1:
        System.err.println(this + " should be re-implemented as a MonoRecordOperation");
        break;
      case 2:
        System.err.println(this + " should be re-implemented as a DuoRecordOperation");
        break;
      case 3:
        System.err.println(this + " should be re-implemented as a TrioRecordOperation");
        break;
    }
    __paramCount = paramCount;
  }

  @Override
  public void shutdown()
  {
  }

  /**
   * @see Transaction#stripNullTransaction(Viewpoint)
   */
  @Override
  public final Params<V> getOperation(final TransientRecord targetRecord,
                                      final Viewpoint viewpoint)
  {
    return new ParamsTail<V>(__paramCount, new ParamsAccumulator.Invoker<V>() {
      @Override
      public V invoke(Object[] params)
      {
        return internalGetOperation(targetRecord, viewpoint, params);
      }

      @Override
      public String toString()
      {
        return "ParamsAccumulator.Invoker(" + ParametricRecordOperation.this + ")";
      }
    });
  }

  /**
   * Get the number of params that this RecordOperation expects to receive to be able to invoke its
   * functionality.
   * 
   * @return The required number of params. zero or more.
   */
  public final int getParamCount()
  {
    return __paramCount;
  }

  /**
   * Check that the number of params passed is equal to the number of params that this object is
   * expecting and call internalGetOperation. This is intended as a way of invoking the
   * functionality without having to jump through the webmacro-enforced ParamsAccumulator hoops and
   * is more effecient for direct java invocations.
   * 
   * @param record
   *          The record that the operation is to be invoked on.
   * @param viewpoint
   *          The viewpoint to use when invoking the operation
   * @param params
   *          The parameters to apply to the operation. There should be paramCount of these.
   * @return The result of invoking the operation.
   * @see #internalGetOperation(TransientRecord, Viewpoint, Object[])
   * @throws IllegalArgumentException
   *           if there are the wrong number of params.
   */
  public final V invoke(final TransientRecord record,
                        final Viewpoint viewpoint,
                        final Object... params)
  {
    if (params.length != __paramCount) {
      throw new IllegalArgumentException(String.format("%s expects %s params but received %s",
                                                       this,
                                                       Integer.valueOf(__paramCount),
                                                       Arrays.toString(params)));
    }
    return internalGetOperation(record, viewpoint, params);
  }

  /**
   * Get the value of the record operation on the specified targetRecord. The instances of
   * TRANSACTION_NULL Transactions will have been automatically filtered out and therefore and
   * replaced by null.
   * 
   * @param targetRecord
   *          The record to perform the operation on.
   * @param viewpoint
   *          A Transaction that is not of type TRANSACTION_NULL or null that will be used to filter
   *          the view onto the data.
   * @return The result of the operation
   */
  protected abstract V internalGetOperation(TransientRecord targetRecord,
                                            Viewpoint viewpoint,
                                            Object... params);
}
