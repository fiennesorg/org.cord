package org.cord.mirror.operation;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;

/**
 * HtmlGuiSupplier that presents a file upload widget.
 * 
 * @author alex
 */
public class FileFormElement
  extends SimpleRecordOperation<Object>
  implements HtmlGuiSupplier
{
  private final String __htmlLabel;

  public FileFormElement(RecordOperationKey<Object> name,
                         String htmlLabel)
  {
    super(name);
    __htmlLabel = htmlLabel;
  }

  /**
   * Return a String version of this FileFormElement. Classes that actually want to return the asset
   * that has been stored by previous uploads with this Object should override this method.
   * 
   * @return the toString() method
   */
  @Override
  public Object getOperation(TransientRecord targetRecord,
                             Viewpoint viewpoint)
  {
    return toString();
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.value(buf, "FileWidget", namePrefix, getName());
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.fileWidget(buf, namePrefix, getName());
  }

  @Override
  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf, getHtmlLabel(), false, namePrefix, getName());
    HtmlUtils.fileWidget(buf, namePrefix, getName());
    HtmlUtilsTheme.closeDiv(buf);
  }

  @Override
  public boolean hasHtmlWidget(TransientRecord record)
  {
    return true;
  }

  @Override
  public String getHtmlLabel()
  {
    return __htmlLabel;
  }

  @Override
  public boolean shouldReloadOnChange()
  {
    return false;
  }

  @Override
  public String toString()
  {
    return "FileFormElement(" + getName() + ")";
  }
}
