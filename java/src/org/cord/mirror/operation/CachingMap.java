package org.cord.mirror.operation;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.FilteredCachingSimpleRecordOperation;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableListener;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.TimestampedConcurrentMap;

/**
 * A CachingMap is a RecordOperation that maps to a Map that contains values which get invalidated
 * when the data changes in a configurable list of Tables. To use it, you give it the list of Tables
 * which should cause it to invalidate its data and then lock it to make it thread safe. Once this
 * is done then every time that you get it from the record then it will be automatically cleared if
 * the appropriate Tables have been updated since it was last cleared. Every time that the Map is
 * cleared then populateEmptyMap is invoked so it is possible to prepopulate it with the appropriate
 * values if this makes sense for the application. If you want more elegant ways of resolving the
 * values in the Map than exposing it directly then you should look at the CachingMapResolver
 * 
 * @author alex
 * @param <K>
 *          The class of Keys that the map stores
 * @param <V>
 *          The class of Values that the map stores
 * @see org.cord.mirror.operation.CachingMapResolver
 * @see #populateEmptyMap(Map, TransientRecord, Viewpoint)
 * @see #addTableChangedTable(Table)
 */
public abstract class CachingMap<K, V>
  extends
  FilteredCachingSimpleRecordOperation<TimestampedConcurrentMap<K, V>, TimestampedConcurrentMap<K, V>>
  implements TableListener
{
  private long _tableChangedTimestamp = 0;

  private final Set<Table> __tableChangedTables = new HashSet<Table>();

  private final Integer __tableChangedType;

  private volatile boolean _isLocked = false;

  private final int __initialCapacity;
  private final float __loadFactor;
  private final int __concurrencyLevel;

  /**
   * @param tableChangedType
   *          if not null, then the tableChanged request will only reset the timestamp if the
   *          invoking Transaction type is set to tableChangedType.
   */
  public CachingMap(RecordOperationKey<TimestampedConcurrentMap<K, V>> name,
                    Integer tableChangedType,
                    int initialCapacity,
                    float loadFactor,
                    int concurrencyLevel)
  {
    super(name,
          name);
    __tableChangedType = tableChangedType;
    __initialCapacity = initialCapacity;
    __loadFactor = loadFactor;
    __concurrencyLevel = concurrencyLevel;
  }

  /**
   * @return true if the table was added to the list, false if it was already there.
   * @throws IllegalStateException
   *           if you try and add a table changed table after this item has been locked.
   */
  public boolean addTableChangedTable(Table table)
  {
    if (_isLocked) {
      throw new IllegalStateException(String.format("Cannot add %s to %s after it has been locked",
                                                    table,
                                                    this));
    }
    if (table == null) {
      throw new NullPointerException("Cannot add a null table in addTableChangedTable");
    }
    table.addTableListener(this);
    return __tableChangedTables.add(table);
  }

  /**
   * Pre-populate the map that has either just been created or has just been cleared. Just provide
   * an empty implementation if prepopulation is not required.
   */
  protected abstract void populateEmptyMap(Map<K, V> map,
                                           TransientRecord record,
                                           Viewpoint viewpoint);

  /**
   * @return a TimestampedConcurrentMap that will have been automatically prepopulated as necessary.
   */
  @Override
  public final synchronized TimestampedConcurrentMap<K, V> calculate(TransientRecord record,
                                                                     Viewpoint viewpoint)
  {
    TimestampedConcurrentMap<K, V> map =
        new TimestampedConcurrentMap<K, V>(__initialCapacity, __loadFactor, __concurrencyLevel);
    populateEmptyMap(map, record, viewpoint);
    return map;
  }

  /**
   * @return a TimestampedConcurrentMap that will have been automatically cleared (and then
   *         prepopulated) if any of the tables have been updated since it was last requested.
   */
  @Override
  public final TimestampedConcurrentMap<K, V> filter(TransientRecord record,
                                                     Viewpoint viewpoint,
                                                     final TimestampedConcurrentMap<K, V> map)
  {
    if (map.getTimestamp() < _tableChangedTimestamp) {
      map.clear();
      populateEmptyMap(map, record, viewpoint);
      map.setTimestamp();
    }
    return map;
  }

  /**
   * If table is on the list of tableChangedTables and if type matches the tableChangedType then
   * cause any cached maps that are retrieved from this CachingMap to be automatically cleared if
   * their timestamp was before now.
   * 
   * @return true
   */
  @Override
  public final boolean tableChanged(Table table,
                                    int recordId,
                                    PersistentRecord persistentRecord,
                                    int type)
  {
    if (__tableChangedTables.contains(table)
        && (__tableChangedType == null || __tableChangedType.intValue() == type)) {
      _tableChangedTimestamp = System.currentTimeMillis();
    }
    return true;
  }
}
