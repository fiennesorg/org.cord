package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BitLinkField;
import org.cord.util.Casters;
import org.cord.util.ObjectUtil;

/**
 * ParametricRecordOperation that returns all the possible choices from a given ChoiceFieldFilter as
 * its parameter.
 */
public class AllChoicesFrom
  extends MonoRecordOperation<String, Iterator<PersistentRecord>>
{
  /**
   * "allChoicesFrom"
   */
  public final static MonoRecordOperationKey<String, Iterator<PersistentRecord>> NAME =
      MonoRecordOperationKey.create("allChoicesFrom",
                                    String.class,
                                    ObjectUtil.<Iterator<PersistentRecord>> castClass(Iterator.class));

  public AllChoicesFrom()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  public static Iterator<PersistentRecord> getAllChoicesFrom(BitLinkField field,
                                                             Viewpoint viewpoint)
  {
    return field.getTargetTable().getRecordList(viewpoint).iterator();
  }

  @Override
  public Iterator<PersistentRecord> calculate(TransientRecord record,
                                              Viewpoint viewpoint,
                                              String fieldName)
  {
    BitLinkField bitLinkFieldFilter = (BitLinkField) record.getFieldFilter(fieldName);
    return getAllChoicesFrom(bitLinkFieldFilter, viewpoint);
  }
}
