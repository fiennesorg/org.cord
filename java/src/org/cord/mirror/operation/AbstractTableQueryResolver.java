package org.cord.mirror.operation;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;

/**
 * Base class for TableOperation implementations that are going to return an optionally queryName
 * cached QueryWrapper
 * 
 * @author alex
 */
public abstract class AbstractTableQueryResolver
  extends SimpleRecordSourceOperation<RecordSource>
{
  public AbstractTableQueryResolver(RecordSourceOperationKey<RecordSource> key)
  {
    super(key);
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    Table invokingTable = recordSource.getTable();
    String queryName = invokingTable.getName() + "." + getName();
    RecordSource result = null;
    if (permitsQueryNamedCaching(invokingTable)) {
      result = invokingTable.getCachedRecordSource(queryName);
    }
    if (result == null) {
      result = createQuery(invokingTable, queryName);
    }
    return RecordSources.viewedFrom(result, viewpoint);
  }

  public boolean permitsQueryNamedCaching(Table invokingTable)
  {
    return true;
  }

  protected abstract Query createQuery(Table invokingTable,
                                       String queryName);
}
