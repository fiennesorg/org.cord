package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BitLinkField;

import com.google.common.collect.ImmutableSet;

public class ForwardBitLinkRecordOperation
  extends AbstractForwardRecordOperation<Long, RecordSource>
{
  public ForwardBitLinkRecordOperation(Table targetTable,
                                       FieldKey<Long> linkFieldName,
                                       RecordOperationKey<RecordSource> linkOperationName,
                                       int linkStyle)
  {
    super(targetTable,
          linkFieldName,
          linkOperationName,
          linkStyle);
  }

  public RecordSource getTargetRecords(TransientRecord linkRecord,
                                       Viewpoint viewpoint)
  {
    return BitLinkField.getSetFlags(linkRecord, getLinkFieldName(), getTargetTable(), viewpoint);
  }

  @Override
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord linkRecord,
                                                        int transactionType,
                                                        Viewpoint viewpoint)
  {
    if (mayHaveDependencies(linkRecord, transactionType)) {
      return getTargetRecords(linkRecord, viewpoint).iterator();
    }
    return ImmutableSet.<PersistentRecord> of().iterator();
  }

  @Override
  public RecordSource getOperation(TransientRecord linkRecord,
                                   Viewpoint viewpoint)
  {
    return getTargetRecords(linkRecord, viewpoint);
  }
}
