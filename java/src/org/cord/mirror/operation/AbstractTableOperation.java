package org.cord.mirror.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.operation.AbstractRecordSourceOperation;

/**
 * Generic base class of TableOperation to use as a starting point for new classes.
 * 
 * @deprecated as this is just a holding class and things should directly subclas the appropriate
 *             base class of RecordSourceOperation implementation.
 */
@Deprecated
public abstract class AbstractTableOperation
  extends AbstractRecordSourceOperation
{
  /**
   * Initialise a new AbstractRecordOperation
   * 
   * @param name
   *          The name by which the RecordOperation will be invoked.
   */
  public AbstractTableOperation(String name,
                                int paramCount)
  {
    super(name,
          paramCount);
  }

  protected abstract Object internalGetOperation(Table invokingTable,
                                                 Viewpoint viewpoint,
                                                 Object... params);

  @Override
  protected final Object invokeWithParams(RecordSource recordSource,
                                          Viewpoint viewpoint,
                                          Object... params)
  {
    return internalGetOperation(recordSource.getTable(), viewpoint, params);
  }
}
