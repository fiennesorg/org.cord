package org.cord.mirror.operation;

import java.sql.PreparedStatement;
import java.util.Iterator;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceResolver;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.PostTransactionDeleteTrigger;
import org.cord.util.Casters;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class ManyToManyRecursiveReference
{
  public final static int LINK_SOURCE_TO_TARGET = 0;

  public final static int LINK_TARGET_TO_SOURCE = 1;

  public final static int LINK_BIDIRECTIONAL = 2;

  public final static IntFieldKey DEFAULT_SOURCELINKNAME = IntFieldKey.create("source_id");

  public final static RecordOperationKey<PersistentRecord> DEFAULT_SOURCENAME =
      LinkField.getLinkName(DEFAULT_SOURCELINKNAME);

  public final static RecordOperationKey<RecordSource> DEFAULT_SOURCEREFERENCENAME =
      RecordOperationKey.create("sourceLinks", RecordSource.class);

  public final static IntFieldKey DEFAULT_TARGETLINKNAME = IntFieldKey.create("target_id");

  public final static RecordOperationKey<PersistentRecord> DEFAULT_TARGETNAME =
      LinkField.getLinkName(DEFAULT_TARGETLINKNAME);

  public final static RecordOperationKey<RecordSource> DEFAULT_TARGETREFERENCENAME =
      RecordOperationKey.create("targetLinks", RecordSource.class);

  public final static ObjectFieldKey<Boolean> DEFAULT_ISDEFINEDNAME =
      BooleanField.createKey("isDefined");

  /**
   * @param linkTable
   *          The Table that represents the links between the records.
   * @param sourceLinkName
   *          The field on linkTable that holds the ids of the source component of the link. Define
   *          this if you want links which have matchingRecord as a source returned.
   * @param targetLinkName
   *          The field on linkTable that holds the ids of the target component of the link. Define
   *          this is you want links which have matchingRecord as a target returned.
   * @param matchingRecord
   *          The record which will be pointed at one or more of sourceLinkName and targetLinkName.
   * @param order
   *          The ordering to apply to the returned records. Utilise null if default ordering
   *          required.
   * @return Query of records from linkTable that matches the above criteria.
   */
  private static Query getLinkRecordsQuery(Table linkTable,
                                           IntFieldKey sourceLinkName,
                                           IntFieldKey targetLinkName,
                                           TransientRecord matchingRecord,
                                           String order)
  {
    if (sourceLinkName == null && targetLinkName == null) {
      throw new IllegalArgumentException("sourceLinkName & targetLinkName are both null");
    }
    StringBuilder filter = new StringBuilder();
    if (sourceLinkName != null) {
      filter.append('(')
            .append(sourceLinkName)
            .append('=')
            .append(matchingRecord.getId())
            .append(')');
    }
    if (targetLinkName != null) {
      if (sourceLinkName != null) {
        filter.append(" or ");
      }
      filter.append('(')
            .append(targetLinkName)
            .append('=')
            .append(matchingRecord.getId())
            .append(')');
    }
    return linkTable.getQuery(null, filter.toString(), order);
  }

  public static void clearIsDefined(Table linkTable,
                                    IntFieldKey sourceLinkName,
                                    FieldKey<Boolean> isDefinedName,
                                    TransientRecord sourceRecord,
                                    Transaction updateTransaction,
                                    long timeout)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    isDefinedName = MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME);
    Query linkQuery = getLinkRecordsQuery(linkTable, sourceLinkName, null, sourceRecord, null);
    clearIsDefined(linkTable, isDefinedName, sourceRecord, linkQuery, updateTransaction, timeout);
  }

  public static void clearIsDefined(Table linkTable,
                                    TransientRecord sourceRecord,
                                    Transaction updateTransaction,
                                    long timeout)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    clearIsDefined(linkTable, sourceRecord, updateTransaction, timeout);
  }

  private static void clearIsDefined(Table linkTable,
                                     FieldKey<Boolean> isDefinedName,
                                     TransientRecord sourceRecord,
                                     Query linkQuery,
                                     Transaction updateTransaction,
                                     long timeout)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    updateTransaction.edit(linkQuery, timeout);
    for (PersistentRecord linkRecord : linkQuery.getRecordList(updateTransaction)) {
      linkRecord.setField(isDefinedName, Boolean.FALSE);
    }
  }

  public static void setIsDefined(Table linkTable,
                                  IntFieldKey sourceLinkName,
                                  IntFieldKey targetLinkName,
                                  FieldKey<Boolean> isDefinedName,
                                  TransientRecord sourceRecord,
                                  TransientRecord targetRecord,
                                  Transaction updateTransaction,
                                  long timeout,
                                  PreparedStatement statement)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    PersistentRecord linkRecord = createLinkRecord(linkTable,
                                                   sourceLinkName,
                                                   targetLinkName,
                                                   sourceRecord,
                                                   targetRecord,
                                                   statement);
    linkRecord = updateTransaction.edit(linkRecord, timeout);
    isDefinedName = MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME);
    linkRecord.setField(isDefinedName, Boolean.TRUE);
  }

  public static void setIsDefined(Table linkTable,
                                  TransientRecord sourceRecord,
                                  TransientRecord targetRecord,
                                  Transaction updateTransaction,
                                  long timeout,
                                  PreparedStatement statement)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException
  {
    setIsDefined(linkTable,
                 null,
                 null,
                 null,
                 sourceRecord,
                 targetRecord,
                 updateTransaction,
                 timeout,
                 statement);
  }

  public static int setIsDefined(Table linkTable,
                                 IntFieldKey sourceLinkName,
                                 IntFieldKey targetLinkName,
                                 FieldKey<Boolean> isDefinedName,
                                 TransientRecord sourceRecord,
                                 Iterator<?> targetIds,
                                 boolean ignoreBadIds,
                                 Transaction updateTransaction,
                                 long timeout,
                                 PreparedStatement statement)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    isDefinedName = MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME);
    Table sourceTable = sourceRecord.getTable();
    Query linkQuery = getLinkedRecordsQuery(linkTable,
                                            sourceLinkName,
                                            targetLinkName,
                                            sourceRecord,
                                            LINK_SOURCE_TO_TARGET,
                                            isDefinedName);
    synchronized (linkQuery) {
      clearIsDefined(linkTable,
                     sourceLinkName,
                     isDefinedName,
                     sourceRecord,
                     updateTransaction,
                     timeout);
      int count = 0;
      while (targetIds.hasNext()) {
        try {
          Integer targetId = Integer.valueOf(targetIds.next().toString());
          PersistentRecord targetRecord = sourceTable.getRecord(targetId, updateTransaction);
          setIsDefined(linkTable,
                       sourceLinkName,
                       targetLinkName,
                       isDefinedName,
                       sourceRecord,
                       targetRecord,
                       updateTransaction,
                       timeout,
                       statement);
          count++;
        } catch (MirrorNoSuchRecordException badIdEx) {
          if (!ignoreBadIds) {
            throw badIdEx;
          }
        } catch (RuntimeException badIdEx) {
          if (!ignoreBadIds) {
            throw badIdEx;
          }
        }
      }
      return count;
    }
  }

  public static int setIsDefined(Table linkTable,
                                 TransientRecord sourceRecord,
                                 Iterator<Object> targetIds,
                                 boolean ignoreBadIds,
                                 Transaction updateTransaction,
                                 long timeout,
                                 PreparedStatement statement)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException
  {
    return setIsDefined(linkTable,
                        null,
                        null,
                        null,
                        sourceRecord,
                        targetIds,
                        ignoreBadIds,
                        updateTransaction,
                        timeout,
                        statement);
  }

  private static void appendLinkRecordFilterComponent(StringBuilder filter,
                                                      IntFieldKey sourceLinkName,
                                                      IntFieldKey targetLinkName,
                                                      TransientRecord sourceRecord,
                                                      TransientRecord targetRecord)
  {
    filter.append('(')
          .append(sourceLinkName)
          .append('=')
          .append(sourceRecord.getId())
          .append(" and ")
          .append(targetLinkName)
          .append('=')
          .append(targetRecord.getId())
          .append(')');
  }

  protected static Query getLinkRecordQuery(Table linkTable,
                                            IntFieldKey sourceLinkName,
                                            IntFieldKey targetLinkName,
                                            TransientRecord sourceRecord,
                                            TransientRecord targetRecord,
                                            int linkType)
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    StringBuilder filter = new StringBuilder();
    switch (linkType) {
      case LINK_SOURCE_TO_TARGET:
        appendLinkRecordFilterComponent(filter,
                                        sourceLinkName,
                                        targetLinkName,
                                        sourceRecord,
                                        targetRecord);
        break;
      case LINK_TARGET_TO_SOURCE:
        appendLinkRecordFilterComponent(filter,
                                        sourceLinkName,
                                        targetLinkName,
                                        targetRecord,
                                        sourceRecord);
        break;
      case LINK_BIDIRECTIONAL:
        filter.append('(');
        appendLinkRecordFilterComponent(filter,
                                        sourceLinkName,
                                        targetLinkName,
                                        sourceRecord,
                                        targetRecord);
        filter.append(" or ");
        appendLinkRecordFilterComponent(filter,
                                        sourceLinkName,
                                        targetLinkName,
                                        targetRecord,
                                        sourceRecord);
        filter.append(')');
        break;
      default:
        throw new IllegalArgumentException("Unknown link type:" + linkType);
    }
    return linkTable.getQuery(null, filter.toString(), null);
  }

  public static boolean getRelationship(Table linkTable,
                                        IntFieldKey sourceLinkName,
                                        IntFieldKey targetLinkName,
                                        TransientRecord sourceRecord,
                                        TransientRecord targetRecord,
                                        int linkType,
                                        FieldKey<Boolean> isDefinedName,
                                        Viewpoint viewpoint)
  {
    Preconditions.checkNotNull(sourceRecord, "sourceRecord");
    Preconditions.checkNotNull(targetRecord, "targetRecord");
    PersistentRecord linkRecord = RecordSources.getOptFirstRecord(
                                                                  getLinkRecordQuery(linkTable,
                                                                                     sourceLinkName,
                                                                                     targetLinkName,
                                                                                     sourceRecord,
                                                                                     targetRecord,
                                                                                     linkType),
                                                                  viewpoint);
    if (linkRecord == null) {
      return false;
    }
    return linkRecord.comp(MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME))
                     .booleanValue();
  }

  public static boolean getRelationship(Table linkTable,
                                        TransientRecord sourceRecord,
                                        TransientRecord targetRecord,
                                        int linkType,
                                        Viewpoint viewpoint)
  {
    return getRelationship(linkTable,
                           null,
                           null,
                           sourceRecord,
                           targetRecord,
                           linkType,
                           null,
                           viewpoint);
  }

  private static void appendLinkedRecordsFilterComponent(StringBuilder filter,
                                                         String linkTableName,
                                                         IntFieldKey sourceLinkName,
                                                         IntFieldKey targetLinkName,
                                                         String sourceTableName,
                                                         int sourceId)
  {
    filter.append("(")
          .append(linkTableName)
          .append('.')
          .append(sourceLinkName)
          .append('=')
          .append(sourceId)
          .append(" and ")
          .append(linkTableName)
          .append('.')
          .append(targetLinkName)
          .append('=')
          .append(sourceTableName)
          .append("." + TransientRecord.FIELD_ID + ')');
  }

  /**
   * @param linkTable
   *          The Table that holds the information joining the records in the sourceTable together.
   * @param sourceLinkName
   *          The name of the field on linkTable that points at the conceptual origination of the
   *          link in sourceTable..
   * @param targetLinkName
   *          The name of the field on linkTable that points at the conceptual target of the link in
   *          sourceTable.
   * @param sourceRecord
   *          The record to or from which we are finding out the references.
   */
  public static Query getLinkedRecordsQuery(Table linkTable,
                                            IntFieldKey sourceLinkName,
                                            IntFieldKey targetLinkName,
                                            TransientRecord sourceRecord,
                                            int linkType,
                                            FieldKey<Boolean> isDefinedName)
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    StringBuilder filter = new StringBuilder();
    Table sourceTable = sourceRecord.getTable();
    appendLinkedRecordsFilterComponents(linkTable,
                                        sourceLinkName,
                                        targetLinkName,
                                        sourceRecord,
                                        linkType,
                                        filter);
    filter.append(" and ")
          .append(linkTable.getName())
          .append('.')
          .append(MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME))
          .append("=1");
    return sourceTable.getQuery(null, filter.toString(), null, null, -1, null, linkTable);
  }

  private static void appendLinkedRecordsFilterComponents(Table linkTable,
                                                          IntFieldKey sourceLinkName,
                                                          IntFieldKey targetLinkName,
                                                          TransientRecord sourceRecord,
                                                          int linkType,
                                                          StringBuilder filter)
  {
    String sourceTableName = sourceRecord.getTable().getName();
    switch (linkType) {
      case LINK_SOURCE_TO_TARGET:
        appendLinkedRecordsFilterComponent(filter,
                                           linkTable.getName(),
                                           sourceLinkName,
                                           targetLinkName,
                                           sourceTableName,
                                           sourceRecord.getId());
        break;
      case LINK_TARGET_TO_SOURCE:
        appendLinkedRecordsFilterComponent(filter,
                                           linkTable.getName(),
                                           targetLinkName,
                                           sourceLinkName,
                                           sourceTableName,
                                           sourceRecord.getId());
        break;
      case LINK_BIDIRECTIONAL:
        filter.append('(');
        appendLinkedRecordsFilterComponent(filter,
                                           linkTable.getName(),
                                           sourceLinkName,
                                           targetLinkName,
                                           sourceTableName,
                                           sourceRecord.getId());
        filter.append(" or ");
        appendLinkedRecordsFilterComponent(filter,
                                           linkTable.getName(),
                                           targetLinkName,
                                           sourceLinkName,
                                           sourceTableName,
                                           sourceRecord.getId());
        filter.append(')');
        break;
      default:
        throw new IllegalArgumentException("Unknown linkType:" + linkType);
    }
  }

  public static Query getLinkedRecordsQuery(Table linkTable,
                                            TransientRecord sourceRecord,
                                            int linkType)
  {
    return getLinkedRecordsQuery(linkTable, null, null, sourceRecord, linkType, null);
  }

  private static PersistentRecord createLinkRecord(Table linkTable,
                                                   IntFieldKey sourceLinkName,
                                                   IntFieldKey targetLinkName,
                                                   int sourceId,
                                                   int targetId,
                                                   Query linkRecords,
                                                   PreparedStatement statement)
      throws MirrorFieldException, MirrorInstantiationException
  {
    synchronized (linkRecords) {
      PersistentRecord existingLinkRecord = RecordSources.getOptOnlyRecord(linkRecords, null);
      if (existingLinkRecord != null) {
        return existingLinkRecord;
      }
      TransientRecord newLinkRecord = linkTable.createTransientRecord();
      newLinkRecord.setField(sourceLinkName, Integer.valueOf(sourceId));
      newLinkRecord.setField(targetLinkName, Integer.valueOf(targetId));
      return newLinkRecord.makePersistent(null, statement);
    }
  }

  public static PersistentRecord createLinkRecord(Table linkTable,
                                                  IntFieldKey sourceLinkName,
                                                  IntFieldKey targetLinkName,
                                                  TransientRecord sourceRecord,
                                                  TransientRecord targetRecord,
                                                  PreparedStatement statement)
      throws MirrorFieldException, MirrorInstantiationException
  {
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    Query linkRecordQuery = getLinkRecordQuery(linkTable,
                                               sourceLinkName,
                                               targetLinkName,
                                               sourceRecord,
                                               targetRecord,
                                               LINK_SOURCE_TO_TARGET);
    return createLinkRecord(linkTable,
                            sourceLinkName,
                            targetLinkName,
                            sourceRecord.getId(),
                            targetRecord.getId(),
                            linkRecordQuery,
                            statement);
  }

  public static PersistentRecord createLinkRecord(Table linkTable,
                                                  TransientRecord sourceRecord,
                                                  TransientRecord targetRecord,
                                                  PreparedStatement statement)
      throws MirrorFieldException, MirrorInstantiationException
  {
    return createLinkRecord(linkTable, null, null, sourceRecord, targetRecord, statement);
  }

  /**
   * @param db
   *          The Db that this linkage is to be registered on.
   * @param transactionPassword
   *          The transaction password for the current db.
   * @param sourceTable
   *          The Table which is to be linked to itself.
   * @param linkTableName
   *          The name of the Table that is going to be used to store the linkages. If this Table
   *          doesn't exist then it will be automatically created (in java space)
   * @param sourceLinkName
   *          The name of the field on the linkTable that contains the reference to the source
   *          record in sourceTable. If this is empty or null then a value of
   *          <a href="#DEFAULT_SOURCELINKNAME">DEFAULT_SOURCELINKNAME</a> is utilised. The
   *          appropriate LinkFieldFilter will be added to linkTable during this method.
   * @param sourceName
   *          The name of the RecordOperation on the linkTable that resolves to the record pointed
   *          at by sourceLinkName. If empty or null then a default value of
   *          <a href="#DEFAULT_SOURCENAME">DEFAULT_SOURCENAME</a> is utilised.
   * @param sourceReferenceName
   *          The name of the RecordOperation on sourceTable that returns the list of linkTable
   *          records that have the sourceRecord as its source field. If this is empty or null then
   *          a value of <a href="#DEFAULT_SOURCEREFERENCENAME" >DEFAULT_SOURCEREFERENCENAME</a> is
   *          utilised.
   * @param lockOnSourceEdit
   *          If true then linkTable records that point at a given sourceTable record as its source
   *          will be locked on edit of the sourceTable record.
   * @param targetLinkName
   *          The name of the field on linkTable that contains the reference to the target record in
   *          sourceTable. If this is empty or null then a value of
   *          <a href="#DEFAULT_TARGETLINKNAME">DEFAULT_TARGETLINKNAME</a> will be utilised. The
   *          appropriate LinkFieldFilter will be added to linkTable during this method.
   * @param targetName
   *          The name of the RecordOperation on the linkTable that resolves to the sourceTable
   *          record pointed at by targetLinkName. If this is empty or null then a value of
   *          <a href="#DEFAULT_TARGETREFERENCENAME" >DEFAULT_TARGETREFERENCENAME</a> will be used.
   * @param targetReferenceName
   *          The name of the RecordOperation on the sourceTable that returns the list of linkTable
   *          records that have the sourceRecord as its target field. If this is empty or null then
   *          a value of <a href="#DEFAULT_TARGETREFERENCENAME" >DEFAULT_TARGETREFERENCENAME</a>
   *          will be utilised.
   * @param lockOnTargetEdit
   *          If true then the linkTable records that point at a given sourceTable record as its
   *          target will be locked on edit of the sourceTable record.
   * @param isDefinedName
   *          The fieldName for the BooleanFieldFilter that holds the defined status of a given
   *          linkTable record. The field will be added to the linkTable during this method.
   * @param isOptionalLinks
   *          If true then the sourceLinkName and targetLinkName fields on linkTable will be able to
   *          be not-defined. This would be an unusual situation and one would probably not utilise
   *          it as it is preferable that a linkTable always represents a relationship between two
   *          existing records. Deletion of a single sourceTable record would normally delete the
   *          linkTable records that referenced it.
   * @param sourceToTargetTargetsName
   *          The name of the RecordOperation that is to be registered on the sourceTable that
   *          returns a Query of sourceTable records that represent all the records that are
   *          targetted from the invoking record.
   * @param targetToSourceTargetsName
   *          The name of the RecordOperation that is to be registered on the sourceTable that
   *          returns a Query of sourceTable records that represent all the records that are
   *          targetting the invoking record.
   * @param allTargetsName
   *          The name of the RecordOperation that is to be registered on the sourceTable that
   *          returns a Query of sourceTable records that represent all the records that are
   *          targetting or being targetted by the invoking record. Note that this Query will
   *          automatically discard duplicate records caused by mirror definition of references.
   * @param sourceToTargetIncludesName
   *          The name of the ParametricRecordOperation that is to be registered on the sourceTable
   *          that enables a check of whether a given potential target sourceTable record is
   *          included in the current list of target records.
   * @param targetToSourceIncludesName
   *          The name of the ParametricRecordOperation that is to be registered on the sourceTable
   *          that enables a check of whether a given potential source sourceTable record is
   *          included in the current list of source records.
   * @param allIncludesName
   *          The name of the ParametricRecordOperation registered on sourceTable that enables a
   *          check of whether a given potential sourceTable record has a link in either diriection
   *          with the invoking record.
   * @return the linkTable that was already existing or that was specifically created and set up to
   *         hold the recursive reference information.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         Table sourceTable,
                                         String linkTableName,
                                         IntFieldKey sourceLinkName,
                                         RecordOperationKey<PersistentRecord> sourceName,
                                         RecordOperationKey<RecordSource> sourceReferenceName,
                                         boolean lockOnSourceEdit,
                                         IntFieldKey targetLinkName,
                                         RecordOperationKey<PersistentRecord> targetName,
                                         RecordOperationKey<RecordSource> targetReferenceName,
                                         boolean lockOnTargetEdit,
                                         ObjectFieldKey<Boolean> isDefinedName,
                                         boolean isOptionalLinks,
                                         RecordOperationKey<RecordSource> sourceToTargetTargetsName,
                                         RecordOperationKey<RecordSource> targetToSourceTargetsName,
                                         RecordOperationKey<RecordSource> allTargetsName,
                                         MonoRecordOperationKey<Object, Boolean> sourceToTargetIncludesName,
                                         MonoRecordOperationKey<Object, Boolean> targetToSourceIncludesName,
                                         MonoRecordOperationKey<Object, Boolean> allIncludesName)
      throws MirrorTableLockedException
  {
    Table linkTable = null;
    try {
      linkTable = db.getTable(linkTableName);
    } catch (RuntimeException nonInitialisedLinkTableEx) {
      linkTable = db.addSoftTable(null, linkTableName, null);
    }
    sourceLinkName = MoreObjects.firstNonNull(sourceLinkName, DEFAULT_SOURCELINKNAME);
    sourceName = MoreObjects.firstNonNull(sourceName, DEFAULT_SOURCENAME);
    sourceReferenceName =
        MoreObjects.firstNonNull(sourceReferenceName, DEFAULT_SOURCEREFERENCENAME);
    linkTable.addField(new LinkField(sourceLinkName,
                                     "From " + sourceTable.getName(),
                                     null,
                                     sourceName,
                                     sourceTable.getName(),
                                     sourceReferenceName,
                                     null,
                                     transactionPassword,
                                     Dependencies.LINK_ENFORCED | (lockOnSourceEdit
                                         ? Dependencies.BIT_S_ON_T_UPDATE
                                         : 0) | (isOptionalLinks ? Dependencies.BIT_OPTIONAL : 0)));
    targetLinkName = MoreObjects.firstNonNull(targetLinkName, DEFAULT_TARGETLINKNAME);
    targetName = MoreObjects.firstNonNull(targetName, DEFAULT_TARGETNAME);
    targetReferenceName =
        MoreObjects.firstNonNull(targetReferenceName, DEFAULT_TARGETREFERENCENAME);
    linkTable.addField(new LinkField(targetLinkName,
                                     "To " + sourceTable.getName(),
                                     null,
                                     targetName,
                                     sourceTable.getName(),
                                     targetReferenceName,
                                     null,
                                     transactionPassword,
                                     Dependencies.LINK_ENFORCED | (lockOnTargetEdit
                                         ? Dependencies.BIT_S_ON_T_UPDATE
                                         : 0) | (isOptionalLinks ? Dependencies.BIT_OPTIONAL : 0)));
    isDefinedName = MoreObjects.firstNonNull(isDefinedName, DEFAULT_ISDEFINEDNAME);
    linkTable.addField(new BooleanField(isDefinedName, "Link is defined?"));
    PostTransactionDeleteTrigger.register(linkTable,
                                          isDefinedName,
                                          transactionPassword,
                                          linkTable.getName() + "." + isDefinedName + " tidy up");
    if (sourceToTargetTargetsName != null) {
      sourceTable.addRecordOperation(new Targets(sourceToTargetTargetsName,
                                                 linkTable,
                                                 sourceLinkName,
                                                 targetLinkName,
                                                 isDefinedName,
                                                 LINK_SOURCE_TO_TARGET));
    }
    if (targetToSourceTargetsName != null) {
      sourceTable.addRecordOperation(new Targets(targetToSourceTargetsName,
                                                 linkTable,
                                                 sourceLinkName,
                                                 targetLinkName,
                                                 isDefinedName,
                                                 LINK_TARGET_TO_SOURCE));
    }
    if (allTargetsName != null) {
      sourceTable.addRecordOperation(new Targets(allTargetsName,
                                                 linkTable,
                                                 sourceLinkName,
                                                 targetLinkName,
                                                 isDefinedName,
                                                 LINK_BIDIRECTIONAL));
    }
    if (sourceToTargetIncludesName != null) {
      sourceTable.addRecordOperation(new Includes(sourceToTargetIncludesName,
                                                  linkTable,
                                                  sourceLinkName,
                                                  targetLinkName,
                                                  isDefinedName,
                                                  LINK_SOURCE_TO_TARGET));
    }
    if (targetToSourceIncludesName != null) {
      sourceTable.addRecordOperation(new Includes(targetToSourceIncludesName,
                                                  linkTable,
                                                  sourceLinkName,
                                                  targetLinkName,
                                                  isDefinedName,
                                                  LINK_TARGET_TO_SOURCE));
    }
    if (allIncludesName != null) {
      sourceTable.addRecordOperation(new Includes(allIncludesName,
                                                  linkTable,
                                                  sourceLinkName,
                                                  targetLinkName,
                                                  isDefinedName,
                                                  LINK_BIDIRECTIONAL));
    }

    return linkTable;
  }

  public final static String DEFAULT_TARGETS = "Targets";

  public final static String DEFAULT_SOURCES = "Sources";

  public final static String DEFAULT_ALL = "All";

  public final static String DEFAULT_INCLUDESTARGET = "IncludesTarget";

  public final static String DEFAULT_INCLUDESSOURCE = "IncludesSource";

  public final static String DEFAULT_INCLUDESEITHER = "IncludesEither";

  /**
   * Set up a reference that utilises as standard an initialisation as possible. <strong>This feels
   * a bit buggered in naming policies so should probably be refactored</strong> This will generally
   * be the most commonly used model. All non-referenced fields in the explicit registerComponents
   * method wil default to their standard default values.
   * 
   * @param db
   *          The Db that this linkage is to be registered on.
   * @param transactionPassword
   *          The transaction password for the current db.
   * @param sourceTable
   *          The Table which is to be linked to itself.
   * @param linkTableName
   *          The name of the Table that is going to be used to store the linkages. If this Table
   *          doesn't exist then it will be automatically created (in java space)
   * @param lockOnSourceEdit
   *          If true then linkTable records that point at a given sourceTable record as its source
   *          will be locked on edit of the sourceTable record.
   * @param lockOnTargetEdit
   *          If true then the linkTable records that point at a given sourceTable record as its
   *          target will be locked on edit of the sourceTable record.
   * @param isOptionalLinks
   *          If true then the sourceLinkName and targetLinkName fields on linkTable will be able to
   *          be not-defined. This would be an unusual situation and one would probably not utilise
   *          it as it is preferable that a linkTable always represents a relationship between two
   *          existing records. Deletion of a single sourceTable record would normally delete the
   *          linkTable records that referenced it.
   * @param sourceOperationNameStub
   *          The base name that is used to generate the values for the following parameters:-
   *          <dl>
   *          <dt>sourceToTargetTargetsName</dt>
   *          <dd>sourceOperationNameStub + <a href="#DEFAULT_TARGETS">DEFAULT_TARGETS</a></dd>
   *          <dt>targetToSourceTargetsName</dt>
   *          <dd>sourceOperationNameStub + <a href="#DEFAULT_SOURCES">DEFAULT_SOURCES</a></dd>
   *          <dt>allTargetsName</dt>
   *          <dd>sourceOperationNameStub + <a href="#DEFAULT_ALL">DEFAULT_ALL</a></dd>
   *          <dt>sourceToTargetIncludesName</dt>
   *          <dd>operationNameStub +
   *          <a href="#DEFAULT_INCLUDESTARGET">DEFAULT_INCLUDESTARGET</</dd>
   *          <dt>targetToSourceIncludesName</dt>
   *          <dd>operationNameStub +
   *          <a href="#DEFAULT_INCLUDESSOURCE">DEFAULT_INCLUDESSOURCE</</dd>
   *          <dt>allIncludesName</dt>
   *          <dd>operationNameStub +
   *          <a href="#DEFAULT_INCLUDESEITHER">DEFAULT_INCLUDESEITHER</</dd>
   *          </dl>
   * @return the linkTable that was already existing or that was specifically created and set up to
   *         hold the recursive reference information.
   */
  public static Table registerComponents(Db db,
                                         String transactionPassword,
                                         Table sourceTable,
                                         String linkTableName,
                                         boolean lockOnSourceEdit,
                                         boolean lockOnTargetEdit,
                                         boolean isOptionalLinks,

                                         String sourceOperationNameStub)
      throws MirrorTableLockedException
  {
    return registerComponents(db,
                              transactionPassword,
                              sourceTable,
                              linkTableName,
                              null,
                              null,
                              null,
                              lockOnSourceEdit,
                              null,
                              null,
                              null,
                              lockOnTargetEdit,
                              null,
                              isOptionalLinks,
                              RecordOperationKey.create(sourceOperationNameStub + DEFAULT_TARGETS,
                                                        RecordSource.class),
                              RecordOperationKey.create(sourceOperationNameStub + DEFAULT_SOURCES,
                                                        RecordSource.class),
                              RecordOperationKey.create(sourceOperationNameStub + DEFAULT_ALL,
                                                        RecordSource.class),
                              MonoRecordOperationKey.create(sourceOperationNameStub
                                                            + DEFAULT_INCLUDESTARGET,
                                                            Object.class,
                                                            Boolean.class),
                              MonoRecordOperationKey.create(sourceOperationNameStub
                                                            + DEFAULT_INCLUDESSOURCE,
                                                            Object.class,
                                                            Boolean.class),
                              MonoRecordOperationKey.create(sourceOperationNameStub
                                                            + DEFAULT_INCLUDESEITHER,
                                                            Object.class,
                                                            Boolean.class));
  }

  /**
   * RecordOperation that resolves the records that link to and / or are linked from the source
   * record by a ManyToManyRecursiveReference.
   */
  public static class Targets
    extends SimpleRecordOperation<RecordSource>
    implements RecordSourceResolver
  {
    private final Table __linkTable;

    private final IntFieldKey __sourceLinkName;

    private final IntFieldKey __targetLinkName;

    private final FieldKey<Boolean> __isDefinedName;

    private final int __linkType;

    public Targets(RecordOperationKey<RecordSource> name,
                   Table linkTable,
                   IntFieldKey sourceLinkName,
                   IntFieldKey targetLinkName,
                   FieldKey<Boolean> isDefinedName,
                   int linkType)
    {
      super(name);
      __linkTable = linkTable;
      __sourceLinkName = sourceLinkName;
      __targetLinkName = targetLinkName;
      __isDefinedName = isDefinedName;
      __linkType = linkType;
    }

    @Override
    public RecordSource getOperation(TransientRecord localRecord,
                                     Viewpoint viewpoint)
    {
      if (viewpoint == null || viewpoint.getRecordCount(__linkTable) == 0) {
        return RecordSources.viewedFrom(getLinkedRecordsQuery(__linkTable,
                                                              __sourceLinkName,
                                                              __targetLinkName,
                                                              localRecord,
                                                              __linkType,
                                                              __isDefinedName),
                                        // __linkType == LINK_BIDIRECTIONAL),
                                        viewpoint);
      }
      StringBuilder filter = new StringBuilder();
      RecordSource links = RecordSources.viewedFrom(getLinkRecordsQuery(__linkTable,
                                                                        __sourceLinkName,
                                                                        __targetLinkName,
                                                                        localRecord,
                                                                        null),
                                                    viewpoint);
      boolean hasLink = false;
      filter.append(__linkTable.getName()).append(".id in (");
      for (PersistentRecord link : links) {
        if (link.is(__isDefinedName)) {
          if (hasLink) {
            filter.append(",");
          }
          filter.append(link.getId());
          hasLink = true;
        }
      }
      if (!hasLink) {
        filter.append("0");
      }
      filter.append(") and ");
      appendLinkedRecordsFilterComponents(__linkTable,
                                          __sourceLinkName,
                                          __targetLinkName,
                                          localRecord,
                                          __linkType,
                                          filter);
      return getTable().getQuery(null, filter.toString(), null, __linkTable);
    }

    @Override
    public RecordSource resolveRecordSource(TransientRecord targetRecord,
                                            Viewpoint viewpoint)
    {
      return RecordSources.viewedFrom(getLinkedRecordsQuery(__linkTable,
                                                            __sourceLinkName,
                                                            __targetLinkName,
                                                            targetRecord,
                                                            __linkType,
                                                            __isDefinedName),
                                      // __linkType == LINK_BIDIRECTIONAL),
                                      viewpoint);
    }
  }

  /**
   * ParametricRecordOperation that determines whether or not the given target record is linked to
   * and / or is linked from the source record by a ManyToManyRecursiveReference.
   */
  public static class Includes
    extends MonoRecordOperation<Object, Boolean>
  {
    public static final int P0_TARGETRECORD = 0;

    private final Table __linkTable;

    private final IntFieldKey __sourceLinkName;

    private final IntFieldKey __targetLinkName;

    private final FieldKey<Boolean> __isDefinedName;

    private final int __linkType;

    protected Includes(MonoRecordOperationKey<Object, Boolean> name,
                       Table linkTable,
                       IntFieldKey sourceLinkName,
                       IntFieldKey targetLinkName,
                       FieldKey<Boolean> isDefinedName,
                       int linkType)
    {
      super(name,
            Casters.NULL);
      __linkTable = linkTable;
      __sourceLinkName = sourceLinkName;
      __targetLinkName = targetLinkName;
      __isDefinedName = isDefinedName;
      __linkType = linkType;
    }

    @Override
    public Boolean calculate(TransientRecord record,
                             Viewpoint viewpoint,
                             Object targetObj)
    {
      if (targetObj == null) {
        return Boolean.FALSE;
      }
      TransientRecord targetRecord = null;
      if (targetObj instanceof TransientRecord) {
        targetRecord = (TransientRecord) targetObj;
      } else {
        try {
          targetRecord =
              (record.getTable().getRecord(Integer.valueOf(targetObj.toString()), viewpoint));
        } catch (RuntimeException badIdEx) {
          return Boolean.FALSE;
        } catch (MirrorNoSuchRecordException badIdEx) {
          return Boolean.FALSE;
        }
      }
      return Boolean.valueOf(getRelationship(__linkTable,
                                             __sourceLinkName,
                                             __targetLinkName,
                                             record,
                                             targetRecord,
                                             __linkType,
                                             __isDefinedName,
                                             viewpoint));
    }
  }
}
