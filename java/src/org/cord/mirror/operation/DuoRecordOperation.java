package org.cord.mirror.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.AbstractGettable;
import org.cord.util.Caster;

import com.google.common.base.Preconditions;

/**
 * RecordOperation that takes two parameters
 * 
 * @author alex
 * @param <A>
 *          The class of the first parameter
 * @param <B>
 *          The class of the second parameter
 * @param <V>
 *          The class of the resulting value
 */
public abstract class DuoRecordOperation<A, B, V>
  extends AbstractLockableOperation
  implements RecordOperation<DuoRecordOperation<A, B, V>.Invoker>
{
  private final DuoRecordOperationKey<A, B, V> __key;
  private final Caster<A> __casterA;
  private final Caster<B> __casterB;

  public DuoRecordOperation(DuoRecordOperationKey<A, B, V> key,
                            Caster<A> casterA,
                            Caster<B> casterB)
  {
    super(key.getKeyName());
    __key = key;
    __casterA = Preconditions.checkNotNull(casterA, "casterA");
    __casterB = Preconditions.checkNotNull(casterB, "casterB");
  }

  @Override
  public DuoRecordOperationKey<A, B, V> getKey()
  {
    return __key;
  }

  @Override
  public final Invoker getOperation(TransientRecord record,
                                    Viewpoint viewpoint)
  {
    return new Invoker(record, viewpoint);
  }

  public abstract V calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              A a,
                              B b);

  @Override
  public void shutdown()
  {
  }

  public class Invoker
    extends AbstractGettable
  {
    private final TransientRecord __record;
    private final Viewpoint __viewpoint;

    private Invoker(TransientRecord record,
                    Viewpoint viewpoint)
    {
      __record = record;
      __viewpoint = viewpoint;
    }

    public V call(A a,
                  B b)
    {
      return calculate(__record, __viewpoint, a, b);
    }

    @Override
    public Second get(Object key)
    {
      return new Second(__casterA.cast(key));
    }

    public class Second
      extends AbstractGettable
    {
      private final A __a;

      private Second(A a)
      {
        __a = a;
      }

      @Override
      public V get(Object key)
      {
        return calculate(__record, __viewpoint, __a, __casterB.cast(key));
      }
    }
  }
}
