package org.cord.mirror.operation;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

/**
 * RecordOperation that provides the ability to assign a constant value to a named "field". Please
 * note that the same Object will be returned irregardless of which record or viewpoint it is
 * assigned on.
 */
public class ConstantRecordOperation<V>
  extends SimpleRecordOperation<V>
{
  public static <V> ConstantRecordOperation<V> create(RecordOperationKey<V> name,
                                                      V value)
  {
    return new ConstantRecordOperation<V>(name, value);
  }

  private final V __value;

  /**
   * Create a new constant.
   * 
   * @param name
   *          The name under which this constant will be referenced.
   * @param value
   *          The value that should be returned when this constant is invoked. Please note that this
   *          should be totally thread safe in the event that it might be mutable.
   */
  public ConstantRecordOperation(RecordOperationKey<V> name,
                                 V value)
  {
    super(name);
    __value = value;
  }

  /**
   * @return The value passed to the creator.
   */
  @Override
  public V getOperation(TransientRecord record,
                        Viewpoint viewpoint)
  {
    return __value;
  }

  @Override
  public String toString()
  {
    return "constant(" + __value + ")";
  }
}
