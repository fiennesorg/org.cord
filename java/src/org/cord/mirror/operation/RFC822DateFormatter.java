package org.cord.mirror.operation;

import java.util.Date;

import javax.mail.internet.MailDateFormat;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.Dates;

/**
 * Provides a ParametricRecordOperation that formats date fields in RFC822 format
 * 
 * @see org.cord.mirror.operation.DateFormatter
 * @see javax.mail.internet.MailDateFormat
 */
public class RFC822DateFormatter
  extends MonoRecordOperation<Object, String>
{
  private final MailDateFormat __mailDateFormat = new MailDateFormat();

  public static final MonoRecordOperationKey<Object, String> NAME =
      MonoRecordOperationKey.create("rfc822", Object.class, String.class);

  public static final int P0_OPERATIONNAME = 0;

  public RFC822DateFormatter()
  {
    super(NAME,
          Casters.NULL);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          Object param)
  {
    if (param instanceof Date) {
      return Dates.format(__mailDateFormat, (Date) param);
    }
    Object value = record.get(param.toString());
    if (value instanceof Date) {
      return Dates.format(__mailDateFormat, (Date) value);
    }
    throw new IllegalArgumentException("DateFormatter can only format Date objects");
  }

}
