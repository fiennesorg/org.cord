package org.cord.mirror.operation;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.StringUtils;

public class AsSterling
  extends MonoRecordOperation<String, String>
{
  /**
   * "asSterling"
   */
  public final static MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("asSterling", String.class, String.class);

  public final static BigDecimal POINTZEROONE = new BigDecimal("0.01");

  private final boolean __interpretIntegerAsPence;

  private final static NumberFormat NUMBERFORMAT = NumberFormat.getNumberInstance();
  static {
    NUMBERFORMAT.setMinimumFractionDigits(2);
    NUMBERFORMAT.setMaximumFractionDigits(2);
  }

  public AsSterling(boolean interpretIntegerAsPence)
  {
    super(NAME,
          Casters.TOSTRING);
    __interpretIntegerAsPence = interpretIntegerAsPence;
  }

  private final static char __iso8859PoundsChar = 0xa3;

  public static String process(TransientRecord record,
                               String field,
                               boolean interpretIntegerAsPence)
  {
    Object value = record.get(field);
    if (value instanceof Number) {
      if (interpretIntegerAsPence && value instanceof Integer) {
        BigDecimal pence = new BigDecimal(((Integer) value).intValue());
        value = pence.multiply(POINTZEROONE);
      }
      synchronized (NUMBERFORMAT) {
        return __iso8859PoundsChar + NUMBERFORMAT.format(value);
      }
    }
    return StringUtils.toString(value);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperation)
  {
    return process(record, recordOperation, __interpretIntegerAsPence);
  }
}
