package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.RecordSources;

import com.google.common.collect.ImmutableSet;

public abstract class AbstractReverseRecordOperation<V>
  extends AbstractRecordSourceResolverOperation
  implements Dependencies
{
  private final Table __linkTable;

  private final FieldKey<V> __linkFieldName;

  private final String __targetOrderClause;

  private final int __linkStyle;

  private final LinkField __linkFieldFilter;

  public AbstractReverseRecordOperation(Table linkTable,
                                        FieldKey<V> linkFieldName,
                                        RecordOperationKey<RecordSource> invocationName,
                                        String targetOrderClause,
                                        int linkStyle,
                                        LinkField linkFieldFilter)
  {
    super(invocationName);
    __linkTable = linkTable;
    __linkFieldName = linkFieldName;
    __targetOrderClause = targetOrderClause;
    __linkStyle = linkStyle;
    __linkFieldFilter = linkFieldFilter;
  }

  public final FieldKey<V> getLinkFieldName()
  {
    return __linkFieldName;
  }

  public final Table getLinkTable()
  {
    return __linkTable;
  }

  public final String getTargetOrderClause()
  {
    return __targetOrderClause;
  }

  protected abstract String buildFilter(TransientRecord record);

  @Override
  protected RecordSource createRecordSource(TransientRecord targetRecord,
                                            Object cacheKey,
                                            Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(getLinkTable().getQuery(cacheKey,
                                                            buildFilter(targetRecord),
                                                            getTargetOrderClause()),
                                    viewpoint);
  }

  @Override
  protected Table getRecordSourceTable()
  {
    return getLinkTable();
  }

  @Override
  public boolean mayHaveDependencies(PersistentRecord record,
                                     int transactionType)
  {
    if (__linkFieldFilter != null
        && __linkFieldFilter.mayHaveDependencies(record, transactionType)) {
      switch (transactionType) {
        case Transaction.TRANSACTION_DELETE:
          return ((__linkStyle & BIT_OPTIONAL_DELETEIFDEFINED) != 0
                  || (__linkStyle & BIT_OPTIONAL) == 0)
                 && (__linkStyle & BIT_S_ON_T_DELETE) != 0;
        case Transaction.TRANSACTION_UPDATE:
          return (__linkStyle & BIT_S_ON_T_UPDATE) != 0;
      }
    }
    return false;
  }

  @Override
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                        int transactionType)
  {
    return getDependentRecords(record, transactionType, (Transaction) null);
  }

  @Override
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                        int transactionType,
                                                        Viewpoint viewpoint)
  {
    if (mayHaveDependencies(record, transactionType)) {
      return resolveRecordSource(record, viewpoint).getRecordList(viewpoint).iterator();
    } else {
      return ImmutableSet.<PersistentRecord> of().iterator();
    }
  }
}
