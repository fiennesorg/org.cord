package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;

/**
 * Provides the functionality to transparently move from a record with a reference to another record
 * to the target record.
 */
public abstract class AbstractForwardRecordOperation<V, T>
  extends SimpleRecordOperation<T>
  implements Dependencies
{
  private final Table _targetTable;

  private final FieldKey<V> _linkFieldName;

  private final int _linkStyle;

  public AbstractForwardRecordOperation(Table targetTable,
                                        FieldKey<V> linkFieldName,
                                        RecordOperationKey<T> invocationName,
                                        int linkStyle)
  {
    super(invocationName);
    _targetTable = targetTable;
    _linkFieldName = linkFieldName;
    _linkStyle = linkStyle;
  }

  protected final Table getTargetTable()
  {
    return _targetTable;
  }

  protected final FieldKey<V> getLinkFieldName()
  {
    return _linkFieldName;
  }

  protected final int getLinkStyle()
  {
    return _linkStyle;
  }

  /**
   * Check to see if this forward link contains any dependencies for the given link record and
   * transaction style.
   * 
   * @param record
   *          The record from the linkTable who is being checked for dependent records.
   * @param transactionType
   *          The style of Transaction as defined in TransactionTypes.
   * @return true if (TRANSACTION_DELETE and BIT_T_ON_S_DELETE) or (TRANSACTION_UPDATE and
   *         BIT_T_ON_S_UPDATE) is defined. Otherwise false.
   * @see Transaction#TRANSACTION_DELETE
   * @see #BIT_T_ON_S_DELETE
   * @see #BIT_T_ON_S_UPDATE
   */
  @Override
  public boolean mayHaveDependencies(PersistentRecord record,
                                     int transactionType)
  {
    switch (transactionType) {
      case Transaction.TRANSACTION_DELETE:
        return ((_linkStyle & BIT_OPTIONAL) == 0) && ((_linkStyle & BIT_T_ON_S_DELETE) != 0);
      case Transaction.TRANSACTION_UPDATE:
        return (_linkStyle & BIT_T_ON_S_UPDATE) != 0;
      default:
        return false;
    }
  }

  @Override
  public Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                        int transactionType)
  {
    return getDependentRecords(record, transactionType, null);
  }
}
