package org.cord.mirror.operation;

import org.cord.mirror.FilteredCachingSimpleRecordOperation;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableListener;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.TimestampedObject;

/**
 * Base implementation of a RecordOperation that enables caching of values with a conditional
 * discarding of the cached when data in the Table changes. Clients should implement calculateValue
 * to build the value that should be cached and isCacheInvalid to state whether a given change to a
 * record in the Table might have rendered the cached values obsolete.
 * 
 * @author alex
 * @param <T>
 *          The class of value that this RecordOperation produces
 */
public abstract class TimestampedRecordOperation<T>
  extends FilteredCachingSimpleRecordOperation<TimestampedObject<T>, T>
  implements TableListener
{
  public TimestampedRecordOperation(RecordOperationKey<T> name)
  {
    super(name);
  }

  private volatile long _tableChangedTimestamp = 0;

  // --------------------------------------------------------------------------
  // final
  @Override
  public final TimestampedObject<T> calculate(TransientRecord targetRecord,
                                              Viewpoint viewpoint)
  {
    return new TimestampedObject<T>(calculateValue(targetRecord, viewpoint));
  }

  @Override
  public final T filter(TransientRecord targetRecord,
                        Viewpoint viewpoint,
                        TimestampedObject<T> timestamped)
  {
    if (timestamped.getTimestamp() < _tableChangedTimestamp || timestamped.getValue() == null) {
      timestamped.setValue(calculateValue(targetRecord, viewpoint));
    }
    return timestamped.getValue();
  }

  @Override
  public final boolean tableChanged(Table table,
                                    int recordId,
                                    PersistentRecord persistentRecord,
                                    int type)
  {
    if (isCacheInvalid(table, recordId, persistentRecord, type)) {
      invalidateCache();
    }
    return true;
  }

  /**
   * Inform this RecordOperation that all cached values are no longer to be trusted from this point
   * onwards. Any resolution of this RecordOperation will therefore end up invoking calculateValue
   * 
   * @see #calculateValue(TransientRecord, Viewpoint)
   */
  public final void invalidateCache()
  {
    _tableChangedTimestamp = System.currentTimeMillis();
  }

  // --------------------------------------------------------------------------
  // abstract
  //
  /**
   * Calculate the value of invoking this RecordOperation on the given targetRecord from the
   * perspective of the given viewpoint. This will only be invoked initially or if the cache is
   * invalidated at a later date.
   */
  protected abstract T calculateValue(TransientRecord targetRecord,
                                      Viewpoint viewpoint);

  /**
   * Check to see whether or not the values in the given record should necessitate the discarding of
   * cached values for this RecordOperation. This is invoked during the TableListener notification
   * of changes on the Table.
   * 
   * @return true if all cached values for this RecordOperation are no longer to be trusted.
   * @see TableListener#tableChanged(Table, int, PersistentRecord, int)
   */
  protected abstract boolean isCacheInvalid(Table table,
                                            int recordId,
                                            PersistentRecord persistentRecord,
                                            int type);
}
