package org.cord.mirror.operation;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.trigger.AbstractFieldTrigger;

import com.google.common.base.Optional;

public class OptionalHttpPrefixFieldTrigger
  extends AbstractFieldTrigger<Optional<String>>
{

  public OptionalHttpPrefixFieldTrigger(FieldKey<Optional<String>> fieldName)
  {
    super(fieldName,
          true,
          false);
  }

  @Override
  protected Object triggerField(TransientRecord record,
                                String fieldName,
                                Transaction transaction)
      throws MirrorFieldException
  {
    Optional<String> optionalUrl = record.comp(getFieldName());
    if (!optionalUrl.isPresent()) {
      return null;
    }
    String url = optionalUrl.get().trim();
    if (url.length() == 0) {
      throw new MirrorFieldException("empty string to null",
                                     null,
                                     record,
                                     fieldName,
                                     optionalUrl,
                                     Optional.absent());
    }
    String lowerCaseUrl = url.toLowerCase();
    if (lowerCaseUrl.startsWith("http://") | lowerCaseUrl.startsWith("https://")) {
      return null;
    }
    throw new MirrorFieldException("prepended default http://",
                                   null,
                                   record,
                                   fieldName,
                                   optionalUrl,
                                   Optional.of("http://" + url));
  }

}
