package org.cord.mirror.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * Delegating implementation of RecordOperation that permits a different implementation of
 * RecordOperation to be associated with a Node for each NodeStyle in the system.
 */
public class IdSpecificRecordOperation<V>
  extends SimpleRecordOperation<V>
{
  private final RecordOperation<? extends V> __defaultRecordOperation;

  private final Int2ObjectMap<RecordOperation<? extends V>> __recordOperations =
      new Int2ObjectOpenHashMap<RecordOperation<? extends V>>();

  public IdSpecificRecordOperation(RecordOperationKey<V> name,
                                   RecordOperation<? extends V> defaultRecordOperation)
  {
    super(name);
    __defaultRecordOperation =
        Preconditions.checkNotNull(defaultRecordOperation, "defaultRecordOperation");
  }

  public void addRecordOperation(int id,
                                 RecordOperation<? extends V> recordOperation)
  {
    if (recordOperation != null) {
      __recordOperations.put(id, recordOperation);
    }
  }

  @Override
  public V getOperation(TransientRecord record,
                        Viewpoint viewpoint)
  {
    RecordOperation<? extends V> recordOperation = __recordOperations.get(record.getId());
    recordOperation = recordOperation == null ? __defaultRecordOperation : recordOperation;
    return recordOperation.getOperation(record, viewpoint);
  }
}