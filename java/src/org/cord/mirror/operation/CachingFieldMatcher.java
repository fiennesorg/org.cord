package org.cord.mirror.operation;

import java.util.concurrent.ExecutionException;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TableListener;
import org.cord.mirror.Transaction;
import org.cord.util.LogicException;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class CachingFieldMatcher<T>
  implements TableListener
{
  /**
   * Create a CachingFieldMatcher around the given FieldMatcher with no expiry or limits on the
   * guava cache. This should only be utilised if you are confident that the maximum number of
   * matches or misses will fit within memory.
   */
  public static <T> CachingFieldMatcher<T> unlimited(FieldMatcher<T> fieldMatcher)
  {
    return new CachingFieldMatcher<T>(fieldMatcher, CacheBuilder.newBuilder());
  }

  public static <T> CachingFieldMatcher<T> maxSize(FieldMatcher<T> fieldMatcher,
                                                   long maxSize)
  {
    return new CachingFieldMatcher<T>(fieldMatcher, CacheBuilder.newBuilder().maximumSize(maxSize));
  }

  public static <T> CachingFieldMatcher<T> withSpec(FieldMatcher<T> fieldMatcher,
                                                    String spec)
  {
    return new CachingFieldMatcher<T>(fieldMatcher, CacheBuilder.from(spec));
  }

  private final FieldMatcher<T> __fieldMatcher;

  private LoadingCache<T, Optional<Integer>> __idCache;

  public CachingFieldMatcher(FieldMatcher<T> fieldMatcher,
                             CacheBuilder<Object, Object> cacheBuilder)
  {
    __fieldMatcher = Preconditions.checkNotNull(fieldMatcher);
    __idCache = cacheBuilder.build(new CacheLoader<T, Optional<Integer>>() {
      @Override
      public Optional<Integer> load(T key)
          throws Exception
      {
        return __fieldMatcher.optId(key);
      }
    });
    fieldMatcher.getTable().addTableListener(this);
  }

  public final FieldMatcher<T> getFieldMatcher()
  {
    return __fieldMatcher;
  }

  /**
   * If the notification type is a {@link Transaction#TRANSACTION_INSERT} then update the cache with
   * the appropriate value, otherwise we don't know what has changed to what, or what has been
   * deleted so we clear the entire cache.
   * 
   * @return true because we want to continue to be informed of changes.
   */
  @Override
  public boolean tableChanged(Table table,
                              int recordId,
                              PersistentRecord persistentRecord,
                              int type)
  {
    if (type == Transaction.TRANSACTION_INSERT) {
      __idCache.put(persistentRecord.comp(__fieldMatcher.getFieldKey()),
                    Optional.of(Integer.valueOf(persistentRecord.getId())));
    } else {
      __idCache.invalidateAll();
    }
    return true;
  }

  /**
   * Resolve the given value against the cache lookup.
   * 
   * @param value
   *          The optional value to look for. If it is undefined then {@link Optional#absent()} will
   *          always be returned.
   * @return The Optional wrapper around the matching id, or {@link Optional#absent()} if either
   *         value is not found or value is not defined.
   */
  public final Optional<Integer> optId(T value)
  {
    if (value == null) {
      return Optional.absent();
    }
    try {
      return __idCache.get(value);
    } catch (ExecutionException e) {
      throw new LogicException(e);
    }
  }
}
