package org.cord.mirror.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.SimpleGettable;

import com.google.common.base.Optional;

/**
 * MonoRecordOperation that resolves the parameter as a RecordOperation on the record and returns
 * the result if defined, and otherwise returns a SimpleGettable that formats to an empty String and
 * always contains itself. It is intended to enable concise webmacro evaluation because suppose you
 * have <em>$record.optionalOperation.property</em> then normally you would have to check whether or
 * not <em>$record.optionalOperation</em> had a value and then resolve property if it did, but now
 * you can just invoke <em>$record.opt.optionalOperation.property</em> and you will not get a
 * NullPointerException but rather just a value that resolves to "" in the template. This is
 * particularly useful if you are using stuff like <em>#wrapDefined "header"
 * $record.opt.optionalOperation.property "footer"</em> and enables optional operations to be
 * expressed very concisefy.
 * 
 * @author alex
 */
public class Opt
  extends MonoRecordOperation<String, Object>
{
  public final static SimpleGettable NOVALUE = new SimpleGettable() {
    @Override
    public boolean containsKey(Object key)
    {
      return false;
    }

    @Override
    public Object get(Object key)
    {
      return this;
    }

    @Override
    public String toString()
    {
      return "";
    }
  };

  public Opt()
  {
    super(MonoRecordOperationKey.create("opt", String.class, Object.class),
          Casters.TOSTRING);
  }

  @Override
  public Object calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String a)
  {
    Object object = record.get(a);
    if (object != null) {
      if (object instanceof Optional<?>) {
        Optional<?> optional = (Optional<?>) object;
        if (optional.isPresent()) {
          return optional.get();
        }
      } else {
        return object;
      }
    }
    return NOVALUE;
  }
}
