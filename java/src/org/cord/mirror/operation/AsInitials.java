package org.cord.mirror.operation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;

import com.google.common.base.Strings;

/**
 * Dual Parametric RecordOperation that resolves the supplied parameter into a String and then grabs
 * the first letter of each of the component words and returns them separated by the second
 * parameter.
 * 
 * @author alex
 */
public class AsInitials
  extends DuoRecordOperation<String, String, String>
{
  public static final DuoRecordOperationKey<String, String, String> NAME =
      DuoRecordOperationKey.create("asInitials", String.class, String.class, String.class);

  private static final Pattern __splitPattern = Pattern.compile("\\w+");

  public AsInitials()
  {
    super(NAME,
          Casters.TOSTRING,
          Casters.TOSTRING);
  }

  public static String asInitials(String value,
                                  String separator)
  {
    if (Strings.isNullOrEmpty(value)) {
      return value;
    }
    separator = separator == null ? "" : separator;
    StringBuilder buf = new StringBuilder();
    Matcher m = __splitPattern.matcher(value);
    while (m.find()) {
      buf.append(Character.toUpperCase(m.group().charAt(0))).append(separator);
    }
    return buf.toString();
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperation,
                          String separator)
  {
    return asInitials(record.getString(recordOperation), separator);
  }
}
