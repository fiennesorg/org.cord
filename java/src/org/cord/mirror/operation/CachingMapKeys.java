package org.cord.mirror.operation;

import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

public class CachingMapKeys<K, V>
  extends SimpleRecordOperation<Iterator<K>>
{
  private final String __cachingMapName;

  public CachingMapKeys(RecordOperationKey<Iterator<K>> name,
                        String cachingMapName)
  {
    super(name);
    __cachingMapName = cachingMapName;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Iterator<K> getOperation(TransientRecord record,
                                  Viewpoint viewpoint)
  {
    return ((Map<K, V>) record.get(__cachingMapName)).keySet().iterator();
  }
}