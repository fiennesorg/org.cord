package org.cord.mirror.operation;

import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.util.StringUtils;

/**
 * Implementation of a TableOperation that has a non-parametric constant table SQL query format.
 */
public class ConstantTableQuery
  extends SimpleRecordSourceOperation<RecordSource>
{
  private final String __where;

  private final String __order;

  public ConstantTableQuery(RecordSourceOperationKey<RecordSource> name,
                            String where,
                            Object order)
  {
    super(name);
    __where = where;
    __order = StringUtils.toString(order);
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                  Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(recordSource.getTable().getQuery(null, __where, __order),
                                    viewpoint);
  }
}
