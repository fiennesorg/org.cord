package org.cord.mirror.operation;

import java.util.Calendar;
import java.util.Date;

import org.cord.mirror.FieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

/**
 * RecordOperation that extracts a given property from a FieldFilter that contains a Date.
 */
public class DateFieldRecordOperation
  extends SimpleRecordOperation<Integer>
{
  private final Calendar __calendar;

  private final int __calendarField;

  private final FieldKey<Date> __targetFieldName;

  public DateFieldRecordOperation(RecordOperationKey<Integer> name,
                                  FieldKey<Date> targetFieldName,
                                  Calendar calendar,
                                  int calendarField)
  {
    super(name);
    __targetFieldName = targetFieldName;
    __calendar = calendar;
    __calendarField = calendarField;
  }

  /**
   * Create a DateFieldRecordOperation with its own internal Calendar Object. This has the advantage
   * that it doesn't share synchronisation locks with other instances.
   */
  public DateFieldRecordOperation(RecordOperationKey<Integer> name,
                                  FieldKey<Date> targetFieldName,
                                  int calendarField)
  {
    this(name,
         targetFieldName,
         Calendar.getInstance(),
         calendarField);
  }

  public static Integer getValue(TransientRecord record,
                                 FieldKey<Date> fieldName,
                                 Calendar calendar,
                                 int calendarField)
  {
    Date date = record.opt(fieldName);
    if (date == null) {
      return null;
    }
    synchronized (calendar) {
      calendar.setTime(record.opt(fieldName));
      return Integer.valueOf(calendar.get(calendarField));
    }
  }

  @Override
  public Integer getOperation(TransientRecord targetRecord,
                              Viewpoint viewpoint)
  {
    return getValue(targetRecord, __targetFieldName, __calendar, __calendarField);
  }
}
