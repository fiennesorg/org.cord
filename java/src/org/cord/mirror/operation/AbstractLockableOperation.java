package org.cord.mirror.operation;

import org.cord.mirror.LockableOperation;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Table;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;

import com.google.common.base.Preconditions;

public class AbstractLockableOperation
  extends NamedImpl
  implements LockableOperation, Cloneable
{
  private Table _table = null;

  private final int _replaceableStatus;

  private int _lockStatus = LO_NEW;

  /**
   * Create an AbstractLockableOperation that permits de-initialisation at up to a specified point
   * in the process.
   */
  public AbstractLockableOperation(String name,
                                   int replaceableStatus)
  {
    super(name);
    LO_NAMES.get(replaceableStatus);
    _replaceableStatus = replaceableStatus;
  }

  /**
   * Create an AbstractLockableOperation that does not permit de-initialisation.
   * 
   * @param name
   *          The name by which this object is to be registered.
   */
  public AbstractLockableOperation(String name)
  {
    this(name,
         LO_INITIALISED);
  }

  /**
   * Check to see whether or not this object is in a locked state and throw a LogicException if it
   * is.
   * 
   * @param errorMessage
   *          The text that will appear in the LogicException if it is thrown.
   */
  public final void blockOnState(int maximumState,
                                 String errorMessage)
  {
    if (getLockStatus() > maximumState) {
      throw new LogicException(errorMessage);
    }
  }

  /**
   * Internal hook method that is invoked directly after the lock status of the LockableOperation is
   * upgraded. It is not expected that implementations of this method (the default is a null method)
   * should throw any exceptions and this should be discouraged.
   */
  protected void lockStatusChanged()
  {
  }

  @Override
  public final void initialise(Table table)
  {
    Preconditions.checkNotNull(table, "table");
    switch (_lockStatus) {
      case LO_NEW:
        _table = table;
        _lockStatus = LO_INITIALISED;
        lockStatusChanged();
        break;
      case LO_INITIALISED:
      case LO_PRELOCKED:
      case LO_LOCKED:
        throw new LogicException(String.format("Cannot call %s.initialise(%s) when in state %s",
                                               this,
                                               table,
                                               LO_NAMES.get(_lockStatus)));
    }
  }
  @Override
  public final void preLock()
      throws MirrorTableLockedException
  {
    switch (_lockStatus) {
      case LO_NEW:
        throw new LogicException("Cannot call .preLock() before .initialise(table)");
      case LO_INITIALISED:
        internalPreLock();
        _lockStatus = LO_PRELOCKED;
        lockStatusChanged();
        break;
      case LO_PRELOCKED:
        return;
      case LO_LOCKED:
        throw new LogicException("Cannot call .preLock() after .lock()");
    }
  }

  /**
   * Internal method that should be implemented by subclasses that require actions during the
   * preLock() action. This is guaranteed to be called only once and it is guaranteed to be called
   * after the initialise(table) method and before the lock() method. The default implementation is
   * to do nothing at all. If a particular implementation is responsible for the creation of
   * additional lockable assets, then it is advisable to manually prelock() these assets in case
   * they get excluded from the current sweep.
   */
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
  }

  @Override
  public final void lock()
  {
    switch (_lockStatus) {
      case LO_NEW:
        throw new LogicException("Cannot call .lock() before .initialise(table) on " + this);
      case LO_INITIALISED:
        throw new LogicException("Cannot call .lock() before .preLock() on " + this);
      case LO_PRELOCKED:
        _lockStatus = LO_LOCKED;
        lockStatusChanged();
        break;
      case LO_LOCKED:
        // not bothered...
    }
  }

  @Override
  public final int getLockStatus()
  {
    return _lockStatus;
  }

  /**
   * Get the Table that this LockableOperation is registered on.
   * 
   * @return the appropriate Table or null if the LockableOperation has not yet been registered on a
   *         Table as yet.
   */
  public final Table getTable()
  {
    return _table;
  }

  @Override
  public final boolean mayDeInitialise(LockableOperation substitute)
  {
    if (getLockStatus() >= _replaceableStatus) {
      return false;
    } else {
      return internalMayDeInitialise(substitute);
    }
  }

  protected boolean internalMayDeInitialise(LockableOperation substitute)
  {
    return false;
  }

  @Override
  public Object clone()
      throws CloneNotSupportedException
  {
    return super.clone();
  }

  @Override
  public boolean equals(Object o)
  {
    if (super.equals(o)) {
      Table table = getTable();
      return table != null && table.equals(((AbstractLockableOperation) o).getTable());
    }
    return false;
  }
}
