package org.cord.mirror.operation;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceResolver;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;

import com.google.common.base.Preconditions;

/**
 * Abstract implementation of RecordSourceResolver based around AbstractRecordOperation to make it
 * easy to deploy anonymous inner class implementations.
 * 
 * @author alex
 */
public abstract class AbstractRecordSourceResolverOperation
  extends SimpleRecordOperation<RecordSource>
  implements RecordSourceResolver
{
  private volatile Table _queryTable = null;

  public AbstractRecordSourceResolverOperation(RecordOperationKey<RecordSource> name)
  {
    super(name);
  }

  public final Table getCachedRecordSourceTable()
  {
    if (_queryTable == null) {
      _queryTable = getRecordSourceTable();
      Preconditions.checkNotNull(_queryTable, "queryTable");
    }
    return _queryTable;
  }

  /**
   * Get the Table that the Query that this RecordSourceResolver will target. This will be invoked
   * at most once by this class and the result will then be cached.
   */
  protected abstract Table getRecordSourceTable();

  @Override
  public final RecordSource getOperation(TransientRecord targetRecord,
                                         Viewpoint viewpoint)
  {
    return resolveRecordSource(targetRecord, viewpoint);
  }

  @Override
  public final RecordSource resolveRecordSource(TransientRecord targetRecord,
                                                Viewpoint viewpoint)
  {
    Object cacheKey = new CacheKey(this, targetRecord.getId());
    RecordSource cachedRecordSource = getCachedRecordSourceTable().getCachedRecordSource(cacheKey);
    if (cachedRecordSource != null) {
      return RecordSources.viewedFrom(cachedRecordSource, viewpoint);
    }
    return createRecordSource(targetRecord, cacheKey, viewpoint);
  }

  public final static class CacheKey
  {
    private final AbstractRecordSourceResolverOperation __container;
    private final int __id;

    public CacheKey(AbstractRecordSourceResolverOperation container,
                    int id)
    {
      __container = container;
      __id = id;
    }

    public AbstractRecordSourceResolverOperation getContainer()
    {
      return __container;
    }

    public int getId()
    {
      return __id;
    }

    @Override
    public boolean equals(Object o)
    {
      if (!(o instanceof CacheKey)) {
        return false;
      }
      CacheKey that = (CacheKey) o;
      return this.getId() == that.getId() & this.getContainer().equals(that.getContainer());
    }

    @Override
    public int hashCode()
    {
      return getContainer().hashCode() ^ getId();
    }

    @Override
    public String toString()
    {
      return getContainer().toString() + ".CacheKey(" + __id + ")";
    }
  }

  /**
   * Create the RecordSource represented by this operation for the given targetRecord. This should
   * be invoked relatively infrequently as the return value will be cached.
   * 
   * @param targetRecord
   *          The record that this operation is being invoked upon.
   * @param cacheKey
   *          The key that the RecordSource should utilise for caching purposes.
   * @param viewpoint
   *          The optional Viewpoint that this RecordSource should contain.
   * @return The apropriate RecordSource.
   */
  protected abstract RecordSource createRecordSource(TransientRecord targetRecord,
                                                     Object cacheKey,
                                                     Viewpoint viewpoint);
}
