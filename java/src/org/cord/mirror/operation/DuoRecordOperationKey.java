package org.cord.mirror.operation;

import org.cord.mirror.RecordOperationKey;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

public class DuoRecordOperationKey<A, B, V>
  extends RecordOperationKey<DuoRecordOperation<A, B, V>.Invoker>
{
  public static final <A, B, V> DuoRecordOperationKey<A, B, V> create(String keyName,
                                                                      Class<A> classA,
                                                                      Class<B> classB,
                                                                      Class<V> classV)
  {
    return new DuoRecordOperationKey<A, B, V>(keyName, classA, classB, classV);
  }

  private final Class<A> __classA;
  private final Class<B> __classB;
  private final Class<V> __classV;

  public DuoRecordOperationKey(String keyName,
                               Class<A> classA,
                               Class<B> classB,
                               Class<V> classV)
  {
    super(keyName,
          ObjectUtil.<DuoRecordOperation<A, B, V>
                    .Invoker> castClass(DuoRecordOperation.Invoker.class));
    __classA = Preconditions.checkNotNull(classA, "classA");
    __classB = Preconditions.checkNotNull(classB, "classB");
    __classV = Preconditions.checkNotNull(classV, "classV");
  }

  public final Class<A> getClassA()
  {
    return __classA;
  }

  public final Class<B> getClassB()
  {
    return __classB;
  }

  public final Class<V> getClassV()
  {
    return __classV;
  }

  @Override
  public DuoRecordOperationKey<A, B, V> copy()
  {
    return new DuoRecordOperationKey<A, B, V>(getKeyName(), getClassA(), getClassB(), getClassV());
  }
}
