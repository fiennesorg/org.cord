package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.Dependencies;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

import com.google.common.collect.ImmutableSet;

public abstract class AbstractDependencies
  extends SimpleRecordOperation<Iterator<PersistentRecord>>
  implements Dependencies
{
  public AbstractDependencies(RecordOperationKey<Iterator<PersistentRecord>> name)
  {
    super(name);
  }

  @Override
  public final Iterator<PersistentRecord> getOperation(TransientRecord targetRecord,
                                                       Viewpoint viewpoint)
  {
    if (targetRecord instanceof PersistentRecord) {
      return internalGetDependentRecords((PersistentRecord) targetRecord,
                                         Transaction.TRANSACTION_UPDATE,
                                         viewpoint);
    }
    return ImmutableSet.<PersistentRecord> of().iterator();
  }

  @Override
  public final Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                              int transactionType)
  {
    return internalGetDependentRecords(record, transactionType, null);
  }

  /**
   * @see Transaction#stripNullTransaction(Viewpoint)
   */
  @Override
  public final Iterator<PersistentRecord> getDependentRecords(PersistentRecord record,
                                                              int transactionType,
                                                              Viewpoint viewpoint)
  {
    return internalGetDependentRecords(record,
                                       transactionType,
                                       Transaction.stripNullTransaction(viewpoint));
  }

  protected abstract Iterator<PersistentRecord> internalGetDependentRecords(PersistentRecord record,
                                                                            int transactionType,
                                                                            Viewpoint viewpoint);
}
