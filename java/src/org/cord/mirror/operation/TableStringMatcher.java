package org.cord.mirror.operation;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.LogicException;

import com.google.common.base.Preconditions;

/**
 * TableOperation that provides a simple interface to search a single field on a Table in a variety
 * of ways for strings.
 */
public class TableStringMatcher
  extends MonoRecordSourceOperation<String, RecordSource>
{
  /**
   * The search term must exactly match the search field for the record to be included. Resolves to
   * "searchField = 'searchTerm'"
   */
  public final static int SEARCH_EXACT = 0;

  /**
   * The search term will match all instances of the search field that are not exactly the same.
   * Resolves to "searchField <> 'searchTerm'"
   */
  public final static int SEARCH_NOTEXACT = 1;

  /**
   * The search term will perform a "LIKE" match between the search field and the search term. LIKE
   * supports the "%" for any number of characters and "_" for any single character. Be careful
   * about escaping these characters as it will not work due to the auto-escaping of escaped
   * characters. This needs fixing.
   */
  public final static int SEARCH_LIKE = 2;

  /**
   * The search term will match any record that fails the LIKE comparison.
   * 
   * @see #SEARCH_LIKE
   */
  public final static int SEARCH_NOTLIKE = 3;

  /**
   * Perform a regular expression match between the search term and the search field. Be careful
   * about escaping characters...
   */
  public final static int SEARCH_REGEXP = 4;

  /**
   * Perform an inverse regular expression match between the search term and the search field. Be
   * careful about escaping characters...
   */
  public final static int SEARCH_NOTREGEXP = 5;

  /**
   * @see #SEARCH_EXACT
   */
  public final static int SEARCH_DEFAULT = SEARCH_EXACT;

  public final static int P0_SEARCHTERM = 0;

  /**
   * SQL terms used for building the search operations. Indexs match up with the SEARCH_* integer
   * constants.
   */
  private final static String[] SEARCH_OPERATORS =
      { " = ", " <> ", " LIKE ", " NOT LIKE ", " REGEXP ", " NOT REGEXP " };

  private final FieldKey<String> _searchField;

  private final int _searchType;

  private final String _orderBy;

  public TableStringMatcher(MonoRecordSourceOperationKey<String, RecordSource> name,
                            FieldKey<String> searchField,
                            int searchType,
                            String orderBy)
  {
    super(name,
          Casters.TOSTRING);
    _searchField = searchField;
    _searchType = searchType;
    _orderBy = orderBy;
  }

  public FieldKey<String> getSearchField()
  {
    return _searchField;
  }

  public int getSearchType()
  {
    return _searchType;
  }

  public String getOrderBy()
  {
    return _orderBy;
  }

  public RecordSource search(Table invokingTable,
                             String searchTerm)
  {
    return search(invokingTable, null, _searchField, searchTerm, _orderBy, _searchType);
  }

  /**
   * Static utility method to generate a search Query for a specific String within a Table.
   * 
   * @param table
   *          The Table within which to search
   * @param queryName
   *          The name that the Query has been precached under (if any). If defined then this may
   *          act as a shortcut for successive invocations, or at least will set the name that the
   *          new Query is cached under. If it is not defined then the Query will be cached under
   *          the filter component, and the optional orderBy components of the query.
   * @param searchField
   *          The name of the field that is to be matched
   * @param searchTerm
   *          The String that searchField is to be compared to
   * @param orderBy
   *          The fields that should be used for ordering the return result, leave to null if
   *          ordering is unspecified
   * @param searchType
   *          The style of search that is to be implemented.
   */
  public static RecordSource search(Table table,
                                    String queryName,
                                    FieldKey<?> searchField,
                                    String searchTerm,
                                    String orderBy,
                                    int searchType)
  {
    Preconditions.checkNotNull(table, "table");
    if (queryName != null) {
      RecordSource cachedQuery = table.getCachedRecordSource(queryName);
      if (cachedQuery != null) {
        return cachedQuery;
      }
    }
    StringBuilder buf =
        new StringBuilder(searchField.getKeyName().length() + SEARCH_OPERATORS[searchType].length()
                          + searchTerm.length() + 2); // 32
    try {
      buf.append(searchField).append(SEARCH_OPERATORS[searchType]);
    } catch (IndexOutOfBoundsException ioobEx) {
      throw new LogicException("Unknown searchType index:" + searchType, ioobEx);
    }
    SqlUtil.toSafeSqlString(buf, searchTerm);
    String filter = buf.toString();
    if (queryName == null) {
      if (orderBy == null) {
        queryName = filter;
      } else {
        buf.append(queryName);
        queryName = buf.toString();
      }
    }
    return table.getQuery(queryName, filter, orderBy);
  }

  /**
   * Search for an exact match of searchTerm in searchField.
   */
  public static RecordSource search(Table table,
                                    FieldKey<?> searchField,
                                    String searchTerm,
                                    String orderBy)
  {
    return search(table, null, searchField, searchTerm, orderBy, SEARCH_DEFAULT);
  }

  /**
   * utility method that matches the given field and term using SEARCH_EXACT and then returns only
   * the first record of the match.
   * 
   * @param ordering
   *          The order that should be applied to the query used to resolve the searchTerm. If you
   *          are confident that there will only be 0 or 1 matches then this parameter is
   *          irrelevant, but if there will be multiple matches then this will determine which one
   *          comes first and is therefore returned by this method. Passing null will use the
   *          default table ordering.
   */
  public static PersistentRecord firstMatch(Table table,
                                            FieldKey<String> searchField,
                                            String searchTerm,
                                            String ordering)
      throws MirrorNoSuchRecordException
  {
    if (searchTerm == null) {
      throw new MirrorNoSuchRecordException("searchTerm was null, hence no matches", table);
    }
    String queryName = "TableStringMatcher.firstMatch(" + searchField + "," + searchTerm + ")";
    return RecordSources.getFirstRecord(search(table,
                                               queryName,
                                               searchField,
                                               searchTerm,
                                               ordering,
                                               SEARCH_EXACT),
                                        null);
  }

  /**
   * Search for the only matching record for a given field value. This should be used in situations
   * when there is only 1 matches as it will throw a LogicException if there is more than 1 because
   * this implies that the db integrity is broken, or MirrorNoSuchRecordException if there are none.
   * 
   * @throws MirrorLogicException
   *           if there is more than one match on the Table.
   * @throws MirrorNoSuchRecordException
   *           If there is no matches on the Table
   * @deprecated until it can be made more consistent in name and exception signature
   */
  @Deprecated
  public static PersistentRecord onlyMatch(Table table,
                                           FieldKey<String> searchField,
                                           String searchTerm,
                                           Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord match = optOnlyMatch(table, searchField, searchTerm, viewpoint);
    if (match == null) {
      throw new MirrorNoSuchRecordException(String.format("%s.%s != '%s'",
                                                          table.getName(),
                                                          searchField,
                                                          searchTerm),
                                            table);
    }
    return match;
  }

  /**
   * @throws MirrorLogicException
   *           If there is more than 1 record matching the search.
   */
  public static PersistentRecord optOnlyMatch(Table table,
                                              FieldKey<String> searchField,
                                              String searchTerm,
                                              Viewpoint viewpoint)
  {
    Preconditions.checkNotNull(table, "table");
    Preconditions.checkNotNull(searchField, "searchField");
    StringBuilder buf = new StringBuilder();
    buf.append(table.getName()).append('.').append(searchField).append('=');
    SqlUtil.toSafeSqlString(buf, searchTerm);
    String filter = buf.toString();
    Query query = table.getQuery(filter, filter, null);
    return RecordSources.getOptOnlyRecord(query, viewpoint, null);
  }

  /**
   * utility method that matches the given field and term using SEARCH_EXACT and then returns only
   * the first record of the match. This method uses the default ordering for table to rank the
   * search query.
   * 
   * @see #firstMatch(Table, FieldKey, String, String)
   */
  public static PersistentRecord firstMatch(Table table,
                                            FieldKey<String> searchField,
                                            String searchTerm)
      throws MirrorNoSuchRecordException
  {
    return firstMatch(table, searchField, searchTerm, null);
  }

  @Override
  public RecordSource calculate(RecordSource recordSource,
                                Viewpoint viewpoint,
                                String a)
  {
    return RecordSources.viewedFrom(search(recordSource.getTable(), a), viewpoint);
  }
}
