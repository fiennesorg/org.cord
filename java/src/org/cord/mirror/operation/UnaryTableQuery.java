package org.cord.mirror.operation;

import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;

/**
 * TableOperation that supports a single parameter that is then inserted into the WHERE clause of
 * the query that is constructed. The explicit call for extracting a UnaryTableQuery from a Table is
 * as follows:- <blockquote> <code>((UnaryTableQuery) Table.get(name)).get(parameter)</code>
 * </blockquote>
 * <p>
 * which condenses in webmacro invocation down to:- <blockquote> <code> $table.name.parameter</code>
 * </blockquote>
 * <p>
 * thereby permitting the generation of easily called parametric queries across single tables.
 * <p>
 * The query that is returned when the <code>get(parameter)</code> command is invoked is of the
 * following format:- <blockquote>SELECT id FROM &lt;targetTable&gt; WHERE
 * "&lt;startWhere&gt;&lt;parameter&gt;&lt;endWhere&gt;" ORDER BY &lt;order&gt;</blockquote>
 */
@Deprecated
public class UnaryTableQuery
  extends AbstractTableOperation
{
  private final Table __targetTable;

  private final String __startWhere;

  private final String __endWhere;

  private final String __order;

  private final String __stringRep;

  public UnaryTableQuery(String name,
                         Table targetTable,
                         String startWhere,
                         String endWhere,
                         String order)
  {
    super(name,
          1);
    __targetTable = targetTable;
    __startWhere = startWhere == null ? "" : startWhere;
    __endWhere = endWhere == null ? "" : endWhere;
    __order = order;
    __stringRep = name + "(" + targetTable + ", \"" + startWhere + "???" + endWhere + "\", \""
                  + order + "\")";
  }

  @Override
  public Object internalGetOperation(Table invokingTable,
                                     Viewpoint viewPoint,
                                     Object... params)
  {
    return __targetTable.getQuery(null, __startWhere + params[0] + __endWhere, __order);
  }

  @Override
  public String toString()
  {
    return __stringRep;
  }
}
