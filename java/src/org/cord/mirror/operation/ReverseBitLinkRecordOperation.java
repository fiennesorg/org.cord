package org.cord.mirror.operation;

import org.cord.mirror.FieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.BitLinkField;

public class ReverseBitLinkRecordOperation
  extends AbstractReverseRecordOperation<Long>
{
  public ReverseBitLinkRecordOperation(Table linkTable,
                                       FieldKey<Long> linkFieldName,
                                       RecordOperationKey<RecordSource> targetOperationName,
                                       String linkOrder,
                                       int linkStyle)
  {
    super(linkTable,
          linkFieldName,
          targetOperationName,
          linkOrder,
          linkStyle,
          null);
  }

  @Override
  protected String buildFilter(TransientRecord record)
  {
    return "(" + getLinkFieldName() + " & " + BitLinkField.getBitMask(record.getId()) + ") != 0";
  }
}
