package org.cord.mirror.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;
import org.cord.util.NumberUtil;
import org.cord.util.StringUtils;

public class FloatAsSterling
  extends MonoRecordOperation<String, String>
{
  /**
   * "floatAsSterling"
   */
  public final static MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("floatAsSterling", String.class, String.class);

  public FloatAsSterling()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  public static String process(TransientRecord record,
                               String field)
  {
    Object value = record.get(field);
    if (value instanceof Number) {
      float floatNumber = ((Number) value).floatValue();
      int storedNumber = (int) (100.0f * floatNumber);
      int poundsValue = storedNumber / 100;
      int penceValue = storedNumber % 100;
      StringBuilder result = new StringBuilder();
      result.append('£').append(poundsValue).append('.').append(NumberUtil.toString(penceValue, 2));
      return result.toString();
    }
    return StringUtils.toString(value);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperationName)
  {
    return process(record, recordOperationName);
  }
}
