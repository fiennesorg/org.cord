package org.cord.mirror.operation;

import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BitLinkField;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.util.Casters;
import org.cord.util.ObjectUtil;

/**
 * ParametricRecordOperation that returns all the possible choices from a given BitLinkFieldFilter
 * as its parameter.
 */
public class AllChoicesFromTable
  extends MonoRecordSourceOperation<String, Iterator<PersistentRecord>>
{
  /**
   * "allChoicesFrom"
   */
  public final static MonoRecordSourceOperationKey<String, Iterator<PersistentRecord>> NAME =
      MonoRecordSourceOperationKey.create("allChoicesFrom",
                                          String.class,
                                          ObjectUtil.<Iterator<PersistentRecord>> castClass(Iterator.class));

  public AllChoicesFromTable()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public Iterator<PersistentRecord> calculate(RecordSource recordSource,
                                              Viewpoint viewpoint,
                                              String a)
  {
    BitLinkField choiceFieldFilter = (BitLinkField) recordSource.getTable().getField(a);
    return choiceFieldFilter.getTargetTable().getRecordList(viewpoint).iterator();
  }
}
