package org.cord.mirror.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.AbstractGettable;
import org.cord.util.Caster;

import com.google.common.base.Preconditions;

/**
 * RecordOperation that takes a single parameter
 * 
 * @author alex
 * @param <A>
 *          The class of the parameter
 * @param <V>
 *          The class of the resulting value
 */
public abstract class MonoRecordOperation<A, V>
  extends AbstractLockableOperation
  implements RecordOperation<MonoRecordOperation<A, V>.Invoker>
{
  private final MonoRecordOperationKey<A, V> __key;
  private final Caster<A> __casterA;

  public MonoRecordOperation(MonoRecordOperationKey<A, V> key,
                             Caster<A> casterA)
  {
    super(key.getKeyName());
    __key = key;
    __casterA = Preconditions.checkNotNull(casterA, "casterA");
  }

  @Override
  public final MonoRecordOperationKey<A, V> getKey()
  {
    return __key;
  }

  @Override
  public final Invoker getOperation(TransientRecord record,
                                    Viewpoint viewpoint)
  {
    return new Invoker(record, viewpoint);
  }

  public abstract V calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              A a);

  @Override
  public void shutdown()
  {
  }

  public class Invoker
    extends AbstractGettable
  {
    private final TransientRecord __record;
    private final Viewpoint __viewpoint;

    protected Invoker(TransientRecord record,
                      Viewpoint viewpoint)
    {
      __record = record;
      __viewpoint = viewpoint;
    }

    public V call(A a)
    {
      return calculate(__record, __viewpoint, a);
    }

    @Override
    public V get(Object key)
    {
      A a;
      try {
        a = __casterA.cast(key);
      } catch (RuntimeException e) {
        throw new IllegalArgumentException(String.format("%s.%s was rejected by %s",
                                                         MonoRecordOperation.this,
                                                         key,
                                                         __casterA),
                                           e);
      }
      return calculate(__record, __viewpoint, a);
    }
  }
}
