package org.cord.mirror.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BitLinkField;
import org.cord.util.Casters;

public class IsSelectedChoiceFrom
  extends DuoRecordOperation<Object, Object, Boolean>
{
  public final static DuoRecordOperationKey<Object, Object, Boolean> NAME =
      DuoRecordOperationKey.create("isSelectedChoiceFrom",
                                   Object.class,
                                   Object.class,
                                   Boolean.class);

  public IsSelectedChoiceFrom()
  {
    super(NAME,
          Casters.NULL,
          Casters.NULL);
  }

  @Override
  public Boolean calculate(TransientRecord record,
                           Viewpoint viewpoint,
                           Object fieldName,
                           Object choiceIndexObj)
  {
    int choiceIndex = 0;
    if (choiceIndexObj instanceof Number) {
      choiceIndex = ((Number) choiceIndexObj).intValue();
    } else if (choiceIndexObj instanceof TransientRecord) {
      choiceIndex = ((TransientRecord) choiceIndexObj).getId();
    } else {
      choiceIndex = Integer.valueOf(choiceIndexObj.toString()).intValue();
    }
    return BitLinkField.isSet(record, fieldName, choiceIndex) ? Boolean.TRUE : Boolean.FALSE;
  }

}
