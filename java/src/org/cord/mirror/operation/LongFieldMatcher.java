package org.cord.mirror.operation;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.cord.mirror.FieldKey;
import org.cord.mirror.Table;

public class LongFieldMatcher
  extends FieldMatcher<Long>
{

  public LongFieldMatcher(Table table,
                          FieldKey<Long> fieldKey)
  {
    super(table,
          fieldKey);
  }

  @Override
  protected void setValue(PreparedStatement preparedStatement,
                          Long value)
      throws SQLException
  {
    preparedStatement.setLong(1, value.longValue());
  }
}
