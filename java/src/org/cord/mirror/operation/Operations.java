package org.cord.mirror.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Casters;

/**
 * Collection of useful Operation implementations that can be utilised as and when necessary.
 */
public class Operations
{
  public static final MonoRecordOperationKey<String, String> SINGLELINE =
      MonoRecordOperationKey.create("singleLine", String.class, String.class);

  /**
   * get a ParametricRecordOperation which resolves its arguments with all whitespace turned into
   * single spaces. This guarantees that it will all be on a single line and will have no tabs and
   * no sequences of spaces longer than a single space.
   */
  public static MonoRecordOperation<String, String> getSingleLine()
  {
    return new MonoRecordOperation<String, String>(SINGLELINE, Casters.TOSTRING) {
      @Override
      public String calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              String recordOperationName)
      {
        return record.getString(recordOperationName).replaceAll("\\s+", " ");
      }
    };
  }

  public static final MonoRecordOperationKey<String, String> ALPHANUMERIC =
      MonoRecordOperationKey.create("alphanumeric", String.class, String.class);

  /**
   * Get a ParametricRecordOperation which resolves its arguments into purely alphanumeric
   * characters. This guarantees that the resulting string will only be composed of [a-zA-Z_0-9]
   */
  public static MonoRecordOperation<String, String> getAlphanumeric()
  {
    return new MonoRecordOperation<String, String>(ALPHANUMERIC, Casters.TOSTRING) {
      @Override
      public String calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              String recordOperationName)
      {
        return record.getString(recordOperationName).replaceAll("\\W", "");
      }
    };
  }
}