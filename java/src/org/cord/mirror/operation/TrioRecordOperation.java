package org.cord.mirror.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TrioRecordOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.util.AbstractGettable;
import org.cord.util.Caster;

import com.google.common.base.Preconditions;

/**
 * RecordOperation that takes 3 parameters
 * 
 * @author alex
 * @param <A>
 *          The class of the first parameter
 * @param <B>
 *          The class of the second parameter
 * @param <C>
 *          The class of the third parameter
 * @param <V>
 *          The class of the resulting value
 */
public abstract class TrioRecordOperation<A, B, C, V>
  extends AbstractLockableOperation
  implements RecordOperation<TrioRecordOperation<A, B, C, V>.Invoker>
{
  private final TrioRecordOperationKey<A, B, C, V> __key;
  private final Caster<A> __casterA;
  private final Caster<B> __casterB;
  private final Caster<C> __casterC;

  public TrioRecordOperation(TrioRecordOperationKey<A, B, C, V> key,
                             Caster<A> casterA,
                             Caster<B> casterB,
                             Caster<C> casterC)
  {
    super(key.getKeyName());
    __key = key;
    __casterA = Preconditions.checkNotNull(casterA, "casterA");
    __casterB = Preconditions.checkNotNull(casterB, "casterB");
    __casterC = Preconditions.checkNotNull(casterC, "casterC");
  }

  @Override
  public TrioRecordOperationKey<A, B, C, V> getKey()
  {
    return __key;
  }

  @Override
  public final Invoker getOperation(TransientRecord record,
                                    Viewpoint viewpoint)
  {
    return new Invoker(record, viewpoint);
  }

  public abstract V calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              A a,
                              B b,
                              C c);

  @Override
  public void shutdown()
  {
  }

  public class Invoker
    extends AbstractGettable
  {
    private final TransientRecord __record;
    private final Viewpoint __viewpoint;

    private Invoker(TransientRecord record,
                    Viewpoint viewpoint)
    {
      __record = record;
      __viewpoint = viewpoint;
    }

    public V call(A a,
                  B b,
                  C c)
    {
      return calculate(__record, __viewpoint, a, b, c);
    }

    @Override
    public Second get(Object a)
    {
      return new Second(__casterA.cast(a));
    }

    public class Second
      extends AbstractGettable
    {
      private final A __a;

      private Second(A a)
      {
        __a = a;
      }

      @Override
      public Third get(Object b)
      {
        return new Third(__casterB.cast(b));
      }

      public class Third
        extends AbstractGettable
      {
        private final B __b;

        private Third(B b)
        {
          __b = b;
        }

        @Override
        public V get(Object key)
        {
          return calculate(__record, __viewpoint, __a, __b, __casterC.cast(key));
        }
      }
    }

  }
}
