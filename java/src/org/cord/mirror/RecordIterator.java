package org.cord.mirror;

import java.util.Iterator;

/**
 * Wrapper class around a Iterator of record ids that transforms it into a Iterator of Records.
 * 
 * @deprecated in preference of a RecordSource based operation
 */
@Deprecated
public final class RecordIterator
  implements Iterator<PersistentRecord>
{
  private final Table __table;

  private final Iterator<Integer> __ids;

  private final Viewpoint __viewpoint;

  /**
   * Create a new non-transactional viewpoint RecordIterator.
   * 
   * @param table
   *          The Table from which the Records should be taken.
   * @param ids
   *          The ListIterator containing the id values as Integers that need to be converted into
   *          Record objects.
   */
  public RecordIterator(Table table,
                        Iterator<Integer> ids)
  {
    this(table,
         ids,
         null);
  }

  /**
   * Create a new transactional viewpoint RecordIterator.
   * 
   * @param table
   *          The Table from which the Records should be taken.
   * @param ids
   *          The ListIterator containing the id values as Integers that need to be converted into
   *          Record objects.
   * @param viewpoint
   *          The transaction whos viewpoint is to be used by this list iterator. If this is not
   *          null, then any updated records in the query will have their writable state returned
   *          rather than their persistent state.
   */
  public RecordIterator(Table table,
                        Iterator<Integer> ids,
                        Viewpoint viewpoint)
  {
    __table = table;
    __ids = ids;
    __viewpoint = viewpoint;
  }

  @Override
  public boolean hasNext()
  {
    return __ids.hasNext();
  }

  /**
   * @throws MissingRecordException
   *           if the id doesn't map onto a valid record.
   */
  @Override
  public PersistentRecord next()
  {
    Integer id = null;
    try {
      id = __ids.next();
      return __table.getRecord(id, __viewpoint);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new MissingRecordException(null, __table, id, mnsrEx);
    }
  }

  @Override
  public void remove()
  {
    __ids.remove();
  }
}
