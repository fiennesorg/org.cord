package org.cord.mirror;

import org.cord.util.SimpleGettable;

/**
 * Extended version of the Gettable interface for describing Objects that are capable of resolving
 * requests from the viewpoint of a given Transaction.
 */
public interface TransactionGettable
  extends SimpleGettable
{
  public Object get(Object key,
                    Viewpoint viewpoint);
}