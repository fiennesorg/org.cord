package org.cord.mirror;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.cord.mirror.field.StringField;

public class StringSubstitutionProcessor
  implements DbProcessor
{
  private final Map<Pattern, String> __regexReplacements = new HashMap<Pattern, String>();

  private final Set<String> __skippedTables = new HashSet<String>();

  private final Set<String> __skippedFields = new HashSet<String>();

  private final Map<String, Set<String>> __targetFields = new HashMap<String, Set<String>>();

  public StringSubstitutionProcessor()
  {
    super();
  }

  public void addSkippedTable(String tableName)
  {
    __skippedTables.add(tableName);
  }

  private String getSkippedFieldKey(String tableName,
                                    String fieldName)
  {
    return tableName + "." + fieldName;
  }

  public void addSkippedField(String tableName,
                              String fieldName)
  {
    __skippedFields.add(getSkippedFieldKey(tableName, fieldName));
  }

  public void addSubstitution(String regex,
                              String replacement)
  {
    __regexReplacements.put(Pattern.compile(regex), replacement);
  }

  @Override
  public boolean shouldProcess(Table table)
  {
    if (table == null) {
      return false;
    }
    String tableName = table.getName();
    if (__skippedTables.contains(tableName)) {
      return false;
    }
    Set<String> targetFields = new HashSet<String>();
    for (Field<?> field : table.getFields()) {
      if (field instanceof StringField) {
        String fieldName = field.getName();
        if (!__skippedFields.contains(getSkippedFieldKey(tableName, fieldName))) {
          targetFields.add(fieldName);
        }
      }
    }
    __targetFields.put(tableName, targetFields);
    return targetFields.size() > 0;
  }

  @Override
  public void process(WritableRecord record)
      throws MirrorFieldException
  {
    Table table = record.getTable();
    Set<String> targetFields = __targetFields.get(table.getName());
    boolean isTitle = table.getName().equals("TitleInstance");
    Iterator<String> i = targetFields.iterator();
    while (i.hasNext()) {
      String fieldName = i.next();
      String oldVal = record.getString(fieldName);
      String newVal = oldVal;
      Iterator<Map.Entry<Pattern, String>> j = __regexReplacements.entrySet().iterator();
      while (j.hasNext()) {
        Map.Entry<Pattern, String> entry = j.next();
        // newVal = newVal.replaceAll((String) entry.getKey(), (String)
        // entry.getValue());
        newVal = entry.getKey().matcher(newVal).replaceAll(entry.getValue());
      }
      if (isTitle && fieldName.equals("subTitle")) {
        // System.out.println(record + ":" + oldVal + " --> " + newVal);
      }
      if (!oldVal.equals(newVal)) {
        record.setField(fieldName, newVal);
        System.out.println(record + "." + fieldName);
      }
    }
  }
}
