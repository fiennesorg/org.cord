package org.cord.mirror;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.cord.util.Shutdownable;

/**
 * StateTrigger is used to describe external triggers that are triggered during the lifecycle of a
 * Transaction. They are designed to manipulate external items rather than the structure of the
 * database. Database related triggers should be implemented by a combination of Dependencies
 * implementing RecordOperations and FieldTriggers. Instead, the StateTrigger operation is a
 * non-blocking operation. If it fails then the Exception will be caught and inserted into a
 * MirrorNestedException, but it is not necessary for the invoking class to catch the exception.
 * <p>
 * The points where it is possible to register a StateTrigger are as follows:-
 * <ul>
 * <li>From Persistent to Writable
 * <li>From Writable to cancelled
 * <li>From Writable to updated
 * <li>From Persistent to PreDeleted
 * <li>From PreDeleted to cancelled
 * <li>From PreDeleted to deleted.
 * </ul>
 * Any particular StateTrigger can be registered for any one or more of these operations, leading to
 * a very flexible system.
 * 
 * @see Table#addStateTrigger(StateTrigger,int)
 */
public interface StateTrigger
  extends Shutdownable
{
  /**
   * Constant to define a DefaultStateTrigger for a StateTrigger that has no default.
   */
  public final static int STATE_UNDEFINED = -1;

  /**
   * State trigger that will be invoked on the PersistentRecord at the point when it is locked for
   * updating.
   */
  public final static int STATE_PERSISTENT_WRITABLE = 0;

  /**
   * State trigger that will be invoked on the PersistentRecord when the Transaction that currently
   * holds the update lock on the record is going to be cancelled. At this point, the data in the
   * WritableRecord should still be available.
   */
  public final static int STATE_WRITABLE_CANCELLED = 1;

  /**
   * State trigger that is invoked on the PersistentRecord that held the update lock on a
   * WriteableRecord immediately after the records have been updated and the appropriate
   * TableListeners notified. At this point, the PersistentRecord will contain the new values that
   * have been synced to the db and the WriteableRecord will have disabled.
   */
  public final static int STATE_WRITABLE_UPDATED = 2;

  /**
   * State trigger that is invoked immediately after the delete lock has been obtained on a
   * PersistentRecord during the invocation of the Transaction.delete(...) method.
   */
  public final static int STATE_PERSISTENT_PREDELETED = 3;

  /**
   * State trigger that will be invoked on the PersistentRecord when the Transaction that currently
   * holds the delete lock on the record is going to be cancelled.
   */
  public final static int STATE_PREDELETED_CANCELLED = 4;

  /**
   * State trigger that is invoked on the PersistentRecord that held the delete lock on itself
   * immediately after the database record has been removed and the appropriate TableListeners have
   * been invoked.
   */
  public final static int STATE_PREDELETED_DELETED = 5;

  /**
   * State trigger that is invoked on the PersistentRecord that holds the Update lock on a
   * WritableRecord immediately before the fields are updated. At the point of invocation, the
   * PersistentRecord will still contain the original database values and the WriteableRecord will
   * contain the updated values.
   */
  public final static int STATE_WRITABLE_PREUPDATE = 6;

  // /**
  // * State trigger that is invoked on the PersistentRecord that holds
  // * the delete lock on itself immediately before the persistent
  // * database record is removed. All data in the PersistentRecord
  // * should still be intact and in sync with the db.
  // **/
  // public final static int STATE_PREDELETED_PREDELETE = 7;
  /**
   * State trigger that is committed on all records that have been involved in a Transaction once
   * the Transaction has been committed to the SQL database. There should be no Table locks on the
   * Db at this point so it is safe to perform queries in the scope of this Transaction.
   */
  public final static int STATE_TRANSACTION_COMMITTED = 8;

  /**
   * State trigger that is committed on all records that were involved in a Transaction that has
   * been cancelled.
   */
  public final static int STATE_TRANSACTION_CANCELLED = 9;

  public final static List<String> STATES =
      Collections.unmodifiableList(Arrays.asList(new String[] { "Persistent-->Writable",
          "Writable-->Cancelled", "Writable-->Updated", "Persistent-->PreDeleted",
          "PreDeleted-->Cancelled", "PreDeleted-->Deleted", "Writable-->preUpdate",
          // "PreDeleted-->preDelete",
          "<disabled>", "Transaction-->committed", "Transaction-->cancelled" }));

  public final static int STATE_COUNT = STATES.size();

  /**
   * Invoke the operation of the StateTrigger from the given record.
   * 
   * @param record
   *          The record whos state is changing
   * @param transaction
   *          The Transaction that is responsible for this current change in state.
   * @param state
   *          The constant that represents the state change that has just taken place.
   * @throws Exception
   *           If an error occurs. Note that this will not block the system in any way, and all
   *           other StateTriggers will get invoked and then the program will continue.
   */
  public void trigger(PersistentRecord record,
                      Transaction transaction,
                      int state)
      throws Exception;

  /**
   * Get the state index that this StateTrigger should be registered under.
   * 
   * @return state value from StateTriggerConstants
   */
  public int getDefaultState();

  /**
   * Is this StateTrigger registerable under other States than the one given by getDefaultState()?
   * 
   * @return true if this StateTrigger can be registered on any State transition.
   */
  public boolean isOverridableState();

  /**
   * Get the name of the Table that this StateTrigger would like to be registered on.
   * 
   * @return The targetted Table name or null if the StateTrigger is Table agnostic and is prepared
   *         to be registered on any Table.
   */
  public String getTargetTableName();
}
