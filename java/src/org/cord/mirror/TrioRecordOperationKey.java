package org.cord.mirror;

import org.cord.mirror.operation.TrioRecordOperation;
import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

public class TrioRecordOperationKey<A, B, C, V>
  extends RecordOperationKey<TrioRecordOperation<A, B, C, V>.Invoker>
{
  public static final <A, B, C, V> TrioRecordOperationKey<A, B, C, V> create(String keyName,
                                                                             Class<A> classA,
                                                                             Class<B> classB,
                                                                             Class<C> classC,
                                                                             Class<V> classV)
  {
    return new TrioRecordOperationKey<A, B, C, V>(keyName, classA, classB, classC, classV);
  }

  private final Class<A> __classA;
  private final Class<B> __classB;
  private final Class<C> __classC;
  private final Class<V> __classV;

  public TrioRecordOperationKey(String keyName,
                                Class<A> classA,
                                Class<B> classB,
                                Class<C> classC,
                                Class<V> classV)
  {
    super(keyName,
          ObjectUtil.<TrioRecordOperation<A, B, C, V>
                    .Invoker> castClass(TrioRecordOperation.Invoker.class));
    __classA = Preconditions.checkNotNull(classA, "classA");
    __classB = Preconditions.checkNotNull(classB, "classB");
    __classC = Preconditions.checkNotNull(classC, "classC");
    __classV = Preconditions.checkNotNull(classV, "classV");
  }

  public final Class<A> getClassA()
  {
    return __classA;
  }

  public final Class<B> getClassB()
  {
    return __classB;
  }

  public final Class<C> getClassC()
  {
    return __classC;
  }

  public final Class<V> getClassV()
  {
    return __classV;
  }

  @Override
  public TrioRecordOperationKey<A, B, C, V> copy()
  {
    return new TrioRecordOperationKey<A, B, C, V>(getKeyName(),
                                                  getClassA(),
                                                  getClassB(),
                                                  getClassC(),
                                                  getClassV());
  }
}
