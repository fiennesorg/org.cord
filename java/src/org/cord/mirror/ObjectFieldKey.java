package org.cord.mirror;

public class ObjectFieldKey<T>
  extends FieldKey<T>
{
  public static <V> ObjectFieldKey<V> create(String keyName,
                                             Class<V> valueClass)
  {
    return new ObjectFieldKey<V>(keyName, valueClass);
  }

  public ObjectFieldKey(String keyName,
                        Class<T> valueClass)
  {
    super(keyName,
          valueClass);
  }

  @Override
  public ObjectFieldKey<T> copy()
  {
    return new ObjectFieldKey<T>(getKeyName(), getValueClass());
  }

  private static final int OBJECTVALUEINDEX_INIT = -1;

  private int _objectValueIndex = OBJECTVALUEINDEX_INIT;

  @Override
  protected void register(Field<T> field)
  {
    super.register(field);
    _objectValueIndex = ((ObjectField<?>) field).getObjectValueIndex();
  }

  @Override
  public T call(TransientRecord record,
                Viewpoint viewpoint)
  {
    Object object;
    try {
      object = record.getObjectValues()[_objectValueIndex];
    } catch (ArrayIndexOutOfBoundsException e) {
      if (_objectValueIndex != OBJECTVALUEINDEX_INIT) {
        System.err.println(String.format("%s index of %s failed on %s.%s - re-registering...",
                                         this,
                                         Integer.toString(_objectValueIndex),
                                         record.getTableName(),
                                         Integer.toString(record.getId())));
      }
      register(record.getTable().getField(this));
      return call(record, viewpoint);
    }
    if (object == null) {
      return null;
    }
    try {
      return getValueClass().cast(object);
    } catch (ClassCastException e) {
      ClassCastException wrapper =
          new ClassCastException(String.format("Invoking %s.%s on %s returns %s of class %s rather than %s",
                                               record.getTable().getName(),
                                               getKeyName(),
                                               record,
                                               object,
                                               object.getClass(),
                                               getValueClass()));
      wrapper.initCause(e);
      throw wrapper;
    }
  }

}
