package org.cord.mirror;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import org.cord.mirror.recordsource.AbstractRecordOrdering;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.CompoundRecordSource;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.mirror.recordsource.SingletonIdList;
import org.cord.sql.ConnectionFacade;
import org.cord.sql.ConnectionPool;
import org.cord.util.DebugConstants;
import org.cord.util.LogicException;
import org.cord.util.SimpleGettable;
import org.cord.util.StringUtils;

import com.google.common.base.Strings;

/**
 * Query represents a collection of Records from a particular Table that match the given filter.
 * Queries are initialised containing the information about the filter that they represent, and then
 * they will extract the information about which Records match this filter on demand.
 * <p>
 */
public final class Query
  extends CompoundRecordSource
  implements SimpleGettable, TransactionGettable
{
  public final static int STATE_ERROR = -1;

  public final static int STATE_NEEDSREBUILDING = 0;

  public final static int STATE_ISREBUILDING = 1;

  public final static int STATE_ISREBUILT = 2;

  public final static int STATE_INIT = 3;

  private String _sqlQuery;

  /**
   * The hashCode of the Query is derived from the SQL representation of the Query.
   */
  private int _hashCode;

  // the following elements make up the definition of the components of the
  // Query...
  private final String __filter;

  private final String __order;

  private final String __groupBy;

  private final String __explicitFrom;

  private final int __limit;

  private volatile long _updateTimestamp = 0;

  private long _lastRebuildDurationNs = -1;

  // TODO: work out what is happening with _resultListCount
  private int _resultListCount = 0;

  private final Object __cacheKey;

  public long getLastRebuildDurationNs()
  {
    return _lastRebuildDurationNs;
  }

  public long getLastRebuildTimestamp()
  {
    return _updateTimestamp;
  }

  /**
   * The ConstantQueryTransform is assumed to be a boolean OR with any existing filters. This will
   * generate a replacement filter of:- <blockquote> existingFilter OR (filterExtension)
   * </blockquote>
   */
  public final static int TYPE_OR = 2;

  /**
   * The ConstantQueryTransform is assumed to be a boolean AND with any existing filters. This will
   * generate a replacements filter of:- <blockquote> existingFilter AND (filterExtension)
   * </blockquote>
   */
  public final static int TYPE_AND = 1;

  /**
   * The ConstantQueryTransform is assumed to have an explicit filterExtension which will be
   * appended directly onto the filter of the base query. It is then the responsibility of the
   * invokee to ensure that any whitespace and / or bracketing is taken care of.
   */
  public final static int TYPE_EXPLICIT = 0;

  public final static int TYPE_ORDEREDBY = -1;

  /**
   * The String that if utilised for an order param will result in an unspecified SQL ordering being
   * utilised. Note that this is different from using NULL which will result in the
   * {@link Table#getDefaultOrdering()} being utilised.
   */
  public static final String NOSQLORDERING = "§";

  /**
   * @param order
   *          The SQL Ordering to utilise. Passing in null will get
   *          {@link Table#getDefaultOrdering()} and utilising {@link #NOSQLORDERING} will
   *          explicitly not utilise any ordering.
   */
  protected Query(Object cacheKey,
                  Table targetTable,
                  String filter,
                  String order,
                  String groupBy,
                  int limit,
                  String explicitFrom,
                  Table... joinTables)
  {
    super(targetTable,
          null);
    __filter = filterNullStrings(filter);
    order = filterNullStrings(order);
    __order = order == null
        ? targetTable.getDefaultOrdering()
        : NOSQLORDERING.equals(order) ? null : order;
    __groupBy = groupBy;
    __limit = limit;
    __explicitFrom = explicitFrom;
    addRecordSource(targetTable);
    for (Table joinTable : joinTables) {
      if (joinTable == null) {
        break;
      }
      addRecordSource(joinTable);
    }
    if (cacheKey == null) {
      __cacheKey = getSqlQuery();
    } else {
      __cacheKey = cacheKey;
    }
  }

  private final static Table[] __emptyTableArray = new Table[0];

  /**
   * An array representation of the referenced Tables. This is useful for invoking varargs methods
   * that want the join tables.
   * 
   * @return A copy of the RecordSources (in this case Tables) that are contained within this Query
   * @see CompoundRecordSource#getRecordSources()
   */
  public Table[] getTableArray()
  {
    return getRecordSources().toArray(__emptyTableArray);
  }

  /**
   * @return the number of Tables that this SimpleTableListener will listen to.
   */
  public final int getTableCount()
  {
    return getRecordSources().size();
  }

  /**
   * @deprecated I haven't decided exactly what should be used instead of this yet, I need to look
   *             at the invocation patterns more, but I suspect that it will be involving
   *             RecordSourceIntersection
   * @see org.cord.mirror.recordsource.RecordSourceIntersection
   */
  @Deprecated
  public IdList intersection(Query query)
      throws IllegalArgumentException
  {
    if (!this.getTable().equals(query.getTable())) {
      throw new IllegalArgumentException(this + ".union(" + query
                                         + ") are against different Tables");
    }
    if (!this.getOrder().equals(query.getOrder())) {
      throw new IllegalArgumentException(this + ".union(" + query
                                         + ") have different order clauses");
    }
    return intersection(this.getIdList(), query.getIdList());
  }

  /**
   * @deprecated I haven't decided exactly what should be used instead of this yet, I need to look
   *             at the invocation patterns more, but I suspect that it will be involving
   *             RecordSourceIntersection
   * @see org.cord.mirror.recordsource.RecordSourceIntersection
   */
  @Deprecated
  public IdList intersection(IdList idList)
  {
    return intersection(this.getIdList(), idList);
  }

  /**
   * @deprecated I haven't decided exactly what should be used instead of this yet, I need to look
   *             at the invocation patterns more, but I suspect that it will be involving
   *             RecordSourceIntersection. Although we might want an IdLists utility class.
   * @see org.cord.mirror.recordsource.RecordSourceIntersection
   */
  @Deprecated
  private IdList intersection(IdList list1,
                              IdList list2)
  {
    IdList shortList = list1.size() < list2.size() ? list1 : list2;
    IdList longList = list1.size() < list2.size() ? list2 : list1;
    IdList resultList = new ArrayIdList(shortList.getTable());
    for (int shortIndex = 0; shortIndex < shortList.size(); shortIndex++) {
      int shortId = shortList.getInt(shortIndex);
      int longIndexSearch = longList.indexOf(shortId);
      if (longIndexSearch != -1) {
        resultList.add(shortId);
      }
    }
    return resultList;
  }

  public final String getExplicitFrom()
  {
    return __explicitFrom;
  }

  /**
   * Get the record limit for this Query or 0 if there is no limit.
   */
  public final int getLimit()
  {
    return __limit;
  }

  public final String getGroupBy()
  {
    return __groupBy;
  }

  /**
   * Return the filter component of this Query.
   */
  public final String getFilter()
  {
    return __filter;
  }

  /**
   * Return the ordering component of this Query.
   * 
   * @return The ordering component of the Query - this should never be null.
   */
  public final String getOrder()
  {
    return __order;
  }

  /**
   * Try to create an ordering in java space that matches that in sql space; if that can't be done,
   * order by id in java space and by the specified sql ordering in sql space
   */
  @SuppressWarnings("unused")
  @Override
  public RecordOrdering getRecordOrdering()
  {
    if (_recordOrdering == null) {
      try {
        _recordOrdering =
            RecordSourceOrdering.createRecordOrdering(getTable().getName(), __order, __order);
      } catch (IllegalArgumentException ex) {
        String name = String.format("SQL:%s, Java:FALLBACK (sorted by id)", __order);
        _recordOrdering =
            new AbstractRecordOrdering(name, name, __order, FieldValueComparator.BY_ID);
        if (DebugConstants.DEBUG_QUERY_GETRECORDORDERING_DEFAULT) {
          DebugConstants.DEBUG_OUT.println(String.format("%s.getRecordOrdering() --> %s",
                                                         this,
                                                         _recordOrdering));
        }
      }
      if (DebugConstants.DEBUG_QUERY_GETRECORDORDERING
          && !DebugConstants.DEBUG_QUERY_GETRECORDORDERING_DEFAULT) {
        DebugConstants.DEBUG_OUT.println(String.format("%s.getRecordOrdering() --> %s",
                                                       this,
                                                       _recordOrdering));
      }
    }
    return _recordOrdering;
  }

  private RecordOrdering _recordOrdering;

  @Override
  public String toString()
  {
    StringBuilder result = new StringBuilder();
    result.append("Query(").append(getSqlQuery()).append(")");
    return result.toString();
  }

  private String filterNullStrings(String source)
  {
    if (source != null) {
      String result = source.trim();
      if (result.length() > 0) {
        return result;
      }
    }
    return null;
  }

  /**
   * @return The size if the Query has been resolved, or -1 if it has not yet been run.
   */
  public final int getSizeIfKnown()
  {
    return isBuilt() ? getIdList().size() : -1;
  }

  @Override
  protected IdList buildIdList(Viewpoint viewpoint)
  {
    return getIdsFromDb(false);
  }

  private IdList createIdList(ResultSet resultSet)
      throws SQLException
  {
    if (!resultSet.next()) {
      return getTable().getEmptyIds();
    }
    int firstId = readId(resultSet, true);
    if (!resultSet.next()) {
      return new SingletonIdList(getTable(), firstId);
    }
    IdList cachedIdList = getCachedIdList();
    IdList idList = cachedIdList == null
        ? new ArrayIdList(getTable())
        : new ArrayIdList(getTable(), cachedIdList.size());
    idList.add(firstId);
    idList.add(readId(resultSet, true));
    int count = 2;
    while (resultSet.next()) {
      idList.add(readId(resultSet, count++ < getTable().getPrecacheSize()));
    }
    return idList;
  }

  private int readId(ResultSet resultSet,
                     boolean cacheRecord)
      throws SQLException
  {
    int id = resultSet.getInt(1);
    if (cacheRecord) {
      getTable().preCacheRecord(id, resultSet);
    }
    return id;
  }

  private final long MS_NS = 1000000;
  private final long SLOWQUERY = 100 * MS_NS;

  private IdList getIdsFromDb(boolean isSecondAttempt)
  {
    ConnectionFacade connectionFacade = getTable().getDb().getThreadConnectionFacade().get();
    Connection connection = null;
    long startTimeNs = System.nanoTime();
    try {
      connection = connectionFacade.checkOutConnection();
      Statement statement = null;
      try {
        statement = connection.createStatement();
        long createStatementNs = System.nanoTime();
        String sqlQuery = getSqlQuery();
        ResultSet resultSet = statement.executeQuery(sqlQuery);
        long executeStatementNs = System.nanoTime();
        _updateTimestamp = System.currentTimeMillis();
        IdList ids = createIdList(resultSet);
        long readDataNs = System.nanoTime();
        _lastRebuildDurationNs = readDataNs - startTimeNs;
        getTable().updateQueryStats(_lastRebuildDurationNs / MS_NS);
        if (DebugConstants.DEBUG_SQL) {
          DebugConstants.DEBUG_OUT.println(ConnectionPool.generateDebugString(connection,
                                                                              String.format("%s --> %s records, %s ns",
                                                                                            sqlQuery,
                                                                                            Integer.toString(ids.size()),
                                                                                            Long.toString(_lastRebuildDurationNs))));
        }
        if (_lastRebuildDurationNs > SLOWQUERY) {
          DebugConstants.DEBUG_OUT.println(String.format("SLOW: C:%.2fms, E:%.2fms, R:%.2fms, x%s, %s",
                                                         Double.valueOf(((createStatementNs
                                                                          - startTimeNs)
                                                                         / 1000000.0)),
                                                         Double.valueOf(((executeStatementNs
                                                                          - createStatementNs)
                                                                         / 1000000.0)),
                                                         Double.valueOf(((readDataNs
                                                                          - executeStatementNs)
                                                                         / 1000000.0)),
                                                         Integer.valueOf(ids.size()),
                                                         sqlQuery));
        }
        return ids;
      } finally {
        if (statement != null) {
          statement.close();
        }
      }
    } catch (SQLException sqlEx) {
      DebugConstants.DEBUG_OUT.println(String.format("%s.getIdsFromDb(%s) --> %s",
                                                     this,
                                                     Boolean.toString(isSecondAttempt),
                                                     sqlEx));
      connectionFacade.checkInBrokenConnection(connection);
      connection = null;
      if (isSecondAttempt) {
        throw new LogicException(this + " caused " + sqlEx, sqlEx);
      } else {
        return getIdsFromDb(true);
      }
    } finally {
      connectionFacade.checkInWorkingConnection(connection);
    }
  }
  public int getResultListCount()
  {
    return _resultListCount;
  }

  public String getSqlQuery()
  {
    if (_sqlQuery != null) {
      return _sqlQuery;
    }
    _sqlQuery = constructSqlQuery(false, getLimit(), -1);
    _hashCode = _sqlQuery.hashCode();
    return _sqlQuery;
  }

  /**
   * Construct the SQL string that is used to resolve this Query. No caching is performed by this
   * method and the SQL will always be constructed from scratch. It is therefore a relatively
   * expensive operation. The state of the Query object is unchanged by this operation.
   * 
   * @param isCount
   *          If true then the SQL that is returned will be of the form "select count(xyz) from
   *          ...", otherwise it will be of the form "select xyz from ...".
   * @return the SqlQuery.
   */
  public String constructSqlQuery(boolean isCount,
                                  int rowCount,
                                  int offset)
  {
    int length = 16 + getTable().getName().length() + getTableCount() * 11
                 + (__filter != null ? 7 + __filter.length() : 0)
                 + (__order != null ? 10 + __order.length() : 0)
                 + (__groupBy != null ? 10 + __groupBy.length() : 0) + (rowCount > 0 ? 10 : 0)
                 + (offset > 0 ? 10 : 0);
    StringBuilder sqlQuery = new StringBuilder(length);
    sqlQuery.append("select ");
    if (isCount) {
      sqlQuery.append("count(");
    }
    sqlQuery.append(getTable().getName()).append('.').append("*");
    if (isCount) {
      sqlQuery.append(')');
    }
    sqlQuery.append(" from ");
    if (__explicitFrom == null) {
      for (int i = getTableCount(); i-- > 0;) {
        sqlQuery.append(((Table) getRecordSources().get(i)).getName());
        if (i > 0) {
          sqlQuery.append(",");
        }
      }
    } else {
      sqlQuery.append(__explicitFrom);
    }
    if (__filter != null) {
      sqlQuery.append(" where ").append(__filter);
    }
    if (__groupBy != null) {
      sqlQuery.append(" group by ").append(__groupBy);
    }
    if (__order != null && __order.length() != 0) {
      sqlQuery.append(" order by ").append(__order);
    }
    if (rowCount > 0) {
      sqlQuery.append(" limit ").append(rowCount);
      if (offset > 0) {
        sqlQuery.append(" offset ").append(offset);
      }
    }
    return sqlQuery.toString();
  }

  /**
   * Check to see whether or not the Query has yet had its SQL representation initialised. Checking
   * this status does not change the state of the Query in any fashion.
   * 
   * @return true if the SQL representation of this Query has been created.
   */
  public boolean hasSqlQuery()
  {
    return _sqlQuery != null;
  }

  @Override
  public final boolean equals(Object object)
  {
    try {
      return ((Query) object).getSqlQuery().equals(this.getSqlQuery());
    } catch (ClassCastException ccEx) {
      return false;
    }
  }

  @Override
  public final int hashCode()
  {
    if (_sqlQuery == null) {
      getSqlQuery();
    }
    return _hashCode;
  }

  /**
   * Get an unmodifiable list of keys that may be used to resolve RecordSourceOperation elements.
   * 
   * @see #getTable()
   * @see Table#getRecordSourceOperations()
   */
  public Collection<?> getPublicKeys()
  {
    return getTable().getRecordSourceOperations().keySet();
  }

  /**
   * Utility method to transform a Collection of Tables into a Table[] which can then be used as a
   * varargs variable to query creation methods.
   * 
   * @return The Table[] containing the contents of tables or a Table[] of length 0 if tables was
   *         either null or empty
   */
  public final static Table[] toArray(Collection<Table> tables)
  {
    return tables == null || tables.size() == 0
        ? __emptyTableArray
        : tables.toArray(__emptyTableArray);
  }

  /**
   * Utility method to transform the Tables contained in a given Query plus an arbitrary number of
   * additional Tables into a Table[]. This is useful for generating a varargs variable when you are
   * extending the scope of an existing Query.
   */
  public final static Table[] toArray(Query tableSource,
                                      Table... joinTables)
  {
    ArrayList<Table> tables = new ArrayList<Table>();
    if (tableSource != null) {
      for (Table table : tableSource.getTableArray()) {
        if (table == null) {
          break;
        }
        tables.add(table);
      }
    }
    if (joinTables != null) {
      for (Table table : joinTables) {
        tables.add(table);
      }
    }
    return toArray(tables);
  }

  /**
   * @return false because SQL databases don't know anything about java transactions and therefore
   *         will always give the same result.
   */
  @Override
  public boolean shouldRebuild(Viewpoint viewpoint)
  {
    return false;
  }

  @Override
  public boolean shouldReorder(Viewpoint viewpoint)
  {
    return viewpoint.isUpdatedTable(getTable());
  }

  @Override
  public Object getCacheKey()
  {
    return __cacheKey;
  }

  /**
   * @deprecated Don't have a replacement just yet, but note that one will be found in the future to
   *             make it more RecordSource friendly.
   */
  @Deprecated
  public static Query transformQuery(Query baseQuery,
                                     String filterExtension,
                                     int filterType,
                                     String replacementOrder,
                                     String groupBy,
                                     int limit,
                                     String explicitFrom,
                                     Table... joinTables)
  {
    String filter = transformFilter(baseQuery, filterExtension, filterType);
    String order = StringUtils.padEmpty(replacementOrder, baseQuery.getOrder());
    return baseQuery.getTable().getQuery(null,
                                         filter,
                                         order,
                                         groupBy,
                                         limit,
                                         explicitFrom,
                                         toArray(baseQuery, joinTables));
  }

  public static String transformFilter(Query baseQuery,
                                       String filterExtension,
                                       int filterType)
  {
    String filter = baseQuery.getFilter();
    if (Strings.isNullOrEmpty(filter)) {
      return filterExtension;
    }
    if (Strings.isNullOrEmpty(filterExtension)) {
      return filter;
    }
    StringBuilder result = new StringBuilder(filter.length() + filterExtension.length() + 7);
    result.append(filter);
    switch (filterType) {
      case Query.TYPE_ORDEREDBY:
        break;
      case Query.TYPE_EXPLICIT:
        result.append(filterExtension);
        break;
      case Query.TYPE_AND:
        result.append(" and (").append(filterExtension).append(')');
        break;
      case Query.TYPE_OR:
        result.append(" or (").append(filterExtension).append(')');
        break;
      default:
        throw new IllegalArgumentException("Unknown filterType: " + filterType);
    }
    return result.toString();
  }

  /**
   * Static utility method that exposes the functionality of the ConstantQueryTransform to 3rd party
   * classes. The new query that is generated uses the basic table.getQuery method which therefore
   * utilises the default values for distinct, group by and limit.
   * 
   * @param baseQuery
   *          The Query on which the transform should be invoked.
   * @param filterExtension
   *          The additional terms that will be included into the filter of the transformed query.
   *          If this is null then the filter will be unchanged.
   * @param filterType
   *          The way in which the filter will be added to the existing query. If this is TYPE_AND
   *          or TYPE_OR then the appropriate boolean operator will be added and then
   *          filterExtension will be appended, automatically wrapped in brackets so it is enough to
   *          ensure that the filterExtension is logically coherent within itself and the transform
   *          will take care of the larger logical integration. If the filterType is TYPE_EXPLICIT
   *          then it is assumed that filterExtension contains any joining logic necessary and it is
   *          directly appended to the filter component.
   * @param replacementOrder
   *          The ordering that should be applied to the resulting transformed query. If this is
   *          null then the ordering will be preserved from the original Query.
   * @param joinTables
   *          any further Tables that are required above and beyond the Tables that are contained in
   *          the original baseQuery.
   * @see Query#TYPE_EXPLICIT
   * @see Query#TYPE_AND
   * @see Query#TYPE_OR
   * @see #toArray(Query, Table[])
   */
  public static Query transformQuery(Query baseQuery,
                                     String filterExtension,
                                     int filterType,
                                     String replacementOrder,
                                     Table... joinTables)
  {
    String filter = Query.transformFilter(baseQuery, filterExtension, filterType);
    String order = StringUtils.padEmpty(replacementOrder, baseQuery.getOrder());
    return baseQuery.getTable().getQuery(null,
                                         filter,
                                         order,
                                         baseQuery.getGroupBy(),
                                         baseQuery.getLimit(),
                                         baseQuery.getExplicitFrom(),
                                         toArray(baseQuery, joinTables));
  }
}
