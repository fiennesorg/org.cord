package org.cord.mirror;

import static com.google.common.base.Preconditions.checkNotNull;

public class FieldValueSuppliers
{
  public static <T> FieldValueSupplier<T> of(final T value)
  {
    return new FieldValueSupplier<T>() {
      @Override
      public T supplyValue(TransientRecord record)
      {
        return value;
      }
    };
  }

  public static <T> FieldValueSupplier<T> ofDefined(final T value)
  {
    return of(checkNotNull(value));
  }

  private static final FieldValueSupplier<Object> NULLSUPPLIER = of(null);

  @SuppressWarnings("unchecked")
  public static <T> FieldValueSupplier<T> nullSupplier()
  {
    return (FieldValueSupplier<T>) NULLSUPPLIER;
  }
}
