package org.cord.mirror;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.cord.mirror.field.IntegerField;

public class IntField
  extends Field<Integer>
{
  public static IntFieldKey createKey(String keyName)
  {
    return new IntFieldKey(keyName);
  }

  private int _intValueIndex = -1;

  protected IntField(IntFieldKey fieldKey,
                     String label,
                     FieldValueSupplier<Integer> newRecordValue)
  {
    super(fieldKey,
          label,
          false,
          Integer.class,
          newRecordValue,
          newRecordValue);
  }

  public IntField(IntFieldKey fieldKey,
                  String label,
                  int defaultValue)
  {
    this(fieldKey,
         label,
         FieldValueSuppliers.of(Integer.valueOf(defaultValue)));
  }

  public IntField(IntFieldKey fieldKey,
                  String label)
  {
    this(fieldKey,
         label,
         0);
  }

  @Override
  public IntFieldKey getKey()
  {
    return (IntFieldKey) super.getKey();
  }

  @Override
  protected synchronized void register(Table table,
                                       int fieldId)
      throws MirrorTableLockedException
  {
    super.register(table, fieldId);
    _intValueIndex = table.getNextIntValueIndex(this);
  }

  protected final int getIntValueIndex()
  {
    return _intValueIndex;
  }

  @Override
  protected void validateSqlDefinition(String sqlDef)
      throws SQLException
  {
    validateSqlDef(IntegerField.ISVALIDSQL, sqlDef);
  }

  @Override
  public void appendDefinedSqlValue(StringBuilder buffer,
                                    Integer value)
  {
    buffer.append(value.intValue());
  }

  @Override
  public Integer getFieldValue(TransientRecord targetRecord)
  {
    return Integer.valueOf(targetRecord.getIntValues()[getIntValueIndex()]);
  }

  @Override
  public Integer validateDefinedValue(TransientRecord record,
                                      Object value)
      throws MirrorFieldException, TypeValidationException
  {
    try {
      return objectToValue(record, value);
    } catch (NumberFormatException nfeEx) {
      nfeEx.printStackTrace();
      return getDefaultDefinedValue(record);
    }
  }

  @Override
  public Integer objectToValue(TransientRecord record,
                               Object obj)
  {
    return IntegerField.castValue(obj);
  }

  @Override
  public void dbToValue(ResultSet resultSet,
                        Object[] objectValues,
                        int[] intValues)
      throws SQLException
  {
    intValues[getIntValueIndex()] = resultSet.getInt(getSqlFieldIndex());
  }

  @Override
  protected void populateInsert(TransientRecord record,
                                PreparedStatement preparedStatement)
      throws SQLException
  {
    preparedStatement.setInt(getSqlFieldIndex(), getIntValue(record));
  }

  @Override
  protected int getSqlType()
  {
    return Types.INTEGER;
  }

  public int getIntValue(TransientRecord record)
  {
    return record.getIntValues()[getIntValueIndex()];
  }

  private void setIntValue(TransientRecord record,
                           int value)
  {
    record.getIntValues()[getIntValueIndex()] = value;
  }

  @Override
  protected boolean isEqualValue(TransientRecord record,
                                 Integer value)
  {
    return value != null && value.intValue() == getIntValue(record);
  }

  @Override
  protected void setRecordToValueStore(TransientRecord record,
                                       Integer value)
  {
    setIntValue(record, value.intValue());
  }

  @Override
  public void storeNewRecordValue(Object[] objectValues,
                                  int[] intValues)
  {
    intValues[getIntValueIndex()] = getNewRecordValue().intValue();
  }

  @Override
  public boolean isEqualValues(TransientRecord record1,
                               TransientRecord record2)
  {
    return getIntValue(record1) == getIntValue(record2);
  }

  @Override
  public int compareValues(TransientRecord record1,
                           TransientRecord record2)
  {
    return Integer.compare(getIntValue(record1), getIntValue(record2));
  }

  @Override
  public int compareValuesIgnoreCase(TransientRecord record1,
                                     TransientRecord record2)
  {
    return compareValues(record1, record2);
  }
}
