package org.cord.mirror;

import org.cord.util.ObjectUtil;

import com.google.common.base.Preconditions;

/**
 * A RecordReference is a pointer around a Record in a given Table that may or may not exist. The
 * Record is only resolved when it is asked for therefore it is safe to hold onto RecordReference
 * items without causing invalidation of the Record caching infrastructure.
 * 
 * @author alex
 */
public final class RecordReference
{
  /**
   * Factory method to create an instance of a RecordReference. This currently always creates a new
   * instance, but it may shift to a caching implementation in the future.
   * 
   * @param tableWrapper
   *          The compulsory TableWrapper that the record is taken from
   * @param id
   *          The optional id of the record. Even if it is defined then it doesn't have to point at
   *          a record.
   * @return The appropriate RecordReference. Never null.
   */
  public static RecordReference getInstance(TableWrapper tableWrapper,
                                            int id)
  {
    return new RecordReference(tableWrapper.getTable(), id);
  }

  private final Table __table;
  private final int __id;

  /**
   * @param table
   *          The compulsory Table that the record should be from.
   * @param id
   *          The optional id within table for the record. Even if it is defined then it doesn't
   *          have to point at a record.
   */
  private RecordReference(Table table,
                          int id)
  {
    __table = Preconditions.checkNotNull(table, "table");
    __id = id;
  }

  public Table getTable()
  {
    return __table;
  }

  /**
   * @return The id, or null if it is not defined.
   */
  public int getId()
  {
    return __id;
  }

  public PersistentRecord getRecord(Viewpoint viewpoint)
  {
    return getTable().getOptRecord(getId(), viewpoint);
  }

  public boolean hasRecord()
  {
    return getTable().getOptRecord(getId()) != null;
  }

  @Override
  protected Object clone()
      throws CloneNotSupportedException
  {
    return this;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == this) {
      return true;
    }
    if (obj instanceof RecordReference) {
      RecordReference that = (RecordReference) obj;
      return this.getId() == that.getId() & this.getTable().equals(that.getTable());
    }
    return false;
  }

  @Override
  public int hashCode()
  {
    return ObjectUtil.hashCode(getTable().hashCode(), getId());
  }

  @Override
  public String toString()
  {
    return "RecordReference(" + getTable() + "," + getId() + ")";
  }

  public void appendSqlValue()
  {

  }
}
