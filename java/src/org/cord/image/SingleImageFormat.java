package org.cord.image;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.field.StringField;
import org.cord.util.DuplicateNamedException;
import org.cord.util.NamedImpl;

public class SingleImageFormat
  extends NamedImpl
  implements DbInitialiser, TableWrapper
{
  public static final String TABLENAME = "SingleImageFormat";

  public final static ObjectFieldKey<String> NAME = StringField.createKey("name");

  public final static ObjectFieldKey<String> IMPARAMETER = StringField.createKey("imParameter");

  public final static ObjectFieldKey<String> IMFORMAT = StringField.createKey("imFormat");

  public final static ObjectFieldKey<String> EXTENSION = StringField.createKey("extension");

  public final static RecordOperationKey<RecordSource> STYLES =
      RecordOperationKey.create("styles", RecordSource.class);

  public final static int ID_PNG = 1;

  public final static int ID_GIF_DITH = 2;

  public final static int ID_GIF_NODITH = 3;

  public final static int ID_JPEG_LOW = 4;

  public final static int ID_JPEG_MED = 5;

  public final static int ID_JPEG_HIGH = 6;

  private Table _singleImageFormats;

  public SingleImageFormat()
  {
    super(TABLENAME);
  }

  @Override
  public void init(Db db,
                   String pwd)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    _singleImageFormats = db.addTable(this, TABLENAME);
    _singleImageFormats.addField(new StringField(NAME, "Name", 32, ""));
    _singleImageFormats.addField(new StringField(IMPARAMETER, "ImageMagick parameters"));
    _singleImageFormats.addField(new StringField(IMFORMAT, "ImageMagick format"));
    _singleImageFormats.addField(new StringField(EXTENSION, "Filename extension"));
    _singleImageFormats.setTitleOperationName(NAME);
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String getTableName()
  {
    return TABLENAME;
  }

  @Override
  public Table getTable()
  {
    return _singleImageFormats;
  }
}
