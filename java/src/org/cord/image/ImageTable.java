package org.cord.image;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionOwnershipException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostFieldTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.AbstractGuiSupplierRecordOperation;
import org.cord.mirror.operation.FileFormElement;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.NodeManager;
import org.cord.node.cc.SingleImageContentCompiler;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.view.UploadSingleImage;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

@Deprecated
public class ImageTable
{
  public final static ObjectFieldKey<String> WEBPATH = StringField.createKey("webpath");

  public final static IntFieldKey WIDTH = IntField.createKey("width");

  public final static IntFieldKey HEIGHT = IntField.createKey("height");

  public final static ObjectFieldKey<String> ALT = StringField.createKey("alt");

  public final static IntFieldKey FORMAT_ID = IntFieldKey.create("format_id");

  public final static RecordOperationKey<PersistentRecord> FORMAT =
      LinkField.getLinkName(FORMAT_ID);

  public final static IntFieldKey SIZEKB = IntField.createKey("sizeKb");

  /**
   * RecordOperation that returns the timestamp as a Long of the currently live file. 0 if not
   * uploaded.
   * 
   * @see #LASTMODIFIEDDATE
   */
  public final static RecordOperationKey<Long> LASTMODIFIED =
      RecordOperationKey.create("lastModified", Long.class);

  /**
   * Name ("explicitCss") of RecordOperation that will return any explicit CSS code that should be
   * included in the img tag. The default implementation of this is just "" for now but it may be
   * extended in the future.
   */
  public final static RecordOperationKey<String> EXPLICITCSS =
      RecordOperationKey.create("explicitCss", String.class);

  /**
   * "css"
   */
  public final static ObjectFieldKey<String> CSS = StringField.createKey("css");

  /**
   * RecordOperation that returns true if the instance has an image has been uploaded and false if
   * it still has the default image.
   */
  public final static RecordOperationKey<Boolean> ISUPLOADED =
      RecordOperationKey.create("isUploaded", Boolean.class);

  public final static RecordOperationKey<File> JAVAFILE =
      RecordOperationKey.create("javaFile", File.class);

  /**
   * RecordOperation that returns the timestamp as a java Date object of the currently live file.
   * 
   * @see #LASTMODIFIED
   */
  public final static RecordOperationKey<Date> LASTMODIFIEDDATE =
      RecordOperationKey.create("lastModifiedDate", Date.class);

  public final static RecordOperationKey<String> IMAGEPREVIEW =
      RecordOperationKey.create("imagePreview", String.class);

  private static ImageMetaDataFactory __imageMetaDataFactory =
      SimpleImageMetaDataFactory.getInstance();

  /**
   * Attempt to import the specified file into the specified instance. You should preset the
   * SINGLEIMAGEINSTANCE_FORMAT_ID parameter before invoking the method. This will set up the
   * following fields deriving the information from the uploaded file after it has had any resizing
   * and format conversion necessary: SINGLEIMAGEINSTANCE_WIDTH, SINGLEIMAGEINSTANCE_HEIGHT,
   * SINGLEIMAGEINSTANCE_WEBPATH and SINGLEIMAGEINSTANCE_SIZEKB. The rescaled image will be placed
   * into the appropriate location inside the webRoot as specified by the behaviour of the
   * SINGLEIMAGESTYLE_WEBPATHROOT that this image is to implement. Note that this method will not
   * perform any alterations to the original file, as this behaviour is invoked as part of the
   * housekeeping during garbage collection of the NodeRequest.
   * 
   * @see #updateInstance(TransientRecord, PersistentRecord, File, Integer, File, String)
   * @see UploadSingleImage#uploadSingleImage(PersistentRecord, WritableRecord, File, Transaction)
   */
  public static TransientRecord uploadFile(TransientRecord instance,
                                           File uploadedFile,
                                           File webRoot)
      throws IOException, MirrorFieldException
  {
    PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
    ImageMetaData uploadedImage = __imageMetaDataFactory.getImageMetaData(uploadedFile);
    PersistentRecord format = instance.comp(ImageTable.FORMAT);
    String imParameter = format.comp(SingleImageFormat.IMPARAMETER);
    String filename = System.currentTimeMillis() + format.comp(SingleImageFormat.EXTENSION);
    String imFormat = format.comp(SingleImageFormat.IMFORMAT);
    String webpath = style.comp(ImageStyleTable.WEBPATHROOT) + filename;
    File targetFile = new File(webRoot, webpath);
    int targetWidth = style.compInt(ImageStyleTable.TARGETWIDTH);
    int targetHeight = style.compInt(ImageStyleTable.TARGETHEIGHT);
    int imageScalingMode = style.compInt(ImageStyleTable.IMAGESCALINGMODE);
    ImageMetaData processedFile = uploadedImage.convert(targetFile,
                                                        targetWidth,
                                                        targetHeight,
                                                        imageScalingMode,
                                                        imParameter,
                                                        imFormat);
    instance.setField(ImageTable.WIDTH, Integer.valueOf(processedFile.getWidth()));
    instance.setField(ImageTable.HEIGHT, Integer.valueOf(processedFile.getHeight()));
    instance.setField(ImageTable.WEBPATH, webpath);
    instance.setField(ImageTable.SIZEKB, Integer.valueOf((int) (targetFile.length() / 1024)));
    return instance;
  }

  /**
   * Core updating method for the updateInstance method.
   * 
   * @see NodeManager#getWebRoot()
   * @see #uploadFile(TransientRecord, File, File)
   */
  public static TransientRecord updateInstance(TransientRecord instance,
                                               PersistentRecord style,
                                               File uploadedFile,
                                               Integer format_id,
                                               File webRoot,
                                               String alt)
      throws MirrorFieldException
  {
    ImageTable.updateAlt(instance, style, alt);
    if (uploadedFile != null) {
      if (format_id != null && !style.is(ImageStyleTable.ISFIXEDFORMAT)) {
        try {
          instance.setField(ImageTable.FORMAT_ID, format_id);
        } catch (MirrorFieldException mfEx) {
          instance.setField(ImageTable.FORMAT_ID, style.compInt(ImageStyleTable.FORMAT_ID));
        }
      } else {
        instance.setField(ImageTable.FORMAT_ID, style.compInt(ImageStyleTable.FORMAT_ID));
      }
      try {
        uploadFile(instance, uploadedFile, webRoot);
      } catch (IOException ioEx) {
        throw new MirrorFieldException("Error uploading image",
                                       ioEx,
                                       instance,
                                       ImageTable.WEBPATH,
                                       uploadedFile,
                                       null);
      }
    }
    return instance;
  }

  /**
   * Change the ImageMetaDataFactory that this class is going to use to analyse uploaded images. The
   * class defaults to a SimpleImageMetaDataFactory that binds on the ImageMetaDataFactory classes
   * and hence utilise ImageMagick for performing the operations.
   * 
   * @throws NullPointerException
   *           if factory is not defined.
   */
  public static void setImageMetaDataFactory(ImageMetaDataFactory factory)
  {
    __imageMetaDataFactory = Preconditions.checkNotNull(factory, "factory");
  }

  /**
   * Prepare a given Table to act as an image instance table. This will register the fields + the
   * filename triggers on the given table.
   */
  public static void initialiseInstanceTable(final NodeManager nodeManager,
                                             String transactionPassword,
                                             Table table,
                                             RecordOperationKey<RecordSource> referencesName)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(WEBPATH, "Webpath", 128, ""));
    table.addField(new IntField(WIDTH, "Width"));
    table.addField(new IntField(HEIGHT, "Height"));
    int altSize = GettableUtils.get(nodeManager, "SingleImageInstance.alt.size", 255);
    table.addField(new StringField(ALT, "Alt tag", altSize, ""));
    table.addField(new LinkField(FORMAT_ID,
                                 "Target format",
                                 null,
                                 FORMAT,
                                 SingleImageFormat.TABLENAME,
                                 referencesName,
                                 null,
                                 transactionPassword,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new IntField(SIZEKB, "Size Kb"));
    table.addField(new StringField(CSS, "CSS class", 32, ""));
    table.addRecordOperation(new SimpleRecordOperation<Long>(LASTMODIFIED) {
      @Override
      public Long getOperation(TransientRecord instance,
                               Viewpoint viewpoint)
      {
        PersistentRecord singleImageStyle = instance.comp(TableContentCompiler.STYLE);
        String webPath = instance.opt(WEBPATH);
        if (Strings.isNullOrEmpty(webPath)
            || webPath.endsWith(singleImageStyle.comp(ImageStyleTable.DEFAULTFILENAME))) {
          return NumberUtil.LONG_ZERO;
        }
        String filename =
            webPath.substring(singleImageStyle.comp(ImageStyleTable.WEBPATHROOT).length());
        int dotIndex = filename.indexOf('.');
        if (dotIndex != -1) {
          try {
            return Long.valueOf(filename.substring(0, dotIndex));
          } catch (NumberFormatException nfEx) {
          }
        }
        File file = new File(nodeManager.getWebRoot(), webPath);
        return new Long(file.lastModified());
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<Date>(LASTMODIFIEDDATE) {
      @Override
      public Date getOperation(TransientRecord instance,
                               Viewpoint viewpoint)
      {
        return new Date(instance.opt(LASTMODIFIED).longValue());
      }
    });
    table.addPreInstantiationTrigger(new ImagePreBooter());
    table.addPostFieldTrigger(new FilenamePostFieldTrigger(nodeManager));
    table.addStateTrigger(new FilenameCancel(nodeManager, table.getName()));
    table.addStateTrigger(new FilenameCommit(nodeManager, table.getName()));
    table.addStateTrigger(new FilenameDelete(nodeManager, table.getName()));
    table.addRecordOperation(new FileFormElement(SingleImageContentCompiler.FILE, "Select image"));
    table.addRecordOperation(new SimpleRecordOperation<Boolean>(ImageTable.ISUPLOADED) {
      @Override
      public Boolean getOperation(TransientRecord instance,
                                  Viewpoint viewpoint)
      {
        return Boolean.valueOf(!instance.opt(WEBPATH)
                                        .equals(instance.comp(TableContentCompiler.STYLE)
                                                        .opt(ImageStyleTable.DEFAULTWEBPATH)));
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<File>(ImageTable.JAVAFILE) {
      @Override
      public File getOperation(TransientRecord instance,
                               Viewpoint viewpoint)
      {
        return new File(nodeManager.getWebRoot(), instance.opt(WEBPATH));
      }
    });
    table.addRecordOperation(new AbstractGuiSupplierRecordOperation(IMAGEPREVIEW, "Image preview") {
      @Override
      public String getOperation(TransientRecord instance,
                                 Viewpoint viewpoint)
      {
        return "<img class=\"imagePreview\" src=\"" + instance.opt(WEBPATH) + "\" />";
      }
    });
  }

  /**
   * StateTrigger on the instance Table that waits for STATE_WRITABLE_CANCELLED and deletes any
   * uploaded files that are no longer required.
   */
  protected static class FilenameCancel
    extends AbstractStateTrigger
  {
    /**
     * The state (STATE_WRITABLE_CANCELLED) that this StateTrigger expects to be registered under.
     * 
     * @see StateTrigger#STATE_WRITABLE_CANCELLED
     */
    public final static int STATE = STATE_WRITABLE_CANCELLED;

    private NodeManager _nodeManager;

    public FilenameCancel(NodeManager nodeManager,
                          String tableName)
    {
      super(STATE,
            false,
            tableName);
      _nodeManager = nodeManager;
    }

    @Override
    public void shutdown()
    {
      super.shutdown();
      _nodeManager = null;
    }

    @Override
    public void trigger(PersistentRecord persistentInstance,
                        Transaction transaction,
                        int state)
    {
      WritableRecord writableInstance = null;
      try {
        writableInstance = persistentInstance.getWritableRecord(transaction);
      } catch (MirrorTransactionOwnershipException notOwnerEx) {
        throw new LogicException("Transaction appears to not own record", notOwnerEx);
      }
      String persistentWebpath = persistentInstance.comp(WEBPATH);
      String writableWebpath = writableInstance.comp(WEBPATH);
      if (persistentWebpath.equals(writableWebpath)) {
        return;
      }
      File targetFile = new File(_nodeManager.getWebRoot(), writableWebpath);
      targetFile.delete();
    }
  }

  /**
   * StateTrigger on the instance Table that waits for STATE_WRITABLE_PREUPDATE events and checks to
   * see if there has been a new image uploaded and if so it deletes the previous image from the
   * HDD.
   */
  protected static class FilenameCommit
    extends AbstractStateTrigger
  {
    /**
     * The state (STATE_WRITABLE_PREUPDATE) that this StateTrigger expects to be registered under.
     * 
     * @see StateTrigger#STATE_WRITABLE_PREUPDATE
     */
    public final static int STATE = STATE_WRITABLE_PREUPDATE;

    private NodeManager _nodeManager;

    public FilenameCommit(NodeManager nodeManager,
                          String tableName)
    {
      super(STATE,
            false,
            tableName);
      _nodeManager = nodeManager;
    }

    @Override
    public void shutdown()
    {
      super.shutdown();
      _nodeManager = null;
    }

    @Override
    public void trigger(PersistentRecord persistentInstance,
                        Transaction transaction,
                        int state)
    {
      if (Boolean.FALSE.equals(persistentInstance.opt(ImageTable.ISUPLOADED))) {
        return;
      }
      String persistentWebpath = persistentInstance.comp(WEBPATH);
      WritableRecord writableInstance = null;
      try {
        writableInstance = persistentInstance.getWritableRecord(transaction);
      } catch (MirrorTransactionOwnershipException notOwnerEx) {
        throw new LogicException("Transaction appears to not own record", notOwnerEx);
      }
      String writableWebpath = writableInstance.comp(WEBPATH);
      if (persistentWebpath.equals(writableWebpath)) {
        return;
      }
      File targetFile = new File(_nodeManager.getWebRoot(), persistentWebpath);
      targetFile.delete();
    }
  }

  /**
   * StateTrigger on the instance Table that waits for a STATE_PREDELETED_DELETED event and then
   * deletes the file (if any) that is associated with the record that is getting deleted.
   * 
   * @author alex
   */
  protected static class FilenameDelete
    extends AbstractStateTrigger
  {
    private final NodeManager __nodeManager;

    public FilenameDelete(NodeManager nodeManager,
                          String tableName)
    {
      super(STATE_PREDELETED_DELETED,
            false,
            tableName);
      __nodeManager = Preconditions.checkNotNull(nodeManager, "nodeManager");
    }

    @Override
    public void trigger(PersistentRecord record,
                        Transaction transaction,
                        int state)
        throws Exception
    {
      if (Boolean.FALSE.equals(record.opt(ISUPLOADED))) {
        return;
      }
      File file = new File(__nodeManager.getWebRoot(), record.opt(WEBPATH));
      file.delete();
    }
  }

  /**
   * Reset all the values for an instance so that it is pointing at the default image again. This is
   * primarily used when an editor wants to delete an uploaded image and doesn't supply an
   * alternative.
   */
  public static void setDefaultImage(TransientRecord instance)
      throws MirrorFieldException
  {
    PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
    instance.setField(WEBPATH, style.opt(ImageStyleTable.DEFAULTWEBPATH));
    instance.setField(WIDTH, Integer.valueOf(style.compInt(ImageStyleTable.DEFAULTWIDTH)));
    instance.setField(HEIGHT, Integer.valueOf(style.compInt(ImageStyleTable.DEFAULTHEIGHT)));
    instance.setField(ALT, style.comp(ImageStyleTable.DEFAULTALT));
    instance.setField(FORMAT_ID, Integer.valueOf(style.compInt(ImageStyleTable.FORMAT_ID)));
  }

  /**
   * Trigger that sets up the CSS for the image and if the webPath is not defined (ie it is really a
   * new image) then invokes setDefaultImage to provide sensible starting values.
   * 
   * @author alex
   * @see ImageTable#setDefaultImage(TransientRecord)
   */
  protected static class ImagePreBooter
    extends PreInstantiationTriggerImpl
  {
    @Override
    public void triggerPre(TransientRecord instance,
                           Gettable params)
    {
      try {
        instance.setField(CSS, instance.comp(TableContentCompiler.STYLE).comp(ImageStyleTable.CSS));
        if (Strings.isNullOrEmpty(instance.opt(WEBPATH))) {
          setDefaultImage(instance);
        }
      } catch (MirrorFieldException mfEx) {
        throw new LogicException("Unexpected field exception when initialising single image from "
                                 + instance.opt(TableContentCompiler.STYLE),
                                 mfEx);
      }
    }
  }

  /**
   * PostFieldTrigger that is responsible for removing any uploaded images that are replaced with
   * new images during the process of a single update cycle.
   */
  protected static class FilenamePostFieldTrigger
    implements PostFieldTrigger<String>
  {
    private NodeManager _nodeManager;

    public FilenamePostFieldTrigger(NodeManager nodeManager)
    {
      _nodeManager = nodeManager;
    }

    @Override
    public void shutdown()
    {
      _nodeManager = null;
    }

    @Override
    public FieldKey<String> getFieldName()
    {
      return WEBPATH;
    }

    @Override
    public void trigger(TransientRecord transientInstance,
                        String fieldName,
                        Object originalValue,
                        boolean hasChanged)
    {
      if (Boolean.FALSE.equals(transientInstance.opt(ImageTable.ISUPLOADED))) {
        return;
      }
      String webpath = originalValue.toString();
      if (transientInstance instanceof WritableRecord) {
        WritableRecord writableInstance = (WritableRecord) transientInstance;
        PersistentRecord persistentInstance = writableInstance.getPersistentRecord();
        if (webpath.equals(persistentInstance.opt(WEBPATH))) {
          return;
        }
      }
      File targetFile = new File(_nodeManager.getWebRoot(), webpath);
      targetFile.delete();
    }
  }

  public static void updateAlt(TransientRecord instance,
                               PersistentRecord style,
                               String alt)
      throws MirrorFieldException
  {
    if (style.is(ImageStyleTable.HASEDITABLEALT)) {
      instance.setFieldIfDefined(ALT, alt);
    }
  }

  public static void copy(Settable settable,
                          PersistentRecord instance)
      throws SettableException
  {
    // TODO: (#259) resolve whether or not STYLE should be defined in
    // ImageTable
    PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
    if (style.is(ImageStyleTable.HASEDITABLEALT)) {
      settable.set(ALT, instance.opt(ALT));
    }
    if (Boolean.TRUE.equals(instance.opt(ISUPLOADED))) {
      settable.set(SingleImageContentCompiler.CGI_FILE, instance.opt(JAVAFILE));
    }
    settable.set(FORMAT_ID, instance.opt(FORMAT_ID));
  }
}
