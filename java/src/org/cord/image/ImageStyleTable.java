package org.cord.image;

import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;

public class ImageStyleTable
{
  public final static String TABLENAME = "SingleImageStyle";

  public final static String SINGLEIMAGESTYLE = "SingleImageStyle";

  public final static ObjectFieldKey<String> DEFAULTFILENAME =
      StringField.createKey("defaultFilename");

  public final static IntFieldKey DEFAULTWIDTH = IntField.createKey("defaultWidth");

  public final static IntFieldKey DEFAULTHEIGHT = IntField.createKey("defaultHeight");

  public final static IntFieldKey TARGETWIDTH = IntField.createKey("targetWidth");

  public final static IntFieldKey TARGETHEIGHT = IntField.createKey("targetHeight");

  /**
   * @see org.cord.image.ImageScalingOptions
   */
  public final static IntFieldKey IMAGESCALINGMODE = IntField.createKey("imageScalingMode");

  public final static ObjectFieldKey<String> WEBPATHROOT = StringField.createKey("webpathRoot");

  public final static ObjectFieldKey<String> DEFAULTALT = StringField.createKey("defaultAlt");

  public final static ObjectFieldKey<Boolean> HASEDITABLEALT =
      BooleanField.createKey("hasEditableAlt");

  public final static IntFieldKey FORMAT_ID = IntFieldKey.create("format_id");

  public final static RecordOperationKey<PersistentRecord> FORMAT =
      LinkField.getLinkName(FORMAT_ID);

  public final static ObjectFieldKey<Boolean> ISFIXEDFORMAT =
      BooleanField.createKey("isFixedFormat");

  /**
   * "css"
   */
  public final static ObjectFieldKey<String> CSS = StringField.createKey("css");

  public final static RecordOperationKey<String> DEFAULTWEBPATH =
      RecordOperationKey.create("defaultWebPath", String.class);

  public static void initialiseStyleTable(String transactionPassword,
                                          Table styleTable)
      throws MirrorTableLockedException
  {
    styleTable.addField(new StringField(DEFAULTFILENAME, "Default image filename"));
    styleTable.addField(new IntField(DEFAULTWIDTH, "Default width"));
    styleTable.addField(new IntField(DEFAULTHEIGHT, "Default height"));
    styleTable.addField(new IntField(TARGETWIDTH, "Target scaled width"));
    styleTable.addField(new IntField(TARGETHEIGHT, "Target scaled height"));
    styleTable.addField(new IntField(IMAGESCALINGMODE, "Scaling mode"));
    styleTable.addField(new StringField(WEBPATHROOT, "Image directory webpath"));
    styleTable.addField(new StringField(DEFAULTALT, "Default value of ALT tag"));
    styleTable.addField(new BooleanField(HASEDITABLEALT, "Is ALT tag editable?", Boolean.TRUE));
    styleTable.addField(new LinkField(FORMAT_ID,
                                      "Default file format",
                                      null,
                                      FORMAT,
                                      SingleImageFormat.TABLENAME,
                                      SingleImageFormat.STYLES,
                                      null,
                                      transactionPassword,
                                      Dependencies.LINK_ENFORCED));
    styleTable.addField(new BooleanField(ISFIXEDFORMAT, "Is file format fixed?", Boolean.FALSE));
    styleTable.addField(new StringField(CSS, "CSS class", 32, ""));
    styleTable.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(DEFAULTWEBPATH) {
      @Override
      public String calculate(TransientRecord singleImageStyle,
                              Viewpoint viewpoint)
      {
        StringBuilder buffer = new StringBuilder(64);
        buffer.append(singleImageStyle.opt(WEBPATHROOT))
              .append(singleImageStyle.opt(DEFAULTFILENAME));
        return buffer.toString();
      }
    });
  }
}
