package org.cord.image;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface ImageScalingOptions
{
  /**
   * (0)
   */
  public final static int SCALE_ALWAYS_INSIDE_MAINTAINASPECT = 0;

  /**
   * (1)
   */
  public final static int SCALE_ALWAYS_OUTSIDE_MAINTAINASPECT = 1;

  /**
   * (2)
   */
  public final static int SCALE_EXACT = 2;

  /**
   * (3)
   */
  public final static int SCALE_IFREQUIRED_INSIDE_MAINTAINASPECT = 3;

  /**
   * (4)
   */
  public final static int SCALE_IFREQUIRED_OUTSIDE_MAINTAINASPECT = 4;

  public final static List<String> SCALE_NAMES =
      Collections.unmodifiableList(Arrays.asList(new String[] {
          "Always scale inside, maintain aspect", "Always scale outside, maintain aspect",
          "Always scale to bounding box", "Scale if required so inside, maintain aspect",
          "Scale if required so outside, maintain aspect" }));
}
