package org.cord.image;

import java.util.Iterator;

import org.cord.util.NoSuchNamedException;

/**
 * Interface describing objects that use image objects.
 * 
 * @deprecated because nobody is using this interface any more
 */
@Deprecated
public interface ImageUtiliser
{
  /**
   * Get a list of all the image names that are used by this ImageUtiliser.
   * 
   * @return Iterator of String objects which are the names of the used images. The order is the
   *         same as the order of usage in the object and multiple instances are also preserved.
   */
  public Iterator<String> getUtilisedImageNames();

  /**
   * Get the number of images used by the ImageUtiliser
   * 
   * @return The image count. Multiple instances are counted towards the total.
   */
  public int getImageCount();

  /**
   * Get the ImageMetaDataProvider that this ImageUtiliser is using to get get it's image
   * information.
   */
  public ImageMetaDataProvider getImageMetaDataProvider();

  /**
   * Get the index based on the usage order within the object of a named image starting at a
   * specific in the listing. This enables you to target specific instances of specific images in
   * duplicate lists.
   * 
   * @param imageName
   *          The name of the image to search for.
   * @param startingIndex
   *          The index to start searching from.
   * @return The image index
   * @throws org.cord.util.NoSuchNamedException
   *           If the specific named image cannot be found after the startingIndex. Please note that
   *           the image may well still exist pre-startingIndex.
   */
  public int getImageIndex(String imageName,
                           int startingIndex)
      throws NoSuchNamedException;

  /**
   * Ask this ImageUtiliser what style the block that a given Image is embedded in is called. This
   * is a slightly odd call and may well be dropped / refactored in the future, but for now it is
   * going to stand. The only point to note is that as we can reuse images and the duplicated images
   * may potentially be in different block styles, and the additional block styles may not be
   * present when the original source image is presented so we will probably just take the first
   * instance of the Block Style name as the official name for the image.
   * 
   * @return The name of the block style. If the block is unstyled then either null or "" may be
   *         returned.
   * @throws NoSuchNamedException
   *           if this ImageUtiliser doesn't contain a reference to imageName.
   */
  public String getImageBlockStyle(String imageName,
                                   int startingIndex)
      throws NoSuchNamedException;
}
