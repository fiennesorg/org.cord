package org.cord.image;

import java.io.File;
import java.io.IOException;

import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

/**
 * An implementation of ImageMetaDataFactory that binds directly onto the appropriate message inside
 * ImageMetaData.
 * 
 * @author alex
 * @see ImageMetaData#getInstance(File)
 */
public final class SimpleImageMetaDataFactory
  implements ImageMetaDataFactory, SingletonShutdownable
{
  private static SimpleImageMetaDataFactory _instance = new SimpleImageMetaDataFactory();

  public static ImageMetaDataFactory getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private SimpleImageMetaDataFactory()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  public ImageMetaData getImageMetaData(File file)
      throws IOException
  {
    return ImageMetaData.getInstance(file);
  }
}