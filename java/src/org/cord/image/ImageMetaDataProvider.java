package org.cord.image;

import org.cord.mirror.Transaction;

/**
 * Interface describing an object that provides ImageMetaData objects on request.
 */
public interface ImageMetaDataProvider
{
  /**
   * Get the requested meta data about a named image.
   * 
   * @return the appropriate meta-data or null if the image cannot be found.
   */
  public ImageMetaData getImageMetaData(String namespace,
                                        String imageName,
                                        Transaction transaction);
}
