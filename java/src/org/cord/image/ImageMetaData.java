package org.cord.image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.cord.sql.SqlUtil;
import org.cord.util.CollectionUtil;
import org.cord.util.EnumerationWrapper;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;

import com.google.common.base.Preconditions;

/**
 * ImageMetaData provides a lightweight wrapper around an image file.
 */
public class ImageMetaData
  implements ImageScalingOptions
{
  private final int __width;

  private final int __height;

  private final int __kilobytes;

  private final File __file;

  private final String __urlPath;

  private final String __format;

  private final String __cssClass;

  private final String __title;

  private final String __alt;

  private final String __caption;

  private final String __imageTarget;

  // identify
  // -ping
  // -format
  // %w:%h:%m
  // filename

  private static String IMAGE_MAGICK_PATH = "/usr/bin/";
  
  public static String getImageMagickPath()
  {
    return IMAGE_MAGICK_PATH;
  }
  
  public static void setImageMagickPath(String path)
  {
    assertValidImageMagickPath(path);
    IMAGE_MAGICK_PATH = path;
  }
  
  public static final String CONF_IMAGE_MAGICK_PATH = "image_magick.path";
  
  public static void setImageMagickPath(Gettable config)
  {
    String path = config.getString(CONF_IMAGE_MAGICK_PATH);
    if (path != null) {
      System.err.println(CONF_IMAGE_MAGICK_PATH + ": "+ path);
      setImageMagickPath(path);
    }
  }
  
  public static void assertValidImageMagickPath(String path)
  {
    File identify = new File(path + "identify");
    if (!identify.canExecute()) {
      throw new IllegalStateException("Unable to execute " + identify);
    }
  }
  
  public static final String imageMagick(String command) {
    return IMAGE_MAGICK_PATH + command;
  }
  
  public final static String escapeFilename(File file)
  {
    return SqlUtil.replaceChar(file.toString(), ' ', "\\ ");
  }

  private final static String[] getIdentifyArray(File source)
  {
    if (source.getName().endsWith("pdf")) {
      return new String[] { imageMagick("identify"), "-quiet", "-format", "%w:%h:%m",
          source.toString() + "[0]" };
    } else {
      return new String[] { imageMagick("identify"), "-quiet", "-ping", "-format", "%w:%h:%m",
          source.toString() + "[0]" };
    }
  }

  /**
   * Get the EXIF orientation of an imageFile as reported by ImageMagick. TopLeft is the default ie
   * no rotation required.
   * 
   * @return Undefined | TopLeft | TopRight | BottomRight | BottomLeft | LeftTop | RightTop |
   *         RightBottom | LeftBottom | Unrecognized
   */
  public static String getImageOrientation(File imageFile)
      throws IOException
  {
    return IoUtil.exec(new String[] { imageMagick("identify"), "-format", "%[orientation]",
        imageFile.toString() }, imageFile.getParentFile(), 0, true);
  }

  /**
   * If an image has EXIF orientation data and if it is not in TopLeft orientation then auto-rotate
   * it so that it is. The resulting image will have the same filename as the original image. If the
   * orientation is TopLeft, Undefined or Unrecognized then the original image is not touched.
   * 
   * @return true if the image was rotated, false if it is unchanged.
   */
  public static boolean autoOrientImage(File imageFile)
      throws IOException
  {
    System.out.println("Original: " + getInstance(imageFile));
    switch (getImageOrientation(imageFile)) {
      case "TopLeft":
      case "Undefined":
      case "Unrecognized":
        return false;
    }
    File backupFile = new File(imageFile.getParentFile(), imageFile.getName() + ".bak");
    IoUtil.unixMv(imageFile, backupFile, true);
    IoUtil.execWithVerboseErrors(new String[] { imageMagick("convert"), "-auto-orient", backupFile.toString(),
        imageFile.toString() });
    backupFile.delete();
    return true;
  }

  public static void main(String[] args)
      throws IOException
  {
    autoOrientImage(new File("/Users/alex/dev/IntegerPurge/edinburghrewards/www/dyn/orig/1/IMG_3948.JPG"));
  }

  private final static String[] getConvertArray(File source,
                                                File target,
                                                int width,
                                                int height,
                                                boolean isExactDimensions,
                                                String additionalParameters,
                                                String targetFormat)
  {
    final List<String> list = new ArrayList<String>();
    list.add(imageMagick("convert"));
    list.add("-auto-orient");
    list.add("-geometry");
    list.add(width + "x" + height + (isExactDimensions ? "!" : ""));
    if (additionalParameters != null && additionalParameters.length() > 0) {
      final Iterator<Object> words =
          new EnumerationWrapper<Object>(new StringTokenizer(additionalParameters));
      while (words.hasNext()) {
        list.add((String) words.next());
      }
    }
    String sourceFilename = source.toString();
    String lowerSourceFilename = sourceFilename.toLowerCase();
    if (lowerSourceFilename.endsWith("pdf") || lowerSourceFilename.equals("ps")) {
      list.add("-density");
      list.add("400x400");
    }
    boolean isPngSource = lowerSourceFilename.endsWith(".png");
    list.add(sourceFilename + ("GIF".equals(targetFormat) || isPngSource ? "" : "[0]"));
    list.add((targetFormat != null && targetFormat.length() != 0 ? targetFormat + ":" : "")
             + escapeFilename(target));
    return CollectionUtil.toStringArray(list);
  }

  /**
   * Factory method to generate a new instance of ImageMetaData from an existing file, performing an
   * ImageMagick sweep of the file to work out it's contents. This is a relatively expensive
   * operation because ImageMagick has to scan the file to extract the meta-data. It might therefore
   * be worth using an implementation of the abstraction of this in ImageMetaDataFactory, namely
   * ImageMetaDataCache, especially when you are going to need read the File multiple times in
   * different locations within your code.
   * 
   * @param source
   *          The file that is to be wrapped.
   * @return The appropriate ImageMetaData object
   * @throws IOException
   *           If the file cannot be cleanly read.
   * @see ImageMetaDataCache
   * @see ImageMetaDataFactory
   */
  public final static ImageMetaData getInstance(File source)
      throws IOException
  {
    Preconditions.checkNotNull(source, "source");
    if (!source.canRead()) {
      throw new IOException("Unable to read " + source);
    }
    StringBuilder outBuf = new StringBuilder();
    StringBuilder errBuf = new StringBuilder();
    String[] identifyArray = getIdentifyArray(source);
    try {
      IoUtil.exec(identifyArray, null, outBuf, errBuf);
      String identifyErrors = errBuf.toString().trim();
      String identifyOutput = outBuf.toString().trim();
      if (identifyErrors.length() > 0) {
        throw new IOException(String.format("Unable to identify \"%s\": output: %s, error: %s",
                                            source,
                                            identifyOutput,
                                            identifyErrors));
      }
      StringTokenizer resultList = new StringTokenizer(identifyOutput, ":");
      ImageMetaData result = new ImageMetaData(Integer.parseInt(resultList.nextToken()),
                                               Integer.parseInt(resultList.nextToken()),
                                               source,
                                               source.getParent(),
                                               resultList.nextToken(),
                                               "",
                                               "",
                                               "",
                                               "",
                                               "");
      return result;
    } catch (RuntimeException rEx) {
      IOException ioEx =
          new IOException("Error executing:" + Arrays.toString(identifyArray) + "-->" + outBuf);
      ioEx.initCause(rEx);
      throw ioEx;
    }
  }

  public ImageMetaData(int width,
                       int height,
                       File file,
                       String urlPath,
                       String format,
                       String cssClass,
                       String title,
                       String caption,
                       String alt,
                       String imageTarget)
  {
    this.__width = width;
    __height = height;
    __file = file;
    __kilobytes = (int) (file.length() / 1024);
    __urlPath = urlPath;
    __format = format;
    __cssClass = cssClass;
    __title = title;
    __caption = caption;
    __alt = alt;
    __imageTarget = imageTarget;
  }

  public String getAlt()
  {
    return __alt;
  }

  public String getImageTarget()
  {
    return __imageTarget;
  }

  public String getTitle()
  {
    return __title;
  }

  public String getCaption()
  {
    return __caption;
  }

  public String getCssClass()
  {
    return __cssClass;
  }

  public int getKilobytes()
  {
    return __kilobytes;
  }

  public final int getWidth()
  {
    return __width;
  }

  public final int getHeight()
  {
    return __height;
  }

  public final String getFormat()
  {
    return __format;
  }

  public final File getFile()
  {
    return __file;
  }

  public final String getUrlPath()
  {
    return __urlPath;
  }

  @Override
  public final String toString()
  {
    return ("ImageMetaData(" + __file + "," + __urlPath + "," + __format + "," + __width + "x"
            + __height + "," + __kilobytes + "KB)");
  }

  private final double getAspect(int width,
                                 int height)
  {
    return height == 0 ? Double.MAX_VALUE : (double) width / (double) height;
  }

  public final double getAspect()
  {
    return getAspect(getWidth(), getHeight());
  }

  /**
   * Convert the wrapped file to a new scale and format with the ability to specify the scalingMode
   * when working out the new dimensions.
   */
  public ImageMetaData convert(File targetFile,
                               int requestedWidth,
                               int requestedHeight,
                               int scalingMode,
                               String additionalParameters,
                               String targetFormat)
      throws IOException
  {
    double currentAspect = getAspect();
    int scaledWidth;
    int scaledHeight;
    double targetAspect = getAspect(requestedWidth, requestedHeight);
    switch (scalingMode) {
      case SCALE_ALWAYS_INSIDE_MAINTAINASPECT:
        if (requestedWidth != this.getWidth() && requestedHeight != this.getHeight()) {
          if (targetAspect > currentAspect) {
            // image is too tall...
            scaledWidth = (this.getWidth() * requestedHeight) / this.getHeight();
            scaledHeight = requestedHeight;
          } else {
            // image is too wide...
            scaledWidth = requestedWidth;
            scaledHeight = (this.getHeight() * requestedWidth) / this.getWidth();
          }
        } else {
          scaledWidth = this.getWidth();
          scaledHeight = this.getHeight();
        }
        break;
      case SCALE_ALWAYS_OUTSIDE_MAINTAINASPECT:
        if (requestedWidth != this.getWidth() && requestedHeight != this.getHeight()) {
          if (targetAspect > currentAspect) {
            // image is taller aspect than target --> lock to width
            scaledWidth = requestedWidth;
            scaledHeight = (this.getHeight() * requestedWidth) / this.getWidth();
          } else {
            // image is wider aspect than target --> lock to height
            scaledWidth = (this.getWidth() * requestedHeight) / this.getHeight();
            scaledHeight = requestedHeight;
          }
        } else {
          // image is bigger than requested, so is ok...
          scaledWidth = this.getWidth();
          scaledHeight = this.getHeight();
        }
        break;
      case SCALE_EXACT:
        scaledWidth = requestedWidth;
        scaledHeight = requestedHeight;
        break;
      case SCALE_IFREQUIRED_INSIDE_MAINTAINASPECT:
        if (requestedWidth < this.getWidth() || requestedHeight < this.getHeight()) {
          if (targetAspect > currentAspect) {
            // image is too tall...
            scaledWidth = (this.getWidth() * requestedHeight) / this.getHeight();
            scaledHeight = requestedHeight;
          } else {
            // image is too wide...
            scaledWidth = requestedWidth;
            scaledHeight = (this.getHeight() * requestedWidth) / this.getWidth();
          }
        } else {
          scaledWidth = this.getWidth();
          scaledHeight = this.getHeight();
        }
        break;
      case SCALE_IFREQUIRED_OUTSIDE_MAINTAINASPECT:
        if (requestedWidth > this.getWidth() || requestedHeight > this.getHeight()) {
          // image is too small in some way...
          if (targetAspect > currentAspect) {
            // image is taller aspect than target --> lock to width
            scaledWidth = requestedWidth;
            scaledHeight = (this.getHeight() * requestedWidth) / this.getWidth();
          } else {
            // image is wider aspect than target --> lock to height
            scaledWidth = (this.getWidth() * requestedHeight) / this.getHeight();
            scaledHeight = requestedHeight;
          }
        } else {
          // image is bigger than requested, so is ok...
          scaledWidth = this.getWidth();
          scaledHeight = this.getHeight();
        }
        break;
      default:
        throw new IllegalArgumentException("Unknown scalingMode:" + scalingMode);
    }
    return convert(targetFile, scaledWidth, scaledHeight, additionalParameters, targetFormat);
  }

  /**
   * Get the ImageDimension object that represents the size of this ImageMetaData.
   */
  public ImageDimension getImageDimension()
  {
    return new ImageDimension(getWidth(), getHeight());
  }

  public ImageMetaData convert(File targetFile,
                               ImageDimension imageDimension,
                               String additionalParameters,
                               String targetFormat)
      throws IOException
  {
    return convert(targetFile,
                   imageDimension.getWidth(),
                   imageDimension.getHeight(),
                   additionalParameters,
                   targetFormat);
  }

  /**
   * Convert the wrapped file to the given width and format while maintaining the aspect ratio.
   */
  public ImageMetaData convertByWidth(File targetFile,
                                      int scaledWidth,
                                      String additionalParameters,
                                      String targetFormat)
      throws IOException
  {
    return convert(targetFile,
                   scaledWidth,
                   getHeight() * scaledWidth / getWidth(),
                   additionalParameters,
                   targetFormat);
  }

  /**
   * Convert the wrapped file to the given height and format while maintaining the aspect ratio;
   */
  public ImageMetaData convertByHeight(File targetFile,
                                       int scaledHeight,
                                       String additionalParameters,
                                       String targetFormat)
      throws IOException
  {
    return convert(targetFile,
                   getWidth() * scaledHeight / getHeight(),
                   scaledHeight,
                   additionalParameters,
                   targetFormat);
  }

  /**
   * Convert the wrapped file to the largest size within the given bounding box while maintaining
   * the aspect ratio.
   */
  public ImageMetaData convertByBox(File targetFile,
                                    int scaledWidth,
                                    int scaledHeight,
                                    String additionalParameters,
                                    String targetFormat)
      throws IOException
  {
    double thisAspect = getWidth() / getHeight();
    double scaledAspect = scaledWidth / scaledHeight;
    if (thisAspect < scaledAspect) {
      scaledWidth = getWidth() * scaledHeight / getHeight();
    } else {
      scaledHeight = getHeight() * scaledWidth / getWidth();
    }
    return convert(targetFile, scaledWidth, scaledHeight, additionalParameters, targetFormat);
  }

  /**
   * Convert the wrapped file to the explicitly specified scaledWidth and scaledHeight with the
   * given format.
   */
  public ImageMetaData convert(File targetFile,
                               int scaledWidth,
                               int scaledHeight,
                               String additionalParameters,
                               String targetFormat)
      throws IOException
  {
    if (getWidth() == scaledWidth & getHeight() == scaledHeight
        & getFormat().equals(targetFormat)) {
      IoUtil.unixLn(getFile(), targetFile, true, true);
    } else {
      IoUtil.execWithVerboseErrors(getConvertArray(getFile(),
                                                   targetFile,
                                                   scaledWidth,
                                                   scaledHeight,
                                                   true,
                                                   additionalParameters,
                                                   targetFormat));
    }
    // TODO: (#261) This assumes that the above ImageMagick conversion has done
    // what it
    // said that it would and therefore we don't need to run an identify on the
    // resulting file. Time will tell whether this is overzealous. Using the
    // java wrapper around IM will probably give us the results directly....
    return new ImageMetaData(scaledWidth,
                             scaledHeight,
                             targetFile,
                             targetFile.getParent(),
                             targetFormat,
                             "",
                             "",
                             "",
                             "",
                             "");
  }
}
