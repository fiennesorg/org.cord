package org.cord.image;

import java.io.File;
import java.io.IOException;

/**
 * Objects that are capable of examining a File and extracting the ImageMetaData about the image
 * that the File represents.
 * 
 * @author alex
 */
public interface ImageMetaDataFactory
{
  public ImageMetaData getImageMetaData(File file)
      throws IOException;
}