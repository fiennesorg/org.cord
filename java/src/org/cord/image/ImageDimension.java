package org.cord.image;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.MoreObjects;

public class ImageDimension
{
  private final int __width;
  private final int __height;

  public ImageDimension(int width,
                        int height)
  {
    checkArgument(width > 0, "width must be greater than 0");
    checkArgument(height > 0, "height must be greater than 0");
    __width = width;
    __height = height;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("width", getWidth())
                      .add("height", getHeight())
                      .toString();
  }

  public int getWidth()
  {
    return __width;
  }

  public int getHeight()
  {
    return __height;
  }

  public static double getAspect(int width,
                                 int height)
  {
    return ((double) width) / ((double) height);
  }

  public double getAspect()
  {
    return getAspect(getWidth(), getHeight());
  }

  /**
   * Scale this ImageDimension to the scaledWidth scaling up or down as necessary.
   */
  public ImageDimension scaleToWidth(int scaledWidth)
  {
    checkArgument(scaledWidth > 0, "scaledWidth must be greater than 0");
    return new ImageDimension(scaledWidth, getHeight() * scaledWidth / getWidth());
  }

  /**
   * Scale this ImageDimension to the given maxWidth by scaling down if necessary.
   */
  public ImageDimension scaleToMaxWidth(int maxWidth)
  {
    return maxWidth < getWidth() ? scaleToWidth(maxWidth) : this;
  }

  /**
   * Scale this ImageDimension to the scaledHeight scaling up or down as necessary.
   */
  public ImageDimension scaleToHeight(int scaledHeight)
  {
    checkArgument(scaledHeight > 0, "scaledHeight must be greater than 0");
    return new ImageDimension(getWidth() * scaledHeight / getHeight(), scaledHeight);
  }

  /**
   * Scale this ImageDeimension to the maxHeight scaling down if necessary.
   */
  public ImageDimension scaleToMaxHeight(int maxHeight)
  {
    return maxHeight < getHeight() ? scaleToHeight(maxHeight) : this;
  }

  /**
   * Get the ImageDimension that is as close as possible to the target box scaling up or down as
   * necessary.
   */
  public ImageDimension scaleToBox(int scaledWidth,
                                   int scaledHeight)
  {
    if (getAspect() < getAspect(scaledWidth, scaledHeight)) {
      scaledWidth = getWidth() * scaledHeight / getHeight();
    } else {
      scaledHeight = getHeight() * scaledWidth / getWidth();
    }
    return new ImageDimension(scaledWidth, scaledHeight);
  }

  /**
   * Get the ImageDimension object that fits inside the target box, only scaling if the current
   * dimensions are too large for the box.
   */
  public ImageDimension scaleToMaxBox(int maxWidth,
                                      int maxHeight)
  {
    return maxWidth < getWidth() | maxHeight < getHeight() ? scaleToBox(maxWidth, maxHeight) : this;
  }

  private int gcd(int a,
                  int b)
  {
    if (b == 0) {
      return a;
    }
    return gcd(b, a % b);
  }

  /**
   * Get the aspect ratio of this ImageDimension expressed as a string in the "a:b:" format.
   */
  public String getAspectRatio()
  {
    int gcd = gcd(getWidth(), getHeight());
    return (getWidth() / gcd) + ":" + (getHeight() / gcd);
  }
}
