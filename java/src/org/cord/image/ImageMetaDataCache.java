package org.cord.image;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.cord.util.NamedImpl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class ImageMetaDataCache
  extends NamedImpl
  implements ImageMetaDataFactory
{
  private LoadingCache<File, ImageMetaData> __cache;

  public static final String CACHE_DEFAULT = "expireAfterAccess=10m, softValues";

  public ImageMetaDataCache(CacheBuilderSpec spec)
  {
    super("ImageMetaDataCache");
    __cache = CacheBuilder.from(spec).build(new CacheLoader<File, ImageMetaData>() {
      @Override
      public ImageMetaData load(File key)
          throws IOException
      {
        return ImageMetaData.getInstance(key);
      }
    });
  }

  public ImageMetaDataCache()
  {
    this(CacheBuilderSpec.parse(CACHE_DEFAULT));
  }

  @Override
  public ImageMetaData getImageMetaData(File file)
      throws IOException
  {
    try {
      return __cache.get(file);
    } catch (ExecutionException e) {
      throw new IOException(String.format("Error reading ImageMetaData from %s", file), e);
    }
  }
}