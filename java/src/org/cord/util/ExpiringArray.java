package org.cord.util;

/**
 * Experimental implementation of a sparse Array that supports expiry of values based on access
 * times, and expiry of Pages of values based on total access time. Not used in anger yet.
 */
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.atomic.AtomicReferenceArray;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class ExpiringArray<T>
{
  private final int __pageBitSize;
  private final int __pageBitMask;
  private final AtomicReferenceArray<Page> __pages;
  private final Function<Integer, T> __factory;

  public ExpiringArray(int pageBitSize,
                       int pageCount,
                       Function<Integer, T> factory)
  {
    Preconditions.checkState(pageBitSize > 0 & pageBitSize < 16,
                             "Illegal pageBitSize of %s",
                             Integer.valueOf(pageBitSize));
    __pageBitSize = pageBitSize;
    __pageBitMask = (1 << __pageBitSize) - 1;
    __pages = new AtomicReferenceArray<Page>(pageCount);
    __factory = Preconditions.checkNotNull(factory, "factory");
  }

  public void expire(long timestamp)
  {
    final int l = __pages.length();
    for (int i = 0; i < l; i++) {
      Page page = __pages.get(i);
      if (page != null) {
        if (page.getTimestamp() < timestamp) {
          __pages.set(i, null);
        } else {
          page.expire(timestamp);
        }
      }
    }
  }

  private Page getPage(int pageIndex)
  {
    Page page = __pages.get(pageIndex);
    if (page != null) {
      return page;
    }
    page = new Page();
    if (__pages.compareAndSet(pageIndex, null, page)) {
      return page;
    }
    return __pages.get(pageIndex);
  }

  public T get(int i)
  {
    return getPage(i >> __pageBitSize).get(i, i & __pageBitMask, System.currentTimeMillis());
  }

  public T lookup(int i)
  {
    int pageIndex = i >> __pageBitSize;
    Page page = __pages.get(pageIndex);
    if (page == null) {
      return null;
    }
    int valueIndex = i & __pageBitMask;
    return page.lookup(valueIndex);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("__pages", __pages).toString();
  }

  public class Page
  {
    private volatile long _timestamp = 0;
    private final AtomicReferenceArray<T> __values;
    private final AtomicLongArray __timestamps;

    private Page()
    {
      __values = new AtomicReferenceArray<T>(__pageBitMask);
      __timestamps = new AtomicLongArray(__pageBitMask);
    }

    public long getTimestamp()
    {
      return _timestamp;
    }

    protected T lookup(int localIndex)
    {
      return __values.get(localIndex);
    }

    protected T get(int i,
                    int localIndex,
                    long timestamp)
    {
      _timestamp = timestamp;
      T value = __values.get(localIndex);
      __timestamps.set(localIndex, timestamp);
      if (value != null) {
        return value;
      }
      value = __factory.apply(Integer.valueOf(i));
      if (__values.compareAndSet(localIndex, null, value)) {
        return value;
      }
      return __values.get(localIndex);
    }

    protected void expire(long timestamp)
    {
      for (int i = 0; i < __pageBitMask; i++) {
        if (__timestamps.get(i) < timestamp) {
          __values.set(i, null);
        }
      }
    }

    @Override
    public String toString()
    {
      return MoreObjects.toStringHelper(this).add("__values", __values).toString();
    }
  }

  public static void main(String[] args)
  {
    System.out.println(256 * 256);
    ExpiringArray<String> ea = new ExpiringArray<String>(4, 16, new Function<Integer, String>() {
      @Override
      public String apply(Integer input)
      {
        return "Value:" + input;
      }
    });
    System.out.println(ea.get(2));
    System.out.println(ea.get(4));
    System.out.println(ea.get(49));
    System.out.println(ea);
  }
}
