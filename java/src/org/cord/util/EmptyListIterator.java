package org.cord.util;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Implementation of ListIterator that always has nothing in it.
 */
public class EmptyListIterator<T>
  implements ListIterator<T>
{
  private static final ListIterator<Object> INSTANCE = new EmptyListIterator<Object>();

  /**
   * Get an instance of EmptyListIterator.
   * 
   * @return An instance!
   */
  @SuppressWarnings("unchecked")
  public static <T> ListIterator<T> getInstance()
  {
    return (ListIterator<T>) INSTANCE;
  }

  private EmptyListIterator()
  {
  }

  @Override
  public void add(T t)
  {
    throw new UnsupportedOperationException();
  }

  /**
   * Check to see if there is another object in the EmptyIterator (obviously not).
   * 
   * @return false.
   */
  @Override
  public boolean hasNext()
  {
    return false;
  }

  /**
   * @return false
   */
  @Override
  public boolean hasPrevious()
  {
    return false;
  }

  /**
   * @throws NoSuchElementException
   *           Always!
   */
  @Override
  public T next()
      throws NoSuchElementException
  {
    throw new NoSuchElementException();
  }

  /**
   * @throws NoSuchElementException
   *           Always!
   */
  @Override
  public T previous()
      throws NoSuchElementException
  {
    throw new NoSuchElementException();
  }

  @Override
  public int nextIndex()
  {
    return 0;
  }

  @Override
  public int previousIndex()
  {
    return -1;
  }

  /**
   * Unsupported operation.
   * 
   * @throws java.lang.UnsupportedOperationException
   *           Always!
   */
  @Override
  public void remove()
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public void set(T o)
  {
    throw new UnsupportedOperationException();
  }
}
