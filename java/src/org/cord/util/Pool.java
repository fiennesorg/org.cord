package org.cord.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Pools are used to hold objects that you want to reuse and which have a waiting/busy/waiting
 * cycle. A Pool keeps track of which objects are currently being used, when they are finished with
 * and if they are still functional.
 * <p>
 * The core implementation of Pool will not actually do anything as yet (that's why it's abstract)
 * because it doesn't understand what data types or behaviours are required. In order to create a
 * usable Pool, you must extend it with the necessary information methods, namely (optional methods
 * in brackets):
 * <ul>
 * <li><code>(resetObject)</code>
 * <li><code>createObject</code>
 * <li><code>destroyObject</code>
 * <li><code>(validateObject)</code>
 * <li><code>(maintenanceMethod)</code>
 * </ul>
 * The basic behaviour of an Pool is that it creates a minimum number of objects automatically,
 * which then wait for requests for use. Once an object is in use then it is not available for other
 * uses. When an object is returned, it is passed through one or more of <code>validateObject</code>
 * (to check it still works) and <code>resetObject</code> (to remove any legacy data from the last
 * use). If the object fails this then it is <code>destroyObject</code>ed and a new object is
 * <code>createObject</code>ed in its place. The free object is now permitted to rejoin the list of
 * available objects.
 * <p>
 * If demand for the objects outstrips the availability, then it is possible for the Pool to create
 * some new temporary objects up to a set maximum number of objects. These "extra" objects will hang
 * around for a bit to see if they are required again, and if not then the Pool will gradually
 * reduce its size back down to its default size. In this way, a Pool should naturally configure
 * itself to handle the situation that is required of it automatically without wasting system
 * resources.
 * <p>
 * There is a demand placed on the clients of the Pool if the operational consistency is going to be
 * maintained, namely:- <blockquote><b>Always purge all references to objects obtained from the Pool
 * when you check the objects back in</b></blockquote> If not, then the next client could check out
 * an object which they think that they have unique rights to and be a little bit surprised when a
 * previous client continues to manipulate 'their' object...
 */
public abstract class Pool
  implements Runnable
{
  // the minimum number of objects in the pool
  private final int __minSize;

  // the maximum size of the pool under stress
  private final int __maxSize;

  // the current total number of objects in the pool
  // (busy + idle)
  private int _totalCount;

  // the ratio by which the pool will shrink after each
  // maintenance pause when left alone.
  private final float __settleRatio;

  // the length of time in milliseconds that the pool takes
  // to start shrinking.
  private final long __maintenancePeriod;

  // random object used to deal with synchronization issues
  // when shrinking the pool
  private final Object __maintenanceSync = new Object();

  // all the objects that are currently "free"
  private final Object[] __availableElements;

  // record of how many objects there are that are "free"
  // necessary cos the availableElements array is not always
  // completely full.
  private int _availableCount;

  // Bag of all the objects that are currently checked out.
  private final ArrayList<Object> __busyElements;

  // global flag for communicating with the thread to say if the
  // Pool must be shutdown.
  private boolean _isShutdown = false;

  // flag to say if validateObject should be run when objects
  // are returned via checkIn.
  private final boolean __validateOnCheckIn;

  // flag to say if resetObject should be run when objects
  // are returned via checkIn
  private final boolean __resetOnCheckIn;

  // flag to say if everything has been started up and ready
  // for action. All methods will freak out if startUp hasn't
  // been called.
  private boolean _isInitialised = false;

  // flag to set the ordering of the maintenance - destroy
  // operations.
  private final boolean __maintainBeforeDestroy;

  // flag to say if the housekeeping method should validate all
  // available non-busy objects.
  private final boolean __validateInHousekeeping;

  private final Thread __housekeepingThread;

  /**
   * Create a new abstract Pool. All containers for the Pool are initilised, but the initial set of
   * available object is not created. This is to permit extended versions of the Pool to perform
   * configuration of the <code>createObject</code> and <code>destroyObject</code> <i>before</i>
   * they are called. If this creator had gone ahead and initilised the available elements, then
   * there would not be a valid startup point.
   * <p>
   * It is therefore essential that the startup sequence for an extension of Pool's creator must
   * consist of the following steps:-
   * <ul>
   * <li>call <code>super(loads_of_params)</code> which executes this setup method.
   * <li>configure any internal parameters that will be used by your implementations of create and
   * destroy object.
   * <li>call <code>startUp()</code> to actually set the ball in motion.
   * </ul>
   * If you don't call the startUp method, then all "practical" methods will throw
   * RuntimeExceptions. You have been warned...
   * 
   * @param minSize
   *          The number of Objects that will <i>always</i> be in existence inside the Pool. Must be
   *          greater than 0!!!
   * @param maxSize
   *          The maximum size that the Pool can grow to. Must be >= minSize!!!
   * @param maintenancePeriod
   *          The time in milliseconds that the maintenance thread should pause for between
   *          housekeeping operations. If it is <= 0 then the maintenance thread will not start and
   *          the client is in full manual control of Pool maintenance (call
   *          <code>performMaintenance()</code> to instigate housekeeping)
   * @param settleRatio
   *          The amount which the excess ratio shrinks each time that housekeeping is performed.
   *          This must lie in the range 0.0 --> 1.0. If set to 1.0, then all the excess objects
   *          will be destroyed in each housekeeping action. If set to 0.1 then 10% of the objects
   *          will be destroyed. This therefore lets you control the rate at which the Pool returns
   *          to a normal size to deal with "bursting" in resource management.
   * @param maintenancePriority
   *          The Priority at which the maintenance thread will run at. It is probably worth having
   *          this at a reasonably high priority because:
   *          <ul>
   *          <li>The thread spends most of it's time asleep and therefore shouldn't affect
   *          performance greatly.
   *          <li>If your resources are costly and you really want them to be destroyed at the
   *          specified rate then this thread <i>must</i> interrupt the other busy threads.
   *          </ul>
   * @param validateOnCheckIn
   *          If true then all objects are passed through <code>validateOnCheckIn</code> when they
   *          are returned to the Pool.
   * @param validateInHousekeeping
   *          If true then the housekeeping method will automatically pass all available objects
   *          through the <code>validateObject</code> method. This is only really useful if you have
   *          turned off <code>validateOnCheckIn</code> and you are running your housekeeping thread
   *          on a lowish priority so that things only get run in periods of comparative calm
   *          <i>or</i> if your objects might pass away (spontaneously) while they are inside the
   *          Pool and you want to try and catch this situation.
   * @param resetOnCheckIn
   *          If true then all objects are passed through <code>resetObject</code> when they are
   *          returned to the Pool.
   * @param maintainBeforeDestroy
   *          If true then the <code>maintenanceMethod</code> will be called before the excess
   *          objects are destroyed in housekeeping, if false then it will be afterwards.
   * @throws java.lang.IllegalArgumentException
   *           If any of the parameters are nonsensical (rather than waiting for unpredictable
   *           behaviour at a later date).
   */
  public Pool(int minSize,
              int maxSize,
              long maintenancePeriod,
              float settleRatio,
              int maintenancePriority,
              boolean validateOnCheckIn,
              boolean validateInHousekeeping,
              boolean resetOnCheckIn,
              boolean maintainBeforeDestroy)
  {
    super();
    if (minSize < 1 || maxSize < minSize || maintenancePeriod < 1 || settleRatio <= 0.0
        || settleRatio > 1.0)
      throw new IllegalArgumentException();
    __minSize = minSize;
    __maxSize = maxSize;
    __availableElements = new Object[maxSize];
    __busyElements = new ArrayList<Object>(maxSize);
    _totalCount = minSize;
    _availableCount = minSize;
    __maintenancePeriod = maintenancePeriod;
    __settleRatio = settleRatio;
    __validateOnCheckIn = validateOnCheckIn;
    __validateInHousekeeping = validateInHousekeeping;
    __resetOnCheckIn = resetOnCheckIn;
    __maintainBeforeDestroy = maintainBeforeDestroy;
    __housekeepingThread = new Thread(this, "Pool");
    __housekeepingThread.setPriority(maintenancePriority);
  }

  /**
   * Switch this Pool into "active" state. Until this function is called, all functional methods
   * will throw RuntimeExceptions. This function also creates the minimum number of objects to
   * satisfy the virgin Pool and starts the maintenance thread.
   */
  public final void startUp()
  {
    for (int i = 0; i < __minSize; i++) {
      __availableElements[i] = createObject();
    }
    _isInitialised = true;
    __housekeepingThread.start();
  }

  // private method that freaks out if the init flag hasn't been
  // setup properly (by startUp())
  private void checkInit(boolean checkForShutdown)
  {
    if (!_isInitialised) {
      throw new IllegalStateException("Cannot use Pool before startUp()");
    }
    if (checkForShutdown && _isShutdown) {
      throw new IllegalStateException("Cannot use Pool after shutdown()");
    }
  }

  /**
   * Attempt to grab an Object from the Pool. There are 3 fundamental modes of behaviour for this
   * method:-
   * <ul>
   * <li>There is an available Object all ready in the Pool. The Object is returned immediately.
   * <li>There are no available objects, but the Pool still has space for expansion. A new Object is
   * created and then returned.
   * <li>There are no available objects and the Pool is at maximum size. The thread will block until
   * an object is made available.
   * </ul>
   * Therefore all calls <i>will</i> eventually return an Object, but some might wait slightly
   * longer than others.
   * 
   * @return An Object from the Pool for you to play with.
   */
  public final synchronized Object checkOut(String reason)
  {
    if (DebugConstants.DEBUG_SQL_THREADCONNECTIONS) {
      System.err.println("--> " + reason + " - " + Thread.currentThread());
    }
    checkInit(true);
    // easy one first: there is an available resource already waiting...
    if (_availableCount > 0) {
      Object object = __availableElements[_availableCount - 1];
      __busyElements.add(object);
      _availableCount--;
      return object;
    }
    // next harder: there is still space to create a new resource...
    if (_totalCount < __maxSize) {
      System.err.println(this + " creating new Object for " + reason + "...");
      Object object = createObject();
      __busyElements.add(object);
      _totalCount++;
      return object;
    }
    // nothings worked: have to wait for an object to become available...
    try {
      System.err.println(this + " waiting for an available Object for " + reason + "...");
      this.wait(10000);
      System.err.println(this + " waiting completed...");
    } catch (InterruptedException e) {
      System.err.println(this + " waiting interrupted...");
    }
    // note that we go recursive because it ENSURES that we have
    // the right to the object (ish).
    return checkOut(reason + "+");
  }

  /**
   * Return an Object to the Pool once you have finished with it. <u>Make sure that you have
   * <i>finished</i> with it.</u> If the object is not in the list of valid busy objects that it
   * will not let you return it. There will be an optional validation (to make sure you haven't
   * broken the object) and reset (to remove any legacy data) operation as set in the creator of the
   * Pool. If there any waiting threads after objects then they will get notified that the object is
   * available.
   * 
   * @param object
   *          The Object that you are putting back.
   * @return true if the object was successfully checked in, false if the object never came from the
   *         Pool in the first place.
   */
  public final synchronized boolean checkIn(Object object,
                                            String reason)
  {
    if (DebugConstants.DEBUG_SQL_THREADCONNECTIONS) {
      System.err.println("<-- " + reason + " - " + Thread.currentThread());
    }
    checkInit(true);
    // make sure that the object is really valid and busy...
    if (!__busyElements.remove(object)) {
      return false;
    }
    if (__validateOnCheckIn) {
      object = validateObject(object);
    }
    if (__resetOnCheckIn) {
      object = resetObject(object);
    }
    __availableElements[_availableCount] = object;
    _availableCount++;
    this.notifyAll();
    return true;
  }

  /**
   * Allow you to politely return an Object that you've broken. Well, shit happens occasionally and
   * this way it saves the overhead of letting the system catch the error (always assuming that
   * you've turned on error checking). Obviously it is more efficient to turn off error checking and
   * catch the wierdness in the objects in your own code and act accordingly. Anway - what this
   * method does is:-
   * <ul>
   * <li>check to see if the offending Object was ever legally checked out. If not then return
   * false.
   * <li>invoke destroyObject on the object in class the client thread had broken it but not fully
   * tidied up the finalisation of the object.
   * <li>create a new object to go in place of the old dead object.
   * <li>notify anyone waiting for a new object.
   * </ul>
   * 
   * @param object
   *          The broken object.
   * @return true if all ok, false if dodgy object.
   * @see #destroyObject(Object)
   */
  public final synchronized boolean checkInBroken(Object object,
                                                  String reason)
  {
    System.err.println("<-- !!! " + reason);
    checkInit(true);
    // ok. the object is assumed broken so try and pull
    // it out of the busy pool...
    if (!__busyElements.remove(object)) {
      return false;
    }
    // it should definately be destroyed in case there is housekeeping
    // that the checkout thread didn't do...
    destroyObject(object);
    // then create a new object and make it available
    // for other threads...
    __availableElements[_availableCount] = createObject();
    _availableCount++;
    this.notifyAll();
    return true;
  }

  /**
   * Optionally override this method to permit an implementation of a Pool reset legacy data in an
   * object. Currently it does nothing (ie returns its input). This will be called (if enabled)
   * automatically on checkIn.
   * 
   * @param obj
   *          The object to reset
   * @return The reset object. If this cannot be performed then the result of a call to
   *         <code>createObject()</code> or equivalent.
   */
  protected Object resetObject(Object obj)
  {
    return obj;
  }

  /**
   * <b>Compulsory</b> override this method to create a new object for the Pool. The object should
   * be immediately ready for use.
   * 
   * @return A new virgin object.
   */
  protected abstract Object createObject();

  /**
   * Destroy an object before it is dropped by the Pool. Any resources held by the Object should be
   * released so that garbage collection can be cleanly performed. Current implementation does
   * absolutely nothing to the object. If this is OK then leave it as such.
   * 
   * @param obj
   *          The Object to destroy.
   */
  protected abstract void destroyObject(Object obj);

  /**
   * Optionally override this method to validate an object before it is reinserted into the list of
   * available objects in the Pool. If your Objects handled by the Pool are potentially breakable by
   * bad client usage then you should overide this method to check that the functionality of the
   * object is still OK. Note that this can still be sidestepped by creating the Pool with
   * validation switched off. This method will be called on all check in operations so it should be
   * as optimised as possible. This implementation does nothing and returns the object directly.
   * 
   * @param obj
   *          The object to validate
   * @return The validated object. If the object has failed the validation then the return value
   *         should be a call to <code>createObject</code>
   */
  protected Object validateObject(Object obj)
  {
    return obj;
  }

  /**
   * Optionally override this method if there is some <i>global</i> work that needs to be run on
   * your Pool whenever housekeeping is performed. If during this operation, it turns out that the
   * Pool has become unhealthy, then <code>shutdown()</code> should be called and then the
   * housekeeping method will catch this condition and abandon the operation. This method can be
   * inserted before or after the body of the housekeeping operation depending on Pool creation
   * parameters. This implementation does nothing at all.
   */
  protected void maintenanceMethod()
  {
  }

  // wait for the required period of time (making sure that you are not
  // shutdown before-hand. waits on __maintenanceSync to ensure that
  // shutdown() can interrupt the operation cleanly and shutdowns are
  // as fast as possible.
  private void maintenancePause()
  {
    synchronized (__maintenanceSync) {
      if (!_isShutdown)
        try {
          __maintenanceSync.wait(__maintenancePeriod);
        } catch (InterruptedException ie) {
        }
    }
  }

  /**
   * Find out how many available objects are currently available in the Pool. Note that this doesn't
   * include the potentially creatable objects in an expandable Pool. If you want the true size then
   * you should call <code>getPotentiallyAvailable()</code>.
   * 
   * @return The number of Objects
   */
  public synchronized final int getAvailable()
  {
    return _availableCount;
  }

  /**
   * Find out how many objects are potentially available from this Pool. Note that some of the
   * objects may not currently exist and will be created on demand when needed.
   * 
   * @return The number of Objects
   */
  public synchronized final int getPotentiallyAvailable()
  {
    return __maxSize - __busyElements.size();
  }

  /**
   * Return the maximum number of objects that it is possible to store in this pool. Please note
   * that this does <i>not</i> take into account the presence of created or non-created objects.
   * This is purely the maximum number of objects that could ever be theoretically created.
   * 
   * @return potential object count.
   */
  public final int getMaxCount()
  {
    return __maxSize;
  }

  /**
   * Find out how many objects are currently in the Pool including both busy and available objects.
   * 
   * @return The number of Objects
   */
  public synchronized final int getTotalCount()
  {
    return _totalCount;
  }

  /**
   * Find out how many objects are currently checked out of this Pool.
   * 
   * @return The number of Objects.
   */
  public synchronized final int getBusyCount()
  {
    return __busyElements.size();
  }

  /**
   * Perform Pool housekeeping. This will perform the following steps:-
   * <ul>
   * <li>Perform <code>maintenanceMethod()</code> for global Pool maintenance. This is configurable
   * as to whether it occurs at the start or end of the operation.
   * <li>Calculate the excess number of objects in the Pool (from the number above the minimumsize
   * and the float ratio) and destroy all of these objects.
   * </ul>
   * You may call this method manually (if you are not running threaded housekeeping) or it will be
   * triggered automatically.
   * 
   * @return The number of objects destroyed, or -1 if the Pool has become shutdown during the
   *         operation.
   */
  public synchronized final int performMaintenance()
  {
    if (__maintainBeforeDestroy) {
      maintenanceMethod();
    }
    if (!_isShutdown) {
      int excess = (int) (0.5 + (_availableCount - __minSize) * __settleRatio);
      for (int i = 0; i < excess; i++) {
        _availableCount--;
        destroyObject(__availableElements[_availableCount]);
        __availableElements[_availableCount] = null;
        _totalCount--;
      }
      if (__validateInHousekeeping) {
        for (int i = 0; i < _availableCount; i++) {
          __availableElements[i] = validateObject(__availableElements[i]);
        }
      }
      if (!__maintainBeforeDestroy) {
        maintenanceMethod();
      }
      if (_isShutdown) {
        return -1;
      } else {
        return excess;
      }
    }
    return -1;
  }

  /**
   * Execute the housekeeping loop. If the maintenancePeriod is <= 0 then just abort immediately,
   * otherwise loop until shutdown performing pause then <code>performMaintenance</code>. Always
   * starts with pause.
   */
  @Override
  public final void run()
  {
    checkInit(false);
    if (__maintenancePeriod > 0) {
      if (!_isShutdown) {
        maintenancePause();
      }
      while (!_isShutdown) {
        synchronized (__maintenanceSync) {
          performMaintenance();
          maintenancePause();
        }
      }
    }
  }

  /**
   * Shutdown this Pool. The following operations are performed:-
   * <ul>
   * <li>The maintenance thread is interrupted if it is paused state. This will then cause it to
   * drop out and self-destruct.
   * <li>Call destroyObject on <i>all</i> objects in the Pool (busy or resting). Cruel but such is
   * life. Death is ugly.
   * <li>Wipe all references to Objects from the Pool. This should help garbage collection proceed
   * as cleanly and as smoothly as possible.
   * </ul>
   */
  public final synchronized void shutdown()
  {
    checkInit(false);
    if (_isShutdown) {
      return;
    }
    _isShutdown = true;
    synchronized (__maintenanceSync) {
      __maintenanceSync.notify();
    }
    for (int i = 0; i < _availableCount; i++) {
      destroyObject(__availableElements[i]);
      __availableElements[i] = null;
    }
    Iterator<Object> busyList = __busyElements.iterator();
    while (busyList.hasNext()) {
      destroyObject(busyList.next());
    }
    __busyElements.clear();
    subShutdown();
  }

  protected void subShutdown()
  {
  }

  /**
   * Find out if this Pool has been shutdown.
   * 
   * @return true if the pool is shutdown.
   */
  public final synchronized boolean isShutdown()
  {
    return _isShutdown;
  }
}
