package org.cord.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.google.common.base.Preconditions;

public class Dates
{
  private final static ThreadLocal<Calendar> __threadLocalCalendar = new ThreadLocal<Calendar>() {
    @Override
    protected Calendar initialValue()
    {
      return Calendar.getInstance();
    }
  };

  /**
   * Build an efficient ThreadLocal SimpleDateFormat supplier. The method will create a template
   * SimpleDateFormat initially and then clone that as required by Threads so this method will raise
   * an exception if format is not suitable.
   * 
   * @param format
   *          The compulsory format that will be implemented by the SimpleDateFormat
   * @throws IllegalArgumentException
   *           if we cannot parse format
   * @deprecated in preference of {@link ThreadLocalSimpleDateFormat}
   */
  @Deprecated
  public static ThreadLocal<SimpleDateFormat> getSimpleDateFormat(String format)
  {
    final SimpleDateFormat sdf = new SimpleDateFormat(format);
    ThreadLocal<SimpleDateFormat> supplier = new ThreadLocal<SimpleDateFormat>() {
      @Override
      protected SimpleDateFormat initialValue()
      {
        return (SimpleDateFormat) sdf.clone();
      }
    };
    supplier.set(sdf);
    return supplier;
  }

  /**
   * Get a Calendar instance that is unique to this Thread and which therefore doesn't need to by
   * synchronized. Note that classes should complete usage of the Calendar in a single visible
   * segment before handing over control to other codebases in case they utilise the Calendar as
   * well and change the values.
   */
  public static Calendar getCalendar()
  {
    return __threadLocalCalendar.get();
  }

  /**
   * Use the given DateFormat to parse a string without throwing an exception with defined return
   * values for null strings and unparseable strings. The method is thread safe.
   * 
   * @param obj
   *          The object to be parsed. If this is a Date already then it will be formatted and then
   *          re-parsed with the format object, thereby forcing it to be quantised to the accuracy
   *          specified by the format.
   * @param nullValue
   *          The optional Date that should be returned if obj is null.
   * @param parseValue
   *          The optional Date that should be returned if it is not possible to parse the string
   *          representation of obj with the given format.
   */
  public final static Date parse(DateFormat format,
                                 Object obj,
                                 Date nullValue,
                                 Date parseValue)
  {
    Preconditions.checkNotNull(format, "format");
    if (obj == null) {
      return nullValue;
    }
    if (obj instanceof Date) {
      synchronized (format) {
        try {
          return format.parse(format.format((Date) obj));
        } catch (ParseException e) {
          throw new LogicException(String.format("%s is unable to format and parse %s",
                                                 format,
                                                 obj),
                                   e);
        }
      }
    }
    String str = obj.toString();
    if (str.length() == 0) {
      return parseValue;
    }
    ParsePosition pp = new ParsePosition(0);
    Date value = null;
    synchronized (format) {
      value = format.parse(str, pp);
    }
    return value == null ? parseValue : value;
  }

  public final static Date parse(ThreadLocalSimpleDateFormat threadLocalFormat,
                                 Object obj,
                                 Date nullValue,
                                 Date parseValue)
  {
    SimpleDateFormat format = threadLocalFormat.get();
    Preconditions.checkNotNull(format, "format");
    if (obj == null) {
      return nullValue;
    }
    if (obj instanceof Date) {
      try {
        return format.parse(format.format((Date) obj));
      } catch (ParseException e) {
        throw new LogicException(String.format("%s is unable to format and parse %s", format, obj),
                                 e);
      }
    }
    String str = obj.toString();
    if (str.length() == 0) {
      return parseValue;
    }
    ParsePosition pp = new ParsePosition(0);
    Date value = null;
    synchronized (format) {
      value = format.parse(str, pp);
    }
    return value == null ? parseValue : value;
  }

  /**
   * Use the given SimpleDateFormat to parse the String representation of an Object and return null
   * if it isn't possible to complete the operation.
   * 
   * @param format
   *          The compulsory format to use to parse the obj
   * @param obj
   *          The optional obj to parse
   * @return The parsed Date or null if it isn't possible.
   */
  public final static Date parse(DateFormat format,
                                 Object obj)
  {
    return parse(format, obj, null, null);
  }

  public final static Date parse(ThreadLocalSimpleDateFormat format,
                                 Object obj)
  {
    return parse(format, obj, null, null);
  }

  /**
   * Try and parse an Object using a sucession of DateFormats until one is found that can parse it,
   * or return null if there is no success.
   */
  public final static Date parse(Object obj,
                                 DateFormat... formats)
  {
    for (DateFormat format : formats) {
      Date d = parse(format, obj);
      if (d != null) {
        return d;
      }
    }
    return null;
  }

  /**
   * Use the given SimpleDateFormat to format a Date without throwing exceptions and with definable
   * behaviour for null dates.
   * 
   * @param format
   *          The forat to use. Not null
   * @param date
   *          The optional date to format
   * @param nullValue
   *          The value to return if date is null
   * @return The formatted date or nullValue if date was null
   */
  public final static String format(DateFormat format,
                                    Date date,
                                    String nullValue)
  {
    Preconditions.checkNotNull(format, "format");
    if (date == null) {
      return nullValue;
    }
    synchronized (format) {
      return format.format(date);
    }
  }

  public final static String format(DateFormat format,
                                    Date date)
  {
    synchronized (format) {
      return format.format(date);
    }
  }

  /**
   * Convert an optional Object into a Date if possible. This will attempt casting and conversion of
   * a Number into a timestamp and therefore a Date, but it doesn't do any parsing of Strings at
   * present.
   * 
   * @param obj
   *          The optional Object to convert.
   * @return The Date object if possible, or null if obj was null.
   * @throws IllegalArgumentException
   *           if it isn't possible to convert Object into a Date, but not if it is null.
   */
  public final static Date toDate(Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Date) {
      return (Date) obj;
    }
    if (obj instanceof Number) {
      return new Date(((Number) obj).longValue());
    }
    throw new IllegalArgumentException(String.format("Unable to convert %s of class %s into a Date",
                                                     obj,
                                                     obj.getClass()));
  }
}
