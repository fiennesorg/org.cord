package org.cord.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Abstract Thread that reads from a BufferedReader and writes all output to a definable location
 * which is defined in the implementing class. This is useful in situations when it is important
 * that a data source must be read without blocking, for example when dealing with a Process that
 * generates a lot of output.
 * 
 * @author alex
 */
public abstract class ThreadedStreamConsumer
  extends Thread
{
  private final BufferedReader __bufferedReader;

  public ThreadedStreamConsumer(BufferedReader bufferedReader)
  {
    __bufferedReader = bufferedReader;
  }

  @Override
  public void run()
  {
    // String line = null;
    int c = -1;
    try {
      while (!isInterrupted() && (c = __bufferedReader.read()) != -1) {
        consume((char) c);
      }
    } catch (IOException ioEx) {
      handle(ioEx);
    }
    close();
  }

  protected abstract void consume(char c);

  protected abstract void close();

  // protected abstract void consume(String line);

  protected abstract void handle(IOException ioEx);

  /**
   * Get an instance that redirects input to the given PrintStream.
   */
  public static ThreadedStreamConsumer getInstance(BufferedReader bufferedReader,
                                                   final PrintStream printStream)
  {
    return new ThreadedStreamConsumer(bufferedReader) {
      @Override
      protected void consume(char c)
      {
        if (printStream != null) {
          printStream.print(c);
        }
      }

      @Override
      protected void close()
      {
        printStream.close();
      }

      @Override
      protected void handle(IOException ioEx)
      {
        if (printStream != null) {
          ioEx.printStackTrace(printStream);
        }
      }
    };
  }

  /**
   * Get an instance that discards all data.
   */
  public static ThreadedStreamConsumer getInstance(BufferedReader bufferedReader)
  {
    return new ThreadedStreamConsumer(bufferedReader) {
      @Override
      protected void consume(char c)
      {
      }

      @Override
      protected void handle(IOException ioEx)
      {
      }

      @Override
      protected void close()
      {
      }
    };
  }

  /**
   * Get an instance that appends all data onto the supplied StringBuffer
   */
  public static ThreadedStreamConsumer getInstance(BufferedReader bufferedReader,
                                                   final StringBuilder buffer)
  {
    if (buffer == null) {
      return getInstance(bufferedReader);
    }
    return new ThreadedStreamConsumer(bufferedReader) {
      @Override
      protected void consume(char c)
      {
        buffer.append(c);
      }

      @Override
      protected void handle(IOException ioEx)
      {
        buffer.append(ExceptionUtil.printStackTrace(ioEx)).append('\n');
      }

      @Override
      protected void close()
      {
      }
    };
  }
}
