package org.cord.util;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class CronManager
  implements Runnable
{
  private Set<CronJob> _cronJobs = new HashSet<CronJob>();

  private SortedMap<Date, CronJob> _nextJobs = new TreeMap<Date, CronJob>();

  private Thread _cronTimer;

  private boolean _isAlive = true;

  private boolean _isRunning = false;

  public CronManager(int threadPriority)
  {
    _cronTimer = new Thread(this, "CronManager");
    _cronTimer.setPriority(threadPriority);
    _cronTimer.start();
  }

  public synchronized void add(CronJob job)
  {
    _cronJobs.add(job);
    queue(job, new Date());
  }

  private void queue(CronJob job,
                     Date now)
  {
    Date executionTime = job.getNextExecutionTime(now);
    if (executionTime.compareTo(now) <= 0) {
      job.shutdown();
      _cronJobs.remove(job);
    } else {
      _nextJobs.put(executionTime, job);
    }
  }

  public synchronized void setIsRunning(boolean isRunning)
  {
    if (_isRunning != isRunning) {
      _isRunning = isRunning;
      notifyAll();
    }
  }

  @Override
  public void run()
  {
    while (_isAlive) {
      Date now = new Date();
      long sleepTime = 0;
      synchronized (this) {
        if (_isRunning && _nextJobs.size() > 0) {
          sleepTime = _nextJobs.firstKey().getTime() - now.getTime();
        }
        if (sleepTime >= 0) {
          try {
            wait(sleepTime);
          } catch (InterruptedException iEx) {
            // not fussed, probably shutting down...
          }
        } else {
          if (_isAlive) {
            if (_nextJobs.size() > 0) {
              CronJob nextJob = _nextJobs.remove(_nextJobs.firstKey());
              nextJob.executeCronJob(now);
              queue(nextJob, now);
            }
          }
        }
      }
    }
  }

  public synchronized void shutdown()
  {
    _isAlive = false;
    _isRunning = false;
    notifyAll();
    clear();
  }

  public synchronized void clear()
  {
    _cronJobs.clear();
    _nextJobs.clear();
  }
}
