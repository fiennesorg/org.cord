package org.cord.util;

import com.google.common.base.Preconditions;

/**
 * Basically just an Object that is a bit more informative when you print it out, but which still
 * has the Object equality testing and therefore useful to use as a private key into shared Maps.
 * 
 * @author alex
 */
public class PrivateCacheKey
{
  private final String __toString;

  public PrivateCacheKey(String toString)
  {
    __toString = Preconditions.checkNotNull(toString, "toString");
  }

  @Override
  public String toString()
  {
    return __toString;
  }
}
