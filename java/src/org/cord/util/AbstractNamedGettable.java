package org.cord.util;

import com.google.common.base.Preconditions;

public abstract class AbstractNamedGettable
  extends AbstractGettable
  implements Named
{
  private final String __name;

  public AbstractNamedGettable(String name)
  {
    __name = Preconditions.checkNotNull(name, "name");
  }

  @Override
  public final String getName()
  {
    return __name;
  }

  @Override
  public int compareTo(Named o)
  {
    return getName().compareTo(o.getName());
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Named n = (Named) o;
    return getName().equals(n.getName());
  }

  @Override
  public int hashCode()
  {
    return getName().hashCode();
  }

  @Override
  public String toString()
  {
    return __name;
  }
}
