package org.cord.util;

import java.util.AbstractList;

import org.json.JSONArray;

import com.google.common.base.Preconditions;

/**
 * Immutable Collection view onto a wrapper JSONArray.
 * 
 * @author alex
 * @see JSONUtils#asImmutable(Object, boolean)
 */
public class JSONArrayList
  extends AbstractList<Object>
{
  private final JSONArray __jArr;
  private final boolean __exposesSource;

  public JSONArrayList(JSONArray jArr,
                       boolean exposesSource)
  {
    __jArr = Preconditions.checkNotNull(jArr, "jArr");
    __exposesSource = exposesSource;
  }

  public final boolean exposesSource()
  {
    return __exposesSource;
  }

  public JSONArray getSource()
  {
    if (exposesSource()) {
      return __jArr;
    }
    throw new IllegalStateException(this + " doesn't grant access to the contained JSONArray");
  }

  @Override
  public int size()
  {
    return __jArr.length();
  }

  @Override
  public Object get(int i)
  {
    Preconditions.checkPositionIndex(i, size());
    return JSONUtils.asImmutable(__jArr.opt(i), __exposesSource);
  }

}
