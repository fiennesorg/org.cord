package org.cord.util;

import com.google.common.base.Function;

public class EnumUtils
{
  /**
   * provide a Function that maps String to instances of an Enum by looking up the Enum using
   * valueOf. All errors are thrown.
   */
  public static <T extends Enum<T>> Function<String, T> valueOfFunction(final Class<T> enumClass)
  {
    return new Function<String, T>() {
      @Override
      public T apply(String input)
      {
        return Enum.valueOf(enumClass, input);
      }
    };
  }
}
