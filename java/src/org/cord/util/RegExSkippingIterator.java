package org.cord.util;

import java.util.Iterator;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

/**
 * Implementation of SkippingIterator that will pass any Object in the Iterator whose String
 * representation matches a given regular expression. Note that the passed Objects may not be
 * Strings themselves, they are only converted into Strings for the purpose of comparison with the
 * regular expression.
 * 
 * @author alex
 * @see StringUtils#toString(Object)
 */
public class RegExSkippingIterator<T>
  extends SkippingIterator<T>
{
  private final Pattern __pattern;

  public RegExSkippingIterator(Iterator<T> iterator,
                               String regex)
  {
    super(iterator);
    __pattern = Strings.isNullOrEmpty(regex) ? null : Pattern.compile(regex);
  }

  @Override
  protected boolean shouldSkip(T nextItem)
  {
    return __pattern == null ? true : !__pattern.matcher(StringUtils.toString(nextItem)).find();
  }
}
