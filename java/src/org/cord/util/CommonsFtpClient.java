package org.cord.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class CommonsFtpClient
  implements FtpClientImplementation
{
  public CommonsFtpClient()
  {
  }

  private void fail(String message,
                    Throwable nestedEx)
      throws IOException
  {
    IOException ioEx = new IOException(message);
    if (nestedEx != null) {
      ioEx.initCause(nestedEx);
    }
    throw ioEx;
  }

  private void log(PrintWriter log,
                   String message)
  {
    if (log != null) {
      log.println(message);
    }
  }

  @Override
  public void uploadFile(File localFile,
                         String remoteServer,
                         String remoteLogin,
                         String remotePassword,
                         String remoteDirectory,
                         String remoteFilename,
                         PrintWriter log)
      throws IOException
  {
    FTPClient ftpClient = new FTPClient();
    ftpClient.enterLocalPassiveMode();
    try {
      log(log, "Attempting connection to " + remoteServer);
      ftpClient.connect(remoteServer);
      ftpClient.enterLocalPassiveMode();
      log(log, "Connection attempt complete");
      if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
        fail("FTP server refused connection", null);
      }
      log(log, "Validated clean connection");
      if (!ftpClient.login(remoteLogin, remotePassword)) {
        fail("FTP server refused authentication", null);
      }
      log(log, "Logged in as " + remoteLogin);
      if (!ftpClient.changeWorkingDirectory(remoteDirectory)) {
        fail("FTP server refused to change directory into: " + remoteDirectory, null);
      }
      log(log, "Changed working directory to " + remoteDirectory);
      FileInputStream fis = null;
      try {
        fis = new FileInputStream(localFile);
        if (!ftpClient.storeFile(remoteFilename, fis)) {
          fail("FTP server was unable to store file", null);
        }
      } finally {
        if (fis != null) {
          fis.close();
        }
      }
      log(log, "...Commons FTP complete");
    } catch (Exception exception) {
      fail("Failure during FTP operation", exception);
    } finally {
      if (ftpClient.isConnected()) {
        try {
          ftpClient.disconnect();
        } catch (IOException ioEx) {
          ioEx.printStackTrace();
        }
      }
    }
  }
}
