package org.cord.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Utility method that provides a fully synchronized Object for storing a Mutable integer value.
 * This can then be used as a target for synchronization in it's own right as well.
 * 
 * @deprecated in preference of {@link AtomicInteger}
 */
@Deprecated
public class MutableInteger
{
  private int _value;

  public MutableInteger(int value)
  {
    _value = value;
  }

  /**
   * Get the value stored in this MutableInteger after obtaining a syncrhonization lock.
   */
  public synchronized int getValue()
  {
    return _value;
  }

  /**
   * Set the value storedin this MutableInteger after obtaining a syncrhonization lock.
   * 
   * @return The new value
   */
  public synchronized int setValue(int value)
  {
    _value = value;
    return _value;
  }

  /**
   * Add the given amount onto the value that is already stored in a syncrhonized fashion.
   * 
   * @return The new value
   */
  public synchronized int add(int amount)
  {
    return setValue(getValue() + amount);
  }

  @Override
  public String toString()
  {
    return "MutableInteger(" + _value + ")";
  }
}
