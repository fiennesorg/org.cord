package org.cord.util;

/**
 * A GettableSupplier produces a value of type T from a Gettable.
 */
public interface GettableSupplier<T>
{
  public T get(Gettable gettable);
}
