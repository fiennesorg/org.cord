package org.cord.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Extension of BufferedReader that enables a single Reader to be segmented into multiple "virtual"
 * BufferedReaders. Each time that the given segmentBreakLine is encountered in the source reader,
 * this is returned as a null line from the readLine() method which will be interpretted as an EOF
 * by the client. Providing that the client doesn't close() the SegmentedBufferedReader then the
 * next line after the segmentBreakLine will be returned by the next call to readLine(). If you want
 * to definitively check whether or not the input stream has reached the end then a call to read()
 * will be accurate in it's reporting of EOF status.
 */
public class SegmentedBufferedReader
  extends BufferedReader
{
  private final String __segmentBreakLine;

  /**
   * @param segmentBreakLine
   *          The String that will be interpretted as the virtual EOF when parsing the reader.
   */
  public SegmentedBufferedReader(Reader reader,
                                 String segmentBreakLine)
  {
    super(reader);
    __segmentBreakLine = segmentBreakLine;
  }

  @Override
  public String readLine()
      throws IOException
  {
    String line = super.readLine();
    if (__segmentBreakLine.equals(line)) {
      return null;
    } else {
      return line;
    }
  }
}
