package org.cord.util;

/**
 * An Object that is capable of providing a key that it should be indexed under in a cache.
 * 
 * @param <T>
 *          The type of the Object that is Cachable.
 * @author alex
 */
public interface Cachable<T>
{
  /**
   * Get the key (if any) that can be used to cache this Cachable Object. If the key is not defined
   * then the Object will not be eligible for caching. The key may be a publically known (eg a
   * constant) or reconstructable (eg a String) Object in which case other classes will be able to
   * resolve this Cachable from the cache, or it can be a private Object in which case it will
   * become harder to resolve (although not impossible because this method is public) and under the
   * control of the key generator.
   * 
   * @return The constant immutable key that this Cachable should be cached under or null if this
   *         Cachable doesn't want to be eligible for caching.
   */
  public Object getCacheKey();
}
