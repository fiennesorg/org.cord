package org.cord.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for implementing services that run asynchronously with the service requests. To
 * implement a QueuedService then the following steps should be followed:-
 * <ul>
 * <li>Define some extension of Object that represents the information required to describe a task.
 * This is the information that will be cached by this class until the task is ready to be
 * performed. This will be referred to as AbstractTask for now.
 * <li>Implement some kind of public addTask functionality that takes your AbstractTask and drops it
 * into addTask(task,priority).
 * <li>Implement processTask(task) to provide the functionality. THis will be passed instances of
 * AbstractTask as they are available for processing.
 * <li>If your implementation requires specific startup or shudown functionalities then these can be
 * tied in by overriding internalStart() or internalStop().
 * </ul>
 * <p>
 * The QueuedService maintains the concept of multiple priority queues and will always process the
 * oldest object from the highest priority queue next. Please note that different priority queues
 * are not processed in parallel, but instead function purely as a way of dynamically re-shuffling
 * the essentially linear queue.
 * <p>
 * The Priorities are assigned in reverse order with the highest priority being 0 and the lowest
 * priority being the highest number.
 * 
 * @see #addTask(Object,int)
 * @see #processTask(Object)
 * @see #internalStart()
 * @see #internalStop()
 */
public abstract class QueuedService
  extends Thread
  implements Shutdownable
{
  // priority (Integer) --> List of Tasks
  private final List<List<Object>> __tasks;

  private final int _priorityLevels;

  private boolean _isShutdown = false;

  /**
   * @param priorityLevels
   *          The number of different independant priority queues that this service should maintain.
   * @param threadPriority
   *          The threading priority that the processing thread of this QueuedService should run at.
   *          This should normally be a slightly lower priority than the thread that is delivering
   *          the requests.
   * @param autoStartThread
   *          If true then the processing thread will be started immediately. Normally this will be
   *          true, but take false if the condition of starting the thread is contingent on some
   *          other parameter. If you wish to start the thread directly then you can invoke
   *          sessionqueue.start().
   * @see Thread#setPriority(int)
   * @see Thread#start()
   */
  public QueuedService(String threadName,
                       int priorityLevels,
                       int threadPriority,
                       boolean autoStartThread)
  {
    super(threadName);
    if (priorityLevels < 1) {
      throw new IllegalArgumentException("priority levels of " + priorityLevels + " is illegal");
    }
    _priorityLevels = priorityLevels;
    __tasks = new ArrayList<List<Object>>();
    for (int i = 0; i < priorityLevels; i++) {
      __tasks.add(new ArrayList<Object>());
    }
    setPriority(threadPriority);
    if (autoStartThread) {
      start();
    }
  }

  @Override
  public void run()
  {
    internalStart();
    try {
      while (!_isShutdown) {
        try {
          processTask(waitForNextTask());
        } catch (InterruptedException ieEx) {
          _isShutdown = true;
        }
      }
    } finally {
      internalStop();
    }
  }

  protected void internalStart()
  {
  }

  protected void internalStop()
  {
  }

  @Override
  public synchronized void shutdown()
  {
    _isShutdown = true;
    notifyAll();
  }

  /**
   * Get the number of tasks that are currently awaiting processing for a given priority.
   * 
   * @param priority
   *          The priority of the tasks that you are want counted.
   * @return The number of tasks at the given priority. Will be >= 0.
   * @throws IndexOutOfBoundsException
   *           if the priority is not recognised.
   */
  public int getTaskCount(int priority)
  {
    return __tasks.get(priority).size();
  }

  /**
   * Get the total task count across all priorities.
   * 
   * @return The sum of all the getTaskCount(priority) values for all the defined priorities.
   */
  public int getTaskCount()
  {
    int taskCount = 0;
    for (int i = 0; i < _priorityLevels; i++) {
      taskCount += getTaskCount(i);
    }
    return taskCount;
  }

  protected synchronized Object getNextTask()
  {
    for (List<Object> tasks : __tasks) {
      if (tasks.size() > 0) {
        return tasks.remove(0);
      }
    }
    return null;
  }

  protected synchronized Object waitForNextTask()
      throws InterruptedException
  {
    Object result = getNextTask();
    while (result == null) {
      wait(10000);
      if (_isShutdown) {
        return null;
      }
      result = getNextTask();
    }
    return result;
  }

  protected synchronized void addTask(Object task,
                                      int priority)
  {
    __tasks.get(priority).add(task);
    notifyAll();
  }

  protected abstract void processTask(Object task);
}
