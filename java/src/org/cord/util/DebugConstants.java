package org.cord.util;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.Table;
import org.cord.node.Node;
import org.cord.node.NodeServlet;
import org.webmacro.servlet.WebContext;

import com.google.common.base.Optional;

public class DebugConstants
{
  public final static PrintWriter DEBUG_OUT =
      new PrintWriter(new OutputStreamWriter(System.err), true);

  public final static boolean DEBUG_AUTH = false;

  public final static boolean DEBUG_DEPENDENCIES = false;

  public final static boolean DEBUG_SQL = false;

  public final static boolean DEBUG_SQL_PRECACHE = false;

  /**
   * If true then attempting to resolve a missing from from {@link Table} will print a warning
   * message to log.
   */
  public final static boolean DEBUG_SQL_GETMISSINGRECORD = true;

  /**
   * If true then log when we create and destroy PreparedStatements
   */
  public final static boolean DEBUG_SQL_PREPAREDSTATEMENTS = false;

  /**
   * If true then attempting to resolve a missing record from a {@link Table} will print a stack
   * trace to the log to identify the problem.
   */
  public final static boolean DEBUG_SQL_GETMISSINGRECORD_STACKTRACE = false;

  public final static boolean DEBUG_SQL_VALIDATEFIELDS = false;

  public final static boolean DEBUG_EXPIRABLE = false;

  public final static boolean DEBUG_RESOURCES = true;

  public final static boolean DEBUG_EXPIRABLE_VERBOSE = false;

  public final static boolean DEBUG_ACCESSLOG = true;

  public static final boolean DEBUG_CACHE_EVICTION = false;

  /**
   * Invoke {@link NodeServlet#logAccess(WebContext)} once the initial population of the WebContext
   * has taken place and before the NodeRequest is constructed. Off by default.
   */
  public final static boolean DEBUG_ACCESSLOG_CONTEXT = false;

  /**
   * Drop a message to the log at the start of {@link NodeServlet#handle(WebContext)} and during
   * {@link NodeServlet#destroyContext(WebContext)} with accurate timestamps to enable further
   * temporal debugging.
   */
  public final static boolean DEBUG_ACCESSLOG_STARTEND = true;

  public static final boolean DEBUG_HTTP_PARAMS = false;

  public final static boolean DEBUG_SESSION = true;

  public final static boolean DEBUG_SESSION_VERBOSE = false;

  public final static boolean DEBUG_BOOTUP = true;

  public final static boolean DEBUG_ADVANCEDSEARCH = false;

  public final static boolean DEBUG_ADVANCEDSEARCH_REBUILD = false;

  public static final boolean DEBUG_DBINITIALISERS = false;

  public static final boolean DEBUG_MAILMAN = true;

  public static final boolean DEBUG_GRIPCOMMERCE = false;

  public static final boolean DEBUG_FEEDBACKEXCEPTION = true;

  public static final boolean STATS_ENABLED = true;

  public static final boolean DEBUG_FIELDLABELS = true;

  public static final boolean DEBUG_MIRROR_STRINGS_TRAILINGSPACES = true;

  public static final boolean DEBUG_TABLES_WITHOUT_TABLEWRAPPERS = false;

  /**
   * If true then print the CacheBuilderSpec for the PersistentRecord cache and the RecordSource
   * cache for each Table when it boots up. Defaults to off.
   */
  public static final boolean DEBUG_TABLE_CACHEBUILDERSPEC = false;

  /**
   * If true then all invocations of query.getRecordOrdering() will have the java representations
   * sent to the log.
   * 
   * @see Query#getRecordOrdering()
   */
  public static final boolean DEBUG_QUERY_GETRECORDORDERING = false;

  /**
   * If true then query.getRecordOrdering() which have SQL orderings that are not representable in
   * java will be sent to the log.
   * 
   * @see Query#getRecordOrdering()
   */
  public static final boolean DEBUG_QUERY_GETRECORDORDERING_DEFAULT = true;

  /**
   * If true then all RuntimeExceptions that result from failed Assertions will have their stack
   * traces dumped to DEBUG_OUT
   * 
   * @see Assertions
   */
  public final static boolean DEBUG_FAILEDASSERTIONS = false;

  public final static boolean DEBUG_DEBUGBOOTUP = false;

  /**
   * If true then add in visible detailing about what hidden values are being included in forms.
   */
  public final static boolean DEBUG_NODEREQUEST_FORMS = false;

  public static final boolean DEBUG_SIMPLERECORDOPERATION_UPGRADE = false;

  /**
   * If true then binding and releasing Db Connections to threads and checking in and out of pools
   * will be debugged.
   */
  public static final boolean DEBUG_SQL_THREADCONNECTIONS = false;

  static {
    if (DEBUG_DEBUGBOOTUP) {
      for (Field field : DebugConstants.class.getFields()) {
        try {
          if (field.getType() == Boolean.TYPE && field.getBoolean(null)) {
            DEBUG_OUT.println(field.getName());
          }
        } catch (IllegalArgumentException e) {
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Formatted method for arguments when debugging methods that adds quotes around string values so
   * that it is clear that this is what they are.
   * 
   * @param arg
   *          Optional argument
   * @return "null", or "string value", or arg.toString()
   * @see #method(Object, String, Object...)
   */
  public static String arg(Object arg)
  {
    StringBuilder out = new StringBuilder();
    try {
      arg(out, arg);
    } catch (IOException e) {
      throw new StringBuilderIOException(out, e);
    }
    return out.toString();
  }

  public static Appendable arg(Appendable out,
                               Object arg)
      throws IOException
  {
    if (arg == null) {
      out.append("null");
      return out;
    }
    if (arg instanceof String) {
      out.append('"').append((String) arg).append('"');
      return out;
    }
    if (arg instanceof Object[]) {
      Object[] array = (Object[]) arg;
      out.append('[');
      if (array.length > 0) {
        arg(out, array[0]);
        for (int i = 1; i < array.length; i++) {
          out.append(", ");
          arg(out, array[i]);
        }
      }
      out.append(']');
      return out;
    }
    out.append(arg.toString());
    return out;
  }

  public static String argClass(Object arg)
  {
    StringBuilder out = new StringBuilder();
    try {
      return argClass(out, arg).toString();
    } catch (IOException e) {
      throw new StringBuilderIOException(out, e);
    }
  }

  public static Appendable argClass(Appendable out,
                                    Object arg)
      throws IOException
  {
    if (arg == null) {
      out.append("null");
      return out;
    }
    arg(out, arg);
    if (!(arg instanceof String)) {
      out.append(" (").append(arg.getClass().toString()).append(')');
    }
    return out;
  }

  /**
   * debug a method invocation to DEBUG_OUT.
   * 
   * @param containingObj
   *          The Object which the method is being invoked upon
   * @param methodName
   *          The name of the method that is being invoked
   * @param args
   *          The arguments that are passed to the method. These will be passed to arg(arg) for
   *          quoting if they are strings
   * @see #arg(Object)
   */
  public static void method(Object containingObj,
                            boolean showThread,
                            String methodName,
                            Object... args)
  {
    DEBUG_OUT.println(formatMethodCall(containingObj, showThread, methodName, args));
  }

  public static String formatMethodCall(Object containingObj,
                                        boolean showThread,
                                        String methodName,
                                        Object... args)
  {
    StringBuilder buf = new StringBuilder();
    if (showThread) {
      buf.append(Thread.currentThread().getName()).append(": ");
    }
    try {
      buf.append(containingObj).append('.').append(methodName).append('(');
      if (args.length > 0) {
        arg(buf, args[0]);
        for (int i = 1; i < args.length; i++) {
          buf.append(", ");
          arg(buf, args[i]);
        }
      }
      buf.append(')');
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
    return buf.toString();
  }

  public static void method(Object containingObj,
                            String methodName,
                            Object... args)
  {
    method(containingObj, false, methodName, args);
  }

  public static void methodThread(Object containingObj,
                                  String methodName,
                                  Object... args)
  {
    method(containingObj, true, methodName, args);
  }

  /**
   * Dump the state of a variable to DEBUG_OUT This will be of the form of "- variableName=value
   * (classname)" with quotes around value if required. The leading "-" is there to get an indent on
   * systems that trim whitespace around outputs so that it can be used in confunction with the
   * method method.
   */
  public static void variable(String variableName,
                              Object value)
  {
    DEBUG_OUT.println("- " + variableName + " = " + argClass(value));
  }
  public static void message(String message)
  {
    DEBUG_OUT.println("- " + message);
  }

  public static void returns(Object value)
  {
    DEBUG_OUT.println("--> " + argClass(value));
  }

  public final static <K, V> String debug(Map<K, V> map)
  {
    StringBuilder result = new StringBuilder();
    Iterator<K> keys = map.keySet().iterator();
    while (keys.hasNext()) {
      K key = keys.next();
      V value = map.get(key);
      result.append("- ")
            .append(key)
            .append(": ")
            .append(value)
            .append(" (")
            .append(value == null ? "null" : value.getClass().getName())
            .append(")\n");
    }
    return result.toString();
  }

  @SuppressWarnings("unused")
  public final static void auth(PersistentRecord node,
                                String view,
                                Object supplier,
                                Boolean result)
  {
    if (DebugConstants.DEBUG_AUTH && result != null) {
      auth(node, view, supplier, result.toString());
    }
  }

  public final static void auth(PersistentRecord node,
                                String view,
                                Object supplier,
                                boolean result)
  {
    if (DebugConstants.DEBUG_AUTH) {
      auth(node, view, supplier, Boolean.toString(result));
    }
  }

  @SuppressWarnings("unused")
  public final static void auth(PersistentRecord node,
                                String view,
                                Object supplier,
                                Optional<Boolean> result)
  {
    if (DebugConstants.DEBUG_AUTH && result.isPresent()) {
      auth(node, view, supplier, result.get());
    }
  }

  private final static void auth(PersistentRecord node,
                                 String view,
                                 Object supplier,
                                 String result)
  {
    DebugConstants.DEBUG_OUT.println("AUTH: " + view + ": " + node.comp(Node.PATH) + ": " + supplier
                                     + " --> " + result);
  }

  /**
   * If {@link #DEBUG_AUTH} is enabled then log the message with the value as an AUTH line, and then
   * return the value.
   */
  public final static boolean authWrapper(String message,
                                          boolean value)
  {
    if (DebugConstants.DEBUG_AUTH) {
      DebugConstants.DEBUG_OUT.println("AUTH: " + message + " --> " + value);
    }
    return value;
  }
}