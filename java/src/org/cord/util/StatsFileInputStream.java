package org.cord.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Extension of a FileInputStream that tracks the number of bytes read from the file and the
 * percentage of the total bytes read from the File at that time.
 * 
 * @author alex
 */
public class StatsFileInputStream
  extends FileInputStream
{
  private final File __file;

  private long _bytesRead = 0;

  public StatsFileInputStream(File file) throws FileNotFoundException
  {
    super(file);
    __file = file;
  }

  /**
   * Get the number of bytes that have been read or skipped across the File since it was opened.
   */
  public long getBytesRytesRead()
  {
    return _bytesRead;
  }

  /**
   * Get the completeness of the read operation expressed as the number of bytes read divided by the
   * total size of the file at this time. Note that invoking this method twice without reading may
   * return different values if the File is growing due to an external process.
   * 
   * @return double between 0.0 (incomplete) and 1.0 (complete)
   */
  public double getCompleness()
  {
    return (double) _bytesRead / (double) __file.length();
  }

  @Override
  public int read()
      throws IOException
  {
    int result = super.read();
    _bytesRead++;
    return result;
  }

  @Override
  public int read(byte[] arg0,
                  int arg1,
                  int arg2)
      throws IOException
  {
    int count = super.read(arg0, arg1, arg2);
    _bytesRead += count;
    return count;
  }

  @Override
  public int read(byte[] arg0)
      throws IOException
  {
    int count = super.read(arg0);
    _bytesRead += count;
    return count;
  }

  @Override
  public long skip(long arg0)
      throws IOException
  {
    long count = super.skip(arg0);
    _bytesRead += count;
    return count;
  }
}
