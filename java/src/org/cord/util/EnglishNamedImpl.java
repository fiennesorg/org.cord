package org.cord.util;

public class EnglishNamedImpl
  extends NamedImpl
  implements EnglishNamed
{
  /**
   * Utility method to create a new instance of EnglishNamedImpl. This is declared as returning an
   * EnglishNamed so it can be useful when generics are expecting that explicitly.
   */
  public static EnglishNamed of(String name,
                                String englishName)
  {
    return new EnglishNamedImpl(name, englishName);
  }

  private final String __englishName;

  public EnglishNamedImpl(String name,
                          String englishName)
  {
    super(name);
    __englishName = (englishName == null ? name : englishName);
  }

  public EnglishNamedImpl(String name)
  {
    this(name,
         name);
  }

  @Override
  public final String getEnglishName()
  {
    return __englishName;
  }

  @Override
  public String toString()
  {
    return getEnglishName();
  }
}
