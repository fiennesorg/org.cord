package org.cord.util;

/**
 * The DelayedFormatOperation is an Object whose String representation is the result of invoking a
 * format method supplied at creation time, but this doesn't take place until the toString method is
 * invoked. This makes it a lightweight way of storing a potentially complex formatted value without
 * the overhead of actually working it out until it is actually asked for. This may be useful as the
 * logicErrorMessage in the compulsory methods in RecordSources and the like.
 * 
 * @see org.cord.mirror.recordsource.RecordSources
 * @author alex
 */
public final class DelayedFormatOperation
{
  private final String __format;

  private final Object[] __params;

  public DelayedFormatOperation(String format,
                                Object... params)
  {
    __format = format;
    __params = params;
  }

  @Override
  public String toString()
  {
    return String.format(__format, __params);
  }
}
