package org.cord.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Wrapper around an Iterator that divides it up into an Iterator of Iterators with each of the
 * inner Iterators having a predefined maximum length. Note that each page is calculated and stored
 * when requested so you don't have to iterate over it immediately.
 * 
 * @author alex
 */
public class PagedIterator<T>
  implements Iterator<Iterator<T>>
{
  private final Iterator<T> __iterator;

  private final int __pageSize;

  public PagedIterator(Iterator<T> iterator,
                       int pageSize)
  {
    super();
    __iterator = iterator;
    __pageSize = pageSize;
  }

  public final int getPageSize()
  {
    return __pageSize;
  }

  @Override
  public void remove()
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean hasNext()
  {
    return __iterator.hasNext();
  }

  @Override
  public Iterator<T> next()
  {
    List<T> a = new ArrayList<T>(__pageSize);
    int i = 0;
    while (i < __pageSize && __iterator.hasNext()) {
      a.add(__iterator.next());
      i++;
    }
    return a.iterator();
  }
}
