package org.cord.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * An Object that represents a set of requirements that must be satisfied by the contents of a
 * Gettable item in order for the Gettable item to be deemed valid. The individual items that must
 * be satisfied are modelled by GettableFilterProperty items.
 * 
 * @see GettableFilterProperty
 * @see Gettable
 */
public class GettableFilter
{
  /**
   * Singleton GettableFilter that has no requirements and is already locked.
   */
  public final static GettableFilter EMPTY = new GettableFilter();
  static {
    EMPTY.lock();
  }

  // PropertyRequirement.getName() --> PropertyRequirement
  private final Map<String, GettableFilterProperty> __properties =
      new HashMap<String, GettableFilterProperty>();

  private final Map<String, GettableFilterProperty> __publicProperties =
      Collections.unmodifiableMap(__properties);

  private boolean _isLocked = false;

  private final List<GettableFilterProperty> __guiItems = new ArrayList<GettableFilterProperty>();

  private final List<GettableFilterProperty> __publicGuiItems =
      Collections.unmodifiableList(__guiItems);

  /**
   * Add a new PropertyRequierement to this BasketPropertyFilter. The PropertyRequirement will be
   * automatically locked in the process so it should be fully initialised before adding.
   */
  public void add(GettableFilterProperty propertyRequirement)
  {
    if (_isLocked) {
      throw new IllegalStateException("Cannot add GettableFilterProperty to locked BasketPropertyFilter");
    }
    propertyRequirement.lock();
    __properties.put(propertyRequirement.getName(), propertyRequirement);
    if (propertyRequirement.isGuiItem()) {
      __guiItems.add(propertyRequirement);
    }
  }

  @Override
  public String toString()
  {
    return "BasketPropertyFilter(" + __publicProperties + ")";
  }

  /**
   * Get all the gui items that have been registered on this BasketPropertyFilter. Any code that
   * receives dynamic user input for updating the BasketPropertyFilter should only be expected to
   * provide a method for updating the GettableFilterPropertys in this List. Any other
   * GettableFilterPropertys that are required but not defined should be taken as an unrecoverable
   * error rather than querying the user for the information.
   * 
   * @return Unmodifiable List of GettableFilterProperty that satisfy isGuiItem() in the order that
   *         they where originally registered.
   * @see #add(GettableFilterProperty)
   * @see GettableFilterProperty
   * @see GettableFilterProperty#isGuiItem()
   */
  public List<GettableFilterProperty> getGuiItems()
  {
    return __publicGuiItems;
  }

  public void lock()
  {
    _isLocked = true;
  }

  public GettableFilterProperty getProperty(String name)
  {
    return __properties.get(name);
  }

  /**
   * @return unmodifiable Collection of GettableFilterProperty.
   */
  public Collection<GettableFilterProperty> getProperties()
  {
    return __publicProperties.values();
  }

  public boolean isValid(Gettable source)
  {
    Iterator<GettableFilterProperty> properties = getProperties().iterator();
    while (properties.hasNext()) {
      GettableFilterProperty property = properties.next();
      if (!property.isValid(source)) {
        return false;
      }
    }
    return true;
  }

  public void registerDefaultValues(Settable source)
      throws SettableException
  {
    Iterator<GettableFilterProperty> properties = getProperties().iterator();
    while (properties.hasNext()) {
      GettableFilterProperty property = properties.next();
      property.registerDefaultValue(source);
    }
  }
}
