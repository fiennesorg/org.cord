package org.cord.util;

/**
 * Interface that describes objects that are capable of expiring their contents based on some
 * criteria.
 */
public interface Expirable
  extends Named
{
  /**
   * Request the ObjectCache to expire any Cached values that are no longer required according to
   * the Caching algorithm that the ObjectCache utilises.
   * 
   * @return The number of Objects that where expired by this action.
   */
  public int expire();
}
