package org.cord.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import com.google.common.base.Strings;

public final class ExceptionUtil
{
  /**
   * Get the Message for a given Throwable and if this is null or empty then return the toString()
   * implementation of the Throwable. So we will always have something to print.
   */
  public static String toStringMessage(Throwable throwable)
  {
    String message = throwable.getMessage();
    if (!Strings.isNullOrEmpty(message)) {
      return message;
    }
    return throwable.toString();
  }

  /**
   * Dump the contents of a the stack trace for a given Throwable into a String
   */
  public static String printStackTrace(Throwable throwable)
  {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    throwable.printStackTrace(printWriter);
    return stringWriter.toString();
  }

  /**
   * Backtrace up the chain of Throwable exceptions using getCause until there is no more nested
   * Throwables and then return the innermost Throwable.
   */
  public static Throwable getOriginalCause(Throwable throwable)
  {
    while (throwable.getCause() != null) {
      throwable = throwable.getCause();
    }
    return throwable;
  }

  public static RuntimeException initCause(RuntimeException rEx,
                                           Throwable cause)
  {
    rEx.initCause(cause);
    return rEx;
  }

  public static String stackTraceSummary(Throwable throwable)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(throwable.getMessage()).append('\n');
    buf.append("    ").append(throwable.getStackTrace()[0]).append('\n');
    Throwable cause = throwable.getCause();
    while (cause != null) {
      buf.append("Caused by: ")
         .append(cause.getClass())
         .append(": ")
         .append(cause.getMessage())
         .append('\n');
      buf.append("    ").append(cause.getStackTrace()[0]).append('\n');
      cause = cause.getCause();
    }
    buf.append("    ...\n");
    return buf.toString();
  }

  public static void printStackTraceSummary(Throwable throwable,
                                            PrintStream out)
  {
    out.print(stackTraceSummary(throwable));
  }

  public static void printStackTraceSummary(Throwable throwable,
                                            PrintWriter out)
  {
    out.print(stackTraceSummary(throwable));
  }
}
