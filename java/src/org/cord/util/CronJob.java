package org.cord.util;

import java.util.Date;

/**
 * Interface implemented by classes that are able to be invoked by the CronManager.
 * 
 * @see CronManager
 */
public interface CronJob
{
  /**
   * Request the CronJob to perform it's allotted task. The CronManager will have done it's best to
   * ensure that the current time is the time requested by the CronJob, but this may or may not be
   * the case.
   * 
   * @param now
   *          The current time.
   */
  public void executeCronJob(Date now);

  /**
   * Get the time when the CronJob would like to be next executed given the current time.
   * 
   * @param now
   *          The current time.
   * @return The time that the CronJob should be next executed. If this time is before the current
   *         time passed to the parameter then this will be treated as a request to stop execution
   *         of the CronJob and the CronManager will remove it from the list of jobs and execute
   *         shutdown() on it.
   * @see #shutdown()
   */
  public Date getNextExecutionTime(Date now);

  public void shutdown();
}
