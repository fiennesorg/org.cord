package org.cord.util;

/**
 * Interface that describes Objects that are interested in being notified when a value is updated in
 * a Settable object.
 * 
 * @see Settable
 * @see ListenableSettable
 */
public interface SettableListener
{
  public void settableUpdate(Settable source,
                             Object key,
                             Object value,
                             Object previousValue);
}
