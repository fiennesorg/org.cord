package org.cord.util;

import java.io.Serializable;
import java.lang.ref.SoftReference;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

/**
 * Utility class for doing useful things to do with the Google Guava library.
 * 
 * @author alex
 */
public class Guava
{
  /**
   * Get an implementation of a memoizing Supplier that will re-request the value again if the
   * SoftReference that is holding the value is reclaimed.
   */
  public static <T> SoftMemoizingSupplier<T> softMemoize(Supplier<T> delegate)
  {
    return new SoftMemoizingSupplier<T>(delegate);
  }

  private static class SoftMemoizingSupplier<T>
    implements Supplier<T>, Serializable
  {
    final Supplier<T> __delegate;
    transient volatile SoftReference<T> _valueRef;

    SoftMemoizingSupplier(Supplier<T> delegate)
    {
      __delegate = Preconditions.checkNotNull(delegate, "delegate");
      ;
    }

    @Override
    public T get()
    {
      SoftReference<T> valueRef = _valueRef;
      T value;
      if (valueRef == null || (value = valueRef.get()) == null) {
        synchronized (this) {
          valueRef = _valueRef;
          if (valueRef == null || (value = valueRef.get()) == null) {
            value = __delegate.get();
            _valueRef = new SoftReference<T>(value);
          }
        }
      }
      return value;
    }

    private static final long serialVersionUID = 0;
  }
}
