package org.cord.util;

import java.util.Collection;

public interface Settable
  extends Gettable
{
  /**
   * Attempt to update a key,value pair in this Settable.
   * 
   * @param name
   *          The name that the value is to be associated with.
   * @param value
   *          The value that this key is to be associated with. A value of null implies that the key
   *          is to be removed from the Settable.
   * @return The value that was associated with key in this Settable before the new assignment was
   *         made. If the key is new then null will be returned.
   * @throws SettableException
   *           If the value that is supplied is unable to be set in this Settable for whatever
   *           reason. If this is not thrown then it should be assumed that the value has been
   *           updated.
   */
  public Object set(String name,
                    Object value)
      throws SettableException;

  public Object set(TypedKey<?> typedKey,
                    Object value)
      throws SettableException;

  /**
   * Optional method to list the keys that the Settable is prepared to expose to an untrusted client
   * as updatable. It is recommended that these keys should be a subset of the getPublicKeys()
   * listing, but this is not enforced anywhere. Not placing a key on this list does not imply that
   * it should not be set, but listing keys that are not settable in this list should be
   * discouraged. The presence of a key on the list does not mean that there is currently a value
   * for the key defined in the Settable at present.
   * 
   * @return Iterator of Object
   * @see Object
   */
  public Collection<String> getSettableKeys();

  /**
   * Get the definition as to what sort of value is defined for a given key. It is not compulsory to
   * return anything for this, but it is recommended that if you are going to define getSettableKeys
   * that you define a ValueType for each of the supplied keys. This can then be used to
   * auto-generate GUIs to gather the information required for a given Settable.
   * 
   * @return The ValueType describing what sort of value should be associated with they key, or null
   *         if nothing is known about the key. Note that returning null doesn't mean that you can't
   *         assign a value to the key.
   */
  public ValueType getValueType(String key);
}
