package org.cord.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IsoCountryCode
  extends NamedImpl
  implements EnglishNamed
{
  public final static int LENGTH = 2;

  private final static Map<String, IsoCountryCode> __instances =
      new LinkedHashMap<String, IsoCountryCode>();

  private final static Map<String, IsoCountryCode> __unmodifiableInstances =
      Collections.unmodifiableMap(__instances);

  /**
   * @return Unmodifiable Map of ISO code Strings to IsoCountryCode instances.
   */
  public final static Map<String, IsoCountryCode> getInstances()
  {
    return __unmodifiableInstances;
  }

  private final static Set<String> __unmodifiableNames =
      Collections.unmodifiableSet(__instances.keySet());

  private final static List<IsoCountryCode> __countryCodes = new ArrayList<IsoCountryCode>();

  private final static List<IsoCountryCode> __unmodifiableCountryCodes =
      Collections.unmodifiableList(__countryCodes);

  public final static IsoCountryCode ISO_AF = new IsoCountryCode("AF", "Afghanistan");

  public final static IsoCountryCode ISO_AL = new IsoCountryCode("AL", "Albania");

  public final static IsoCountryCode ISO_DZ = new IsoCountryCode("DZ", "Algeria");

  public final static IsoCountryCode ISO_AS = new IsoCountryCode("AS", "American Samoa");

  public final static IsoCountryCode ISO_AD = new IsoCountryCode("AD", "Andorra");

  public final static IsoCountryCode ISO_AO = new IsoCountryCode("AO", "Angola");

  public final static IsoCountryCode ISO_AI = new IsoCountryCode("AI", "Anguilla");

  public final static IsoCountryCode ISO_AQ = new IsoCountryCode("AQ", "Antarctica");

  public final static IsoCountryCode ISO_AG = new IsoCountryCode("AG", "Antigua and Barbuda");

  public final static IsoCountryCode ISO_AR = new IsoCountryCode("AR", "Argentina");

  public final static IsoCountryCode ISO_AM = new IsoCountryCode("AM", "Armenia");

  public final static IsoCountryCode ISO_AW = new IsoCountryCode("AW", "Aruba");

  public final static IsoCountryCode ISO_AU = new IsoCountryCode("AU", "Australia");

  public final static IsoCountryCode ISO_AT = new IsoCountryCode("AT", "Austria");

  public final static IsoCountryCode ISO_AZ = new IsoCountryCode("AZ", "Azerbaijan");

  public final static IsoCountryCode ISO_BS = new IsoCountryCode("BS", "Bahamas");

  public final static IsoCountryCode ISO_BH = new IsoCountryCode("BH", "Bahrain");

  public final static IsoCountryCode ISO_BD = new IsoCountryCode("BD", "Bangladesh");

  public final static IsoCountryCode ISO_BB = new IsoCountryCode("BB", "Barbados");

  public final static IsoCountryCode ISO_BY = new IsoCountryCode("BY", "Belarus");

  public final static IsoCountryCode ISO_BE = new IsoCountryCode("BE", "Belgium");

  public final static IsoCountryCode ISO_BZ = new IsoCountryCode("BZ", "Belize");

  public final static IsoCountryCode ISO_BJ = new IsoCountryCode("BJ", "Benin");

  public final static IsoCountryCode ISO_BM = new IsoCountryCode("BM", "Bermuda");

  public final static IsoCountryCode ISO_BT = new IsoCountryCode("BT", "Bhutan");

  public final static IsoCountryCode ISO_BO = new IsoCountryCode("BO", "Bolivia");

  public final static IsoCountryCode ISO_BA = new IsoCountryCode("BA", "Bosnia-Herzegovina");

  public final static IsoCountryCode ISO_BW = new IsoCountryCode("BW", "Botswana");

  public final static IsoCountryCode ISO_BV = new IsoCountryCode("BV", "Bouvet Island");

  public final static IsoCountryCode ISO_BR = new IsoCountryCode("BR", "Brazil");

  public final static IsoCountryCode ISO_IO =
      new IsoCountryCode("IO", "British Indian Ocean Territories");

  public final static IsoCountryCode ISO_BN = new IsoCountryCode("BN", "Brunei Darussalam");

  public final static IsoCountryCode ISO_BG = new IsoCountryCode("BG", "Bulgaria");

  public final static IsoCountryCode ISO_BF = new IsoCountryCode("BF", "Burkina Faso");

  public final static IsoCountryCode ISO_BI = new IsoCountryCode("BI", "Burundi");

  public final static IsoCountryCode ISO_KH = new IsoCountryCode("KH", "Cambodia");

  public final static IsoCountryCode ISO_CM = new IsoCountryCode("CM", "Cameroon");

  public final static IsoCountryCode ISO_CA = new IsoCountryCode("CA", "Canada");

  public final static IsoCountryCode ISO_CV = new IsoCountryCode("CV", "Cape Verde");

  public final static IsoCountryCode ISO_KY = new IsoCountryCode("KY", "Cayman Islands");

  public final static IsoCountryCode ISO_CF = new IsoCountryCode("CF", "Central African Republic");

  public final static IsoCountryCode ISO_TD = new IsoCountryCode("TD", "Chad");

  public final static IsoCountryCode ISO_CL = new IsoCountryCode("CL", "Chile");

  public final static IsoCountryCode ISO_CN = new IsoCountryCode("CN", "China");

  public final static IsoCountryCode ISO_CX = new IsoCountryCode("CX", "Christmas Island");

  public final static IsoCountryCode ISO_CC = new IsoCountryCode("CC", "Cocos (Keeling) Islands");

  public final static IsoCountryCode ISO_CO = new IsoCountryCode("CO", "Colombia");

  public final static IsoCountryCode ISO_KM = new IsoCountryCode("KM", "Comoros");

  public final static IsoCountryCode ISO_CG = new IsoCountryCode("CG", "Congo");

  public final static IsoCountryCode ISO_CD =
      new IsoCountryCode("CD", "Congo, the democratic republic of the");

  public final static IsoCountryCode ISO_CK = new IsoCountryCode("CK", "Cook Islands");

  public final static IsoCountryCode ISO_CR = new IsoCountryCode("CR", "Costa Rica");

  public final static IsoCountryCode ISO_CI = new IsoCountryCode("CI", "Cote d'Ivoire");

  public final static IsoCountryCode ISO_HR = new IsoCountryCode("HR", "Croatia");

  public final static IsoCountryCode ISO_CU = new IsoCountryCode("CU", "Cuba");

  public final static IsoCountryCode ISO_CY = new IsoCountryCode("CY", "Cyprus");

  public final static IsoCountryCode ISO_CZ = new IsoCountryCode("CZ", "Czech Republic");

  public final static IsoCountryCode ISO_DK = new IsoCountryCode("DK", "Denmark");

  public final static IsoCountryCode ISO_DJ = new IsoCountryCode("DJ", "Djibouti");

  public final static IsoCountryCode ISO_DM = new IsoCountryCode("DM", "Dominica");

  public final static IsoCountryCode ISO_DO = new IsoCountryCode("DO", "Dominican Republic");

  public final static IsoCountryCode ISO_EC = new IsoCountryCode("EC", "Ecuador");

  public final static IsoCountryCode ISO_EG = new IsoCountryCode("EG", "Egypt");

  public final static IsoCountryCode ISO_SV = new IsoCountryCode("SV", "El Salvador");

  public final static IsoCountryCode ISO_GQ = new IsoCountryCode("GQ", "Equatorial Guinea");

  public final static IsoCountryCode ISO_ER = new IsoCountryCode("ER", "Eritrea");

  public final static IsoCountryCode ISO_EE = new IsoCountryCode("EE", "Estonia");

  public final static IsoCountryCode ISO_ET = new IsoCountryCode("ET", "Ethiopia");

  public final static IsoCountryCode ISO_FK =
      new IsoCountryCode("FK", "Falkland Islands (Malvinas)");

  public final static IsoCountryCode ISO_FO = new IsoCountryCode("FO", "Faroe Islands");

  public final static IsoCountryCode ISO_FJ = new IsoCountryCode("FJ", "Fiji");

  public final static IsoCountryCode ISO_FI = new IsoCountryCode("FI", "Finland");

  public final static IsoCountryCode ISO_FR = new IsoCountryCode("FR", "France");

  public final static IsoCountryCode ISO_FX =
      new IsoCountryCode("FX", "France (European Territories)");

  public final static IsoCountryCode ISO_TF =
      new IsoCountryCode("TF", "French Southern Territories");

  public final static IsoCountryCode ISO_GA = new IsoCountryCode("GA", "Gabon");

  public final static IsoCountryCode ISO_GM = new IsoCountryCode("GM", "Gambia");

  public final static IsoCountryCode ISO_GE = new IsoCountryCode("GE", "Georgia");

  public final static IsoCountryCode ISO_DE = new IsoCountryCode("DE", "Germany");

  public final static IsoCountryCode ISO_GH = new IsoCountryCode("GH", "Ghana");

  public final static IsoCountryCode ISO_GI = new IsoCountryCode("GI", "Gibraltar");

  public final static IsoCountryCode ISO_GR = new IsoCountryCode("GR", "Greece");

  public final static IsoCountryCode ISO_GL = new IsoCountryCode("GL", "Greenland");

  public final static IsoCountryCode ISO_GD = new IsoCountryCode("GD", "Grenada");

  public final static IsoCountryCode ISO_GP = new IsoCountryCode("GP", "Guadeloupe (Fr.)");

  public final static IsoCountryCode ISO_GU = new IsoCountryCode("GU", "Guam (US)");

  public final static IsoCountryCode ISO_GT = new IsoCountryCode("GT", "Guatemala");

  public final static IsoCountryCode ISO_GG = new IsoCountryCode("GG", "Guernsey");

  public final static IsoCountryCode ISO_GF = new IsoCountryCode("GF", "Guiana (Fr.)");

  public final static IsoCountryCode ISO_GN = new IsoCountryCode("GN", "Guinea");

  public final static IsoCountryCode ISO_GW = new IsoCountryCode("GW", "Guinea Bissau");

  public final static IsoCountryCode ISO_GY = new IsoCountryCode("GY", "Guyana");

  public final static IsoCountryCode ISO_HT = new IsoCountryCode("HT", "Haiti");

  public final static IsoCountryCode ISO_HM = new IsoCountryCode("HM", "Heard & McDonald Islands");

  public final static IsoCountryCode ISO_HN = new IsoCountryCode("HN", "Honduras");

  public final static IsoCountryCode ISO_HK = new IsoCountryCode("HK", "Hong Kong");

  public final static IsoCountryCode ISO_HU = new IsoCountryCode("HU", "Hungary");

  public final static IsoCountryCode ISO_IS = new IsoCountryCode("IS", "Iceland");

  public final static IsoCountryCode ISO_IN = new IsoCountryCode("IN", "India");

  public final static IsoCountryCode ISO_ID = new IsoCountryCode("ID", "Indonesia");

  public final static IsoCountryCode ISO_IR = new IsoCountryCode("IR", "Iran, Islamic Republic Of");

  public final static IsoCountryCode ISO_IQ = new IsoCountryCode("IQ", "Iraq");

  public final static IsoCountryCode ISO_IE = new IsoCountryCode("IE", "Ireland");

  public final static IsoCountryCode ISO_IL = new IsoCountryCode("IL", "Israel");

  public final static IsoCountryCode ISO_IM = new IsoCountryCode("IM", "Isle of Man");

  public final static IsoCountryCode ISO_IT = new IsoCountryCode("IT", "Italy");

  public final static IsoCountryCode ISO_JM = new IsoCountryCode("JM", "Jamaica");

  public final static IsoCountryCode ISO_JP = new IsoCountryCode("JP", "Japan");

  public final static IsoCountryCode ISO_JE = new IsoCountryCode("JE", "Jersey");

  public final static IsoCountryCode ISO_JO = new IsoCountryCode("JO", "Jordan");

  public final static IsoCountryCode ISO_KZ = new IsoCountryCode("KZ", "Kazakhstan");

  public final static IsoCountryCode ISO_KE = new IsoCountryCode("KE", "Kenya");

  public final static IsoCountryCode ISO_KI = new IsoCountryCode("KI", "Kiribati");

  public final static IsoCountryCode ISO_KP =
      new IsoCountryCode("KP", "Korea, Democratic People's Republic Of");

  public final static IsoCountryCode ISO_KR = new IsoCountryCode("KR", "Korea, Republic Of");

  public final static IsoCountryCode ISO_KW = new IsoCountryCode("KW", "Kuwait");

  public final static IsoCountryCode ISO_KG = new IsoCountryCode("KG", "Kyrgyz Republic");

  public final static IsoCountryCode ISO_LA = new IsoCountryCode("LA", "Laos");

  public final static IsoCountryCode ISO_LV = new IsoCountryCode("LV", "Latvia");

  public final static IsoCountryCode ISO_LB = new IsoCountryCode("LB", "Lebanon");

  public final static IsoCountryCode ISO_LS = new IsoCountryCode("LS", "Lesotho");

  public final static IsoCountryCode ISO_LR = new IsoCountryCode("LR", "Liberia");

  public final static IsoCountryCode ISO_LY = new IsoCountryCode("LY", "Libyan Arab Jamahiriya");

  public final static IsoCountryCode ISO_LI = new IsoCountryCode("LI", "Liechtenstein");

  public final static IsoCountryCode ISO_LT = new IsoCountryCode("LT", "Lithuania");

  public final static IsoCountryCode ISO_LU = new IsoCountryCode("LU", "Luxembourg");

  public final static IsoCountryCode ISO_MO = new IsoCountryCode("MO", "Macau");

  public final static IsoCountryCode ISO_MK =
      new IsoCountryCode("MK", "Macedonia, The Former Yugoslav Republic Of");

  public final static IsoCountryCode ISO_MG = new IsoCountryCode("MG", "Madagascar");

  public final static IsoCountryCode ISO_MW = new IsoCountryCode("MW", "Malawi");

  public final static IsoCountryCode ISO_MY = new IsoCountryCode("MY", "Malaysia");

  public final static IsoCountryCode ISO_MV = new IsoCountryCode("MV", "Maldives");

  public final static IsoCountryCode ISO_ML = new IsoCountryCode("ML", "Mali");

  public final static IsoCountryCode ISO_MT = new IsoCountryCode("MT", "Malta");

  public final static IsoCountryCode ISO_MH = new IsoCountryCode("MH", "Marshall Islands");

  public final static IsoCountryCode ISO_MQ = new IsoCountryCode("MQ", "Martinique (Fr.)");

  public final static IsoCountryCode ISO_MR = new IsoCountryCode("MR", "Mauritania");

  public final static IsoCountryCode ISO_MU = new IsoCountryCode("MU", "Mauritius");

  public final static IsoCountryCode ISO_YT = new IsoCountryCode("YT", "Mayotte");

  public final static IsoCountryCode ISO_MX = new IsoCountryCode("MX", "Mexico");

  public final static IsoCountryCode ISO_FM = new IsoCountryCode("FM", "Micronesia");

  public final static IsoCountryCode ISO_MD = new IsoCountryCode("MD", "Moldova");

  public final static IsoCountryCode ISO_MC = new IsoCountryCode("MC", "Monaco");

  public final static IsoCountryCode ISO_MN = new IsoCountryCode("MN", "Mongolia");

  public final static IsoCountryCode ISO_ME = new IsoCountryCode("ME", "Montenegro");

  public final static IsoCountryCode ISO_MS = new IsoCountryCode("MS", "Montserrat");

  public final static IsoCountryCode ISO_MA = new IsoCountryCode("MA", "Morocco");

  public final static IsoCountryCode ISO_MZ = new IsoCountryCode("MZ", "Mozambique");

  public final static IsoCountryCode ISO_MM = new IsoCountryCode("MM", "Myanmar");

  public final static IsoCountryCode ISO_NA = new IsoCountryCode("NA", "Namibia");

  public final static IsoCountryCode ISO_NR = new IsoCountryCode("NR", "Nauru");

  public final static IsoCountryCode ISO_NP = new IsoCountryCode("NP", "Nepal");

  public final static IsoCountryCode ISO_AN = new IsoCountryCode("AN", "Netherland Antilles");

  public final static IsoCountryCode ISO_NL = new IsoCountryCode("NL", "Netherlands");

  public final static IsoCountryCode ISO_NC = new IsoCountryCode("NC", "New Caledonia (Fr.)");

  public final static IsoCountryCode ISO_NZ = new IsoCountryCode("NZ", "New Zealand");

  public final static IsoCountryCode ISO_NI = new IsoCountryCode("NI", "Nicaragua");

  public final static IsoCountryCode ISO_NE = new IsoCountryCode("NE", "Niger");

  public final static IsoCountryCode ISO_NG = new IsoCountryCode("NG", "Nigeria");

  public final static IsoCountryCode ISO_NU = new IsoCountryCode("NU", "Niue");

  public final static IsoCountryCode ISO_NF = new IsoCountryCode("NF", "Norfolk Island");

  public final static IsoCountryCode ISO_MP = new IsoCountryCode("MP", "Northern Mariana Islands");

  public final static IsoCountryCode ISO_NO = new IsoCountryCode("NO", "Norway");

  public final static IsoCountryCode ISO_OM = new IsoCountryCode("OM", "Oman");

  public final static IsoCountryCode ISO_PK = new IsoCountryCode("PK", "Pakistan");

  public final static IsoCountryCode ISO_PW = new IsoCountryCode("PW", "Palau");

  public final static IsoCountryCode ISO_PA = new IsoCountryCode("PA", "Panama");

  public final static IsoCountryCode ISO_PG = new IsoCountryCode("PG", "Papua New Guinea");

  public final static IsoCountryCode ISO_PY = new IsoCountryCode("PY", "Paraguay");

  public final static IsoCountryCode ISO_PE = new IsoCountryCode("PE", "Peru");

  public final static IsoCountryCode ISO_PH = new IsoCountryCode("PH", "Philippines");

  public final static IsoCountryCode ISO_PN = new IsoCountryCode("PN", "Pitcairn");

  public final static IsoCountryCode ISO_PL = new IsoCountryCode("PL", "Poland");

  public final static IsoCountryCode ISO_PF = new IsoCountryCode("PF", "Polynesia (Fr.)");

  public final static IsoCountryCode ISO_PT = new IsoCountryCode("PT", "Portugal");

  public final static IsoCountryCode ISO_PR = new IsoCountryCode("PR", "Puerto Rico (US)");

  public final static IsoCountryCode ISO_QA = new IsoCountryCode("QA", "Qatar");

  public final static IsoCountryCode ISO_RE = new IsoCountryCode("RE", "Reunion (Fr.)");

  public final static IsoCountryCode ISO_RO = new IsoCountryCode("RO", "Romania");

  public final static IsoCountryCode ISO_RU = new IsoCountryCode("RU", "Russian Federation");

  public final static IsoCountryCode ISO_RW = new IsoCountryCode("RW", "Rwanda");

  public final static IsoCountryCode ISO_SH = new IsoCountryCode("SH", "Saint Helena");

  public final static IsoCountryCode ISO_KN = new IsoCountryCode("KN", "Saint Kitts and Nevis");

  public final static IsoCountryCode ISO_LC = new IsoCountryCode("LC", "Saint Lucia");

  public final static IsoCountryCode ISO_PM = new IsoCountryCode("PM", "Saint Pierre And Miquelon");

  public final static IsoCountryCode ISO_VC =
      new IsoCountryCode("VC", "Saint Vincent And The Grandines");

  public final static IsoCountryCode ISO_WS = new IsoCountryCode("WS", "Samoa");

  public final static IsoCountryCode ISO_SM = new IsoCountryCode("SM", "San Marino");

  public final static IsoCountryCode ISO_ST = new IsoCountryCode("ST", "Sao Tome And Principe");

  public final static IsoCountryCode ISO_SA = new IsoCountryCode("SA", "Saudi Arabia");

  public final static IsoCountryCode ISO_SN = new IsoCountryCode("SN", "Senegal");

  public final static IsoCountryCode ISO_SC = new IsoCountryCode("SC", "Seychelles");

  public final static IsoCountryCode ISO_RS = new IsoCountryCode("RS", "Serbia");

  public final static IsoCountryCode ISO_SL = new IsoCountryCode("SL", "Sierra Leone");

  public final static IsoCountryCode ISO_SG = new IsoCountryCode("SG", "Singapore");

  public final static IsoCountryCode ISO_SK = new IsoCountryCode("SK", "Slovakia (Slovak Rep)");

  public final static IsoCountryCode ISO_SI = new IsoCountryCode("SI", "Slovenia");

  public final static IsoCountryCode ISO_SB = new IsoCountryCode("SB", "Solomon Islands");

  public final static IsoCountryCode ISO_SO = new IsoCountryCode("SO", "Somalia");

  public final static IsoCountryCode ISO_ZA = new IsoCountryCode("ZA", "South Africa");

  public final static IsoCountryCode ISO_GS =
      new IsoCountryCode("GS", "South Georgia and South Sandwich Islands");

  public final static IsoCountryCode ISO_ES = new IsoCountryCode("ES", "Spain");

  public final static IsoCountryCode ISO_LK = new IsoCountryCode("LK", "Sri Lanka");

  public final static IsoCountryCode ISO_SD = new IsoCountryCode("SD", "Sudan");

  public final static IsoCountryCode ISO_SR = new IsoCountryCode("SR", "Suriname");

  public final static IsoCountryCode ISO_SJ =
      new IsoCountryCode("SJ", "Svalbard & Jan Mayen Islands");

  public final static IsoCountryCode ISO_SZ = new IsoCountryCode("SZ", "Swaziland");

  public final static IsoCountryCode ISO_SE = new IsoCountryCode("SE", "Sweden");

  public final static IsoCountryCode ISO_CH = new IsoCountryCode("CH", "Switzerland");

  public final static IsoCountryCode ISO_SY = new IsoCountryCode("SY", "Syrian Arab Republic");

  public final static IsoCountryCode ISO_TW = new IsoCountryCode("TW", "Taiwan");

  public final static IsoCountryCode ISO_TJ = new IsoCountryCode("TJ", "Tajikistan");

  public final static IsoCountryCode ISO_TZ =
      new IsoCountryCode("TZ", "Tanzania, United Republic Of");

  public final static IsoCountryCode ISO_TH = new IsoCountryCode("TH", "Thailand");

  public final static IsoCountryCode ISO_TL = new IsoCountryCode("TL", "Timor-Lests");

  public final static IsoCountryCode ISO_TG = new IsoCountryCode("TG", "Togo");

  public final static IsoCountryCode ISO_TK = new IsoCountryCode("TK", "Tokelau");

  public final static IsoCountryCode ISO_TO = new IsoCountryCode("TO", "Tonga");

  public final static IsoCountryCode ISO_TT = new IsoCountryCode("TT", "Trinidad And Tobago");

  public final static IsoCountryCode ISO_TN = new IsoCountryCode("TN", "Tunisia");

  public final static IsoCountryCode ISO_TR = new IsoCountryCode("TR", "Turkey");

  public final static IsoCountryCode ISO_TM = new IsoCountryCode("TM", "Turkmenistan");

  public final static IsoCountryCode ISO_TC = new IsoCountryCode("TC", "Turks And Caicos Islands");

  public final static IsoCountryCode ISO_TV = new IsoCountryCode("TV", "Tuvalu");

  public final static IsoCountryCode ISO_UG = new IsoCountryCode("UG", "Uganda");

  public final static IsoCountryCode ISO_UA = new IsoCountryCode("UA", "Ukraine");

  public final static IsoCountryCode ISO_AE = new IsoCountryCode("AE", "United Arab Emirates");

  public final static IsoCountryCode ISO_GB = new IsoCountryCode("GB", "United Kingdom");

  public final static IsoCountryCode ISO_US = new IsoCountryCode("US", "United States");

  public final static IsoCountryCode ISO_UM =
      new IsoCountryCode("UM", "United States Minor Outlying Islands");

  public final static IsoCountryCode ISO_UY = new IsoCountryCode("UY", "Uruguay");

  public final static IsoCountryCode ISO_UZ = new IsoCountryCode("UZ", "Uzbekistan");

  public final static IsoCountryCode ISO_VA = new IsoCountryCode("VA", "Vatican City");

  public final static IsoCountryCode ISO_VU = new IsoCountryCode("VU", "Vanuatu");

  public final static IsoCountryCode ISO_VE = new IsoCountryCode("VE", "Venezuela");

  public final static IsoCountryCode ISO_VN = new IsoCountryCode("VN", "Vietnam");

  public final static IsoCountryCode ISO_VG = new IsoCountryCode("VG", "Virgin Islands, British");

  public final static IsoCountryCode ISO_VI = new IsoCountryCode("VI", "Virgin Islands, U.S.");

  public final static IsoCountryCode ISO_WF = new IsoCountryCode("WF", "Wallis And Futuna");

  public final static IsoCountryCode ISO_EH = new IsoCountryCode("EH", "Western Sahara");

  public final static IsoCountryCode ISO_YE = new IsoCountryCode("YE", "Yemen");

  public final static IsoCountryCode ISO_YU = new IsoCountryCode("YU", "Yugoslavia");

  public final static IsoCountryCode ISO_ZM = new IsoCountryCode("ZM", "Zambia");

  public final static IsoCountryCode ISO_ZW = new IsoCountryCode("ZW", "Zimbabwe");

  public final static List<IsoCountryCode> EUROPE =
      Collections.unmodifiableList(Arrays.asList(new IsoCountryCode[] { ISO_AL, ISO_AD, ISO_AT,
          ISO_BY, ISO_BE, ISO_BA, ISO_BG, ISO_HR, ISO_CY, ISO_CZ, ISO_DK, ISO_EE, ISO_FO, ISO_FI,
          ISO_FR, ISO_GB, ISO_GE, ISO_DE, ISO_GI, ISO_GG, ISO_GR, ISO_GL, ISO_HU, ISO_IS, ISO_IE,
          ISO_IM, ISO_IT, ISO_JE, ISO_KZ, ISO_LV, ISO_LI, ISO_LT, ISO_LU, ISO_ME, ISO_MK, ISO_MT,
          ISO_MD, ISO_MC, ISO_NL, ISO_NO, ISO_PL, ISO_PT, ISO_RO, ISO_RU, ISO_RS, ISO_SM, ISO_SK,
          ISO_SI, ISO_ES, ISO_SE, ISO_CH, ISO_TJ, ISO_TR, ISO_TM, ISO_UA, ISO_UZ }));

  public final static List<IsoCountryCode> NOT_EUROPE;

  static {
    List<IsoCountryCode> notEurope = new ArrayList<IsoCountryCode>();
    Iterator<IsoCountryCode> countryCodes = getCountryCodes().iterator();
    while (countryCodes.hasNext()) {
      IsoCountryCode countryCode = countryCodes.next();
      if (!EUROPE.contains(countryCode)) {
        notEurope.add(countryCode);
      }
    }
    NOT_EUROPE = Collections.unmodifiableList(notEurope);
  }

  /**
   * @param name
   *          The two letter iso country code. case insensitive.
   * @return The IsoCountryCode identified by name. If name is null or if the name is not
   *         recognised, then null will be returned.
   */
  public final static IsoCountryCode getInstance(String name)
  {
    return name == null ? null : __instances.get(name.toUpperCase());
  }

  /**
   * @return Unmodifiable Set of Strings containing two letter country codes. Sorted alphabetically
   *         by English Name.
   */
  public final static Set<String> getNames()
  {
    return __unmodifiableNames;
  }

  /**
   * @return Unmodifiable List of IsoCountryCodes. Ordering undefined.
   */
  public final static List<IsoCountryCode> getCountryCodes()
  {
    return __unmodifiableCountryCodes;
  }

  private final String __englishName;

  private IsoCountryCode(String name,
                         String englishName)
  {
    super(name);
    if (__instances.containsKey(name)) {
      throw new IllegalArgumentException(name + " is already defined as " + getInstance(name));
    }
    __englishName = englishName;
    __instances.put(name, this);
    __countryCodes.add(this);
  }

  @Override
  public final String getEnglishName()
  {
    return __englishName;
  }

  private static IsoCountryCodeValidator __validator = new IsoCountryCodeValidator();

  /**
   * Get a StringValidator implementation that checks for valid Iso Country Codes.
   */
  public final static IsoCountryCodeValidator getValidator()
  {
    return __validator;
  }

  /**
   * Singleton class that implements a StringValidator check for valid Iso Country Codes.
   */
  public static class IsoCountryCodeValidator
    extends NamedImpl
    implements StringValidator
  {
    public final static String NAME = "IsoCountryCodeValidator";

    private IsoCountryCodeValidator()
    {
      super(NAME);
    }

    @Override
    public boolean isValid(String string)
    {
      return getInstance(string) != null;
    }

    @Override
    public String validate(String string)
        throws StringValidatorException
    {
      if (isValid(string)) {
        return string;
      }
      throw new StringValidatorException("not an ISO 3166 country code", string);
    }
  }
}
