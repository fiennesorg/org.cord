package org.cord.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.common.base.Strings;

public class NamedPropertyImpl
  extends AbstractNamedGettable
{
  public final static Map<String, String> EMPTY_IMMUTABLE_MAP = Collections.emptyMap();

  private final Map<String, String> __properties = new HashMap<String, String>();;

  private final Map<String, String> __roProperties = Collections.unmodifiableMap(__properties);

  private volatile boolean _hasChanged = false;

  private final boolean __exposePublicKeys;

  public NamedPropertyImpl(String name,
                           boolean exposePublicKeys)
  {
    super(name);
    __exposePublicKeys = exposePublicKeys;
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    if (__exposePublicKeys) {
      return __roProperties.keySet();
    }
    return Collections.emptyList();
  }

  /**
   * Change the hasChanged flag to the given value.
   * 
   * @see #hasChanged()
   */
  protected final void setHasChanged(boolean flag)
  {
    _hasChanged = flag;
  }

  /**
   * Ask the Basket if it has had any Products added to it since it was either created or since the
   * resetHasChanged() method has been called.
   * 
   * @return true if there has been Products added to the Basket.
   */
  public final boolean hasChanged()
  {
    return _hasChanged;
  }

  @Override
  public String get(Object key)
  {
    return __properties.get(key.toString());
  }

  /**
   * Get a String property from this object, padding null values as empty strings. Note that this
   * will not distinguish between a key that is not defined and a key that is mapped to empty
   * string.
   */
  public String padNullGet(Object key)
  {
    return Strings.nullToEmpty(getString(key));
  }

  protected String set(final String name,
                       final String value)
  {
    if (name == null) {
      return null;
    }
    String result = null;
    if (value == null) {
      result = __properties.remove(name);
      if (result != null) {
        setHasChanged(true);
      }
    } else {
      result = __properties.put(name, value);
      if (!value.equals(result)) {
        setHasChanged(true);
      }
    }
    return result;
  }

  public final Map<String, String> getProperties()
  {
    return __roProperties;
  }

  public final Iterator<String> getPropertyNames()
  {
    return __roProperties.keySet().iterator();
  }

}
