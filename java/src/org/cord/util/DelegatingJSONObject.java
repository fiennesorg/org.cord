package org.cord.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONObjectBuilder;
import org.json.JSONObjects;

import com.google.common.base.Preconditions;

public class DelegatingJSONObject
  implements JSONObject
{
  private final JSONObject __first;
  private final JSONObject __second;

  public DelegatingJSONObject(JSONObject first,
                              JSONObject second)
  {
    __first = Preconditions.checkNotNull(first, "first");
    __second = Preconditions.checkNotNull(second, "second");
  }

  @Override
  public boolean equalsMap(Map<String, Object> map)
  {
    if (this.length() != map.size()) {
      return false;
    }
    for (String key : keySet()) {
      if (!opt(key).equals(map.get(key))) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Object get(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.get(key) : __second.get(key);
  }

  @Override
  public boolean getBoolean(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getBoolean(key) : __second.getBoolean(key);
  }

  @Override
  public double getDouble(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getDouble(key) : __second.getDouble(key);
  }

  @Override
  public int getInt(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getInt(key) : __second.getInt(key);
  }

  @Override
  public JSONArray getJSONArray(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getJSONArray(key) : __second.getJSONArray(key);
  }

  @Override
  public JSONObject getJSONObject(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getJSONObject(key) : __second.getJSONObject(key);
  }

  @Override
  public long getLong(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getLong(key) : __second.getLong(key);
  }

  @Override
  public String getString(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getString(key) : __second.getString(key);
  }

  @Override
  public boolean has(String key)
  {
    return __first.has(key) || __second.has(key);
  }

  @Override
  public boolean isNull(String key)
  {
    return __first.has(key) ? __first.isNull(key) : __second.isNull(key);
  }

  @Override
  public Iterator<String> keys()
  {
    return keySet().iterator();
  }

  private Set<String> keySet()
  {
    Set<String> keys = new HashSet<String>();
    for (Iterator<String> i = __first.keys(); i.hasNext();) {
      keys.add(i.next());
    }
    for (Iterator<String> i = __second.keys(); i.hasNext();) {
      keys.add(i.next());
    }
    return keys;
  }

  @Override
  public int length()
  {
    return keySet().size();
  }

  // @Override
  // public JSONArray names()
  // {
  // return MapBasedJSONObject.names(this);
  // }

  @Override
  public Object opt(String key)
  {
    return __first.has(key) ? __first.opt(key) : __second.opt(key);
  }

  @Override
  public boolean optBoolean(String key)
  {
    return __first.has(key) ? __first.optBoolean(key) : __second.optBoolean(key);
  }

  @Override
  public boolean optBoolean(String key,
                            boolean defaultValue)
  {
    return __first.has(key)
        ? __first.optBoolean(key, defaultValue)
        : __second.optBoolean(key, defaultValue);
  }

  @Override
  public double optDouble(String key)
  {
    return __first.has(key) ? __first.optDouble(key) : __second.optDouble(key);
  }

  @Override
  public double optDouble(String key,
                          double defaultValue)
  {
    return __first.has(key)
        ? __first.optDouble(key, defaultValue)
        : __second.optDouble(key, defaultValue);
  }

  @Override
  public int optInt(String key)
  {
    return __first.has(key) ? __first.optInt(key) : __second.optInt(key);
  }

  @Override
  public int optInt(String key,
                    int defaultValue)
  {
    return __first.has(key)
        ? __first.optInt(key, defaultValue)
        : __second.optInt(key, defaultValue);
  }

  @Override
  public JSONArray optJSONArray(String key)
  {
    return __first.has(key) ? __first.optJSONArray(key) : __second.optJSONArray(key);
  }

  @Override
  public JSONObject optJSONObject(String key)
  {
    return __first.has(key) ? __first.optJSONObject(key) : __second.optJSONObject(key);
  }

  @Override
  public long optLong(String key)
  {
    return __first.has(key) ? __first.optLong(key) : __second.optLong(key);
  }

  @Override
  public long optLong(String key,
                      long defaultValue)
  {
    return __first.has(key)
        ? __first.optLong(key, defaultValue)
        : __second.optLong(key, defaultValue);
  }

  @Override
  public String optString(String key)
  {
    return __first.has(key) ? __first.optString(key) : __second.optString(key);
  }

  @Override
  public String optString(String key,
                          String defaultValue)
  {
    return __first.has(key)
        ? __first.optString(key, defaultValue)
        : __second.optString(key, defaultValue);
  }

  @Override
  public Iterator<String> sortedKeys()
  {
    return new TreeSet<String>(keySet()).iterator();
  }

  @Override
  public String toString()
  {
    return JSONObjects.toString(this);
  }

  @Override
  public String toString(int indentFactor)
  {
    return JSONObjects.toString(this, indentFactor, 0);
  }

  @Override
  public Appendable write(Appendable writer)
      throws IOException
  {
    return JSONObjects.write(this, writer);
  }

  @Override
  public JSONObject put(String key,
                        boolean value)
      throws JSONException
  {
    return __first.put(key, value);
  }

  @Override
  public JSONObject put(String key,
                        double value)
      throws JSONException
  {
    return __first.put(key, value);
  }

  @Override
  public JSONObject put(String key,
                        int value)
      throws JSONException
  {
    return __first.put(key, value);
  }

  @Override
  public JSONObject put(String key,
                        long value)
      throws JSONException
  {
    return __first.put(key, value);
  }

  // @Override
  // public JSONObject put(String key,
  // Map<String, ?> value)
  // throws JSONException
  // {
  // return __first.put(key, value);
  // }

  @Override
  public JSONObject put(String key,
                        Object value)
      throws JSONException
  {
    return __first.put(key, value);
  }

  @Override
  public JSONObject putOnce(String key,
                            Object value)
      throws JSONException
  {
    return __first.putOnce(key, value);
  }

  @Override
  public JSONObject putOpt(String key,
                           Object value)
      throws JSONException
  {
    return __first.putOpt(key, value);
  }

  @Override
  public Object remove(String key)
  {
    return __first.remove(key);
  }

  @Override
  public Integer optInteger(String key)
  {
    return __first.has(key) ? __first.optInteger(key) : __second.optInteger(key);
  }

  @Override
  public Integer optInteger(String key,
                            Integer defaultValue)
  {
    return __first.has(key)
        ? __first.optInteger(key, defaultValue)
        : __second.optInteger(key, defaultValue);
  }

  // @Override
  // public WritableJSONObject writableClone()
  // {
  // throw new UnsupportedOperationException("DelegatingJSONObject does not support writableClone
  // (yet)");
  // }

  @Override
  public String toString(int indentFactor,
                         int indent)
  {
    throw new UnsupportedOperationException("DelegatingJSONObject does not support toString(indentFactory, indent) (yet)");
  }

  @Override
  public Double getDoubleObj(String key)
      throws JSONException
  {
    return __first.has(key) ? __first.getDoubleObj(key) : __second.getDoubleObj(key);
  }

  @Override
  public Double optDoubleObj(String key,
                             Double defaultValue)
  {
    return __first.has(key)
        ? __first.optDoubleObj(key, defaultValue)
        : __second.optDoubleObj(key, defaultValue);
  }

  @Override
  public <A extends JSONArray, O extends JSONObject> O clone(JSONBuilder<A, O> builder)
      throws JSONException
  {
    JSONObjectBuilder<O> objBuilder = builder.createJSONObjectBuilder();
    for (String key : keySet()) {
      objBuilder.putOnce(key, opt(key));
    }
    return objBuilder.build();
  }
}
