package org.cord.util;

/**
 * Interface to be implemented by classes that include a soft-version of the "final" java construct.
 * This is generally used for classes that are registered in a registry and allows the registered
 * objects to state whether or not they are allowed to be replaced by other definitions at a later
 * date.
 * 
 * @deprecated because nobody is using it and I don't have any plans for it in the future.
 */
@Deprecated
public interface Finalisable
{
  /**
   * Check to see if this Object implements soft-final.
   * 
   * @return true if the Object is soft-final and therefore cannot be overridden, or false if the
   *         Object is not soft-final and can therefore be replaced with another Object.
   */
  public boolean isFinal();

  /**
   * Inform this Object that it has since been replaced with another Finalisable Object. The Object
   * should destroy any associated resources.
   */
  public void hasBeenOveridden();
}
