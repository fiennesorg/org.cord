package org.cord.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Class that represents a grouping of zero or more unique IsoCountryCodes.
 */
public class IsoCountryCodeGrouping
  extends EnglishNamedImpl
{
  private final Set<IsoCountryCode> __isoCountryCodes = new HashSet<IsoCountryCode>();

  private final Set<IsoCountryCode> __publicIsoCountryCodes =
      Collections.unmodifiableSet(__isoCountryCodes);

  private boolean _isLocked = false;

  public IsoCountryCodeGrouping(String name,
                                String englishName)
  {
    super(name,
          englishName);
  }

  public static IsoCountryCodeGrouping getInstance(String name,
                                                   String englishName,
                                                   String commaSeperatedIsoCodes)
  {
    IsoCountryCodeGrouping grouping = new IsoCountryCodeGrouping(name, englishName);
    StringTokenizer codes = new StringTokenizer(commaSeperatedIsoCodes, ",");
    while (codes.hasMoreTokens()) {
      grouping.add(IsoCountryCode.getInstance(codes.nextToken()));
    }
    grouping.lock();
    return grouping;
  }

  /**
   * @return Immutable Set of IsoCountryCode objects
   * @see IsoCountryCode
   */
  public final Set<IsoCountryCode> getIsoCountryCodes()
  {
    return __publicIsoCountryCodes;
  }

  public void add(IsoCountryCode code)
  {
    if (isLocked()) {
      throw new IllegalStateException("Cannot add IsoCountryCodes to locked IsoCountryCodeGrouping");
    }
    if (code != null) {
      __isoCountryCodes.add(code);
    }
  }

  public final boolean isLocked()
  {
    return _isLocked;
  }

  public void lock()
  {
    _isLocked = true;
  }

  public boolean contains(IsoCountryCode code)
  {
    return __isoCountryCodes.contains(code);
  }
}
