package org.cord.util;

import java.util.concurrent.ConcurrentHashMap;

/**
 * An extension of ConcurrentHashMap that carries with it a timestamp. The timestamp can be read at
 * any point, but can also be set to the current time at any point. The timestamp has no
 * functionality other than being able to be read and reset.
 */
public class TimestampedConcurrentMap<K, V>
  extends ConcurrentHashMap<K, V>
  implements Timestamped
{
  /**
   * 
   */
  private static final long serialVersionUID = 7879062380860351300L;

  private volatile long _timestamp;

  /**
   * Create a new empty TimestampedHashMap with the timestamp set to now.
   */
  public TimestampedConcurrentMap(int initialCapacity,
                                  float loadFactor,
                                  int concurrencyLevel)
  {
    super(initialCapacity,
          loadFactor,
          concurrencyLevel);
    _timestamp = System.currentTimeMillis();
  }

  /**
   * Read the last time that the timestamp was set to now
   */
  @Override
  public long getTimestamp()
  {
    return _timestamp;
  }

  /**
   * Set the timestamp to now
   */
  @Override
  public void setTimestamp()
  {
    _timestamp = System.currentTimeMillis();
  }
}