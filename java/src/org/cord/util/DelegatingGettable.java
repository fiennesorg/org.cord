package org.cord.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.collect.ImmutableList;

/**
 * Implementation of Gettable that contains an ordered list of Gettable items and passes any
 * incoming Gettable requests out to the nested Gettables in order until one of them provides an
 * answer. The order of parsing of the requests is the same as the order of Gettable registration
 * (ie the initially ordered Gettable is taken as definitive).
 * 
 * @author alex
 */
public class DelegatingGettable
  implements Gettable
{
  private final List<Gettable> __gettables;

  /**
   * @param isThreadSafe
   *          If true then the internal list of Gettables is held in a CopyOnWriteArrayList. More
   *          Gettables can be added at any time, but getting values out while be slightly slower.
   *          If false then a non-concurrent ArrayList will be used instead. In this case you should
   *          complete population of the Gettables before extracting values from the
   *          DelegatingGettable.
   */
  public DelegatingGettable(boolean isThreadSafe)
  {
    __gettables = isThreadSafe ? new CopyOnWriteArrayList<Gettable>() : new ArrayList<Gettable>();
  }

  public void addGettable(Gettable gettable)
  {
    if (gettable != null && !__gettables.contains(gettable)) {
      __gettables.add(gettable);
    }
  }

  @Override
  public String toString()
  {
    return "DelegatingGettable(" + __gettables + ")";
  }

  /**
   * Query the list of registered Gettables in order of registration until one of them provides a
   * non-null value for key, returning null if key is not defined in any Gettable.
   */
  @Override
  public Object get(Object key)
  {
    for (Gettable gettable : __gettables) {
      Object value = gettable.get(key);
      if (value != null) {
        return value;
      }
    }
    return null;
  }

  /**
   * Request this DelegatingGettable identifies which is the first Gettable to take responsibility
   * for the key, traversing in order of registration.
   * 
   * @return The first Gettable that doesn't return null when queried about the key or null if none
   *         of the Gettables admit to the key.
   */
  public Gettable getResponsibleGettable(Object key)
  {
    for (Gettable gettable : __gettables) {
      if (gettable.containsKey(key)) {
        return gettable;
      }
    }
    return null;
  }

  @Override
  public boolean containsKey(Object key)
  {
    for (Gettable gettable : __gettables) {
      if (gettable.containsKey(key)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    for (Gettable gettable : __gettables) {
      Collection<?> c = gettable.getValues(key);
      if (c != null) {
        return c;
      }
    }
    return null;
  }

  /**
   * Build the set of public keys by querying every nested Gettable as to their public keys and then
   * stripping out duplicates.
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    Set<String> keys = new HashSet<String>();
    for (Gettable gettable : __gettables) {
      for (String key : gettable.getPublicKeys()) {
        keys.add(key);
      }
    }
    return Collections.unmodifiableSet(keys);
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    for (Gettable gettable : __gettables) {
      Boolean b = gettable.getBoolean(key);
      if (b != null) {
        return b;
      }
    }
    return null;
  }

  @Override
  public String getString(Object key)
  {
    for (Gettable gettable : __gettables) {
      String s = gettable.getString(key);
      if (s != null) {
        return s;
      }
    }
    return null;
  }

  @Override
  public File getFile(Object key)
  {
    for (Gettable gettable : __gettables) {
      File f = gettable.getFile(key);
      if (f != null) {
        return f;
      }
    }
    return null;
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    for (Gettable gettable : __gettables) {
      Collection<?> values = gettable.getPaddedValues(key);
      if (values.size() > 0) {
        return values;
      }
    }
    return ImmutableList.of();
  }
}
