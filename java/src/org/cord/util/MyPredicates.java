package org.cord.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;

public class MyPredicates
{
  public static Predicate<String> startsWith(String prefix)
  {
    Preconditions.checkArgument(!Strings.isNullOrEmpty(prefix), "prefix");
    return new Predicate<String>() {
      @Override
      public boolean apply(String input)
      {
        return input != null && input.startsWith(prefix);
      }
    };
  }
}
