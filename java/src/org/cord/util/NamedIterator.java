package org.cord.util;

import java.util.Iterator;

import com.google.common.base.Preconditions;

/**
 * Wrapper around an Iterator that resolves Named objects inside the Iterator into their respective
 * names. Any non-Named objects will have their toString() method called.
 * 
 * @see Named#getName()
 */
public class NamedIterator
  implements Iterator<String>
{
  private final Iterator<?> __iterator;

  public NamedIterator(Iterator<?> iterator)
  {
    Preconditions.checkNotNull(iterator, "NamedIterator(iterator)");
    __iterator = iterator;
  }

  @Override
  public boolean hasNext()
  {
    return __iterator.hasNext();
  }

  @Override
  public String next()
  {
    Object object = __iterator.next();
    if (object instanceof Named) {
      return ((Named) object).getName();
    } else {
      return object.toString();
    }
  }

  @Override
  public void remove()
  {
    __iterator.remove();
  }
}
