package org.cord.util;

/**
 * Interface that extends Named to provide the option for a human readable version of the unique
 * machine name.
 */
public interface EnglishNamed
  extends Named
{
  /**
   * Get the name of this object in a human readable format. This is distuinguished against the
   * getName() method which returns a more programmatic unique identifier.
   * 
   * @return The human readable version of the name. If this is not defined, then implementations
   *         should return the value of getName() rather than null.
   * @see #getName()
   */
  public String getEnglishName();
}
