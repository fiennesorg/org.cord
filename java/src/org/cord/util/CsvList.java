package org.cord.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Somewhat incorrectly named class that reads Tab Seperated files rather than Comma Seperated
 * Files.
 */
public class CsvList
  extends AbstractList<String>
{
  public static List<CsvList> getCsvLists(BufferedReader bufferedReader,
                                          String seperator,
                                          boolean autoTrimValues)
      throws IOException
  {
    ArrayList<CsvList> csvLists = new ArrayList<CsvList>();
    String nextLine = null;
    while ((nextLine = bufferedReader.readLine()) != null) {
      csvLists.add(new CsvList(nextLine, seperator, autoTrimValues));
    }
    bufferedReader.close();
    return csvLists;
  }

  private List<String> __list = new ArrayList<String>();

  private final String __seperator;

  private final boolean __autoTrimValues;

  public CsvList(String line,
                 String seperator,
                 boolean autoTrimValues)
  {
    if (seperator.length() != 1) {
      throw new IllegalArgumentException("seperator is not of length 1:" + seperator);
    }
    __seperator = seperator;
    __autoTrimValues = autoTrimValues;
    StringTokenizer values = new StringTokenizer(line, __seperator, true);
    String value = null;
    while ((value = nextValue(values)) != null) {
      __list.add(value);
    }
  }

  public CsvList(String line,
                 char seperator,
                 boolean autoTrimValues)
  {
    this(line,
         Character.toString(seperator),
         autoTrimValues);
  }

  public CsvList(String line)
  {
    this(line,
         '\t',
         false);
  }

  private boolean _terminatesInSeperator = false;

  private String nextValue(StringTokenizer values)
  {
    if (values.hasMoreTokens()) {
      String value = values.nextToken();
      if (value.equals(__seperator)) {
        if (!values.hasMoreTokens()) {
          _terminatesInSeperator = true;
        }
        return "";
      }
      if (values.hasMoreTokens()) {
        values.nextToken();
        if (!values.hasMoreTokens()) {
          _terminatesInSeperator = true;
        }
      }
      value = stripQuotedQuotes(stripQuotes(value));
      return (__autoTrimValues ? value.trim() : value);
    } else {
      if (_terminatesInSeperator) {
        _terminatesInSeperator = false;
        return "";
      }
    }
    return null;
  }

  private String stripQuotes(String value)
  {
    if (value.length() > 2 && value.startsWith("\"") && value.endsWith("\"")) {
      return value.substring(1, value.length() - 1);
    }
    return value;
  }

  private String stripQuotedQuotes(String value)
  {
    return value.replaceAll("\"\"", "\"");
  }

  public String getValue(int index)
  {
    return get(index);
  }

  @Override
  public String get(int index)
  {
    try {
      return __list.get(index);
    } catch (IndexOutOfBoundsException ioobEx) {
      IndexOutOfBoundsException wrapperEx =
          new IndexOutOfBoundsException("Illegal index (" + index + ") on " + __list);
      wrapperEx.initCause(ioobEx);
      throw wrapperEx;
    }
  }

  @Override
  public int size()
  {
    return __list.size();
  }
}
