package org.cord.util;

/**
 * StringValidator that passes or fails Strings based on their length
 */
public class LengthStringValidator
  extends NamedImpl
  implements StringValidator
{
  public final static int UNDEFINED = -1;

  private final int __minimumLength;

  private final int __maximumLength;

  private final boolean __suggestTruncatedOption;

  private final String __error;

  public LengthStringValidator(String name,
                               int minimumLength,
                               int maximumLength,
                               boolean suggestTruncatedOption)
  {
    super(name);
    __minimumLength = minimumLength;
    __maximumLength = maximumLength;
    __suggestTruncatedOption = suggestTruncatedOption;
    __error = "String length doesn't fall between (" + toString(__minimumLength) + "..."
              + toString(__maximumLength) + ")";
  }

  public LengthStringValidator(String name,
                               int minimumLength,
                               int maximumLength)
  {
    this(name,
         minimumLength,
         maximumLength,
         false);
  }

  private final static String toString(int value)
  {
    return (value == UNDEFINED ? "*" : NumberUtil.toString(value));
  }

  @Override
  public boolean isValid(String string)
  {
    int stringLength = string.length();
    if (stringLength >= __minimumLength || __minimumLength == UNDEFINED) {
      if ((stringLength <= __maximumLength || __maximumLength == UNDEFINED)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public final String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return string;
    }
    String validatedString = null;
    if (__suggestTruncatedOption && __maximumLength != UNDEFINED) {
      validatedString = string.substring(0, __maximumLength);
    }
    throw new StringValidatorException(__error, string, validatedString);
  }
}
