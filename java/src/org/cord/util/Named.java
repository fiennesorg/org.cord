package org.cord.util;

/**
 * Interface to be implemented by Objects which have a String identification that uniquely
 * identifies that instance of the Object. This can then be used for both debugging, but also for
 * intelligent lookups in classes such as NamedList. The interface is specified as an extension of
 * Comparable and implementations of the interface are expected to support a natural ordering via
 * the name of the objects.
 */
public interface Named
  extends Comparable<Named>
{
  /**
   * Return the unique identifying String that identifies this instance of the Object.
   */
  public String getName();
}
