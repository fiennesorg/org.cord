package org.cord.util;

import javax.servlet.http.HttpSession;

/**
 * Wrapper around an HttpSession that provides a Gettable interface to the data source.
 * 
 * @author alex
 */
public class GettableSession
  extends AbstractGettable
{
  private final HttpSession __session;

  public GettableSession(HttpSession session)
  {
    __session = session;
  }

  protected HttpSession getSession()
  {
    return __session;
  }

  @Override
  public Object get(Object key)
  {
    return __session.getAttribute(key.toString());
  }
}
