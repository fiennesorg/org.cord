package org.cord.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple String parser that replaces named parameters within a String with the values defined in a
 * ParameterSupplier. The named parameters should be prefixed with a $ sign to signify their
 * variable status.
 */
public class ParametricStringResolver
{
  private final static Pattern PATTERN = Pattern.compile("\\$\\w+");

  private final Gettable __supplier;

  /**
   * @param supplier
   *          The Gettable that contains the mappings between the variable names and their values.
   */
  public ParametricStringResolver(Gettable supplier)
  {
    __supplier = supplier;
  }

  /**
   * Resolve the variable names inside source against the values defined in supplier.
   */
  public String resolve(String source)
  {
    StringBuffer result = new StringBuffer();
    Matcher matcher = PATTERN.matcher(source);
    while (matcher.find()) {
      String name = matcher.group();
      Object value = __supplier.get(name.substring(1));
      matcher.appendReplacement(result, value == null ? name : value.toString());
    }
    return result.length() == 0 ? source : result.toString();
  }
}
