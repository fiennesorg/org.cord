package org.cord.util;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import com.google.common.collect.ImmutableList;

/**
 * Utility abstract class that provides default implementations of all methods in Gettable except
 * for the core get method. Other methods should be overridden if there is a more appropriate
 * functionality than the default supplied functionality which is generally delegating to
 * GettableUtils
 * 
 * @author alex
 * @see org.cord.util.Gettable#get(Object)
 * @see org.cord.util.GettableUtils
 */
public abstract class AbstractGettable
  implements Gettable
{
  /**
   * @see GettableUtils#toBoolean(Object)
   */
  @Override
  public Boolean getBoolean(Object key)
  {
    return GettableUtils.toBoolean(get(key));
  }

  /**
   * @return An empty List. If your implementation has a better solution then this method should be
   *         overriden.
   * @see Collections#emptyList()
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    return Collections.emptyList();
  }

  /**
   * @see GettableUtils#wrapValue(Object)
   */
  @Override
  public Collection<?> getValues(Object key)
  {
    return GettableUtils.wrapValue(get(key));
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    Collection<?> values = getValues(key);
    return values == null ? ImmutableList.of() : values;
  }

  @Override
  public String getString(Object key)
  {
    return StringUtils.toString(get(key));
  }

  /**
   * Use GettableUtils to wrap the Object that key points to as a File
   * 
   * @see GettableUtils#toFile(Object)
   */
  @Override
  public File getFile(Object key)
  {
    return GettableUtils.toFile(get(key));
  }

  /**
   * Crude implementation of containsKey that just checks the value associated with key against
   * null. If you have a nicer way of calculating this that is more efficient then override this
   * method.
   */
  @Override
  public boolean containsKey(Object key)
  {
    return get(key) != null;
  }
}
