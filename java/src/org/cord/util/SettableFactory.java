package org.cord.util;

/**
 * Interface that describes Objects that have an optional hotswappable Settable associated with
 * them. Due to the intended hotswappable nature of the Settable objects, clients should not cache
 * these Settable references, but should instead store the SettableFactory hook and then retrieve
 * the Settable before asking for a single parameter. If the client requires a block of parameters
 * that are all interconnected, then it is sufficient to cache the Settable for the duration of the
 * retrieval of this block of parameters thereby preventing the possibility of splitting a block of
 * parameters across two Settable sources in the unlikely event that the Settable is swapped in the
 * middle of the information retrieval.
 */
public interface SettableFactory
  extends GettableFactory
{
  /**
   * Get the Settable that is currently associated with this SettableFactory. The Settable that is
   * returned from this method should be the same reference as the Gettable that is returned from
   * the getGettable method, as this is intended purely as an alternative view onto the Gettable
   * item.
   * 
   * @return The appropriate Settable or null if there is not currently a Settable associated with
   *         this SettableFactory.
   * @see #getGettable()
   */
  public Settable getSettable();
}
