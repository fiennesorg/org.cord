package org.cord.util;

public class Casters
{
  /**
   * Caster implementation that generates String by invoking toString() on the argument.
   */
  public static final Caster<String> TOSTRING = new Caster<String>() {
    @Override
    public String cast(Object obj)
        throws ClassCastException
    {
      return obj.toString();
    }
  };

  /**
   * Caster implementation that takes an Object in and returns the same Object.
   */
  public static final Caster<Object> NULL = new Caster<Object>() {
    @Override
    public Object cast(Object obj)
        throws ClassCastException
    {
      return obj;
    }
  };

  /**
   * Caster implementation that directly casts an Object into the desired class.
   */
  @SuppressWarnings("unchecked")
  public static final <T> Caster<T> CAST()
  {
    return (Caster<T>) NULL;
  };

  /**
   * Caster implementation that utilises {@link NumberUtil#toInteger(Object)} to produce an Integer
   */
  public static final Caster<Integer> TOINT = new Caster<Integer>() {
    @Override
    public Integer cast(Object obj)
        throws ClassCastException
    {
      return NumberUtil.toInteger(obj);
    }
  };

  public static final Caster<Double> TODOUBLE = new Caster<Double>() {
    @Override
    public Double cast(Object obj)
        throws ClassCastException
    {
      return NumberUtil.toDouble(obj);
    }
  };
}
