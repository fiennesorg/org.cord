package org.cord.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public interface FtpClientImplementation
{
  public void uploadFile(File localFile,
                         String remoteServer,
                         String remoteLogin,
                         String remotePassword,
                         String remoteDirectory,
                         String remoteFilename,
                         PrintWriter log)
      throws IOException;
}
