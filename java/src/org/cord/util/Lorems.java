package org.cord.util;

import org.cord.mirror.field.StringField;

import com.google.common.base.Strings;

import net._01001111.text.LoremIpsum;

public class Lorems
{
  private static final LoremIpsum __lorem = new LoremIpsum();

  public static String getParagraph(String header,
                                    int maxLength)
  {
    header = Strings.nullToEmpty(header);
    maxLength = maxLength - header.length();
    String p = __lorem.paragraph();
    if (p.length() > maxLength) {
      int i = p.lastIndexOf('.', maxLength);
      if (i == -1) {
        p = p.substring(0, p.lastIndexOf(' ', maxLength)) + ".";
      } else {
        p = p.substring(0, p.lastIndexOf('.', maxLength) + 1);
      }
    }
    return header + p;
  }

  public static String getVarCharParagraph(String header)
  {
    return getParagraph(header, StringField.LENGTH_VARCHAR);
  }

  public static LoremIpsum getLoremIpsum()
  {
    return __lorem;
  }
}
