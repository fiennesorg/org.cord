package org.cord.util;

import java.io.File;
import java.util.Collection;

/**
 * Extension of SimpleGettable that provides various typed access methods that make it possible for
 * implementations to handle them in ways that are appropriate to their domain.
 */
public interface Gettable
  extends SimpleGettable
{
  /**
   * Retrieve a Boolean flag from this Gettable refernced by key. If the core Object is not of type
   * Boolean then the result should be Boolean.valueOf the String represenation of the value.
   * 
   * @return The appropriate Boolean value or null if there is no value defined for key.
   * @see GettableUtils#toBoolean(Object)
   */
  public Boolean getBoolean(Object key);

  /**
   * If this Gettable supports multi-valued keys then return the set if Values appropriate for this
   * key (optional operation). If there is only a single value then a Collection of size 1 should be
   * returned.
   * 
   * @return Collection of Objects or null if key is not defined for this Gettable.
   * @see GettableUtils#wrapValue(Object)
   */
  public Collection<?> getValues(Object key);

  /**
   * @return Collection of Objects, never null. If there are no values assigned to key then an empty
   *         Collection should be returned.
   */
  public Collection<?> getPaddedValues(Object key);

  /**
   * Optional method to list the keys that the Gettable is prepared to expose to untrusted clients.
   * If the key list is generated algorithmically, or you cannot expose it for whatever reason, then
   * it is better to return an empty Collection instead. Note that a key not being inside the
   * getKeys() listing doesn't mean that it cannot be utilised. The ordering of the keys is
   * undefined, but a single key should not occur more than once.
   * 
   * @see <a href="https://dev.fiennes.org/redmine/issues/5267">#5267</a>
   */
  public Collection<String> getPublicKeys();

  public String getString(Object key);

  public File getFile(Object key);
}
