package org.cord.util;

public class NumericStringValidator
  extends NamedImpl
  implements StringValidator
{
  public final static String NAME = "NumericStringValidator";

  private final boolean __autoStripChars;

  private final boolean __isOptional;

  private final boolean __acceptsNegative;

  /**
   * @param autoStripChars
   *          If true then if the inital parse of the supplied string fails, then all non-numeric
   *          chars will be stripped out and the parse re-tried. If this then succeeds then a
   *          StringValidatorException is thrown with the stripped value as the proposed value.
   * @param isOptional
   *          If true then a value of null or "" will be passed by the validate method. If the
   *          autoStripChars is true then the system will approve a stripped string that equals ""
   *          and throw this in the StringValidatorException as a proposed value.
   * @param acceptsNegative
   *          If true then parsed string below the value of 0 will be accepted, otherwise the
   *          validator will only pass numeric values that are greater than or equal to 0.
   */
  public NumericStringValidator(boolean autoStripChars,
                                boolean isOptional,
                                boolean acceptsNegative)
  {
    super(NAME);
    __autoStripChars = autoStripChars;
    __isOptional = isOptional;
    __acceptsNegative = acceptsNegative;
  }

  private String stripChars(String string)
  {
    if (string == null) {
      return null;
    }
    StringBuilder result = new StringBuilder(string.length());
    for (int i = 0; i < string.length(); i++) {
      char c = string.charAt(i);
      if (Character.isDigit(c)) {
        result.append(c);
      }
    }
    return result.toString();
  }

  @Override
  public boolean isValid(String string)
  {
    if (__isOptional && (string == null || string.length() == 0)) {
      return true;
    }
    try {
      long value = Long.parseLong(string);
      return (__acceptsNegative ? true : value >= 0);
    } catch (NumberFormatException nfEx) {
      return false;
    }
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return string;
    }
    String proposedString = null;
    if (__autoStripChars) {
      String strippedString = stripChars(string);
      if (isValid(strippedString)) {
        proposedString = strippedString;
      }
    }
    throw new StringValidatorException("not a numeric value", string, proposedString);
  }
}
