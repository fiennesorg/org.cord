package org.cord.util;

public class Booleans
{
  /**
   * Convert an Object into a Boolean, utilising numbers equalling zero as false and with assignable
   * behaviour for nulls
   * 
   * @param obj
   *          The Object to convert
   * @param nullValue
   *          The value to return if obj is null
   * @param emptyStringValue
   *          The value to return if obj cannot be resolved by any other method and its toString
   *          representation is "". To get the same behaviour as the standard
   *          Boolean.valueOf(string) use Boolean.FALSE. However it may be useful to use null if you
   *          have a system that interchanges "" and null representing the same value.
   * @return The appropriate Boolean or nullValue if obj is null or emptyStringValue if obj.toString
   *         is ""
   */
  public static Boolean valueOf(Object obj,
                                Boolean nullValue,
                                Boolean emptyStringValue)
  {
    if (obj == null) {
      return nullValue;
    }
    if (obj instanceof Boolean) {
      return (Boolean) obj;
    }
    if (obj instanceof Number) {
      Number n = (Number) obj;
      return Boolean.valueOf(n.intValue() != 0);
    }
    String string = obj.toString();
    if ("".equals(string)) {
      return emptyStringValue;
    }
    return Boolean.valueOf(string);
  }
}
