package org.cord.util;

import java.util.Collection;
import java.util.Collections;

/**
 * Wrapper around an existing Gettable Object that makes it conform to the Settable interface, but
 * throws a SettableException if the values are attempted to be changed. This therefor lets you use
 * read only implementations of Gettable Objects in situations when Settable Objects are expected,
 * but where you know that the mutability of the Settable interface is not going to be invoked.
 */
public class ImmutableSettableGettable
  extends ForwardingGettable
  implements Settable
{
  public ImmutableSettableGettable(Gettable gettable)
  {
    super(gettable);
  }

  @Override
  public Collection<String> getSettableKeys()
  {
    return Collections.emptyList();
  }

  @Override
  public Object set(String key,
                    Object value)
      throws SettableException
  {
    throw new SettableException("ImmutableSettableGettable is immutable!", this, key, value, null);
  }

  @Override
  public ValueType getValueType(String key)
  {
    return null;
  }

  @Override
  public Object set(TypedKey<?> typedKey,
                    Object value)
      throws SettableException
  {
    return set(typedKey.getKeyName(), value);
  }

}