package org.cord.util;

public interface SingletonShutdownable
{
  public void shutdownSingleton();
}
