package org.cord.util;

import java.util.Iterator;

import com.google.common.base.Preconditions;

/**
 * Wrapper around an Iterator that skips any element in the iteration that is either an instance of
 * or not an instance of a specified Class.
 */
public class ClassSkippingIterator<T>
  extends SkippingIterator<T>
{
  private final Class<?> __targetClass;

  private final boolean _skipTargetClass;

  /**
   * @param iterator
   *          the source Iterator
   * @param targetClass
   *          The Class that the Objects in the source Iterator must either confirm or not confirm
   *          to. nulls trigger a NullPointerException.
   * @param skipTargetClass
   *          If true then all instances of targetClass will be skipped, if false then all instances
   *          of non-targetClass will be skipped.
   */
  public ClassSkippingIterator(Iterator<T> iterator,
                               Class<?> targetClass,
                               boolean skipTargetClass)
  {
    super(iterator);
    __targetClass =
        Preconditions.checkNotNull(targetClass,
                                   "ClassSkippingIterator does not accept null classes");
    _skipTargetClass = skipTargetClass;
  }

  @Override
  protected boolean shouldSkip(T object)
  {
    return _skipTargetClass ? __targetClass.isInstance(object) : !__targetClass.isInstance(object);
  }
}
