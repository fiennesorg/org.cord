package org.cord.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONArray;
import org.json.WritableJSONObject;

import com.google.common.base.Charsets;
import com.google.common.io.Closeables;

/**
 * Static utility methods for doing stuff to do with Crockford's JSON implementation.
 * 
 * @author alex
 */
public class JSONUtils
{
  private JSONUtils()
  {
  }

  /**
   * If an Object is an instance of {@link WritableJSONObject} or {@link WritableJSONArray} then
   * wrap them in {@link JSONObjectGettable} and {@link JSONArrayList} as appropriate. Other Objects
   * get passed through untouched.
   * 
   * @param obj
   *          The optional Object to be wrapped
   */
  public static Object asImmutable(Object obj,
                                   boolean exposesSource)
  {
    if (obj instanceof JSONObject) {
      return new JSONObjectGettable((JSONObject) obj, exposesSource);
    }
    if (obj instanceof JSONArray) {
      return new JSONArrayList((JSONArray) obj, exposesSource);
    }
    return obj;
  }

  public static JSONObject readJSONObject(File file,
                                          JSONBuilder jsonBuilder)
      throws IOException, JSONException
  {
    return jsonBuilder.toJSONObject(new InputStreamReader(new FileInputStream(file),
                                                          Charsets.UTF_8));
    // Reader reader = new InputStreamReader(new FileInputStream(file), Charsets.UTF_8);
    // try {
    // return new WritableJSONObject(new JSONTokener(reader, jsonBuilder));
    // } finally {
    // Closeables.close(reader, false);
    // }
  }

  public static <A extends JSONArray, O extends JSONObject> A readJSONArray(File file,
                                                                            JSONBuilder<A, O> jsonBuilder)
      throws JSONException, IOException
  {
    return jsonBuilder.toJSONArray(new InputStreamReader(new FileInputStream(file),
                                                         Charsets.UTF_8));
    // try {
    // return new WritableJSONArray(new JSONTokener(reader, WritableJSONFactory.getInstance()));
    // } finally {
    // Closeables.close(reader, false);
    // }
  }

  public static void write(File file,
                           JSONArray jArr,
                           int indent)
      throws IOException, JSONException
  {
    Writer writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8);
    try {
      if (indent < 1) {
        jArr.write(writer);
      } else {
        writer.write(jArr.toString(indent));
      }
      writer.append('\n');
    } finally {
      Closeables.close(writer, false);
    }
  }

  public static void write(File file,
                           JSONArray jArr)
      throws IOException, JSONException
  {
    write(file, jArr, 0);
  }

  public static void write(File file,
                           JSONObject jObj,
                           int indent)
      throws IOException, JSONException
  {
    Writer writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8);
    try {
      if (indent < 1) {
        jObj.write(writer);
      } else {
        writer.write(jObj.toString(indent));
      }
      writer.append('\n');
    } finally {
      Closeables.close(writer, false);
    }
  }

  public static void write(File file,
                           JSONObject jObj)
      throws IOException, JSONException
  {
    write(file, jObj, 0);
  }

  /**
   * Get a value from a JSONObject with expansion of dotted keys.
   * 
   * @return null if the key or any contained sub-key isn't found in jObj. Otherwise the defined
   *         value or NULL if the value explicitly maps to null
   * @throws ClassCastException
   *           if you reference a sub-container that is defined but which isn't a JSONObject.
   */
  public static Object get(JSONObject jObj,
                           String key)
  {
    Object obj = jObj.opt(key);
    if (obj != null) {
      return obj;
    }
    int i = key.indexOf('.');
    if (i == -1) {
      return null;
    }
    String keyHeader = key.substring(0, i);
    obj = jObj.opt(keyHeader);
    if (obj == null) {
      return null;
    }
    if (obj instanceof JSONObject) {
      return get((JSONObject) obj, key.substring(i + 1));
    }
    throw new ClassCastException(String.format("%s maps to %s which is not a JSONObject",
                                               keyHeader,
                                               obj));
  }

  /**
   * put a value into a JSONObject with expansion of dotted keys. If the referenced sub-JSONObjects
   * don't exist then create them as necessary.
   * 
   * @see JSONObject#put(String, Object)
   * @throws JSONException
   *           if the put operation fails
   * @throws ClassCastException
   *           if you reference a sub-container that isn't a JSONObject and which therefore can't be
   *           a valid target for put
   */
  public static void put(JSONObject jObj,
                         String key,
                         Object value)
      throws JSONException
  {
    int i = key.indexOf(".");
    if (i == -1) {
      jObj.put(key, value);
      return;
    }
    String keyHeader = key.substring(0, i);
    Object obj = jObj.opt(keyHeader);
    JSONObject subJObj = null;
    if (obj == null) {
      subJObj = new WritableJSONObject();
      jObj.put(keyHeader, subJObj);
    } else if (obj instanceof JSONObject) {
      subJObj = (JSONObject) obj;
    } else {
      throw new ClassCastException(String.format("%s maps to %s which is not a JSONObject",
                                                 keyHeader,
                                                 obj));
    }
    put(subJObj, key.substring(i + 1), value);
  }

  public static JSONObject asJSONObject(Gettable gettable)
      throws JSONException
  {
    JSONObject jObj = new WritableJSONObject();
    for (String key : gettable.getPublicKeys()) {
      jObj.put(key.toString(), gettable.get(key));
    }
    return jObj;
  }

  private static void addToJSON(JSONObject jObj,
                                String key,
                                String separator,
                                Object value)
      throws JSONException
  {
    int i = key.indexOf(separator);
    if (i == -1) {
      jObj.put(key, value);
      return;
    }
    String namespace = key.substring(0, i);
    JSONObject subJObj = jObj.optJSONObject(namespace);
    if (subJObj == null) {
      subJObj = new WritableJSONObject();
      jObj.put(namespace, subJObj);
    }
    addToJSON(subJObj, key.substring(i + separator.length()), separator, value);
  }

  public static JSONObject asJSONObject(Gettable gettable,
                                        String separator)
      throws JSONException
  {
    JSONObject jObj = new WritableJSONObject();
    for (String key : gettable.getPublicKeys()) {
      addToJSON(jObj, key, separator, gettable.get(key));
    }
    return jObj;
  }

  private static void toGettable(final JSONObject jObj,
                                 final SettableMap map,
                                 final String namespace,
                                 final String separator)
  {
    for (Iterator<String> i = jObj.keys(); i.hasNext();) {
      String key = i.next();
      Object value = jObj.opt(key);
      if (value != null) {
        if (value instanceof JSONObject) {
          toGettable((JSONObject) value, map, namespace + key + separator, separator);
        } else if (value instanceof JSONArray) {
          throw new NotImplementedException();
        } else {
          map.put(namespace + key, value);
        }
      }
    }
  }

  public static void addToSettable(final JSONObject jObj,
                                   final Settable settable,
                                   final String namespace,
                                   final String separator)
      throws SettableException
  {
    for (Iterator<String> i = jObj.keys(); i.hasNext();) {
      final String key = i.next();
      final Object value = jObj.opt(key);
      if (value != null) {
        if (value instanceof JSONObject) {
          addToSettable((JSONObject) value, settable, namespace + key + separator, separator);
        } else if (value instanceof JSONArray) {
          throw new NotImplementedException();
        } else {
          settable.set(namespace + key, value);
        }
      }
    }
  }

  public static SettableMap toSettableMap(JSONObject jObj,
                                          String separator)
  {
    SettableMap map = new SettableMap();
    toGettable(jObj, map, "", separator);
    return map;
  }

  public static void put(JSONObject jObj,
                         Gettable gettable,
                         String... keys)
      throws JSONException
  {
    for (String key : keys) {
      jObj.put(key, gettable.get(key));
    }
  }

  public static void put(JSONObject jObj,
                         Gettable gettable,
                         TypedKey<?>... keys)
      throws JSONException
  {
    for (TypedKey<?> key : keys) {
      jObj.put(key.getKeyName(), gettable.get(key));
    }
  }

  public static void put(JSONObject jObj,
                         Gettable gettable,
                         Iterable<String> keys)
      throws JSONException
  {
    for (String key : keys) {
      jObj.put(key, gettable.get(key));
    }
  }
}
