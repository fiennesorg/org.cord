package org.cord.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cord.node.NodeServlet;
import org.cord.node.requires.RequirementMgr;
import org.cord.webmacro.FormsTool;
import org.cord.webmacro.VarsTool;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

public final class HtmlUtilsOriginal
  extends HtmlUtilsTheme
{
  // public static final String ACTIVENAMEPOSTFIX = "-active";

  /**
   * CSS class that will be applied to the div that contains a widget. In addition the widget will
   * also contain a more specific CSS class that describes the type of widget that it contains. The
   * repeatable layout can be defined in this class and then the differences from the default for
   * specific types can be defined in the more specific classes. Note that this will also be applied
   * to forms that are classified as CSS_W_VALUE (w_value) even though they don't have a widget, but
   * they occupy the space that a widget would have done if it exists.
   * 
   * @see #CSS_W_VALUE
   */
  private static final String CSS_W_WIDGET = "w_widget";

  private static final String CSS_W_TEXT = "w_text";

  private static final String CSS_W_TEXTAREA = "w_textarea";

  private static final String CSS_W_SELECT = "w_select";

  private static final String CSS_W_MULTISELECT = "w_multiSelect";

  private static final String CSS_W_CHECKBOX = "w_checkbox";

  private static final String CSS_W_FILE = "w_file";

  private static final String CSS_W_VALUE = "w_value";

  // private static final String CSS_W_DATE = "w_date";

  private static final String CSS_W_SUBMIT = "w_submit";

  private static final String CSS_W_CANCEL = "w_cancel";

  private static final String CSS_W_RADIO = "w_radio";

  // private static final String CSS_W_RADIOS = "w_radios";

  private static final String CSS_W_COMPULSORY = "w_compulsory";

  // private static final String CSS_W_ISDEFINED = "w_isDefined";

  /**
   * The prefix that is applied to class values for Widget Interface items. This will apply to the
   * actual widgets in terms of INPUT, TEXTAREA, SELECT etc etc and is added as a prefix to ensure
   * that it doesn't clash with existing CSS rules due to the naming of the fields in the database.
   */
  private static final String CSS_WI_CLASS = "wi_";

  /**
   * Class that is applied to a div around a label and widget pair.
   */
  private static final String CSS_W_LABELWIDGET = "w_labelWidget";

  /**
   * Suffix added to basic widget class when creating a widget around a label and widget pair
   */
  private static final String LABELWIDGETSUFFIX = "LabelWidget";

  // public static void openSpan(Appendable buf,
  // String id,
  // Object... classNames)
  // throws IOException
  // {
  // buf.append("<span");
  // addAttribute(buf, "id", id);
  // addAttribute(buf, "class", StringUtils.join(" ", classNames));
  // buf.append(">");
  // }

  @Override
  public void openWidgetDiv(Appendable buf,
                            String className,
                            Object... ids)
      throws IOException
  {
    HtmlUtilsTheme.openDiv(buf, appendIds(ids), CSS_W_WIDGET, className);
  }

  private static void openLabelWidgetDiv(Appendable buf,
                                         boolean isCompulsory)
      throws IOException
  {
    if (isCompulsory) {
      HtmlUtilsTheme.openDiv(buf, null, CSS_W_LABELWIDGET, CSS_W_COMPULSORY);
    } else {
      HtmlUtilsTheme.openDiv(buf, null, CSS_W_LABELWIDGET);
    }
  }

  private static void openLabelWidgetDiv(Appendable buf,
                                         String widget,
                                         boolean isCompulsory)
      throws IOException
  {
    if (isCompulsory) {
      HtmlUtilsTheme.openDiv(buf,
                             null,
                             CSS_W_LABELWIDGET,
                             widget + LABELWIDGETSUFFIX,
                             CSS_W_COMPULSORY);
    } else {
      HtmlUtilsTheme.openDiv(buf, null, CSS_W_LABELWIDGET, widget + LABELWIDGETSUFFIX);
    }
  }

  @Override
  public void openLabelWidget(Appendable buf,
                              String label,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    openLabelWidgetDiv(buf, isCompulsory);
    if (!Strings.isNullOrEmpty(label)) {
      label(buf, label, isCompulsory, ids);
    }
  }

  @Override
  public void openLabelWidget(Appendable buf,
                              String label,
                              String widget,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    openLabelWidgetDiv(buf, widget, isCompulsory);
    if (!Strings.isNullOrEmpty(label)) {
      label(buf, label, isCompulsory, ids);
    }
  }

  // public static void openParagraph(Appendable out,
  // String id,
  // Object... classNames)
  // throws IOException
  // {
  // out.append("<p");
  // addAttribute(out, "id", id);
  // addAttribute(out, "class", StringUtils.join(" ", classNames));
  // out.append(">");
  // }

  // public static void closeParagraph(Appendable out)
  // throws IOException
  // {
  // out.append("</p>\n");
  // }

  /**
   * @param contents
   *          The visible contents of the label. Note that this value is currently not escaped or
   *          validated in any way as inline markup inside a label is permitted in HTML.
   */
  @Override
  public void label(Appendable buf,
                    String contents,
                    boolean isCompulsory,
                    Object... ids)
      throws IOException
  {
    buf.append("<label for=\"");
    appendIds(buf, ids);
    buf.append("\">");
    if (isCompulsory) {
      buf.append("<em>");
    }
    buf.append(contents);
    if (isCompulsory) {
      buf.append("</em>");
    }
    buf.append("</label>\n");
  }

  /**
   * @param value
   *          The optional data that will be placed inside the w_value div. This isn't escaped at
   *          all so it is possible to nest active HTML inside the div, but this does mean that the
   *          responsibility of ensuring that the HTML is well-formed rests with the invoking class.
   *          If the value is null then the behaviour will be dependable on the Appendable buf that
   *          is passed in, but this will normally be to render the word "null"
   */
  @Override
  public void value(Appendable buf,
                    Object value,
                    Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_VALUE, ids);
    buf.append(StringUtils.toString(value));
    closeDiv(buf);
  }

  @Override
  public void errorValue(Appendable buf,
                         String error,
                         Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_VALUE, ids);
    HtmlUtilsTheme.openDiv(buf, null, "error");
    buf.append(error);
    closeDiv(buf);
    closeDiv(buf);

  }

  @Override
  public void labelErrorValue(Appendable buf,
                              String label,
                              Object value,
                              Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_VALUE, false, ids);
    errorValue(buf, value.toString(), ids);
    closeDiv(buf);
  }

  /**
   * Append a label and static value to the Form.
   */
  @Override
  public void labelValue(Appendable buf,
                         String label,
                         Object value,
                         Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_VALUE, false, ids);
    value(buf, value, ids);
    closeDiv(buf);
  }

  private static void appendIdNameClass(Appendable buf,
                                        Object... ids)
      throws IOException
  {
    buf.append(" id=\"");
    appendIds(buf, ids);
    buf.append("\" name=\"");
    appendIds(buf, ids);
    buf.append("\" class=\"").append(CSS_WI_CLASS);
    appendIds(buf, ids);
    buf.append("\"");
  }

  private static void appendIdNameCustomClass(Appendable buf,
                                              String customClass,
                                              Object... ids)
      throws IOException
  {
    buf.append(" id=\"");
    appendIds(buf, ids);
    buf.append("\" name=\"");
    appendIds(buf, ids);
    buf.append("\" class=\"");
    if (customClass != null) {
      buf.append(customClass).append(' ');
    }
    buf.append(CSS_WI_CLASS);
    appendIds(buf, ids);
    buf.append("\"");
  }

  @Override
  public void textWidget(String autocomplete,
                         Appendable buf,
                         String value,
                         Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_TEXT);
    textInputWidget(autocomplete, buf, value, ids);
    closeDiv(buf);
  }

  /**
   * Create an {@link #autocompleteWidget(Appendable, String, Iterable, Object...)} with the
   * appropriate labelling. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on
   * the file dependencies, but this is the responsibility of the invoking method.
   */
  @Override
  public void autocomplete(Appendable buf,
                           String label,
                           String value,
                           boolean isCompulsory,
                           Iterable<String> options,
                           Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_TEXT, isCompulsory, ids);
    autocompleteWidget(buf, value, options, ids);
    closeDiv(buf);
  }

  /**
   * Invoke {@link #autocompleteInputWidget(Appendable, String, Iterable, Object...)} wrapped in a
   * widget div. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on the file
   * dependencies, but this is the responsibility of the invoking method.
   */
  @Override
  public void autocompleteWidget(Appendable buf,
                                 String value,
                                 Iterable<String> options,
                                 Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_TEXT);
    autocompleteInputWidget(buf, value, options, ids);
    closeDiv(buf);
  }

  /**
   * Render a {@link #textInputWidget(String, Appendable, String, Object...)} with the appropriate
   * javascript to give autocompletion. This needs to have had {@link RequirementMgr#KEY_QUERY_UI}
   * registered on the file dependencies, but this is the responsibility of the invoking method.
   */
  @Override
  public void autocompleteInputWidget(Appendable buf,
                                      String value,
                                      Iterable<String> options,
                                      Object... ids)
      throws IOException
  {
    buf.append("<script>\n" + "$(function() {\n" + "var availableTags = [\n");
    for (Iterator<String> i = options.iterator(); i.hasNext();) {
      String option = i.next();
      if (option != null) {
        buf.append('"').append(StringUtils.toHtmlValue(option)).append('"');
        if (i.hasNext()) {
          buf.append(",");
        }
        buf.append('\n');
      }
    }
    buf.append("];\n" + "$( \"#");
    appendIds(buf, ids);
    buf.append("\" ).autocomplete({\n" + "source: availableTags\n" + "});\n" + "});\n"
               + "</script>\n");
    textInputWidget(null, buf, value, ids);
  }

  @Override
  public void textInputWidget(String autocomplete,
                              Appendable buf,
                              String value,
                              Object... ids)
      throws IOException
  {
    textInputWidget(autocomplete, buf, null, value, ids);
  }

  @Override
  public void textInputWidget(String autocomplete,
                              Appendable buf,
                              String cssClass,
                              String value,
                              Object... ids)
      throws IOException
  {
    buf.append("<input type=\"text\"");
    appendIdNameCustomClass(buf, cssClass, ids);
    addAttribute(buf, "value", value);
    addAttribute(buf, "autocomplete", autocomplete);
    buf.append(" />");
  }

  @Override
  public void text(String autocomplete,
                   Appendable buf,
                   String label,
                   String value,
                   boolean isCompulsory,
                   Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_TEXT, isCompulsory, ids);
    textWidget(autocomplete, buf, value, ids);
    closeDiv(buf);
  }

  private static final SimpleDateFormat __dateSelectorFormat = new SimpleDateFormat("dd/MM/yyyy");
  private static final DateTimeFormatter __jodaDateFormat = DateTimeFormat.forPattern("dd/MM/yyyy");

  @Override
  public Date parseDateSelector(Object date)
  {
    return Dates.parse(__dateSelectorFormat, date);
  }

  @Override
  public String formatDateSelector(Date date)
  {
    return Dates.format(__dateSelectorFormat, date);
  }

  @Override
  public Optional<DateTime> jodaDateParse(String text)
  {
    try {
      return Optional.of(__jodaDateFormat.parseDateTime(text));
    } catch (Exception e) {
      return Optional.absent();
    }
  }

  @Override
  public String jodaDateFormat(Optional<DateTime> date)
  {
    return date.isPresent() ? __jodaDateFormat.print(date.get()) : "";
  }

  @Override
  public void dateSelector(Appendable buf,
                           Map<Object, Object> context,
                           String label,
                           Date value,
                           Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_QUERY_UI);
    text(null, buf, label, Dates.format(__dateSelectorFormat, value), false, ids);
    buf.append("<script type='text/javascript'>$('#");
    appendIds(buf, ids);
    buf.append("').datepicker({ dateFormat:'dd/mm/yy', changeMonth:true, changeYear:true});</script>\n");
  }

  private void jodaDateJavascript(Appendable buf,
                                  Object... ids)
      throws IOException
  {
    buf.append("<script type='text/javascript'>$('#");
    appendIds(buf, ids);
    buf.append("').datepicker({ dateFormat:'dd/mm/yy', changeMonth:true, changeYear:true});</script>\n");
  }

  @Override
  public void jodaDate(Appendable buf,
                       Map<Object, Object> context,
                       String label,
                       Optional<DateTime> value,
                       boolean isCompulsory,
                       Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_QUERY_UI);
    text(null,
         buf,
         label,
         value.isPresent() ? __jodaDateFormat.print(value.get()) : "",
         isCompulsory,
         ids);
    jodaDateJavascript(buf, ids);
  }

  @Override
  public void jodaDateWidget(Appendable buf,
                             Map<Object, Object> context,
                             Optional<DateTime> value,
                             Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_QUERY_UI);
    textWidget(null, buf, value.isPresent() ? __jodaDateFormat.print(value.get()) : "", ids);
    jodaDateJavascript(buf, ids);
  }

  private static final SimpleDateFormat __dateTimeSelectorFormat =
      new SimpleDateFormat("dd/MM/yyyy HH:mm");
  private static final DateTimeFormatter __jodaTimeFormat =
      DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");

  @Override
  public Date parseDateTimeSelector(Object dateTime)
  {
    return Dates.parse(__dateTimeSelectorFormat, dateTime);
  }

  @Override
  public String formatDateTimeSelector(Date date)
  {
    return Dates.format(__dateTimeSelectorFormat, date);
  }

  @Override
  public Optional<DateTime> jodaTimeParse(String text)
  {
    try {
      return Optional.of(__jodaTimeFormat.parseDateTime(text));
    } catch (Exception e) {
      return Optional.absent();
    }
  }

  @Override
  public String jodaTimeFormat(Optional<DateTime> dateTime)
  {
    return dateTime.isPresent() ? __jodaTimeFormat.print(dateTime.get()) : "";
  }

  private void jodaTimeJavascript(Appendable buf,
                                  Object... ids)
      throws IOException
  {
    buf.append("<script type='text/javascript'>$('#");
    appendIds(buf, ids);
    buf.append("').datetimepicker({ dateFormat: 'dd/mm/yy', timeFormat: 'hh:mm'});</script>\n");
  }

  @Override
  public void dateTimeSelector(Appendable buf,
                               Map<Object, Object> context,
                               String label,
                               Optional<Date> value,
                               Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_JQUERY_TIMEPICKER);
    text(null,
         buf,
         label,
         value.isPresent() ? Dates.format(__dateTimeSelectorFormat, value.get()) : "",
         false,
         ids);
    jodaTimeJavascript(buf, ids);
  }

  @Override
  public void dateTimeSelectorWidget(Appendable buf,
                                     Map<Object, Object> context,
                                     Optional<Date> value,
                                     Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_JQUERY_TIMEPICKER);
    textWidget(null,
               buf,
               value.isPresent() ? Dates.format(__dateTimeSelectorFormat, value.get()) : "",
               ids);
    jodaTimeJavascript(buf, ids);
  }

  @Override
  public void jodaTime(Appendable buf,
                       Map<Object, Object> context,
                       String label,
                       Optional<DateTime> value,
                       boolean isCompulsory,
                       Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_JQUERY_TIMEPICKER);
    text(null, buf, label, jodaTimeFormat(value), isCompulsory, ids);
    jodaTimeJavascript(buf, ids);
  }

  @Override
  public void jodaTimeWidget(Appendable buf,
                             Map<Object, Object> context,
                             Optional<DateTime> value,
                             Object... ids)
      throws IOException
  {
    VarsTool.getRequirements(context).needs(RequirementMgr.KEY_JQUERY_TIMEPICKER);
    textWidget(null, buf, jodaTimeFormat(value), ids);
    jodaTimeJavascript(buf, ids);
  }

  public void passwordWidget(String autocomplete,
                             Appendable buf,
                             String value,
                             Object... ids)
      throws IOException
  {
    value = StringUtils.toHtmlValue(value);
    openWidgetDiv(buf, CSS_W_TEXT);
    buf.append("<input");
    appendIdNameClass(buf, ids);
    addAttribute(buf, "autocomplete", autocomplete);
    addAttribute(buf, "type", "password");
    addAttribute(buf, "value", value);
    buf.append(" />");
    closeDiv(buf);
  }

  @Override
  public void password(String autocomplete,
                       Appendable buf,
                       String label,
                       String value,
                       boolean isCompulsory,
                       Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_TEXT, isCompulsory, ids);
    passwordWidget(autocomplete, buf, value, ids);
    closeDiv(buf);
  }

  /**
   * Append an HTML textarea with no surrounding divs.
   */
  // TODO: merge textareaRaw into textareaWidget if not required elsewhere...
  private static void textareaRaw(Appendable buf,
                                  String value,
                                  int cols,
                                  int rows,
                                  Object... ids)
      throws IOException
  {
    value = StringUtils.noHtml(value);
    buf.append("<textarea");
    appendIdNameClass(buf, ids);
    if (cols > 0) {
      buf.append(" cols=\"").append(NumberUtil.toString(cols)).append("\"");
    }
    if (rows > 0) {
      buf.append(" rows=\"").append(NumberUtil.toString(rows)).append("\"");
    }
    buf.append(">").append(value).append("</textarea>");
  }

  /**
   * Append an HTML textarea surrounded by a {@link #CSS_W_TEXTAREA} div.
   */
  @Override
  public void textareaWidget(Appendable buf,
                             String value,
                             int cols,
                             int rows,
                             boolean isCompulsory,
                             Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_TEXTAREA);
    textareaRaw(buf, value, cols, rows, ids);
    closeDiv(buf);
  }

  @Override
  public void textarea(Appendable buf,
                       String label,
                       String value,
                       int cols,
                       int rows,
                       boolean isCompulsory,
                       Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_TEXTAREA, isCompulsory, ids);
    textareaWidget(buf, value, cols, rows, isCompulsory, ids);
    closeDiv(buf);
  }

  @Override
  public void openSelect(Appendable buf,
                         boolean isMultiple,
                         boolean shouldReloadOnChange,
                         boolean isCompulsory,
                         Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, isMultiple ? CSS_W_MULTISELECT : CSS_W_SELECT);
    openSelectWidget(buf, null, isMultiple, shouldReloadOnChange, isCompulsory, ids);
  }

  @Override
  public void openSelectWidget(Appendable buf,
                               String cssClass,
                               boolean isMultiple,
                               boolean shouldReloadOnChange,
                               boolean isCompulsory,
                               Object... ids)
      throws IOException
  {
    cssClass = StringUtils.toHtmlValue(cssClass);
    if (isMultiple) {
      activeIndicator(buf, ids);
    }
    buf.append("<select");
    appendIdNameCustomClass(buf, cssClass, ids);
    if (isMultiple) {
      addAttribute(buf, "multiple", "multiple");
    }
    if (shouldReloadOnChange) {
      addAttribute(buf, "onchange", "this.form.submit()");
    }
    buf.append(">\n");
  }

  /**
   * @param valueObj
   *          The value that will be submitted if this option is selected. Will be converted to a
   *          safe HTML value.
   * @param contentsObj
   *          The human visible data that will be used to represent this value. Will be converted to
   *          a safe HTML value
   * @param isSelected
   *          Whether or not the value should be selected automatically in the GUI
   * @see StringUtils#toHtmlValue(Object)
   */
  @Override
  public void option(Appendable buf,
                     Object valueObj,
                     Object contentsObj,
                     boolean isSelected)
      throws IOException
  {
    buf.append("<option");
    if (isSelected) {
      addAttribute(buf, "selected", "selected");
    }
    addAttribute(buf, "value", valueObj);
    buf.append(">").append(StringUtils.noHtml(contentsObj)).append("</option>\n");
  }

  @Override
  public void closeSelectWidget(Appendable buf)
      throws IOException
  {
    buf.append("</select>\n");
  }

  /**
   * @param summary
   *          The optional summary which will be included between the close select and the close
   *          div. There is no filtering applied to this summary as it may contain HTML
   */
  @Override
  public void closeSelect(Appendable buf,
                          String summary)
      throws IOException
  {
    closeSelectWidget(buf);
    if (!Strings.isNullOrEmpty(summary)) {
      buf.append(summary);
    }
    closeDiv(buf);
  }

  /**
   * Render an input field of type checkbox into a containinger value div in the output buffer.
   */
  @Override
  public void checkboxInput(Appendable buf,
                            String value,
                            boolean isChecked,
                            boolean shouldReloadOnChange,
                            boolean isCompulsory,
                            Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_CHECKBOX);
    checkboxInputWidget(buf, value, isChecked, shouldReloadOnChange, isCompulsory, ids);
    closeDiv(buf);
  }

  /**
   * Render an input field of type checkbox into the output buffer.
   * 
   * @param value
   *          the HTML value that will be submitted if the checkbox is checked. If null then the
   *          string "true" will be utilised.
   */
  @Override
  public void checkboxInputWidget(Appendable buf,
                                  String value,
                                  boolean isChecked,
                                  boolean shouldReloadOnChange,
                                  boolean isCompulsory,
                                  Object... ids)
      throws IOException
  {
    value = StringUtils.toHtmlValue(StringUtils.padEmpty(value, "true"));
    activeIndicator(buf, ids);
    buf.append("<input type=\"checkbox\"");
    appendIdNameClass(buf, ids);
    buf.append(" value=\"").append(value).append("\"");
    if (isChecked) {
      buf.append(" checked=\"checked\"");
    }
    if (shouldReloadOnChange) {
      buf.append(" onclick=\"this.form.submit()\"");
    }
    buf.append(" />");
  }

  // public static void checkboxInputWidget(Appendable buf,
  // String value,
  // String targetAnchor,
  // boolean isChecked,
  // Object... ids)
  // throws IOException
  // {
  // value = StringUtils.toHtmlValue(StringUtils.padEmpty(value, "true"));
  // activeIndicator(buf, ids);
  // buf.append("<input type=\"checkbox\"");
  // appendIdNameClass(buf, ids);
  // buf.append(" value=\"").append(value).append("\"");
  // if (isChecked) {
  // buf.append(" checked=\"checked\"");
  // }
  // if (StringUtils.notEmpty(targetAnchor)) {
  // buf.append(" onclick=\"this.form.submit('").append(targetAnchor).append("')\"");
  // }
  // buf.append(" />");
  // }

  public void radioWidget(Appendable buf,
                          String name,
                          String value,
                          boolean isChecked,
                          String label,
                          Object... ids)
      throws IOException
  {
    buf.append("<input type=\"radio\"");
    appendIdNameClass(buf, ids);
    addAttribute(buf, "name", name);
    addAttribute(buf, "value", value);
    if (isChecked) {
      addAttribute(buf, "checked", "checked");
    }
    buf.append(" />");
    label(buf, label, false, ids);
  }

  @Override
  public void radio(Appendable buf,
                    String name,
                    String value,
                    boolean isChecked,
                    String label,
                    Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_RADIO);
    radioWidget(buf, name, value, isChecked, label, ids);
    closeDiv(buf);
  }

  @Override
  public void openRadios(Appendable buf,
                         String label)
      throws IOException
  {
    buf.append("<fieldset class=\"w_radios\">")
       .append("<legend class=\"w_radios\">")
       .append(StringUtils.noHtml(label))
       .append("</legend>\n");
  }

  @Override
  public void closeRadios(Appendable buf)
      throws IOException
  {
    buf.append("</fieldset>\n");
  }

  /**
   * Render an input field of type checkbox into the output buffer.
   */
  @Override
  public void checkboxInputWidget(Appendable buf,
                                  String value,
                                  boolean isChecked,
                                  String confirmOnClickMessage,
                                  boolean shouldReloadOnChange,
                                  boolean isCompulsory,
                                  Object... ids)
      throws IOException
  {
    value = StringUtils.toHtmlValue(StringUtils.padEmpty(value, "true"));
    activeIndicator(buf, ids);
    buf.append("<input type=\"checkbox\"");
    appendIdNameClass(buf, ids);
    buf.append(" value=\"").append(value).append("\"");
    if (isChecked) {
      buf.append(" checked=\"checked\"");
    }
    if (shouldReloadOnChange) {
      if (confirmOnClickMessage == null) {
        buf.append(" onclick=\"this.form.submit()\"");
      } else {
        buf.append(" onclick=\"if (confirm('")
           .append(confirmOnClickMessage)
           .append("')) { this.form.submit(); } else { this.checked=true }\")");
      }
    } else {
      if (confirmOnClickMessage != null) {
        buf.append(" onclick=\"return confirm('").append(confirmOnClickMessage).append("')");
      }
    }
    buf.append(" />");
  }

  @Override
  public void checkbox(Appendable buf,
                       String label,
                       String value,
                       boolean isChecked,
                       boolean shouldReloadOnChange,
                       boolean isCompulsory,
                       Object... ids)
      throws IOException
  {
    openLabelWidget(buf, label, CSS_W_CHECKBOX, isCompulsory, ids);
    checkboxInput(buf, value, isChecked, shouldReloadOnChange, isCompulsory, ids);
    closeDiv(buf);
  }

  @Override
  public void fileWidget(Appendable buf,
                         Object... ids)
      throws IOException
  {
    openWidgetDiv(buf, CSS_W_FILE);
    buf.append("<input type=\"file\"");
    appendIdNameClass(buf, ids);
    buf.append("/>\n");
    closeDiv(buf);
  }

  /**
   * Add an Input element with a type of button.
   * 
   * @see #submitButton(Appendable, String, String, String, String)
   */
  public void submitButtonWidget(Appendable buf,
                                 String value,
                                 String onclick,
                                 String onmouseover,
                                 String onmouseout)
      throws IOException
  {
    value = StringUtils.toHtmlValue(value);
    openWidgetDiv(buf, CSS_W_SUBMIT);
    buf.append("<input type=\"button\" value=\"").append(value).append('"');
    addAttribute(buf, "onclick", onclick);
    addAttribute(buf, "onmouseover", onmouseover);
    addAttribute(buf, "onmouseout", onmouseout);
    buf.append(" />");
    closeDiv(buf);
  }

  /**
   * Nest an input button widget inside the appropriate divs with a label.
   * 
   * @see #submitButtonWidget(Appendable, String, String, String, String)
   */
  @Override
  public void submitButton(Appendable buf,
                           String value,
                           String onclick,
                           String onmouseover,
                           String onmouseout)
      throws IOException
  {
    openLabelWidgetDiv(buf, CSS_W_SUBMIT, false);
    submitButtonWidget(buf, value, onclick, onmouseover, onmouseout);
    closeDiv(buf);
  }

  public void submitWidget(Appendable buf,
                           String value,
                           String onclick)
      throws IOException
  {
    value = StringUtils.toHtmlValue(value);
    openWidgetDiv(buf, CSS_W_SUBMIT);
    buf.append("<input");
    addAttribute(buf, "type", "submit");
    addAttribute(buf, "value", value);
    addAttribute(buf, "onclick", onclick);
    buf.append(" />");
    closeDiv(buf);
  }

  @Override
  public void submit(Appendable buf,
                     String value,
                     String onclick)
      throws IOException
  {
    openLabelWidgetDiv(buf, CSS_W_SUBMIT, false);
    submitWidget(buf, value, onclick);
    closeDiv(buf);
  }

  @Override
  public void errorBlock(Appendable buf,
                         String error,
                         String classNames)
      throws IOException
  {
    HtmlUtilsTheme.openDiv(buf, null, "error", classNames);
    buf.append(error);
    closeDiv(buf);
  }

  // public static void errorBlock(Appendable buf,
  // String error)
  // throws IOException
  // {
  // errorBlock(buf, error, null);
  // }
  //
  // public static void errorInline(Appendable buf,
  // String error)
  // throws IOException
  // {
  // openSpan(buf, null, "error");
  // buf.append(error);
  // closeSpan(buf);
  // }

  @Override
  public void includeFormValidator(Appendable out,
                                   Map<Object, Object> context)
      throws IOException
  {
    Boolean hasIncludedFormValidator =
        (Boolean) VarsTool.delegatingGet(context, FormsTool.WEBMACRO_W_HASINCLUDEDFORMVALIDATOR);
    if (Boolean.TRUE.equals(hasIncludedFormValidator)) {
      return;
    }
    String assetDomain =
        (String) VarsTool.delegatingGet(context, NodeServlet.INITPARAM_ASSETDOMAIN);
    String staticsBuildNodePath =
        (String) VarsTool.delegatingGet(context, NodeServlet.INITPARAM_STATICSBUILDNODEPATH);
    out.append("<script type=\"text/javascript\" src=\"")
       .append(assetDomain)
       .append("/")
       .append(staticsBuildNodePath)
       .append("/scripts/validateForm.js\"></script>\n");
    VarsTool.setOuter(context, FormsTool.WEBMACRO_W_HASINCLUDEDFORMVALIDATOR, Boolean.TRUE);
  }

  @Override
  public void compulsorySubmitButton(Appendable out,
                                     String value,
                                     Map<Object, Object> context,
                                     Set<Object> compulsoryFields)
      throws IOException
  {
    HtmlUtils.getTheme().includeFormValidator(out, context);
    HtmlUtilsTheme.hiddenInput(out,
                               StringUtils.join(",", compulsoryFields),
                               "requiredParameterList");
    HtmlUtilsTheme.hiddenInput(out, "0", "validSubmit");
    HtmlUtils.submitButton(out,
                           value,
                           "validateForm(this.form)",
                           "this.form.validSubmit.value=1",
                           "this.form.validSubmit.value=0");
  }

  @Override
  public void cancelButton(Appendable out,
                           String value,
                           String url)
      throws IOException
  {
    openLabelWidgetDiv(out, CSS_W_CANCEL, false);
    openWidgetDiv(out, CSS_W_CANCEL);
    out.append("<a href=\"").append(url).append("\">").append(value).append("</a>");
    closeDiv(out);
    closeDiv(out);
  }

}
