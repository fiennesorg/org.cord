package org.cord.util;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Implementation of DateFormat that behaves as {@link SimpleDateFormat} with the addition of date
 * postfixes like 1st, 2nd, 3rd etc.
 * 
 * @author alex
 */
public class NthDateFormat
  extends DateFormat
{
  private static final long serialVersionUID = 1L;

  public static final String DATE_POSTFIX = "XX";

  public static final List<String> DATE_POSTFIXES;

  static {
    String[] p = new String[32];
    for (int i = 0; i <= 31; i++) {
      switch (i) {
        case 1:
        case 21:
        case 31:
          p[i] = "st";
          break;
        case 2:
        case 22:
          p[i] = "nd";
          break;
        case 3:
        case 23:
          p[i] = "rd";
          break;
        default:
          p[i] = "th";
      }
    }
    DATE_POSTFIXES = ImmutableList.copyOf(p);
  }

  /**
   * Create a new DateFormat that will either be an NthDateFormat or a SimpleDateFormat as
   * necessary.
   */
  public static DateFormat getInstance(String format)
  {
    int i = format.indexOf(DATE_POSTFIX);
    if (i == -1) {
      return new SimpleDateFormat(format);
    }
    if (format.indexOf(DATE_POSTFIX, i + 1) != -1) {
      throw new IllegalArgumentException(String.format("\"%s\" contains more than one 'XX'",
                                                       format));
    }
    return new NthDateFormat(format);
  }

  private final SimpleDateFormat __formatter;
  private final SimpleDateFormat __parser;

  private NthDateFormat(String format)
  {
    __parser = new SimpleDateFormat(format.replace("'" + DATE_POSTFIX + "'", ""));
    __formatter = new SimpleDateFormat(format);
  }

  @Override
  public StringBuffer format(Date date,
                             StringBuffer toAppendTo,
                             FieldPosition fieldPosition)
  {
    String s;
    synchronized (__formatter) {
      s = __formatter.format(date, new StringBuffer(), fieldPosition)
                     .toString()
                     .replace(DATE_POSTFIX,
                              DATE_POSTFIXES.get(__formatter.getCalendar()
                                                            .get(Calendar.DAY_OF_MONTH)));
    }
    toAppendTo.append(s);
    return toAppendTo;
  }

  @Override
  public Date parse(String source,
                    ParsePosition pos)
  {
    source = source.replace("st", "").replace("nd", "").replace("rd", "").replace("th", "");
    return __parser.parse(source, pos);
  }

  @Override
  public int hashCode()
  {
    return __formatter.hashCode();
  }
}
