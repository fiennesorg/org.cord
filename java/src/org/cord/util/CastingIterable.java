package org.cord.util;

import java.util.Iterator;

/**
 * Iterable wrapper that means that the Iterators returned by {@link #iterator()} are
 * {@link CastingIterator}s that look like they are of a given Class.
 * 
 * @author alex
 * @param <T>
 */
public class CastingIterable<T>
  implements Iterable<T>
{
  public static <T> CastingIterable<T> of(Iterable<?> source,
                                          Class<T> valueClass)
  {
    return new CastingIterable<T>(source, valueClass);
  }

  private Iterable<?> __source;
  private Class<T> __valueClass;

  public CastingIterable(Iterable<?> source,
                         Class<T> valueClass)
  {
    __source = source;
    __valueClass = valueClass;
  }

  @Override
  public Iterator<T> iterator()
  {
    return CastingIterator.of(__source.iterator(), __valueClass);
  }

}
