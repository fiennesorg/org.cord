package org.cord.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

public class KeyHierarchyGettable
  implements Gettable
{
  private final Gettable __source;

  public KeyHierarchyGettable(Gettable source)
  {
    __source = Preconditions.checkNotNull(source, "source Gettable");
  }

  @Override
  public Object get(Object key)
  {
    Object result = null;
    for (String subkey : new KeyHierarchy(key)) {
      if ((result = __source.get(subkey)) != null) {
        return result;
      }
    }
    return result;
  }

  @Override
  public boolean containsKey(Object key)
  {
    boolean result = false;
    for (String subkey : new KeyHierarchy(key)) {
      if (result = __source.containsKey(subkey)) {
        return result;
      }
    }
    return result;
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    Boolean result = null;
    for (String subkey : new KeyHierarchy(key)) {
      if ((result = __source.getBoolean(subkey)) != null) {
        return result;
      }
    }
    return result;
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    Collection<?> result = null;
    for (String subkey : new KeyHierarchy(key)) {
      if ((result = __source.getValues(subkey)) != null) {
        return result;
      }
    }
    return result;
  }

  /**
   * @return The public keys as declared by the wrapped Gettable
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    return __source.getPublicKeys();
  }

  @Override
  public String getString(Object key)
  {
    String result = null;
    for (String subkey : new KeyHierarchy(key)) {
      if ((result = __source.getString(subkey)) != null) {
        return result;
      }
    }
    return result;
  }

  @Override
  public File getFile(Object key)
  {
    File result = null;
    for (String subkey : new KeyHierarchy(key)) {
      if ((result = __source.getFile(subkey)) != null) {
        return result;
      }
    }
    return result;
  }

  public static class KeyHierarchy
    implements Iterable<String>
  {
    private final String __value;

    private final List<String> __hierarchy;

    public KeyHierarchy(Object key)
    {
      String allKeys = Preconditions.checkNotNull(key, "key").toString();
      int i = allKeys.lastIndexOf('.');
      if (i != -1) {
        __value = allKeys.substring(i + 1);
        __hierarchy = new ArrayList<String>();
        __hierarchy.add(allKeys);
        while ((i = allKeys.lastIndexOf('.', i - 1)) != -1) {
          __hierarchy.add(allKeys.substring(0, i + 1) + __value);
        }
        __hierarchy.add(__value);
      } else {
        __value = allKeys;
        __hierarchy = null;
      }
    }

    @Override
    public Iterator<String> iterator()
    {
      if (__hierarchy == null) {
        return Iterators.singletonIterator(__value);
      } else {
        return __hierarchy.iterator();
      }
    }
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    Collection<?> values = getValues(key);
    return values == null ? ImmutableList.of() : values;
  }
}
