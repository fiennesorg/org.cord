package org.cord.util;

/**
 * Interface that describes an object that validates strings in some way to ensure that they confirm
 * to a certain standard.
 */
public interface StringValidator
  extends Named
{
  /**
   * Check that the string confirms to the criteria of the StringValidator. This enables the
   * StringValidator to reject the String with an argument as to why the String was accepted, but
   * incurs the overhead of having to construct a StringValidatorException to encapsulate this
   * information.
   * 
   * @param string
   *          The string to check.
   * @return The original string if it is approved.
   * @throws StringValidatorException
   *           If the string is not approved.
   * @see #isValid(String)
   */
  public String validate(String string)
      throws StringValidatorException;

  /**
   * Check to see whether a given String would be valid if submitted to validate(String). This has
   * the advantage that if you are not concerned why a String is not valid, and your want to test a
   * large number of Strings which will have a high proportion of invalid values in them that the
   * overhead of generating StringValidatorExceptions incurred by validate(String) is avoided.
   * 
   * @param string
   *          The string to check
   * @return true if the String is valid, false if it is invalid.
   * @see #validate(String)
   */
  public boolean isValid(String string);
}
