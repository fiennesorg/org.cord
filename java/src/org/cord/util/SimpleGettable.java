package org.cord.util;

/**
 * SimpleGettable is Gettable without all the typed methods and only the core get method.
 * 
 * @author alex
 */
public interface SimpleGettable
{
  /**
   * Retrieve an Object of some type which is referenced by another Object as a key.
   * 
   * @return The value Object or null if the key doesn't map onto anything in this case.
   */
  public Object get(Object key);

  /**
   * Does this contain the given key? This will test against the Object key via equals() and so you
   * should pass in a key that is the same as the key that you used to put things into the
   * SimpleGettable.
   * 
   * @return true if it does contain the key, false if not
   */
  public boolean containsKey(Object key);

}
