package org.cord.util;

import java.util.concurrent.ConcurrentHashMap;

import com.google.common.base.Preconditions;

/**
 * Cache that contains namespaced names ie hash keys that are prefixed with a constant namespace
 * name. There are generally not that many of them and currently we are building a new one each time
 * that we want to look up its value in a Gettable which is very inefficient - much better to store
 * the result in the cache.
 * 
 * @author alex
 * @see org.cord.util.NamespacedNameCacheManager
 */
public class NamespacedNameCache
{
  public enum Ordering {
    PREFIXNAMESPACE,
    POSTFIXNAMESPACE
  }

  private final ConcurrentHashMap<Object, String> __cache = Maps.newConcurrentHashMap();

  private final String __namespace;

  private final Ordering __ordering;

  /**
   * @param namespace
   *          The string that is automatically prefixed to all keys when they are stored in the
   *          cache.
   */
  public NamespacedNameCache(String namespace,
                             Ordering ordering)
  {
    __namespace = Assertions.notEmpty(namespace, "NamespacedNameCache.namespace");
    __ordering = Preconditions.checkNotNull(ordering, "NamespacedNameCache.ordering");
  }

  /**
   * Get the namespace that this cache is using to build its compound names.
   * 
   * @return String of length greater than 1
   */
  public final String getNamespace()
  {
    return __namespace;
  }

  public String getNamespacedName(Object name)
  {
    String namespacedName = __cache.get(name);
    if (namespacedName == null) {
      switch (__ordering) {
        case PREFIXNAMESPACE:
          namespacedName = __namespace + name;
          break;
        case POSTFIXNAMESPACE:
          namespacedName = name + __namespace;
          break;
      }
      __cache.put(name, namespacedName);
    }
    return namespacedName;
  }

  @Override
  public String toString()
  {
    return "NamespacedNameCache(" + __namespace + ")";
  }
}
