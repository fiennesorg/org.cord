package org.cord.util;

/**
 * LogicException is a RuntimeException that wraps around another generic exception, thereby letting
 * you throw freak out conditions without losing the stack trace of the internal problems...
 */
public class LogicException
  extends RuntimeException
{
  /**
   * 
   */
  private static final long serialVersionUID = 6500439385780855203L;

  /**
   * Create a new logic exception that wraps no exception. This is sort of useful because it lets
   * you catch LogicException around your software and you will catch this whereas generic runtimes
   * (rather than carefully crafted LogicExceptions) will slip past.
   * 
   * @param details
   *          Information about why the exception is being thrown.
   */
  public LogicException(String details)
  {
    this(details,
         null);
  }

  /**
   * Create a new logic exception wrapped around another exception.
   * 
   * @param cause
   *          The (possibly non Runtime) Exception that is to be wrapped in the logic exception.
   * @param details
   *          Information about why the exception is being thrown.
   */
  public LogicException(String details,
                        Throwable cause)
  {
    super(details,
          cause);
  }

  public LogicException(Throwable cause)
  {
    super(cause);
  }
}
