package org.cord.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.io.Closeables;
import com.google.common.io.Closer;
import com.google.common.io.Files;
import com.google.common.io.Resources;

/**
 * Static Utility methods that provide supporting functionality to the java.io package.
 */
public final class IoUtil
{
  /**
   * Create a unique temporary directory within a named parent directory. This directory will have a
   * name consisting of a fixed base filename followed by the current system time in milliseconds.
   * This means that the chances of choosing a filename that already exists is minimised. This is
   * further enhanced by getting the system to increment this time stamp by 1 if the directory
   * already exists and repeating this process a definable number of times until a free slot is
   * found.
   * 
   * @param path
   *          The parent directory in which the temp directory is to be created.
   * @param baseFilename
   *          The string that is going to start the filename of the temp directory
   * @param attempts
   *          The maximum number of times that the system should attempt to find a free slot before
   *          giving up.
   * @throws IllegalArgumentException
   *           if <code>path</code> is unsuitable. This will happen if:-
   *           <ul>
   *           <li>it is null
   *           <li>it referes to a file that is not a directory
   *           <li>it referes to a directory that the java process does not have write permissions
   *           on.
   *           </ul>
   * @throws LogicException
   *           If the directory cannot be created within <code>attempts</code> tries.
   */
  public final static File getTempDir(File path,
                                      String baseFilename,
                                      int attempts)
  {
    if (path == null || !path.isDirectory() || !path.canWrite()) {
      throw new IllegalArgumentException("Invalid temp path:" + path);
    }
    long tempCode = System.currentTimeMillis();
    for (int i = 0; i < attempts; i++) {
      String directoryName =
          baseFilename == null ? Long.toString(tempCode) : baseFilename + tempCode;
      File directory = new File(path, directoryName);
      if (directory.exists()) {
        tempCode++;
      } else {
        directory.mkdir();
        return directory;
      }
    }
    throw new LogicException("Failed to find empty directory base:" + tempCode);
  }

  /**
   * Attempt to delete a file and all its contents if it is a directory. This should be used with
   * <i>caution</i> as it will agressively delete whole trees of information given half a chance.
   * 
   * @param file
   *          The source file to delete. If this is a directory then delete all it's contents as
   *          well.
   * @return The number of files deleted if the operation was sucessful, or -1 if one of the files
   *         in the process was not able to be deleted. Please note that failure doesn't signify
   *         that the file system has not been touched, but rather that the operation failed at some
   *         point in the process.
   * @see java.io.File#delete() File.delete()
   */
  public final static int recursiveDelete(File file)
  {
    if (file == null || !file.exists()) {
      return 0;
    }
    if (!file.canWrite()) {
      return -1;
    }
    if (file.isDirectory()) {
      File[] fileList = file.listFiles();
      int totalFileCount = 0;
      for (int i = 0; i < fileList.length; i++) {
        int fileCount = recursiveDelete(fileList[i]);
        if (fileCount == -1) {
          return -1;
        } else {
          totalFileCount += fileCount;
        }
      }
      return (file.delete() ? totalFileCount + 1 : -1);
    } else {
      return (file.delete() ? 1 : -1);
    }
  }

  /**
   * @param commandArray
   *          The commands to be invoked.
   * @param trackSuccessfulOutput
   *          true if you are interested in the output of the Process.
   * @return If trackSuccessfulOutput was true, then the return value will be the contents of the
   *         standard out of the invoked Process. If false, then the output will be null.
   * @throws IOException
   *           If the process failed in some way (ie non zero return type) then the IOException will
   *           hold the output of the error stream for the Process.
   */
  public final static String exec(String[] commandArray,
                                  File workingDirectory,
                                  int successfulExitValue,
                                  boolean trackSuccessfulOutput)
      throws IOException
  {
    Runtime runtime = Runtime.getRuntime();
    String[] envp = {};
    Process process = null;
    try {
      process = runtime.exec(commandArray, envp, workingDirectory);
      try {
        int exitValue = process.waitFor();
        if (exitValue != successfulExitValue) {
          throw new IOException(getContent(process.getErrorStream()));
        } else {
          if (trackSuccessfulOutput) {
            return getContent(process.getInputStream());
          } else {
            return null;
          }
        }
      } catch (InterruptedException intEx) {
        throw new IOException("Unable to complete exec:" + intEx);
      }
    } finally {
      if (process != null) {
        process.getInputStream().close();
        process.getOutputStream().close();
        process.getErrorStream().close();
      }
    }
  }

  /**
   * @param command
   *          The commands to be invoked.
   * @param trackSuccessfulOutput
   *          true if you are interested in the output of the Process.
   * @return If trackSuccessfulOutput was true, then the return value will be the contents of the
   *         standard out of the invoked Process. If false, then the output will be null.
   * @throws IOException
   *           If the process failed in some way (ie non zero return type) then the IOException will
   *           hold the output of the error stream for the Process.
   */
  public final static String exec(String command,
                                  File workingDirectory,
                                  int successfulExitValue,
                                  boolean trackSuccessfulOutput)
      throws IOException
  {
    Runtime runtime = Runtime.getRuntime();
    String[] envp = {};
    Process process = null;
    try {
      process = runtime.exec(command, envp, workingDirectory);
      try {
        int exitValue = process.waitFor();
        if (exitValue != successfulExitValue) {
          throw new IOException(getContent(process.getErrorStream()));
        } else {
          if (trackSuccessfulOutput) {
            return getContent(process.getInputStream());
          } else {
            return null;
          }
        }
      } catch (InterruptedException intEx) {
        throw new IOException("Unable to complete exec:" + intEx);
      }
    } finally {
      if (process != null) {
        process.getInputStream().close();
        process.getOutputStream().close();
        process.getErrorStream().close();
      }
    }
  }

  public final static int exec(String[] commandArray,
                               File workingDirectory,
                               PrintStream outStream,
                               PrintStream errStream)
      throws IOException
  {
    Runtime runtime = Runtime.getRuntime();
    String[] envp = {};
    Process process = runtime.exec(commandArray, envp, workingDirectory);
    return exec(process,
                ThreadedStreamConsumer.getInstance(new BufferedReader(new InputStreamReader(process.getInputStream())),
                                                   outStream),
                ThreadedStreamConsumer.getInstance(new BufferedReader(new InputStreamReader(process.getErrorStream())),
                                                   errStream));
  }

  public final static int exec(String[] commandArray,
                               File workingDirectory,
                               StringBuilder outBuffer,
                               StringBuilder errBuffer)
      throws IOException
  {
    Runtime runtime = Runtime.getRuntime();
    String[] envp = {};
    Process process = runtime.exec(commandArray, envp, workingDirectory);
    return exec(process,
                ThreadedStreamConsumer.getInstance(new BufferedReader(new InputStreamReader(process.getInputStream())),
                                                   outBuffer),
                ThreadedStreamConsumer.getInstance(new BufferedReader(new InputStreamReader(process.getErrorStream())),
                                                   errBuffer));
  }

  private final static int exec(Process process,
                                ThreadedStreamConsumer out,
                                ThreadedStreamConsumer err)
      throws IOException
  {
    out.start();
    err.start();
    try {
      return process.waitFor();
    } catch (InterruptedException intEx) {
      IOException ioEx = new IOException("Interrupted while waiting for thread completion");
      ioEx.initCause(intEx);
      throw ioEx;
    } finally {
      try {
        out.join(10000);
        err.join(10000);
      } catch (InterruptedException ioEx) {
        // don't think that this is relevant...
      }
      process.getInputStream().close();
      process.getOutputStream().close();
      process.getErrorStream().close();
    }
  }

  /**
   * Execute the given commandArray with the default workingDirectory, a successful value of 0 and
   * no tracking of the output of the process. This is generally a safe alternative for "normal"
   * executions which you have faith in their ability to complete.
   * 
   * @see #exec(String[],File,int,boolean)
   */
  public final static void exec(String[] commandArray)
      throws IOException
  {
    exec(commandArray, null, 0, false);
  }

  /**
   * Fast non-portable move command that spawns a seperate process to invoke a Unix "mv" command to
   * complete the operation. This is often much more efficient than copying the file by hand,
   * especially when the filesize is large, or if the file is a directory and you wish to move and
   * the directory and the contents.
   * 
   * @param sourceFile
   *          The file which is to be moved.
   * @param targetFile
   *          The location to which the file should be moved to.
   * @param canOverwriteTarget
   *          If true then the operation will not bother checking if the targetFile exists. If
   *          false, then an already existing targetFile wlil throw an IOException.
   * @throws IOException
   *           If:-
   *           <ul>
   *           <li>sourceFile is null
   *           <li>sourceFile cannot be read
   *           <li>targetFile is null
   *           <li>targetFile exists and canOverwriteTarget is false
   *           <li>The mv process returns an error code, the message of the IOException will contain
   *           any output on the error stream of the sub-process.
   *           </ul>
   */
  public final static void unixMv(File sourceFile,
                                  File targetFile,
                                  boolean canOverwriteTarget)
      throws IOException
  {
    if (sourceFile == null || !sourceFile.canRead()) {
      throw new IOException("unixMv: bad source file: " + sourceFile);
    }
    if (targetFile == null || (targetFile.exists() && !canOverwriteTarget)) {
      throw new IOException("unixMv: bad target file: " + targetFile);
    }
    String[] commandArray = { "mv", sourceFile.toString(), targetFile.toString() };
    exec(commandArray);
  }

  /**
   * Execute the given commandArray with errors generating an IOException that includes debugging as
   * to the contents of the commandArray that generated the error.
   */
  public static void execWithVerboseErrors(String[] commandArray)
      throws IOException
  {
    StringBuilder buffer = new StringBuilder();
    try {
      IoUtil.exec(commandArray, null, buffer, buffer);
    } catch (IOException ioEx) {
      IOException wrapperEx = new IOException("Caught error while running: "
                                              + Arrays.toString(commandArray) + ":-\n" + buffer);
      wrapperEx.initCause(ioEx);
      throw wrapperEx;
    }
  }

  public final static void unixCp(File sourceFile,
                                  File targetFile,
                                  boolean canOverwriteTarget)
      throws IOException
  {
    if (sourceFile == null || !sourceFile.canRead()) {
      throw new IOException("unixCp: bad source file: " + sourceFile);
    }
    if (targetFile == null || (targetFile.exists() && !canOverwriteTarget)) {
      throw new IOException("unixCp: bad target file: " + targetFile);
    }
    String[] commandArray = { "cp", "-R", sourceFile.toString(), targetFile.toString() };
    exec(commandArray);
  }

  public final static void cp(File sourceFile,
                              File targetFile,
                              boolean canOverwriteTarget)
      throws IOException
  {
    if (sourceFile == null || !sourceFile.canRead()) {
      throw new IOException("cp: bad source file: " + sourceFile);
    }
    if (targetFile == null || (targetFile.exists() && !canOverwriteTarget)) {
      throw new IOException("cp: bad target file: " + targetFile);
    }
    Closer closer = Closer.create();
    try {
      RandomAccessFile source = closer.register(new RandomAccessFile(sourceFile, "r"));
      FileChannel sourceChannel = closer.register(source.getChannel());
      RandomAccessFile target = closer.register(new RandomAccessFile(targetFile, "rw"));
      FileChannel targetChannel = closer.register(target.getChannel());
      sourceChannel.transferTo(0, source.length(), targetChannel);
    } finally {
      closer.close();
    }
  }

  public static void copy(URL source,
                          File destination)
      throws IOException
  {
    Resources.asByteSource(source).copyTo(Files.asByteSink(destination));
  }

  public static boolean deleteRecursive(File path)
  {
    if (!path.exists())
      throw new IllegalArgumentException(path.getAbsolutePath());
    boolean ret = true;
    if (path.isDirectory()) {
      for (File f : path.listFiles()) {
        ret = ret && deleteRecursive(f);
      }
    }
    return ret && path.delete();
  }

  /**
   * Non-portable command to create a symbolic link under unix-like systems.
   */
  public final static void unixLn(File sourceFile,
                                  File targetLink,
                                  boolean canOverwriteTarget,
                                  boolean utiliseRecursiveDelete)
      throws IOException
  {
    if (sourceFile == null || !sourceFile.canRead()) {
      throw new IOException("unixLn: bad source file: " + sourceFile);
    }
    if (targetLink == null || (targetLink.exists() && !canOverwriteTarget)) {
      throw new IOException("unixLn: bad target file: " + targetLink);
    }
    if (targetLink.exists()) {
      if (canOverwriteTarget) {
        if (utiliseRecursiveDelete) {
          if (recursiveDelete(targetLink) == -1) {
            throw new IOException("unixLn: error deleting: " + targetLink);
          }
        } else {
          if (!targetLink.delete()) {
            throw new IOException("unixLn: error deleting: " + targetLink);
          }
        }
      } else {
        throw new IOException("unixLn: target exists and canOverwriteTarget is false: "
                              + targetLink);
      }
    }
    String[] commandArray = { "ln", "-s", sourceFile.toString(), targetLink.toString() };
    exec(commandArray);
  }

  /**
   * Read the total content of an InputStream into a StringBuilder a line a time seperated by
   * linebreaks with the option of a definable character encoding.
   * 
   * @param inputStream
   *          The compulsory inputStream that is to be read
   * @param characterEncoding
   *          The optional character encoding that is to be utilised when reading the inputStream.
   *          If this is null then the default character encoding for the platform is utilised.
   */
  public final static String getContent(InputStream inputStream,
                                        String characterEncoding)
      throws IOException
  {
    Preconditions.checkNotNull(inputStream, "inputStream");
    InputStreamReader inputStreamReader;
    if (characterEncoding != null) {
      inputStreamReader = new InputStreamReader(inputStream, characterEncoding);
    } else {
      inputStreamReader = new InputStreamReader(inputStream);
    }
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    StringBuilder content = new StringBuilder();
    String nextLine = null;
    while ((nextLine = bufferedReader.readLine()) != null) {
      content.append(nextLine);
      content.append('\n');
    }
    return content.toString();
  }

  public final static String getContentThenClose(InputStream inputStream,
                                                 String characterEncoding)
      throws IOException
  {
    boolean threw = true;
    try {
      String content = getContent(inputStream, characterEncoding);
      threw = false;
      return content;
    } finally {
      Closeables.close(inputStream, threw);
    }
  }

  /**
   * Get the contents of the inputStream as a String using the default character encoding for the
   * platform.
   * 
   * @param inputStream
   *          The compulsory inputStream that is to be read.
   * @see #getContent(InputStream, String)
   */
  public final static String getContent(InputStream inputStream)
      throws IOException
  {
    return getContent(inputStream, null);
  }

  public static String getContent(File file)
      throws IOException
  {
    StringBuilder result = new StringBuilder();
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      String line = null;
      while ((line = br.readLine()) != null) {
        result.append(line).append("\n");
      }
    } finally {
      Closeables.close(br, false);
    }
    return result.toString();
  }

  /**
   * Write the given content out to the given file and then close the output stream.
   */
  public final static void writeContent(File file,
                                        String content)
      throws IOException
  {
    FileWriter fileWriter = null;
    try {
      fileWriter = new FileWriter(file);
      fileWriter.write(content);
    } finally {
      if (fileWriter != null) {
        fileWriter.close();
      }
    }
  }

  /**
   * Open a connection to the given URL and download the contents as a String
   * 
   * @param url
   *          The URL to resolve. Not null.
   * @param defaultContentEncoding
   *          the character encoding to use for the response if the server doesn't specify one. If
   *          this isn't defined and is required then UTF-8 will be utilised.
   * @return The contents of the response from the server.
   */
  public static String getContent(URL url,
                                  String defaultContentEncoding)
      throws IOException
  {
    Preconditions.checkNotNull(url, "url");
    try {
      URLConnection conn = url.openConnection();
      if (conn instanceof HttpURLConnection) {
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        if (httpConn.getResponseCode() >= 400) {
          throw new IOException("ResponseCode of " + httpConn.getResponseCode() + " from " + url);
        }
      }
      String encoding = conn.getContentEncoding();
      if (encoding == null) {
        encoding = StringUtils.padEmpty(defaultContentEncoding, "UTF-8");
      }
      StringWriter writer = new StringWriter();
      BufferedReader bufferedReader = null;
      boolean threw = true;
      try {
        bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), encoding));
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
          writer.write(line);
          writer.append('\n');
        }
        threw = false;
      } finally {
        Closeables.close(bufferedReader, threw);
      }
      return writer.toString();
    } catch (IOException ioEx) {
      throw new IOException(String.format("Error while resolving %s", url), ioEx);
    }
  }

  /**
   * Read the given InputStream until no data is available and return the contents as a single byte
   * array.
   */
  public static byte[] getBytes(InputStream inputStream)
      throws IOException
  {
    ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
    byte[] buf = new byte[1024];
    int size = -1;
    while ((size = inputStream.read(buf)) != -1) {
      out.write(buf, 0, size);
    }
    return out.toByteArray();
  }

  public static String submitPostRequest(URL url,
                                         Map<String, String> key_values)
      throws IOException
  {
    StringBuilder buf = new StringBuilder();
    for (Iterator<Map.Entry<String, String>> i = key_values.entrySet().iterator(); i.hasNext();) {
      Map.Entry<String, String> entry = i.next();
      buf.append(URLEncoder.encode(entry.getKey(), "UTF-8"))
         .append("=")
         .append(URLEncoder.encode(entry.getValue(), "UTF-8"));
      if (i.hasNext()) {
        buf.append("&");
      }
    }
    return submitPostRequest(url, buf.toString());
  }

  public static String submitPostRequest(URL url,
                                         String postContents)
      throws IOException
  {
    URLConnection urlConnection = url.openConnection();
    HttpURLConnection httpUrlConnection = (HttpURLConnection) urlConnection;
    httpUrlConnection.setRequestMethod("POST");
    httpUrlConnection.setDoOutput(true);
    byte[] postContentsBytes = postContents.getBytes(StandardCharsets.UTF_8);
    httpUrlConnection.setFixedLengthStreamingMode(postContentsBytes.length);
    httpUrlConnection.setRequestProperty("Content-Type",
                                         "application/x-www-form-urlencoded; charset=UTF-8");
    httpUrlConnection.connect();
    try (OutputStream os = httpUrlConnection.getOutputStream()) {
      os.write(postContentsBytes);
    }
    InputStream in = null;
    try {
      in = httpUrlConnection.getInputStream();
      return getContent(in, httpUrlConnection.getContentEncoding());
    } catch (IOException ioEx) {
      InputStream err = null;
      try {
        err = httpUrlConnection.getErrorStream();
        throw new IOException(String.format("%s: %s: %s: %s",
                                            url,
                                            Integer.valueOf(httpUrlConnection.getResponseCode()),
                                            httpUrlConnection.getResponseMessage(),
                                            err == null
                                                ? "No Error Response"
                                                : getContent(err,
                                                             httpUrlConnection.getContentEncoding())),
                              ioEx);
      } finally {
        Closeables.close(err, false);
      }
    } finally {
      Closeables.close(in, false);
    }
  }

  public static String submitPostRequest(String targetUrl,
                                         String postContents)
      throws IOException
  {
    URL url = new URL(targetUrl);
    return submitPostRequest(url, postContents);
  }

  /**
   * Take a URL, connect to the remote server and if the response is a redirect then return the
   * location that you have been redirected to, otherwise just return the original location.
   */
  public static String resolveRedirect(String url)
      throws IOException
  {
    URL u = new URL(url);
    URLConnection c = u.openConnection();
    HttpURLConnection h = (HttpURLConnection) c;
    switch (h.getResponseCode()) {
      case HttpURLConnection.HTTP_MOVED_TEMP:
      case HttpURLConnection.HTTP_MOVED_PERM:
        return h.getHeaderField("Location");
    }
    return url;
  }

  public static Thread threadedConsumeInputStream(InputStream inputStream,
                                                  PrintStream out)
  {
    ThreadedInputStreamConsumer consumer = new ThreadedInputStreamConsumer(inputStream, out);
    Thread thread = new Thread(consumer);
    thread.start();
    return thread;
  }

  static class ThreadedInputStreamConsumer
    implements Runnable
  {
    private final InputStream __inputStream;
    private final PrintStream __out;

    private ThreadedInputStreamConsumer(InputStream inputStream,
                                        PrintStream out)
    {
      __inputStream = Preconditions.checkNotNull(inputStream, "inputStream");
      __out = out;
    }

    @Override
    public void run()
    {
      if (__out == null) {
        try {
          while (__inputStream.read() != -1) {
          }
          __inputStream.close();
        } catch (IOException ioEx) {
          ioEx.printStackTrace();
        }
      } else {
        Reader r = new InputStreamReader(__inputStream);
        int i = -1;
        try {
          while ((i = r.read()) != -1) {
            __out.print((char) i);
          }
          r.close();
        } catch (IOException ioEx) {
          ioEx.printStackTrace(__out);
        }
      }

    }
  }

  /**
   * <b>Deprecated.</b> This method suffers from poor symlink detection and race conditions. This
   * functionality can be supported suitably only by shelling out to an operating system command
   * such as {@code rm -rf} or {@code del /s}. This method is scheduled to be removed from Guava in
   * Guava release 11.0.
   * <p>
   * Deletes all the files within a directory. Does not delete the directory itself.
   * <p>
   * If the file argument is a symbolic link or there is a symbolic link in the path leading to the
   * directory, this method will do nothing. Symbolic links within the directory are not followed.
   * 
   * @param directory
   *          the directory to delete the contents of
   * @throws IllegalArgumentException
   *           if the argument is not a directory
   * @throws IOException
   *           if an I/O error occurs
   */
  public static void deleteDirectoryContents(File directory)
      throws IOException
  {
    Preconditions.checkArgument(directory.isDirectory(), "Not a directory: %s", directory);
    // Symbolic links will have different canonical and absolute paths
    if (!directory.getCanonicalPath().equals(directory.getAbsolutePath())) {
      DebugConstants.DEBUG_OUT.println("symbolic link - ignoring");
      DebugConstants.variable("canonical", directory.getCanonicalPath());
      DebugConstants.variable("absolutePath", directory.getAbsolutePath());
      return;
    }
    File[] files = directory.listFiles();
    if (files == null) {
      throw new IOException("Error listing files for " + directory);
    }
    for (File file : files) {
      deleteRecursively(file);
    }
  }

  /**
   * <b>Deprecated.</b> This method suffers from poor symlink detection and race conditions. This
   * functionality can be supported suitably only by shelling out to an operating system command
   * such as {@code rm -rf} or {@code del /s}. This method is scheduled to be removed from Guava in
   * Guava release 11.0.
   * <p>
   * Deletes a file or directory and all contents recursively.
   * <p>
   * If the file argument is a symbolic link the link will be deleted but not the target of the
   * link. If the argument is a directory, symbolic links within the directory will not be followed.
   * 
   * @param file
   *          the file to delete
   * @throws IOException
   *           if an I/O error occurs
   */
  public static void deleteRecursively(File file)
      throws IOException
  {
    DebugConstants.method("IoUtil", "deleteRecursively", file);
    if (file.isDirectory()) {
      deleteDirectoryContents(file);
    }
    if (!file.delete()) {
      throw new IOException("Failed to delete " + file);
    }
  }

  /**
   * Calculate the relative component of a child File's path with respect to a parent File.
   */
  public static String getRelativePath(File parent,
                                       File child)
  {
    Preconditions.checkNotNull(parent, "parent");
    Preconditions.checkNotNull(child, "child");
    String p = parent.toString();
    String c = child.toString();
    Preconditions.checkState(c.startsWith(p), "%s is not a child of %s", child, parent);
    return c.substring(p.length());
  }
}
