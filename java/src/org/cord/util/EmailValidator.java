package org.cord.util;

import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Singleton StringValidator that utilises the InternetAddress class to check that the String
 * supplied is a valid email address (in structure at least).
 */
public class EmailValidator
  extends NamedImpl
  implements StringValidator
{
  private final static EmailValidator __rfcInstance = new EmailValidator(false);

  private final static EmailValidator __dottedInstance = new EmailValidator(true);

  public final static EmailValidator getRfcInstance()
  {
    return __rfcInstance;
  }

  public final static EmailValidator getDottedInstance()
  {
    return __dottedInstance;
  }

  private final static Pattern __pattern =
      Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$");

  public static boolean isValidEmail(String email)
  {
    return __pattern.matcher(email).find();
  }

  public final static String NAME = "EmailValidator";

  private final boolean __checkForDottedDomains;

  private EmailValidator(boolean checkForDottedDomains)
  {
    super(NAME);
    __checkForDottedDomains = checkForDottedDomains;
  }

  public String getError(String address)
  {
    if (address == null) {
      return "null is not a valid email address";
    }
    try {
      InternetAddress ia = new InternetAddress(address, true);
      if (__checkForDottedDomains) {
        if (!hasDottedDomain(ia)) {
          return address + " does not have a dotted domain";
        }
      }
    } catch (AddressException aEx) {
      return aEx.getMessage();
    }
    return null;
  }

  public static boolean hasDottedDomain(InternetAddress internetAddress)
  {
    String email = internetAddress.getAddress();
    int atIndex = email.indexOf('@');
    if (atIndex == -1) {
      throw new LogicException("Cannot find @ in " + internetAddress);
    }
    return email.indexOf('.', atIndex) != -1;
  }

  @Override
  public boolean isValid(String address)
  {
    return getError(address) == null;
  }

  @Override
  public String validate(String address)
      throws StringValidatorException
  {
    String error = getError(address);
    if (error == null) {
      return address;
    } else {
      throw new StringValidatorException(error, null);
    }
  }
}
