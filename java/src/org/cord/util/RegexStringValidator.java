package org.cord.util;

import java.util.regex.Pattern;

/**
 * StringValidator that will only pass Strings that match a given regular expression.
 */
public class RegexStringValidator
  extends NamedImpl
  implements StringValidator
{
  private final Pattern __pattern;

  private final String __errorMessage;

  public RegexStringValidator(String name,
                              String regularExpression,
                              String errorMessage)
  {
    super(name);
    __pattern = Pattern.compile(regularExpression);
    __errorMessage = errorMessage;
  }

  @Override
  public boolean isValid(String string)
  {
    return __pattern.matcher(string).matches();
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return string;
    }
    throw new StringValidatorException(__errorMessage, string);
  }
}