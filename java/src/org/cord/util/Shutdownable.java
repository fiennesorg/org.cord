package org.cord.util;

/**
 * Interface that describes items that are capable of being shutdown to terminate their operation.
 */
public interface Shutdownable
{
  public void shutdown();
}
