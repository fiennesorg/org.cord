package org.cord.util;

/**
 * Exception that is thrown when you invoke a comp method with a TypedKey and the resulting
 * operation generates a null value.
 * 
 * @author alex
 */
public class UndefinedCompException
  extends RuntimeException
{
  private static final long serialVersionUID = -8232620497416630086L;

  private final Object __target;
  private final TypedKey<?> __key;
  private final Object __value;
  private final Object[] __params;

  public UndefinedCompException(String message,
                                Object target,
                                TypedKey<?> key,
                                Object value,
                                Throwable cause,
                                Object... params)
  {
    super(String.format("%s.comp(%s) resolves %s which returned null", target, key, value),
          cause);
    __target = target;
    __key = key;
    __value = value;
    __params = params;
  }

  public UndefinedCompException(Object target,
                                TypedKey<?> key,
                                Object value,
                                Throwable cause,
                                Object... params)
  {
    this(String.format("%s.comp(%s) resolves %s which returned null", target, key, value),
         target,
         key,
         value,
         cause,
         params);
  }

  public Object[] getParams()
  {
    return __params;
  }

  public Object getValue()
  {
    return __value;
  }

  public Object getTarget()
  {
    return __target;
  }

  public TypedKey<?> getKey()
  {
    return __key;
  }
}
