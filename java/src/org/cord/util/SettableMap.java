package org.cord.util;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ImmutableList;

/**
 * Very rough implementation of Settable that works by extending HashMap.
 */
public class SettableMap
  extends HashMap<String, Object>
  implements Settable
{
  @Override
  public boolean containsKey(Object key)
  {
    return super.containsKey(key.toString());
  }

  @Override
  public Object get(Object key)
  {
    return super.get(GettableUtils.toKey(key));
  }

  public Object get(String key)
  {
    return super.get(key);
  }

  public static SettableMap newIfNull(SettableMap params)
  {
    return params != null ? params : new SettableMap();
  }

  /**
   * 
   */
  private static final long serialVersionUID = -6028983081844411539L;

  @Override
  public Object set(String key,
                    Object value)
      throws SettableException
  {
    return put(GettableUtils.toKey(key).toString(), value);
  }

  /**
   * Resolve the key into its String equivalent and then put the value into the underlying Map.
   */
  public Object put(TypedKey<?> key,
                    Object value)
  {
    return super.put(key.getKeyName(), value);
  }

  /**
   * put a value in the SettableMap if there is not already a value associated with key. Note that
   * having key set to null counts as having a value and therefore in this case this operation would
   * do nothing.
   * 
   * @return true if value was associated with key, false if there already was a value for key.
   */
  public Object putIfAbsent(String key,
                            Object value)
  {
    if (containsKey(key)) {
      return get(key);
    }
    put(key, value);
    return null;
  }

  public Object putIfAbsent(TypedKey<?> key,
                            Object value)
  {
    return putIfAbsent(key.getKeyName(), value);
  }

  /**
   * if value is not null then put it into the Map
   * 
   * @return true if the value was added to the map, false if value was null
   */
  public boolean putIfDefined(String key,
                              Object value)
  {
    if (value == null) {
      return false;
    }
    put(key, value);
    return true;
  }

  public boolean putIfDefined(TypedKey<?> key,
                              Object value)
  {
    return putIfDefined(key.getKeyName(), value);
  }

  /**
   * If value is null and key is not already in map then add value to map.
   * 
   * @return true if the value was added to the map, false if value was null or key was already
   *         defined.
   */
  public boolean putIfAbsentAndDefined(String key,
                                       Object value)
  {
    if (value == null) {
      return false;
    }
    return putIfAbsent(key, value) == null;
  }

  public boolean putIfAbsentAndDefined(TypedKey<?> key,
                                       Object value)
  {
    return putIfAbsentAndDefined(key.getKeyName(), value);
  }

  public SettableMap add(String key,
                         Object value)
  {
    put(key, value);
    return this;
  }

  public SettableMap add(TypedKey<?> key,
                         Object value)
  {
    return add(key.getKeyName(), value);
  }

  /**
   * @return the keys that are currently set in the hashmap
   * @see HashMap#keySet()
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    return keySet();
  }

  @Override
  public Collection<String> getSettableKeys()
  {
    return Collections.emptyList();
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    return GettableUtils.wrapValue(get(key));
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    Collection<?> values = getValues(key);
    return values == null ? ImmutableList.of() : values;
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    return GettableUtils.toBoolean(get(key));
  }

  @Override
  public String getString(Object key)
  {
    return StringUtils.toString(get(key));
  }

  @Override
  public File getFile(Object key)
  {
    return GettableUtils.toFile(get(key));
  }

  @Override
  public ValueType getValueType(String key)
  {
    return null;
  }

  public String toVerboseString()
  {
    StringBuilder buf = new StringBuilder();
    buf.append("{");
    for (Map.Entry<String, Object> entry : entrySet()) {
      buf.append(entry.getKey()).append("(").append(entry.getKey().getClass()).append(")=");
      buf.append(entry.getValue()).append("(").append(entry.getValue().getClass()).append("), ");
    }
    buf.append("}");
    return buf.toString();
  }

  @Override
  public Object set(TypedKey<?> key,
                    Object value)
      throws SettableException
  {
    return set(key.getKeyName(), value);
  }
}
