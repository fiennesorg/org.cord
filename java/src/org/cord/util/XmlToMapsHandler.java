package org.cord.util;

import java.util.Map;

public interface XmlToMapsHandler
{
  /**
   * Perform any work that is necessary on a populated Map as produced by an XmlToMaps object that
   * this has been registered on.
   * 
   * @param source
   *          The XmlToMaps item that has generated the map
   * @param map
   *          key/value String mapping of one data item.
   */
  public void handle(XmlToMaps source,
                     Map<String, String> map)
      throws Exception;
}
