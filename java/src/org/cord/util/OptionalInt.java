package org.cord.util;

public abstract class OptionalInt
{
  public static OptionalInt fromNullable(Integer value)
  {
    return value == null ? absent() : of(value.intValue());
  }

  public static OptionalInt of(int value)
  {
    return new Present(value);
  }

  private final static OptionalInt UNDEFINED_INSTANCE = new Absent();

  public static OptionalInt absent()
  {
    return UNDEFINED_INSTANCE;
  }

  public abstract boolean isPresent();

  public abstract int get();

  public int or(int notPresentValue)
  {
    return isPresent() ? get() : notPresentValue;
  }

  static class Present
    extends OptionalInt
  {
    private final int __value;

    private Present(int value)
    {
      __value = value;
    }

    @Override
    public boolean isPresent()
    {
      return true;
    }

    @Override
    public int get()
    {
      return __value;
    }
  }

  static class Absent
    extends OptionalInt
  {
    @Override
    public boolean isPresent()
    {
      return false;
    }

    @Override
    public int get()
    {
      throw new IllegalStateException("OptionalInt is not present");
    }

  }
}
