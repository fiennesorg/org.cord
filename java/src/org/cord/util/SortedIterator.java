package org.cord.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.common.collect.ImmutableSet;

/**
 * Iterator wrapper that drops the contents of the source Iterator into a TreeSet before
 * re-iterating across it, thereby transparently sorting the data according to the specified
 * Comparator.
 */
public class SortedIterator<T>
  implements Iterator<T>
{
  private final Iterator<T> __sortedIterator;

  public SortedIterator(Iterator<T> sourceIterator)
  {
    this(sourceIterator,
         null);
  }

  /**
   * @param sourceIterator
   *          The original Iterator that is to be sorted.
   * @param comparator
   *          The Comparator that is to provide the ordering that is to be applied to the
   *          sourceIterator.
   */
  public SortedIterator(Iterator<T> sourceIterator,
                        Comparator<? super T> comparator)
  {
    if (sourceIterator.hasNext()) {
      SortedSet<T> sortedSet = new TreeSet<T>(comparator);
      while (sourceIterator.hasNext()) {
        sortedSet.add(sourceIterator.next());
      }
      __sortedIterator = sortedSet.iterator();
    } else {
      __sortedIterator = ImmutableSet.<T> of().iterator();
    }
  }

  @Override
  public boolean hasNext()
  {
    return __sortedIterator.hasNext();
  }

  @Override
  public T next()
  {
    return __sortedIterator.next();
  }

  /**
   * @throws UnsupportedOperationException
   *           Always!
   */
  @Override
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}
