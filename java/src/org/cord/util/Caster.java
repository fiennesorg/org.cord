package org.cord.util;

public interface Caster<T>
{
  public T cast(Object obj)
      throws ClassCastException;
}
