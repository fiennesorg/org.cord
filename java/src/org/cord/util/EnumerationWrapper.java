package org.cord.util;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Wrapper class that provides an Iterator interface around an Enumeration.
 */
public class EnumerationWrapper<T>
  implements Iterator<T>
{
  private Enumeration<T> _enumeration;

  public EnumerationWrapper(Enumeration<T> enumeration)
  {
    _enumeration = enumeration;
  }

  @Override
  public boolean hasNext()
  {
    return _enumeration.hasMoreElements();
  }

  @Override
  public T next()
  {
    return _enumeration.nextElement();
  }

  @Override
  public void remove()
  {
    throw new UnsupportedOperationException("EnumerationWrapper does not support remove()");
  }
}
