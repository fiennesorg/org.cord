package org.cord.util;

import java.math.BigDecimal;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.field.SterlingField;
import org.cord.node.FeedbackErrorException;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/**
 * Utility class of static methods for doing funky things to numbers...
 */
public final class NumberUtil
{
  public final static long GRANULARITY_SECOND = 1000;

  public final static long GRANULARITY_MINUTE = 1000 * 60;

  public final static long GRANULARITY_HOUR = 1000 * 60 * 60;

  public final static long GRANULARITY_DAY = 1000 * 60 * 60 * 24;

  public final static long GRANULARITY_WEEK = 1000 * 60 * 60 * 24 * 7;

  public final static Integer INTEGER_ZERO = Integer.valueOf(0);

  public final static Integer INTEGER_ONE = Integer.valueOf(1);

  public final static Integer INTEGER_MINUSONE = Integer.valueOf(-1);

  public static final Double DOUBLE_ZERO = Double.valueOf(0.0d);

  public static final Long LONG_ZERO = Long.valueOf(0l);

  public static final BigDecimal BIGDECIMAL_ZERO = new BigDecimal("0");

  public static boolean isZero(BigDecimal value)
  {
    return BIGDECIMAL_ZERO.compareTo(value) == 0;
  }

  public final static long round(long number,
                                 long granularity)
  {
    if (granularity <= 0) {
      return number;
    }
    return (number / granularity) * granularity;
  }

  public final static String toString(long number,
                                      int minimumLength)
  {
    StringBuilder result = new StringBuilder();
    result.append(number);
    while (result.length() < minimumLength) {
      result.insert(0, '0');
    }
    return result.toString();
  }

  public final static int TOSTRING_INT_COUNT = 100;
  private final static String[] TOSTRING_INT = new String[TOSTRING_INT_COUNT];
  static {
    for (int i = 0; i < TOSTRING_INT_COUNT; i++) {
      TOSTRING_INT[i] = Integer.toString(i);
    }
  }

  /**
   * Convert an int to a String representation in base 10 with optimisation for numbers between 0
   * and {@link #TOSTRING_INT_COUNT}. Numbers that fall outside this range delegate to
   * {@link Integer#toString(int)} so if you know that you are likely to have a large number then it
   * is more efficient to invoke this directly.
   */
  public final static String toString(int number)
  {
    if (number < 0 | number >= TOSTRING_INT_COUNT) {
      return Integer.toString(number);
    }
    return TOSTRING_INT[number];
  }

  /**
   * Constrain a given int variable between an inclusive upper and lower limit.
   */
  public final static int constrain(int number,
                                    int min,
                                    int max)
  {
    return Math.min(Math.max(number, min), max);
  }

  public final static double constrain(double number,
                                       double min,
                                       double max)
  {
    return Math.min(Math.max(number, min), max);
  }

  /**
   * Get a number representing the sign of an integer.
   * 
   * @param number
   *          the integer whose sign is due to be checked.
   * @return 1 if the sign of number is positive, -1 if the sign of number is negative and 0 if the
   *         number is equal to 0.
   */
  public final static int sign(int number)
  {
    if (number > 0) {
      return 1;
    }
    if (number < 0) {
      return -1;
    }
    return 0;
  }

  public final static int compare(long n1,
                                  long n2)
  {
    if (n1 < n2) {
      return -1;
    }
    if (n1 > n2) {
      return 1;
    }
    return 0;
  }

  /**
   * Ensure that an int has the correct number of digits by prepending the appropriate number of 0s
   * to the start of the default string representation of the int.
   */
  public final static String toString(int i,
                                      int digitCount)
  {
    digitCount = Math.max(0, digitCount);
    if (digitCount == 0) {
      return Integer.toString(i);
    }
    StringBuilder result = new StringBuilder();
    result.append(i);
    while (result.length() < digitCount) {
      result.insert(0, '0');
    }
    return result.toString();
  }

  // public final static String toString(double d,
  // int decimalPlaces)
  // {
  // StringBuilder result = new StringBuilder();
  // result.append(d);
  // }
  public final static int toInt(Object object,
                                boolean mapNullToInt,
                                int nullValue,
                                boolean mapBadFormatToInt,
                                int badFormatValue)
      throws NumberFormatException
  {
    if (object == null) {
      if (mapNullToInt) {
        return nullValue;
      } else {
        throw new IllegalArgumentException("Cannot process null with mapNullToInt set to false");
      }
    }
    if (object instanceof Number) {
      return ((Number) object).intValue();
    }
    try {
      return Integer.parseInt(object.toString());
    } catch (NumberFormatException nfEx) {
      if (mapBadFormatToInt) {
        return badFormatValue;
      } else {
        throw nfEx;
      }
    }
  }

  /**
   * Convert an Object into a BigDecimal
   * 
   * @param nullValue
   *          The value that is returned if object is null
   * @param numberFormatValue
   *          The value that is returned if the object is parsed as a String and the operation fails
   */
  public final static BigDecimal toBigDecimal(Object object,
                                              BigDecimal nullValue,
                                              BigDecimal numberFormatValue)
  {
    if (object == null) {
      return nullValue;
    }
    if (object instanceof BigDecimal) {
      return (BigDecimal) object;
    }
    if (object instanceof Number) {
      return new BigDecimal(((Number) object).doubleValue());
    }
    try {
      return new BigDecimal(object.toString());
    } catch (NumberFormatException nfEx) {
      return numberFormatValue;
    }
  }

  public final static BigDecimal setScale(BigDecimal bd,
                                          int scale)
  {
    if (bd == null) {
      return null;
    }
    return bd.setScale(scale);
  }

  /**
   * Convert an object to a BigDecimal or return null if not possible
   */
  public final static BigDecimal toBigDecimal(Object object)
  {
    return toBigDecimal(object, null, null);
  }

  /**
   * Resolve an Object into an int with null and invalid formats throwing NullPointer and
   * NumberFormatException
   */
  public final static int toInt(Object object)
      throws NullPointerException, NumberFormatException
  {
    return toInt(object, false, 0, false, 0);
  }

  public final static int toInt(Object object,
                                int nullValue,
                                int badFormatValue)
  {
    return toInt(object, true, nullValue, true, badFormatValue);
  }

  public final static int toInt(Object object,
                                int defaultValue)
  {
    return toInt(object, defaultValue, defaultValue);
  }

  /**
   * Convert an Object into an Integer with all failure paths throwing the error message defined by
   * the caller
   * 
   * @return The appropriate Integer, never null
   * @throws NullPointerException
   *           if obj is null
   * @throws NumberFormatException
   *           if obj.toString() cannot be parsed
   */
  public final static Integer toInteger(final Object obj,
                                        String errorMessage,
                                        Object... msgParams)
  {
    Preconditions.checkNotNull(obj, errorMessage, msgParams);
    if (obj instanceof Integer) {
      return (Integer) obj;
    }
    if (obj instanceof Number) {
      return Integer.valueOf(((Number) obj).intValue());
    }
    try {
      return Integer.valueOf(obj.toString());
    } catch (NumberFormatException nfEx) {
      NumberFormatException wrapper =
          new NumberFormatException(String.format(errorMessage, msgParams));
      wrapper.initCause(nfEx);
      throw nfEx;
    }
  }

  public final static Integer toInteger(final Object object,
                                        final boolean mapNullToInt,
                                        final Integer nullValue,
                                        final boolean mapBadFormatToInt,
                                        final Integer badFormatValue)
      throws NullPointerException, NumberFormatException
  {
    if (object == null) {
      if (mapNullToInt) {
        return nullValue;
      }
      throw new IllegalArgumentException("Cannot process null with mapNullToInt set to false");
    }
    if (object instanceof Integer) {
      return (Integer) object;
    }
    if (object instanceof Number) {
      return Integer.valueOf(((Number) object).intValue());
    }
    final String string = object.toString();
    if (string.length() == 0) {
      if (mapBadFormatToInt) {
        return badFormatValue;
      }
      throw new NumberFormatException("Cannot parse \"\" as an Integer");
    }
    try {
      return string.indexOf('.') == -1
          ? Integer.valueOf(string)
          : Integer.valueOf((int) Double.parseDouble(string));
    } catch (NumberFormatException nfEx) {
      if (mapBadFormatToInt) {
        return badFormatValue;
      }
      throw nfEx;
    }
  }

  public final static Double toDouble(final Object object,
                                      final boolean mapNullToInt,
                                      final Double nullValue,
                                      final boolean mapBadFormatToInt,
                                      final Double badFormatValue)
      throws NullPointerException, NumberFormatException
  {
    if (object == null) {
      if (mapNullToInt) {
        return nullValue;
      }
      throw new IllegalArgumentException("Cannot process null with mapNullToInt set to false");
    }
    if (object instanceof Double) {
      return (Double) object;
    }
    if (object instanceof Number) {
      return Double.valueOf(((Number) object).doubleValue());
    }
    final String string = object.toString();
    if (string.length() == 0) {
      if (mapBadFormatToInt) {
        return badFormatValue;
      }
      throw new NumberFormatException("Cannot parse \"\" as an Integer");
    }
    try {
      return Double.valueOf(string);
    } catch (NumberFormatException nfEx) {
      if (mapBadFormatToInt) {
        return badFormatValue;
      }
      throw nfEx;
    }
  }

  public static Long toLongObj(Object obj)
      throws NullPointerException, NumberFormatException
  {
    if (obj == null) {
      throw new NullPointerException();
    }
    if (obj instanceof Long) {
      return (Long) obj;
    }
    if (obj instanceof Number) {
      return Long.valueOf(((Number) obj).longValue());
    }
    return Long.valueOf(obj.toString());
  }

  /**
   * Convert an Object into a Long with a given fallback if it is not possible to do so.
   */
  public static Long toLongObj(Object obj,
                               Long fallback)
  {
    if (obj != null) {
      if (obj instanceof Long) {
        return (Long) obj;
      }
      if (obj instanceof Number) {
        return Long.valueOf(((Number) obj).longValue());
      }
      String s = obj.toString();
      if (s.length() > 0) {
        try {
          return Long.valueOf(s);
        } catch (NumberFormatException e) {
        }
      }
    }
    return fallback;
  }

  /**
   * Convert an Object into a long with all failures throwing RuntimeExceptions.
   * 
   * @throws NullPointerException
   *           if obj is null
   * @throws NumberFormatException
   *           if we have had to convert obj into a String and we can't parse the result
   */
  public static long toLong(Object obj)
      throws NullPointerException, NumberFormatException
  {
    Preconditions.checkNotNull(obj);
    if (obj instanceof Number) {
      return ((Number) obj).longValue();
    }
    String s = obj.toString();
    return Long.parseLong(s);
  }

  public static long toLong(Object obj,
                            long fallback)
  {
    if (obj == null) {
      return fallback;
    }
    if (obj instanceof Number) {
      return ((Number) obj).longValue();
    }
    String s = obj.toString();
    try {
      return Long.parseLong(s);
    } catch (NumberFormatException e) {
      return fallback;
    }
  }

  /**
   * Parse an Object into an Integer, with all errors throwing exceptions.
   * 
   * @return The required Integer, never null
   */
  public final static Integer toInteger(Object object)
      throws NullPointerException, NumberFormatException
  {
    return toInteger(object, false, null, false, null);
  }

  /**
   * A Function that transforms Objects into Integer with all inputs that cannot be converted
   * throwing a RuntimeException.
   */
  public final static Function<Object, Integer> OBJECT_TO_INTEGER =
      new Function<Object, Integer>() {
        @Override
        public Integer apply(Object input)
        {
          return toInteger(input);
        }
      };

  /**
   * Try and resolve the specified object into an Integer with definable values to be used for
   * either null objects or unparseable objects.
   */
  public final static Integer toInteger(Object object,
                                        Integer nullValue,
                                        Integer badFormatValue)
  {
    return toInteger(object, true, nullValue, true, badFormatValue);
  }

  public final static Double toDouble(Object object,
                                      Double nullValue,
                                      Double badFormatValue)
  {
    return toDouble(object, true, nullValue, true, badFormatValue);
  }

  /**
   * Try and resolve the specified object into an Integer, returning the defaultValue for all values
   * that cannot be parsed.
   */
  public final static Integer toInteger(Object object,
                                        Integer defaultValue)
  {
    return toInteger(object, defaultValue, defaultValue);
  }

  public final static Optional<Integer> toOptInteger(Object object)
  {
    return Optional.fromNullable(toInteger(object, null));
  }

  public final static Double toDouble(Object object,
                                      Double defaultValue)
  {
    return toDouble(object, defaultValue, defaultValue);
  }

  public final static Double toDouble(Object object)
  {
    return toDouble(object, false, null, false, null);
  }

  public final static boolean booleanValue(Boolean flag,
                                           boolean nullValue)
  {
    return flag == null ? nullValue : flag.booleanValue();
  }

  public final static Float toFloat(Object object,
                                    Float nullValue,
                                    Float badFormatValue)
  {
    if (object == null) {
      return nullValue;
    }
    if (object instanceof Float) {
      return (Float) object;
    }
    if (object instanceof Number) {
      return new Float(((Number) object).floatValue());
    }
    try {
      return Float.valueOf(object.toString());
    } catch (NumberFormatException nfEx) {
      return badFormatValue;
    }
  }

  /**
   * Convert an Object into an Integer with client friendly {@link FeedbackErrorException} errors if
   * this is not possible.
   * 
   * @param error
   *          The error message if the conversion is not possible. This will be passed through
   *          {@link String#format(String, Object...)} with obj as the first parameter so can be
   *          parametric if required.
   * @return The parsed Integer - never null.
   * @throws FeedbackErrorException
   *           If it is not possible to convert obj into an Integer. This will not include any
   *           nested exceptions if any.§
   */
  public static Integer toInteger(Object obj,
                                  String title,
                                  PersistentRecord node,
                                  String error)
      throws FeedbackErrorException
  {
    Integer i = toInteger(obj, null);
    if (i == null) {
      throw new FeedbackErrorException(title, node, error, obj);
    }
    return i;
  }

  /**
   * Add 2 optional Integers together
   * 
   * @return The result which may be null if both integer1 and integer2 are null
   */
  public static Integer add(Integer integer1,
                            Integer integer2)
  {
    if (integer1 == null || integer1.intValue() == 0) {
      return integer2;
    }
    if (integer2 == null || integer2.intValue() == 0) {
      return integer1;
    }
    return Integer.valueOf(integer1.intValue() + integer2.intValue());
  }

  /**
   * Add 2 optional Longs together
   * 
   * @return The result which may be null if both long1 and long2 are null
   */
  public static Long add(Long long1,
                         Long long2)
  {
    if (long1 == null || long1.longValue() == 0) {
      return long2;
    }
    if (long2 == null || long2.longValue() == 0) {
      return long1;
    }
    return Long.valueOf(long1.longValue() + long2.longValue());
  }

  @Deprecated
  public static String asSterling(Object obj)
  {
    Integer i = SterlingField.parseFormattedOptSterling(obj);
    return i == null ? "" : SterlingField.toSterlingString(i.intValue());
  }

  private static NumberUtil INSTANCE = new NumberUtil();

  public static NumberUtil getInstance()
  {
    return INSTANCE;
  }

  public static double nanosToSeconds(long nanos)
  {
    return nanos / 1000000000d;
  }

  public static double nanosToMs(long nanos)
  {
    return nanos / 1000000d;
  }

  public static double msToSeconds(long ms)
  {
    return ms / 1000d;
  }

  private NumberUtil()
  {
  }
}
