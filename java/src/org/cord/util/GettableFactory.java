package org.cord.util;

/**
 * Interface that describes Objects that have an optional hotswappable Gettable associated with
 * them. Due to the intended hotswappable nature of the Gettable objects, clients should not cache
 * these Gettable references, but should instead store the GettableFactory hook and then retrieve
 * the Gettable before asking for a single parameter. If the client requires a block of parameters
 * that are all interconnected, then it is sufficient to cache the Gettable for the duration of the
 * retrieval of this block of parameters thereby preventing the possibility of splitting a block of
 * parameters across two Gettable sources in the unlikely event that the Gettable is swapped in the
 * middle of the information retrieval.
 */
public interface GettableFactory
{
  /**
   * Get the Gettable that is currently associated with this GettableFactory.
   * 
   * @return The appropriate Gettable or null if there is not currently a Gettable associated with
   *         this GettableFactory.
   */
  public Gettable getGettable();

  /**
   * Singleton implementation of GettableFactory that always returns Gettable.emptyGettable()
   * 
   * @see GettableUtils#emptyGettable()
   */
  public static GettableFactory EMPTYGETTABLEFACTORY = new GettableFactory() {
    @Override
    public Gettable getGettable()
    {
      return GettableUtils.emptyGettable();
    }
  };
}
