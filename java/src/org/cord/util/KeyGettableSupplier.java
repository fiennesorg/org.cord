package org.cord.util;

/**
 * A KeyGettableSupplier describes an Object that is capable of retrieving a T from the combination
 * of a Gettable and the key that was used to resolve this Gettable. This is useful for
 * {@link GettableUtils#getValues(Gettable, String, String, KeyGettableSupplier)} when the T wants
 * to use the key that the item was encoded under in the Gettable.
 */
public interface KeyGettableSupplier<T>
{
  public T get(String key,
               Gettable gettable);
}
