package org.cord.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.cord.node.config.NodeConfig;

import com.google.common.base.MoreObjects;

public class RegexFilter
  implements Filter
{
  private final Pattern __pattern = StringUtils.AMPERSANDS_NOT_CHARENTITIES;
  private final Pattern __comments = Pattern.compile("<!--.*?-->");

  public static final String INITPARAM_CONTENTTYPES = "RegexFilter.contentTypes";
  private final List<String> __contentTypes = new ArrayList<String>();

  public static final String INITPARAM_STRIPCOMMENTS = "RegexFilter.stripComments";
  private boolean _stripComments = false;

  @Override
  public void init(FilterConfig config)
      throws ServletException
  {
    NodeConfig nodeConfig =
        new NodeConfig(config, DebugConstants.DEBUG_RESOURCES ? DebugConstants.DEBUG_OUT : null);
    Gettable params = nodeConfig.getGettable(NodeConfig.Namespace.NODE);
    _stripComments =
        MoreObjects.firstNonNull(params.getBoolean(INITPARAM_STRIPCOMMENTS), Boolean.FALSE)
                   .booleanValue();
    __contentTypes.addAll(Arrays.asList(MoreObjects.firstNonNull(params.getString(INITPARAM_CONTENTTYPES),
                                                                 "text/html,application/rss+xml")
                                                   .split(",")));
  }

  @Override
  public void doFilter(ServletRequest request,
                       ServletResponse result,
                       FilterChain filterChain)
      throws IOException, ServletException
  {
    String contentType = result.getContentType();
    if (contentType != null) {
      for (String eligibleType : __contentTypes) {
        if (contentType.startsWith(eligibleType)) {
          CachingHttpServletResponseWrapper responseWrapper =
              new CachingHttpServletResponseWrapper((HttpServletResponse) result);
          filterChain.doFilter(request, responseWrapper);
          String output = responseWrapper.getOutput();
          output = __pattern.matcher(output).replaceAll("&amp;");
          if (_stripComments) {
            output = __comments.matcher(output).replaceAll("");
          }
          result.getWriter().print(output);
          result.flushBuffer();
          return;
        }
      }
    }
    filterChain.doFilter(request, result);
  }

  @Override
  public void destroy()
  {
  }

  public class CachingHttpServletResponseWrapper
    extends HttpServletResponseWrapper
  {
    private final ByteArrayOutputStream __baos;

    public CachingHttpServletResponseWrapper(HttpServletResponse response)
    {
      super(response);
      __baos = new ByteArrayOutputStream();
    }

    @Override
    public void setContentLength(int arg0)
    {
    }

    public String getOutput()
        throws UnsupportedEncodingException
    {
      try {
        __baos.flush();
      } catch (IOException e) {
      }
      return __baos.toString(getCharacterEncoding());
    }

    @Override
    public ServletOutputStream getOutputStream()
        throws IOException
    {
      return new ServletOutputStream() {
        @Override
        public void write(int arg0)
            throws IOException
        {
          __baos.write(arg0);
        }

        @Override
        public void write(byte[] b,
                          int off,
                          int len)
            throws IOException
        {
          __baos.write(b, off, len);
        }

        @Override
        public void write(byte[] b)
            throws IOException
        {
          __baos.write(b);
        }
      };
    }
  }
}
