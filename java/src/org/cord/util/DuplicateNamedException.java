package org.cord.util;

public class DuplicateNamedException
  extends NamedException
{
  /**
   * 
   */
  private static final long serialVersionUID = -8540700700178293173L;

  private final String _name;

  public DuplicateNamedException(String name,
                                 String details)
  {
    super(null,
          name + ":" + details);
    _name = name;
  }

  public final String getName()
  {
    return _name;
  }
}
