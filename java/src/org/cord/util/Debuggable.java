package org.cord.util;

/**
 * Interface to describe Objects that are prepared to offer up slightly more informative information
 * than the normal <code>.toString()</code> method.
 */
public interface Debuggable
{
  /**
   * Get the name of the instance of the Debuggable Object.
   * 
   * @return the instance name of the Object.
   */
  public String getName();

  /**
   * Return an informative String that es the internal state of the Object. The String may be
   * multi-lined.
   * 
   * @return Debugging String.
   */
  public String debug();
}
