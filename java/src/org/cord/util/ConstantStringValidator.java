package org.cord.util;

import com.google.common.base.Strings;

/**
 * Utility implementation of StringValidator that provides a variety of default validation methods.
 * This is an ugly pattern and it should be refactored...
 */
public class ConstantStringValidator
  extends NamedImpl
  implements StringValidator
{
  public final static int FILTER_ANY = 0;

  public final static int FILTER_NONE = 1;

  public final static int FILTER_ALPHANUMERIC = 2;

  public final static int FILTER_NONZEROLENGTH = 3;

  private final static String[] NAMES = { "All", "None", "Alphanumeric", "NonZeroLength" };

  private final static String[] REASONS = { "All strings accepted", "No strings accepted",
      "Alphanumeric characters only", "All string with length greater than 0" };

  private final static StringValidator[] __stringValidators;
  static {
    __stringValidators = new StringValidator[NAMES.length];
    for (int i = 0; i < NAMES.length; i++) {
      __stringValidators[i] = new ConstantStringValidator(i);
    }
  }

  public final static StringValidator getInstance(int style)
  {
    return __stringValidators[style];
  }

  /**
   * Utility method to check to see if a string is entirely composed of letters and digits. This may
   * be useful for people who wish to restrict the naming policy applied to Named objects.
   * 
   * @param source
   *          The string to check.
   * @return true if <i>all</i> the characters that make up the String return true to
   *         Character.isLetterOrDigit(char).
   */
  public final static boolean isLettersOrDigits(String source)
  {
    int sourceLength = source.length();
    for (int i = 0; i < sourceLength; i++) {
      if (!Character.isLetterOrDigit(source.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  private final int __style;

  private final String __reason;

  private ConstantStringValidator(int style)
  {
    super(NAMES[style]);
    __style = style;
    __reason = REASONS[style];
  }

  public final String getReason()
  {
    return __reason;
  }

  @Override
  public boolean isValid(String string)
  {
    switch (__style) {
      case FILTER_ANY:
        return true;
      case FILTER_NONE:
        return false;
      case FILTER_ALPHANUMERIC:
        return isLettersOrDigits(string);
      case FILTER_NONZEROLENGTH:
        return !Strings.isNullOrEmpty(string);
    }
    return false;
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return string;
    }
    throw new StringValidatorException(__reason, string);
  }
}
