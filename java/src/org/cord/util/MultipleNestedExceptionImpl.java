package org.cord.util;

import java.util.ArrayList;
import java.util.Iterator;

public class MultipleNestedExceptionImpl
  extends Exception
  implements MultipleNestedException
{
  /**
   * 
   */
  private static final long serialVersionUID = 7263461791812890476L;

  private final ArrayList<Exception> __nestedExceptions = new ArrayList<Exception>();

  public MultipleNestedExceptionImpl(String message)
  {
    super(message);
  }

  public void addException(Exception exception)
  {
    if (exception != null) {
      __nestedExceptions.add(exception);
    }
  }

  /**
   * @see #addException(Exception)
   */
  @Override
  public Iterator<Exception> getNestedExceptions()
  {
    return __nestedExceptions.iterator();
  }

  @Override
  public void printStackTrace()
  {
    super.printStackTrace();
    for (int i = 0; i < __nestedExceptions.size(); i++) {
      System.err.println("NestedException: " + i + "/" + __nestedExceptions.size());
      __nestedExceptions.get(i).printStackTrace();
    }
  }
}
