package org.cord.util;

import java.util.Comparator;

public class NamedComparator
  implements Comparator<Named>
{
  private final static NamedComparator __instance = new NamedComparator();

  public static NamedComparator getInstance()
  {
    return __instance;
  }

  private NamedComparator()
  {
  }

  @Override
  public int compare(Named n1,
                     Named n2)
  {
    return n1.getName().compareTo(n2.getName());
  }
}
