package org.cord.util;

public interface Timestamped
{
  public long getTimestamp();

  public void setTimestamp();
}
