package org.cord.util;

public class EnglishNamedPropertyImpl
  extends NamedPropertyImpl
  implements EnglishNamed
{
  private final String __englishName;

  public EnglishNamedPropertyImpl(String name,
                                  boolean exposePublicKeys,
                                  String englishName)
  {
    super(name,
          exposePublicKeys);
    __englishName = englishName;
  }

  @Override
  public final String getEnglishName()
  {
    return __englishName;
  }
}
