package org.cord.util;

import java.io.File;

import org.cord.mirror.TransientRecord;

import com.google.common.base.Strings;

public class FileValueType
  extends ValueType
{
  public static String TYPE = "File";

  private final String __defaultFilePath;

  public FileValueType(String key,
                       String englishKey,
                       String summary,
                       String defaultFilePath,
                       boolean isCompulsory)
  {
    super(TYPE,
          key,
          englishKey,
          summary,
          isCompulsory);
    __defaultFilePath = defaultFilePath;
  }

  @Override
  public boolean isValid(Object value)
  {
    String filePath = StringUtils.toString(value);
    if (!Strings.isNullOrEmpty(filePath)) {
      File file = new File(filePath);
      return file.canRead();
    }
    return false;
  }

  @Override
  public Object getDefaultValue(TransientRecord record)
  {
    return __defaultFilePath;
  }
}
