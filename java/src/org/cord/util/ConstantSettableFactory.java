package org.cord.util;

import com.google.common.base.Preconditions;

/**
 * Implementation of SettableFactory that always returns the same Settable Object as declared in the
 * initialiser.
 */
public class ConstantSettableFactory
  implements SettableFactory
{
  private final Settable __settable;

  public ConstantSettableFactory(Settable settable)
  {
    __settable = Preconditions.checkNotNull(settable, "settable");
  }

  @Override
  public Settable getSettable()
  {
    return __settable;
  }

  @Override
  public Gettable getGettable()
  {
    return __settable;
  }
}