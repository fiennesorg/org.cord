package org.cord.util;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

public class BooleanValueType
  extends ValueType
{
  public static final String TYPE = "Boolean";

  private final FieldValueSupplier<? extends Boolean> __defaultValue;

  public BooleanValueType(String key,
                          String englishKey,
                          String summary,
                          boolean isCompulsory,
                          FieldValueSupplier<? extends Boolean> defaultValue)
  {
    super(TYPE,
          key,
          englishKey,
          summary,
          isCompulsory);
    __defaultValue = defaultValue;
  }

  @Override
  public boolean isValid(Object value)
  {
    return value instanceof Boolean;
  }

  @Override
  public Object getDefaultValue(TransientRecord record)
  {
    return __defaultValue.supplyValue(record);
  }
}
