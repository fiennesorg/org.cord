package org.cord.util;

import java.util.Calendar;
import java.util.Date;

import org.cord.util.DateQuantiser.Granularity;

import com.google.common.base.Preconditions;

/**
 * QuantisedCurrentTime produces quantised versions of the current time according to the specified
 * Granularity. The same Date Object will be utilised until the next granularity point is passed.
 * The only exception to this is during the period after 29 days in a month that has more than 29
 * days due to the unpredictability of month lengths.
 * 
 * @author alex
 */
public class QuantisedCurrentTime
{
  private final DateQuantiser __quantiser = new DateQuantiser();

  private final Granularity __granularity;

  private final Calendar __calendar = Calendar.getInstance();

  private Date _now = new Date(0);

  public QuantisedCurrentTime(Granularity granularity)
  {
    __granularity = Preconditions.checkNotNull(granularity, "granularity");
    updateNow();
  }

  private void updateNow()
  {
    long now = System.currentTimeMillis();
    if (now - _now.getTime() > __granularity.duration()) {
      _now = new Date(__quantiser.quantise(now, __granularity));
      __calendar.setTime(_now);
    }
  }

  public final Date getDate()
  {
    updateNow();
    return _now;
  }

  public final int get(int field)
  {
    updateNow();
    return __calendar.get(field);
  }
}
