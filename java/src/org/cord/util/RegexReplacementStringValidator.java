package org.cord.util;

import java.util.regex.Pattern;

import com.google.common.base.Strings;

/**
 * StringValidator that "cleans" Strings by replacing occurences of a given regular expression with
 * a constant String.
 */
public class RegexReplacementStringValidator
  extends NamedImpl
  implements StringValidator
{
  private final Pattern __pattern;

  private final String __replacementValue;

  public RegexReplacementStringValidator(String name,
                                         String regularExpression,
                                         String replacementValue)
  {
    super(name);
    __pattern = Pattern.compile(regularExpression);
    __replacementValue = Strings.nullToEmpty(replacementValue);
  }

  @Override
  public boolean isValid(String string)
  {
    return __pattern.matcher(string).matches();
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    String replacedString = __pattern.matcher(string).replaceAll(__replacementValue);
    if (replacedString.equals(string)) {
      return string;
    }
    throw new StringValidatorException(this.toString(), string, replacedString);
  }
}