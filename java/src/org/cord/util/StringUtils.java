package org.cord.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONComponents;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public final class StringUtils
{
  private static final StringUtils __instance = new StringUtils();

  public static final StringUtils getInstance()
  {
    return __instance;
  }

  public static final char DOUBLEQUOTES_OPEN = '“';

  public static final char DOUBLEQUOTES_CLOSE = '”';

  private StringUtils()
  {
  }

  /**
   * Escape characters inside the String representation of object so that it can be utilsed safely
   * inside a JSON encoded string value. Note that this does not wrap the string in double quotes.
   * If string is null then an empty String is returned.
   */
  public final static String toJson(Object object)
  {
    if (object == null) {
      return "";
    }
    String string = object.toString();
    StringBuilder buf = new StringBuilder(string.length() + 2);
    try {
      JSONComponents.escapeChars(string, buf);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  /**
   * Escape characters inside the String representation of present Optional wrappers with null or
   * absent optionalObject returning "".
   */
  public final static String toJson(Optional<?> optionalObject)
  {
    if (optionalObject == null || !optionalObject.isPresent()) {
      return "";
    }
    return toJson(optionalObject.get());
  }

  public final static String toStackTrace(Throwable throwable)
  {
    return ExceptionUtil.printStackTrace(throwable);
  }

  public final static String padAlignRight(String source,
                                           int length,
                                           char paddingCharacter)
  {
    if (source.length() >= length) {
      return source;
    }
    StringBuilder result = new StringBuilder(length);
    for (int i = 0; i < length - source.length(); i++) {
      result.append(paddingCharacter);
    }
    result.append(source);
    return result.toString();
  }

  public final static String wordWrap(String source,
                                      int width)
  {
    StringBuilder result = new StringBuilder();
    StringTokenizer words = new StringTokenizer(source);
    int cursor = 0;
    while (words.hasMoreTokens()) {
      String word = words.nextToken();
      cursor += word.length() + 1;
      if (cursor > width) {
        result.append('\n');
        cursor = 0;
      }
      result.append(word).append(' ');
    }
    return result.toString();
  }

  /**
   * @deprecated in preference of {@link Strings#nullToEmpty(String)}
   */
  @Deprecated
  public final static String padNull(String source)
  {
    return Strings.nullToEmpty(source);
  }

  /**
   * Take an unspecified Object and convert it to its default String implementation, handling null
   * objects in a non-destructive manner. This should never throw NullPointerException unless the
   * internal toString() implementation of the Object is flawed.
   * 
   * @param object
   *          The Object to convert to a String.
   * @return The result of toString() on object, or null if object is null.
   * @see Object#toString()
   */
  public final static String toString(Object object)
  {
    return toString(object, null);
  }

  /**
   * Invoke toString() on a given object unless it is null whereupon you return an alternative
   * String value.
   * 
   * @param object
   *          The optional value to convert to a String
   * @param nullValue
   *          The optional value that will be returned if object is null
   * @return The result of invoking toString() on object or null if both object and nullValue are
   *         null
   */
  public final static String toString(Object object,
                                      String nullValue)
  {
    return object == null ? nullValue : object.toString();
  }

  /**
   * Function that transforms an Object into its String representation with null inputs being passed
   * through unchanged.
   */
  public final static Function<Object, String> TOSTRING = new Function<Object, String>() {
    @Override
    public String apply(Object input)
    {
      return StringUtils.toString(input);
    }
  };

  public final static Pattern XML_TAGS = Pattern.compile("<[^>]*>");

  /**
   * Attempt to remove all XML tags from the given source and append the resulting text to the
   * results StringBuilder.
   */
  public final static void stripXmlTags(StringBuilder result,
                                        String source)
  {
    if (Strings.isNullOrEmpty(source)) {
      return;
    }
    String[] notTags = XML_TAGS.split(source);
    for (int i = 0; i < notTags.length; i++) {
      result.append(notTags[i]);
    }
  }

  public final static String stripXmlTags(String source)
  {
    StringBuilder result = new StringBuilder();
    stripXmlTags(result, source);
    return result.toString();
  }

  /**
   * Strip out empty Strings and replace them with null, all other Strings are untouched.
   * 
   * @param source
   *          The optional string to be processed.
   * @return The source if it has one ore more characters in it, otherwise null.
   */
  public final static String stripEmpty(String source)
  {
    if (source == null || source.length() == 0) {
      return null;
    }
    return source;
  }

  public final static String convertHtmlBrsToLineBreaks(String source)
  {
    source = source.replaceAll("</p><p>", "\n\n");
    source = source.replaceAll("</p>$", "");
    source = source.replaceAll("^<p>", "");
    source = source.replaceAll("<p>", "\n\n");
    source = source.replaceAll("<P>", "\n\n");
    source = source.replaceAll("<br>", "\n");
    source = source.replaceAll("<br />", "\n");
    source = source.replaceAll("<BR>", "\n");
    return source;
  }

  public final static String debugString(String source)
  {
    if (source == null) {
      return "null";
    }
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < source.length(); i++) {
      result.append(i)
            .append(":")
            .append((int) source.charAt(i))
            .append(":")
            .append(source.charAt(i))
            .append("  ");
    }
    return result.toString();
  }

  private final static Pattern __lineBreaks = Pattern.compile("(\n\r|\r\n|\n|\r)");

  public static String[] splitByLines(String source)
  {
    return __lineBreaks.split(source);
  }

  public static String stripMsChars(String source)
  {
    source = source.replace((char) 8216, '\'');
    source = source.replace((char) 8217, '\'');
    source = source.replace((char) 8220, '"');
    source = source.replace((char) 8221, '"');
    source = source.replace((char) 8211, '-');
    source = source.replaceAll(Character.toString((char) 8230), "...");
    return source;
  }

  private final static Pattern __notAsciiCharacters = Pattern.compile("[^\\p{ASCII}]");

  public static String removeAccents(String source)
  {
    try {
      String normalizedValue = Normalizer.normalize(source, Normalizer.Form.NFD);
      Matcher matcher = __notAsciiCharacters.matcher(normalizedValue);
      if (!matcher.find()) {
        return source;
      }
      String strippedValue = matcher.replaceAll("");
      return strippedValue;
    } catch (RuntimeException ex) {
      throw ExceptionUtil.initCause(new IllegalArgumentException("removeAccents(" + source
                                                                 + ") was rejected"),
                                    ex);
    }
  }

  public static String toTitleCase(String s)
  {
    char[] chars = s.trim().toCharArray();
    boolean found = false;
    for (int i = 0; i < chars.length; i++) {
      if (!found && Character.isLetter(chars[i])) {
        chars[i] = Character.toUpperCase(chars[i]);
        found = true;
      } else {
        switch (chars[i]) {
          case '-':
            found = false;
            break;
          case '\'':
          case '"':
            break;
          default:
            found = !Character.isWhitespace(chars[i]);
        }
      }
    }
    return String.valueOf(chars);
  }

  /**
   * Extract the area and district components of a postcode. In a correctly formatted code, this
   * will be the information before the central space.
   */
  public static String getPostcodeAreaDistrict(String postcode)
  {
    postcode = postcode.trim();
    postcode = postcode.replace('-', ' ');
    if (postcode.length() <= 3) {
      return "";
    }
    postcode = postcode.substring(0, postcode.length() - 3);
    postcode = postcode.trim();
    postcode = postcode.toUpperCase();
    return postcode;
  }

  /**
   * Return the value if not null and length greater than 0, failing that return defaultValue.
   * 
   * @throws IllegalStateException
   *           if defaultValue is either null or of length 0
   * @return either value or defaultValue, but always guaranteed not to be null and of length
   *         greater than 0.
   */
  public static String firstNotEmpty(String value,
                                     String defaultValue)
  {
    if (Strings.isNullOrEmpty(value)) {
      Preconditions.checkState(defaultValue != null && defaultValue.length() > 0,
                               "defaultValue == %s",
                               defaultValue);
      return defaultValue;
    }
    return value;
  }

  public static String padEmpty(String value,
                                String defaultValue)
  {
    return Strings.isNullOrEmpty(value) ? defaultValue : value;
  }

  /**
   * Check to see if a CharSequence is either null, or composed entirely of whitespace. This doesn't
   * create any new objects during the checking process so doesn't have any garbage collection
   * overheads.
   */
  public static boolean isWhitespace(CharSequence charSequence)
  {
    if (charSequence == null) {
      return true;
    }
    for (int i = 0; i < charSequence.length(); i++) {
      if (charSequence.charAt(i) > ' ') {
        return false;
      }
    }
    return true;
  }

  /**
   * Check to see that the given String has at least one defined character in it.
   * 
   * @deprecated in preference of ! @Link {@link Strings#isNullOrEmpty(String)}
   */
  public static boolean notEmpty(String string)
  {
    return !isNullOrWhitespace(string);
  }

  public static boolean isNullOrWhitespace(String string)
  {
    if (string == null) {
      return true;
    }
    final int l = string.length();
    switch (l) {
      case 0:
        return true;
      case 1:
        return string.charAt(0) <= ' ';
      default:
        for (int i = 0; i < l; i++) {
          if (string.charAt(i) > ' ') {
            return false;
          }
        }
    }
    return true;
  }

  public static int length(CharSequence string)
  {
    return string == null ? 0 : string.length();
  }

  // &(?!(\\w+|#\\d+);) = regex to match all & that aren't &quot; or &#123;
  // note that this doesn't validate the escapes, but it is better than nothing
  public static final Pattern AMPERSANDS_NOT_CHARENTITIES = Pattern.compile("&(?!(\\w+|#\\d+);)");

  public static final Pattern OPENTAG = Pattern.compile("<");

  public static final Pattern CLOSETAG = Pattern.compile(">");

  /**
   * Remove all ampersands symbols that are not part of character references and convert open and
   * close tags into their character references. Passing a null reference in will return ""
   */
  public static String noHtml(Object obj)
  {
    if (obj == null) {
      return "";
    }
    String source = obj.toString();
    int firstHtmlCharIndex = getFirstHtmlCharIndex(source);
    if (firstHtmlCharIndex == -1) {
      return source;
    }
    final int l = source.length();
    StringBuilder buf = new StringBuilder(l + 6);
    buf.append(source, 0, firstHtmlCharIndex);
    boolean hasAmpersand = false;
    boolean hasChanged = false;
    for (int i = firstHtmlCharIndex; i < l; i++) {
      char c = source.charAt(i);
      switch (c) {
        case '&':
          hasAmpersand = true;
          buf.append(c);
          break;
        case '<':
          buf.append("&lt;");
          hasChanged = true;
          break;
        case '>':
          buf.append("&gt;");
          hasChanged = true;
          break;
        default:
          buf.append(c);
      }
    }
    if (hasChanged) {
      source = buf.toString();
    }
    if (hasAmpersand) {
      source = AMPERSANDS_NOT_CHARENTITIES.matcher(source).replaceAll("&amp;");
    }
    return source;
  }
  
  public static String safeHtml(Object obj)
  {
    return lineBreaksToHtml(noHtml(obj));
  }
  
  public static String safeTxt(Object obj)
  {
    return MoreObjects.firstNonNull(obj, "").toString();
  }

  private static void test(String s)
  {
    System.out.println(s + " --> " + noHtml(s));
  }

  public static void main(String[] args)
  {
    test("abc");
    test("a<b>c");
    test("<abc>");
    test("a&bc");
    test("&abc");
    test("abc&");
    test("abc&gt;");
  }

  private static int getFirstHtmlCharIndex(String source)
  {
    final int l = source.length();
    for (int i = 0; i < l; i++) {
      switch (source.charAt(i)) {
        case '&':
        case '<':
        case '>':
          return i;
      }
    }
    return -1;
  }

  public static final Pattern MULTIPLELINEBREAKS =
      Pattern.compile("(\r\n){2,}|\n{2,}", Pattern.MULTILINE);

  public static final Pattern SINGLELINEBREAK = Pattern.compile("\r\n|\n", Pattern.MULTILINE);

  /**
   * Wrap the string in a P tag and then urn all multiple line breaks into close P, open P's with
   * single line breaks becoming BRs
   * 
   * @param source
   *          The compulsory text that is to be processed.
   * @return The HTML which will always be at least one paragraph wrapped around source unless
   *         source is an empty String in which case an empty String is returned.
   * @see #MULTIPLELINEBREAKS
   * @see #SINGLELINEBREAK
   */
  public static String lineBreaksToHtml(String source)
  {
    if (source.length() == 0) {
      return "";
    }
    source = MULTIPLELINEBREAKS.matcher(source).replaceAll("</p><p>");
    source = SINGLELINEBREAK.matcher(source).replaceAll("<br />");
    return "<p>" + source + "</p>";
  }

  /**
   * Replace all newline characters with <br&nbsp;/> after trimming the string.
   * 
   * @param source
   *          The String to tidy up
   * @return null if source is null, source if source doesn't contain any newlines and the tidied
   *         string otherwise.
   */
  public static String crToBr(String source)
  {
    if (Strings.isNullOrEmpty(source)) {
      return source;
    }
    if (source.indexOf('\n') == -1) {
      return source;
    }
    return source.trim().replace("\n", "<br />");
  }

  /**
   * Pattern that matches a valid URL
   */
  public static final Pattern URL =
      Pattern.compile("(([\\w]+:)?//)?(([\\d\\w]|%[a-fA-f\\d]{2,2})+(:([\\d\\w]|%[a-fA-f\\d]{2,2})+)?@)?([\\d\\w][-\\d\\w]{0,253}[\\d\\w]\\.)+[\\w]{2,4}(:[\\d]+)?(/([-+_~.\\d\\w]|%[a-fA-f\\d]{2,2})*)*(\\?(&?([-+_~.\\d\\w]|%[a-fA-f\\d]{2,2})=?)*)?(#([-+_~.\\d\\w]|%[a-fA-f\\d]{2,2})*)?");

  /**
   * Convert all URLs in the source into valid A references. Prepend protocol-less URLs with http://
   * 
   * @see #URL
   */
  public static String urlsToHtml(String source)
  {
    Matcher matcher = URL.matcher(source);
    if (!matcher.find()) {
      return source;
    }
    matcher.reset();
    StringBuffer result = new StringBuffer(source.length() + 32);
    while (matcher.find()) {
      String url = matcher.group();
      if (url.indexOf("://") == -1) {
        matcher.appendReplacement(result, "<a href=\"http://$0\">$0</a>");
      } else {
        matcher.appendReplacement(result, "<a href=\"$0\">$0</a>");
      }
    }
    matcher.appendTail(result);
    return result.toString();
  }

  public static String toUpperCaseFirstLetter(String source)
  {
    if (source.length() == 0) {
      return source;
    }
    char firstChar = source.charAt(0);
    if (!Character.isLetter(firstChar) || Character.isUpperCase(firstChar)) {
      return source;
    }
    char upperFirstChar = Character.toUpperCase(firstChar);
    if (source.length() == 1) {
      return Character.toString(upperFirstChar);
    }
    return upperFirstChar + source.substring(1);
  }

  /**
   * Take the given String and convert the first letter in the String to lower case. If the string
   * is of zero length, or the first character is not a letter or if is already lower case then the
   * original String object will be returned untouched.
   * 
   * @param source
   *          The string to convert. null will generate a NullPointerException
   */
  public static String toLowerCaseFirstLetter(String source)
  {
    if (source.length() == 0) {
      return source;
    }
    char firstChar = source.charAt(0);
    if (!Character.isLetter(firstChar) || Character.isLowerCase(firstChar)) {
      return source;
    }
    char lowerFirstChar = Character.toLowerCase(firstChar);
    if (source.length() == 1) {
      return Character.toString(lowerFirstChar);
    }
    return lowerFirstChar + source.substring(1);
  }

  private static final Pattern __squeezeBlanksRE = Pattern.compile("\\s+");

  /**
   * Remove all trailing and leading whitespace from source, and replace all whitespace sequences
   * with a single space character.
   */
  public static String squeezeBlanks(String source)
  {
    Matcher matcher = __squeezeBlanksRE.matcher(source.trim());
    return matcher.replaceAll(" ");
  }

  public static final Pattern DOUBLEQUOTES = Pattern.compile("\"");

  /**
   * Convert a value into a representation that can be safely used inside an HTML value tag. This is
   * done by converting all " characters into &amp;quot; This will also replace all null occurrences
   * with "" so that the result can safely be used inside HTML regardless of the input. This has
   * been placed inside StringUtils rather than the previous locationof SqlUtil because it is useful
   * functionality to have when writing webmacro templates and there is an instance available as
   * $StringUtils in the context automatically.
   */
  public static String toHtmlValue(Object obj)
  {
    if (obj == null) {
      return "";
    }
    String str = obj.toString();
    if (str.indexOf('"') == -1) {
      return str;
    }
    return DOUBLEQUOTES.matcher(obj.toString()).replaceAll("&quot;");
  }

  /**
   * Convert a value into a suitable single quote wrapped attribute value by escaping nested single
   * quotes and changing carriage returns and line feeds into space characters. Null obj becomes ""
   */
  public static String toAttributeValue(Object obj)
  {
    if (obj == null) {
      return "";
    }
    String str = obj.toString();
    if (str.indexOf('\'') != -1) {
      str = str.replace("'", "\\'");
    }
    str = str.replace('\n', ' ');
    str = str.replace('\r', ' ');
    return str;
  }

  private final static Pattern WHITESPACE = Pattern.compile("\\s+");

  /**
   * Replace all elements of whitespace in the String representation of obj with a non-breaking
   * space code. Sequences of more than one whitespace character are replaced with a single
   * non-breaking space. This is useful if you want to force a single value onto a single line.
   * 
   * @param obj
   *          The optional Obj to convert. A null value will result in a return value of ""
   * @return The converted String. Never null.
   */
  public static String whitespaceToNbsp(Object obj)
  {
    if (obj == null) {
      return "";
    }
    return WHITESPACE.matcher(StringUtils.toString(obj)).replaceAll("&nbsp;");
  }

  /**
   * Append a series of String representations of Objects to a StringBuilder with a given separator
   * between successive Strings. Empty or null values are ignored.
   */
  public static void join(StringBuilder buf,
                          String separator,
                          Object... values)
  {
    boolean needsSeparator = false;
    for (Object obj : values) {
      if (obj != null) {
        String value = obj.toString();
        if (value.length() > 0) {
          if (needsSeparator) {
            buf.append(separator);
          } else {
            needsSeparator = true;
          }
          buf.append(value);
        }
      }
    }
  }

  /**
   * Append the String representations of the values in a Collection to a StringBuilder with a given
   * separator between successive Strings. Empty or null values are ignore.
   */
  public static void join(StringBuilder buf,
                          String separator,
                          Iterable<?> values)
  {
    boolean needsSeparator = false;
    for (Object obj : values) {
      if (obj != null) {
        String value = obj.toString();
        if (value.length() > 0) {
          if (needsSeparator) {
            buf.append(separator);
          } else {
            needsSeparator = true;
          }
          buf.append(value);
        }
      }
    }
  }

  /**
   * Join a series of Strings with a given separator between successive values and return the result
   * in a String
   * 
   * @see #join(StringBuilder, String, Object...)
   */
  public static String join(String separator,
                            Object... values)
  {
    if (values.length == 0) {
      return "";
    }
    StringBuilder buf = new StringBuilder();
    join(buf, separator, values);
    return buf.toString();
  }

  /**
   * Join a series of Strings with a given separator between successive values and return the result
   * in a String
   * 
   * @see #join(StringBuilder, String, Iterable)
   */
  public static String join(String separator,
                            Iterable<?> values)
  {
    if (Iterables.isEmpty(values)) {
      return "";
    }
    StringBuilder buf = new StringBuilder();
    join(buf, separator, values);
    return buf.toString();
  }

  public static String toConstantPattern(String s)
  {
    StringBuilder buf = new StringBuilder(s.length());
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      switch (c) {
        case '\\':
        case '[':
        case ']':
        case '^':
        case '$':
        case '{':
        case '}':
          buf.append('\\');
        default:
          buf.append(c);
      }
    }
    return buf.toString();
  }

  /**
   * Format the suppliedMessage, and if it is not defined then format the supplied defaultMessage
   * instead.
   * 
   * @param message
   *          The message that will be formatted with messageParams if it is defined and isn't of
   *          zero length.
   * @param fallbackMessage
   *          The message that will be formatted with fallbackParams if the message param is empty.
   * @return The appropriately formatted string. Never null.
   * @see String#format(java.lang.String, java.lang.Object[])
   */
  public static String format(String message,
                              Object[] messageParams,
                              String fallbackMessage,
                              Object... fallbackParams)
  {
    if (Strings.isNullOrEmpty(message)) {
      return String.format(fallbackMessage, fallbackParams);
    }
    return String.format(message, messageParams);
  }

  /**
   * Calculate the number of bytes that would be necessary to represent the given CharSequence in
   * UTF-8. This doesn't allocate any memory, but rather just iterates through the value counting up
   * the character types so is reasonably efficient.
   * 
   * @param source
   *          The CharSequence to evaluate. Compulsory.
   * @return The number of bytes in UTF-8 required. This will greater than or equal to the length of
   *         source
   */
  public static int lengthAsUtf8(CharSequence source)
  {
    int l = 0;
    int sl = source.length();
    for (int i = 0; i < sl; i++) {
      int c = source.charAt(i);
      if (c < 128) {
        l++;
      } else if (c < 2048) {
        l += 2;
      } else if (c < 65536) {
        l += 3;
      } else {
        l += 4;
      }
    }
    return l;
  }

  /**
   * If the given Object is defined then append it to the StringBuilder, otherwise do nothing. This
   * stops you getting "null" appearing in your StringBuilder outputs...
   * 
   * @param buf
   *          The compulsory StringBuilder to append onto.
   * @param obj
   *          The optional Object to append to the StringBuilder if it is defined.
   */
  public static void appendIfDefined(StringBuilder buf,
                                     Object obj)
  {
    Preconditions.checkNotNull(buf, "buf");
    if (obj != null) {
      buf.append(obj);
    }
  }

  /**
   * If a given string is longer than the maxLength then return an abbreviated substring of the
   * first maxLength characters with an optional postfix, otherwise return the original string.
   * 
   * @param string
   *          The compulsory string to constrain
   * @param maxLength
   *          The maximum length in characters
   * @param abbreviatedPostfix
   *          Optional String that will get appended to the abbreviated string. If it is not defined
   *          then no postfix is used. If the string is not abbreviated then no postfix is used.
   * @return A String whose length will be less than or equal to maxLength plus abbreviatedPostfix
   *         length characters. Never null.
   */
  public static String constrainLength(String string,
                                       int maxLength,
                                       String abbreviatedPostfix)
  {
    Preconditions.checkNotNull(string, "string");
    if (string.length() > maxLength) {
      String abbreviated = string.substring(0, maxLength);
      if (abbreviatedPostfix != null && abbreviatedPostfix.length() > 0) {
        abbreviated += abbreviatedPostfix;
      }
      return abbreviated;
    }
    return string;
  }

  /**
   * Trim all occurences of the specified set of characters from either end of a string. This is
   * equivalent to the PHP trim behaviour
   * 
   * @param string
   *          The compulsory string to trim
   * @param trimmedChars
   *          The compulsory string that contains all of the characters that should be removed from
   *          either end of the string
   * @return The appropriately trimmed String
   */
  public static String trim(final String string,
                            final String trimmedChars)
  {
    Preconditions.checkNotNull(string, "data");
    Preconditions.checkNotNull(trimmedChars, "trimmedChars");
    int begin = 0;
    while (trimmedChars.indexOf(string.charAt(begin)) != -1 && begin < string.length()) {
      begin++;
    }
    int end = string.length() - 1;
    while (trimmedChars.indexOf(string.charAt(end)) != -1 && end >= 0) {
      end--;
    }
    return string.substring(begin, end + 1);
  }

  /**
   * Invoke the standard String.trim() on a String if it is defined and return null if it is not
   * defined.
   * 
   * @param string
   *          The optional String to trim()
   * @return The trimmed String or null if string was null.
   */
  public static String trim(String string)
  {
    return string == null ? null : string.trim();
  }

  /**
   * Generate an md5 checksum of a given item of String data
   */
  public static String md5(String data)
  {
    MessageDigest algorithm;
    try {
      algorithm = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      throw new LogicException("MD5 not found", e);
    }
    algorithm.reset();
    algorithm.update(getBytesUtf8(data));
    return byteArray2Hex(algorithm.digest());
  }

  /**
   * Convert a byte array into a lower case hexadecimal String
   */
  public static String byteArray2Hex(byte[] hash)
  {
    StringBuilder buf = new StringBuilder(hash.length * 2);
    for (byte b : hash) {
      String hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        buf.append('0');
      }
      buf.append(hex);
    }
    return buf.toString();
  }

  /**
   * URLDecode an encoded string using the UTF-8 character encoding.
   * 
   * @throws LogicException
   *           if the UTF-8 character encoding cannot be found.
   * @see URLDecoder#decode(String, String)
   */
  public static String urlDecodeUtf8(String encodedData)
  {
    try {
      return URLDecoder.decode(encodedData, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new LogicException("Cannot resolve UTF-8 character encoding", e);
    }
  }

  /**
   * URLEncode a string using the UTF-8 character encoding.
   * 
   * @throws LogicException
   *           if the UTF-8 character encoding cannot be found
   * @see URLEncoder#encode(String, String)
   * @deprecated in preference of the appropriate UrlEscapers once we have upgraded to guava 15
   */
  @Deprecated
  public static String urlEncodeUtf8(String data)
  {
    try {
      return URLEncoder.encode(data, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new LogicException("Cannot resolve UTF-8 character encoding", e);
    }
  }

  /**
   * Extract that byte array representation of a String utilising UTF-8 encoding
   */
  public static byte[] getBytesUtf8(String data)
  {
    return data.getBytes(Charsets.UTF_8);
  }

  /**
   * Find the index of the first character with an ASCII code below 32
   * 
   * @param s
   *          The compulsory string to search
   * @return The appropriate index or -1 if either the String is empty or there are no low chars.
   * @throws NullPointerException
   *           if s is null
   */
  public static final int indexOfUnprintable(final String s)
  {
    final int l = s.length();
    for (int i = 0; i < l; i++) {
      if (s.charAt(i) < 32) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Strip out all the characters with an ASCII code below 32. If there are no characters in this
   * category then the original String is returned.
   * 
   * @param s
   *          The compulsory String to transform
   * @return The transformed String
   */
  public static final String stripUnprintable(String s)
  {
    int index = indexOfUnprintable(s);
    if (index == -1) {
      return s;
    }
    int l = s.length();
    StringBuilder buf = new StringBuilder(s.length());
    for (int i = 0; i < l; i++) {
      char c = s.charAt(i);
      if (c >= 32) {
        buf.append(c);
      }
    }
    return buf.toString();
  }

  /**
   * Append a possesive apostrophe-s to the end of an owner string, taking into account plural
   * strings. The owner will automatically be trimmed to remove leading and trailing whitespace. If
   * the owner is null or of 0 length after trimming then a RuntimException is generated.
   */
  public static final String toPossesive(String owner)
  {
    Preconditions.checkNotNull(owner, "source");
    owner = owner.trim();
    Preconditions.checkState(owner.length() > 0,
                             "Cannot generate possesive of empty trimmed String");
    if (Character.toLowerCase(owner.charAt(owner.length() - 1)) == 's') {
      return owner + "’";
    } else {
      return owner + "’s";
    }
  }

  /**
   * Take the given String and rerrange it so that it is of the form "last word, first words"
   * thereby making it sortable alphabetically and giving sensible results for western names. If
   * there are no space characters in the String then the trimmed version of the original name will
   * be returned.
   */
  public static String toSortableName(String name)
  {
    name = name.trim();
    int i = name.lastIndexOf(' ');
    if (i == -1) {
      return name;
    }
    return name.substring(i + 1).trim() + ", " + name.substring(0, i).trim();
  }

  /**
   * Create a sortable version of a string.
   * 
   * @param prefixes
   *          Iterable list of prefixes that should be stripped off the start of the string and
   *          appended to the end after a comma. These should be lower case, and should include the
   *          appropriate spaces, eg "the ", to prevent it matching words like "theatre". The
   *          transform will preserve the case of the original. If there are multiple overlapping
   *          matches then only the first match will be processed.
   * @return The appropriately processed version of original. At the very least this will have had
   *         whitespace trimmed off it. It will not have had its case changed so should be suitable
   *         for display.
   */
  public static String toSortable(String original,
                                  Iterable<String> prefixes)
  {
    original = original.trim();
    String lowerCase = original.toLowerCase();
    for (String prefix : prefixes) {
      if (lowerCase.startsWith(prefix)) {
        return (original.substring(prefix.length()) + ", "
                + original.substring(0, prefix.length())).trim();
      }
    }
    return original;
  }

  /**
   * Revert an alphabetically sorted string such as "Fiennes, Alex" to it's natural state of "Alex
   * Fiennes"
   * 
   * @param sorted
   *          compulsory string in sorted format
   */
  public static String fromSortable(String sorted)
  {
    int i = sorted.indexOf(',');
    switch (i) {
      case -1:
        return sorted;
      case 0:
        return sorted.substring(1);
      default:
        return (sorted.substring(i + 1).trim() + " " + sorted.substring(0, i)).trim();
    }
  }

  /**
   * basic set of prefixes to use with {@link #toSortable(String, Iterable)} that ignores "the, a,
   * an" at start of sortable strings.
   */
  public static ImmutableList<String> SORTABLE_PREFIXES = ImmutableList.of("the ", "a ", "an ");

  private static void appendCsvValue(StringBuilder buf,
                                     Object value)
  {
    if (value == null) {
      return;
    }
    if (value instanceof Number) {
      buf.append(value.toString());
      return;
    }
    buf.append('"').append(value.toString().replace("\"", "\"\"")).append('"');
  }

  public static String asCsvLine(Object... values)
  {
    if (values.length == 0) {
      return "\n";
    }
    StringBuilder buf = new StringBuilder();
    appendCsvValue(buf, values[0]);
    for (int i = 1; i < values.length; i++) {
      buf.append(',');
      appendCsvValue(buf, values[i]);
    }
    buf.append("\n");
    return buf.toString();
  }

  /**
   * Convert all {, }, # and $ characters that might be parsed by webmacro into their safely escaped
   * versions so the source would pass unchanged through a webmacro parser.
   */
  public static String noWebmacro(String source)
  {
    return source.replace("#", "\\#").replace("$", "\\$").replace("{", "\\{").replace("}", "\\}");
  }

  /**
   * Execute an indexOf query where source and targe characters are converted to lower case during
   * the compare.
   */
  public static int indexOfIgnoresCase(String source,
                                       int sourceOffset,
                                       int sourceCount,
                                       String target,
                                       int targetOffset,
                                       int targetCount,
                                       int fromIndex)
  {
    if (fromIndex >= sourceCount) {
      return (targetCount == 0 ? sourceCount : -1);
    }
    if (fromIndex < 0) {
      fromIndex = 0;
    }
    if (targetCount == 0) {
      return fromIndex;
    }

    char first = Character.toLowerCase(target.charAt(targetOffset));
    int max = sourceOffset + (sourceCount - targetCount);

    for (int i = sourceOffset + fromIndex; i <= max; i++) {
      /* Look for first character. */
      if (Character.toLowerCase(source.charAt(i)) != first) {
        while (++i <= max && Character.toLowerCase(source.charAt(i)) != first)
          ;
      }
      /* Found first character, now look at the rest of v2 */
      if (i <= max) {
        int j = i + 1;
        int end = j + targetCount - 1;
        for (int k = targetOffset
                     + 1; j < end
                          && Character.toLowerCase(source.charAt(j)) == Character.toLowerCase(target.charAt(k)); j++, k++)
          ;

        if (j == end) {
          /* Found whole string. */
          return i - sourceOffset;
        }
      }
    }
    return -1;
  }

  /**
   * Execute an indexOf query where source and targe characters are converted to lower case during
   * the compare.
   */
  public static int indexOfIgnoresCase(String source,
                                       String target)
  {
    return indexOfIgnoresCase(source, 0, source.length(), target, 0, target.length(), 0);
  }

  /**
   * Execute an indexOf query where source characters are converted to lower case during the compare
   * and it expects target to already consiste of lower case characters.
   */
  public static int indexOfIgnoresSourceCase(String source,
                                             int sourceOffset,
                                             int sourceCount,
                                             String target,
                                             int targetOffset,
                                             int targetCount,
                                             int fromIndex)
  {
    if (fromIndex >= sourceCount) {
      return (targetCount == 0 ? sourceCount : -1);
    }
    if (fromIndex < 0) {
      fromIndex = 0;
    }
    if (targetCount == 0) {
      return fromIndex;
    }

    char first = target.charAt(targetOffset);
    int max = sourceOffset + (sourceCount - targetCount);

    for (int i = sourceOffset + fromIndex; i <= max; i++) {
      /* Look for first character. */
      if (Character.toLowerCase(source.charAt(i)) != first) {
        while (++i <= max && Character.toLowerCase(source.charAt(i)) != first)
          ;
      }
      /* Found first character, now look at the rest of v2 */
      if (i <= max) {
        int j = i + 1;
        int end = j + targetCount - 1;
        for (int k = targetOffset
                     + 1; j < end
                          && Character.toLowerCase(source.charAt(j)) == target.charAt(k); j++, k++)
          ;

        if (j == end) {
          /* Found whole string. */
          return i - sourceOffset;
        }
      }
    }
    return -1;
  }

  /**
   * Execute an indexOf query where source characters are converted to lower case during the compare
   * and it expects target to already consiste of lower case characters.
   */
  public static int indexOfIgnoresSourceCase(String source,
                                             String target)
  {
    return indexOfIgnoresSourceCase(source, 0, source.length(), target, 0, target.length(), 0);
  }

  @Deprecated
  public static boolean isEmpty(String string)
  {
    return isNullOrWhitespace(string);
  }

  /**
   * Convert a string to camel case dropping all characters that are not numbers or digits from the
   * java perspective and append the result to the StringBuilder. Any character immediately
   * following a non number digit character will be uppercase. All other remaining characters will
   * have their case unchanged.
   * 
   * @see Character#isLetterOrDigit(char)
   */
  public static void appendLetterDigitCamelCase(StringBuilder buf,
                                                String source)
  {
    boolean isCapital = true;
    for (int i = 0; i < source.length(); i++) {
      char c = source.charAt(i);
      if (Character.isLetterOrDigit(c)) {
        if (isCapital) {
          buf.append(Character.toUpperCase(c));
          isCapital = false;
        } else {
          buf.append(c);
        }
      } else {
        isCapital = true;
      }
    }
  }

  /**
   * Convert a String to a camel case string after dropping all non number and digit characters.
   * 
   * @see #appendLetterDigitCamelCase(StringBuilder, String)
   */
  public static String toLetterDigitCamelCase(String source)
  {
    StringBuilder buf = new StringBuilder(source.length());
    appendLetterDigitCamelCase(buf, source);
    return buf.toString();
  }
}
