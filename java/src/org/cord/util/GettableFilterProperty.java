package org.cord.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.ImmutableSet;

/**
 * An Object that represents a single requirement from a GettableFilter.
 * 
 * @see GettableFilter
 */
public class GettableFilterProperty
  extends EnglishNamedImpl
{
  private final boolean __isCompulsory;

  private final String __defaultValue;

  // List of EnglishNamed
  private final List<EnglishNamed> __validValues;

  // immutable version of __validValues
  private final List<EnglishNamed> __publicValidValues;

  private final boolean __isGuiItem;

  private boolean _isLocked;

  /**
   * @param name
   *          The name of the property that is to be referenced. This is the key that will be
   *          utilised to retrieve the value from the Gettable source that it is being referenced
   *          against.
   * @param englishName
   *          The name of the property as it would be represented in a form (assuming that it is a
   *          GUI property). Note that you are still permitted to utilise the english name in
   *          non-gui situations.
   * @param isCompulsory
   *          If true then the property must be defined for isValid(basket) to be true. If false
   *          then the property will be requested (assuming that it is a gui item), but failure to
   *          supply a value will not result in a failure condition.
   * @param isGuiItem
   *          If true then the property will be added to the list of GUI items that is maintained by
   *          the GettableFilter that it is registered on and will be included in
   *          GettableFilter.getGuiItems().
   * @param supportsValidValues
   *          If true then the GettableFilterProperty is assumed to control a pull-down menu of
   *          values. This will pre-generate a space for pull-down options that can be registered
   *          via addConstantValidValue(validValue). If the sub-class is going to be dynamically
   *          generating the results of getValidValues then setting this to false will save on the
   *          creation of an extra ArrayList.
   * @param defaultValue
   *          The value that is to be used as the de facto default for this GettableFilterProperty.
   */
  public GettableFilterProperty(String name,
                                String englishName,
                                boolean isCompulsory,
                                boolean isGuiItem,
                                boolean supportsValidValues,
                                String defaultValue)
  {
    super(name,
          englishName);
    __isCompulsory = isCompulsory;
    __isGuiItem = isGuiItem;
    if (supportsValidValues) {
      __validValues = new ArrayList<EnglishNamed>();
      __publicValidValues = Collections.unmodifiableList(__validValues);
    } else {
      __validValues = null;
      __publicValidValues = null;
    }
    __defaultValue = defaultValue;
  }

  public final boolean hasValidValues()
  {
    return __validValues != null;
  }

  public synchronized void addConstantValidValue(EnglishNamed validValue)
  {
    if (isLocked()) {
      throw new IllegalStateException("PropertyRequirement is locked");
    }
    if (validValue == null) {
      return;
    }
    if (__validValues == null) {
      throw new IllegalStateException("PropertyRequirement does not support ConstantValidValue");
    }
    __validValues.add(validValue);
  }

  public void addConstantValidValue(Object object)
  {
    if (object != null) {
      String defaultString = object.toString();
      addConstantValidValue(new EnglishNamedImpl(defaultString, defaultString));
    }
  }

  public void addConstantValidValues(Iterator<?> values)
  {
    while (values.hasNext()) {
      Object nextValue = values.next();
      if (nextValue instanceof EnglishNamed) {
        addConstantValidValue((EnglishNamed) nextValue);
      } else {
        addConstantValidValue(nextValue);
      }
    }
  }

  public synchronized void lock()
  {
    _isLocked = true;
  }

  public synchronized boolean isLocked()
  {
    return _isLocked;
  }

  /**
   * Check to see whether or not this requirement should be included in an interactive GUI to permit
   * the user to change the value. If this is false then an invalid value should be a fatal error,
   * while true values should let the user attempt to try and rectify the situation. In general,
   * non-gui items are thought of as something that the Gettable source should have before the
   * GettableFilter cycle is started, and gui items are for fields that should either be added or
   * updated during the cycle.
   */
  public boolean isGuiItem()
  {
    return __isGuiItem;
  }

  public boolean isCompulsory()
  {
    return __isCompulsory;
  }

  public String getDefaultValue()
  {
    return __defaultValue;
  }

  /**
   * Register the default value (if defined) if the given Basket doesn't have a valid value already
   * stored on itself. <i>Note that if the value that is currently stored on the source is defined,
   * but doesn't pass the isValid(source) check, and the getDefaultValue() is null then the source
   * will have a value of null set. I am currently of mixing feelings as to whether or not this is
   * the "correct" behaviour.</i>
   */
  public void registerDefaultValue(Settable source)
      throws SettableException
  {
    if (getDefaultValue() != null) {
      String value = getValue(source);
      if (value == null || !isValid(source)) {
        source.set(getName(), getDefaultValue());
      }
    }
  }

  /**
   * Get the List of valid values that is appropriate for the given named property with respect to
   * the given Gettable source. This method should be overridden to actually do something useful if
   * your implementation of GettableFilterProperty has a non-constant list of valid values. <i>We
   * need to check that supporting code that is utilising this method is not caching this
   * information and that any triggers that might cause the list to be rebuilt also cause the list
   * introspection client to refresh the representation of the list. However, this is tricky without
   * knowing the situations when this might occur.</i>
   * 
   * @param source
   *          The Gettable that is currently being queried to determine whether or not it is valid.
   * @return Iterator of EnglishNamed choices where the getName() has the appropriate value and the
   *         getEnglishName() has the publically visible version of this value. If there are no
   *         valid values defined then this will be an empty list.
   */
  public Iterator<EnglishNamed> getValidValues(Gettable source)
  {
    return __publicValidValues == null
        ? ImmutableSet.<EnglishNamed> of().iterator()
        : __publicValidValues.iterator();
  }

  /**
   * Get the value defined on source that is targetted by this property.
   * 
   * @param source
   *          The source that will define the value.
   * @return The String version of the value defined on source as converted by GettableUtils, or
   *         null if the source doesn't define a value.
   * @see GettableUtils#get(Gettable,String,String)
   */
  public String getValue(Gettable source)
  {
    return GettableUtils.get(source, getName(), null);
  }

  /**
   * Get the value that this filter targets, providing automatic expansion to the EnglishName
   * version of the value if it is found. This is performed by resolving the conventional value
   * utilising getValue(source) and then traversing the list of valid values that are currently
   * registered on this PropertyRequirement looking for items with matching Name properties. If one
   * is found then the EnglishName expansion is utilised, otherwise the original Name is returned.
   * 
   * @param source
   *          The Gettable item whos value is to be expanded.
   * @return The EnglishName version of the value. If the GettableFilterProperty does not support
   *         valid values (or the value is not found (which may or may not be a bug - please
   *         re-examine)) then the original Name is returned. If the value is not defined then null
   *         will be returned.
   * @see #getValue(Gettable)
   */
  public String getEnglishValue(Gettable source)
  {
    String value = getValue(source);
    if (value == null) {
      return null;
    }
    Iterator<EnglishNamed> validValues = getValidValues(source);
    if (validValues == null) {
      return null;
    }
    while (validValues.hasNext()) {
      EnglishNamed validValue = validValues.next();
      if (value.equals(validValue.getName())) {
        return validValue.getEnglishName();
      }
    }
    return value;
  }

  /**
   * Check to see whether the value defined in the given Gettable satisfies the demands set out in
   * this PropertyRequirement.
   */
  public boolean isValid(Gettable source)
  {
    String proposedValue = getValue(source);
    Iterator<EnglishNamed> validValues = getValidValues(source);
    if (validValues != null && validValues.hasNext()) {
      while (validValues.hasNext()) {
        EnglishNamed validValue = validValues.next();
        if (validValue.getName().equals(proposedValue)) {
          return true;
        }
      }
      return false;
    } else {
      return (isCompulsory() ? proposedValue != null : true);
    }
  }
}
