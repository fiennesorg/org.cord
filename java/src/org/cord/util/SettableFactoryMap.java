package org.cord.util;

public class SettableFactoryMap
  implements SettableFactory
{
  private final Settable __settable = new SettableMap();

  @Override
  public Settable getSettable()
  {
    return __settable;
  }

  @Override
  public Gettable getGettable()
  {
    return __settable;
  }
}
