package org.cord.util;

import java.util.Comparator;
import java.util.Iterator;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Implementation of Comparator that nests a sequence of sub-comparators, invoking each in turn
 * until one specifies that the two Objects are not equal.
 */
public class DelegatingComparator<T>
  implements Comparator<T>
{
  public static class Builder<T>
  {
    private ImmutableList.Builder<Comparator<T>> __list = ImmutableList.builder();

    /**
     * Add a Comparator to the chain in this Builder. This Comparator will be invoked if all the
     * preceeding Comparators viewed the T being compared as equal.
     * 
     * @param comparator
     *          The compulsory Comparator to add.
     */
    public Builder<T> of(Comparator<T> comparator)
    {
      __list.add(Preconditions.checkNotNull(comparator, "comparator"));
      return this;
    }

    public Builder<T> of(Iterator<Comparator<T>> i)
    {
      while (i.hasNext()) {
        of(i.next());
      }
      return this;
    }

    public Builder<T> of(Iterable<Comparator<T>> i)
    {
      return of(i.iterator());
    }

    public DelegatingComparator<T> build()
    {
      return new DelegatingComparator<T>(__list.build());
    }
  }

  public static <T> Builder<T> of(Iterator<Comparator<T>> i)
  {
    Builder<T> builder = of(i.next());
    builder.of(i);
    return builder;
  }

  public static <T> Builder<T> of(Comparator<T> comparator)
  {
    return new Builder<T>().of(comparator);
  }

  private final ImmutableList<Comparator<T>> __comparators;
  private final int __size;

  public DelegatingComparator(ImmutableList<Comparator<T>> comparators)
  {
    Preconditions.checkState(comparators != null && comparators.size() > 0,
                             "invalid comparators: %s",
                             comparators);
    __comparators = comparators;
    __size = comparators.size();
  }

  @Override
  public int compare(final T t1,
                     final T t2)
  {
    int v = __comparators.get(0).compare(t1, t2);
    if (v != 0) {
      return v;
    }
    final int l = __size;
    for (int i = 1; i < l; i++) {
      v = __comparators.get(i).compare(t1, t2);
      if (v != 0) {
        return v;
      }
    }
    return 0;
  }

  @Override
  public String toString()
  {
    StringBuilder buf = new StringBuilder();
    buf.append("DelegatingComparator(").append(__comparators).append(")");
    return buf.toString();
  }

}