package org.cord.util;

public class NotImplementedException
  extends LogicException
{
  /**
   * 
   */
  private static final long serialVersionUID = 939225707664216334L;

  public NotImplementedException()
  {
    super("Not Implemented");
  }

  public NotImplementedException(String message)
  {
    super(message);
  }

  public NotImplementedException(String message,
                                 Throwable cause)
  {
    super(message,
          cause);
  }
}