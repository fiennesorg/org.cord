package org.cord.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class SingletonShutdown
{
  private static List<SingletonShutdownable> __instances = new ArrayList<SingletonShutdownable>();

  public static void registerSingleton(SingletonShutdownable instance)
  {
    if (DebugConstants.DEBUG_RESOURCES) {
      DebugConstants.DEBUG_OUT.println("SingletonShutdown: Registering: " + instance);
    }
    __instances.add(instance);
  }

  public static void shutdown()
  {
    Iterator<SingletonShutdownable> i = __instances.iterator();
    while (i.hasNext()) {
      SingletonShutdownable next = i.next();
      if (DebugConstants.DEBUG_RESOURCES) {
        DebugConstants.DEBUG_OUT.println("SingletonShutdown: Shutting down: " + next);
      }
      next.shutdownSingleton();
      i.remove();
    }
  }
}
