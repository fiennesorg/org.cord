package org.cord.util;

/**
 * Base class for {@link SimpleGettable} implementations that provides implementations for
 * {@link TypedKey} access methods.
 * 
 * @author alex
 */
@Deprecated
public abstract class AbstractSimpleGettable
  implements SimpleGettable
{
}
