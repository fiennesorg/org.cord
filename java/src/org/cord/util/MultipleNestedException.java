package org.cord.util;

import java.util.Iterator;

public interface MultipleNestedException
{
  /**
   * @return Iterator of Exception
   * @see Exception
   */
  public Iterator<Exception> getNestedExceptions();
}
