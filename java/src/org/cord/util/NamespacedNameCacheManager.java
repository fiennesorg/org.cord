package org.cord.util;

import java.util.concurrent.ConcurrentHashMap;

import org.cord.util.NamespacedNameCache.Ordering;

/**
 * There are many different subsystems that need access to the NamespacedNameCache so this provides
 * a way of sharing the populated instances between them
 * 
 * @author alex
 * @see org.cord.node.NodeManager#getNamespacedNameCacheManager()
 * @see org.cord.util.NamespacedNameCache
 */
public class NamespacedNameCacheManager
{
  private final ConcurrentHashMap<String, NamespacedNameCache> __prefixCaches =
      Maps.newConcurrentHashMap();

  private final ConcurrentHashMap<String, NamespacedNameCache> __postfixCaches =
      Maps.newConcurrentHashMap();

  public NamespacedNameCache getPrefixNamespacedNameCache(String namespace)
  {
    NamespacedNameCache cache = __prefixCaches.get(namespace);
    if (cache == null) {
      cache = new NamespacedNameCache(namespace, Ordering.PREFIXNAMESPACE);
      __prefixCaches.put(namespace, cache);
    }
    return cache;
  }

  public NamespacedNameCache getPostfixNamespacedNameCache(String namespace)
  {
    NamespacedNameCache cache = __postfixCaches.get(namespace);
    if (cache == null) {
      cache = new NamespacedNameCache(namespace, Ordering.POSTFIXNAMESPACE);
      __postfixCaches.put(namespace, cache);
    }
    return cache;
  }
}
