package org.cord.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.cord.node.requires.RequirementMgr;
import org.joda.time.DateTime;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;

public abstract class HtmlUtilsTheme
{

  /**
   * Append an attribute declaration onto an output buffer. This will add a leading space before the
   * name. The value will have double quotes automatically escaped.
   * 
   * @param value
   *          The value of the attribute. If this is empty or null then no attribute will be
   *          appended.
   * @see StringUtils#toHtmlValue(Object)
   */
  public final static void addAttribute(Appendable buf,
                                        String name,
                                        Object value)
      throws IOException
  {
    if (value != null) {
      buf.append(" ")
         .append(name)
         .append("=\"")
         .append(StringUtils.toHtmlValue(value.toString()))
         .append("\"");
    }
  }

  public final static void openDivOfClass(Appendable buf,
                                          String cssClass)
      throws IOException
  {
    buf.append("<div");
    addAttribute(buf, "class", cssClass);
    buf.append(">");
  }

  /**
   * Create an open div with optional id and class attributes.
   */
  public final static void openDivOfIdClass(Appendable buf,
                                            String cssId,
                                            String cssClass)
      throws IOException
  {
    buf.append("<div");
    addAttribute(buf, "id", cssId);
    addAttribute(buf, "class", cssClass);
    buf.append(">");
  }

  public final static void openSpanOfClass(Appendable buf,
                                           String cssClass)
      throws IOException
  {
    buf.append("<span");
    addAttribute(buf, "class", cssClass);
    buf.append(">");
  }

  public final static void closeSpan(Appendable buf)
      throws IOException
  {
    buf.append("</span>");
  }

  public final static String appendIds(Object... ids)
  {
    if (ids.length == 0) {
      return null;
    }
    StringBuilder buf = new StringBuilder();
    try {
      HtmlUtilsTheme.appendIds(buf, ids);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  /**
   * Append a close div tag followed by a newline.
   */
  public final static void closeDiv(Appendable buf)
      throws IOException
  {
    buf.append("</div>\n");
  }

  /**
   * Append count close div tags followed by a newline.
   */
  public final static void closeDivs(Appendable buf,
                                     int count)
      throws IOException
  {
    for (int i = 0; i < count; i++) {
      buf.append("</div>");
    }
    buf.append('\n');
  }

  public final static void activeIndicator(Appendable buf,
                                           Object... ids)
      throws IOException
  {
    String id = appendIds(ids);
    HtmlUtilsTheme.hiddenInput(buf, "true", id, HtmlUtilsTheme.ACTIVENAMEPOSTFIX);
  }

  public final static void appendIds(Appendable buf,
                                     Object... ids)
      throws IOException
  {
    boolean isFirstChar = true;
    for (Object obj : ids) {
      if (obj != null) {
        String id = obj.toString();
        if (id.length() > 0) {
          if (isFirstChar) {
            if (!Character.isLetter(id.charAt(0))) {
              throw new IllegalArgumentException(String.format("%s does not begin with a letter and is therefore an illegal id",
                                                               StringUtils.join("", ids)));
            }
            isFirstChar = false;
          }
          for (int i = 0; i < id.length(); i++) {
            char c = id.charAt(i);
            if (Character.isLetterOrDigit(c) | c == '_' | c == '-') {
              buf.append(c);
            } else {
              throw new IllegalArgumentException(String.format("%s contains non alphanumeric character: \"%s\"",
                                                               StringUtils.join("", ids),
                                                               Character.toString(c)));
            }
          }
        }
      }
    }
  }

  public final static void hiddenInput(Appendable buf,
                                       Object value,
                                       Object... names)
      throws IOException
  {
    buf.append("<input");
    addAttribute(buf, "type", "hidden");
    buf.append(" name=\"");
    appendIds(buf, names);
    buf.append("\"");
    addAttribute(buf, "value", value);
    buf.append(" />\n");
    if (DebugConstants.DEBUG_NODEREQUEST_FORMS) {
      buf.append(String.format("(%s = %s)<br />",
                               Joiner.on("").join(names),
                               DebugConstants.argClass(value)));
    }
  }

  public static final String ACTIVENAMEPOSTFIX = "-active";

  @Deprecated
  public static final String CSS_W_TEXT = "w_text";

  @Deprecated
  public static final String CSS_W_SELECT = "w_select";

  @Deprecated
  public static final String CSS_W_FILE = "w_file";

  @Deprecated
  public static final String CSS_W_VALUE = "w_value";

  public abstract void openWidgetDiv(Appendable buf,
                                     String className,
                                     Object... ids)
      throws IOException;

  public abstract void openLabelWidget(Appendable buf,
                                       String label,
                                       boolean isCompulsory,
                                       Object... ids)
      throws IOException;

  public abstract void openLabelWidget(Appendable buf,
                                       String label,
                                       String widget,
                                       boolean isCompulsory,
                                       Object... ids)
      throws IOException;

  /**
   * @param contents
   *          The visible contents of the label. Note that this value is currently not escaped or
   *          validated in any way as inline markup inside a label is permitted in HTML.
   */
  public abstract void label(Appendable buf,
                             String contents,
                             boolean isCompulsory,
                             Object... ids)
      throws IOException;

  /**
   * @param value
   *          The optional data that will be placed inside the w_value div. This isn't escaped at
   *          all so it is possible to nest active HTML inside the div, but this does mean that the
   *          responsibility of ensuring that the HTML is well-formed rests with the invoking class.
   *          If the value is null then the behaviour will be dependable on the Appendable buf that
   *          is passed in, but this will normally be to render the word "null"
   */
  public abstract void value(Appendable buf,
                             Object value,
                             Object... ids)
      throws IOException;

  /**
   * Create a visible label::value item, with a corresponding hidden field behind it. The default
   * implementation just invoked {@link #hiddenInput(Appendable, Object, Object...)} and
   * {@link #labelValue(Appendable, String, Object, Object...)} to generate the widget. The visible
   * value has "__vsbl" appended onto the end of the id to avoid getting multiple copies of the same
   * id. Implementations of HtmlUtilsTheme should override this if necessary, but it should "just
   * work" using the custom implementation of
   * {@link #labelValue(Appendable, String, Object, Object...)}
   */
  public void visible(Appendable buf,
                      String label,
                      Object visibleValue,
                      Object submitValue,
                      Object... ids)
      throws IOException
  {
    hiddenInput(buf, submitValue, ids);
    labelValue(buf, label, visibleValue, ArrayUtils.extendSingle(ids, "__vsbl"));
  }

  public abstract void errorValue(Appendable buf,
                                  String error,
                                  Object... ids)
      throws IOException;

  public abstract void labelErrorValue(Appendable buf,
                                       String label,
                                       Object value,
                                       Object... ids)
      throws IOException;

  /**
   * Append a label and static value to the Form.
   */
  public abstract void labelValue(Appendable buf,
                                  String label,
                                  Object value,
                                  Object... ids)
      throws IOException;

  public abstract void textWidget(String autocomplete, 
                                  Appendable buf,
                                  String value,
                                  Object... ids)
      throws IOException;

  /**
   * Create an {@link #autocompleteWidget(Appendable, String, Iterable, Object...)} with the
   * appropriate labelling. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on
   * the file dependencies, but this is the responsibility of the invoking method.
   */
  public abstract void autocomplete(Appendable buf,
                                    String label,
                                    String value,
                                    boolean isCompulsory,
                                    Iterable<String> options,
                                    Object... ids)
      throws IOException;

  /**
   * Invoke {@link #autocompleteInputWidget(Appendable, String, Iterable, Object...)} wrapped in a
   * widget div. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on the file
   * dependencies, but this is the responsibility of the invoking method.
   */
  public abstract void autocompleteWidget(Appendable buf,
                                          String value,
                                          Iterable<String> options,
                                          Object... ids)
      throws IOException;

  /**
   * Render a {@link #textInputWidget(String, Appendable, String, Object...)} with the appropriate
   * javascript to give autocompletion. This needs to have had {@link RequirementMgr#KEY_QUERY_UI}
   * registered on the file dependencies, but this is the responsibility of the invoking method.
   */
  public abstract void autocompleteInputWidget(Appendable buf,
                                               String value,
                                               Iterable<String> options,
                                               Object... ids)
      throws IOException;

  public abstract void textInputWidget(String autocomplete,
                                       Appendable buf,
                                       String value,
                                       Object... ids)
      throws IOException;

  public abstract void textInputWidget(String autocomplete, 
                                       Appendable buf,
                                       String cssClass,
                                       String value,
                                       Object... ids)
      throws IOException;

  public abstract void text(String autocomplete,
                            Appendable buf,
                            String label,
                            String value,
                            boolean isCompulsory,
                            Object... ids)
      throws IOException;

  /**
   * @param date
   *          The Object to be parsed into a Date
   * @return The Date if it is possible to parse or null if impossible.
   */
  public abstract Date parseDateSelector(Object date);
  public abstract String formatDateSelector(Date date);

  public abstract void dateSelector(Appendable buf,
                                    Map<Object, Object> context,
                                    String label,
                                    Date value,
                                    Object... ids)
      throws IOException;

  public abstract void jodaDate(Appendable buf,
                                Map<Object, Object> context,
                                String label,
                                Optional<DateTime> value,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  public abstract void jodaDateWidget(Appendable buf,
                                      Map<Object, Object> context,
                                      Optional<DateTime> value,
                                      Object... ids)
      throws IOException;

  public abstract Optional<DateTime> jodaTimeParse(String text);
  public abstract String jodaTimeFormat(Optional<DateTime> time);

  public abstract Optional<DateTime> jodaDateParse(String text);
  public abstract String jodaDateFormat(Optional<DateTime> date);

  public abstract Date parseDateTimeSelector(Object date);
  public abstract String formatDateTimeSelector(Date date);

  public abstract void dateTimeSelector(Appendable buf,
                                        Map<Object, Object> context,
                                        String label,
                                        Optional<Date> value,
                                        Object... ids)
      throws IOException;

  public abstract void dateTimeSelectorWidget(Appendable buf,
                                              Map<Object, Object> context,
                                              Optional<Date> value,
                                              Object... ids)
      throws IOException;

  public abstract void jodaTimeWidget(Appendable buf,
                                      Map<Object, Object> context,
                                      Optional<DateTime> value,
                                      Object... ids)
      throws IOException;

  public abstract void jodaTime(Appendable buf,
                                Map<Object, Object> context,
                                String label,
                                Optional<DateTime> value,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  public abstract void password(String autocomplete,
                                Appendable buf,
                                String label,
                                String value,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  public abstract void textareaWidget(Appendable buf,
                                      String value,
                                      int cols,
                                      int rows,
                                      boolean isCompulsory,
                                      Object... ids)
      throws IOException;

  public abstract void textarea(Appendable buf,
                                String label,
                                String value,
                                int cols,
                                int rows,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  public abstract void openSelect(Appendable buf,
                                  boolean isMultiple,
                                  boolean shouldReloadOnChange,
                                  boolean isCompulsory,
                                  Object... ids)
      throws IOException;

  public abstract void openSelectWidget(Appendable buf,
                                        String cssClass,
                                        boolean isMultiple,
                                        boolean shouldReloadOnChange,
                                        boolean isCompulsory,
                                        Object... ids)
      throws IOException;

  public abstract void option(Appendable buf,
                              Object valueObj,
                              Object contentsObj,
                              boolean isSelected)
      throws IOException;

  public abstract void closeSelectWidget(Appendable buf)
      throws IOException;

  public abstract void closeSelect(Appendable buf,
                                   String summary)
      throws IOException;

  public abstract void checkboxInput(Appendable buf,
                                     String value,
                                     boolean isChecked,
                                     boolean shouldReloadOnChange,
                                     boolean isCompulsory,
                                     Object... ids)
      throws IOException;

  public abstract void checkboxInputWidget(Appendable buf,
                                           String value,
                                           boolean isChecked,
                                           boolean shouldReloadOnChange,
                                           boolean isCompulsory,
                                           Object... ids)
      throws IOException;

  public abstract void radio(Appendable buf,
                             String name,
                             String value,
                             boolean isChecked,
                             String label,
                             Object... ids)
      throws IOException;

  public abstract void openRadios(Appendable buf,
                                  String label)
      throws IOException;

  public abstract void closeRadios(Appendable buf)
      throws IOException;
  /**
   * Render an input field of type checkbox into the output buffer.
   */
  public abstract void checkboxInputWidget(Appendable buf,
                                           String value,
                                           boolean isChecked,
                                           String confirmOnClickMessage,
                                           boolean shouldReloadOnChange,
                                           boolean isCompulsory,
                                           Object... ids)
      throws IOException;

  public abstract void checkbox(Appendable buf,
                                String label,
                                String value,
                                boolean isChecked,
                                boolean shouldReloadOnChange,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  /**
   * Use URLEncode to encode a string into UTF-8 URL encoding. Ths wraps the
   * UnsupportedEncodingException in a LogicException thereby making code easier to read.
   * 
   * @throws LogicException
   *           if for some reason the system cannot find UTF-8 character encoding. This should never
   *           happen.
   * @see URLEncoder#encode(java.lang.String, java.lang.String)
   */
  public final static String urlEncode(String source)
  {
    try {
      return URLEncoder.encode(source, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new LogicException("Lost UTF-8", e);
    }
  }

  public abstract void fileWidget(Appendable buf,
                                  Object... ids)
      throws IOException;

  public abstract void submit(Appendable buf,
                              String value,
                              String onclick)
      throws IOException;

  public abstract void submitButton(Appendable buf,
                                    String value,
                                    String onclick,
                                    String onmouseover,
                                    String onmouseout)
      throws IOException;

  public abstract void errorBlock(Appendable buf,
                                  String error,
                                  String classNames)
      throws IOException;

  /**
   * Add in whatever HTML code is necessary to trigger the form validator on the current form.
   */
  public abstract void includeFormValidator(Appendable out,
                                            Map<Object, Object> context)
      throws IOException;

  public final static void openDiv(Appendable buf,
                                   String id,
                                   Object... classNames)
      throws IOException
  {
    buf.append("<div");
    addAttribute(buf, "id", id);
    addAttribute(buf, "class", StringUtils.join(" ", classNames));
    buf.append(">");
  }

  public abstract void compulsorySubmitButton(Appendable out,
                                              String value,
                                              Map<Object, Object> context,
                                              Set<Object> compulsoryFields)
      throws IOException;

  public abstract void cancelButton(Appendable out,
                                    String value,
                                    String url)
      throws IOException;
}
