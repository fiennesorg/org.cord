package org.cord.util;

/**
 * Exception that is thrown by a StringValidator if it rejects a String.
 * 
 * @see StringValidator#validate(String)
 */
public class StringValidatorException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = -3282520985484080340L;

  private final String _string;

  private final String _proposedSubstitution;

  public StringValidatorException(String reason,
                                  String string,
                                  String proposedSubstitution)
  {
    super(reason);
    _string = string;
    _proposedSubstitution = proposedSubstitution;
  }

  public StringValidatorException(String reason,
                                  String string)
  {
    this(reason,
         string,
         null);
  }

  /**
   * Get the string that generated the error. This is not included in the name of the exception to
   * provide minimal security. ie if you want to know what it was, then you have to explicitly catch
   * the exception and ask for it.
   * 
   * @return the erroneous string.
   */
  public final String getString()
  {
    return _string;
  }

  /**
   * Get the proposed substitution for the offending String (if any). Anything which throws a a
   * StringValidatorException which defines a proposed substitution should take care that the
   * substitution is valid as it will probably get resubmitted to the original class, otherwise
   * non-terminating loops may occur.
   * 
   * @return The proposed subsitution for the offending String, or null if no solution is found.
   */
  public final String getProposedSubstitution()
  {
    return _proposedSubstitution;
  }
}
