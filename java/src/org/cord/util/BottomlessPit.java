package org.cord.util;

/**
 * Singleton webmacro class that always returns an empty String no matter what variable is
 * requested. This is useful in situations when you want a temporary empty Object that will be
 * filled at a later situation by a concrete instance of the item, but you don't want the overhead
 * of creating the item until the user has jumped through some hoops first.
 */
public class BottomlessPit
{
  private static BottomlessPit __instance;

  public static BottomlessPit getInstance()
  {
    return __instance;
  }

  private BottomlessPit()
  {
  }

  public Object get(Object key)
  {
    return "";
  }
}
