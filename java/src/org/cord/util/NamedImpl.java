package org.cord.util;

import com.google.common.base.Preconditions;

/**
 * Base implementation of the Named interface.
 */
public class NamedImpl
  implements Named
{
  private final String __name;

  /**
   * Create a new instance with a non-validated name.
   * 
   * @param name
   *          The name of the instance.
   */
  public NamedImpl(String name)
  {
    __name = Preconditions.checkNotNull(name, "name");
  }

  @Override
  public final String getName()
  {
    return __name;
  }

  @Override
  public String toString()
  {
    return getName();
  }

  @Override
  public int compareTo(Named n)
  {
    return getName().compareTo(n.getName());
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Named n = (Named) o;
    return getName().equals(n.getName());
  }

  @Override
  public int hashCode()
  {
    return getName().hashCode();
  }
}
