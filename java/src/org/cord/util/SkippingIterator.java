package org.cord.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.google.common.collect.ImmutableSet;

/**
 * Abstract class that describes an Object that wraps around an Iterator and selectively passes or
 * rejects values in the Iterator while still appearing to be an Iterator to all intents and
 * purposes. In order to implement this then you should just implement a single method
 * shouldSkip(Object object) that will be invoked automatically as and when it is required.
 * 
 * @see #shouldSkip(Object)
 */
public abstract class SkippingIterator<T>
  implements Iterator<T>
{
  private final Iterator<T> _iterator;

  private boolean _isInitialised;

  private T _nextObject;

  private boolean _hasNext;

  public SkippingIterator(Iterator<T> iterator)
  {
    _iterator = iterator == null ? ImmutableSet.<T> of().iterator() : iterator;
    _isInitialised = false;
  }

  protected abstract boolean shouldSkip(T object);

  private void calculateNextObject()
  {
    boolean foundValue = false;
    while ((!foundValue) && (_hasNext = _iterator.hasNext())) {
      _nextObject = _iterator.next();
      if (!shouldSkip(_nextObject)) {
        foundValue = true;
        _nextObject = filterNext(_nextObject);
      }
    }
    _isInitialised = true;
  }

  protected T filterNext(T obj)
  {
    return obj;
  }

  @Override
  public final boolean hasNext()
  {
    if (!_isInitialised) {
      calculateNextObject();
    }
    return _hasNext;
  }

  @Override
  public final T next()
      throws NoSuchElementException
  {
    if (hasNext()) {
      T nextObject = _nextObject;
      calculateNextObject();
      return nextObject;
    }
    throw new NoSuchElementException();
  }

  /**
   * Be careful with this one as it delegates directly to the underlying Iterator. Maybe it is nicer
   * to just reject it on principle...
   */
  @Override
  public final void remove()
      throws UnsupportedOperationException, IllegalStateException
  {
    _iterator.remove();
  }
}
