package org.cord.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

public class XmlToMaps
  extends DefaultHandler
{
  private final String __mapTag;

  private final XmlToMapsHandler __handler;

  private final Set<String> __outerTags = new HashSet<String>();

  private Map<String, String> _currentRecord;

  private String _tagName;

  private StringBuilder _tagValue;

  public static void parse(InputSource inputSource,
                           XmlToMapsHandler handler,
                           String containerTag,
                           String externalTag)
      throws SAXException, IOException
  {
    XMLReader xmlReader = XMLReaderFactory.createXMLReader();
    XmlToMaps xmlToMaps = new XmlToMaps(containerTag, handler);
    xmlToMaps.addOuterTag(externalTag);
    xmlReader.setContentHandler(xmlToMaps);
    xmlReader.setErrorHandler(xmlToMaps);
    xmlReader.parse(inputSource);
  }

  public static void parse(File xml,
                           String encoding,
                           XmlToMapsHandler handler,
                           String containerTag,
                           String externalTag)
      throws SAXException, IOException
  {
    InputStreamReader reader = null;
    try {
      reader = new InputStreamReader(new FileInputStream(xml), encoding);
      parse(new InputSource(reader), handler, containerTag, externalTag);
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }

  public XmlToMaps(String mapTag,
                   XmlToMapsHandler handler)
  {
    __mapTag = mapTag;
    __handler = handler;
  }

  public void addOuterTag(String tagName)
  {
    __outerTags.add(tagName);
  }

  @Override
  public void endElement(String namespaceURI,
                         String localName,
                         String qName)
      throws SAXException
  {
    if (__outerTags.contains(localName)) {
      if (_currentRecord != null) {
        throw new SAXException("Cannot have </" + localName + "> inside <" + __mapTag + ">");
      }
      return;
    }
    if (__mapTag.equals(localName)) {
      if (_currentRecord == null) {
        throw new SAXException("</" + __mapTag + "> without <" + __mapTag + ">");
      }
      try {
        __handler.handle(this, _currentRecord);
      } catch (Exception e) {
        SAXException sEx = new SAXException("Failed processing in endElement of " + _tagName
                                            + " using " + _currentRecord,
                                            e);
        throw sEx;
      }
      _currentRecord = null;
      _tagName = null;
      return;
    }
    _currentRecord.put(_tagName, _tagValue.toString());
    _tagName = null;
    _tagValue = null;
  }

  @Override
  public void characters(char[] ch,
                         int start,
                         int length)
  {
    if (_tagValue != null) {
      _tagValue.append(ch, start, length);
    }
  }

  @Override
  public void startElement(String namespaceURI,
                           String localName,
                           String qName,
                           Attributes atts)
      throws SAXException
  {
    if (__outerTags.contains(localName)) {
      if (_currentRecord != null) {
        throw new SAXException("Cannot nest outer tag <" + localName + "> inside <" + __mapTag
                               + ">");
      }
      return;
    }
    if (__mapTag.equals(localName)) {
      if (_currentRecord == null) {
        _currentRecord = new HashMap<String, String>();
      } else {
        throw new SAXException("Cannot nest <" + __mapTag + "> inside itself");
      }
      return;
    }
    if (_currentRecord == null) {
      throw new SAXException("Unexpected tag <" + localName + "> outside <" + __mapTag + ">");
    }
    if (_tagName == null) {
      _tagName = localName;
      _tagValue = new StringBuilder();
    } else {
      throw new SAXException("<" + localName + "> should not be nested inside <" + _tagName + ">");
    }
  }

  /*
   * (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
   */
  @Override
  public void error(SAXParseException arg0)
      throws SAXException
  {
    System.err.println(this + ".error()");
    System.err.println("currentRecord:" + _currentRecord);
    System.err.println("tagName:" + _tagName);
    System.err.println("tagValue:" + _tagValue);
    arg0.printStackTrace();
  }

  /*
   * (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException )
   */
  @Override
  public void fatalError(SAXParseException arg0)
      throws SAXException
  {
    System.err.println(this + ".fatalError()");
    System.err.println(_currentRecord);
    System.err.println("tagName:" + _tagName);
    System.err.println("tagValue:" + _tagValue);
    arg0.printStackTrace();
  }

  /*
   * (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
   */
  @Override
  public void warning(SAXParseException arg0)
      throws SAXException
  {
    System.err.println(this + ".warning()");
    System.err.println(_currentRecord);
    System.err.println("tagName:" + _tagName);
    System.err.println("tagValue:" + _tagValue);
    arg0.printStackTrace();
  }
}
