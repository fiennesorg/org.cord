package org.cord.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

/**
 * Utility class of static methods for performing operations on Arrays of Objects.
 */
public final class ArrayUtils
{
  /**
   * return a copy of the source array which is guaranteed to have targetLength, with all nulls
   * mapped to "" and trim() invoked on all values. If source is shorter than targetLength then it
   * will be expanded with "" values, and if it is longer then extra entries will be dropped.
   */
  public static String[] copyTrimAndPad(String[] source,
                                        int targetLength)
  {
    String[] result = new String[targetLength];
    for (int i = 0; i < targetLength; i++) {
      if (i < source.length) {
        result[i] = Strings.nullToEmpty(source[i]).trim();
      } else {
        result[i] = "";
      }
    }
    return result;
  }

  /**
   * Return a copy of the source array with the resulting array having a guaranteed minimum length
   * and all nulls converted to "" and all Strings trimmed. If the source array is longer than
   * minLength then the resulting array is also longer.
   */
  public static String[] copyAndPad(String[] source,
                                    int minLength)
  {
    return copyTrimAndPad(source, Math.max(source.length, minLength));
  }

  /**
   * Search through an array of Objects for the first occurence of a given Object. This method will
   * handle nulls in an intelligent way for both the array and the targetted object.
   * 
   * @param object
   *          The object to search for
   * @param array
   *          The array to search through
   * @return the index of the object or -1 if it is not found.
   */
  public static int indexOf(final Object object,
                            final Object[] array)
  {
    final int l = array.length;
    for (int i = 0; i < l; i++) {
      if (Objects.equal(array[i], object)) {
        return i;
      }
    }
    return -1;
  }

  public static int lastIndexOf(final Object[] array,
                                final Object object)
  {
    if (array.length == 0) {
      return -1;
    }
    for (int i = array.length - 1; i >= 0; i--) {
      if (Objects.equal(array[i], object)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Attempt to decode a valueOrIndex as an Integer, failing that look up the String in a list of
   * possible values.
   * 
   * @param valueOrIndex
   *          Either a String version of the index, or a value that is contained in values.
   * @param values
   *          A list of possible values for valueOrIndex
   * @param defaultIndex
   *          The index that should be returned if one of the following happen:-
   *          <ul>
   *          <li>valueOrIndex is null
   *          <li>the decoded version of valueOrIndex is an int outside the scope of values.
   *          <li>valueOrIndex contain a non-numeric String that is not found in values.
   *          </ul>
   * @return The required index, or defaultIndex if there has been an error.
   */
  public static int extractIndex(String valueOrIndex,
                                 String[] values,
                                 int defaultIndex)
  {
    int index = defaultIndex;
    if (valueOrIndex == null) {
      return index;
    }
    try {
      index = Integer.parseInt(valueOrIndex);
    } catch (NumberFormatException nfEx) {
      index = indexOf(valueOrIndex, values);
    }
    if (index >= 0 && index < values.length) {
      return index;
    }
    return defaultIndex;
  }

  /**
   * Create a new Iterator that contains all the elements of the source Iterator, but with the
   * ordering randomised. This will iterate across all members of the source iterator before
   * returning the reordered iterator thereby making it unsuitable for streaming usages.
   * 
   * @param source
   *          The Iterator in normal ordering
   * @return Iterator in randomised ordering.
   * @see Collections#shuffle(java.util.List)
   */
  public static <T> Iterator<T> randomise(Iterator<T> source)
  {
    List<T> randomisedList = new ArrayList<T>();
    while (source.hasNext()) {
      randomisedList.add(source.next());
    }
    Collections.shuffle(randomisedList);
    return randomisedList.iterator();
  }

  /**
   * Initialise a new HashMap containing the data in the given keys and values array such that
   * keys[n] --> values[n]. If the length of either keys or values is not identical then only the
   * shorter of the two will be the length of the Map. The Map that is returned is mutable.
   */
  public static Map<Object, Object> asMap(Object[] keys,
                                          Object[] values)
  {
    HashMap<Object, Object> map = new HashMap<Object, Object>();
    int mapSize = Math.min(keys.length, values.length);
    for (int i = 0; i < mapSize; i++) {
      if (keys[i] != null) {
        map.put(keys[i], values[i]);
      }
    }
    return map;
  }

  /**
   * @deprecated in preference of {@link Arrays#toString(Object[])}
   */
  @Deprecated
  public static String toString(Object[] array)
  {
    if (array == null) {
      return "null";
    }
    StringBuilder result = new StringBuilder();
    result.append('{');
    for (int i = 0; i < array.length; i++) {
      result.append('"').append(array[i]).append('"');
      if (i < array.length - 1) {
        result.append(", ");
      }
    }
    result.append('}');
    return result.toString();
  }

  /**
   * Create a new array that starts with the contents of source and is followed by the values of all
   * the varargs p. A new array is always created. Null values are preserved.
   */
  public static <T> T[] extendMany(T[] source,
                                   T... p)
  {
    T[] result = Arrays.copyOf(source, source.length + p.length);
    System.arraycopy(p, 0, result, source.length, p.length);
    return result;
  }

  /**
   * Create a new array that contains the contents of source with p appended on the end. A new array
   * is always created. Null values are preserved.
   */
  public static <T> T[] extendSingle(T[] source,
                                     T p)
  {
    T[] result = Arrays.copyOf(source, source.length + 1);
    result[source.length] = p;
    return result;
  }

  /**
   * Create a Comparator that will compare arrays by comparing a specified set of columns within the
   * array.
   * 
   * @param <T>
   *          The Comparable<T> class that the arrays are going to be composed of. You obviously
   *          can't sort by arrays of things that are not comparable...
   * @param columns
   *          The columns that are to be compared. The first one will take priority and the second
   *          will only be evaluated if the first is not equal and so forth.
   * @return The appropriate comparator.
   */
  public static <T extends Comparable<T>> Comparator<T[]> createComparator(final int... columns)
  {
    return new Comparator<T[]>() {

      @Override
      public int compare(T[] o1,
                         T[] o2)
      {
        for (int column : columns) {
          int v = o1[column].compareTo(o2[column]);
          if (v != 0) {
            return v;
          }
        }
        return 0;
      }
    };
  }

  /**
   * check that none of the values in source are null and throw a NullPointerException if any of
   * them are.
   * 
   * @return source, if there are no null values.
   * @throws NullPointerException
   *           if either source is null, or if any of the values in source are null
   */
  public static <T> T[] checkNotNull(T[] source,
                                     String nullError)
  {
    final int l = source.length;
    for (int i = 0; i < l; i++) {
      if (source[i] == null) {
        throw new NullPointerException(String.format("%s: %s @ %s",
                                                     nullError,
                                                     Arrays.toString(source),
                                                     Integer.valueOf(i)));
      }
    }
    return source;
  }
}
