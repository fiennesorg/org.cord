package org.cord.util;

/**
 * Base class for the RuntimeExceptions that are thrown by errors manipulating the Named interface
 * code.
 */
public class NamedException
  extends RuntimeException
{
  /**
   * 
   */
  private static final long serialVersionUID = 8155848419868951025L;

  private final Named _source;

  public NamedException(Named source,
                        String details)
  {
    super(makeDetailString(source, details));
    _source = source;
  }

  private final static String makeDetailString(Named source,
                                               String details)
  {
    return source + "(" + details + ")";
  }

  public final Named getSource()
  {
    return _source;
  }
}
