package org.cord.util;

import java.util.Collection;

/**
 * Filtered view of Settable with all keys being automatically prepended with the namespace value.
 * 
 * @author alex
 */
public class NamespaceSettable
  extends NamespaceGettable
  implements Settable
{
  private final Settable __source;

  private static Settable calculateSource(Settable source)
  {
    if (source instanceof NamespaceSettable) {
      NamespaceSettable nsSource = (NamespaceSettable) source;
      return nsSource.getWrappedGettable();
    }
    return source;
  }

  public NamespaceSettable(NamespacedNameCache nameCache,
                           Settable source)
  {
    super(nameCache,
          source);
    __source = calculateSource(source);
  }

  public NamespaceSettable(NamespacedNameCacheManager manager,
                           Settable source,
                           String namespace)
  {
    super(manager,
          source,
          namespace);
    __source = calculateSource(source);
  }

  @Override
  public final Settable getWrappedGettable()
  {
    return __source;
  }

  @Override
  public Object set(String key,
                    Object value)
      throws SettableException
  {
    return __source.set(buildKey(key), value);
  }

  @Override
  public Collection<String> getSettableKeys()
  {
    return removeNamespace(__source.getSettableKeys());
  }

  @Override
  public ValueType getValueType(String key)
  {
    return __source.getValueType(buildKey(key));
  }

  @Override
  public String toString()
  {
    return "NamespaceSettable(" + getNamespace() + ", " + __source + ")";
  }

  @Override
  public Object set(TypedKey<?> typedKey,
                    Object value)
      throws SettableException
  {
    return set(typedKey.getKeyName(), value);
  }
}
