package org.cord.util;

import java.io.PrintStream;

/**
 * Class to hold static methods for doing useful things with threads.
 */
public final class ThreadUtil
{
  public static void dumpThreadStatus(PrintStream out)
  {
    Thread currentThread = Thread.currentThread();
    out.println("Current thread:" + currentThread);
    ThreadGroup currentThreadGroup = currentThread.getThreadGroup();
    out.println("Current thread group:" + currentThreadGroup);
    Thread[] threads = new Thread[100];
    int count = currentThreadGroup.enumerate(threads, true);
    for (int i = 0; i < count; i++) {
      out.println("--" + threads[i]);
    }
  }
}
