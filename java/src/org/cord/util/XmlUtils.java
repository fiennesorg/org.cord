package org.cord.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.io.Closer;

public class XmlUtils
{
  public static String getTextElement(Element parentElement,
                                      String tagName)
  {
    Node tag = parentElement.getElementsByTagName(tagName).item(0);
    Node cdata = tag.getChildNodes().item(0);
    return cdata == null ? "" : ((CharacterData) cdata).getData();
  }

  public static String getOptTextElement(Element parentElement,
                                         String tagName)
  {
    List<Element> children = getElementsByTagName(parentElement, tagName);
    switch (children.size()) {
      case 0:
        return null;
      case 1:
        Node cdata = children.get(0).getChildNodes().item(0);
        return cdata == null ? "" : ((CharacterData) cdata).getData();
      default:
        throw new IllegalArgumentException(String.format("%s has %s named %s",
                                                         parentElement,
                                                         children,
                                                         tagName));
    }
  }

  /**
   * Get the value of an attribute from an element with non-defined attributes returning null. This
   * is as opposed to the default getAttribute value that pads undefined attributes to "" thereby
   * making it harder to determine whether they have been set or not.
   * 
   * @param element
   *          compulsory
   * @param name
   *          compulsory
   * @return The value contained in the attribute or null if the attribute is not defined.
   */
  public static String getOptAttribute(Element element,
                                       String name)
  {
    Preconditions.checkNotNull(element, "element");
    Preconditions.checkNotNull(name, "name");
    Attr attr = element.getAttributeNode(name);
    if (attr == null) {
      return null;
    }
    return attr.getValue();
  }

  /**
   * Get the attributes of this Element expressed as a java Map of names to values
   */
  public static Map<String, String> getAttributes(Element element)
  {
    Preconditions.checkNotNull(element, "element");
    NamedNodeMap nnm = element.getAttributes();
    int count = nnm.getLength();
    Map<String, String> attributes = new HashMap<String, String>(count);
    for (int i = 0; i < count; i++) {
      Node attribute = nnm.item(i);
      attributes.put(attribute.getNodeName(), attribute.getNodeValue());
    }
    return attributes;
  }

  /**
   * Convert name to a valid XML NAME token; the conversion is one-way, and the only requirement is
   * that no two unique names convert to the same value.
   */
  public static String encodeName(String name)
  {
    // This is actually a reversible encoding: letters, digits and '-' are
    // left alone, '_' is converted to '__', and all other characters are
    // represented as a four-digit hex value preceded by '_'. The entire
    // sequence is preceded by a single '_'.
    StringBuilder buf = new StringBuilder("_");
    for (int i = 0; i != name.length(); ++i) {
      char c = name.charAt(i);
      if ((c < 127) && (Character.isLetterOrDigit(c) || c == '-')) {
        buf.append(c);
      } else if (c == '_') {
        buf.append("__");
      } else {
        buf.append(String.format("_%04x", Integer.valueOf((int) c)));
      }
    }
    return buf.toString();
  }

  /**
   * Returns whether name is a valid XML NAME token, as defined by
   * http://www.w3.org/TR/2000/REC-xml-20001006#NT-Name with the added restrictions of:
   * <ul>
   * <li>CombiningChar, Extender and ':' are not included in the set of valid characters</li>
   * <li>Empty strings are not valid</li>
   * </ul>
   */
  public static boolean isValidName(String name)
  {
    boolean result = !name.isEmpty();
    if (result) {
      char c = name.charAt(0);
      result = Character.isLetter(c) || c == '_';
    }
    for (int i = 1; i != name.length() && result; ++i) {
      char c = name.charAt(i);
      result = result && (Character.isLetterOrDigit(c) || c == '_' || c == '-');
    }
    return result;
  }

  /**
   * Create an element of the given elementName, append it as a child to parentNode and then return
   * the element.
   * 
   * @return The created element. Never null.
   */
  public static Element addElement(Document document,
                                   Node parent,
                                   String elementName)
  {
    return (Element) parent.appendChild(document.createElement(elementName));
  }

  public static Element addElementNS(Document document,
                                     Node parent,
                                     String namespaceUri,
                                     String elementName)
  {
    return (Element) parent.appendChild(document.createElementNS(namespaceUri, elementName));
  }

  /**
   * Create an element that contains a text value with the toString representation of value, append
   * it to parentNode and return the element.
   * 
   * @return The element, or null if value was null and the element was not created.
   */
  public static Element addTextElement(Document document,
                                       Node parent,
                                       String elementName,
                                       Object value)
  {
    if (value != null) {
      Element textElement = addElement(document, parent, elementName);
      textElement.appendChild(document.createTextNode(value.toString()));
      return textElement;
    }
    return null;
  }

  /**
   * @return The created text element, or null if value was null and no element was created.
   */
  public static Element addTextElementNS(Document document,
                                         Node parent,
                                         String namespaceUri,
                                         String elementName,
                                         Object value)
  {
    if (value != null) {
      Element textElement = addElementNS(document, parent, namespaceUri, elementName);
      textElement.appendChild(document.createTextNode(value.toString()));
      return textElement;
    }
    return null;
  }

  /**
   * If value can be converted to a non-empty string then create an element that contains a text
   * value with the toString representation of value, append it to parentNode and return the
   * element.
   * 
   * @return The element, or null if value was null and the element was not created.
   */
  @Deprecated
  public static Element addNotEmptyTextElement(Document document,
                                               Node parent,
                                               String elementName,
                                               Object value)
  {
    if (StringUtils.notEmpty(StringUtils.toString(value))) {
      return addTextElement(document, parent, elementName, value);
    }
    return null;
  }

  public static Element addNotEmptyTextElementNS(Document document,
                                                 Node parent,
                                                 String namespaceUri,
                                                 String elementName,
                                                 Object value)
  {
    if (!Strings.isNullOrEmpty(StringUtils.toString(value))) {
      return addTextElementNS(document, parent, namespaceUri, elementName, value);
    }
    return null;
  }

  @Deprecated
  public static boolean setAttributeIfDefined(Element element,
                                              String name,
                                              Object value)
  {
    Assertions.notEmpty(name, "name");
    if (value != null) {
      element.setAttribute(name, value.toString());
      return true;
    }
    return false;
  }

  /**
   * If the value is not null then invoke toString() on it and set it as a named attribute on the
   * element.
   * 
   * @param element
   *          The compulsory element
   * @param namespaceUri
   *          The optional namespace URI
   * @param name
   *          The compulsory non-empty name of the attribute
   * @param value
   *          The optional value
   * @return true if an attribute was set, otherwise false
   */
  public static boolean setAttributeIfDefinedNS(Element element,
                                                String namespaceUri,
                                                String name,
                                                Object value)
  {
    Preconditions.checkNotNull(element, "element");
    Assertions.notEmpty(name, "name");
    if (value != null) {
      element.setAttributeNS(namespaceUri, name, value.toString());
      return true;
    }
    return false;
  }

  @Deprecated
  public static boolean setAttributeIfHasContents(Element element,
                                                  String name,
                                                  Object value)
  {
    String valueString = StringUtils.toString(value);
    if (StringUtils.notEmpty(valueString)) {
      return setAttributeIfDefined(element, name, valueString);
    }
    return false;
  }

  public static boolean setAttributeIfHasContentsNS(Element element,
                                                    String namespaceUri,
                                                    String name,
                                                    Object value)
  {
    String valueString = StringUtils.toString(value);
    if (!Strings.isNullOrEmpty(valueString)) {
      return setAttributeIfDefinedNS(element, namespaceUri, name, valueString);
    }
    return false;
  }

  /**
   * Is the given Node not null and an Element and got the given name?
   * 
   * @return true if all the criteria are satisfied otherwise false.
   */
  public static boolean isElementNamed(Node node,
                                       String tagName)
  {
    return (node != null && (node instanceof Element)
            && (((Element) node).getTagName().equals(tagName)));
  }

  /**
   * Get a unique child element of the specified parentElement with IllegalArgumentExceptions if
   * this is not possible for the given parentElement.
   * 
   * @param isCompulsory
   *          if true and the unique child is not found then throw an IllegalArgumentException
   * @return The requested child or null if it is not found and isCompulsory is false
   * @throws IllegalArgumentException
   *           if there is more than one child named childTagName or if there are no children named
   *           childTagName and isCompulsory is true.
   */
  public static Element getUniqueChild(Element parentElement,
                                       String childTagName,
                                       boolean isCompulsory)
  {
    Element match = null;
    NodeList children = parentElement.getChildNodes();
    for (int i = 0; i < children.getLength(); i++) {
      Node item = children.item(i);
      if (item instanceof Element) {
        Element potentialMatch = (Element) item;
        if (potentialMatch.getTagName().equals(childTagName)) {
          if (match == null) {
            match = potentialMatch;
          } else {
            throw new IllegalArgumentException(String.format("%s is not a single unique child of %s",
                                                             childTagName,
                                                             parentElement));
          }
        }
      }
    }
    if (match == null && isCompulsory) {
      throw new IllegalArgumentException(String.format("%s is not a child of %s",
                                                       childTagName,
                                                       parentElement));
    }
    return match;
  }

  /**
   * Get a List of all the Elements that are immediate children of the specific parent Element and
   * which have the given tagName.
   * 
   * @return The appropriate unmodifiable List which may be empty.
   */
  public static List<Element> getElementsByTagName(final Element parent,
                                                   final String tagName)
  {
    final NodeList children = parent.getChildNodes();
    final int childrenCount = children.getLength();
    if (childrenCount == 0) {
      return Collections.emptyList();
    }
    final List<Element> elements = new ArrayList<Element>();
    for (int i = 0; i < childrenCount; i++) {
      final Node node = children.item(i);
      if (isElementNamed(node, tagName)) {
        elements.add((Element) node);
      }
    }
    return Collections.unmodifiableList(elements);
  }

  public static List<Element> getChildElements(final Element parent)
  {
    final NodeList children = parent.getChildNodes();
    final int childrenCount = children.getLength();
    if (childrenCount == 0) {
      return Collections.emptyList();
    }
    final List<Element> elements = new ArrayList<Element>();
    for (int i = 0; i < childrenCount; i++) {
      final Node node = children.item(i);
      if (node instanceof Element) {
        elements.add((Element) node);
      }
    }
    return Collections.unmodifiableList(elements);
  }

  /**
   * Provide an unmodifiable List viewpoint of a NodeList thereby permitting you to use it with
   * other Collections.
   */
  public static List<Node> asList(final NodeList nodeList)
  {
    return new AbstractList<Node>() {
      @Override
      public Node get(int index)
      {
        return nodeList.item(index);
      }

      @Override
      public int size()
      {
        return nodeList.getLength();
      }
    };
  }

  /**
   * validate that element is defined and that it has the specified tagName, throwing a
   * RuntimeException if this is not the case.
   * 
   * @return The element that was passed in thereby permitting chaining
   */
  public static Element assertIsTagName(Element element,
                                        String tagName)
  {
    Preconditions.checkNotNull(element, "element");
    Assertions.isTrue(element.getTagName().equals(tagName),
                      "<%s> is not a <%s> element",
                      element.getTagName(),
                      tagName);
    return element;
  }

  /**
   * Parse the given inputStream and then close it. Note that it will be closed regardless of
   * whether the parsing succeeded or not.
   */
  public static Document parse(DocumentBuilder documentBuilder,
                               InputStream inputStream)
      throws SAXException, IOException
  {
    Closer closer = Closer.create();
    closer.register(inputStream);
    try {
      return documentBuilder.parse(inputStream);
    } finally {
      closer.close();
    }
  }

  /**
   * Convert a Document into it's String implementation with optional indentation.
   */
  public static String toString(Document document,
                                boolean isIndented)
  {
    try {
      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer transformer = tf.newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      transformer.setOutputProperty(OutputKeys.INDENT, isIndented ? "yes" : "no");
      StringWriter writer = new StringWriter();
      transformer.transform(new DOMSource(document), new StreamResult(writer));
      return writer.toString();
    } catch (TransformerException e) {
      throw new LogicException(e);
    }
  }
}
