package org.cord.util;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.TransientRecord;

public class StringValueType
  extends ValueType
{
  public static final String TYPE = "String";

  private final StringValidator __stringValidator;

  private final FieldValueSupplier<?> __defaultValue;

  public StringValueType(String key,
                         String englishKey,
                         String summary,
                         StringValidator stringValidator,
                         FieldValueSupplier<?> defaultValue,
                         boolean isCompulsory)
  {
    super(TYPE,
          key,
          englishKey,
          summary,
          isCompulsory);
    __stringValidator = stringValidator;
    __defaultValue = defaultValue;
  }

  @Override
  public boolean isValid(Object value)
  {
    if (!(value instanceof String)) {
      return false;
    }
    return __stringValidator.isValid((String) value);
  }

  @Override
  public Object getDefaultValue(TransientRecord record)
  {
    return __defaultValue.supplyValue(record);
  }
}
