package org.cord.util;

import java.io.File;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * Provided a filtered view of a Gettable where all supplied keys are automatically prepended with a
 * constant namepsace value before accessing the Gettable. This is useful in situations when you
 * wish to next sets of values inside a single Gettable object while maintaining seperation from the
 * sets without the processing method using the Gettable being aware of the fact that this has
 * happened, or what the naming convention is that is being used (all keys flattened into Strings in
 * this context so source Gettables that use non-Strings as keys will not function correctly).
 * 
 * @author alex
 */
public class NamespaceGettable
  implements Gettable, GettableWrapper
{
  private final NamespacedNameCache __cache;

  private final Gettable __source;

  private final String __namespace;

  /**
   * @param nameCache
   *          The cache of constructed names that include the namespace.
   */
  public NamespaceGettable(NamespacedNameCache nameCache,
                           Gettable source)
  {
    __cache = nameCache;
    __source = source;
    __namespace = __cache.getNamespace();
  }

  /**
   * @param manager
   *          The optional manager that manages caching of the generated names to avoid rebuilding
   *          them repeatedly.
   */
  public NamespaceGettable(NamespacedNameCacheManager manager,
                           Gettable source,
                           String namespace)
  {
    if (source instanceof NamespaceGettable) {
      NamespaceGettable ngSource = (NamespaceGettable) source;
      __source = ngSource.getWrappedGettable();
      __namespace = ngSource.getNamespace() + namespace;
    } else {
      __source = source;
      __namespace = namespace;
    }
    __cache = manager == null ? null : manager.getPrefixNamespacedNameCache(__namespace);
  }

  public NamespaceGettable(Gettable source,
                           String prefixNamespace)
  {
    this(null,
         source,
         prefixNamespace);
  }

  public final String getNamespace()
  {
    return __namespace;
  }

  @Override
  public Gettable getWrappedGettable()
  {
    return __source;
  }

  public String buildKey(Object key)
  {
    if (__cache == null) {
      return __namespace + key;
    }
    return __cache.getNamespacedName(key.toString());
  }

  @Override
  public Object get(Object key)
  {
    return __source.get(buildKey(key));
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __source.containsKey(buildKey(key));
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    return __source.getBoolean(buildKey(key));
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    return __source.getValues(buildKey(key));
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    return __source.getPaddedValues(buildKey(key));
  }

  protected Collection<String> removeNamespace(Collection<String> namespacedKeys)
  {
    List<String> keys = Lists.newArrayList();
    for (String key : namespacedKeys) {
      String stringKey = StringUtils.toString(key);
      if (stringKey.startsWith(__namespace)) {
        keys.add(stringKey.substring(__namespace.length()));
      }
    }
    return keys;
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    return removeNamespace(__source.getPublicKeys());
  }

  @Override
  public String getString(Object key)
  {
    return __source.getString(buildKey(key));
  }

  @Override
  public File getFile(Object key)
  {
    return __source.getFile(buildKey(key));
  }

  @Override
  public String toString()
  {
    return "NamespaceGettable(" + __namespace + ", " + __source + ")";
  }
}
