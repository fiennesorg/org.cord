package org.cord.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * A Map of keys to StringTransforms with a variety of utility methods for feeding in values that
 * want to be transformed.
 * 
 * @author alex
 */
public class StringTransformMap
{
  private Map<Object, StringTransform> __stringTransforms = new HashMap<Object, StringTransform>();

  public StringTransformMap()
  {
  }

  public StringTransform registerTransform(Object key,
                                           StringTransform transform)
  {
    Preconditions.checkNotNull(key, "key");
    Preconditions.checkNotNull(transform, "transform");
    return __stringTransforms.put(key, transform);
  }

  public StringTransform getStringTransform(Object key)
  {
    return __stringTransforms.get(key);
  }

  // TODO: StringTransformMap - determine whether or not we should be flatting keys out into String
  public String transformString(Object key,
                                String source)
  {
    StringTransform transform = getStringTransform(key);
    return transform == null ? source : transform.transform(source);
  }

  public Object transformObject(Object key,
                                Object source)
  {
    return source instanceof String ? transformString(key, StringUtils.toString(source)) : source;
  }

  /**
   * Pass each item that is a String in the collection through the StringTransform associated with
   * key.
   */
  public Collection<?> transformCollection(Object key,
                                           Collection<?> collection)
  {
    if (collection == null) {
      return null;
    }
    StringTransform transform = getStringTransform(key);
    if (transform == null || collection.size() == 0) {
      return collection;
    }
    List<Object> list = new ArrayList<Object>(collection.size());
    for (Iterator<?> i = collection.iterator(); i.hasNext();) {
      Object source = i.next();
      list.add(source instanceof String
          ? transform.transform(StringUtils.toString(source))
          : source);
    }
    return list;
  }

  public int size()
  {
    return __stringTransforms.size();
  }
}
