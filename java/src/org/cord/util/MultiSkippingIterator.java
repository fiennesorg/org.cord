package org.cord.util;

import java.util.Iterator;
import java.util.Set;

/**
 * Wrapper around an Iterator that skips any element in the iteration that is either included or
 * excluding in a particular Set of test cases.
 */
public class MultiSkippingIterator<T>
  extends SkippingIterator<T>
{
  private final Set<T> _skipMe;

  private final boolean _isInclusiveSkip;

  /**
   * @param iterator
   *          The source iterator
   * @param skipMe
   *          The Set that is containing the objects to either include or exclude. Please note that
   *          changing the contents of this Set while iterating across the source iterator may
   *          result in undefined behaviour.
   * @param isInclusiveSkip
   *          if true then only Objects inside skipMe will be skipped, if false then only Objects
   *          inside skipMe will be accepted.
   */
  public MultiSkippingIterator(Iterator<T> iterator,
                               Set<T> skipMe,
                               boolean isInclusiveSkip)
  {
    super(iterator);
    _skipMe = skipMe;
    _isInclusiveSkip = isInclusiveSkip;
  }

  @Override
  protected boolean shouldSkip(T object)
  {
    if (_skipMe == null) {
      return false;
    }
    return _isInclusiveSkip ? _skipMe.contains(object) : !_skipMe.contains(object);
  }
}
