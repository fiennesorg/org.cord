package org.cord.util;

import org.cord.mirror.Viewpoint;

public interface TypedGettable
  extends SimpleGettable
{
  public <T> T get(TypedKey<T> key);

  public <T> T get(TypedKey<T> key,
                   Viewpoint viewpoint);

  /**
   * Does this contain a key that is the same as the key contained withing TypedKey?
   */
  public boolean containsKey(TypedKey<?> key);

}
