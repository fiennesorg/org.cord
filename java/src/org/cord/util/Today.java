package org.cord.util;

import org.cord.util.DateQuantiser.Granularity;

/**
 * Singleton class that provides a lightweight interface to the Date at midnight this morning which
 * is useful for ensuring that caches which should be updated once per day don't get confused by
 * lots of times during the day.
 * 
 * @author alex
 */
public final class Today
  extends QuantisedCurrentTime
  implements SingletonShutdownable
{
  private static Today _instance = new Today();

  public static Today getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private Today()
  {
    super(Granularity.DAY);
    SingletonShutdown.registerSingleton(this);
  }
}
