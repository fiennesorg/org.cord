package org.cord.util;

import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.cord.node.NodeRequest;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.oreilly.servlet.MultipartRequest;

@Deprecated
public class NestedMaps
{
  /**
   * Copy all the values from defaults that do not have existing values in target including deep
   * traversal of contained Maps.
   */
  @SuppressWarnings("unchecked")
  public static void addDefaultValues(Map<Object, Object> target,
                                      Map<Object, Object> defaults)
  {
    for (Map.Entry<Object, Object> e : defaults.entrySet()) {
      if (e.getValue() instanceof Map<?, ?>) {
        Map<Object, Object> subtarget = createSubMap(target, e.getKey());
        addDefaultValues(subtarget, (Map<Object, Object>) e.getValue());
      } else {
        if (!target.containsKey(e.getKey())) {
          target.put(e.getKey(), e.getValue());
        }
      }
    }
  }

  /**
   * Convert the parameters contained within the HttpServletRequest into a Map of String to either
   * null, String or List of String depending on the duplicity of the defined values. To be honest,
   * I'm not sure whether or not you would ever get a null value defined, but one has to code
   * against these eventualities...
   */
  @SuppressWarnings("unchecked")
  public static Map<Object, Object> asLiteralMap(HttpServletRequest request)
  {
    Map<Object, Object> map = Maps.newHashMap();
    Map<String, String[]> params = request.getParameterMap();
    for (Map.Entry<String, String[]> e : params.entrySet()) {
      String[] values = e.getValue();
      if (values == null) {
        map.put(e.getKey(), null);
      } else if (values.length == 1) {
        map.put(e.getKey(), values[0]);
      } else {
        map.put(e.getKey(), Lists.newArrayList(values));
      }
    }
    return map;
  }

  /**
   * Convert the parameters contained within the MultipartRequest into a Map of String to either
   * null, File, String or List of String.
   */
  @SuppressWarnings("unchecked")
  public static Map<Object, Object> asLiteralMap(MultipartRequest request)
  {
    Map<Object, Object> map = Maps.newHashMap();
    for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
      String k = e.nextElement();
      String[] values = request.getParameterValues(k);
      if (values == null) {
        map.put(k, null);
      } else if (values.length == 1) {
        map.put(k, values[0]);
      } else {
        map.put(k, Lists.newArrayList(values));
      }
    }
    for (Enumeration<String> e = request.getFileNames(); e.hasMoreElements();) {
      String k = e.nextElement();
      map.put(k, request.getFile(k));
    }
    return map;
  }

  /**
   * Create a new Map that contains the result of stripping out NodeRequest encodings for optional
   * and boolean fields from a literal representation of the incoming HttpServletRequest. The
   * resulting Map will only contain the actual data values which may be null if isDefined-name is
   * defined and which will be a Boolean if name-active is defined. From this point onwards the Map
   * can be treated as a perfectly normal Map and the encodings don't have to raise their head
   * again. Please not that this method doesn't process or expect to be passed Nested Maps and
   * should be done before the hierarchy gets appropriately expanded.
   */
  public static Map<Object, Object> parseNodeRequestEncodings(Map<Object, Object> literal)
  {
    Preconditions.checkNotNull(literal);
    Map<Object, Object> result = Maps.newHashMap();
    for (Map.Entry<Object, Object> e : literal.entrySet()) {
      final Object k = e.getKey();
      final Object v = e.getValue();
      if (k instanceof String) {
        final String s = (String) k;
        if (s.startsWith(NodeRequest.ISDEFINED_PREFIX)) {
          String tail = s.substring(NodeRequest.ISDEFINED_PREFIX.length());
          if (!result.containsKey(tail)) {
            result.put(tail, null);
          }
        } else if (s.endsWith(NodeRequest.CGI_BOOLEAN_ACTIVE)) {
          String head = s.substring(0, s.length() - NodeRequest.CGI_BOOLEAN_ACTIVE.length());
          Object rv = result.get(head);
          if (rv == null) {
            result.put(head, Boolean.FALSE);
          } else {
            result.put(head, NodeRequest.toBoolean(rv.toString()));
          }
        } else {
          Object rv = result.get(s);
          if (rv == null) {
            result.put(s, v);
          } else {
            result.put(s, MoreObjects.firstNonNull(NodeRequest.toBoolean(v.toString()), Boolean.FALSE));
          }
        }
      } else {
        result.put(k, v);
      }
    }
    return result;
  }

  /**
   * Copy the values held in an HttpSession into a Map interface.
   */
  public static Map<Object, Object> asMap(HttpSession session)
  {
    Preconditions.checkNotNull(session, "HttpSession");
    Map<Object, Object> map = Maps.newHashMap();
    for (Enumeration<?> i = session.getAttributeNames(); i.hasMoreElements();) {
      String key = (String) i.nextElement();
      map.put(key, session.getAttribute(key));
    }
    return map;
  }

  public static Map<Object, Object> parseFully(HttpServletRequest request,
                                               HttpSession session)
  {
    Map<Object, Object> map = asLiteralMap(request);
    map = parseNodeRequestEncodings(map);
    addDefaultValues(map, asMap(session));
    return map;
  }

  /**
   * Put a value into a Map with automatic expansion of nested values.
   * 
   * @param map
   *          The map to put the value into. Not null.
   * @param key
   *          The key. Not null. If this contains any instances of the separator then it will be
   *          broken up into nested Maps appropriately.
   * @param s
   *          The character that represents hierarchy in the nested maps.
   * @param value
   *          The value to place into the appropriate depth of Map. Maybe null.
   * @return The old value that key used to hold in the Map. Maybe null.
   */
  public static Object put(Map<Object, Object> map,
                           String key,
                           char s,
                           Object value)
  {
    Preconditions.checkNotNull(map, "map");
    Preconditions.checkNotNull(key, "String key");
    final int i = key.indexOf(s);
    switch (i) {
      case -1:
        return map.put(key, value);
      case 0:
        throw new IllegalArgumentException("Leading separators are not permitted in " + key);
      default:
        if (i + 1 == key.length()) {
          throw new IllegalArgumentException("Trailing separators are not permitted in " + key);
        }
        return put(createSubMap(map, key.substring(0, i)), key.substring(i + 1), s, value);
    }
  }

  /**
   * Get a value from a Map with automatic expansion of nested values.
   * 
   * @param map
   *          The map to get the value out of. Not null.
   * @param key
   *          The key. Not null. If this contains any instances of the separator then it will be
   *          broken up into nested Maps appropriately.
   * @param s
   *          The character that represents hierarchy in the nested maps.
   * @return The value contained in the Map or null if there is no value defined. Please note that
   *         null could be either because the deepest nested Map doesn't contain the deepest nested
   *         key, or because a higher level of nesting is not present. So get "a.b.c" would return
   *         null if there isn't a nested map named "a"
   */
  public static Object get(Map<Object, Object> map,
                           String key,
                           char s)
  {
    int i = key.indexOf(s);
    switch (i) {
      case -1:
        return map.get(key);
      case 0:
        throw new IllegalArgumentException("Leading separators are not permitted in " + key);
      default:
        if (i + 1 == key.length()) {
          throw new IllegalArgumentException("Trailing separators are not permitted in " + key);
        }
        Map<Object, Object> submap = getSubMap(map, key.substring(0, i));
        if (submap == null) {
          return null;
        }
        return get(submap, key.substring(i + 1), s);
    }
  }

  /**
   * Check to see if a key is defined in a Map with automatic expansion of nested values.
   * 
   * @param map
   *          The map to check for the key. Not null.
   * @param key
   *          The key. Not null. If this contains any instances of the separator then it will be
   *          broken up into nested Maps appropriately.
   * @param s
   *          The character that represents hierarchy in the nested maps.
   * @return True if there is a key defined, otherwise false. Note that false can mean an
   *         intermediate nested map is not defined.
   */
  public static boolean containsKey(Map<Object, Object> map,
                                    String key,
                                    char s)
  {
    Preconditions.checkNotNull(map, "map");
    Preconditions.checkNotNull(key, "String key");
    int i = key.indexOf(s);
    switch (i) {
      case -1:
        return map.containsKey(key);
      case 0:
        throw new IllegalArgumentException("Leading separators are not permitted in " + key);
      default:
        if (i + 1 == key.length()) {
          throw new IllegalArgumentException("Trailing separators are not permitted in " + key);
        }
        Map<Object, Object> submap = getSubMap(map, key.substring(0, i));
        if (submap == null) {
          return false;
        }
        return containsKey(submap, key.substring(i + 1), s);
    }
  }

  /**
   * Check to see if a submap already exists and return that otherwise create and insert a new one.
   * 
   * @return The appropriate submap
   * @throws ClassCastException
   *           if key is already defined in map but it isn't pointing to a Map.
   */
  public static Map<Object, Object> createSubMap(Map<Object, Object> map,
                                                 Object key)
  {
    Map<Object, Object> submap = getSubMap(map, key);
    if (submap == null) {
      submap = Maps.newHashMap();
      map.put(key, submap);
    }
    return submap;
  }

  /**
   * Get a nested submap out of a map.
   * 
   * @param map
   *          The containing map. Not null
   * @param key
   *          The key of the submap. No expansion or anything is done to this key.
   * @return The appropriate submap or null if key doesn't map to anything.
   * @throws ClassCastException
   *           if key is defined but isn't a Map
   */
  @SuppressWarnings("unchecked")
  public static Map<Object, Object> getSubMap(Map<Object, Object> map,
                                              Object key)
  {
    Preconditions.checkNotNull(key, "Object key");
    return ObjectUtil.castTo(map.get(key), Map.class);
  }

  // get a nested map from a map

  // flatten a nested map into a linear map
}
