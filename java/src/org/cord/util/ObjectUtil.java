package org.cord.util;

import java.util.Comparator;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/**
 * Utility methods for doing things to Objects
 * 
 * @author alex
 */
public class ObjectUtil
{
  /**
   * Attempt to cast the given Object into an instance of the requested class with an informative
   * error message if this is not possible.
   * 
   * @param obj
   *          The Object to convert. If null then null returned.
   * @param c
   *          The Class that obj is to be converted to. Not null.
   * @return obj as a T, or null if obj is null
   * @throws ClassCastException
   *           if it is not possible to make the conversion. The error message will be much more
   *           informative than just trying to cast directly and will detail what was trying to be
   *           cast and from what class to which class.
   */
  public static <T> T castTo(Object obj,
                             Class<T> c)
      throws ClassCastException
  {
    try {
      return c.cast(obj);
    } catch (ClassCastException ccEx) {
      ClassCastException wrapper =
          new ClassCastException(String.format("Cannot cast \"%s\" from %s into %s",
                                               obj,
                                               obj == null ? "null" : obj.getClass(),
                                               c));
      wrapper.initCause(ccEx);
      throw wrapper;
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> Class<T> castClass(Class<?> aClass)
  {
    return (Class<T>) aClass;
  }

  /**
   * Compare two values using the given Comparator with definable behaviour that does not invovle
   * the Comparator for when one or more of the values is null.
   * 
   * @param nullIsSmall
   *          If true then null is taken as coming before any defined value, otherwise it comes
   *          after the defined values.
   */
  public static <T> int compare(Comparator<T> comparator,
                                T t1,
                                T t2,
                                boolean nullIsSmall)
  {
    Integer n = compareAgainstNull(t1, t2, nullIsSmall);
    return n == null ? comparator.compare(t1, t2) : n.intValue();
  }

  /**
   * Compare two values using their natural ordering with definable behaviour that does not involve
   * the natural ordering for when one or more of the values is null.
   * 
   * @param nullIsSmall
   *          If true then null is taken as coming before any defined value, otherwise it comes
   *          after the defined values.
   */
  public static <T> int compare(Comparable<T> t1,
                                T t2,
                                boolean nullIsSmall)
  {
    Integer n = compareAgainstNull(t1, t2, nullIsSmall);
    return n == null ? t1.compareTo(t2) : n.intValue();
  }

  /**
   * Compare the two values against each other purely on the basis as to whether or not they are
   * null. Both null will return 0, either one being null will return -1 or 1 depending on the state
   * of nullIsSmall, neither being null will return null as the method has no knowledge of the
   * ordering.
   * 
   * @return The ordering or null if both t1 and t2 are defined and therefore no ordering can be
   *         deduced.
   */
  public static Integer compareAgainstNull(Object t1,
                                           Object t2,
                                           boolean nullIsSmall)
  {
    if (t1 == null) {
      if (t2 == null) {
        return ZERO;
      }
      return nullIsSmall ? MINUSONE : PLUSONE;
    }
    if (t2 == null) {
      return nullIsSmall ? PLUSONE : MINUSONE;
    }
    return null;
  }

  private static final Integer ZERO = Integer.valueOf(0);
  private static final Integer PLUSONE = Integer.valueOf(1);
  private static final Integer MINUSONE = Integer.valueOf(-1);

  /**
   * Compare two non-null objects against a variable number of Comparators until one of them gives a
   * non-equal answer.
   * 
   * @param t1
   *          Compulsory object to compare against t2
   * @param t2
   *          Compulsory object to compare against t1
   * @param comparators
   *          The comparators that are going to be invoked. null values are not supported.
   * @return 0 if all the comparators deem t1 and t2 equal (or if there are no comparators),
   *         otherwise the result of the first comparator to express an opinion. Not that the
   *         remaining comparators will not get invoked.
   */
  public static <T> int compare(T t1,
                                T t2,
                                Comparator<T>... comparators)
  {
    Preconditions.checkNotNull(t1, "t1");
    Preconditions.checkNotNull(t2, "t2");
    final int l = comparators.length;
    for (int i = 0; i < l; i++) {
      int c = comparators[i].compare(t1, t2);
      if (c != 0) {
        return c;
      }
    }
    return 0;
  }

  /**
   * Compare two Objects by casting the first into Comparable and then using their natural ordering.
   * You'll get RuntimeExceptions if o1 is not castable into Comparable, or if the natural ordering
   * of o1 dislikes o2.
   */
  @SuppressWarnings("unchecked")
  public static int compare(Object o1,
                            Object o2)
  {
    return castTo(o1, Comparable.class).compareTo(o2);
  }

  /**
   * Compare two Objects using {@link String#compareToIgnoreCase(String)} if they are Strings
   * {@link #compare(Object, Object)} otherwise.
   */
  public static int compareIgnoreCase(Object o1,
                                      Object o2)
  {
    if (o1 instanceof String) {
      return ((String) o1).compareToIgnoreCase(castTo(o2, String.class));
    }
    return compare(o1, o2);
  }

  /**
   * Compare an {@link Optional} wrapper around an object with a conventional nullable Object. This
   * saves us the bother of creating a temporary wrapper Optional around the nullable object.
   */
  public static boolean equals(Optional<?> optional,
                               Object nullable)
  {
    if (optional.isPresent()) {
      return optional.get().equals(nullable);
    }
    return nullable == null;
  }

  public static int hashCode(int a1)
  {
    return a1;
  }

  public static int hashCode(int a1,
                             int a2)
  {
    return (31 + a1) * 31 + a2;
  }

  public static int hashCode(int a1,
                             int a2,
                             int a3)
  {
    return ((31 + a1) * 31 + a2) * 31 + a3;
  }

  public static int hashCode(int... a)
  {
    int result = 1;
    for (int element : a) {
      result = 31 * result + element;
    }
    return result;
  }

}
