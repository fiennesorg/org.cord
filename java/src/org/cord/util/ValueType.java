package org.cord.util;

import org.cord.mirror.TransientRecord;

/**
 * ValueType is the abstract base class of objects that are capable of describing what sort of value
 * is expected in a given situation.
 * 
 * @author alex
 */
public abstract class ValueType
  extends EnglishNamedImpl
{
  private final String __type;

  private final String __summary;

  private final boolean __isCompulsory;

  /**
   * @param type
   *          The type of value that this is as defined by the subclasses
   * @param key
   *          The key that this ValueType is bound to - this will be returned by getName()
   * @param englishKey
   *          The human readable version for the key - this will be returned by getEnglishName()
   * @param summary
   *          Optional summary of the purpose of the value - null if not defined.
   * @see Named#getName
   */
  public ValueType(String type,
                   String key,
                   String englishKey,
                   String summary,
                   boolean isCompulsory)
  {
    super(key,
          englishKey);
    __type = type;
    __summary = summary;
    __isCompulsory = isCompulsory;
  }

  public final String getType()
  {
    return __type;
  }

  public final String getSummary()
  {
    return __summary;
  }

  public abstract boolean isValid(Object value);

  public abstract Object getDefaultValue(TransientRecord record);

  public boolean isCompulsory()
  {
    return __isCompulsory;
  }
}
