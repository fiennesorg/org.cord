package org.cord.util;

public class SettableException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 3347294231183762118L;

  private final Settable __settable;

  private final Object __key;

  private final Object __value;

  public SettableException(String message,
                           Settable settable,
                           Object key,
                           Object value,
                           Throwable nestedCause)
  {
    super(message,
          nestedCause);
    __settable = settable;
    __key = key;
    __value = value;
  }

  public final Settable getSettable()
  {
    return __settable;
  }

  public final Object getKey()
  {
    return __key;
  }

  public final Object getValue()
  {
    return __value;
  }
}
