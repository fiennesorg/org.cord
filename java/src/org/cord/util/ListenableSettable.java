package org.cord.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.common.base.Preconditions;

/**
 * Wrapper class that encapsulates a Settable and fires off a message to any SettableListeners that
 * are registered when the values are updated.
 * 
 * @see SettableListener
 */
public class ListenableSettable
  extends ForwardingGettable
  implements Settable
{
  private final Settable __wrappedSettable;

  private final Set<SettableListener> __settableListeners = new HashSet<SettableListener>();

  public ListenableSettable(Settable wrappedSettable)
  {
    super(wrappedSettable);
    __wrappedSettable = Preconditions.checkNotNull(wrappedSettable, "wrappedSettable");
  }

  public void addSettableListener(SettableListener listener)
  {
    synchronized (__settableListeners) {
      if (listener != null && !__settableListeners.contains(listener)) {
        __settableListeners.add(listener);
      }
    }
  }

  /**
   * Static utility method to check whether a given gettable is actually a ListenableSettable and if
   * so then registering a given listener for settable update notification. This is designed for
   * situations when a class is utilising a Gettable data-source for configuration and is caching
   * the information in some manner once the gettable has been initially queried. If this is the
   * case then the class should implement SettableListener and it should invoke this method on the
   * Gettable that has been passed. Once this has been done, then the class will the get notified of
   * updates to the parameters if and when they occur thereby enabling action to be taken to
   * invalidate the internal cached data.
   */
  public static void addSettableListener(Gettable gettable,
                                         SettableListener listener)
  {
    if (gettable != null && gettable instanceof ListenableSettable) {
      ((ListenableSettable) gettable).addSettableListener(listener);
    }
  }

  /**
   * Invoke the equivalent method on the wrapped Settable object and if no RuntimeExceptions are
   * generated, then the SettableListeners are notified of the change of state. The order of the
   * notification is undefined. If any of the listeners generate a RuntimeException then the
   * notification loop is terminated and the remaining listeners do not receive a message.
   */
  @Override
  public Object set(String key,
                    Object value)
      throws SettableException
  {
    Object previousValue = __wrappedSettable.set(key, value);
    Iterator<SettableListener> listeners = __settableListeners.iterator();
    while (listeners.hasNext()) {
      listeners.next().settableUpdate(__wrappedSettable, key, value, previousValue);
    }
    return previousValue;
  }

  @Override
  public Collection<String> getSettableKeys()
  {
    return __wrappedSettable.getSettableKeys();
  }

  @Override
  public ValueType getValueType(String key)
  {
    return __wrappedSettable.getValueType(key);
  }

  @Override
  public Object set(TypedKey<?> typedKey,
                    Object value)
      throws SettableException
  {
    return set(typedKey.getKeyName(), value);
  }

}
