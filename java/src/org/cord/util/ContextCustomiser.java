package org.cord.util;

import org.webmacro.Context;

/**
 * Interface that describe Objects that are capable of appending additional information to an
 * existing WebMacro Context Object.
 */
public interface ContextCustomiser
{
  /**
   * Add in any information that is required to the given Context Object.
   */
  public void customise(Context context);
}
