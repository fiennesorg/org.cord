package org.cord.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * SessionDomainExpander is a utility class that is responsible for widening the scope of
 * HttpSessions from single hosts to definable wild-card domains. This is useful in situations when
 * you wish to use multiple sub-domains or sub-hosts in the domain to map onto a single sessions
 * without having to re-authenticate on the multiple locations.
 */
public class SessionDomainExpander
{
  private final String _cookieName;

  private final String _domain;

  public SessionDomainExpander(String cookieName,
                               String domain)
  {
    _cookieName = cookieName;
    _domain = domain;
  }

  /**
   * Get a Cookie with the specified name from an HttpServletRequest.
   * 
   * @return The specified Cookie or null if the cookie isn't found.
   * @throws NullPointerException
   *           if request is null
   */
  public static Cookie getCookie(HttpServletRequest request,
                                 String name)
  {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        if (cookies[i].getName().equals(name)) {
          return cookies[i];
        }
      }
    }
    return null;
  }

  /**
   * Grab the session associated with the request, creating one if necessary and drop a Cookie into
   * the response containing the appropriate session id String with the specified domain wildcards.
   * 
   * @return The existing HttpSession associated with the request, or the newly created HttpSession
   *         if this was the first request.
   */
  public HttpSession expandDomain(HttpServletRequest request,
                                  HttpServletResponse response)
  {
    HttpSession session = request.getSession(true);
    if (session.getAttribute(_domain) == null) {
      Cookie domainSessionCookie = new Cookie(_cookieName, session.getId());
      domainSessionCookie.setVersion(0);
      domainSessionCookie.setMaxAge(-1);
      domainSessionCookie.setPath("/");
      domainSessionCookie.setDomain(_domain);
      response.addCookie(domainSessionCookie);
      session.setAttribute(_domain, _domain);
    }
    return session;
  }
}
