package org.cord.util;

import java.util.concurrent.ConcurrentHashMap;

public class Maps
{
  public static final int INITIALCAPACITY = 16;
  public static final float LOADFACTOR = 0.75f;
  public static final int CORES = 4;

  public static <K, V> ConcurrentHashMap<K, V> newConcurrentHashMap()
  {
    return new ConcurrentHashMap<K, V>(INITIALCAPACITY, LOADFACTOR, CORES);
  }

  public static <K, V> ConcurrentHashMap<K, V> newConcurrentHashMap(int initialCapacity)
  {
    return new ConcurrentHashMap<K, V>(initialCapacity, LOADFACTOR, CORES);
  }

  public static <K, V> ConcurrentHashMap<K, V> newConcurrentHashMap(int initialCapacity,
                                                                    float loadFactor)
  {
    return new ConcurrentHashMap<K, V>(initialCapacity, loadFactor, CORES);
  }

  public static <K, V> ConcurrentHashMap<K, V> newConcurrentHashMap(int initialCapacity,
                                                                    float loadFactor,
                                                                    int concurrencyLevel)
  {
    return new ConcurrentHashMap<K, V>(initialCapacity, loadFactor, concurrencyLevel);
  }
}
