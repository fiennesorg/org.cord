package org.cord.util;

import java.util.Collection;

import com.google.common.collect.ImmutableMap;

public class NamedUtils
{
  public static <T extends Named> ImmutableMap<String, T> getNamedMap(T... nameds)
  {
    ImmutableMap.Builder<String, T> builder = ImmutableMap.builder();
    for (T named : nameds) {
      builder.put(named.getName(), named);
    }
    return builder.build();
  }

  public static <T extends Named> ImmutableMap<String, T> getNamedMap(Collection<T> nameds)
  {
    ImmutableMap.Builder<String, T> builder = ImmutableMap.builder();
    for (T named : nameds) {
      builder.put(named.getName(), named);
    }
    return builder.build();
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap()
  {
    return ImmutableMap.of();
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(String n1,
                                                                      String en1)
  {
    return ImmutableMap.of(n1, of(n1, en1));
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(String n1,
                                                                      String en1,
                                                                      String n2,
                                                                      String en2)
  {
    return ImmutableMap.of(n1, of(n1, en1), n2, of(n2, en2));
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(String n1,
                                                                      String en1,
                                                                      String n2,
                                                                      String en2,
                                                                      String n3,
                                                                      String en3)
  {
    return ImmutableMap.of(n1, of(n1, en1), n2, of(n2, en2), n3, of(n3, en3));
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(String n1,
                                                                      String en1,
                                                                      String n2,
                                                                      String en2,
                                                                      String n3,
                                                                      String en3,
                                                                      String n4,
                                                                      String en4)
  {
    return ImmutableMap.of(n1, of(n1, en1), n2, of(n2, en2), n3, of(n3, en3), n4, of(n4, en4));
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(String n1,
                                                                      String en1,
                                                                      String n2,
                                                                      String en2,
                                                                      String n3,
                                                                      String en3,
                                                                      String n4,
                                                                      String en4,
                                                                      String n5,
                                                                      String en5)
  {
    return ImmutableMap.of(n1,
                           of(n1, en1),
                           n2,
                           of(n2, en2),
                           n3,
                           of(n3, en3),
                           n4,
                           of(n4, en4),
                           n5,
                           of(n5, en5));
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(Iterable<? extends EnglishNamed> englishNameds)
  {
    ImmutableMap.Builder<String, EnglishNamed> builder = ImmutableMap.builder();
    for (EnglishNamed englishNamed : englishNameds) {
      builder.put(englishNamed.getName(), englishNamed);
    }
    return builder.build();
  }

  public static ImmutableMap<String, EnglishNamed> getEnglishNamedMap(EnglishNamed... englishNameds)
  {
    ImmutableMap.Builder<String, EnglishNamed> builder = ImmutableMap.builder();
    for (EnglishNamed englishNamed : englishNameds) {
      builder.put(englishNamed.getName(), englishNamed);
    }
    return builder.build();
  }

  public static Named of(String name)
  {
    return new NamedImpl(name);
  }

  public static EnglishNamed of(String name,
                                String englishName)
  {
    return new EnglishNamedImpl(name, englishName);
  }
}
