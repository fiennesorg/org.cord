package org.cord.util;

import com.google.common.base.Preconditions;

public class TypedKey<V>
{
  public static <T> TypedKey<T> create(String keyName,
                                       Class<T> valueClass)
  {
    return new TypedKey<T>(keyName, valueClass);
  }

  private final String __keyName;
  private final Class<V> __valueClass;

  public TypedKey(String keyName,
                  Class<V> valueClass)
  {
    __keyName = Preconditions.checkNotNull(keyName, "name");
    __valueClass = Preconditions.checkNotNull(valueClass, "valueClass");
  }

  @Override
  public final String toString()
  {
    return __keyName;
  }

  public final String getKeyName()
  {
    return __keyName;
  }

  public final Class<V> getValueClass()
  {
    return __valueClass;
  }

  public TypedKey<V> copy()
  {
    return create(getKeyName(), getValueClass());
  }
}
