package org.cord.util;

import java.util.regex.Pattern;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Static utility class of lightweight tests against variables that may be used for sanity checking.
 * There are probably several items that should be migrated into other XxxUtils classes...
 * 
 * @see DebugConstants#DEBUG_FAILEDASSERTIONS
 */
public final class Assertions
{
  public static final boolean ASSERTIONS_ENABLED = true;

  /**
   * Check to see that the give value is greater than or equal to 0
   * 
   * @throws IllegalArgumentException
   *           if value is less than 0
   */
  public static int notNegative(int value,
                                String message,
                                Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (value < 0) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "%s is less than 0",
                                                                              Integer.valueOf(value))));
      }
    }
    return value;
  }

  /**
   * If the library has been compiled with DEBUG_FAILEDASSERTIONS then drop a stack trace of the
   * supplied RuntimeException to DEBUG_OUT (with a header of Assertions.PRINTSTACKTRACE) and then
   * return the exception. Clients can therefore perform "throw Assertions.printStackTrace(new
   * RuntimeException())" and if the client has a noisy library then the failed assertion will
   * automatically appear in the debug log regardless of whether someone transparently catches and
   * discards it or not. This should obviously only be used for things that you don't expect to
   * fail.
   * 
   * @return runtimeException
   */
  public static <T extends RuntimeException> T printStackTrace(T runtimeException)
  {
    if (DebugConstants.DEBUG_FAILEDASSERTIONS) {
      DebugConstants.DEBUG_OUT.println("Assertions.PRINTSTACKTRACE");
      runtimeException.printStackTrace(DebugConstants.DEBUG_OUT);
    }
    return runtimeException;
  }

  /**
   * Filter out all Strings that are null or of length 0 with an IllegalArgumentException with the
   * user supplied message.
   * 
   * @param string
   *          The String to check.
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   * @throws IllegalArgumentException
   *           If string is null or of length zero.
   * @return the original string.
   */
  public static String notEmpty(String string,
                                String message,
                                Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (Strings.isNullOrEmpty(string)) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "\"%s\" is either null or has zero length",
                                                                              string)));
      }
    }
    return string;
  }

  /**
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   * @throws IllegalStateException
   *           If flag is not true
   */
  public static void isTrue(boolean flag,
                            String message,
                            Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (!flag) {
        throw printStackTrace(new IllegalStateException(StringUtils.format(message,
                                                                           messageParams,
                                                                           "flag is false")));
      }
    }
  }

  /**
   * Check that a given number is within a specified range and throw an IllegalArgumentException if
   * it is not.
   * 
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   * @throws IllegalArgumentException
   *           if value doesn't lie between min and max inclusive
   */
  public static void withinRange(int value,
                                 int min,
                                 int max,
                                 String message,
                                 Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (value < min || value > max) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "%s does not lie between %s and %s",
                                                                              Integer.valueOf(value),
                                                                              Integer.valueOf(min),
                                                                              Integer.valueOf(max))));
      }
    }
  }

  /**
   * Check that obj1 and obj2 are equal and throw an IllegalArgumentException if not. Both values as
   * null will pass.
   * 
   * @param obj1
   *          Optional value
   * @param obj2
   *          Optional value
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   */
  public static void isEqual(Object obj1,
                             Object obj2,
                             String message,
                             Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (!Objects.equal(obj1, obj2)) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "%s does not equal %s",
                                                                              obj1,
                                                                              obj2)));
      }
    }
  }

  /**
   * Check that obj1 and obj2 are not equal to each other.
   * 
   * @param obj1
   *          optional value
   * @param obj2
   *          optional value
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   */
  public static void notEqual(Object obj1,
                              Object obj2,
                              String message,
                              Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      if (Objects.equal(obj1, obj2)) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "%s equals %s",
                                                                              obj1,
                                                                              obj2)));
      }
    }
  }

  /**
   * Check that a given string matches a regex Pattern
   * 
   * @param string
   *          The string that must match the given regex
   * @param pattern
   *          The compulsory compiled Pattern to match against.
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   */
  public static void matchesRegex(String string,
                                  Pattern pattern,
                                  String message,
                                  Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      Preconditions.checkNotNull(pattern, "Cannot invoke matchesPattern against a null Pattern");
      if (string == null || !pattern.matcher(string).matches()) {
        throw printStackTrace(new IllegalArgumentException(StringUtils.format(message,
                                                                              messageParams,
                                                                              "\"%s\" does not match \"%s\"",
                                                                              string,
                                                                              pattern)));
      }
    }
  }

  /**
   * Compile a given regex and then check that a given string matches it. This is less effecient
   * than caching the Pattern that you want to utilise and passing it in directly especially if you
   * are going to invoke the method a lot.
   * 
   * @param string
   *          The string that must match the given regex
   * @param regex
   *          The compulsory compiled Pattern to match against.
   * @param message
   *          optional formatted error message
   * @param messageParams
   *          optional params for the formatted error message
   * @see #matchesRegex(String, Pattern, String, Object[])
   */
  public static void matchesRegex(String string,
                                  String regex,
                                  String message,
                                  Object... messageParams)
  {
    if (ASSERTIONS_ENABLED) {
      matchesRegex(string, Pattern.compile(regex), message, messageParams);
    }
  }
}
