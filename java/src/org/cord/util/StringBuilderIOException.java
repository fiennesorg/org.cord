package org.cord.util;

import java.io.IOException;

/**
 * A subclass of LogicException that signifies that the "impossible" act of a StringBuilder throwing
 * an IOException during its lifespan has taken place.
 * 
 * @author alex
 */
public class StringBuilderIOException
  extends LogicException
{
  private static final long serialVersionUID = 1L;

  public StringBuilderIOException(StringBuilder buf,
                                  IOException ioEx)
  {
    super(buf + " has thrown an IOException which should be impossible",
          ioEx);
  }
}
