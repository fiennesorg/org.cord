package org.cord.util;

/**
 * An Object that is capable of turning a String into another String
 * 
 * @author alex
 */
public interface StringTransform
{
  /**
   * Transform the source into the appropriate form.
   * 
   * @param source
   *          The String to be transformed
   * @return The transformed string - null is permissable
   */
  public String transform(String source);
}
