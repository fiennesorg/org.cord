package org.cord.util;

import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * A wrapper around a Map that presents a Gettable
 * 
 * @author alex
 */
public class GettableMapWrapper
  extends AbstractGettable
{
  private final Map<String, ?> __map;

  /**
   * @param map
   *          The backend data source for this Gettable. Must not be null
   */
  public GettableMapWrapper(Map<String, ?> map)
  {
    Preconditions.checkNotNull(map, "map");
    __map = map;
  }

  @Override
  public Object get(Object key)
  {
    return __map.get(key.toString());
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __map.containsKey(key);
  }
}
