package org.cord.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Container that contains a number of EnglishNamed choices, including a single default choice.
 * 
 * @author alex
 */
public class EnglishNamedOptions
{
  private List<EnglishNamed> __options = new ArrayList<EnglishNamed>();

  private List<EnglishNamed> __roOptions = Collections.unmodifiableList(__options);

  private boolean _isLocked = false;

  private EnglishNamed _defaultOption;

  public EnglishNamedOptions()
  {
  }

  public EnglishNamedOptions(EnglishNamed option)
  {
    this();
    addOption(option, true);
  }

  public int size()
  {
    return __options.size();
  }

  public EnglishNamed getDefaultOption()
  {
    return _defaultOption;
  }

  public void addOption(EnglishNamed option,
                        boolean isDefault)
  {
    if (_isLocked) {
      throw new IllegalStateException(this + " is locked");
    }
    Preconditions.checkNotNull(option, "option");
    __options.add(option);
    if (isDefault || _defaultOption == null) {
      _defaultOption = option;
    }
  }

  /**
   * Add in a number of EnglishNamed options as non-default values to this container.
   */
  public void addOptions(Iterable<EnglishNamed> options)
  {
    for (EnglishNamed option : options) {
      addOption(option, false);
    }
  }

  public Iterator<EnglishNamed> getOptions()
  {
    return __roOptions.iterator();
  }

  public void lock()
  {
    _isLocked = true;
  }

  public boolean isLocked()
  {
    return _isLocked;
  }
}
