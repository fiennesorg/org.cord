package org.cord.util;

import java.util.Arrays;

import com.google.common.base.Preconditions;

/**
 * StringValidator that deems the target String to be valid if it is found inside a supplied array
 * of Strings.
 */
public class ArrayContentsValidator
  extends NamedImpl
  implements StringValidator
{
  /**
   * "ArrayContentsValidator"
   */
  public final static String NAME = "ArrayContentsValidator";

  private final String[] __array;
  private final boolean __includeArrayInErrors;

  public ArrayContentsValidator(String[] array,
                                boolean includeArrayInErrors)
  {
    super(NAME);
    Preconditions.checkNotNull(array, "array");
    __array = array;
    __includeArrayInErrors = includeArrayInErrors;
  }

  @Override
  public boolean isValid(String string)
  {
    return ArrayUtils.indexOf(string, __array) != -1;
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return null;
    }
    String error = null;
    if (__includeArrayInErrors) {
      error = String.format("\"%s\" not found in %s", string, Arrays.toString(__array));
    }
    error = String.format("\"%s\" not found in array", string);
    throw new StringValidatorException(error, string);
  }
}
