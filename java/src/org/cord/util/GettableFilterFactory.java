package org.cord.util;

/**
 * Interface that describes Objects that are capable of producing a GettableFilter that represents
 * the requirements that they have from a given Gettable source. It is not specified whether or not
 * the GettableFilter that is returned is to be constant or not. This may then be utilised to
 * construct a set of GettableFilterFactories that can then be utilised to construct compound GUIs.
 */
public interface GettableFilterFactory
{
  /**
   * Get the GettableFilter that currently represents that requirements of this
   * GettableFilterFactory.
   * 
   * @return The appropriate GettableFilterFactory, or null if no requirements exist.
   */
  public GettableFilter getGettableFilter();
}
