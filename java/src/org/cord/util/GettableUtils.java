package org.cord.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Class of static utility methods for manipulating Gettable Objects.
 */
public final class GettableUtils
{
  private static final GettableUtils __instance = new GettableUtils();

  public static final GettableUtils getInstance()
  {
    return __instance;
  }

  private GettableUtils()
  {
  }

  public static Object toKey(Object obj)
  {
    if (obj instanceof TypedKey<?>) {
      return ((TypedKey<?>) obj).getKeyName();
    }
    return obj;
  }

  public static Object get(GettableFactory source,
                           String name,
                           Object defaultValue)
  {
    return (source == null ? defaultValue : get(source.getGettable(), name, defaultValue));
  }

  public static Object delegatingGet(Gettable source,
                                     Object baseName,
                                     Object instanceName,
                                     Object variableName)
  {
    Object result = source.get(baseName + "." + instanceName + "." + variableName);
    if (result != null) {
      return result;
    }
    return source.get(baseName + "." + variableName);
  }

  public static Object compulsoryDelegatingGet(Gettable source,
                                               Object baseName,
                                               Object instanceName,
                                               Object variableName)
  {
    String fullInstanceName = baseName + "." + instanceName + "." + variableName;
    Object result = source.get(fullInstanceName);
    if (result != null) {
      return result;
    }
    String fullBaseName = baseName + "." + variableName;
    result = source.get(fullBaseName);
    if (result != null) {
      return result;
    }
    throw new NullPointerException("undefined(" + fullInstanceName + "|" + fullBaseName + ")");
  }

  public static Object delegatingGet(Gettable source,
                                     Object baseName,
                                     Object instanceName,
                                     Object variableName,
                                     Object defaultValue)
  {
    Object result = delegatingGet(source, baseName, instanceName, variableName);
    return result == null ? defaultValue : result;
  }

  public static String delegatingGet(Gettable source,
                                     Object baseName,
                                     Object instanceName,
                                     Object variableName,
                                     String defaultValue)
  {
    return StringUtils.toString(delegatingGet(source,
                                              baseName,
                                              instanceName,
                                              variableName,
                                              (Object) defaultValue));
  }

  public static boolean delegatingGet(Gettable source,
                                      Object baseName,
                                      Object instanceName,
                                      Object variableName,
                                      boolean defaultValue)
  {
    return Boolean.valueOf(delegatingGet(source,
                                         baseName,
                                         instanceName,
                                         variableName,
                                         Boolean.toString(defaultValue)))
                  .booleanValue();
  }

  public static int delegatingGet(Gettable source,
                                  Object baseName,
                                  Object instanceName,
                                  Object variableName,
                                  int defaultValue)
  {
    return NumberUtil.toInt(delegatingGet(source, baseName, instanceName, variableName),
                            defaultValue);
  }

  public static boolean get(Gettable source,
                            String name,
                            boolean defaultValue)
  {
    Object obj = get(source, name, Boolean.valueOf(defaultValue));
    if (obj == null) {
      return defaultValue;
    }
    if (obj instanceof Boolean) {
      return ((Boolean) obj).booleanValue();
    }
    return Boolean.valueOf(StringUtils.toString(obj)).booleanValue();
  }

  public static boolean get(GettableFactory source,
                            String name,
                            boolean defaultValue)
  {
    return (source == null ? defaultValue : get(source.getGettable(), name, defaultValue));
  }

  public static Object get(Gettable source,
                           String name,
                           Object defaultValue)
  {
    if (source == null) {
      return defaultValue;
    }
    Object value = source.get(name);
    return (value == null ? defaultValue : value);
  }

  /**
   * Get a named string from the given GettableFactory or return the defaultValue if the
   * GettableFactory doesn't contain a Gettable or if the contained Gettable doesn't contain the
   * key.
   */
  public static String get(GettableFactory source,
                           String name,
                           String defaultValue)
  {
    return source == null ? defaultValue : get(source.getGettable(), name, defaultValue);
  }

  public static String get(Gettable source,
                           String name,
                           String defaultValue)
  {
    return StringUtils.toString(get(source, name, (Object) defaultValue));
  }

  /**
   * Get an Object from a GettableFactory source and convert it into an int with a fallback default
   * int if this is not possible.
   */
  public static int get(GettableFactory source,
                        String name,
                        int defaultValue)
  {
    return (source == null ? defaultValue : getInt(source.getGettable(), name, defaultValue));
  }

  public static long get(GettableFactory source,
                         String name,
                         long defaultValue)
  {
    return (source == null ? defaultValue : getLong(source.getGettable(), name, defaultValue));
  }

  /**
   * Get an Object from a Gettable source and convert it into an int with a fallback default int if
   * this is not possible.
   */
  public static int getInt(Gettable source,
                           String name,
                           int defaultValue)
  {
    if (source == null) {
      return defaultValue;
    }
    Object value = source.get(name);
    if (value == null) {
      return defaultValue;
    }
    try {
      return Integer.parseInt(value.toString());
    } catch (NumberFormatException nfEx) {
      return defaultValue;
    }
  }

  public static OptionalInt getOptionalInt(Gettable source,
                                           String name)
  {
    if (source != null) {
      return OptionalInt.fromNullable(NumberUtil.toInteger(source.get(name), null));
    }
    return OptionalInt.absent();
  }

  public static Integer getInteger(Gettable source,
                                   String name,
                                   Integer defaultValue)
  {
    if (source == null) {
      return defaultValue;
    }
    return NumberUtil.toInteger(source.get(name), defaultValue, defaultValue);
  }

  public static long getLong(Gettable source,
                             String name,
                             long defaultValue)
  {
    if (source == null) {
      return defaultValue;
    }
    Object value = source.get(name);
    if (value == null) {
      return defaultValue;
    }
    try {
      return Long.parseLong(value.toString());
    } catch (NumberFormatException nfEx) {
      return defaultValue;
    }
  }

  /**
   * Get a String value from a Gettable source with optional obfuscating logging to a given output
   * stream.
   * 
   * @param source
   *          The Gettable that contains the value
   * @param key
   *          The key to the value
   * @param defaultValue
   *          The value to utilise in case of non-defintion of value
   * @param log
   *          The PrintStream for logging. If null, then no logging is performed.
   * @param requestor
   *          The Named object that is making the request. If not null then logging will be prefixed
   *          with "requestor.key=" as opposed to "key="
   * @param obfuscateValue
   *          If true then the value will always be represented in the log by "******" thereby
   *          hiding sensitive data.
   */
  public static Object get(Gettable source,
                           String key,
                           Object defaultValue,
                           PrintStream log,
                           Named requestor,
                           boolean obfuscateValue)
  {
    Object value = get(source, key, defaultValue);
    if (log != null) {
      log.println((requestor == null ? "" : requestor.getName() + '.') + key + "=\""
                  + (obfuscateValue ? "******" : value) + "\"");
    }
    return value;
  }

  /**
   * Take a named value from source and split it up according to some regular expression, thereby
   * permitting a set of keys on a given Gettable to be implemented as an internally defined value
   * on the Gettable itself. This is intended primarily for situations when there is sensitive data
   * contained inside a Gettable that is defined at Runtime and therefore the definition of the
   * Gettable should also include the information about which items are to be exposed.
   */
  public static Collection<String> getKeys(Gettable source,
                                           String keysKey,
                                           String splitRegex)
  {
    String keysValue = get(source, keysKey, null);
    if (keysValue == null) {
      return Collections.emptyList();
    }
    try {
      String[] keys = keysValue.split(splitRegex);
      List<String> result = Lists.newArrayList();
      for (String key : keys) {
        result.add(key);
      }
      return Collections.unmodifiableList(result);
    } catch (PatternSyntaxException badSplitRegexEx) {
      return Collections.emptyList();
    }
  }

  /**
   * Utility method to aid in the default implemenation of getValues(key) for Gettable items that
   * don't really support the concept of multi-valued keys.
   * 
   * @param value
   *          The value that is to be wrapped. May be null
   * @return Either a Collections.EMPTY_LIST or a SingleCollections, unless the value is actually a
   *         Collection in which case value is just passed through unchanged.
   */
  public static Collection<?> wrapValue(Object value)
  {
    if (value == null) {
      return null;
    }
    if (value instanceof Collection<?>) {
      return (Collection<?>) value;
    }
    return ImmutableList.of(value);
  }

  public static Boolean toBoolean(Object value)
  {
    if (value == null) {
      return null;
    }
    if (value instanceof Boolean) {
      return (Boolean) value;
    }
    return Boolean.valueOf(value.toString());
  }

  public static Boolean toBoolean(Object value,
                                  Boolean nullValue)
  {
    return MoreObjects.firstNonNull(toBoolean(value), nullValue);
  }

  /**
   * Convert an Object into a File by treating the string representation as the explicit path to the
   * File. If the Object is already a File then it is returned unchanged.
   * 
   * @return The File, or null if value is null. The File is not guaranteed to exist.
   */
  public static File toFile(Object value)
  {
    if (value == null) {
      return null;
    }
    if (value instanceof File) {
      return (File) value;
    }
    return new File(StringUtils.toString(value));
  }

  /**
   * Replace null Gettables with an EmptyGettable instance.
   */
  public static final Gettable padNull(Gettable gettable)
  {
    return gettable == null ? emptyGettable() : gettable;
  }

  public static void set(Settable settable,
                         Gettable gettable,
                         String... keys)
      throws SettableException
  {
    for (String key : keys) {
      settable.set(key, gettable.get(key));
    }
  }

  public static void set(Settable settable,
                         Gettable gettable,
                         TypedKey<?>... keys)
      throws SettableException
  {
    for (TypedKey<?> key : keys) {
      settable.set(key, gettable.get(key.getKeyName()));
    }
  }

  public static void set(Settable settable,
                         Gettable gettable,
                         Iterable<String> keys)
      throws SettableException
  {
    for (String key : keys) {
      settable.set(key, gettable.get(key));
    }
  }

  public static void debug(Gettable gettable,
                           Appendable out)
      throws IOException
  {
    if (gettable == null) {
      out.append("null");
      return;
    }
    out.append(gettable.toString())
       .append(", ")
       .append(gettable.getClass().toString())
       .append(", publicKeys{");
    Iterator<String> i = gettable.getPublicKeys().iterator();
    TreeSet<String> keys = new TreeSet<String>();
    while (i.hasNext()) {
      keys.add(StringUtils.toString(i.next()));
    }
    i = keys.iterator();
    while (i.hasNext()) {
      String key = i.next();
      out.append(key).append('=');
      String value = Strings.nullToEmpty(StringUtils.toString(gettable.get(key)));
      if (value.length() > 60) {
        value = value.substring(0, 60) + "...";
      }
      out.append(value);
      out.append("\n");
    }
    out.append("}");
  }

  public static String debug(Gettable gettable)
  {
    StringBuilder out = new StringBuilder();
    try {
      debug(gettable, out);
    } catch (IOException e) {
      throw new StringBuilderIOException(out, e);
    }
    return out.toString();
  }

  /**
   * Get a filtered view onto the public keys on a given Gettable with only keys matching the regex
   * being included
   * 
   * @return Iterator of String filtered from the gettable.getPublicKeys() stream.
   * @see Gettable#getPublicKeys()
   */
  public static Iterator<String> getMatchingPublicKeys(Gettable gettable,
                                                       String regex)
  {
    final Pattern p = Pattern.compile(regex);
    return new SkippingIterator<String>(gettable.getPublicKeys().iterator()) {
      @Override
      protected boolean shouldSkip(String object)
      {
        return !p.matcher(StringUtils.toString(object)).find();
      }
    };
  }

  public static Gettable emptyGettable()
  {
    return EMPTYGETTABLE;
  }

  private static final EmptyGettable EMPTYGETTABLE = new EmptyGettable();

  private static class EmptyGettable
    implements Gettable
  {

    private EmptyGettable()
    {
    }

    @Override
    public Boolean getBoolean(Object key)
    {
      return null;
    }

    @Override
    public File getFile(Object key)
    {
      return null;
    }

    @Override
    public Collection<String> getPublicKeys()
    {
      return Collections.emptyList();
    }

    @Override
    public String getString(Object key)
    {
      return null;
    }

    @Override
    public Collection<?> getValues(Object key)
    {
      return null;
    }

    @Override
    public boolean containsKey(Object key)
    {
      return false;
    }

    @Override
    public Object get(Object key)
    {
      return null;
    }

    @Override
    public Collection<?> getPaddedValues(Object key)
    {
      return ImmutableList.of();
    }
  }

  /**
   * Invoke getValues on the supplied Gettable and return a Collection that contains String
   * representations of the supplied values.
   * 
   * @return The appropriate Collection or null if the gettable doesn't have information on the key.
   * @see Gettable#getValues(Object)
   * @see StringUtils#toString(Object)
   */
  public static Collection<String> getValuesAsStrings(Gettable gettable,
                                                      String key)
  {
    Collection<?> values = gettable.getValues(key);
    if (values == null) {
      return null;
    }
    if (values.size() == 0) {
      return Collections.emptyList();
    }
    ArrayList<String> strings = new ArrayList<String>(values.size());
    for (Object value : values) {
      strings.add(StringUtils.toString(value));
    }
    return strings;
  }

  /**
   * Singleton implementation of SettableFactory that always returns Settable.EMPTYSETTABLE
   * 
   * @see GettableUtils#EMPTYSETTABLE
   */
  public static final SettableFactory EMPTYSETTABLEFACTORY = new SettableFactory() {
    @Override
    public Gettable getGettable()
    {
      return GettableUtils.EMPTYSETTABLE;
    }

    @Override
    public Settable getSettable()
    {
      return GettableUtils.EMPTYSETTABLE;
    }
  };

  /**
   * Singleton that represents a constant empty Gettable.
   */
  public final static Settable EMPTYSETTABLE = new Settable() {
    /**
     * @return null
     */
    @Override
    public Object get(Object key)
    {
      return null;
    }

    @Override
    public Collection<Object> getValues(Object key)
    {
      return null;
    }

    @Override
    public Boolean getBoolean(Object key)
    {
      return null;
    }

    @Override
    public String getString(Object key)
    {
      return null;
    }

    @Override
    public Collection<String> getPublicKeys()
    {
      return Collections.emptyList();
    }

    @Override
    public Collection<String> getSettableKeys()
    {
      return Collections.emptyList();
    }

    @Override
    public Object set(String key,
                      Object value)
        throws SettableException
    {
      throw new SettableException("Immutable EmptySettable", this, key, value, null);
    }

    @Override
    public File getFile(Object key)
    {
      return null;
    }

    @Override
    public ValueType getValueType(String key)
    {
      return null;
    }

    @Override
    public boolean containsKey(Object key)
    {
      return false;
    }

    @Override
    public Object set(TypedKey<?> key,
                      Object value)
        throws SettableException
    {
      return set(key.getKeyName(), value);
    }

    @Override
    public Collection<?> getPaddedValues(Object key)
    {
      return ImmutableList.of();
    }
  };

  /**
   * Get the original wrapped Gettable contained within a Gettable or return the original gettable.
   * This will keep traversing up nested GettableWrappers to the deepest Gettable
   * 
   * @return if gettable is not a GettableWrapper then gettable, otherwise the wrapped Gettable
   */
  public static Gettable getWrappedGettable(Gettable gettable)
  {
    if (gettable instanceof GettableWrapper) {
      return getWrappedGettable(((GettableWrapper) gettable).getWrappedGettable());
    }
    return gettable;
  }

  /**
   * @deprecated as it isn't used and I think that I prefer my new
   *             {@link #getValues(Gettable, String, String, GettableSupplier)}
   */
  @Deprecated
  public static <T> List<T> get(GettableSupplier<T> supplier,
                                String keysName,
                                Gettable source)
  {
    String[] keys =
        Preconditions.checkNotNull(source.getString(keysName), "%s.%s", source, keysName)
                     .split(",");
    List<T> values = Lists.newArrayListWithCapacity(keys.length);
    for (String key : keys) {
      values.add(supplier.get(new NamespaceGettable(source, key.trim())));
    }
    return values;
  }

  /**
   * Get a List of named keys from a Gettable where the keys to be got are stored as a comma
   * separated list inside the same Gettable. So for example, if we had a namespace of "namespace."
   * and the name of the keys was "keys" then we would a Gettable that contained the following:
   * <dl>
   * <dt>namespace.keys</dt>
   * <dd>alpha,beta,gamma</dd>
   * <dt>namespace.alpha.value</dt>
   * <dd>alpha value</dd>
   * <dt>namespace.beta.value</dt>
   * <dd>beta value</dd>
   * <dt>namespace.gamma.value</dt>
   * <dd>gamma value</dd>
   * </dl>
   * 
   * @param <T>
   *          The type of value that will be retrieved from the Gettable
   * @param gettable
   *          The Gettable that contains both the keys and the values
   * @param namespace
   *          The prefix that is applied to both keys and the key values contained within the keys
   * @param keysName
   *          The name relative to prefix of the String that contains a comma separated list of keys
   *          to resolve
   * @param supplier
   *          The GettableSupplier that will be passed a NamespacedGettable view to retrieve the
   *          key. If the supplier returns null for any key that is passed in then the method will
   *          fail.
   * @return An ImmutableList of T. If keys is not defined or empty then this will be empty. This
   *         will not contain any null elements.
   */
  public static <T> ImmutableList<T> getValues(Gettable gettable,
                                               String namespace,
                                               String keysName,
                                               GettableSupplier<T> supplier)
  {
    Gettable namespaced = new NamespaceGettable(gettable, namespace + '.');
    String allKeys = namespaced.getString(keysName);
    if (Strings.isNullOrEmpty(allKeys)) {
      return ImmutableList.of();
    }
    ImmutableList.Builder<T> builder = ImmutableList.builder();
    for (String key : getKeySplitter().split(allKeys)) {
      try {
        builder.add(supplier.get(new NamespaceGettable(namespaced, key + '.')));
      } catch (RuntimeException e) {
        throw new IllegalArgumentException(String.format("Failure to boot %s.%s",
                                                         namespace,
                                                         keysName),
                                           e);
      }
    }
    return builder.build();
  }

  private static final Splitter __keySplitter = Splitter.on(',').trimResults().omitEmptyStrings();

  /**
   * Get a {@link Splitter} that is preconfigured to split out comma separated keys with trimming of
   * results and empty strings ommitted. This is the behaviour used by internal key splitting
   * algorithmis and is exposed to let 3rd party extensions utilise the same behaviour.
   */
  public static Splitter getKeySplitter()
  {
    return __keySplitter;
  }

  /**
   * Get a List of T's from a Gettable that have been encoded in a similar manner to
   * {@link #getValues(Gettable, String, String, GettableSupplier)} but which we are going to use a
   * {@link KeyGettableSupplier} to extract the value thereby letting us use the key as part of the
   * definition of the T itself.
   * 
   * @return An ImmutableList of T. If keys is not defined or empty then this will be empty. This
   *         will not contain any null elements.
   */
  public static <T> ImmutableList<T> getValues(Gettable gettable,
                                               String namespace,
                                               String keysName,
                                               KeyGettableSupplier<T> supplier)
  {
    Gettable namespaced = new NamespaceGettable(gettable, namespace + '.');
    String allKeys = namespaced.getString(keysName);
    if (Strings.isNullOrEmpty(allKeys)) {
      return ImmutableList.of();
    }
    ImmutableList.Builder<T> builder = ImmutableList.builder();
    for (String key : getKeySplitter().split(allKeys)) {
      try {
        builder.add(supplier.get(key, new NamespaceGettable(namespaced, key + '.')));
      } catch (RuntimeException e) {
        throw new IllegalArgumentException(String.format("Error initialising %s.%s",
                                                         namespace,
                                                         key),
                                           e);
      }
    }
    return builder.build();
  }

  /**
   * Return a KeyGettableSupplier that generates an EnglishNamed where the Name is the key and the
   * EnglishName is the specified field in the Gettable.
   */
  public static KeyGettableSupplier<EnglishNamed> getEnglishNamedKeyGettableSupplier(final String englishNameKey)
  {
    return new KeyGettableSupplier<EnglishNamed>() {
      @Override
      public EnglishNamed get(String key,
                              Gettable gettable)
      {
        return new EnglishNamedImpl(key, englishNameKey);
      }
    };
  }

  public static List<String> getStrings(Gettable gettable,
                                        String namespace,
                                        String keys)
  {
    String allKeys = gettable.getString(namespace + "." + keys);
    if (Strings.isNullOrEmpty(allKeys)) {
      return ImmutableList.of();
    }
    ImmutableList.Builder<String> builder = ImmutableList.builder();
    for (String key : Splitter.on(',').trimResults().omitEmptyStrings().split(allKeys)) {
      builder.add(Preconditions.checkNotNull(gettable.getString(namespace + '.' + key),
                                             "%s.%s",
                                             namespace,
                                             key));
    }
    return builder.build();
  }

  /**
   * Create a new GetttableSupplier that returns a String with the given key from the Gettable.
   */
  public static GettableSupplier<String> getStringGettableSupplier(final String key)
  {
    return new GettableSupplier<String>() {
      @Override
      public String get(Gettable gettable)
      {
        DebugConstants.method(this, "get", gettable);
        DebugConstants.variable("-->", gettable.getString(key));
        return gettable.getString(key);
      }
    };

  }

  /**
   * Create a new GettableSupplier that returns a Boolean with the given key from the Gettable.
   */
  public static GettableSupplier<Boolean> getBooleanGettableSupplier(final String key)
  {
    return new GettableSupplier<Boolean>() {
      @Override
      public Boolean get(Gettable gettable)
      {
        return gettable.getBoolean(key);
      }
    };
  }

  /**
   * Get a GettableSupplier that returns an EnglishNamed where the name and englishName have been
   * resolved using {@link Gettable#getString(Object)} with the supplied keys.
   */
  public static GettableSupplier<EnglishNamed> getEnglishNamedGettableSupplier(final String nameKey,
                                                                               final String englishNameKey)
  {
    return new GettableSupplier<EnglishNamed>() {
      @Override
      public EnglishNamed get(Gettable gettable)
      {
        return new EnglishNamedImpl(gettable.getString(nameKey),
                                    gettable.getString(englishNameKey));
      }
    };
  }

  /**
   * Get a GettableSupplier that returns an EnglishNamed where the names are looked up with keys of
   * "name" and "englishName"
   */
  public static GettableSupplier<EnglishNamed> getEnglishNamedGettableSupplier()
  {
    return getEnglishNamedGettableSupplier("name", "englishName");
  }

  /**
   * Use reflection to try and find a static getInstance(Gettable) method on the given Class and
   * then invoke it with the supplied Gettable.
   * 
   * @throws IllegalArgumentException
   *           if it was not possible to invoke the method on the Class. This may be because the
   *           Class doesn't have the method, or because the method itself threw an exception.
   * @return the result of invoking the method which may be null
   */
  public static Object getInstance(Class<?> c,
                                   Gettable gettable)
  {
    try {
      return c.getMethod("getInstance", Gettable.class).invoke(null, gettable);
    } catch (Exception e) {
      throw new IllegalArgumentException(String.format("Failed to invoke %s.getInstance(Gettable)",
                                                       c),
                                         e);
    }
  }

  /**
   * Convert a null value to an empty ArrayList for use in situations where you don't want to have
   * to check for null when using {@link Gettable#getValues(Object)}
   */
  public static Collection<?> nullToEmptyValues(Collection<?> values)
  {
    return values == null ? Lists.newArrayList() : values;
  }
}
