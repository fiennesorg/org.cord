package org.cord.util;

public interface PredicateInt
{
  public boolean apply(int input);
}
