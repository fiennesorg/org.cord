package org.cord.util;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Extension of SettableMap that allows the definition of a set of ValueType items to describe how
 * it should be utilised.
 * 
 * @author alex
 */
public class DescribedSettableMap
  extends SettableMap
{
  private static final long serialVersionUID = 1L;

  private Map<String, ValueType> __valueTypes = new LinkedHashMap<String, ValueType>();

  private Map<String, ValueType> __roValueTypes = Collections.unmodifiableMap(__valueTypes);

  /**
   * Add a ValueType to the set of SettableKeys for this Settable. If the ValueType declares a
   * default value then store this in the Settable according to the key associated with the
   * ValueType.
   */
  public ValueType addValueType(ValueType valueType)
  {
    ValueType oldValueType = __valueTypes.put(valueType.getName(), valueType);
    Object defaultValue = valueType.getDefaultValue(null);
    if (defaultValue != null) {
      put(valueType.getName(), defaultValue);
    }
    return oldValueType;
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    return __roValueTypes.keySet();
  }

  @Override
  public Collection<String> getSettableKeys()
  {
    return __roValueTypes.keySet();
  }

  @Override
  public ValueType getValueType(String key)
  {
    return __valueTypes.get(key);
  }
}
