package org.cord.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import com.google.common.base.Preconditions;

/*
 * Wrapper around a JSONObject that provides a read-only Gettable interface to the JSONObject. Data
 * is not cached so the data is "live".
 */
public class JSONObjectGettable
  extends AbstractGettable
{
  private final JSONObject __jObj;
  private final boolean __exposesSource;

  public JSONObjectGettable(JSONObject jObj,
                            boolean exposesSource)
  {
    __jObj = Preconditions.checkNotNull(jObj, "jObj");
    __exposesSource = exposesSource;
  }

  public final boolean exposesSource()
  {
    return __exposesSource;
  }

  /**
   * @throws IllegalStateException
   *           if this GettableJSONObject has been initialised to not expose the source JSONObject
   */
  public final JSONObject getSource()
  {
    if (__exposesSource) {
      return __jObj;
    }
    throw new IllegalStateException(this + " doesn't grant access to the contained JSONObject");
  }

  @Override
  public Object get(Object key)
  {
    return JSONUtils.asImmutable(__jObj.opt(key.toString()), __exposesSource);
  }

  public JSONArrayList getJSONArrayList(String key)
  {
    return (JSONArrayList) get(key);
  }

  public JSONObjectGettable getJSONObjectGettable(String key)
  {
    return (JSONObjectGettable) get(key);
  }

  /**
   * Invoke keys() on the nested JSONObject and return the results as an immutable List. The
   * returned value is not a live reflection of the state of the JSONObject but rather a snapshot.
   * 
   * @see JSONObject#keys()
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    List<String> keys = new ArrayList<String>();
    Iterator<String> i = __jObj.keys();
    while (i.hasNext()) {
      keys.add(i.next().toString());
    }
    return Collections.unmodifiableList(keys);
  }

}
