package org.cord.util;

public class NoSuchNamedException
  extends NamedException
{
  /**
   * 
   */
  private static final long serialVersionUID = -6143372224978675802L;

  private final String _name;

  public NoSuchNamedException(String name,
                              String details)
  {
    super(null,
          name + ":" + details);
    _name = name;
  }

  public final String getName()
  {
    return _name;
  }
}
