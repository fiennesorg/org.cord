package org.cord.util;

import java.io.IOException;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import com.google.common.base.Optional;

public enum JodaGranularity {
  DATE {
    @Override
    public void selector(Appendable buf,
                         Map<Object, Object> context,
                         String label,
                         Optional<DateTime> value,
                         boolean isCompulsory,
                         Object... ids)
        throws IOException
    {
      HtmlUtils.jodaDate(buf, context, label, value, isCompulsory, ids);
    }
    @Override
    public void widget(Appendable buf,
                       Map<Object, Object> context,
                       Optional<DateTime> value,
                       Object... ids)
        throws IOException
    {
      HtmlUtils.jodaDateWidget(buf, context, value, ids);
    }
    @Override
    public Optional<DateTime> parseFormatted(String text)
    {
      return HtmlUtils.jodaDateParse(text);
    }
    @Override
    public DateTime quantise(DateTime dateTime)
    {
      return dateTime.withTime(0, 0, 0, 0);
    }
  },
  TIME {
    @Override
    public void selector(Appendable buf,
                         Map<Object, Object> context,
                         String label,
                         Optional<DateTime> value,
                         boolean isCompulsory,
                         Object... ids)
        throws IOException
    {
      HtmlUtils.jodaTime(buf, context, label, value, isCompulsory, ids);
    }
    @Override
    public void widget(Appendable buf,
                       Map<Object, Object> context,
                       Optional<DateTime> value,
                       Object... ids)
        throws IOException
    {
      HtmlUtils.jodaTimeWidget(buf, context, value, ids);
    }
    @Override
    public Optional<DateTime> parseFormatted(String text)
    {
      return HtmlUtils.jodaTimeParse(text);
    }
    @Override
    public DateTime quantise(DateTime dateTime)
    {
      return dateTime.withMillisOfSecond(0);
    }
  },
  TIMESTAMP {
    @Override
    public void selector(Appendable buf,
                         Map<Object, Object> context,
                         String label,
                         Optional<DateTime> value,
                         boolean isCompulsory,
                         Object... ids)
        throws IOException
    {
      HtmlUtils.text(null,
                     buf,
                     label,
                     value.isPresent() ? ISODateTimeFormat.basicDateTime().print(value.get()) : "",
                     isCompulsory,
                     ids);
    }
    @Override
    public void widget(Appendable buf,
                       Map<Object, Object> context,
                       Optional<DateTime> value,
                       Object... ids)
        throws IOException
    {
      HtmlUtils.textWidget(null,
                           buf,
                           value.isPresent()
                               ? ISODateTimeFormat.basicDateTime().print(value.get())
                               : "",
                           ids);
    }
    @Override
    public Optional<DateTime> parseFormatted(String text)
    {
      try {
        return Optional.of(ISODateTimeFormat.basicDateTime().parseDateTime(text));
      } catch (Exception e) {
        return Optional.absent();
      }
    }
    @Override
    public DateTime quantise(DateTime dateTime)
    {
      return dateTime;
    }
  };

  public abstract void selector(Appendable buf,
                                Map<Object, Object> context,
                                String label,
                                Optional<DateTime> value,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException;

  public abstract void widget(Appendable buf,
                              Map<Object, Object> context,
                              Optional<DateTime> value,
                              Object... ids)
      throws IOException;

  /**
   * Attempt to parse the text value into a DateTime. This will attempt {@link #parseMs(String)}
   * first, and if that fails then try {@link #parseFormatted(String)}.
   */
  public final Optional<DateTime> parse(String text)
  {
    Optional<DateTime> optDateTime = parseMs(text);
    if (optDateTime.isPresent()) {
      return optDateTime;
    }
    return parseFormatted(text);
  }

  /**
   * Try and parse a text into a DateTime if it was formatted as a long ms timestamp.
   */
  public final Optional<DateTime> parseMs(String text)
  {
    Long ms = NumberUtil.toLongObj(text, null);
    return ms == null ? Optional.<DateTime> absent() : Optional.of(new DateTime(ms));
  }

  /**
   * Try and parse text into a DateTime if it was formatted in the default format for the
   * JodaGranularity implementation.
   */
  public abstract Optional<DateTime> parseFormatted(String text);

  public abstract DateTime quantise(DateTime dateTime);

  public Optional<DateTime> quantise(Optional<DateTime> dateTime)
  {
    if (dateTime.isPresent()) {
      return Optional.of(quantise(dateTime.get()));
    }
    return Optional.absent();
  }
}
