package org.cord.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

/**
 * An ImmutableMap Builder that only adds values for keys that have not yet been defined and
 * discards duplicates quietly. This is intended to let you define a Map that represents defined
 * values with fallback values from some other source. Add the definitions with the highest priority
 * first followed by the defaults in descending priority. Null values are not defined.
 * <p>
 * The primary motivation behind this is that the conventional ImmutableMap.Builder fails when you
 * add more than one value for a key and therefore is not suitable for overriding values.
 * 
 * @author alex
 */
public class DelegatingImmutableMapBuilder<K, V>
{
  private final ImmutableMap.Builder<K, V> __builder;
  private final Set<K> __keys = new HashSet<K>();

  public DelegatingImmutableMapBuilder()
  {
    __builder = new ImmutableMap.Builder<K, V>();
  }

  public ImmutableMap<K, V> build()
  {
    return __builder.build();
  }

  public DelegatingImmutableMapBuilder<K, V> put(K key,
                                                 V value)
  {
    Preconditions.checkNotNull(key, "key");
    Preconditions.checkNotNull(value, "value");
    if (!__keys.contains(key)) {
      __builder.put(key, value);
      __keys.add(key);
    }
    return this;
  }

  public DelegatingImmutableMapBuilder<K, V> put(Map<? extends K, ? extends V> values)
  {
    for (Map.Entry<? extends K, ? extends V> entry : values.entrySet()) {
      put(entry.getKey(), entry.getValue());
    }
    return this;
  }
}
