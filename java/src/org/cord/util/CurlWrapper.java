package org.cord.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.common.base.Preconditions;

/**
 * Wrapper around the command line libcurl utility for making rapid file transfers. This is useful
 * in situations when the native java libraries are not getting it right intelligently enough for
 * non-standard transfer situations - curl is very flexible and will generally work it out.
 */
public class CurlWrapper
  implements FtpClientImplementation
{
  public final static String CURL_PATH = "curl.path";

  public final static String CURL_PATH_DEFAULT = "/usr/bin/curl";

  private final String __curlPath;

  public CurlWrapper(String curlPath)
  {
    __curlPath = Preconditions.checkNotNull(curlPath, "curlPath");
  }

  public CurlWrapper(Gettable config)
  {
    this(GettableUtils.get(config, CURL_PATH, CURL_PATH_DEFAULT));
  }

  @Override
  public void uploadFile(File localFile,
                         String remoteServer,
                         String remoteLogin,
                         String remotePassword,
                         String remoteDirectory,
                         String remoteFilename,
                         PrintWriter log)
      throws IOException
  {
    String[] cmd = new String[6];
    cmd[0] = __curlPath;
    cmd[1] = "-T";
    cmd[2] = localFile.getAbsolutePath();
    cmd[3] = "-u";
    cmd[4] = remoteLogin + ":" + remotePassword;
    cmd[5] = "ftp://" + remoteServer + "/" + remoteDirectory + "/"
             + (remoteFilename == null ? localFile.getName() : remoteFilename);
    StringBuilder buffer = new StringBuilder();
    IoUtil.exec(cmd, null, buffer, buffer);
    if (log != null) {
      log.println(buffer.toString());
    }
  }
}