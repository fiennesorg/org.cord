package org.cord.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;

import com.google.common.collect.Sets;

/**
 * Utility class to try and determine the latest timestamp from a CVS checkout by recursively
 * parsing the CVS/Entries files.
 * 
 * @author alex
 */
public class SweepCvs
{
  private final SimpleDateFormat __parser = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy");

  public SweepCvs(File rootDir) throws IOException, ParseException
  {
    TreeSet<Entry> entries = Sets.newTreeSet();
    addDir(entries, rootDir);
    System.out.println(entries.first());
    System.out.println(entries.last());
  }

  private void addDir(Collection<Entry> e,
                      File dir)
      throws IOException, ParseException
  {
    File entries = new File(dir, "CVS/Entries");
    try (BufferedReader entriesReader = new BufferedReader(new FileReader(entries))) {
      String line = null;
      while ((line = entriesReader.readLine()) != null) {
        String[] tokens = line.split("/");
        switch (tokens.length) {
          case 1:
            if (!"D".equals(tokens[0])) {
              throw new RuntimeException("HELP: " + Arrays.toString(tokens));
            }
            break;
          case 2:
            if (!"D".equals(tokens[0])) {
              throw new RuntimeException("HELP: " + Arrays.toString(tokens));
            }
            addDir(e, new File(dir, tokens[1]));
            break;
          case 4:
          case 5:
          case 6:
            Entry entry = new Entry(dir, tokens[1], __parser.parse(tokens[3]));
            e.add(entry);
            break;
          default:
            System.out.println("handle: " + tokens.length + ": " + Arrays.toString(tokens));
        }
      }
    }
  }

  class Entry
    implements Comparable<Entry>
  {
    final File __dir;
    final String __filename;
    final Date __date;

    public Entry(File dir,
                 String filename,
                 Date date)
    {
      __dir = dir;
      __filename = filename;
      __date = date;
    }

    @Override
    public String toString()
    {
      return __dir + "/" + __filename + " - " + __date;
    }

    @Override
    public int compareTo(Entry arg0)
    {
      return __date.compareTo(arg0.__date);
    }
  }

  public static void main(String[] args)
      throws Exception
  {
    new SweepCvs(new File("/Users/alex/legacy/fiennes.co.uk/org.cord"));
  }
}
