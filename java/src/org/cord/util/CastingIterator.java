package org.cord.util;

import java.util.Iterator;

/**
 * Wrapper around a general purpose Iterator that we know is of a given Class that automatically
 * typecasts the return values letting us use it as if it was declared to be of the correct class.
 * 
 * @author alex
 * @param <T>
 */
public class CastingIterator<T>
  implements Iterator<T>
{
  public static <T> CastingIterator<T> of(Iterator<?> source,
                                          Class<T> valueClass)
  {
    return new CastingIterator<T>(source, valueClass);
  }

  private final Iterator<?> __source;
  private final Class<T> __valueClass;

  public CastingIterator(Iterator<?> source,
                         Class<T> valueClass)
  {
    __source = source;
    __valueClass = valueClass;
  }

  @Override
  public boolean hasNext()
  {
    return __source.hasNext();
  }

  @Override
  public T next()
  {
    return ObjectUtil.castTo(__source.next(), __valueClass);
  }

  @Override
  public void remove()
  {
    __source.remove();
  }
}
