package org.cord.util;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Utility method that provides a fully synchronized Object for storing a Mutable boolean value.
 * This can then be used as a target for synchronization in it's own right as well.
 * 
 * @deprecated in favour of {@link AtomicBoolean}
 */
@Deprecated
public class MutableBoolean
{
  private boolean _value;

  public MutableBoolean(boolean value)
  {
    _value = value;
  }

  public synchronized boolean getValue()
  {
    return _value;
  }

  public synchronized void setValue(boolean value)
  {
    _value = value;
  }

  @Override
  public String toString()
  {
    return "MutableBoolean(" + _value + ")";
  }
}
