package org.cord.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Class that permits the restriction of Date values to fixed sizes or quanta. The public methods
 * are all thread safe which enables a single DateQuantiser to safely be shared between multiple
 * threads. However, if there is likely to be multiple concurrent access, it will be more performant
 * to create multiple instances of DateQuantiser to prevent unnecessary Thread blocking and the
 * associated performance hit.
 */
public class DateQuantiser
{
  public enum Granularity {
    MILLISECOND(DURATION_MILLISECOND, "HH:mm:ss:S'ms' dd/MM/yy"),
    SECOND(DURATION_SECOND, "HH:mm:ss dd MMMM yyyy"),
    MINUTE(DURATION_MINUTE, "HH:mm dd MMMM yyyy"),
    HOUR(DURATION_HOUR, "ha dd MMMM yyyy"),
    DAY(DURATION_DAY, "dd MMMM yyyy"),
    MONTH(DURATION_DAY * 29l, "MMMM yyyy"),
    YEAR(DURATION_DAY * 365l, "yyyy");
    private final long __duration;
    private final SimpleDateFormat __format;

    Granularity(long duration,
                String format)
    {
      __duration = duration;
      __format = new SimpleDateFormat(format);
    }

    public final long duration()
    {
      return __duration;
    }

    public final String toString(Date date)
    {
      synchronized (__format) {
        return __format.format(date);
      }
    }
  }

  public final static long DURATION_MILLISECOND = 1l;

  public final static long DURATION_SECOND = 1000l;

  public final static long DURATION_MINUTE = DURATION_SECOND * 60l;

  public final static long DURATION_HOUR = DURATION_MINUTE * 60l;

  public final static long DURATION_DAY = DURATION_HOUR * 24l;

  public final static long DURATION_WEEK = DURATION_DAY * 7l;

  private final Calendar __calendar;

  private DateQuantiser(Calendar calendar)
  {
    __calendar = calendar;
  }

  public DateQuantiser()
  {
    this(Calendar.getInstance());
  }

  public DateQuantiser(TimeZone zone,
                       Locale aLocale)
  {
    this(Calendar.getInstance(zone, aLocale));
  }

  public Date quantise(Date date,
                       Granularity granularity)
  {
    switch (granularity) {
      case YEAR:
        return byYear(date);
      case MONTH:
        return byMonth(date);
      case DAY:
        return byDay(date);
      case HOUR:
        return byHour(date);
      case MINUTE:
        return byMinute(date);
      case SECOND:
        return bySecond(date);
      default:
        return date;
    }
  }

  public long quantise(long date,
                       Granularity granularity)
  {
    switch (granularity) {
      case YEAR:
        return byYear(date);
      case MONTH:
        return byMonth(date);
      case DAY:
        return byDay(date);
      case HOUR:
        return byHour(date);
      case MINUTE:
        return byMinute(date);
      case SECOND:
        return bySecond(date);
      default:
        return date;
    }
  }

  public Date bySecond(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      bySecond(__calendar);
      return __calendar.getTime();
    }
  }

  public long bySecond(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      bySecond(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void bySecond(Calendar calendar)
  {
    calendar.set(Calendar.MILLISECOND, 0);
  }

  public Date byMinute(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      byMinute(__calendar);
      return __calendar.getTime();
    }
  }

  public long byMinute(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      byMinute(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void byMinute(Calendar calendar)
  {
    bySecond(calendar);
    calendar.set(Calendar.SECOND, 0);
  }

  public Date byHour(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      byHour(__calendar);
      return __calendar.getTime();
    }
  }

  public long byHour(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      byHour(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void byHour(Calendar calendar)
  {
    byMinute(calendar);
    calendar.set(Calendar.MINUTE, 0);
  }

  public Date byDay(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      byDay(__calendar);
      return __calendar.getTime();
    }
  }

  public long byDay(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      byDay(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void byDay(Calendar calendar)
  {
    byHour(calendar);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
  }

  public Date byMonth(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      byMonth(__calendar);
      return __calendar.getTime();
    }
  }

  public long byMonth(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      byMonth(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void byMonth(Calendar calendar)
  {
    byDay(calendar);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
  }

  public Date byYear(Date date)
  {
    synchronized (__calendar) {
      __calendar.setTime(date);
      byYear(__calendar);
      return __calendar.getTime();
    }
  }

  public long byYear(long date)
  {
    synchronized (__calendar) {
      __calendar.setTimeInMillis(date);
      byYear(__calendar);
      return __calendar.getTimeInMillis();
    }
  }

  private void byYear(Calendar calendar)
  {
    byMonth(calendar);
    calendar.set(Calendar.MONTH, 0);
  }
}
