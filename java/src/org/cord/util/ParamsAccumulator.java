package org.cord.util;

import java.util.Arrays;

public class ParamsAccumulator
{
  public static final Object[] NOPARAMS = new Object[0];

  public interface Invoker<V>
  {
    public V invoke(Object[] params);
  }

  /**
   * If maxParams is > 0, call invoker.invoke(); otherwise return a new SimpleGettable-derived
   * object that accumulates the value passed to get(), and calls invoker().invoke() once maxParams
   * have been accumulated.
   */
  public static <V> Object accumulate(int maxParams,
                                      Invoker<V> invoker)
  {
    if (maxParams == 0) {
      return invoker.invoke(NOPARAMS);
    }
    return new ParamsTail<V>(maxParams, invoker);
  }

  /* ==================================================================== */
  /* private/protected implementation */
  public static abstract class Params<V>
    extends AbstractGettable
  {
    /* Internal API */
    protected abstract Object invoke(Object[] params,
                                     Object key);

    protected abstract Params<V> tail();

    /**
     * @return true because you can pass any key you like as a parameter.
     */
    @Override
    public final boolean containsKey(Object key)
    {
      return true;
    }

    /* Debug functionality */
    protected abstract Object[] getParams(Object[] params,
                                          Object key);

    protected abstract Object[] getParams(Object key);

    protected abstract String getName();

    @Override
    public final String toString()
    {
      return String.format("%s(%s)", getName(), Arrays.toString(getParams(null)));
    }
  }

  /* -------------------------------------------------------------------- */
  /**
   * Tail element of an accumulated Parameter list
   */
  public static class ParamsTail<V>
    extends Params<V>
  {
    /* private/protected implementation */
    private final int __maxParams;

    private final Invoker<V> __invoker;

    public ParamsTail(int maxParams,
                      Invoker<V> invoker)
    {
      __maxParams = maxParams;
      __invoker = invoker;
    }

    public final Invoker<V> getInvoker()
    {
      return __invoker;
    }

    /**
     * @return Either another Gettable ParamsElem if more parameters are required or the result of
     *         invoking the appropriate functionality with the existing params if the addition of
     *         this parameter satisfies the requirements.
     * @see Invoker#invoke(Object[])
     */
    @Override
    public final Object get(Object key)
    {
      if (__maxParams == 1) {
        return invoke(new Object[1], key);
      }
      return new ParamsElem<V>(__maxParams, 1, key, this);
    }

    public final Object get(String key)
    {
      return get((Object) key);
    }

    /* Internal API */
    @Override
    protected final Object invoke(Object[] params,
                                  Object key)
    {
      params[0] = key;
      return __invoker.invoke(params);
    }

    @Override
    protected final Params<V> tail()
    {
      return null;
    }

    /* Debug functionality */
    @Override
    protected final Object[] getParams(Object key)
    {
      return getParams(new Object[1], key);
    }

    @Override
    protected final Object[] getParams(Object[] params,
                                       Object key)
    {
      params[0] = key;
      return params;
    }

    @Override
    protected String getName()
    {
      return __invoker.toString();
    }
  }

  /* -------------------------------------------------------------------- */
  /** Non-tail params elements */
  private static class ParamsElem<V>
    extends Params<V>
  {
    private final int __maxParams;

    private final int __curParams;

    private final Object __key;

    private final Params<V> __tail;

    public ParamsElem(int maxParams,
                      int curParams,
                      Object key,
                      Params<V> tail)
    {
      __maxParams = maxParams;
      __curParams = curParams;
      __key = key;
      __tail = tail;
    }

    /* Internal API */
    @Override
    protected final Object invoke(Object[] params,
                                  Object key)
    {
      params[__curParams] = key;
      return tail().invoke(params, __key);
    }

    @Override
    public Params<V> tail()
    {
      return __tail;
    }

    /* Debug functionality */
    @Override
    protected final Object[] getParams(Object key)
    {
      return getParams(new Object[__maxParams], key);
    }

    @Override
    protected final Object[] getParams(Object[] params,
                                       Object key)
    {
      params[__curParams] = key;
      return tail().getParams(params, __key);
    }

    /* SimpleGettable overrides */
    @Override
    public final Object get(Object key)
    {
      if (__curParams == __maxParams - 1) {
        return invoke(new Object[__maxParams], key);
      }
      return new ParamsElem<V>(__maxParams, __curParams + 1, key, this);
    }

    @Override
    protected String getName()
    {
      return tail().getName();
    }
  }
}
