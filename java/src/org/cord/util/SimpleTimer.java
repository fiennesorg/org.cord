package org.cord.util;

public class SimpleTimer
{
  private final long _creationTime;

  private long _lastAccessTime;

  public SimpleTimer()
  {
    _creationTime = System.currentTimeMillis();
    _lastAccessTime = _creationTime;
  }

  private void updateLastAccessTime()
  {
    _lastAccessTime = System.currentTimeMillis() - _creationTime;
  }

  public long getElapsedTime()
  {
    updateLastAccessTime();
    return _lastAccessTime;
  }

  public long getLapTime()
  {
    long lastLapTime = _lastAccessTime;
    updateLastAccessTime();
    return _lastAccessTime - lastLapTime;
  }
}
