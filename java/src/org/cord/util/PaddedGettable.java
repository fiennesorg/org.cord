package org.cord.util;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

/**
 * ForwardingGettable that wherever possible pads null requested values with a sensible default
 * value. This is useful in situations where you are planning to padNull all of the values you get
 * out of the Gettable.
 * 
 * @author alex
 */
public class PaddedGettable
  extends ForwardingGettable
{
  private final Object __paddedObject;

  private final File __paddedFile;

  /**
   * Create an explicitly defined PaddedGettableWrapper
   * 
   * @param paddedObject
   *          The Object which will pad null values for get(key)
   * @param paddedFile
   *          The Object which will pad null values for getFile(key)
   * @see #get(Object)
   * @see #getFile(Object)
   */
  public PaddedGettable(Gettable source,
                        Object paddedObject,
                        File paddedFile)
  {
    super(source);
    __paddedObject = paddedObject;
    __paddedFile = paddedFile;
  }

  /**
   * Create a default instance which doesn't pad null Objects and Files.
   */
  public PaddedGettable(Gettable source)
  {
    this(source,
         null,
         null);
  }

  /**
   * @return the wrapped Object or the paddedObject that was passed to the creator (null in the
   *         default implementation)
   */
  @Override
  public Object get(Object key)
  {
    return MoreObjects.firstNonNull(super.get(key), __paddedObject);
  }

  /**
   * @return the wrapper File or the paddedFile that was passed to the creator (null in the default
   *         implementation)
   */
  @Override
  public File getFile(Object key)
  {
    return MoreObjects.firstNonNull(super.getFile(key), __paddedFile);
  }

  /**
   * @return The wrapped flag of false if not defined.
   */
  @Override
  public Boolean getBoolean(Object key)
  {
    return MoreObjects.firstNonNull(super.getBoolean(key), Boolean.FALSE);
  }

  /**
   * The wrapped collection or an empty list if not defined
   */
  @Override
  public Collection<?> getValues(Object key)
  {
    return MoreObjects.firstNonNull(super.getValues(key), Collections.emptyList());
  }

  /**
   * The wrapped String or "" if not defined
   */
  @Override
  public String getString(Object key)
  {
    return Strings.nullToEmpty(super.getString(key));
  }
}
