package org.cord.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.common.collect.Iterators;

/**
 * Provides a wrapper class around an Iterator that enables the Iterator to be traversed backwards
 * from start to finish.
 */
public class ReverseListIterator<T>
  implements ListIterator<T>
{
  private ListIterator<T> _listIterator;

  public ReverseListIterator(T object)
  {
    this(Iterators.singletonIterator(object));
  }

  public ReverseListIterator(ListIterator<T> source)
  {
    _listIterator = source;
  }

  /**
   * Create an Iterator that moves from the end of the given ListIterator to the beginning. Be aware
   * that the first operation that this creater does is to advance the ListIterator to the end of
   * it's list so it is ready to start the reverse iteration process. It is therefore much more
   * efficient if you are in a position to pass a ListIterator to the constructor with it's current
   * read point as close to the end of the List as possible.
   * 
   * @param listIterator
   *          the ListIterator that should be traversed backwards.
   */
  public ReverseListIterator(ListIterator<T> listIterator,
                             boolean shouldAdvanceToEnd)
  {
    this(shouldAdvanceToEnd ? advanceToEnd(listIterator) : listIterator);
  }

  /**
   * Create an Iterator that moves backwards through the existing Iterator. This is the least
   * efficient way of creating a ReverseListIterator because it has to perform the following steps:-
   * <ul>
   * <li>Create an internal ArrayList
   * <li>Traverse the source iterator to its end, dropping the extracted values into the ArrayList.
   * Note that values <i>before</i> the current point in the source iterator will not be included
   * <li>Create a ListIterator of this cached ArrayList and use this for the reverse lookup.
   * </ul>
   * 
   * @param iterator
   *          The iterator that needs to be reversed.
   */
  public ReverseListIterator(Iterator<T> iterator)
  {
    List<T> tempStore = new ArrayList<T>();
    while (iterator.hasNext()) {
      tempStore.add(iterator.next());
    }
    _listIterator = tempStore.listIterator(tempStore.size());
  }

  /**
   * Construct an Iterator that moves backwards across all the information held in the given list.
   * This is optimised to ensure maximum speed of creation.
   * 
   * @param list
   *          The source list that needs to be traversed backwards.
   */
  public ReverseListIterator(List<T> list)
  {
    this(list.listIterator(list.size()));
  }

  private static <T extends Object> ListIterator<T> advanceToEnd(ListIterator<T> listIterator)
  {
    while (listIterator.hasNext()) {
      listIterator.next();
    }
    return listIterator;
  }

  @Override
  public boolean hasNext()
  {
    return _listIterator.hasPrevious();
  }

  @Override
  public boolean hasPrevious()
  {
    return _listIterator.hasNext();
  }

  @Override
  public T next()
  {
    return _listIterator.previous();
  }

  @Override
  public T previous()
  {
    return _listIterator.next();
  }

  @Override
  public int nextIndex()
  {
    return _listIterator.previousIndex();
  }

  @Override
  public int previousIndex()
  {
    return _listIterator.nextIndex();
  }

  /**
   * @throws UnsupportedOperationException
   *           always
   */
  @Override
  public void set(T o)
  {
    throw new UnsupportedOperationException();
  }

  /**
   * @throws UnsupportedOperationException
   *           always
   */
  @Override
  public void remove()
  {
    throw new UnsupportedOperationException();
  }

  /**
   * @throws UnsupportedOperationException
   *           always
   */
  @Override
  public void add(T o)
  {
    throw new UnsupportedOperationException();
  }

}
