package org.cord.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The IsoCountryCodeGroupingManager is responsible for maintaining a collection of
 * IsoCountryCodeGroup items and retrieving the appropriate group(s) for a given IsoCountryCode
 * 
 * @see IsoCountryCode
 * @see IsoCountryCodeGrouping
 */
public class IsoCountryCodeGroupingManager
{
  private final SortedSet<IsoCountryCodeGrouping> __groupings =
      new TreeSet<IsoCountryCodeGrouping>();

  private final SortedSet<IsoCountryCodeGrouping> __publicGroupings =
      Collections.unmodifiableSortedSet(__groupings);

  /**
   * Add the specified IsoCountryCodeGrouping to the manager. The grouping will automatically be
   * locked before it is added to the manager to ensure that it is a non-moving target.
   */
  public void add(IsoCountryCodeGrouping grouping)
  {
    if (grouping != null) {
      grouping.lock();
      __groupings.add(grouping);
    }
  }

  /**
   * @return unmodifiable SortedSet of IsoCountryCodeGrouping
   * @see IsoCountryCodeGrouping
   */
  public SortedSet<IsoCountryCodeGrouping> getGroupings()
  {
    return __publicGroupings;
  }

  /**
   * Get a list of IsoCountryCodeGroupings that contain the specified country.
   */
  public Iterator<IsoCountryCodeGrouping> getGroupings(IsoCountryCode country)
  {
    return new GroupingSearch(__publicGroupings.iterator(), country);
  }

  /**
   * SkippingIterator that acts on lists of IsoCountryCodeGrouping items and only passes ones that
   * include a specified IsoCountryCode.
   */
  public class GroupingSearch
    extends SkippingIterator<IsoCountryCodeGrouping>
  {
    private final IsoCountryCode __country;

    public GroupingSearch(Iterator<IsoCountryCodeGrouping> iterator,
                          IsoCountryCode country)
    {
      super(iterator);
      __country = country;
    }

    @Override
    protected boolean shouldSkip(IsoCountryCodeGrouping grouping)
    {
      if (grouping == null) {
        return true;
      }
      return !grouping.contains(__country);
    }
  }
}
