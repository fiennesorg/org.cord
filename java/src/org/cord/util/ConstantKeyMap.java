package org.cord.util;

import java.util.AbstractList;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReferenceArray;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

/**
 * Lightweight implementation of a Map that utilises an external ImmutableMap of Keys to indexes to
 * enable us to store our values in an Array. Not used in anger yet, and probably requires further
 * testing.
 * 
 * @author alex
 */
public class ConstantKeyMap<K, V>
  implements Map<K, V>
{
  private final ImmutableMap<K, Integer> __keys;
  private final AtomicReferenceArray<V> __values;

  public ConstantKeyMap(ImmutableMap<K, Integer> keys)
  {
    __keys = Preconditions.checkNotNull(keys, "keys");
    __values = new AtomicReferenceArray<V>(__keys.size());
  }

  @Override
  public void clear()
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __keys.containsKey(key);
  }

  @Override
  public boolean containsValue(Object value)
  {
    int l = __values.length();
    for (int i = 0; i < l; i++) {
      if (Objects.equal(value, __values.get(i))) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Set<Map.Entry<K, V>> entrySet()
  {
    return new AbstractSet<Map.Entry<K, V>>() {
      @Override
      public Iterator<Map.Entry<K, V>> iterator()
      {
        return new Iterator<Map.Entry<K, V>>() {
          private final Iterator<Map.Entry<K, Integer>> __i = __keys.entrySet().iterator();

          @Override
          public boolean hasNext()
          {
            return __i.hasNext();
          }

          @Override
          public Map.Entry<K, V> next()
          {
            final Map.Entry<K, Integer> e = __i.next();
            return new Map.Entry<K, V>() {
              @Override
              public K getKey()
              {
                return e.getKey();
              }

              @Override
              public V getValue()
              {
                return __values.get(e.getValue().intValue());
              }

              @Override
              public V setValue(V value)
              {
                throw new UnsupportedOperationException();
              }
            };
          }

          @Override
          public void remove()
          {
            throw new UnsupportedOperationException();
          }
        };
      }

      @Override
      public int size()
      {
        return __values.length();
      }
    };
  }
  @Override
  public V get(Object key)
  {
    Integer i = __keys.get(key);
    return i == null ? null : __values.get(i.intValue());
  }

  @Override
  public boolean isEmpty()
  {
    return false;
  }

  @Override
  public Set<K> keySet()
  {
    return __keys.keySet();
  }

  @Override
  public V put(K key,
               V value)
  {
    Integer i = __keys.get(key);
    if (i == null) {
      throw new IllegalArgumentException(String.format("%s is not supported as a valid key in %s",
                                                       key,
                                                       this));
    }
    return __values.getAndSet(i.intValue(), value);
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m)
  {
    for (K key : m.keySet()) {
      put(key, m.get(key));
    }
  }

  @Override
  public V remove(Object key)
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public int size()
  {
    return __values.length();
  }

  @Override
  public Collection<V> values()
  {
    return new AbstractList<V>() {
      @Override
      public V get(int arg0)
      {
        return __values.get(arg0);
      }

      @Override
      public int size()
      {
        return ConstantKeyMap.this.size();
      }
    };
  }
}
