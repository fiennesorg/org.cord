package org.cord.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Collection of frequently used compound utility methods for operating on Collections.
 */
public final class CollectionUtil
{
  private static final CollectionUtil __instance = new CollectionUtil();

  public static final CollectionUtil getInstance()
  {
    return __instance;
  }

  private CollectionUtil()
  {
  }

  /**
   * Find the first index of a an object from a list starting at a specified point in the list.
   * 
   * @param list
   *          The list to search
   * @param object
   *          The object to search for
   * @param startIndex
   *          The start point for the searcH
   * @return The index of the object or -1 if the object cannot be found.
   * @throws IndexOutOfBoundsException
   *           If startIndex is outside the legal range for the list.
   */
  public final static int indexOf(List<?> list,
                                  Object object,
                                  int startIndex)
  {
    return list.subList(startIndex, list.size()).indexOf(object) + startIndex;
  }

  /**
   * Convert an array of Objects into an array of the toString() representations of the source
   * array.
   */
  public final static String[] toStringArray(Object[] objArray)
  {
    String[] strArray = new String[objArray.length];
    for (int i = 0; i < objArray.length; i++) {
      strArray[i] = objArray[i] == null ? null : objArray[i].toString();
    }
    return strArray;
  }

  /**
   * Convert a collection into a String array of it's contents.
   */
  public final static String[] toStringArray(Collection<?> collection)
  {
    Object[] objArray = collection.toArray();
    return toStringArray(objArray);
  }

  /**
   * Calculate the algabraic union of the two sets.
   */
  public final static <T> Set<T> union(Collection<? extends T> collection1,
                                       Collection<? extends T> collection2)
  {
    Set<T> result = new HashSet<T>();
    if (collection1 != null) {
      result.addAll(collection1);
    }
    if (collection2 != null) {
      result.addAll(collection2);
    }
    return result;
  }

  /**
   * @throws ClassCastException
   *           If there are non-Collection object in the iterator.
   */
  public final static <T> Set<T> union(Iterator<Collection<? extends T>> collections)
  {
    Set<T> result = new HashSet<T>();
    while (collections.hasNext()) {
      result.addAll(collections.next());
    }
    return result;
  }

  public static final <T> void removeIntersection(Collection<T> c1,
                                                  Collection<T> c2)
  {
    if (c1 == null || c2 == null) {
      return;
    }
    boolean oneIsSmallest = c1.size() < c2.size();
    Collection<T> larger = oneIsSmallest ? c2 : c1;
    Collection<T> smallest = oneIsSmallest ? c1 : c2;
    Iterator<T> i = smallest.iterator();
    while (i.hasNext()) {
      Object o = i.next();
      if (larger.remove(o)) {
        i.remove();
      }
    }
  }

  /**
   * Return the Set that contains all elements that are in both the supplied Collections.
   * 
   * @param fastContainsCollection
   *          If you have a Collection that has a faster contains() implementation then you should
   *          use this as the second argument. If both your Collections share the same contains()
   *          performance then it will be more efficient to use this variable for the larger of the
   *          two.
   * @return Immutable Set that is independent of the source collections.
   */
  public final static <T> Set<T> intersection(Collection<? extends T> c1,
                                              Collection<? extends T> fastContainsCollection)
  {
    if (c1 == null || fastContainsCollection == null || c1.size() == 0
        || fastContainsCollection.size() == 0) {
      return Collections.emptySet();
    }
    HashSet<T> intersection = new HashSet<T>();
    for (T o1 : c1) {
      if (fastContainsCollection.contains(o1)) {
        intersection.add(o1);
      }
    }
    return Collections.unmodifiableSet(intersection);
  }

  /**
   * Return true if there is at least one element that is in both collections.
   * 
   * @param fastContainsCollection
   *          If you have a Collection that has a faster contains() implementation then you should
   *          use this as the second argument. If both your Collections share the same contains()
   *          performance then it will be more efficient to use this variable for the larger of the
   *          two.
   */
  public final static boolean hasIntersection(Collection<?> c1,
                                              Collection<?> fastContainsCollection)
  {
    if (c1 == null || fastContainsCollection == null || c1.size() == 0
        || fastContainsCollection.size() == 0) {
      return false;
    }
    for (Object o1 : c1) {
      if (fastContainsCollection.contains(o1)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param collections
   *          Iterator of Collection Objects that are to have their intersection calculated.
   * @throws ClassCastException
   *           if any of the Objects in collections are not Collections
   */
  public final static <T> Set<T> intersection(Iterator<Collection<? extends T>> collections)
  {
    Set<T> result = new HashSet<T>();
    if (collections != null) {
      if (collections.hasNext()) {
        result.addAll(collections.next());
        if (collections.hasNext()) {
          while (collections.hasNext()) {
            result = intersection(result, collections.next());
            if (result.size() == 0) {
              return result;
            }
          }
        }
      }
    }
    return result;
  }

  /**
   * Initialise a new HashMap containing the data in the given keys and values array such that
   * keys[n] --> values[n]. If the length of either keys or values is not identical then only the
   * shorter of the two will be the length of the Map. The Map that is returned is mutable. If keys
   * has duplicate values then the latest values will take precedence. null values in keys will drop
   * the corresponding value from the values list.
   */
  public <K, V> Map<K, V> asMap(List<K> keys,
                                List<V> values)
  {
    int mapSize = Math.min(keys.size(), values.size());
    HashMap<K, V> map = new HashMap<K, V>(mapSize);
    for (int i = 0; i < mapSize; i++) {
      K key = keys.get(i);
      if (key != null) {
        map.put(key, values.get(i));
      }
    }
    return map;
  }

  /**
   * @deprecated in preference of {@link List#remove(Object)} - optimising around standard
   *             implementations is a bad idea.
   */
  @Deprecated
  public final static <T> boolean fastRemove(ArrayList<T> c,
                                             T o)
  {
    int count = c.size();
    for (int i = 0; i < count; i++) {
      if (c.get(i) == o) {
        c.remove(i);
        return true;
      }
    }
    return false;
  }

  public static <T> Collection<T> padNull(Collection<T> c)
  {
    if (c == null) {
      return Collections.emptyList();
    }
    return c;
  }

  /**
   * Generate a sublist of a List which is guaranteed to not throw IndexOutOfBoundsException. This
   * has the same behaviour as List.subList(from,to) with the exception that you will always get a
   * sensible result regardless of the index values that you give it.
   * 
   * @param <T>
   *          The type of the element in the List
   * @param source
   *          The List that you want to make a sublist of. Must be defined.
   * @param fromIndex
   *          The start index of the list. If less than zero or greater than the length of the list
   *          then this will be automatically constrained to a viable index point.
   * @param toIndex
   *          The end index of the list. If less than the constrained startIndex or more than the
   *          length of the list then this will be automatically constrained to a viable index
   *          point.
   * @return The appropriate sublist.
   * @see List#subList(int, int)
   */
  public static <T> List<T> safeSubList(List<T> source,
                                        int fromIndex,
                                        int toIndex)
  {
    fromIndex = Math.min(Math.max(fromIndex, 0), source.size());
    toIndex = Math.min(Math.max(toIndex, fromIndex), source.size());
    return source.subList(fromIndex, toIndex);
  }

  public static void join(StringBuilder buf,
                          IntCollection ints,
                          String separator)
  {
    IntIterator i = ints.iterator();
    if (i.hasNext()) {
      buf.append(i.nextInt());
      while (i.hasNext()) {
        buf.append(separator).append(i.nextInt());
      }
    }
  }
}
