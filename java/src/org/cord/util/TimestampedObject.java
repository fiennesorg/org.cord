package org.cord.util;

/**
 * A basic implementation of Timestamped that provides a binding between a non-null Object and a
 * timestamp.
 * 
 * @author alex
 * @param <T>
 *          The class of the value that this wraps
 */
public final class TimestampedObject<T>
  implements Timestamped
{
  private volatile long _timestamp;

  private volatile T _value;

  public TimestampedObject(T value)
  {
    setValue(value);
  }

  @Override
  public long getTimestamp()
  {
    return _timestamp;
  }

  @Override
  public void setTimestamp()
  {
    _timestamp = System.currentTimeMillis();
  }

  public T getValue()
  {
    return _value;
  }

  public void setValue(T value)
  {
    if (value == null) {
      throw new NullPointerException(this + " does not accept null values");
    }
    _value = value;
    setTimestamp();
  }

  @Override
  public String toString()
  {
    return _value.toString();
  }
}
