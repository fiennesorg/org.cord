package org.cord.util;

public class IntegerStringValidator
  extends NamedImpl
  implements StringValidator
{
  public final static String NAME = "IntegerStringValidator";

  private final boolean __autoStripChars;

  private final boolean __isOptional;

  /**
   * @param autoStripChars
   *          If true then if the inital parse of the supplied string fails, then all non-numeric
   *          chars will be stripped out and the parse re-tried. If this then succeeds then a
   *          StringValidatorException is thrown with the stripped value as the proposed value.
   * @param isOptional
   *          If true then a value of null or "" will be passed by the validate method. If the
   *          autoStripChars is true then the system will approve a stripped string that equals ""
   *          and throw this in the StringValidatorException as a proposed value.
   */
  public IntegerStringValidator(boolean autoStripChars,
                                boolean isOptional)
  {
    super(NAME);
    __autoStripChars = autoStripChars;
    __isOptional = isOptional;
  }

  private String stripChars(String string)
  {
    if (string == null) {
      return null;
    }
    StringBuffer result = new StringBuffer(string.length());
    for (int i = 0; i < string.length(); i++) {
      char c = string.charAt(i);
      if (Character.isDigit(c)) {
        result.append(c);
      }
    }
    return result.toString();
  }

  @Override
  public boolean isValid(String string)
  {
    if (string == null || string.length() == 0) {
      return __isOptional;
    }
    try {
      Integer.parseInt(string);
      return true;
    } catch (NumberFormatException nfEx) {
      return false;
    }
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    if (isValid(string)) {
      return string;
    }
    String proposedString = null;
    if (__autoStripChars) {
      String strippedString = stripChars(string);
      if (__isOptional && strippedString.length() == 0) {
        proposedString = strippedString;
      } else {
        try {
          Integer.parseInt(strippedString);
          proposedString = strippedString;
        } catch (NumberFormatException nf2Ex) {
        }
      }
    }
    throw new StringValidatorException("not an integer", string, proposedString);
  }
}
