package org.cord.util;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Gettable wrapper around a {@link JSONObject}
 * 
 * @author alex
 */
public class GettableJSONObject
  implements Gettable
{
  private final Optional<File> __parentDir;
  private final JSONObject __jObj;

  /**
   * @param parentDir
   *          The optional directory that will be used by {@link #getFile(Object)} if our JSONObject
   *          has got relative paths stored in it. Set to {@link Optional#absent()} if the values
   *          are absolute paths.
   * @param jObj
   */
  public GettableJSONObject(Optional<File> parentDir,
                            JSONObject jObj)
  {
    __parentDir = Preconditions.checkNotNull(parentDir);
    __jObj = Preconditions.checkNotNull(jObj);
  }

  private String toJsonKey(Object key)
  {
    return StringUtils.toString(key);
  }

  @Override
  public Object get(Object key)
  {
    return __jObj.opt(toJsonKey(key));
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __jObj.has(toJsonKey(key));
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    String k = toJsonKey(key);
    if (__jObj.has(k)) {
      return Boolean.valueOf(__jObj.optBoolean(k));
    }
    return null;
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    Object obj = get(key);
    if (obj instanceof JSONArray) {
      JSONArray jArray = (JSONArray) obj;
      ImmutableList.Builder<Object> builder = ImmutableList.builder();
      for (Object value : jArray) {
        builder.add(value);
      }
      return builder.build();
    }
    return obj == null ? null : GettableUtils.wrapValue(obj);
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    Collection<?> values = getValues(key);
    return values == null ? ImmutableList.of() : values;
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    ImmutableList.Builder<String> builder = ImmutableList.builder();
    Iterator<String> i = __jObj.keys();
    while (i.hasNext()) {
      builder.add(i.next());
    }
    return builder.build();
  }

  @Override
  public String getString(Object key)
  {
    return __jObj.optString(toJsonKey(key));
  }

  @Override
  public File getFile(Object key)
  {
    String path = getString(key);
    if (path != null) {
      return __parentDir.isPresent() ? new File(__parentDir.get(), path) : new File(path);
    }
    return null;
  }
}
