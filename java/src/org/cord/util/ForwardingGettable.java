package org.cord.util;

import java.io.File;
import java.util.Collection;

import com.google.common.base.Preconditions;

/**
 * A Gettable wrapper around a Gettable. While this may seem perverse it is actually useful if you
 * have control of the Gettable that is being wrapped and you don't want other people to have access
 * to it. This maintains the Gettable interface to it while ensuring that the outside world doesn't
 * tweak with where the values are coming from.
 * 
 * @author alex
 */
public class ForwardingGettable
  implements Gettable
{
  private final Gettable __wrapped;

  public ForwardingGettable(Gettable wrapped)
  {
    __wrapped = Preconditions.checkNotNull(wrapped, "wrapped");
  }

  @Override
  public String toString()
  {
    return "GettableWrapper(" + __wrapped + ")";
  }

  protected final Gettable getWrapped()
  {
    return __wrapped;
  }

  @Override
  public Object get(Object key)
  {
    return __wrapped.get(key);
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __wrapped.containsKey(key);
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    return __wrapped.getBoolean(key);
  }

  @Override
  public Collection<?> getValues(Object key)
  {
    return __wrapped.getValues(key);
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    return __wrapped.getPublicKeys();
  }

  @Override
  public String getString(Object key)
  {
    return __wrapped.getString(key);
  }

  @Override
  public File getFile(Object key)
  {
    return __wrapped.getFile(key);
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    return __wrapped.getPaddedValues(key);
  }
}
