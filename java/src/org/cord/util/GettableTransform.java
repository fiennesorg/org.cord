package org.cord.util;

import java.util.Collection;

import com.google.common.base.Preconditions;

/**
 * Wrapper around a Gettable that utilises a StringTransformMap to transparently transform String
 * values.
 * 
 * @author alex
 */
public class GettableTransform
  extends ForwardingGettable
  implements Gettable
{
  private final StringTransformMap __transforms;

  public GettableTransform(Gettable gettable,
                           StringTransformMap transforms)
  {
    super(gettable);
    __transforms = Preconditions.checkNotNull(transforms, "transforms");
  }

  /**
   * If the wrapped value is a String then pass it through the StringTransformMap.
   * 
   * @see StringTransformMap#transformObject(Object, Object)
   */
  @Override
  public Object get(Object key)
  {
    return __transforms.transformObject(key, getWrapped().get(key));
  }

  /**
   * Resolve the Collection in the wrapped Gettable and then pass the values through the
   * StringTransformMap.
   * 
   * @see StringTransformMap#transformCollection(Object, Collection)
   */
  @Override
  public Collection<?> getValues(Object key)
  {
    return __transforms.transformCollection(key, getWrapped().getValues(key));
  }

  /**
   * Pass the wrapped value through the StringTransformMap.
   * 
   * @see StringTransformMap#transformString(Object, String)
   */
  @Override
  public String getString(Object key)
  {
    return __transforms.transformString(key, getWrapped().getString(key));
  }

}
