package org.cord.util;

import com.google.common.base.Preconditions;

/**
 * StringValidator that ensures that the String passed to it may be parsed as an integer that can be
 * used as an index for a specified array, with optional checking to make sure that it always has a
 * consistent number of digits (ie has leading 0's).
 */
public class ArrayIndexValidator
  extends NamedImpl
  implements StringValidator
{
  /**
   * "ArrayIndexValidator"
   */
  public final static String NAME = "ArrayIndexValidator";

  /**
   * value for digitCount (0) that makes the ArrayIndexValidator ignore the number of digits in the
   * value.
   */
  public final static int DIGITCOUNT_IGNORE = 0;

  private final String[] __array;

  private final int __digitCount;

  /**
   * @param array
   *          The array which the index must validate against.
   * @param digitCount
   *          The number of digits that the must be in the value. If this is irrelevant then utilise
   *          DIGITCOUNT_IGNORE.
   * @see #DIGITCOUNT_IGNORE
   */
  public ArrayIndexValidator(String[] array,
                             int digitCount)
  {
    super(NAME);
    Preconditions.checkNotNull(array, "array");
    __array = array;
    __digitCount = Math.max(DIGITCOUNT_IGNORE, digitCount);
  }

  /**
   * Create an ArrayIndexValidator that is not fussed about the number of digits in the value.
   * 
   * @see #DIGITCOUNT_IGNORE
   */
  public ArrayIndexValidator(String[] array)
  {
    this(array,
         DIGITCOUNT_IGNORE);
  }

  public String getError(String string)
  {
    try {
      int value = Integer.parseInt(string);
      if (value < 0 || value > __array.length) {
        return "Outside array range";
      }
      if (__digitCount != DIGITCOUNT_IGNORE && string.length() != __digitCount) {
        return "Invalid length";
      }
    } catch (NumberFormatException nfEx) {
      return "Not a number";
    }
    return null;
  }

  @Override
  public boolean isValid(String string)
  {
    return getError(string) == null;
  }

  @Override
  public String validate(String string)
      throws StringValidatorException
  {
    String error = getError(string);
    if (error == null) {
      return string;
    } else {
      throw new StringValidatorException(error, string);
    }
  }
}
