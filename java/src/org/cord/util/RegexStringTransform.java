package org.cord.util;

import java.util.regex.Pattern;

import com.google.common.base.Preconditions;

/**
 * StringTransform that performs a replaceAll operation on all matches to a given regex.
 * 
 * @author alex
 * @see java.util.regex.Matcher#replaceAll(java.lang.String)
 */
public class RegexStringTransform
  implements StringTransform
{
  private final Pattern __pattern;

  private final String __replacement;

  public RegexStringTransform(Pattern pattern,
                              String replacement)
  {
    __pattern = pattern;
    __replacement = Preconditions.checkNotNull(replacement, "replacement");
  }

  public RegexStringTransform(String regex,
                              String replacement)
  {
    this(Pattern.compile(regex),
         replacement);
  }

  @Override
  public String transform(String source)
  {
    return __pattern.matcher(source).replaceAll(__replacement);
  }
}
