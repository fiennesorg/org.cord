package org.cord.util;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.cord.node.requires.RequirementMgr;
import org.joda.time.DateTime;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public final class HtmlUtils
{
  private static volatile HtmlUtilsTheme __currentTheme = new HtmlUtilsOriginal();

  public static HtmlUtilsTheme getTheme()
  {
    return __currentTheme;
  }

  public static void setTheme(HtmlUtilsTheme theme)
  {
    __currentTheme = Preconditions.checkNotNull(theme);
  }

  public static final String CSS_W_DATE = "w_date";

  public static void openWidgetDiv(Appendable buf,
                                   String className,
                                   Object... ids)
      throws IOException
  {
    getTheme().openWidgetDiv(buf, className, ids);
  }

  public static void openLabelWidget(Appendable buf,
                                     String label,
                                     boolean isCompulsory,
                                     Object... ids)
      throws IOException
  {
    getTheme().openLabelWidget(buf, label, isCompulsory, ids);
  }

  public static void openLabelWidget(Appendable buf,
                                     String label,
                                     String widget,
                                     boolean isCompulsory,
                                     Object... ids)
      throws IOException
  {
    getTheme().openLabelWidget(buf, label, widget, isCompulsory, ids);
  }

  /**
   * @param contents
   *          The visible contents of the label. Note that this value is currently not escaped or
   *          validated in any way as inline markup inside a label is permitted in HTML.
   */
  public static void label(Appendable buf,
                           String contents,
                           boolean isCompulsory,
                           Object... ids)
      throws IOException
  {
    getTheme().label(buf, contents, isCompulsory, ids);
  }

  /**
   * @param value
   *          The optional data that will be placed inside the w_value div. This isn't escaped at
   *          all so it is possible to nest active HTML inside the div, but this does mean that the
   *          responsibility of ensuring that the HTML is well-formed rests with the invoking class.
   *          If the value is null then the behaviour will be dependable on the Appendable buf that
   *          is passed in, but this will normally be to render the word "null"
   */
  public static void value(Appendable buf,
                           Object value,
                           Object... ids)
      throws IOException
  {
    getTheme().value(buf, value, ids);
  }

  public static void errorValue(Appendable buf,
                                String error,
                                Object... ids)
      throws IOException
  {
    getTheme().errorValue(buf, error, ids);

  }

  public static void labelErrorValue(Appendable buf,
                                     String label,
                                     Object value,
                                     Object... ids)
      throws IOException
  {
    getTheme().labelErrorValue(buf, label, value, ids);
  }

  /**
   * Append a label and static value to the Form.
   */
  public static void labelValue(Appendable buf,
                                String label,
                                Object value,
                                Object... ids)
      throws IOException
  {
    getTheme().labelValue(buf, label, value, ids);
  }

  public static void textWidget(String autocomplete,
                                Appendable buf,
                                String value,
                                Object... ids)
      throws IOException
  {
    getTheme().textWidget(autocomplete, buf, value, ids);
  }

  /**
   * Create an {@link #autocompleteWidget(Appendable, String, Iterable, Object...)} with the
   * appropriate labelling. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on
   * the file dependencies, but this is the responsibility of the invoking method.
   */
  public static void autocomplete(Appendable buf,
                                  String label,
                                  String value,
                                  boolean isCompulsory,
                                  Iterable<String> options,
                                  Object... ids)
      throws IOException
  {
    getTheme().autocomplete(buf, label, value, isCompulsory, options, ids);
  }

  /**
   * Invoke {@link #autocompleteInputWidget(Appendable, String, Iterable, Object...)} wrapped in a
   * widget div. This needs to have had {@link RequirementMgr#KEY_QUERY_UI} registered on the file
   * dependencies, but this is the responsibility of the invoking method.
   */
  public static void autocompleteWidget(Appendable buf,
                                        String value,
                                        Iterable<String> options,
                                        Object... ids)
      throws IOException
  {
    getTheme().autocompleteWidget(buf, value, options, ids);
  }

  /**
   * Render a {@link #textInputWidget(String, Appendable, String, Object...)} with the appropriate
   * javascript to give autocompletion. This needs to have had {@link RequirementMgr#KEY_QUERY_UI}
   * registered on the file dependencies, but this is the responsibility of the invoking method.
   */
  public static void autocompleteInputWidget(Appendable buf,
                                             String value,
                                             Iterable<String> options,
                                             Object... ids)
      throws IOException
  {
    getTheme().autocompleteInputWidget(buf, value, options, ids);
  }

  public static void textInputWidget(String autocomplete,
                                     Appendable buf,
                                     String value,
                                     Object... ids)
      throws IOException
  {
    getTheme().textInputWidget(autocomplete, buf, value, ids);
  }

  public static void textInputWidget(String autocomplete,
                                     Appendable buf,
                                     String cssClass,
                                     String value,
                                     Object... ids)
      throws IOException
  {
    getTheme().textInputWidget(autocomplete, buf, cssClass, value, ids);
  }

  public static void text(String autocomplete,
                          Appendable buf,
                          String label,
                          String value,
                          boolean isCompulsory,
                          Object... ids)
      throws IOException
  {
    getTheme().text(autocomplete, buf, label, value, isCompulsory, ids);
  }

  public static Date parseDateSelector(Object date)
  {
    return getTheme().parseDateSelector(date);
  }
  
  public static String formatDateSelector(Date date)
  {
    return getTheme().formatDateSelector(date);
  }

  public static void jodaDate(Appendable buf,
                              Map<Object, Object> context,
                              String label,
                              Optional<DateTime> value,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    getTheme().jodaDate(buf, context, label, value, isCompulsory, ids);
  }

  public static void jodaDateWidget(Appendable buf,
                                    Map<Object, Object> context,
                                    Optional<DateTime> value,
                                    Object... ids)
      throws IOException
  {
    getTheme().jodaDateWidget(buf, context, value, ids);
  }

  public static void dateSelector(Appendable buf,
                                  Map<Object, Object> context,
                                  String label,
                                  Date value,
                                  Object... ids)
      throws IOException
  {
    getTheme().dateSelector(buf, context, label, value, ids);
  }

  public static Date parseDateTimeSelector(Object dateTime)
  {
    return getTheme().parseDateTimeSelector(dateTime);
  }

  public static Optional<DateTime> jodaDateParse(String textDate)
  {
    return getTheme().jodaDateParse(textDate);
  }

  public static String jodaDateFormat(Optional<DateTime> date)
  {
    return getTheme().jodaDateFormat(date);
  }

  public static void dateTimeSelector(Appendable buf,
                                      Map<Object, Object> context,
                                      String label,
                                      Date value,
                                      Object... ids)
      throws IOException
  {
    getTheme().dateTimeSelector(buf, context, label, Optional.fromNullable(value), ids);
  }

  public static void jodaTime(Appendable buf,
                              Map<Object, Object> context,
                              String label,
                              Optional<DateTime> value,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    getTheme().jodaTime(buf, context, label, value, isCompulsory, ids);
  }

  public static Optional<DateTime> jodaTimeParse(String textTime)
  {
    return getTheme().jodaTimeParse(textTime);
  }

  public static String jodaTimeFormat(Optional<DateTime> time)
  {
    return getTheme().jodaTimeFormat(time);
  }

  public static void jodaTimeWidget(Appendable buf,
                                    Map<Object, Object> context,
                                    Optional<DateTime> value,
                                    Object... ids)
      throws IOException
  {
    getTheme().jodaTimeWidget(buf, context, value, ids);
  }

  public static void dateTimeSelectorWidget(Appendable buf,
                                            Map<Object, Object> context,
                                            Date value,
                                            Object... ids)
      throws IOException
  {
    getTheme().dateTimeSelectorWidget(buf, context, Optional.fromNullable(value), ids);
  }

  public static void password(String autocomplete,
                              Appendable buf,
                              String label,
                              String value,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    getTheme().password(autocomplete, buf, label, value, isCompulsory, ids);
  }

  public static void textareaWidget(Appendable buf,
                                    String value,
                                    int cols,
                                    int rows,
                                    boolean isCompulsory,
                                    Object... ids)
      throws IOException
  {
    getTheme().textareaWidget(buf, value, cols, rows, isCompulsory, ids);
  }

  public static void textarea(Appendable buf,
                              String label,
                              String value,
                              int cols,
                              int rows,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    getTheme().textarea(buf, label, value, cols, rows, isCompulsory, ids);
  }

  public static void openSelect(Appendable buf,
                                boolean isMultiple,
                                boolean shouldReloadOnChange,
                                boolean isCompulsory,
                                Object... ids)
      throws IOException
  {
    getTheme().openSelect(buf, isMultiple, shouldReloadOnChange, isCompulsory, ids);
  }

  public static void openSelectWidget(Appendable buf,
                                      String cssClass,
                                      boolean isMultiple,
                                      boolean shouldReloadOnChange,
                                      boolean isCompulsory,
                                      Object... ids)
      throws IOException
  {
    getTheme().openSelectWidget(buf, cssClass, isMultiple, shouldReloadOnChange, isCompulsory, ids);
  }

  /**
   * @param valueObj
   *          The value that will be submitted if this option is selected. Will be converted to a
   *          safe HTML value.
   * @param contentsObj
   *          The human visible data that will be used to represent this value. Will be converted to
   *          a safe HTML value
   * @param isSelected
   *          Whether or not the value should be selected automatically in the GUI
   * @see StringUtils#toHtmlValue(Object)
   */
  public static void option(Appendable buf,
                            Object valueObj,
                            Object contentsObj,
                            boolean isSelected)
      throws IOException
  {
    getTheme().option(buf, valueObj, contentsObj, isSelected);
  }

  public static void closeSelectWidget(Appendable buf)
      throws IOException
  {
    getTheme().closeSelectWidget(buf);
  }

  /**
   * @param summary
   *          The optional summary which will be included between the close select and the close
   *          div. There is no filtering applied to this summary as it may contain HTML
   */
  public static void closeSelect(Appendable buf,
                                 String summary)
      throws IOException
  {
    getTheme().closeSelect(buf, summary);
  }

  /**
   * Render an input field of type checkbox into a containinger value div in the output buffer.
   */
  public static void checkboxInput(Appendable buf,
                                   String value,
                                   boolean isChecked,
                                   boolean shouldReloadOnChange,
                                   boolean isCompulsory,
                                   Object... ids)
      throws IOException
  {
    getTheme().checkboxInput(buf, value, isChecked, shouldReloadOnChange, isCompulsory, ids);
  }

  /**
   * Render an input field of type checkbox into the output buffer.
   * 
   * @param value
   *          the HTML value that will be submitted if the checkbox is checked. If null then the
   *          string "true" will be utilised.
   */
  public static void checkboxInputWidget(Appendable buf,
                                         String value,
                                         boolean isChecked,
                                         boolean shouldReloadOnChange,
                                         boolean isCompulsory,
                                         Object... ids)
      throws IOException
  {
    getTheme().checkboxInputWidget(buf, value, isChecked, shouldReloadOnChange, isCompulsory, ids);
  }

  public static void radio(Appendable buf,
                           String name,
                           String value,
                           boolean isChecked,
                           String label,
                           Object... ids)
      throws IOException
  {
    getTheme().radio(buf, name, value, isChecked, label, ids);
  }

  public static void openRadios(Appendable buf,
                                String label)
      throws IOException
  {
    getTheme().openRadios(buf, label);
  }

  public static void closeRadios(Appendable buf)
      throws IOException
  {
    getTheme().closeRadios(buf);
  }

  /**
   * Render an input field of type checkbox into the output buffer.
   */
  public static void checkboxInputWidget(Appendable buf,
                                         String value,
                                         boolean isChecked,
                                         String confirmOnClickMessage,
                                         boolean shouldReloadOnChange,
                                         boolean isCompulsory,
                                         Object... ids)
      throws IOException
  {
    getTheme().checkboxInputWidget(buf,
                                   value,
                                   isChecked,
                                   confirmOnClickMessage,
                                   shouldReloadOnChange,
                                   isCompulsory,
                                   ids);
  }

  public static void checkbox(Appendable buf,
                              String label,
                              String value,
                              boolean isChecked,
                              boolean shouldReloadOnChange,
                              boolean isCompulsory,
                              Object... ids)
      throws IOException
  {
    getTheme().checkbox(buf, label, value, isChecked, shouldReloadOnChange, isCompulsory, ids);
  }

  public static void fileWidget(Appendable buf,
                                Object... ids)
      throws IOException
  {
    getTheme().fileWidget(buf, ids);
  }

  /**
   * Nest an input button widget inside the appropriate divs with a label.
   */
  public static void submitButton(Appendable buf,
                                  String value,
                                  String onclick,
                                  String onmouseover,
                                  String onmouseout)
      throws IOException
  {
    getTheme().submitButton(buf, value, onclick, onmouseover, onmouseout);
  }

  public static void submit(Appendable buf,
                            String value,
                            String onclick)
      throws IOException
  {
    getTheme().submit(buf, value, onclick);
  }

  public static void errorBlock(Appendable buf,
                                String error,
                                String classNames)
      throws IOException
  {
    getTheme().errorBlock(buf, error, classNames);
  }

  /**
   * Invoke {@link HtmlUtilsTheme#visible(Appendable, String, Object, Object, Object...)} for the
   * currently active theme.
   */
  public static void visible(Appendable buf,
                             String label,
                             Object visibleValue,
                             Object submitValue,
                             Object... ids)
      throws IOException
  {
    getTheme().visible(buf, label, visibleValue, submitValue, ids);
  }
}
