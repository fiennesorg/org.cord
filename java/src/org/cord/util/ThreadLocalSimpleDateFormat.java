package org.cord.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ThreadLocalSimpleDateFormat
{
  private final SimpleDateFormat __masterFormat;

  private final ThreadLocal<SimpleDateFormat> __localFormat = new ThreadLocal<SimpleDateFormat>() {
    @Override
    protected SimpleDateFormat initialValue()
    {
      return (SimpleDateFormat) __masterFormat.clone();
    }
  };

  public ThreadLocalSimpleDateFormat(String format)
  {
    __masterFormat = new SimpleDateFormat(format);
  }

  public ThreadLocalSimpleDateFormat(String format,
                                     Locale locale)
  {
    __masterFormat = new SimpleDateFormat(format, locale);
  }

  public SimpleDateFormat get()
  {
    return __localFormat.get();
  }

  /**
   * If the given date is defined then format it using this format otherwise return null.
   * 
   * @param date
   *          optional date
   * @return optional formatted date
   */
  public String optFormat(Date date)
  {
    if (date == null) {
      return null;
    }
    return get().format(date);
  }
}
