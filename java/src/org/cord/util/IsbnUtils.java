package org.cord.util;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

/**
 * Static utility methods for manipulating Strings that represent ISBN-10 and ISBN-13 codes.
 * 
 * @author alex
 */
public final class IsbnUtils
{
  /**
   * January 1st 2007 (1167609600000). booted during the static method
   */
  public static final Date JAN07;
  static {
    Calendar c = Calendar.getInstance();
    c.clear();
    c.set(2007, 0, 1, 0, 0, 0);
    JAN07 = c.getTime();
  }

  private IsbnUtils()
  {
  }

  public static String chooseIsbn(String isbn10,
                                  String isbn13,
                                  Date pubDate)
  {
    if (pubDate == null) {
      return getIsbn13(isbn10, isbn13);
    }
    if (JAN07.compareTo(pubDate) == 1) {
      return getIsbn10(isbn10, isbn13);
    } else {
      return getIsbn13(isbn10, isbn13);
    }
  }

  public static String getIsbn10(String isbn10,
                                 String isbn13)
  {
    if (Strings.isNullOrEmpty(isbn10)) {
      return isbn13to10(isbn13);
    } else {
      return isbn10;
    }
  }

  public static String getIsbn13(String isbn10,
                                 String isbn13)
  {
    if (Strings.isNullOrEmpty(isbn13)) {
      return isbn10to13(isbn10);
    } else {
      return isbn13;
    }
  }

  private static final Pattern __nonIsbnCharacters = Pattern.compile("[^0123456789X]");

  /**
   * Convert isbn to upper case and then strip out all characters apart from 0-9 and X.
   */
  public static final String sanitiseIsbnChars(String isbn)
  {
    isbn = isbn.toUpperCase();
    Matcher matcher = __nonIsbnCharacters.matcher(isbn);
    isbn = matcher.replaceAll("");
    return isbn;
  }

  private static int[] __isbn13Weightings = new int[] { 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1 };

  public static char isbn13CheckDigit(CharSequence source)
  {
    int result = 0;
    for (int i = 0; i < 12; i++) {
      result += __isbn13Weightings[i] * (source.charAt(i) - '0');
    }
    result = 10 - result % 10;
    switch (result) {
      case 10:
        return '0';
      default:
        return (char) ('0' + result);
    }
  }

  private static int[] __isbn10Weightings = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

  public static char isbn10CheckDigit(CharSequence source)
  {
    int result = 0;
    for (int i = 0; i < 9; i++) {
      result += __isbn10Weightings[i] * (source.charAt(i) - '0');
    }
    result = result % 11;
    switch (result) {
      case 11:
        return 'X';
      default:
        return (char) ('0' + result);
    }
  }

  public static String isbn10to13(String isbn10)
  {
    isbn10 = sanitiseIsbnChars(isbn10);
    if (isbn10.length() != 10) {
      throw new IllegalArgumentException("Not an ISBN-10:" + isbn10);
    }
    StringBuilder isbn13 = new StringBuilder();
    isbn13.append("978").append(isbn10.substring(0, 9)).append(isbn13CheckDigit(isbn13));
    return isbn13.toString();
  }

  public static final Pattern __isbn10Pattern = Pattern.compile("[0-9]{9}[0-9xX](?=[\\W])?");

  public static String allIsbn10sTo13s(String source)
  {
    if (Strings.isNullOrEmpty(source)) {
      return source;
    }
    Matcher m = __isbn10Pattern.matcher(source);
    if (!m.find()) {
      return source;
    }
    m.reset();
    StringBuilder result = new StringBuilder(source.length() + 32);
    int i = 0;
    while (m.find()) {
      result.append(source.substring(i, m.start()));
      result.append(isbn10to13(m.group()));
      i = m.end();
    }
    result.append(source.substring(i));
    return result.toString();
  }

  public static void main(String[] args)
  {
    System.out.println(allIsbn10sTo13s("http://127.0.0.1/Books/The-Private-Memoirs-And-Confessions-Of-A-Justified-Sinner-0141441534"));
  }

  public static String isbn13to10(String isbn13)
  {
    isbn13 = sanitiseIsbnChars(isbn13);
    if (isbn13.length() != 13) {
      throw new IllegalArgumentException("Not an ISBN-13:" + isbn13);
    }
    if (!isbn13.startsWith("978")) {
      throw new IllegalArgumentException("Only ISBN-13 starting 978 can be converted to ISBN-10: "
                                         + isbn13);
    }
    isbn13 = isbn13.substring(3, 12);
    return isbn13 + isbn10CheckDigit(isbn13);
  }
}
