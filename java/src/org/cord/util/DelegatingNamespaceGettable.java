package org.cord.util;

import com.google.common.base.Preconditions;

/**
 * A Gettable that holds a number of NamespacedGettable views onto a given source Gettable. When it
 * is asked for things, it will query each of the Gettables in turn until it finds one that has a
 * namespaced parameter that matches the requested name.
 * 
 * @author alex
 */
public final class DelegatingNamespaceGettable
  extends ForwardingGettable
{
  private final NamespacedNameCacheManager __manager;

  private final Gettable __source;

  public DelegatingNamespaceGettable(NamespacedNameCacheManager manager,
                                     Gettable source,
                                     boolean isThreadSafe)
  {
    super(new DelegatingGettable(isThreadSafe));
    __manager = Preconditions.checkNotNull(manager, "manager");
    __source = Preconditions.checkNotNull(source, "source");
  }

  private DelegatingGettable getDelegatingGettable()
  {
    return (DelegatingGettable) getWrapped();
  }

  /**
   * Add an alternative namespaced view to the list of namespaces to try.
   * 
   * @param namespace
   *          The string that will be prepended to keys used to retrieve items from the Gettable.
   */
  public void addNamespace(String namespace)
  {
    getDelegatingGettable().addGettable(new NamespaceGettable(__manager, __source, namespace));
  }

  /**
   * Add an alternative namespaced view to the list of namespaces to try.
   * 
   * @param nameCache
   *          The cache that is already initialised to return the namespaced versions of keys used
   *          to retrieve items from the Gettable
   */
  public void addNamespace(NamespacedNameCache nameCache)
  {
    getDelegatingGettable().addGettable(new NamespaceGettable(nameCache, __source));
  }
}
