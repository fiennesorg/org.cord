package org.cord.util;

import java.util.Iterator;

/**
 * Wrapper around an Iterator that skips any element in the iteration that is equal to a supplied
 * object.
 */
public class SingleSkippingIterator<T>
  extends SkippingIterator<T>
{
  private final T _skipMe;

  public SingleSkippingIterator(Iterator<T> iterator,
                                T skipMe)
  {
    super(iterator);
    _skipMe = skipMe;
  }

  @Override
  protected boolean shouldSkip(T object)
  {
    if (_skipMe == null) {
      return object == null;
    } else {
      return _skipMe.equals(object);
    }
  }
}
