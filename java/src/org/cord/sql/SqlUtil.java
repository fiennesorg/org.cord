package org.cord.sql;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.cord.util.Dates;
import org.cord.util.IoUtil;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.StringUtils;

import com.google.common.collect.Maps;

/**
 * Utility class for making static data manipulations that are useful when dealing with SQL
 * databases.
 */
public final class SqlUtil
  implements SingletonShutdownable
{
  private static SqlUtil _instance = new SqlUtil();

  private final static Map<Character, String> __noHtmlMap = new HashMap<Character, String>();

  private final static String __noHtmlSearchTokens = "<>";

  private final static Map<Character, String> __textToHtmlMap = new HashMap<Character, String>();

  private final static SimpleDateFormat DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  private final static SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");

  private final static String __textToHtmlSearchTokens = "\n<>";
  static {
    __noHtmlMap.put(new Character('<'), "&lt;");
    __noHtmlMap.put(new Character('>'), "&gt;");
    __textToHtmlMap.put(new Character('<'), "&lt;");
    __textToHtmlMap.put(new Character('>'), "&gt;");
    __textToHtmlMap.put(new Character('\n'), "<br />");
  }

  public static String toDatetimeString(Date date)
  {
    return Dates.format(DATETIME, date, null);
  }

  public static String toDateString(Date date)
  {
    return Dates.format(DATE, date, null);
  }

  /**
   * Convert a string into a 'safe' sql string by escaping all single quotes ("'" --> "''") and
   * backslashes ("\" --> "\\") thereby making the complete string be treated as an atomic SQL
   * parameter.
   * 
   * @param source
   *          The string to be processed. If null then nothing is appended to buf.
   * @return The StringBuilder to allow chaining.
   */
  public static StringBuilder escapeQuotes(StringBuilder buf,
                                           String source)
  {
    if (source != null) {
      int l = source.length();
      for (int i = 0; i < l; i++) {
        char c = source.charAt(i);
        switch (c) {
          case '\'':
            buf.append("''");
            break;
          case '\\':
            buf.append("\\\\");
            break;
          default:
            buf.append(c);
        }
      }
    }
    return buf;
  }

  public static StringBuilder textToHtml(StringBuilder buffer,
                                         String source)
  {
    return replaceChars(buffer, source, __textToHtmlSearchTokens, __textToHtmlMap);
  }

  /**
   * Remove all of the instances of "&lt;" and "&gt;" replacing them with the safe entities of
   * "&amp;lt;" and "&amp;gt;" thereby ensuring the HTML cannot be embedded in a String.
   */
  public static StringBuilder noHtml(StringBuilder buffer,
                                     String source)
  {
    return replaceChars(buffer, source, __noHtmlSearchTokens, __noHtmlMap);
  }

  private SqlUtil()
  {
    SingletonShutdown.registerSingleton(this);
  }

  /**
   * Get an instance of the SqlUtil for dropping into webmacro contexts.
   * 
   * @return static instance of SqlUtil
   */
  public static SqlUtil getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  /**
   * convert a String into a safe format where it can be used as a value in constructing SQL command
   * strings including the surrounding quotes. The following operations are performed:-
   * <ul>
   * <li>Escape internal SQL single quotes
   * <li>Wrap the String with single quotes
   * </ul>
   * 
   * @param source
   *          The string to process
   * @return processed string
   */
  public static StringBuilder toSafeSqlString(StringBuilder buf,
                                              Object source)
  {
    buf.append('\'');
    escapeQuotes(buf, StringUtils.toString(source));
    buf.append('\'');
    return buf;
  }

  /**
   * Convert a String to a safe format with surrounding % and ' quotes.
   */
  public static StringBuilder toSafeSqlLikeString(StringBuilder buf,
                                                  Object source)
  {
    buf.append("'%");
    escapeQuotes(buf, StringUtils.toString(source));
    buf.append("%'");
    return buf;
  }

  public static StringBuilder replaceChar(StringBuilder buffer,
                                          String source,
                                          char find,
                                          String replace)
  {
    if (source.indexOf(find) != -1) {
      String searchToken = Character.toString(find);
      StringTokenizer list = new StringTokenizer(source, searchToken, true);
      while (list.hasMoreTokens()) {
        String nextToken = list.nextToken();
        if (nextToken.equals(searchToken)) {
          buffer.append(replace);
        } else {
          buffer.append(nextToken);
        }
      }
    } else {
      buffer.append(source);
    }
    return buffer;
  }

  public static String replaceChar(String source,
                                   char find,
                                   String replace)
  {
    if (source.indexOf(find) != -1) {
      return replaceChar(new StringBuilder(), source, find, replace).toString();
    } else {
      return source;
    }
  }

  public static StringBuilder replaceChars(StringBuilder buffer,
                                           String source,
                                           String searchTokens,
                                           Map<Character, String> replacementRules)
  {
    if (source == null) {
      return buffer;
    }
    StringTokenizer tokens = new StringTokenizer(source, searchTokens, true);
    while (tokens.hasMoreTokens()) {
      String nextToken = tokens.nextToken();
      int tokenIndex = searchTokens.indexOf(nextToken);
      switch (tokenIndex) {
        case -1:
          buffer.append(nextToken);
          break;
        default:
          buffer.append(replacementRules.get(Character.valueOf(searchTokens.charAt(tokenIndex))));
      }
    }
    return buffer;
  }

  public static String backupDb(String database,
                                String dumpDbPath,
                                File outputDirectory)
      throws IOException
  {
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyMMdd.HHmmss");
    String backupFile = (database + "." + dateFormatter.format(new Date()) + ".sql.gz");
    String[] commandArray = new String[] { dumpDbPath, database, backupFile };
    return IoUtil.exec(commandArray, outputDirectory, 0, false);
  }

  /**
   * If the supplied statement is defined then attempt to close it.
   * 
   * @param statement
   *          Optional statement to be closed if defined.
   * @return True if statement was defined and closed, false if statement was null.
   * @throws SQLException
   *           If there is a problem closing the statement
   */
  public static boolean close(Statement statement)
      throws SQLException
  {
    if (statement != null) {
      statement.close();
      return true;
    }
    return false;
  }

  public static String rowToString(ResultSet resultSet)
      throws SQLException
  {
    Map<String, Object> m = Maps.newLinkedHashMap();
    ResultSetMetaData rsmd = resultSet.getMetaData();
    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
      m.put(rsmd.getColumnLabel(i), resultSet.getObject(i));
    }
    return m.toString();
  }
}
