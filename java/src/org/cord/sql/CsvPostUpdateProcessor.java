package org.cord.sql;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;

/**
 * Processor that is invoked on a record by the CsvTableUpdate once the work of the
 * CsvFieldProcessors is completed.
 * 
 * @see CsvTableUpdate#register(CsvPostUpdateProcessor)
 * @see CsvFieldProcessor
 */
public abstract class CsvPostUpdateProcessor
{
  public abstract void process(TransientRecord record)
      throws MirrorFieldException;
}
