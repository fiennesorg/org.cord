package org.cord.sql;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;

public class LinearCsvFieldProcessor
  extends CsvFieldProcessor
{
  private final String __mirrorFieldName;

  public LinearCsvFieldProcessor(String mirrorFieldName)
  {
    __mirrorFieldName = mirrorFieldName;
  }

  public String getMirrorFieldName()
  {
    return __mirrorFieldName;
  }

  @Override
  public void process(TransientRecord record,
                      String csvValue)
      throws MirrorFieldException
  {
    record.setField(getMirrorFieldName(), csvValue);
  }
}
