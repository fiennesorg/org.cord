package org.cord.sql;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;

/**
 * Class that represents a transformation from a CsvBased field into a mirror based field when
 * applying a CsvTableUpdate.
 * 
 * @see CsvTableUpdate
 * @see #process(TransientRecord,String)
 */
public abstract class CsvFieldProcessor
{
  /**
   * Do any work associated with this incoming value on the given record.
   */
  public abstract void process(TransientRecord record,
                               String csvValue)
      throws MirrorFieldException;
}
