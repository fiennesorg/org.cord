package org.cord.sql;

import java.sql.Connection;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/**
 * A ConnectionPoolFacade provides a {@link ConnectionFacade} wrapper around a
 * {@link ConnectionPool}.
 */
public class ConnectionPoolFacade
  implements ConnectionFacade
{
  private final ConnectionPool __connectionPool;

  public ConnectionPoolFacade(ConnectionPool connectionPool)
  {
    __connectionPool = Preconditions.checkNotNull(connectionPool);
  }

  @Override
  public Connection checkOutConnection()
  {
    return __connectionPool.checkOutConnection("ConnectionPoolFacade");
  }

  @Override
  public void checkInBrokenConnection(Connection c)
  {
    if (c != null) {
      __connectionPool.checkInBroken(c, "ConnectionPoolFacade");
    }
  }

  @Override
  public void checkInWorkingConnection(Connection c)
  {
    if (c != null) {
      __connectionPool.checkInConnection(c, "ConnectionPoolFacade");
    }
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("connectionPool", __connectionPool).toString();
  }
}
