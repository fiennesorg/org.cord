package org.cord.sql;

import java.io.BufferedReader;
import java.io.IOException;

import org.cord.mirror.Table;
import org.cord.util.CsvList;

/**
 * CsvTableUpdate that utilises a BufferedReader wrapper around a given InputStream to deliver lines
 * from CR seperated character data. The user is still responsible for implementing the
 * getRecord(csvList) method to create a working implementation.
 * 
 * @see #getRecord(CsvList)
 */
public abstract class BufferedReaderCsvTableUpdate
  extends CsvTableUpdate
{
  private BufferedReader _bufferedReader;

  private boolean _autoCloseInputStream;

  public BufferedReaderCsvTableUpdate(Table table,
                                      String sourceTableName,
                                      String transactionPassword,
                                      boolean aggressivelyAcquireLocks,
                                      long acquireLockTimeout,
                                      boolean failOnRecordError,
                                      char csvSeperator,
                                      boolean autoTrimValues)
  {
    super(table,
          sourceTableName,
          transactionPassword,
          aggressivelyAcquireLocks,
          acquireLockTimeout,
          failOnRecordError,
          csvSeperator,
          autoTrimValues);
  }

  /**
   * @param bufferedReader
   *          The source of lines for the csv conversion
   * @param autoCloseInputStream
   *          if true then inputStream will automatically be closed when the nextLine is null.
   */
  public void setBufferedReader(BufferedReader bufferedReader,
                                boolean autoCloseInputStream)
  {
    _bufferedReader = bufferedReader;
    _autoCloseInputStream = autoCloseInputStream;
  }

  @Override
  protected String nextCsvLine()
      throws IOException
  {
    String line = _bufferedReader.readLine();
    if (line == null && _autoCloseInputStream) {
      _bufferedReader.close();
    }
    return line;
  }
}
