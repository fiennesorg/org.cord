package org.cord.sql;

import java.sql.Connection;

import com.google.common.base.Preconditions;

/**
 * A SingleConnectionFacade implements ConnectionFacade but always returns the same Connection every
 * time it is asked for one. It is important that it is only every made available to a single
 * Thread.
 * 
 * @author alex
 */
public class SingleConnectionFacade
  implements ConnectionFacade
{
  private final Connection __connection;
  private boolean _isBroken = false;

  public SingleConnectionFacade(Connection connection)
  {
    __connection = Preconditions.checkNotNull(connection);
  }

  @Override
  public void checkInBrokenConnection(Connection c)
  {
    _isBroken = true;
  }

  public boolean isBroken()
  {
    return _isBroken;
  }

  @Override
  public void checkInWorkingConnection(Connection c)
  {
  }

  @Override
  public Connection checkOutConnection()
  {
    return __connection;
  }

}
