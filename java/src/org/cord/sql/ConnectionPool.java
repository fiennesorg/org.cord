package org.cord.sql;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;

import org.cord.mirror.Db;
import org.cord.util.LogicException;
import org.cord.util.Pool;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

/**
 * ConnectionPool provides a fast and stable way of re-using database connections across multiple
 * threads and applications.
 */
public final class ConnectionPool
  extends Pool
{
  private final String __connectionRequest;

  private final String __driverClassName;

  private final String __dbName;

  /**
   * Create a new ConnectionPool object.
   * 
   * @param minSize
   *          The minimum number of Connections that will be spawned by this pool. These connections
   *          will be opened immediately and kept open until the pool is shutdown. The number of
   *          active threads will never drop below this limit (although it may grow over it). The
   *          value must be at least one.
   * @param maxSize
   *          The absolute maximum number of simultaneous connections that this pool can have active
   *          at any one time. If set to the same as minSize, will result in fixed size connection
   *          cache. The value must be at least the minSize.
   * @param maintenancePeriod
   *          The time in milliseconds between which the pool will wake up and shrink excess
   *          connections and validate all remaining passive connections (if activated - see
   *          <code>validateInHousekeeping</code>). The time should be set to be under the default
   *          timeout for connections on the server to ensure that the idle connections are kept
   *          open.
   * @param settleRatio
   *          The speed with which the excess number of connections are closed up at each sweep of
   *          the housekeeping cycle. The value should be in the range of 0.0 to 1.0 with the number
   *          representing the percentage of objects that are destroyed on each sweep (ie 1.0 for
   *          rapid settle, 0.1 for very slow).
   * @param maintenancePriority
   *          The priority which the thread running the housekeeping is to run at. This can be set
   *          lower than the default <code>Thread.NORM_PRIORITY</code> to increase performance
   *          because:-
   *          <ul>
   *          <li>Housekeeping involves database queries and should therefore be left for periods
   *          when the server is not loaded with more important, higher priority threads.
   *          <li>If the server is excessively loaded, then the thread will not get immediately
   *          executed at the correct time, but this is not a problem because all the available
   *          connections will be under use and therefore it is not necessary to ping the db to keep
   *          them open.
   *          </ul>
   * @param validateOnCheckIn
   *          If set to true, then the connections will be tested for validity every time they are
   *          checked back into the server. This is probably not a good idea due to the performance
   *          hit (the test will take place in the same thread was using the connection) and should
   *          not be used in a trusted environment (ie where you don't people to arbitrarily close
   *          you db connections before giving them back).
   * @param validateInHousekeeping
   *          If true then the pool will 'ping' the server on each of the idle connections at every
   *          cycle of the housekeeping thread. This is probably quite a good thing because
   *          otherwise you're db will disappear if you don't keep using it.
   * @param driverClassName
   *          The fully qualified clas name for the jdbc class that is going to create new
   *          connections. The class will be immediately loaded.
   * @param connectionRequest
   *          The database connection request that will be used along with the driverClassName to
   *          instantiate new connections to the db for the duration of this pool.
   * @throws java.lang.ClassNotFoundException
   *           if the driverClassName cannot be instantiated.
   */
  public ConnectionPool(int minSize,
                        int maxSize,
                        long maintenancePeriod,
                        float settleRatio,
                        int maintenancePriority,
                        boolean validateOnCheckIn,
                        boolean validateInHousekeeping,
                        String driverClassName,
                        String connectionRequest,
                        String dbName)
      throws ClassNotFoundException
  {
    super(minSize,
          maxSize,
          maintenancePeriod,
          settleRatio,
          maintenancePriority,
          validateOnCheckIn,
          validateInHousekeeping,
          false, // no reset's of connections on checkin
          true); // maintainBeforeDestroy - irrelevant
    Class.forName(driverClassName);
    __driverClassName = driverClassName;
    __connectionRequest = connectionRequest;
    __dbName = dbName;
    startUp();
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("dbName", __dbName)
                      .add("TotalCount", getTotalCount())
                      .add("AvailableCount", getAvailable())
                      .add("PotentiallyAvailableCount", getPotentiallyAvailable())
                      .add("BusyCount", getBusyCount())
                      .toString();
  }

  @Override
  protected void subShutdown()
  {
    System.err.println("Looking for Driver classes to dergister...");
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    ArrayList<Driver> driversToUnload = new ArrayList<Driver>();
    while (drivers.hasMoreElements()) {
      Driver driver = drivers.nextElement();
      if (driver.getClass().getClassLoader() != null
          && driver.getClass().getClassLoader().equals(getClass().getClassLoader())) {
        driversToUnload.add(driver);
        System.err.println("Noting: " + driver);
      } else {
        System.err.println("Ignoring: " + driver);
      }
    }
    for (Driver driver : driversToUnload) {
      System.err.println("Unloading: " + driver);
      try {
        DriverManager.deregisterDriver(driver);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public static String getConnectionRequest(String protocol,
                                            String hostname,
                                            String database,
                                            String userName,
                                            String password,
                                            String characterEncoding)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("jdbc:")
       .append(protocol)
       .append("://")
       .append(hostname)
       .append("/")
       .append(database)
       .append("?user=")
       .append(userName)
       .append("&password=")
       .append(password);
    if ("mysql".equals(protocol)) {
      buf.append("&useUnicode=true")
         .append("&useServerPrepStmts=true")
         .append("&cachePrepStmts=true")
         .append("&prepStmtCacheSize=100");
      // .append("&useConfigs=maxPerformance")
      // .append("&maintainTimeStats=false")
      // .append("&cacheResultSetMetadata=true");
      if (!Strings.isNullOrEmpty(characterEncoding)) {
        buf.append("&characterEncoding=").append(characterEncoding);
        // .append("&characterSetResults=").append(characterEncoding);
      }
    }
    return buf.toString();
  }

  /**
   * Get the connection string that is passed to the db to instantiate a new connection.
   * 
   * @return connection request string!
   */
  public String getConnectionRequest()
  {
    return __connectionRequest;
  }

  /**
   * Get the fully qualified class name of the jdbc Driver class that is used for instantiating new
   * connections to the db.
   * 
   * @return driver class name string!
   */
  public String getDriverClassName()
  {
    return __driverClassName;
  }

  private final static String PING_COMMAND = "/* ping */ select (1)";

  /**
   * Internal method to validate that a given connection is valid. The validation is performed by
   * performing a "<code>SELECT(1)</code>" command on the db which is probably one of the fastest
   * commands available. If the connection is 'broken' then the method calls
   * <code>createObject()</code> and then returns the new hopefully functional object.
   * 
   * @param obj
   *          The Connection to check.
   * @return Either the original connection is still valid, or a new connection if not.
   */
  @Override
  protected Object validateObject(Object obj)
  {
    Connection connection = (Connection) obj;
    try {
      Statement s = connection.createStatement();
      s.executeQuery(PING_COMMAND);
      s.close();
      return connection;
    } catch (SQLException e) {
      e.printStackTrace();
      try {
        connection.close();
      } catch (SQLException ee) {
      }
      return createObject();
    }
  }

  /**
   * Create a new connection to the database. Please note that this connection is <u>not</u>
   * associated with the pool in any way (the internal pool calls this method) and it will not be
   * reused. If you want a cached connection then call <code>checkOutConnection</code>.
   * 
   * @return A new connection to the database.
   */
  @Override
  public Object createObject()
  {
    try {
      System.err.println(this + ".createObject()");
      return DriverManager.getConnection(__connectionRequest);
    } catch (SQLException e) {
      System.err.println(__connectionRequest);
      throw new LogicException("Database connection error: " + e, e);
    }
  }

  /**
   * Attempt to close a given connection. This is not really to be called by the users, but is
   * rather just a public method for the pool to call itself... The connection to be shut down.
   */
  @Override
  public void destroyObject(Object obj)
  {
    try {
      System.err.println(this + ".destroyObj(" + obj + ")");
      Connection c = (Connection) obj;
      c.close();
    } catch (SQLException e) {
      // we don't really care about destruction exceptions...
    }
  }

  /**
   * Get a free connection from out of the pool. If there is not a connection available, then it
   * will create a new connection (if permitted). If this is not possible (too many connections
   * already), then the thread will <code>wait()</code> until there is a connection made available
   * (by other threads checking in their own connections. It is therefore imperative that you check
   * your connections back in after user by calling <code>checkInConnection(connection)</code>
   * otherwise threadlock is very likely.
   * 
   * @return A database connection.
   */
  public Connection checkOutConnection(String reason)
  {
    return (Connection) checkOut(reason);
  }

  /**
   * Return a connection to the pool for reuse. Do not attempt to use a connection again after this
   * method has been called as this may lead to unexpected behaviour (as two threads attempt to
   * perform operations on the same connection).
   * 
   * @param connection
   *          The connection to be returned to the Pool.
   */
  public void checkInConnection(Connection connection,
                                String reason)
  {
    checkIn(connection, reason);
  }

  public static String generateDebugString(Connection connection,
                                           String message)
      throws SQLException
  {
    return Db.getDbName(connection) + "#" + connection.hashCode() + ": " + message;
  }
}
