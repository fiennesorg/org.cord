package org.cord.sql;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.cord.mirror.Table;
import org.cord.util.CsvList;

/**
 * CsvTableUpdate that utilises a BufferedReader wrapper around a given InputStream to deliver lines
 * from CR seperated character data. The user is still responsible for implementing the
 * getRecord(csvList) method to create a working implementation.
 * 
 * @see #getRecord(CsvList)
 */
public abstract class InputStreamCsvTableUpdate
  extends BufferedReaderCsvTableUpdate
{
  public InputStreamCsvTableUpdate(Table table,
                                   String sourceTableName,
                                   String transactionPassword,
                                   boolean aggressivelyAcquireLocks,
                                   long acquireLockTimeout,
                                   boolean failOnRecordError,
                                   char csvSeperator,
                                   boolean autoTrimValues)
  {
    super(table,
          sourceTableName,
          transactionPassword,
          aggressivelyAcquireLocks,
          acquireLockTimeout,
          failOnRecordError,
          csvSeperator,
          autoTrimValues);
  }

  /**
   * @param inputStream
   *          The InputStream that is going to be converted into a BufferedReader for delivering
   *          lines to the parser.
   * @param autoCloseInputStream
   *          if true then inputStream will automatically be closed when the nextLine is null.
   */
  public void setInputStream(InputStream inputStream,
                             boolean autoCloseInputStream)
  {
    setBufferedReader(new BufferedReader(new InputStreamReader(inputStream)), autoCloseInputStream);
  }
}
