package org.cord.sql;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.util.Assertions;
import org.cord.util.CsvList;
import org.cord.util.MultipleNestedExceptionImpl;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap.Entry;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public abstract class CsvTableUpdate
{
  private final String __sourceTableName;

  private final Db __db;

  private final Table __table;

  private final String __transactionPassword;

  private final boolean __aggressivelyAcquireLocks;

  private final long __acquireLockTimeout;

  private boolean __failOnRecordError;

  private final String __csvSeperator;

  private final boolean __autoTrimValues;

  private final Int2ObjectMap<CsvFieldProcessor> __indexToFieldProcessor =
      new Int2ObjectOpenHashMap<CsvFieldProcessor>();

  private final List<CsvPostUpdateProcessor> __postUpdateProcessors =
      new ArrayList<CsvPostUpdateProcessor>();

  public CsvTableUpdate(Table table,
                        String sourceTableName,
                        String transactionPassword,
                        boolean aggressivelyAcquireLocks,
                        long acquireLockTimeout,
                        boolean failOnRecordError,
                        char csvSeperator,
                        boolean autoTrimValues)
  {
    __db = table.getDb();
    __table = table;
    __sourceTableName = sourceTableName;
    __transactionPassword = transactionPassword;
    __aggressivelyAcquireLocks = aggressivelyAcquireLocks;
    __acquireLockTimeout = acquireLockTimeout;
    __csvSeperator = Character.toString(csvSeperator);
    __autoTrimValues = autoTrimValues;
    __failOnRecordError = failOnRecordError;
  }

  public final String getSourceTableName()
  {
    return __sourceTableName;
  }

  /**
   * Get the Table that this CsvTableUpdate expects to be updating values on.
   */
  public final Table getTable()
  {
    return __table;
  }

  public void addMapping(int index,
                         CsvFieldProcessor fieldProcessor)
  {
    Assertions.notNegative(index, "addMapping(index)");
    Preconditions.checkNotNull(fieldProcessor, "addMapping(fieldProcessor)");
    __indexToFieldProcessor.put(index, fieldProcessor);
  }

  public void register(CsvPostUpdateProcessor postUpdateProcessor)
  {
    if (postUpdateProcessor != null) {
      __postUpdateProcessors.add(postUpdateProcessor);
    }
  }

  /**
   * @return The next source line that needs processing or null if there are no more lines.
   * @throws IOException
   *           if it is not possible to get the next line from whatever is supplying the lines. The
   *           underlying Exception may not be an IOException, but this is a convenient wrapper to
   *           hold it with that fits with the conceptual style of problem. This will be a blocking
   *           exception, ie it will always cause the update to be aborted on the grounds that if it
   *           was not possible to read the next line once then it will be not worth attempting it a
   *           second time.
   */
  protected abstract String nextCsvLine()
      throws IOException;

  /**
   * Get or create the appropriate record that should be affected by the given csvList. If the
   * record is created then the system is not expecting there to be any pre-initialisation of values
   * from the csvList, other than what is required to define the mapping between the csvList and the
   * record. This implies that any foreign key mappings that are to be defined on the newly created
   * records should not have to be initialised in order for the record to be created. This therefore
   * implies that they will either be defined as either BIT_OPTIONAL or BIT_AUTO_LINK if they are
   * BIT_ENFORCED. This will ensure that the record can be created before the main import process
   * kicks in.
   * 
   * @param csvList
   *          The import data that is to be translated into a mirror record. If null then a
   *          NullPointerException may be generated.
   * @return The record that correspond to the csvList. A return value of null implies that the
   *         csvList should not be mapped to a record and it will therefore be ignored from the
   *         processing sweep.
   * @see Dependencies#BIT_AUTO_LINK
   * @see Dependencies#BIT_OPTIONAL
   * @see Dependencies#BIT_ENFORCED
   */
  protected abstract PersistentRecord getRecord(CsvList csvList);

  protected final void processLine(PersistentRecord record,
                                   CsvList csvList)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    try (Transaction transaction = __db.createTransaction(Db.DEFAULT_TIMEOUT,
                                                          Transaction.TRANSACTION_UPDATE,
                                                          __transactionPassword,
                                                          "CsvTableUpdate.processLine(...)")) {
      if (__aggressivelyAcquireLocks) {
        record =
            transaction.editWithLockStealing(record, __acquireLockTimeout, __transactionPassword);
      } else {
        record = transaction.edit(record, __acquireLockTimeout);
      }
      Iterator<Entry<CsvFieldProcessor>> entries =
          __indexToFieldProcessor.int2ObjectEntrySet().iterator();
      while (entries.hasNext()) {
        Entry<CsvFieldProcessor> entry = entries.next();
        CsvFieldProcessor fieldProcessor = entry.getValue();
        String csvValue = csvList.getValue(entry.getIntKey());
        fieldProcessor.process(record, csvValue);
      }
      Iterator<CsvPostUpdateProcessor> postUpdateProcessors = __postUpdateProcessors.iterator();
      while (postUpdateProcessors.hasNext()) {
        postUpdateProcessors.next().process(record);
      }
      transaction.commit();
    }
  }

  protected final void deleteRecord(PersistentRecord record)
      throws MirrorTransactionTimeoutException
  {
    try (Transaction transaction = __db.createTransaction(Db.DEFAULT_TIMEOUT,
                                                          Transaction.TRANSACTION_DELETE,
                                                          __transactionPassword,
                                                          "CsvTableUpdate-deleteRecord(...)")) {
      if (__aggressivelyAcquireLocks) {
        transaction.deleteWithLockStealing(record, __acquireLockTimeout, __transactionPassword);
      } else {
        transaction.delete(record, __acquireLockTimeout);
      }
      transaction.commit();
    }
  }

  private MultipleNestedExceptionImpl addException(MultipleNestedExceptionImpl nestedExceptions,
                                                   Exception exception)
      throws MultipleNestedExceptionImpl
  {
    exception.printStackTrace();
    if (nestedExceptions == null) {
      nestedExceptions = new MultipleNestedExceptionImpl("CsvTableUpdate");
    }
    nestedExceptions.addException(exception);
    if (__failOnRecordError) {
      throw nestedExceptions;
    }
    return nestedExceptions;
  }

  public final void performUpdate()
      throws MultipleNestedExceptionImpl
  {
    IntSet uploadedIds = new IntOpenHashSet();
    String nextCsvLine = null;
    MultipleNestedExceptionImpl nestedExceptions = null;
    try {
      while ((nextCsvLine = nextCsvLine()) != null) {
        CsvList csvList = new CsvList(nextCsvLine, __csvSeperator, __autoTrimValues);
        PersistentRecord record = getRecord(csvList);
        if (record != null) {
          try {
            processLine(record, csvList);
          } catch (MirrorException mirrorException) {
            nestedExceptions = addException(nestedExceptions, mirrorException);
          }
          uploadedIds.add(record.getId());
        }
      }
    } catch (IOException ioEx) {
      throw addException(nestedExceptions, ioEx);
    }
    IntSet deletedIds = new IntOpenHashSet();
    for (IntIterator i = __table.getIdList().iterator(); i.hasNext();) {
      int id = i.nextInt();
      if (!uploadedIds.remove(id)) {
        deletedIds.add(id);
      }
    }
    for (IntIterator i = deletedIds.iterator(); i.hasNext();) {
      int id = i.nextInt();
      PersistentRecord record = null;
      try {
        record = __table.getRecord(id);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        // this shouldn't really happen, but it has so we will dump
        // some information a and then continue...
        System.err.println("Lost record during CsvTableUpdate, uploadedIds: " + uploadedIds
                           + ", deletedIds: " + deletedIds);
        mnsrEx.printStackTrace();
      }
      if (record != null) {
        try {
          deleteRecord(record);
        } catch (MirrorException mirrorException) {
          nestedExceptions = addException(nestedExceptions, mirrorException);
        }
      }
    }
    if (nestedExceptions != null) {
      throw nestedExceptions;
    }
  }
}
