package org.cord.sql;

import java.sql.Connection;

/**
 * A ConnectionFacade is a Facade in front of something that is capable of checking out and
 * returning Connections.
 * 
 * @author alex
 */
public interface ConnectionFacade
{
  public Connection checkOutConnection();

  /**
   * If connection is defined then take take whatever action is required now that the current user
   * is finished with it.
   * 
   * @param connection
   *          Optional Connection
   */
  public void checkInWorkingConnection(Connection connection);

  /**
   * If connection is defined then mark it down as broken now that the current user is finished with
   * it.
   * 
   * @param connection
   *          Optional Connection
   */
  public void checkInBrokenConnection(Connection connection);
}
