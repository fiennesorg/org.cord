package org.cord.sql;

import org.cord.mirror.Db;

import com.google.common.base.Preconditions;

/**
 * ThreadLocal supplier of a ConnectionFacade so that we can bind different ways of resolving
 * Connections to different Threads.
 * 
 * @see Db#getThreadConnectionFacade()
 * @author alex
 */
public class ThreadConnectionFacade
  extends ThreadLocal<ConnectionFacade>
{
  private final ConnectionFacade __default;

  /**
   * @param defaultSupplier
   *          the compulsory ConnectionFacade that will be utilised unless it is explicitly
   *          overridden on a Thread by Thread basis.
   */
  public ThreadConnectionFacade(ConnectionFacade defaultSupplier)
  {
    __default = Preconditions.checkNotNull(defaultSupplier);
  }

  @Override
  protected ConnectionFacade initialValue()
  {
    return __default;
  }
}
