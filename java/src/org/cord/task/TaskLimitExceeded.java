package org.cord.task;

public class TaskLimitExceeded
  extends TaskException
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public TaskLimitExceeded(String message,
                           TaskFactory taskFactory,
                           Throwable cause)
  {
    super(message,
          taskFactory,
          null,
          cause);
  }
}
