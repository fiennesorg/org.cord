package org.cord.task;

public class TaskException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private final TaskFactory __taskFactory;

  private final Task __task;

  public TaskException(String message,
                       TaskFactory taskFactory,
                       Task task,
                       Throwable cause)
  {
    super(message,
          cause);
    __taskFactory = taskFactory;
    __task = task;
  }

  public TaskFactory getTaskFactory()
  {
    return __taskFactory;
  }

  public Task getTask()
  {
    return __task;
  }
}
