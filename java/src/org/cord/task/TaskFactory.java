package org.cord.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.Settable;

/**
 * A TaskFactory is extended to generate a given Task to solve a given domain specific problem.
 * 
 * @author alex
 */
public abstract class TaskFactory
  extends EnglishNamedImpl
{
  private final TaskManager __taskManager;

  private final Map<String, Task> __runningTasks = new LinkedHashMap<String, Task>();

  private final Map<String, Task> __roRunningTasks = Collections.unmodifiableMap(__runningTasks);

  private final Map<String, Task> __completedTasks = new LinkedHashMap<String, Task>();

  private final Map<String, Task> __roCompletedTasks =
      Collections.unmodifiableMap(__completedTasks);

  private final Map<String, Task> __failedTasks = new LinkedHashMap<String, Task>();

  private final Map<String, Task> __roFailedTasks = Collections.unmodifiableMap(__failedTasks);

  private final int __minimumTaskCount;

  private final int __maximumTaskCount;

  private final File __logDir;

  private final String __webLogDir;

  /**
   * @param logDir
   *          if null then delegate to TaskManager values
   * @param webLogDir
   *          if null then delegate to TaskManager values
   * @see TaskManager#getLogDir()
   * @see TaskManager#getWebLogDir()
   */
  public TaskFactory(String name,
                     String englishName,
                     TaskManager taskManager,
                     File logDir,
                     String webLogDir,
                     int minimumTaskCount,
                     int maximumTaskCount)
  {
    super(name,
          englishName);
    __taskManager = taskManager;
    __logDir = logDir == null ? taskManager.getLogDir() : logDir;
    __webLogDir = webLogDir == null ? taskManager.getWebLogDir() : webLogDir;
    if (minimumTaskCount < 0) {
      throw new IllegalArgumentException("Invalid minimumTaskCount:" + minimumTaskCount);
    }
    __minimumTaskCount = minimumTaskCount;
    if (minimumTaskCount > maximumTaskCount) {
      throw new IllegalArgumentException("Invalid min:max task counts: " + minimumTaskCount + ":"
                                         + maximumTaskCount);
    }
    __maximumTaskCount = maximumTaskCount;
  }

  /**
   * Issue a stopTask request to all currently running tasks. While this doesn't mean that all tasks
   * will have terminated at the end of the invocation of this method, it should mean that they
   * should all terminate shortly (assuming that they are well behaved).
   * 
   * @see Task#stopTask()
   */
  protected void shutdown()
  {
    for (Task task : new HashSet<Task>(getRunningTasks().values())) {
      task.stopTask();
    }
  }

  public File getLogDir()
  {
    return __logDir;
  }

  public int getMinimumTaskCount()
  {
    return __minimumTaskCount;
  }

  public int getMaximumTaskCount()
  {
    return __maximumTaskCount;
  }

  public Map<String, Task> getCompletedTasks()
  {
    return __roCompletedTasks;
  }

  public Map<String, Task> getFailedTasks()
  {
    return __roFailedTasks;
  }

  public Map<String, Task> getRunningTasks()
  {
    return __roRunningTasks;
  }

  public Task getTask(String name)
  {
    synchronized (__runningTasks) {
      Task task = __runningTasks.get(name);
      if (task != null) {
        return task;
      }
      task = __completedTasks.get(name);
      if (task != null) {
        return task;
      }
      return __failedTasks.get(name);
    }
  }

  public Task launchTask(Gettable input)
      throws InvalidTaskInput, FileNotFoundException, TaskLimitExceeded
  {
    System.err.println(this + ".launchTask(" + input + ")");
    if (__runningTasks.size() >= __maximumTaskCount) {
      throw new TaskLimitExceeded("Maximum running task count reached", this, null);
    }
    Task task = new Task(this, __logDir, __webLogDir, input);
    task.startTaskThread();
    return task;
  }

  /**
   * Keep launching new Tasks using the defined default Input until the number running Tasks is not
   * less than the minimum number of defined Tasks. This will obtain a sync lock on the Task
   * creation process so should guarantee to be correct at end of invocation.
   * 
   * @return Set of Task containing all of the Tasks that were started during this invocation.
   * @throws FileNotFoundException
   *           If there was a problem with the Log File with one of the Tasks. Any previously
   *           started Tasks will continue to run.
   * @throws InvalidTaskInput
   *           If there was a problem with the default Input for one of the Tasks. Any previously
   *           started Tasks will continue to run.
   * @see #getMinimumTaskCount()
   */
  public Set<Task> startMinimumTasks(Gettable defaults)
      throws InvalidTaskInput, FileNotFoundException
  {
    Set<Task> tasks = new LinkedHashSet<Task>();
    synchronized (__runningTasks) {
      while (__runningTasks.size() < getMinimumTaskCount()) {
        try {
          Task task = launchTask(getNewInput(defaults));
          tasks.add(task);
          System.err.println(this + ": launched minimumTask:" + task);
        } catch (TaskLimitExceeded tlEx) {
          throw new LogicException("TaskLimit should not be exceeded in startMinimumTasks", tlEx);
        }
      }
    }
    return tasks;
  }

  public final TaskManager getTaskManager()
  {
    return __taskManager;
  }

  protected void setRunningTask(Task task)
  {
    System.err.println(this + ".setRunningTask(" + task + ")");
    __runningTasks.put(task.getName(), task);
  }

  protected void setCompletedTask(Task task)
  {
    synchronized (__runningTasks) {
      __runningTasks.remove(task.getName());
      __completedTasks.put(task.getName(), task);
    }
  }

  protected void setFailedTask(Task task)
  {
    synchronized (__runningTasks) {
      __runningTasks.remove(task.getName());
      __failedTasks.put(task.getName(), task);
    }
  }

  /**
   * Check to see whether or not the input conditions for a new Task from this TaskFactory are
   * satisfactory for starting the Task. This will be invoked before the Task Thread is started and
   * is a pre-filter to enable the bulk of problems to be caught before Thread management becomes an
   * issue. If the method throws a RuntimeException then it will be automatically caught and wrapped
   * with an InvalidTaskInput by the invoking method during normal processing.
   */
  protected abstract void validateInput(Task task)
      throws InvalidTaskInput;

  protected abstract void executeTask(Task task)
      throws Exception;

  /**
   * Get a Settable item that can used as the input in launchTask on this TaskFactory utilising the
   * defaults Gettable from the controlling TaskManager.
   * 
   * @see TaskManager#getDefaults()
   * @see #getNewInput(Gettable)
   */
  public Settable getNewInput()
  {
    return getNewInput(getTaskManager().getDefaults());
  }

  /**
   * Get a Settable item that can be used as the input in launchTask on this TaskFactory. If you
   * 
   * @return Settable containing default values as appropriate.
   * @see #launchTask(Gettable)
   * @see Settable#getValueType(String)
   */
  public abstract Settable getNewInput(Gettable defaults);
}
