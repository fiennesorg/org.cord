package org.cord.task;

import org.cord.util.NamedImpl;

public final class TaskStatus
  extends NamedImpl
{
  public static final TaskStatus INPUTERROR = new TaskStatus("Input Error", 0);

  public static final TaskStatus INIT = new TaskStatus("Initialised", 1);

  public static final TaskStatus RUNNING = new TaskStatus("Running", 2);

  public static final TaskStatus COMPLETED = new TaskStatus("Completed", 3);

  public static final TaskStatus FAILED = new TaskStatus("Failed", 4);

  private final int __id;

  private TaskStatus(String name,
                     int id)
  {
    super(name);
    __id = id;
  }

  public int getId()
  {
    return __id;
  }
}
