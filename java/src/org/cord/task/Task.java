package org.cord.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import org.cord.util.Gettable;
import org.cord.util.NamedImpl;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableMap;

import com.google.common.base.Strings;

/**
 * A Task Object represents a single invocation of an operation. It is produced by a TaskFactory and
 * has the initial state of the Task stored in a TaskContext.
 * 
 * @author alex
 */
public class Task
  extends NamedImpl
{
  private final Thread __executeThread;

  private final TaskFactory __taskFactory;

  private final File __logFile;

  private final PrintStream __logStream;

  private final String __webLog;

  private final Gettable __input;

  private final Settable __output = new SettableMap();

  private Date _startTime;

  private Date _finishTime;

  private int _percentComplete = -1;

  private String _statusMessage = "";

  private TaskStatus _taskStatus;

  private Throwable _execEx;

  private AtomicBoolean _isStopped = new AtomicBoolean(false);

  /**
   * Create a new Task and validate that the input definition contained in the TaskContext is valid
   * and that the Task is therefore suitable for running.
   * 
   * @param taskFactory
   * @throws InvalidTaskInput
   *           If the TaskFactory rejects the input contained in the TaskContext.
   * @throws FileNotFoundException
   * @see TaskFactory#validateInput(Task)
   */
  protected Task(TaskFactory taskFactory,
                 File logDir,
                 String webLogDir,
                 Gettable input)
      throws InvalidTaskInput, FileNotFoundException
  {
    super(System.currentTimeMillis() + "." + taskFactory.getName());
    __taskFactory = taskFactory;
    String logName = getName() + ".log";
    __logFile = new File(logDir, logName);
    __webLog = webLogDir + logName;
    __logStream = new PrintStream(new FileOutputStream(__logFile), true);
    __input = input;
    __executeThread = new Thread(getName()) {
      @Override
      public void run()
      {
        getLog().println(Task.this + " started at:" + new Date());
        System.err.println(Task.this + " started at:" + new Date());
        try {
          getTaskFactory().executeTask(Task.this);
          _taskStatus = TaskStatus.COMPLETED;
          getTaskFactory().setCompletedTask(Task.this);
        } catch (Throwable throwable) {
          _execEx = throwable;
          _taskStatus = TaskStatus.FAILED;
          getTaskFactory().setFailedTask(Task.this);
          getLog().println(Task.this + " caught exception at:" + new Date());
          getLog().println("Task input:" + getInput());
          _execEx.printStackTrace(getLog());
        }
        getLog().println(Task.this + " closing down at:" + new Date());
        close();
        _finishTime = new Date();
      }
    };
    _taskStatus = TaskStatus.INPUTERROR;
    try {
      getTaskFactory().validateInput(this);
    } catch (RuntimeException rEx) {
      throw new InvalidTaskInput("RuntimeException in input validation", this, rEx);
    }
    _taskStatus = TaskStatus.INIT;
  }

  public Object getStopSync()
  {
    return _isStopped;
  }

  public boolean hasStopRequest()
  {
    return _isStopped.get();
  }

  public void stopTask()
  {
    getLog().println(this + " received stop message at:" + new Date());
    _isStopped.set(true);
    synchronized (_isStopped) {
      _isStopped.notifyAll();
    }
    setStatusMessage("Stop message received");
  }

  public String getStatusMessage()
  {
    return _statusMessage;
  }

  public void setStatusMessage(String statusMessage)
  {
    _statusMessage = Strings.nullToEmpty(statusMessage);
  }

  public int getPercentComplete()
  {
    return _percentComplete;
  }

  public void setPercentComplete(int p)
  {
    _percentComplete = NumberUtil.constrain(p, 0, 100);
  }

  public Date getStartTime()
  {
    return _startTime;
  }

  public Date getFinishTime()
  {
    return _finishTime;
  }

  public Throwable getExecutionException()
  {
    return _execEx;
  }

  protected void close()
  {
    __logStream.close();
  }

  protected void clearLogs()
  {
    if (_taskStatus == TaskStatus.RUNNING) {
      throw new IllegalStateException("Cannot clearLogs() on a running task:" + this);
    }
  }

  public Settable getOutput()
  {
    return __output;
  }

  public Gettable getInput()
  {
    return __input;
  }

  public PrintStream getLog()
  {
    return __logStream;
  }

  public File getLogFile()
  {
    return __logFile;
  }

  public String getWebLog()
  {
    return __webLog;
  }

  public TaskStatus getTaskStatus()
  {
    return _taskStatus;
  }

  protected void startTaskThread()
  {
    getTaskFactory().setRunningTask(Task.this);
    _taskStatus = TaskStatus.RUNNING;
    __executeThread.start();
    _startTime = new Date();
  }

  public TaskFactory getTaskFactory()
  {
    return __taskFactory;
  }
}
