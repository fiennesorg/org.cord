package org.cord.task;

/**
 * TaskException that can be thrown when a Task is requested to do something after it has received a
 * stop request.
 * 
 * @author alex
 */
public class TaskStoppedException
  extends TaskException
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public TaskStoppedException(String detailMessage,
                              TaskFactory taskFactory,
                              Task task,
                              Throwable cause)
  {
    super(detailMessage,
          taskFactory,
          task,
          cause);
  }
}
