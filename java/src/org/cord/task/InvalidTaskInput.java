package org.cord.task;

public class InvalidTaskInput
  extends TaskException
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public InvalidTaskInput(String message,
                          Task task,
                          Throwable cause)
  {
    super(message,
          task.getTaskFactory(),
          task,
          cause);
  }
}
