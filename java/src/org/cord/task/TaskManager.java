package org.cord.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.cord.util.Gettable;

import com.google.common.base.Preconditions;

/**
 * The TaskManager holds the definitions and state of the current Task framework.
 * 
 * @author alex
 */
public class TaskManager
{
  public static final String INIT_LOGDIR = "TaskManager.logDir";

  public static final String INIT_WEBLOGDIR = "TaskManager.webLogDir";

  private final Map<String, TaskFactory> __taskFactories = new HashMap<String, TaskFactory>();

  private final Collection<TaskFactory> __roTaskFactories =
      Collections.unmodifiableCollection(__taskFactories.values());

  private final File __logDir;

  private final String __webLogDir;

  private final Gettable __defaults;

  public TaskManager(Gettable defaults,
                     File logDir,
                     String webLogDir)
  {
    __defaults = defaults;
    __logDir = Preconditions.checkNotNull(logDir, "TaskManager.logDir");
    __webLogDir = Preconditions.checkNotNull(webLogDir, "TaskManager.webLogDir");
  }

  public TaskManager(Gettable config)
  {
    this(config,
         new File(Preconditions.checkNotNull(config.getString(INIT_LOGDIR), "TaskManager.logDir")),
         config.getString(INIT_WEBLOGDIR));
  }

  public File getLogDir()
  {
    return __logDir;
  }

  public Gettable getDefaults()
  {
    return __defaults;
  }

  public String getWebLogDir()
  {
    return __webLogDir;
  }

  /**
   * @return immutable Iterator of TaskFactory
   * @see TaskFactory
   */
  public Iterator<TaskFactory> getTaskFactories()
  {
    return __roTaskFactories.iterator();
  }

  /**
   * @return true if the TaskFactory was registered, false if there was already a TaskFactory
   *         registered with the same name. Please note that it does not replace the existing
   *         TaskFactory
   */
  public boolean addTaskFactory(TaskFactory factory)
  {
    if (__taskFactories.containsKey(factory.getName())) {
      return false;
    }
    __taskFactories.put(factory.getName(), factory);
    return true;
  }

  public TaskFactory getTaskFactory(String name)
  {
    return __taskFactories.get(name);
  }

  /**
   * @return Map of (TaskFactory, Set(Task)) corresponding to started tasks. If a TaskFactory does
   *         not start any Tasks then it will still have an entry mapping to an empty Set.
   * @see TaskFactory#startMinimumTasks(Gettable)
   */
  public Map<TaskFactory, Set<Task>> startMinimumTasks()
      throws InvalidTaskInput, FileNotFoundException
  {
    Map<TaskFactory, Set<Task>> map = new LinkedHashMap<TaskFactory, Set<Task>>();
    Iterator<TaskFactory> i = getTaskFactories();
    while (i.hasNext()) {
      TaskFactory tf = i.next();
      map.put(tf, tf.startMinimumTasks(getDefaults()));
    }
    return map;
  }

  /**
   * Request all registered TaskFactorys to shut themselves down.
   * 
   * @see TaskFactory#shutdown()
   */
  public void shutdown()
  {
    Iterator<TaskFactory> i = getTaskFactories();
    while (i.hasNext()) {
      i.next().shutdown();
    }
  }

  // public void launchTimedTask(String name,
  // long startTime,
  // Gettable input)
  // {
  // }
  //
  // class PendingTask
  // {
  // private final String __name;
  // private final long __startTime;
  // private final Gettable __input;
  //
  // PendingTask(String name,
  // long startTimed,
  // Gettable input)
  // {
  // __name = name;
  // __startTime = startTimed;
  // __input = input;
  // }
  // }
}
