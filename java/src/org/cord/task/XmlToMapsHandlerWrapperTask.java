package org.cord.task;

import java.util.Map;

import org.cord.util.XmlToMaps;
import org.cord.util.XmlToMapsHandler;

/**
 * Wrapper around a 3rd party XmlToMapsHandler that only handles requests if the Task that this
 * wrapper is associated with is still active. This makes it very easy to retrofit existing XML
 * processors into a Task based structure.
 * 
 * @author alex
 */
public class XmlToMapsHandlerWrapperTask
  implements XmlToMapsHandler
{
  private final Task __task;

  private final XmlToMapsHandler __handler;

  private final String __loggingTitle;

  private final String __loggingTag;

  private volatile int _counter = 0;

  public XmlToMapsHandlerWrapperTask(Task task,
                                     XmlToMapsHandler handler,
                                     String loggingTitle,
                                     String loggingTag)
  {
    __task = task;
    __handler = handler;
    __loggingTitle = loggingTitle;
    __loggingTag = loggingTag;
  }

  /**
   * Constructor that doesn't provide any logging during the handle method
   */
  public XmlToMapsHandlerWrapperTask(Task task,
                                     XmlToMapsHandler handler)
  {
    this(task,
         handler,
         null,
         null);
  }

  /**
   * Check to see if the Task is still active and invoke the wrapper handler if it is. This will
   * also invoke preHandle immediately before invoking handle.
   * 
   * @throws TaskStoppedException
   *           if the Task has received a stop request
   * @throws Exception
   *           chained from the wrapped handler.
   * @see #preHandle(XmlToMaps, Map)
   */
  @Override
  public void handle(XmlToMaps source,
                     Map<String, String> map)
      throws Exception
  {
    if (__task != null && __task.hasStopRequest()) {
      throw new TaskStoppedException("Task has been stopped",
                                     __task.getTaskFactory(),
                                     __task,
                                     null);
    }
    preHandle(source, map);
    _counter++;
    __handler.handle(source, map);
  }

  public int getCounter()
  {
    return _counter;
  }

  /**
   * Hook method that is invoked before handle delegates to the wrapped handler and which offers
   * optionally loggin by default. If logginTag was defined in the constructor then this will log
   * the value of loggingTag in the current map to the Task log file. If you want a different
   * behaviour then just override this method with your own custom functionality. It is advised that
   * you don't change the contents of map if you want to avoid unpredictable behaviour...
   * 
   * @param source
   * @param map
   */
  protected void preHandle(XmlToMaps source,
                           Map<String, String> map)
  {
    if (__task != null && __loggingTag != null) {
      __task.getLog().println(__loggingTitle + "(" + _counter + ") " + map.get(__loggingTag));
    }
  }
}
