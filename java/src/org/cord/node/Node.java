package org.cord.node;

import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.IdList;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostRetrievalTrigger;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordOperationValueComparator;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.RelatedMirrorFieldException;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.CachingMap;
import org.cord.mirror.operation.CachingMapResolver;
import org.cord.mirror.operation.DuoRecordOperation;
import org.cord.mirror.operation.DuoRecordOperationKey;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.AbstractRecordOrdering;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceIdFilter;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.CompoundUniqueFieldTrigger;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.mirror.trigger.PostInstantiationTriggerImpl;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.mirror.trigger.UnaccentedAscii;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.href.NodeLinkTitleGenerator;
import org.cord.node.json.Handler_Node;
import org.cord.node.operation.BreadcrumbTrail;
import org.cord.node.operation.ChildNamed;
import org.cord.node.operation.ElderNodeSiblings;
import org.cord.node.operation.NodeChildNumber;
import org.cord.node.operation.UserRecordOperation;
import org.cord.node.operation.UserStyleSpecificNodeRecordOperation;
import org.cord.node.operation.YoungerNodeSiblings;
import org.cord.node.trigger.NodeFilenameValidator;
import org.cord.node.trigger.NodeOwnerUpdateTrigger;
import org.cord.node.trigger.NodePostBooter;
import org.cord.node.trigger.NodePreBooter;
import org.cord.node.trigger.StyleSpecificNodePreBooter;
import org.cord.node.view.DeleteNodeFactory;
import org.cord.node.view.EditFactory;
import org.cord.node.view.ReadViewFactory;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.cord.util.PrivateCacheKey;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.SettableMap;
import org.cord.util.StringUtils;
import org.cord.util.TimestampedConcurrentMap;
import org.cord.webmacro.AppendableParametricMacro;
import org.json.JSONObject;
import org.webmacro.Macro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class Node
  extends NodeTableWrapper
{
  /**
   * The name of the Node table ("Node").
   */
  public final static String TABLENAME = "Node";

  /**
   * The id value (1) of the root Node in the node tree. This is the only Node which is permitted to
   * have an undefined PARENTNODE and an empty FILENAME
   * 
   * @see #PARENTNODE
   * @see #PARENTNODE_ID
   * @see #CHILDRENNODES
   * @see #FILENAME
   */
  public final static int ROOTNODE_ID = 1;

  /**
   * Integer field ("parentNode_id") that contains the id of the Node record that is the immediate
   * parent of this Node. All Nodes have a parent except for the root node of the tree.
   * 
   * @see #PARENTNODE
   * @see #ROOTNODE_ID
   * @see #CHILDRENNODES
   */
  public final static IntFieldKey PARENTNODE_ID = IntFieldKey.create("parentNode_id");

  /**
   * RecordOperation ("parentNode") that resolves to the TransientRecord containing the immediate
   * parent of this Node. This will always resolve except when the current node is the root node in
   * which case it will resolve to null.
   * 
   * @see #PARENTNODE_ID
   * @see #ROOTNODE_ID
   * @see #CHILDRENNODES
   */
  public final static RecordOperationKey<PersistentRecord> PARENTNODE =
      RecordOperationKey.create("parentNode", PersistentRecord.class);

  /**
   * RecordOperation ("childrenNodes") that resolves to a QueryWrapper that contains all the
   * children of the current node. This will have the default ordering applied and if you just want
   * to know if there is a child then you should consider using {@link #UNSORTEDCHILDRENNODES} as it
   * will perform better.
   * 
   * @see #PARENTNODE_ID
   * @see #PARENTNODE
   */
  public final static RecordOperationKey<RecordSource> CHILDRENNODES =
      RecordOperationKey.create("childrenNodes", RecordSource.class);

  /**
   * RecordOperation ("unsortedChildrenNodes") that resolves to a RecordSource containing all of the
   * children of the current node with unspecified ordering. If you want a predictable default
   * ordering then utilise {@link #CHILDRENNODES}
   */
  public final static RecordOperationKey<RecordSource> UNSORTEDCHILDRENNODES =
      RecordOperationKey.create("unsortedChildrenNodes", RecordSource.class);

  /**
   * String field ("title") that contains the human readable version of the title of the node. Free
   * text, up to 255 characters.
   */
  public final static ObjectFieldKey<String> TITLE = StringField.createKey("title");

  /**
   * String field ("htmlTitle") that contains the breadcrumb version of the title of this node. This
   * should be short and informative and is designed to be used with the htmlTitle fields of the
   * parents of this Node for constructing breadcrumb trails and window titles of rendered Nodes.
   */
  public final static ObjectFieldKey<String> HTMLTITLE = StringField.createKey("htmlTitle");

  /**
   * String field ("filename") that contains the filename of this Node.
   * 
   * @see NodeDbConstants#FILENAME
   */
  public final static ObjectFieldKey<String> FILENAME = NodeDbConstants.FILENAME.copy();

  /**
   * RecordOperation ("path") that returns the path to the given Node.
   * 
   * @see #PATH_SEPERATOR
   * @see #FILENAME
   */
  public final static RecordOperationKey<String> PATH =
      RecordOperationKey.create("path", String.class);

  /**
   * String ("/") that is used to separate FILENAME elements when constructing a PATH value.
   * 
   * @see #FILENAME
   * @see #PATH
   */
  public final static char PATH_SEPERATOR = '/';
  public final static String PATH_SEPERATOR_STRING = Character.toString(PATH_SEPERATOR);

  /**
   * Integer field ("number") that is used to sort Node records within their NodeGroup.
   * 
   * @see NodeDbConstants#NUMBER
   */
  public final static IntFieldKey NUMBER = NodeDbConstants.NUMBER.copy();

  /**
   * Boolean field ("isPublished") that is used to signify that a node is not to be displayed to the
   * general public.
   */
  public final static ObjectFieldKey<Boolean> ISPUBLISHED = BooleanField.createKey("isPublished");

  public final static boolean ISPUBLISHED_DEFAULT = false;

  /**
   * Datetime field ("creationTime") that contains the time when the node was created. Please note
   * that this is actual database time, rather than the creation time of the object that the node
   * represents. This is handled automatically by a CreationTimeTrigger on the field.
   */
  public final static ObjectFieldKey<Date> CREATIONTIME = FastDateField.createKey("creationTime");

  /**
   * Datetime field ("modificationTime") that contains the time when the node was last updated.
   * Please note that this is actual database time and currently only refers to fields within the
   * Node itself rather than Regions associated with the Node.
   */
  public final static ObjectFieldKey<Date> MODIFICATIONTIME =
      FastDateField.createKey("modificationTime");

  /**
   * IntegerFieldFilter named "nodeStyle_id" on Node that contains the reference to the NodeStyle
   * that this Node implements.
   * 
   * @see #NODESTYLE
   * @see NodeStyle#NODES
   */
  public final static IntFieldKey NODESTYLE_ID = IntFieldKey.create("nodeStyle_id");

  /**
   * RecordOperation named "nodeStyle" that returns the NodeStyle record that this Node implements.
   * 
   * @see #NODESTYLE_ID
   * @see NodeStyle#NODES
   */
  public final static RecordOperationKey<PersistentRecord> NODESTYLE =
      RecordOperationKey.create("nodeStyle", PersistentRecord.class);

  /**
   * IntegerFieldFilter ("owner_id") that contains the reference to the User that conceptually owns
   * this Node. The value is initialised to the value of NodeStyle.owner_id by NodePreBooter.
   * 
   * @see #OWNER
   * @see User#TABLENAME
   * @see User#OWNEDNODES
   * @see NodeStyle#OWNER_ID
   */
  public final static IntFieldKey OWNER_ID = IntFieldKey.create("owner_id");

  /**
   * RecordOperation named "owner" that resolves into the TransientRecord from the User field that
   * 'owns' this record.
   * 
   * @see #OWNER_ID
   * @see User#TABLENAME
   * @see User#OWNEDNODES
   */
  public final static RecordOperationKey<PersistentRecord> OWNER =
      RecordOperationKey.create("owner", PersistentRecord.class);

  public final static int OWNER_ID_DEFAULT = 1;

  /**
   * IntegerFieldFilter ("viewAcl_id") that contains the reference to the Acl record that must be
   * passed to view the Node. This is initialised to the value of NodeStyle.viewAcl_id by the
   * NodePreBooter. If this value is to be updated from this initial value then the user must pass
   * UPDATEVIEWACL_ID.
   * 
   * @see #VIEWACL
   * @see Acl#TABLENAME
   * @see Acl#VIEWNODES
   * @see NodeStyle#VIEWACL_ID
   * @see NodeStyle#UPDATEVIEWACL_ID
   */
  public final static IntFieldKey VIEWACL_ID = IntFieldKey.create("viewAcl_id");

  /**
   * RecordOperation ("viewAcl") that resolves to the TransientRecord pointed at by VIEWACL_ID.
   * 
   * @see #VIEWACL_ID
   * @see Acl#TABLENAME
   * @see Acl#VIEWNODES
   * @see NodeStyle#VIEWACL_ID
   */
  public final static RecordOperationKey<PersistentRecord> VIEWACL =
      RecordOperationKey.create("viewAcl", PersistentRecord.class);

  /**
   * IntegerFieldFilter ("editAcl_id") that contains the reference to the Acl record that must be
   * passed to edit the Node. This is initialised to the value of NodeStyle.editAcl_id by the
   * NodePreBooter. If this value is to be updated from this initial value then the user must pass
   * UPDATEEDITACL_ID.
   * 
   * @see #EDITACL
   * @see Acl#TABLENAME
   * @see Acl#EDITNODES
   * @see NodeStyle#EDITACL_ID
   * @see NodeStyle#UPDATEEDITACL_ID
   */
  public final static IntFieldKey EDITACL_ID = IntFieldKey.create("editAcl_id");

  /**
   * RecordOperation ("editAcl") that resolves to the TransientRecord pointed at by EDITACL_ID.
   * 
   * @see #EDITACL_ID
   * @see Acl#TABLENAME
   * @see Acl#EDITNODES
   * @see NodeStyle#EDITACL_ID
   */
  public final static RecordOperationKey<PersistentRecord> EDITACL =
      RecordOperationKey.create("editAcl", PersistentRecord.class);

  /**
   * "publishAcl_id"
   */
  public final static IntFieldKey PUBLISHACL_ID = IntFieldKey.create("publishAcl_id");

  /**
   * "publishAcl"
   */
  public final static RecordOperationKey<PersistentRecord> PUBLISHACL =
      RecordOperationKey.create("publishAcl", PersistentRecord.class);

  /**
   * IntegerFieldFilter ("deleteAcl_id") that contains the reference to the Acl record that must be
   * passed in order to delete the node. This is initialised to the value of NodeStyle.deleteAcl_id
   * by the NodePreBooter. If this value is to be updated from this initial value then the user must
   * pass the UPDATEDELETEACL_ID ACl.
   * 
   * @see #DELETEACL
   * @see Acl#TABLENAME
   * @see Acl#DELETENODES
   * @see NodeStyle#DELETEACL_ID
   * @see NodeStyle#UPDATEDELETEACL_ID
   */
  public final static IntFieldKey DELETEACL_ID = IntFieldKey.create("deleteAcl_id");

  /**
   * RecordOperation ("deleteAcl") that resolves to the TransientRecord from the Acl table pointed
   * to by DELETEACL_ID.
   * 
   * @see #DELETEACL_ID
   * @see Acl#TABLENAME
   * @see Acl#DELETENODES
   * @see NodeStyle#DELETEACL_ID
   * @see NodeStyle#UPDATEDELETEACL_ID
   */
  public final static RecordOperationKey<PersistentRecord> DELETEACL =
      RecordOperationKey.create("deleteAcl", PersistentRecord.class);

  /**
   * "generation"
   */
  public final static RecordOperationKey<Integer> GENERATION =
      RecordOperationKey.create("generation", Integer.class);

  /**
   * RecordOperation ("outgoingLinks") that resolves to the QueryWrapper containing all the links
   * the originate from this Node.
   */
  public final static String OUTGOINGLINKS = "outgoingLinks";

  /**
   * RecordOperation ("incomingLinks") that resolves to the QueryWrapper containing all the links
   * that point to this Node.
   */
  public final static String INCOMINGLINKS = "incomingLinks";

  public static final MonoRecordOperationKey<Object, Gettable> COMPILE =
      MonoRecordOperationKey.create("compile", Object.class, Gettable.class);

  public static final DuoRecordOperationKey<Object, NodeRequest, Gettable> PARAMETRICCOMPILE =
      DuoRecordOperationKey.create("parametricCompile",
                                   Object.class,
                                   NodeRequest.class,
                                   Gettable.class);

  public final static String DEFAULTORDER = "Node.number, Node.htmlTitle";

  /**
   * The default length of time (250ms) spent attempting to attain a lock on a Node before resorting
   * to the ContestedLockFilter resolution. This is set long enough that any automated processes
   * that might have the lock on the system have time to complete before trying to interrupt the
   * process.
   */
  public final static long LOCK_DEFAULTTIMEOUT = 250;

  public final static IntFieldKey NODEGROUPSTYLE_ID = IntFieldKey.create("nodeGroupStyle_id");

  public final static RecordOperationKey<PersistentRecord> NODEGROUPSTYLE =
      RecordOperationKey.create("nodeGroupStyle", PersistentRecord.class);

  /**
   * RecordSourceOperation ("orderedByPath") that re-orders the list of Nodes according the
   * ascending order of path
   * 
   * @see #PATH
   */
  public final static RecordSourceOperationKey<RecordSource> ORDEREDBYPATH =
      RecordSourceOperationKey.create("orderedByPath", RecordSource.class);

  /**
   * TransientRecord Comparator that will sort alphabetically by {@link Node#PATH}
   */
  public final static Comparator<TransientRecord> BYPATH =
      new RecordOperationValueComparator<String>(Node.PATH);

  /**
   * The name of the operation that returns a list of published children Nodes on this Node. The
   * webmacro invocation is $node.childrenMenuNodes.get($nodeRequest)
   */
  public final static MonoRecordOperationKey<Object, RecordSource> CHILDRENMENUNODES =
      MonoRecordOperationKey.create("childrenMenuNodes", Object.class, RecordSource.class);

  public final static RecordOperationKey<String> URL =
      RecordOperationKey.create("url", String.class);

  /**
   * Parametric RecordOperation named href that resolves into an href to the invoking node where the
   * invoking parameter is resolved as a RecordOperation on the invoking node. If this is not
   * possible then it is taken as an explicit string and used directly. If the contents were
   * resolved from a RecordOperation then they will automatically have
   * {@link StringUtils#noHtml(Object)} invoked on them to escape illegal characters. If you have a
   * RecordOperation that does return HTML that you want to preserve then use {@link #EXPLICITHREF}
   * to manually pass in the contents. The title of the link will be the value of getDescription on
   * the node that is being pointed at.
   * 
   * @see #DESCRIPTION
   * @see #EXPLICITHREF
   */
  // TODO: decide on what the Transactional viewpoint on HREF is
  public final static MonoRecordOperationKey<String, String> HREF =
      MonoRecordOperationKey.create("href", String.class, String.class);

  /**
   * Parametric RecordOperation named explicitHref that resolves into an href to the invoking node
   * where the parameter is always taken as the contents of the link rather than being attempted as
   * a RecordOperation on the node. The title of the link will be the value of getDescription on the
   * node that is being pointed at.
   * 
   * @see #DESCRIPTION
   * @see #HREF
   */
  // TODOL: decide on what the Transactional viewpoint on EXPLICITHREF is
  public final static MonoRecordOperationKey<String, String> EXPLICITHREF =
      MonoRecordOperationKey.create("explicitHref", String.class, String.class);

  /**
   * isMoveable is a RecordOperation that returns true if it would be possible to select this node
   * as a client for moving to another location.
   * 
   * @see #assertIsMoveable(PersistentRecord)
   */
  public final static RecordOperationKey<Boolean> ISMOVEABLE =
      RecordOperationKey.create("isMoveable", Boolean.class);

  public final static RecordOperationKey<String> NOTMOVEABLEREASON =
      RecordOperationKey.create("notMoveableReason", String.class);

  public final static String INITPARAM_NODE_DEFAULTVIEWACLID = "Node.defaultViewAclId";

  public final static String INITPARAM_NODE_DEFAULTEDITACLID = "Node.defaultEditAclId";

  public final static String INITPARAM_NODE_DEFAULTPUBLISHACLID = "Node.defaultPublishAclId";

  public final static String INITPARAM_NODE_DEFAULTDELETEACLID = "Node.defaultDeleteAclId";

  public final static String INITPARAM_NODE_CHILDRENMENUNODES_ORDERING =
      "Node.childrenMenuNodes.ordering";

  public final static String INITPARAM_NODE_CHILDRENMENUNODES_ORDERING_DEFAULT =
      "Node.htmlTitle, Node.id";

  /**
   * $node.isPathPublished returns true if the node is published and all its parents are published,
   * ie it is entirely publically visible.
   * 
   * @see UnpublishedNodeTracker#isPublished(PersistentRecord)
   */
  public final static RecordOperationKey<Boolean> ISPATHPUBLISHED =
      RecordOperationKey.create("isPathPublished", Boolean.class);

  public final static RecordSourceOperationKey<RecordSource> RS_ISPATHPUBLISHED =
      RecordSourceOperationKey.create(ISPATHPUBLISHED.getKeyName(), RecordSource.class);

  public final static MonoRecordOperationKey<Object, Boolean> IS_CHILD_OF =
      MonoRecordOperationKey.create("isChildOf", Object.class, Boolean.class);

  /**
   * $node.uniqueChildName(proposedName) calls {@link #getUniqueChildName(PersistentRecord, String)}
   */
  public final static MonoRecordOperationKey<String, String> UNIQUE_CHILDNAME =
      MonoRecordOperationKey.create("uniqueChildName", String.class, String.class);

  /**
   * Parametric RecordOperation that compiles the given named compiler and recurses up the parentage
   * chain until it finds a node that defines it. If there is no definition that is defined then
   * return null instead.
   */
  public final static MonoRecordOperationKey<String, Gettable> YOUNGESTCOMPILE =
      MonoRecordOperationKey.create("youngestCompile", String.class, Gettable.class);

  /**
   * Parametric RecordOperation that returns the QueryWrapper of all child nodes of the given
   * NodeGroupStyle. The NodeGroupStyle can be expressed as either the record itself, the id of the
   * record or the name of the NodeGroupStyle, eg:- <blockquote><code>
   * $node.childrenNodesByNodeGroupStyle.get($nodeGroupStyle)
   * $node.childrenNodesByNodeGroupStyle.get(24)
   * $node.childrenNodesByNodeGroupStyle.get("Style name")
   * </code></blockquote> If you pass in any parameter that doesn't make sense, ie something that
   * resolves to a NodeGroupStyle that isn't a child of the invoking Node, or something that isn't
   * even a NodeGroupStyle then an empty QueryWrapper is resolved, ie no child nodes.
   */
  public final static MonoRecordOperationKey<Object, RecordSource> CHILDRENNODESBYNODEGROUPSTYLE =
      MonoRecordOperationKey.create("childrenNodesByNodeGroupStyle",
                                    Object.class,
                                    RecordSource.class);

  public final static MonoRecordOperationKey<Object, RecordSource> CHILDRENNODESBYNODESTYLE =
      MonoRecordOperationKey.create("childrenNodesByNodeStyle", Object.class, RecordSource.class);

  /**
   * StringField that holds a TEXT unformatted description of the Node. This is intended to be
   * presented in parallel with the {@link #TITLE} field and therefore doesn't have to duplicate the
   * information.
   */
  public final static ObjectFieldKey<String> DESCRIPTION = StringField.createKey("description");

  public final static ObjectFieldKey<Boolean> ISNOINDEX = BooleanField.createKey("isNoIndex");

  public final static ObjectFieldKey<Boolean> ISNOFOLLOW = BooleanField.createKey("isNoFollow");

  public final static ObjectFieldKey<Boolean> ISMENUITEM = NodeStyle.ISMENUITEM.copy();

  public final static ObjectFieldKey<Boolean> ISSEARCHABLE = BooleanField.createKey("isSearchable");

  // public static final ObjectFieldKey<String> JSONSRC = JSONObjectField.createKey("jsonSrc");
  // public static final RecordOperationKey<JSONObject> JSON =
  // JSONObjectField.createAsJSONKey("json");

  public static final ObjectFieldKey<JSONObject> JSON = PureJsonObjectField.createKey("json");

  /**
   * @see HtmlGuiSupplier#ASFORMELEMENT
   */
  public final static MonoRecordOperationKey<String, Macro> ASFORMELEMENT =
      HtmlGuiSupplier.ASFORMELEMENT;

  public static final MonoRecordOperationKey<String, Macro> ASFORMVALUE =
      HtmlGuiSupplier.ASFORMVALUE;

  /**
   * RecordOperation that determines whether the node is deletable by the given user. <blockquote>
   * <code>
   * $node.isDeletableBy.get($user)
   * </code></blockquote>
   * <p>
   * $user can be either a PersistentRecord, a NodeRequest, or an Object that is resolveable into a
   * User id.
   * </p>
   * 
   * @see #isDeletable(PersistentRecord, PersistentRecord, boolean, String)
   */
  public final static MonoRecordOperationKey<Object, Boolean> ISDELETABLEBY =
      MonoRecordOperationKey.create("isDeletableBy", Object.class, Boolean.class);

  /**
   * The String which, if included in a filename of a Node, will be replaced with the integer id
   * value of the node. This will be automatically stripped out of the title of the Node if the
   * title is derived from the filename and not explicitly specified.
   */
  public static final String FILENAME_ID = "$(id)";

  public static final RecordOperationKey<TimestampedConcurrentMap<Object, Object>> HREFS =
      RecordOperationKey.create("hrefs",
                                ObjectUtil.<TimestampedConcurrentMap<Object, Object>> castClass(TimestampedConcurrentMap.class));

  private StyleSpecificNodePreBooter _styleSpecificNodePreBooter;

  private UserStyleSpecificNodeRecordOperation<RecordSource> _childrenMenuNodes;

  private final int __defaultViewAclId;

  private final int __defaultEditAclId;

  private final int __defaultPublishAclId;

  private final int __defaultDeleteAclId;

  private final NodeLinkTitleGenerator __nodeLinkTitleGenerator;

  protected Node(NodeManager nodeManager,
                 int defaultViewAclId,
                 int defaultEditAclId,
                 int defaultPublishAclId,
                 int defaultDeleteAclId,
                 NodeLinkTitleGenerator nodeLinkTitleGenerator)
  {
    super(nodeManager,
          TABLENAME);
    __defaultViewAclId = defaultViewAclId;
    __defaultEditAclId = defaultEditAclId;
    __defaultPublishAclId = defaultPublishAclId;
    __defaultDeleteAclId = defaultDeleteAclId;
    __nodeLinkTitleGenerator =
        Preconditions.checkNotNull(nodeLinkTitleGenerator, "nodeLinkTitleGenerator");
  }

  public static final String INITPARAM_NODE_NODELINKTITLEGENERATOR_CLASS =
      "Node.NodeLinkTitleGenerator.class";

  protected Node(NodeManager nodeManager,
                 Gettable params)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException
  {
    this(nodeManager,
         GettableUtils.getInt(params, INITPARAM_NODE_DEFAULTVIEWACLID, 2),
         GettableUtils.getInt(params, INITPARAM_NODE_DEFAULTEDITACLID, 4),
         GettableUtils.getInt(params, INITPARAM_NODE_DEFAULTPUBLISHACLID, 4),
         GettableUtils.getInt(params, INITPARAM_NODE_DEFAULTDELETEACLID, 4),
         loadNodeLinkTitleGenerator(nodeManager.getGettable()));
  }

  private static NodeLinkTitleGenerator loadNodeLinkTitleGenerator(Gettable gettable)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException
  {
    String className =
        MoreObjects.firstNonNull(gettable.getString(INITPARAM_NODE_NODELINKTITLEGENERATOR_CLASS),
                                 "org.cord.node.href.OptTitleDescription");
    DebugConstants.variable(INITPARAM_NODE_NODELINKTITLEGENERATOR_CLASS, className);
    return loadNodeLinkTitleGenerator(className);
  }

  private static NodeLinkTitleGenerator loadNodeLinkTitleGenerator(String className)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException
  {
    Class<?> c = Class.forName(className);
    if (!NodeLinkTitleGenerator.class.isAssignableFrom(c)) {
      throw new IllegalArgumentException("Unable to cast " + className
                                         + " into a NodeLinkTitleGenerator");
    }
    return (NodeLinkTitleGenerator) c.newInstance();
  }

  @Override
  public void shutdown()
  {
    _styleSpecificNodePreBooter = null;
    _childrenMenuNodes = null;
  }

  @Override
  public void initTable(final Db db,
                        final String pwd,
                        final Table table)
      throws MirrorTableLockedException
  {
    table.setDefaultOrdering(Node.TABLENAME + "." + Node.NUMBER + "," + Node.TABLENAME + "."
                             + Node.HTMLTITLE);
    table.addField(new LinkField(Node.PARENTNODE_ID,
                                 "Parent node",
                                 null,
                                 Node.PARENTNODE,
                                 Node.TABLENAME,
                                 Node.CHILDRENNODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.BIT_S_ON_T_DELETE));
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(UNSORTEDCHILDRENNODES) {
      @Override
      public RecordSource getOperation(TransientRecord node,
                                       Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(Node.TABLENAME)
           .append('.')
           .append(Node.PARENTNODE_ID)
           .append('=')
           .append(node.getId());
        return RecordSources.viewedFrom(Node.this.getTable()
                                                 .getQuery(null,
                                                           buf.toString(),
                                                           Query.NOSQLORDERING),
                                        viewpoint);
      }
    });
    table.addField(new IntField(Node.NUMBER, "Ordering number"));
    table.addField(new StringField(Node.TITLE, "Page title"));
    table.addField(new StringField(Node.HTMLTITLE, "Navigation title"));
    table.addField(new StringField(Node.FILENAME, "URL"));
    table.addField(new BooleanField(Node.ISPUBLISHED,
                                    "Is published?",
                                    Boolean.valueOf(ISPUBLISHED_DEFAULT)));
    table.addField(new FastDateField(Node.CREATIONTIME,
                                     "Creation time").setMayHaveHtmlWidget(false));
    table.addField(new FastDateField(Node.MODIFICATIONTIME,
                                     "Modification time").setMayHaveHtmlWidget(false));
    table.addField(new LinkField(Node.NODESTYLE_ID,
                                 "Page type",
                                 null,
                                 Node.NODESTYLE,
                                 NodeStyle.TABLENAME,
                                 NodeStyle.NODES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED).setMayHaveHtmlWidget(false));
    table.addField(new LinkField(Node.OWNER_ID,
                                 "Page owner",
                                 Integer.valueOf(OWNER_ID_DEFAULT),
                                 Node.OWNER,
                                 User.TABLENAME,
                                 User.OWNEDNODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.LINK_ENFORCED).setMayHaveHtmlWidget(false));
    table.addField(new LinkField(Node.VIEWACL_ID,
                                 "View page rights",
                                 Integer.valueOf(__defaultViewAclId),
                                 Node.VIEWACL,
                                 Acl.TABLENAME,
                                 Acl.VIEWNODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(Node.EDITACL_ID,
                                 "Edit page rights",
                                 Integer.valueOf(__defaultEditAclId),
                                 Node.EDITACL,
                                 Acl.TABLENAME,
                                 Acl.EDITNODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(Node.PUBLISHACL_ID,
                                 "Publish/Unpublish page rights",
                                 Integer.valueOf(__defaultPublishAclId),
                                 Node.PUBLISHACL,
                                 Acl.TABLENAME,
                                 Acl.PUBLISHNODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(Node.DELETEACL_ID,
                                 "Delete page rights",
                                 Integer.valueOf(__defaultDeleteAclId),
                                 Node.DELETEACL,
                                 Acl.TABLENAME,
                                 Acl.DELETENODES,
                                 Node.DEFAULTORDER,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(Node.NODEGROUPSTYLE_ID,
                                 "NodeGroupStyle",
                                 null,
                                 Node.NODEGROUPSTYLE,
                                 NodeGroupStyle.TABLENAME,
                                 NodeGroupStyle.NODES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED).setMayHaveHtmlWidget(false));
    table.addField(new StringField(DESCRIPTION,
                                   "Short page description",
                                   StringField.LENGTH_TEXT,
                                   "").setMayBeTextArea(true));
    table.addField(new BooleanField(ISNOINDEX, "Ban external search engines from indexing?"));
    table.addField(new BooleanField(ISNOFOLLOW,
                                    "Ban external search engines from following links from this page?"));
    table.addField(new BooleanField(ISMENUITEM, "Appears in menu navigation?") {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    table.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord node,
                             Gettable params)
          throws MirrorException
      {
        Object isMenuItem = params.get(ISMENUITEM);
        node.setField(ISMENUITEM,
                      isMenuItem != null
                          ? isMenuItem
                          : node.comp(Node.NODESTYLE).opt(NodeStyle.ISMENUITEM));
      }
    });
    table.addField(new BooleanField(ISSEARCHABLE,
                                    "Is searchable using internal site search?",
                                    Boolean.TRUE));
    table.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord node,
                             Gettable params)
          throws MirrorException
      {
        PersistentRecord parentNode = node.opt(PARENTNODE);
        if (parentNode != null) {
          node.setField(ISSEARCHABLE, parentNode.opt(ISSEARCHABLE));
        }
      }
    });
    // table.addField(new JSONObjectField(JSONSRC, "JSON", JSON, null));
    table.addField(new PureJsonObjectField(JSON,
                                           "JSON",
                                           PureJsonObjectField.emptyFieldValueSupplier()));
    table.addFieldTrigger(new NodeFilenameValidator(getNodeManager().getFilenameGenerator()));
    table.addFieldTrigger(new UnaccentedAscii(Node.FILENAME, true, false, true));
    CompoundUniqueFieldTrigger<String> uniqueFilename =
        new CompoundUniqueFieldTrigger<String>(table, Node.FILENAME, false);
    uniqueFilename.addField(Node.PARENTNODE_ID);
    table.addFieldTrigger(uniqueFilename);
    table.setTitleOperationName(Node.PATH);
    table.addRecordOperation(new SimpleRecordOperation<Integer>(GENERATION) {
      @Override
      public Integer getOperation(TransientRecord node,
                                  Viewpoint viewpoint)
      {
        return Integer.valueOf(getGeneration(node));
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<String>(PATH) {
      @Override
      public String getOperation(TransientRecord node,
                                 Viewpoint viewpoint)
      {
        if (node.getId() == Node.ROOTNODE_ID) {
          return "/";
        }
        StringBuilder buf = new StringBuilder();
        appendNodePath(buf, node.getPersistentRecord());
        return buf.toString();
      }
    });
    table.addRecordOperation(new ShortTitle());
    table.addRecordOperation(new NodeChildNumber());
    table.addPreInstantiationTrigger(new NodePreBooter(getNodeManager(), pwd));
    table.addRecordOperation(new NodeLocker());
    table.addPostInstantiationTrigger(new NodePostBooter(getNodeManager(), pwd));
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_PREDELETED_DELETED,
                                                   false,
                                                   Node.TABLENAME) {
      @Override
      public void trigger(PersistentRecord node,
                          Transaction transaction,
                          int state)
          throws MirrorNoSuchRecordException, MirrorTransactionTimeoutException
      {
        PersistentRecord nodeStyle = node.comp(NODESTYLE);
        for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES, transaction)) {
          getNodeManager().getRegionStyle()
                          .getContentCompiler(regionStyle)
                          .destroyInstance(node, regionStyle, transaction);
        }
      }
    });
    table.addFieldTrigger(new AbstractFieldTrigger<Integer>(NODEGROUPSTYLE_ID, true, false) {
      @Override
      protected Object triggerField(TransientRecord node,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        PersistentRecord nodeGroupStyle = node.comp(Node.NODEGROUPSTYLE);
        node.setField(Node.NODESTYLE_ID, nodeGroupStyle.opt(NodeGroupStyle.CHILDNODESTYLE_ID));
        PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
        int viewAclId = nodeStyle.compInt(NodeStyle.VIEWACL_ID);
        if (viewAclId != __defaultViewAclId) {
          node.setField(VIEWACL_ID, Integer.valueOf(viewAclId));
        }
        int editAclId = nodeStyle.compInt(NodeStyle.EDITACL_ID);
        if (editAclId != __defaultEditAclId) {
          node.setField(EDITACL_ID, Integer.valueOf(editAclId));
        }
        int publishAclId = nodeStyle.compInt(NodeStyle.PUBLISHACL_ID);
        if (publishAclId != __defaultPublishAclId) {
          node.setField(PUBLISHACL_ID, Integer.valueOf(publishAclId));
        }
        int deleteAclId = nodeStyle.compInt(NodeStyle.DELETEACL_ID);
        if (deleteAclId != __defaultDeleteAclId) {
          node.setField(DELETEACL_ID, Integer.valueOf(deleteAclId));
        }
        Boolean isPublished = nodeStyle.comp(NodeStyle.DEFAULTISPUBLISHED);
        if (isPublished.booleanValue() != Node.ISPUBLISHED_DEFAULT) {
          node.setField(Node.ISPUBLISHED, isPublished);
        }
        return null;
      }
    });
    _styleSpecificNodePreBooter = new StyleSpecificNodePreBooter(db);
    table.addPreInstantiationTrigger(_styleSpecificNodePreBooter);
    table.addRecordOperation(new YoungerNodeSiblings(db));
    table.addRecordOperation(new ElderNodeSiblings(db));
    table.addFieldTrigger(new NodeOwnerUpdateTrigger());
    UserRecordOperation<RecordSource> defaultChildrenMenuNodes =
        new UserRecordOperation<RecordSource>(Node.CHILDRENMENUNODES,
                                              getNodeManager(),
                                              User.ID_ANONYMOUS) {
          private final String __ordering =
              GettableUtils.get(getNodeManager(),
                                INITPARAM_NODE_CHILDRENMENUNODES_ORDERING,
                                INITPARAM_NODE_CHILDRENMENUNODES_ORDERING_DEFAULT);

          private final PrivateCacheKey CACHEKEY =
              new PrivateCacheKey("Node.childrenMenuNodes.dcmnf");

          @Override
          public RecordSource resolveParametricRequest(TransientRecord node,
                                                       TransientRecord user,
                                                       Viewpoint viewpoint)
          {
            String filter = (String) node.getCachedValue(CACHEKEY);
            if (filter == null) {
              StringBuilder buf = new StringBuilder(70);
              buf.append("(Node.parentNode_id=")
                 .append(node.getId())
                 .append(") and (Node.isMenuItem=1) and (Node.isPublished=1)");
              filter = buf.toString();
              node.setCachedValue(CACHEKEY, filter);
            }
            return RecordSources.viewedFrom(getTable().getQuery(filter, filter, __ordering),
                                            viewpoint);
          }
        };
    _childrenMenuNodes =
        new UserStyleSpecificNodeRecordOperation<RecordSource>(CHILDRENMENUNODES,
                                                               getNodeManager(),
                                                               User.ID_ANONYMOUS,
                                                               defaultChildrenMenuNodes);
    table.addRecordOperation(_childrenMenuNodes);
    table.addRecordOperation(new BreadcrumbTrail());
    table.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(Node.ORDEREDBYPATH) {
      private final RecordOrdering __recordOrdering =
          new AbstractRecordOrdering(Node.ORDEREDBYPATH.getKeyName(),
                                     "Ordered by path",
                                     null,
                                     BYPATH);

      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                      Viewpoint viewpoint)
      {
        return new RecordSourceOrdering(table, null, recordSource, __recordOrdering);
      }
    });
    table.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(Node.RS_ISPATHPUBLISHED) {
      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource nodes,
                                                      Viewpoint viewpoint)
      {
        return RecordSources.viewedFrom(new SingleRecordSourceIdFilter(nodes, null) {
          @Override
          protected boolean accepts(int id)
          {
            return getNodeManager().getUnpublishedNodes().isPublished(id);
          }

          @Override
          public String toString()
          {
            return String.format("%s.isPathPublished", getWrappedRecordSource());
          }
        }, viewpoint);
      }
    });
    // table.addQueryOperation(new AbstractQueryOperation(Node.ORDEREDBYPATH) {
    // protected Object internalGetOperation(Query targetQuery,
    // Transaction viewpoint)
    // {
    // return
    // NodePathComparator.sortByPath(targetQuery.getRecordList(viewpoint)
    // .iterator());
    // }
    // });
    table.addRecordOperation(new Node.Url(getNodeManager()));
    CachingMap<Object, Object> cachingMap =
        new CachingMap<Object, Object>(HREFS, null, 8, 0.75f, 1) {
          @Override
          protected void populateEmptyMap(Map<Object, Object> map,
                                          TransientRecord node,
                                          Viewpoint viewpoint)
          {
            // do nothing as we build values in CachingMapResolver...
          }
        };
    cachingMap.addTableChangedTable(table);
    table.addRecordOperation(cachingMap);
    table.addRecordOperation(new CachingMapResolver<String, String>(HREF,
                                                                    "hrefs",
                                                                    Casters.TOSTRING) {
      @Override
      protected String filterMapKey(Map<String, String> map,
                                    TransientRecord record,
                                    Viewpoint viewpoint,
                                    String key)
      {
        if (record.hasRecordOperation(key)) {
          key = StringUtils.noHtml(record.getString(key));
        }
        return key;
      }

      @Override
      protected String filterMapValue(Map<String, String> map,
                                      TransientRecord node,
                                      Viewpoint viewpoint,
                                      String key,
                                      String value)
      {
        if (value != null) {
          return value;
        }
        String href = constructHref(node,
                                    key,
                                    URL,
                                    "nodestyle" + node.opt(Node.NODESTYLE_ID),
                                    generateLinkTitle(node, key),
                                    node.is(ISPUBLISHED));
        map.put(key, href);
        return href;
      }
    });
    table.addRecordOperation(new CachingMapResolver<String, String>(EXPLICITHREF,
                                                                    "hrefs",
                                                                    Casters.TOSTRING) {
      @Override
      protected String filterMapValue(Map<String, String> map,
                                      TransientRecord node,
                                      Viewpoint viewpoint,
                                      String var,
                                      String value)
      {
        if (value != null) {
          return value;
        }
        String linkContents = var.toString();
        String href = constructHref(node,
                                    linkContents,
                                    URL,
                                    "nodestyle" + node.opt(Node.NODESTYLE_ID),
                                    generateLinkTitle(node, linkContents),
                                    node.is(ISPUBLISHED));
        map.put(var, href);
        return href;
      }
    });
    // $node.compile.<regionStyleName/regionStyle>
    class Compile
      extends MonoRecordOperation<Object, Gettable>
    {
      public Compile()
      {
        super(COMPILE,
              Casters.NULL);
      }

      @Override
      public Gettable calculate(TransientRecord node,
                                Viewpoint viewpoint,
                                Object var)
      {
        if (var instanceof PersistentRecord) {
          return compileOpt((PersistentRecord) node, (PersistentRecord) var, null, viewpoint);
        } else {
          return compileOpt((PersistentRecord) node, StringUtils.toString(var), null, viewpoint);
        }
      }
    }
    table.addRecordOperation(new Compile());
    // $node.parametricCompile.<regionStyleName>.<nodeRequest>
    class ParametricCompile
      extends DuoRecordOperation<Object, NodeRequest, Gettable>
    {
      public ParametricCompile()
      {
        super(PARAMETRICCOMPILE,
              Casters.NULL,
              Casters.<NodeRequest> CAST());
      }

      @Override
      public Gettable calculate(TransientRecord node,
                                Viewpoint viewpoint,
                                Object regionStyle,
                                NodeRequest nodeRequest)
      {
        try {
          if (regionStyle instanceof PersistentRecord) {
            return compile((PersistentRecord) node,
                           (PersistentRecord) regionStyle,
                           nodeRequest,
                           viewpoint);
          } else {
            return compile((PersistentRecord) node,
                           StringUtils.toString(regionStyle),
                           nodeRequest,
                           viewpoint);
          }
        } catch (MirrorNoSuchRecordException mnsrEx) {
          IllegalArgumentException iaEx =
              new IllegalArgumentException(node + "." + getName() + "." + regionStyle + "."
                                           + nodeRequest + " cannot be resolved");
          iaEx.initCause(mnsrEx);
          throw iaEx;
        }
      }
    }
    ;
    table.addRecordOperation(new ParametricCompile());
    table.addFieldTrigger(ImmutableFieldTrigger.create(Node.NODESTYLE_ID));
    table.addFieldTrigger(ImmutableFieldTrigger.create(Node.CREATIONTIME));
    class ChildrenNodesByNodeGroupStyle
      extends MonoRecordOperation<Object, RecordSource>
    {
      public ChildrenNodesByNodeGroupStyle()
      {
        super(CHILDRENNODESBYNODEGROUPSTYLE,
              Casters.NULL);
      }

      @Override
      public RecordSource calculate(TransientRecord node,
                                    Viewpoint viewpoint,
                                    Object nodeGroupStyleObj)
      {
        int nodeGroupStyleId = 0;
        if (nodeGroupStyleObj instanceof Number) {
          nodeGroupStyleId = ((Number) nodeGroupStyleObj).intValue();
        } else if (nodeGroupStyleObj instanceof TransientRecord) {
          TransientRecord nodeGroupStyle = (TransientRecord) nodeGroupStyleObj;
          if (!nodeGroupStyle.getTable().getName().equals(NodeGroupStyle.TABLENAME)) {
            return RecordSources.viewedFrom(getTable().getEmptyRecordSource(), viewpoint);
          }
          nodeGroupStyleId = nodeGroupStyle.getId();
        } else {
          String nodeGroupStyleName = StringUtils.toString(nodeGroupStyleObj);
          try {
            nodeGroupStyleId = getNodeManager().getNodeGroupStyle()
                                               .getInstance(node.compInt(NODESTYLE_ID),
                                                            nodeGroupStyleName,
                                                            viewpoint)
                                               .getId();
          } catch (MirrorNoSuchRecordException mnsrEx) {
            return RecordSources.viewedFrom(getTable().getEmptyRecordSource(), viewpoint);
          }
        }
        return RecordSources.viewedFrom(getChildNodesByNodeGroupStyle(node.getId(),
                                                                      nodeGroupStyleId),
                                        viewpoint);
      }
    }
    table.addRecordOperation(new ChildrenNodesByNodeGroupStyle());
    class ChildrenNodesByNodeStyle
      extends MonoRecordOperation<Object, RecordSource>
    {
      public ChildrenNodesByNodeStyle()
      {
        super(CHILDRENNODESBYNODESTYLE,
              Casters.NULL);
      }

      @Override
      public RecordSource calculate(TransientRecord node,
                                    Viewpoint viewpoint,
                                    Object nodeStyleObj)
      {
        int nodeStyleId = 0;
        RecordSource result = null;
        if (nodeStyleObj instanceof Number) {
          nodeStyleId = ((Number) nodeStyleObj).intValue();
        } else if (nodeStyleObj instanceof TransientRecord) {
          TransientRecord nodeStyle = (TransientRecord) nodeStyleObj;
          if (!nodeStyle.getTable().getName().equals(NodeStyle.TABLENAME)) {
            throw new IllegalArgumentException("Cannot pass " + nodeStyleObj + " to " + node
                                               + ".childrenNodesByStyle, NodeStyle expected");
          }
          nodeStyleId = nodeStyle.getId();
        }
        if (nodeStyleId != 0) {
          result = RecordSources.viewedFrom(getChildNodesByNodeStyle(node.getId(), nodeStyleId),
                                            viewpoint);
        } else {
          String nodeStyleName = StringUtils.toString(nodeStyleObj);
          result = RecordSources.viewedFrom(getChildNodesByNodeStyle(node.getId(), nodeStyleName),
                                            viewpoint);
        }
        return result;
      }
    }
    table.addRecordOperation(new ChildrenNodesByNodeStyle());
    class AsFormElement
      extends MonoRecordOperation<String, Macro>
    {
      public AsFormElement()
      {
        super(ASFORMELEMENT,
              Casters.TOSTRING);
      }

      @Override
      public Macro calculate(final TransientRecord node,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            node.appendFormElement("", name, out, context);
          }
        };
      }
    }
    table.addRecordOperation(new AsFormElement());
    table.addRecordOperation(new MonoRecordOperation<String, Macro>(ASFORMVALUE, Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord node,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            node.appendFormValue("", name, out, context);
          }
        };
      }
    });
    class IsPathPublished
      extends SimpleRecordOperation<Boolean>
    {
      public IsPathPublished()
      {
        super(ISPATHPUBLISHED);
      }
      @Override
      public Boolean getOperation(TransientRecord node,
                                  Viewpoint viewpoint)
      {
        if (node instanceof PersistentRecord) {
          return Boolean.valueOf(getNodeManager().getUnpublishedNodes().isPublished(node));
        }
        return Boolean.FALSE;
      }
    }
    table.addRecordOperation(new IsPathPublished());
    class IsMoveable
      extends SimpleRecordOperation<Boolean>
    {
      public IsMoveable()
      {
        super(ISMOVEABLE);
      }
      @Override
      public Boolean getOperation(TransientRecord node,
                                  Viewpoint viewpoint)
      {
        if (node instanceof PersistentRecord) {
          try {
            getNodeManager().getNode().assertIsMoveable((PersistentRecord) node);
            return Boolean.TRUE;
          } catch (NodeMoveException e) {
          }
        }
        return Boolean.FALSE;
      }
    }
    table.addRecordOperation(new IsMoveable());
    class NotMoveableReason
      extends SimpleRecordOperation<String>
    {
      public NotMoveableReason()
      {
        super(NOTMOVEABLEREASON);
      }
      @Override
      public String getOperation(TransientRecord node,
                                 Viewpoint viewpoint)
      {
        if (node instanceof PersistentRecord) {
          try {
            getNodeManager().getNode().assertIsMoveable((PersistentRecord) node);
          } catch (NodeMoveException e) {
            return e.getMessage();
          }
        }
        return "";
      }
    }
    table.addRecordOperation(new NotMoveableReason());
    class IsChildOf
      extends MonoRecordOperation<Object, Boolean>
    {
      public IsChildOf()
      {
        super(IS_CHILD_OF,
              Casters.NULL);
      }

      @Override
      public Boolean calculate(TransientRecord node,
                               Viewpoint viewpoint,
                               Object parentNodeObj)
      {
        if (!(node instanceof PersistentRecord)) {
          return Boolean.FALSE;
        }
        PersistentRecord childNode = (PersistentRecord) node;
        PersistentRecord parentNode = null;
        if (parentNodeObj instanceof PersistentRecord) {
          parentNode = (PersistentRecord) parentNodeObj;
        } else {
          try {
            parentNode = getTable().getRecord(parentNodeObj);
          } catch (MirrorNoSuchRecordException e) {
            throw new LogicException("Cannot resolve parentNode of " + parentNodeObj, e);
          }
        }
        return Boolean.valueOf(isChildOf(parentNode, childNode));
      }
    }
    table.addRecordOperation(new IsChildOf());
    class UniqueChildName
      extends MonoRecordOperation<String, String>
    {
      public UniqueChildName()
      {
        super(UNIQUE_CHILDNAME,
              Casters.TOSTRING);
      }

      @Override
      public String calculate(TransientRecord node,
                              Viewpoint viewpoint,
                              String filename)
      {
        return getUniqueChildName((PersistentRecord) node, StringUtils.toString(filename));
      }
    }
    table.addRecordOperation(new UniqueChildName());
    table.addRecordOperation(new ChildNamed(getNodeManager()));
    table.addPostInstantiationTrigger(new PostInstantiationTriggerImpl() {
      @Override
      public void triggerPost(PersistentRecord childNode,
                              Gettable params)
          throws MirrorException
      {
        getChildNamed().cacheChild(childNode);
      }
    });
    table.addPostRetrievalTrigger(new PostRetrievalTrigger() {
      @Override
      public void trigger(PersistentRecord childNode)
      {
        getChildNamed().cacheChild(childNode);
      }
    });
    class YoungestCompile
      extends MonoRecordOperation<String, Gettable>
    {
      public YoungestCompile()
      {
        super(YOUNGESTCOMPILE,
              Casters.TOSTRING);
      }

      @Override
      public Gettable calculate(TransientRecord node,
                                Viewpoint viewpoint,
                                String regionStyleName)
      {
        while (true) {
          try {
            Gettable compiled =
                getNodeManager().getNode()
                                .compile((PersistentRecord) node, regionStyleName, null, viewpoint);
            if (compiled instanceof PersistentRecord) {
              PersistentRecord instance = (PersistentRecord) compiled;
              try {
                if (instance.is(TableContentCompiler.ISDEFINED)) {
                  return instance;
                }
              } catch (IllegalArgumentException iaEx) {
                // not an TableContentCompiler so recurse...
              }
            }
          } catch (MirrorNoSuchRecordException e) {
            // no RegionStyle defined on node so recurse...
          }
          node = node.opt(PARENTNODE);
          if (node == null) {
            return null;
          }
        }
      }
    }
    table.addRecordOperation(new YoungestCompile());
    class IsDeletableBy
      extends MonoRecordOperation<Object, Boolean>
    {
      public IsDeletableBy()
      {
        super(ISDELETABLEBY,
              Casters.NULL);
      }

      @Override
      public Boolean calculate(TransientRecord node,
                               Viewpoint viewpoint,
                               Object userObj)
      {
        PersistentRecord user = null;
        if (userObj instanceof PersistentRecord) {
          user = (PersistentRecord) userObj;
        } else if (userObj instanceof NodeRequest) {
          user = ((NodeRequest) userObj).getUser();
        } else {
          user = getNodeManager().getUser().getTable().getCompRecord(NumberUtil.toInteger(userObj));
        }
        return Boolean.valueOf(isDeletable((PersistentRecord) node,
                                           user,
                                           false,
                                           ISDELETABLEBY.getKeyName()));
      }
    }
    table.addRecordOperation(new IsDeletableBy());
    getNodeManager().getJMgr().register(new Handler_Node(getTransactionPassword()));
  }

  private void appendNodePath(StringBuilder buf,
                              TransientRecord node)
  {
    PersistentRecord parentNode = node.opt(Node.PARENTNODE);
    if (parentNode == null) {
      return;
    }
    appendNodePath(buf, parentNode);
    buf.append(Node.PATH_SEPERATOR).append(node.comp(Node.FILENAME));
  }

  /**
   * Generate the tooltip that should be utilised for a link to a given Node. This is currently just
   * the description if the linkContents are the same as the title. Failing that it will either the
   * title of the Node or "title - description" if description has a value.
   * 
   * @see #HREF
   * @see #EXPLICITHREF
   */
  public String generateLinkTitle(TransientRecord node,
                                  String linkContents)
  {
    return __nodeLinkTitleGenerator.generateLinkTitle(node, linkContents);
  }

  protected ChildNamed getChildNamed()
  {
    return (ChildNamed) getTable().getRecordOperation(ChildNamed.NAME);
  }

  public UserStyleSpecificNodeRecordOperation<RecordSource> getChildrenMenuNodes()
  {
    return _childrenMenuNodes;
  }

  public final StyleSpecificNodePreBooter getStyleSpecificNodePreBooter()
  {
    return _styleSpecificNodePreBooter;
  }

  public class Url
    extends SimpleRecordOperation<String>
  {
    private final NodeManager __nodeMgr;

    protected Url(NodeManager nodeMgr)
    {
      super(URL);
      __nodeMgr = nodeMgr;
    }

    @Override
    public String getOperation(TransientRecord node,
                               Viewpoint viewpoint)
    {
      String servletPath = __nodeMgr.getHttpServletPath();
      if (Strings.isNullOrEmpty(servletPath)) {
        return node.comp(PATH);
      }
      StringBuilder buf = new StringBuilder();
      buf.append(servletPath);
      appendNodePath(buf, node.getPersistentRecord());
      return buf.toString();
    }
  }

  /**
   * Construct an href to a page.
   * 
   * @param record
   *          The record which is able to resolve details about the link target. This will normally
   *          either be a Node or a TableContentCompiler instance.
   * @param linkContents
   *          The String that wil be contained inside the a tag. There is no processing applied to
   *          this String and therefore it can contain HTML or whatever as required. It is the
   *          callers responsibility to do any sanitisation if it isn't trusted.
   * @param urlOperation
   *          The operation which when invoked on record will give you the URL that this link should
   *          point at
   * @param linkClass
   *          The optional CSS class that should be applied to the link
   * @param linkTitle
   *          The optional value that will appear in the title attribute of the link
   * @param isPublished
   *          If false, then the class of "unpublished" will be applied to the link (in addition to
   *          linkClass if defined). This lets you highlight links to elements that are yet to be
   *          published.
   * @return XHTML element
   */
  public static String constructHref(TransientRecord record,
                                     String linkContents,
                                     RecordOperationKey<String> urlOperation,
                                     String linkClass,
                                     String linkTitle,
                                     boolean isPublished)
  {
    final StringBuilder result = new StringBuilder(64);
    final String url = record.opt(urlOperation);
    result.append("<a href=\"").append(url).append("\"");
    if (!(Strings.isNullOrEmpty(linkClass) && isPublished)) {
      result.append(" class=\"");
      if (!isPublished) {
        result.append("unpublished ");
      }
      if (!Strings.isNullOrEmpty(linkClass)) {
        result.append(linkClass);
      }
      result.append('\"');
    }
    if (!Strings.isNullOrEmpty(linkTitle)) {
      result.append(" title=\"").append(StringUtils.toHtmlValue(linkTitle)).append("\"");
    }
    result.append(">");
    linkContents = StringUtils.trim(linkContents);
    if (Strings.isNullOrEmpty(linkContents)) {
      result.append(url);
    } else {
      result.append(linkContents);
    }
    result.append("</a>");
    return result.toString();
  }

  /**
   * Return a Query of all Nodes that are children of a given parent with a griven NodeGroupStyle.
   * This can be invoked from the webmacro interface with the following:-
   * <blockquote>$node.childrenNodesByStyle.get($nodeGroupStyle)</blockquote> where nodeGroupStyle
   * is either a number, PersistentRecord or a name.
   */
  public RecordSource getChildNodesByNodeGroupStyle(int parentNode_id,
                                                    int nodeGroupStyle_id)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("((Node.parentNode_id=")
       .append(parentNode_id)
       .append(") and (Node.nodeGroupStyle_id=")
       .append(nodeGroupStyle_id)
       .append("))");
    String filter = buf.toString();
    return getTable().getQuery(filter, filter, null);
  }

  /**
   * Return a Query that gets all the children of the parentNode_id that have a style of
   * nodeStyle_id
   */
  public RecordSource getChildNodesByNodeStyle(int parentNode_id,
                                               int nodeStyle_id)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("((Node.parentNode_id=")
       .append(parentNode_id)
       .append(") and (Node.nodeStyle_id=")
       .append(nodeStyle_id)
       .append("))");
    String filter = buf.toString();
    return getTable().getQuery(filter, filter, null);
  }

  /**
   * Return a Query that gets all the children of the parentNode_id that have a style with name
   * nodeStyle
   */
  public RecordSource getChildNodesByNodeStyle(int parentNode_id,
                                               String nodeStyleName)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("((Node.parentNode_id=")
       .append(parentNode_id)
       .append(") and (Node.nodeStyle_id=NodeStyle.id) and (NodeStyle.name=");
    SqlUtil.toSafeSqlString(buf, nodeStyleName);
    buf.append("))");
    String filter = buf.toString();
    return getTable().getQuery(filter, filter, null, getNodeManager().getNodeStyle().getTable());
  }

  @Deprecated
  public boolean permitsChildCreation(int parentNode_id,
                                      PersistentRecord nodeGroupStyle)
  {
    return getNodeManager().getNodeGroupStyle()
                           .mayCreateNode(getTable().getCompRecord(parentNode_id),
                                          nodeGroupStyle,
                                          null);
  }

  @Deprecated
  public boolean permitsChildCreation(PersistentRecord parentNode,
                                      PersistentRecord nodeGroupStyle,
                                      int userId)
  {
    return getNodeManager().getNodeGroupStyle()
                           .mayCreateNode(parentNode,
                                          nodeGroupStyle,
                                          getNodeManager().getUser()
                                                          .getTable()
                                                          .getCompRecord(userId));
  }

  @Deprecated
  public boolean permitsChildCreation(PersistentRecord parentNode,
                                      PersistentRecord nodeGroupStyle,
                                      NodeRequest nodeRequest)
  {
    return getNodeManager().getNodeGroupStyle()
                           .mayCreateNode(parentNode, nodeGroupStyle, nodeRequest.getUser());
  }

  /**
   * @throws MirrorNoSuchRecordException
   *           If regionStyleName is not registered as a valid RegionStyle below the NodeStyle that
   *           node implements.
   */
  public Gettable compile(PersistentRecord node,
                          String regionStyleName,
                          NodeRequest nodeRequest,
                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord regionStyleInstance =
        getNodeManager().getRegionStyle()
                        .getInstance(node.compInt(NODESTYLE_ID), regionStyleName, viewpoint);
    return compile(node, regionStyleInstance, nodeRequest, viewpoint);
  }

  public Gettable compileOpt(PersistentRecord node,
                             String regionStyleName,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    PersistentRecord regionStyleInstance =
        getNodeManager().getRegionStyle()
                        .getOptInstance(node.compInt(NODESTYLE_ID), regionStyleName, viewpoint);
    if (regionStyleInstance == null) {
      return null;
    }
    return compileOpt(node, regionStyleInstance, nodeRequest, viewpoint);
  }

  public Gettable compile(PersistentRecord node,
                          PersistentRecord regionStyle,
                          NodeRequest nodeRequest,
                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    ContentCompiler contentCompiler =
        getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
    return contentCompiler.compile(node, regionStyle, nodeRequest, viewpoint);
  }

  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    ContentCompiler contentCompiler =
        getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
    return contentCompiler.compileOpt(node, regionStyle, nodeRequest, viewpoint);
  }

  public PersistentRecord createNode(int parentNode_id,
                                     int nodeGroupStyle_id,
                                     Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    return createNode(parentNode_id, nodeGroupStyle_id, null, null, null, null, null, params);
  }

  private Object definedOrGettable(Object defined,
                                   Gettable params,
                                   FieldKey<?> key)
  {
    if (defined != null) {
      return defined;
    }
    if (params != null) {
      return params.get(key.getKeyName());
    }
    return null;
  }

  public PersistentRecord createNode(PersistentRecord parentNode,
                                     PersistentRecord nodeGroupStyle,
                                     Integer owner_id,
                                     String filename,
                                     String title,
                                     String htmlTitle,
                                     Boolean isPublished,
                                     Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException
  {
    TransientRecord tNode = getTable().createTransientRecord();
    if (nodeGroupStyle.compInt(NodeGroupStyle.PARENTNODESTYLE_ID) != parentNode.compInt(Node.NODESTYLE_ID)) {
      throw new MirrorFieldException(String.format("Cannot create a node in %s under %s of style %s",
                                                   nodeGroupStyle,
                                                   parentNode,
                                                   parentNode.comp(Node.NODESTYLE)),
                                     null,
                                     tNode,
                                     Node.NODEGROUPSTYLE_ID);
    }
    final PersistentRecord nodeStyle = nodeGroupStyle.comp(NodeGroupStyle.CHILDNODESTYLE);
    if (!nodeStyle.is(NodeStyle.ISEDITABLEFILENAME)) {
      filename = nodeStyle.comp(NodeStyle.FILENAME);
    }
    tNode.setField(PARENTNODE_ID, Integer.valueOf(parentNode.getId()));
    tNode.setField(NODEGROUPSTYLE_ID, Integer.valueOf(nodeGroupStyle.getId()));
    if (params != null) {
      filename = filename == null ? params.getString(FILENAME) : filename;
      // owner_id =
      // owner_id == null ? NumberUtil.toInteger(params.get(OWNER_ID),
      // null,
      // null) : owner_id;
      title = title == null ? params.getString(TITLE) : title;
      htmlTitle = htmlTitle == null ? params.getString(HTMLTITLE) : htmlTitle;
      if (isPublished == null) {
        isPublished = params.getBoolean(ISPUBLISHED);
      }
    }
    if (!Strings.isNullOrEmpty(filename)) {
      tNode.setField(FILENAME, 
                     filename.indexOf(FILENAME_ID) == -1 ? 
                         filename : 
                           filename.replace(Node.FILENAME_ID, 
                                            Integer.toString(tNode.getPendingId())));
    }
    tNode.setFieldIfDefined(OWNER_ID, definedOrGettable(owner_id, params, OWNER_ID));
    // if (owner_id != null) {
    // tNode.setField(OWNER_ID, owner_id);
    // }
    if (title == null) {
      title = filename;
      if (title != null && title.indexOf(FILENAME_ID) != -1) {
        title = title.replace(FILENAME_ID, "").trim();
      }
    }
    tNode.setFieldIfDefined(TITLE, title);
    tNode.setFieldIfDefined(HTMLTITLE,
                            MoreObjects.firstNonNull(htmlTitle,
                                                     MoreObjects.firstNonNull(title, filename)));
    if (params != null) {
      tNode.setFieldIfDefined(DESCRIPTION, params, DESCRIPTION);
      tNode.setFieldIfDefined(NUMBER, params, Node.NUMBER);
      tNode.setFieldIfDefined(ISNOINDEX, params, ISNOINDEX);
      tNode.setFieldIfDefined(ISNOFOLLOW, params, ISNOFOLLOW);
      tNode.setFieldIfDefined(ISMENUITEM, params, ISMENUITEM);
      tNode.setFieldIfDefined(ISSEARCHABLE, params, ISSEARCHABLE);
    }
    if (Boolean.TRUE.equals(isPublished) && !tNode.is(Node.ISPUBLISHED)) {
      PersistentRecord pNode = tNode.makePersistent(params);
      try (Transaction transaction =
          getNodeManager().getDb()
                          .createTransaction(Db.DEFAULT_TIMEOUT,
                                             Transaction.TRANSACTION_UPDATE,
                                             getTransactionPassword(),
                                             "isPublished")) {
        WritableRecord wNode = transaction.edit(pNode);
        wNode.setField(Node.ISPUBLISHED, Boolean.TRUE);
        transaction.commit();
      }
      return pNode;
    } else {
      return tNode.makePersistent(params);
    }
  }
  public PersistentRecord createNode(int parentNode_id,
                                     int nodeGroupStyle_id,
                                     Integer owner_id,
                                     String filename,
                                     String title,
                                     String htmlTitle,
                                     Boolean isPublished,
                                     Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    return createNode(getNodeManager().getNode().getTable().getRecord(parentNode_id),
                      getNodeManager().getNodeGroupStyle().getTable().getRecord(nodeGroupStyle_id),
                      owner_id,
                      filename,
                      title,
                      htmlTitle,
                      isPublished,
                      params);
  }

  public PersistentRecord createNode(int parentNode_id,
                                     int nodeGroupStyle_id,
                                     String filename,
                                     String title,
                                     String htmlTitle,
                                     boolean isPublished,
                                     Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    return createNode(parentNode_id,
                      nodeGroupStyle_id,
                      (Integer) GettableUtils.padNull(params).get(Node.OWNER_ID),
                      filename,
                      title,
                      htmlTitle,
                      Boolean.valueOf(isPublished),
                      params);
  }

  /**
   * Resolve the given path into a node id by traversing the node hierarchy by filename. Note that
   * the hostname is not required to resolve this call.
   */
  public PersistentRecord getNodeByPath(String path,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    String[] filenames = path.split("/");
    PersistentRecord node = getRootNode(viewpoint);
    for (int i = 0; i < filenames.length; i++) {
      String filename = filenames[i];
      if (filename.length() > 0) {
        node = getChildNodeByFilename(node, filename, viewpoint);
      }
    }
    return node;
  }

  /**
   * Build a Query to resolve the given child node of a parent node id and extract the first record
   * from the query. This should not be invoked in general as it is likely to be inefficient,
   * instead people should use getChildNodeByFilename as then the caching methods work more
   * efficiently.
   * 
   * @return The appropriate id or null if there are no matching records
   * @see #getChildNodeByFilename(TransientRecord, String, Viewpoint)
   */
  public Integer optNodeIdByFilename(int parentNodeId,
                                     String filename)
  {
    StringBuilder buf = new StringBuilder(64);
    buf.append("Node.parentNode_id=").append(parentNodeId).append(" and Node.filename=");
    SqlUtil.toSafeSqlString(buf, filename);
    String filter = buf.toString();
    Query query = getTable().getQuery(filter, filter, Query.NOSQLORDERING);
    IdList nodeIds = query.getIdList();
    switch (nodeIds.size()) {
      case 0:
        return null;
      case 1:
        return Integer.valueOf(nodeIds.getInt(0));
      default:
        throw new MirrorLogicException(String.format("%s returns multipled records: %s",
                                                     query,
                                                     query.getRecordList()),
                                       null);
    }
  }

  /**
   * Find the child of a parentNode with a given filename utilising full caching techniques.
   * 
   * @see ChildNamed
   */
  public PersistentRecord getChildNodeByFilename(TransientRecord parentNode,
                                                 String filename,
                                                 Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord childNode = getOptNodeByFilename(parentNode, filename, viewpoint);
    if (childNode == null) {
      throw new MirrorNoSuchRecordException("\"" + filename + "\" is not a child of " + parentNode,
                                            getTable());
    }
    return childNode;
  }

  /**
   * @return The appropriate node or null if there is no node of the given filename
   * @see ChildNamed
   */
  public PersistentRecord getOptNodeByFilename(TransientRecord parentNode,
                                               String filename,
                                               Viewpoint viewpoint)
  {
    return parentNode.opt(ChildNamed.NAME, viewpoint, filename);

  }

  public PersistentRecord createOrResolveNode(int parentNode_id,
                                              int nodeGroupStyle_id,
                                              String filename,
                                              String title,
                                              String htmlTitle,
                                              boolean isPublished,
                                              Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    try {
      return createNode(parentNode_id,
                        nodeGroupStyle_id,
                        filename,
                        title,
                        htmlTitle,
                        isPublished,
                        params);
    } catch (MirrorInstantiationException miEx) {
      PersistentRecord relatedNode = getRelatedNode(miEx);
      if (relatedNode != null) {
        return relatedNode;
      }
      throw miEx;
    }
  }

  /**
   * If the Throwable is a RelatedMirrorFieldException and if the related record is from Node then
   * return it, otherwise return null
   * 
   * @see RelatedMirrorFieldException#getRelatedRecord(Throwable)
   */
  public PersistentRecord getRelatedNode(Throwable throwable)
  {
    PersistentRecord record = RelatedMirrorFieldException.getRelatedRecord(throwable);
    if (record != null && record.getTable().getName().equals(Node.TABLENAME)) {
      return record;
    }
    return null;
  }

  /**
   * Create a Node if necessary and then update it with values from inputs. Note that if there is
   * already a Node with the given sanitised {@link Node#FILENAME} under parentNode_id then this
   * Node will be updated rather than a new one being created. Use
   * {@link #createAndUpdateNewNode(int, int, Gettable, Map, String)} if this is not what you want.
   */
  public PersistentRecord createAndUpdateNode(int parentNode_id,
                                              int nodeGroupStyle_id,
                                              Gettable inputs,
                                              Map<Object, Object> outputs,
                                              String pwd)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    PersistentRecord pNode = null;
    try {
      pNode = createNode(parentNode_id, nodeGroupStyle_id, inputs);
    } catch (MirrorInstantiationException miEx) {
      pNode = getRelatedNode(miEx);
      if (pNode == null) {
        throw miEx;
      }
    }
    updateNode(pNode, inputs, outputs, pwd);
    return pNode;
  }

  /**
   * Create a new Node and then update it with values from inputs. If there is already a Node with
   * the given sanitised {@link Node#FILENAME} then this will fail with a
   * {@link MirrorInstantiationException}.
   */
  public PersistentRecord createAndUpdateNewNode(int parentNode_id,
                                                 int nodeGroupStyle_id,
                                                 Gettable inputs,
                                                 Map<Object, Object> outputs,
                                                 String pwd)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    PersistentRecord pNode = createNode(parentNode_id, nodeGroupStyle_id, inputs);
    updateNode(pNode, inputs, outputs, pwd);
    return pNode;
  }

  public void updateNode(PersistentRecord node,
                         Gettable inputs,
                         Map<Object, Object> outputs,
                         String pwd)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    try (
        Transaction transaction = getNodeManager().getDb()
                                                  .createTransaction(Db.DEFAULT_TIMEOUT,
                                                                     Transaction.TRANSACTION_UPDATE,
                                                                     pwd,
                                                                     "Node.updateNode")) {
      WritableRecord wNode = transaction.edit(node);
      updateNode(wNode, transaction, inputs, outputs);
      transaction.commit();
    }
  }

  /**
   * Create a copy of the given node in the target location.
   * 
   * @param successMessages
   *          A message list that if defined will get one message for each node that is copied
   *          stating where it was copied from and to.
   * @param failureMessages
   *          A message list that if defined will get one message for each node that fails to copy.
   *          If the list is not defined then the message will be thrown as an Exception and the
   *          copy operation will cease. If it is defined then once the message has been logged and
   *          then the remaining nodes that haven't been attempted copied yet will be tried.
   * @return The copy of the node or null if there was a problem copying the node and
   *         failureMessages was defined.
   */
  public PersistentRecord copyNode(PersistentRecord fromNode,
                                   PersistentRecord toParentNode,
                                   PersistentRecord toNodeGroupStyle,
                                   boolean copyChildren,
                                   Integer fallbackToNodeStyleId,
                                   String toFilename,
                                   Boolean toIsPublished,
                                   List<String> successMessages,
                                   List<String> failureMessages)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    return copyNode(fromNode,
                    toParentNode,
                    toNodeGroupStyle,
                    0,
                    copyChildren,
                    fallbackToNodeStyleId,
                    toFilename,
                    toIsPublished,
                    null,
                    successMessages,
                    failureMessages);
  }

  /**
   * Create a copy of the given node in the target location.
   * 
   * @param fromNode
   * @param targetParentNode_id
   * @param targetNodeGroupStyle_id
   * @param nodeIndex
   *          The position that the fromNode appeared in the list of nodes in the
   *          targetNodeGroupStyle index grouping.
   * @param toFilename
   * @param toIsPublished
   * @param toNodeId
   *          The id of the original copy of the root of the fromNode tree.
   * @param successMessages
   *          A message list that if defined will get one message for each node that is copied
   *          stating where it was copied from and to.
   * @param failureMessages
   *          A message list that if defined will get one message for each node that fails to copy.
   *          If the list is not defined then the message will be thrown as an Exception and the
   *          copy operation will cease. If it is defined then once the message has been logged and
   *          then the remaining nodes that haven't been attempted copied yet will be tried.
   * @return The copy of the node or null if there was a problem copying the node and
   *         failureMessages was defined.
   * @throws MirrorNoSuchRecordException
   * @throws MirrorTransactionTimeoutException
   * @throws MirrorInstantiationException
   * @throws MirrorFieldException
   * @throws FeedbackErrorException
   */
  private PersistentRecord copyNode(PersistentRecord fromNode,
                                    PersistentRecord toParentNode,
                                    PersistentRecord toNodeGroupStyle,
                                    int nodeIndex,
                                    boolean copyChildren,
                                    Integer fallbackToNodeStyleId,
                                    String toFilename,
                                    Boolean toIsPublished,
                                    Integer toNodeId,
                                    List<String> successMessages,
                                    List<String> failureMessages)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    TransientRecord.assertIsTableNamed(fromNode, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(toParentNode, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(toNodeGroupStyle, NodeGroupStyle.TABLENAME);
    if (toNodeId != null && fromNode.getId() == toNodeId.intValue()) {
      return null;
    }
    PersistentRecord toParentNodeStyle = toParentNode.comp(Node.NODESTYLE);
    final NodeManager _NodeManager = getNodeManager();
    final NodeGroupStyle _NodeGroupStyle = _NodeManager.getNodeGroupStyle();
    if (toParentNodeStyle.getId() != toNodeGroupStyle.compInt(NodeGroupStyle.PARENTNODESTYLE_ID)) {
      if (fallbackToNodeStyleId != null) {
        toNodeGroupStyle =
            RecordSources.getOptFirstRecord(_NodeGroupStyle.getChildrenNodeGroupStyles(toParentNodeStyle.getId(),
                                                                                       fallbackToNodeStyleId.intValue()),
                                            null);
        if (toNodeGroupStyle == null) {
          String errorMessage = "Cannot copy " + fromNode + " because " + fallbackToNodeStyleId
                                + " is not a valid fallback child NodeStyle of " + toParentNode;
          if (failureMessages == null) {
            throw new IllegalArgumentException(errorMessage);
          } else {
            failureMessages.add(errorMessage);
            return null;
          }
        }
      } else {
        String errorMessage = "Cannot copy " + fromNode + " because " + toNodeGroupStyle
                              + " is not a valid child of " + toParentNode;
        if (failureMessages == null) {
          throw new IllegalArgumentException(errorMessage);
        } else {
          failureMessages.add(errorMessage);
          return null;
        }
      }
    }
    Settable settable = new SettableMap();
    boolean hasPostCopyableContentCompiler = false;
    final RegionStyle _RegionStyle = _NodeManager.getRegionStyle();
    try {
      if (Strings.isNullOrEmpty(toFilename)) {
        toFilename = null;
      }
      if (fromNode.compInt(Node.PARENTNODE_ID) == toParentNode.getId()) {
        settable.set(FILENAME, toFilename);
      } else {
        settable.set(FILENAME, toFilename == null ? fromNode.opt(FILENAME) : toFilename);
      }
      settable.set(HTMLTITLE, fromNode.opt(HTMLTITLE));
      settable.set(TITLE, fromNode.opt(TITLE));
      settable.set(OWNER_ID, fromNode.opt(OWNER_ID));
      settable.set(NUMBER, fromNode.opt(NUMBER));
      settable.set(ISPUBLISHED, toIsPublished == null ? fromNode.opt(ISPUBLISHED) : toIsPublished);
      settable.set(DESCRIPTION, fromNode.opt(DESCRIPTION));
      settable.set(ISNOINDEX, fromNode.opt(ISNOINDEX));
      settable.set(ISNOFOLLOW, fromNode.opt(ISNOFOLLOW));
      settable.set(ISMENUITEM, fromNode.opt(ISMENUITEM));
      settable.set(ISSEARCHABLE, fromNode.opt(ISSEARCHABLE));
      for (PersistentRecord regionStyle : fromNode.comp(Node.NODESTYLE)
                                                  .comp(NodeStyle.REGIONSTYLES)) {
        ContentCompiler contentCompiler = _RegionStyle.getContentCompiler(regionStyle);
        if (contentCompiler instanceof CopyableContentCompiler) {
          Settable namespaceSettable = _RegionStyle.getNamespaceSettable(regionStyle, settable);
          ((CopyableContentCompiler) contentCompiler).copyContentCompiler(namespaceSettable,
                                                                          fromNode,
                                                                          regionStyle,
                                                                          regionStyle);
        }
        if (contentCompiler instanceof PostCopyableContentCompiler) {
          hasPostCopyableContentCompiler = true;
        }
      }
    } catch (SettableException setEx) {
      throw new LogicException("SettableMap shouldn't throw SettableException", setEx);
    }
    final PersistentRecord toNode;
    int max = toNodeGroupStyle.compInt(NodeGroupStyle.MAXIMUM);
    if (max == NodeGroupStyle.MAXIMUM_UNLIMITED) {
      toNode = createAndUpdateNode(toParentNode.getId(),
                                   toNodeGroupStyle.getId(),
                                   settable,
                                   null,
                                   getTransactionPassword());
    } else {
      RecordSource autoCreateNodes =
          getChildNodesByNodeGroupStyle(toParentNode.getId(), toNodeGroupStyle.getId());
      if (max == autoCreateNodes.getRecordList(null).size()) {
        toNode = autoCreateNodes.getRecordList(null).get(nodeIndex);
        try (Transaction t = _NodeManager.getDb()
                                         .createTransaction(Db.DEFAULT_TIMEOUT,
                                                            Transaction.TRANSACTION_UPDATE,
                                                            getTransactionPassword(),
                                                            "copyNode")) {
          updateNode(toNode, t, settable, null);
          t.commit();
        }
      } else {
        toNode = createAndUpdateNode(toParentNode.getId(),
                                     toNodeGroupStyle.getId(),
                                     settable,
                                     null,
                                     getTransactionPassword());
      }
    }
    if (hasPostCopyableContentCompiler) {
      try (
          Transaction t = _NodeManager.getDb()
                                      .createTransaction(Db.DEFAULT_TIMEOUT,
                                                         Transaction.TRANSACTION_UPDATE,
                                                         getTransactionPassword(),
                                                         "copyNode-PostCopyableContentCompilers")) {
        WritableRecord rwToNode = t.edit(toNode);
        for (PersistentRecord regionStyle : fromNode.comp(Node.NODESTYLE)
                                                    .comp(NodeStyle.REGIONSTYLES)) {
          ContentCompiler contentCompiler = _RegionStyle.getContentCompiler(regionStyle);
          if (contentCompiler instanceof PostCopyableContentCompiler) {
            ((PostCopyableContentCompiler) contentCompiler).copyContentCompiler(fromNode,
                                                                                regionStyle,
                                                                                rwToNode,
                                                                                t);
          }
        }
        t.commit();
      }
    }
    if (successMessages != null) {
      successMessages.add(fromNode + " copied to " + toNode);
    }
    if (toNodeId == null) {
      toNodeId = Integer.valueOf(toNode.getId());
    }
    if (copyChildren) {
      for (PersistentRecord childNodeGroupStyle : fromNode.opt(Node.NODESTYLE)
                                                          .opt(NodeStyle.CHILDRENNODEGROUPSTYLES)) {
        RecordSource childNodes =
            getChildNodesByNodeGroupStyle(fromNode.getId(), childNodeGroupStyle.getId());
        int j = 0;
        for (PersistentRecord childNode : childNodes) {
          copyNode(childNode,
                   toNode,
                   childNodeGroupStyle,
                   j,
                   copyChildren,
                   fallbackToNodeStyleId,
                   null,
                   null,
                   toNodeId,
                   successMessages,
                   failureMessages);
          j++;
        }
      }
    }
    return toNode;
  }

  /**
   * @param proposedFilename
   *          The optional filename that the node will utilise if possible. If not defined (or
   *          empty) then the current filename will be utilised if possible.
   * @see #assertIsMoveable(PersistentRecord)
   */
  public PersistentRecord moveNode(PersistentRecord fromNode,
                                   PersistentRecord toParentNode,
                                   PersistentRecord toNodeGroupStyle,
                                   String proposedFilename)
      throws NodeMoveException
  {
    TransientRecord.assertIsTableNamed(fromNode, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(toParentNode, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(toNodeGroupStyle, NodeGroupStyle.TABLENAME);
    assertIsMoveable(fromNode);
    PersistentRecord fromNodeParent = fromNode.comp(Node.PARENTNODE);
    if (fromNodeParent == toParentNode) {
      return fromNode;
    }
    String fromNodeFilename = fromNode.opt(Node.FILENAME);
    PersistentRecord fromNodeStyle = fromNode.comp(Node.NODESTYLE);
    if (fromNodeStyle.getId() != toNodeGroupStyle.compInt(NodeGroupStyle.CHILDNODESTYLE_ID)) {
      throw new NodeMoveException(String.format("%s cannot have %s of NodeStyle %s as a child because it is not a %s",
                                                toNodeGroupStyle,
                                                fromNode,
                                                fromNode.opt(Node.NODESTYLE),
                                                toNodeGroupStyle.opt(NodeGroupStyle.CHILDNODESTYLE)),
                                  null);
    }
    PersistentRecord toParentNodeStyle = toParentNode.comp(Node.NODESTYLE);
    if (toParentNodeStyle.getId() != toNodeGroupStyle.compInt(NodeGroupStyle.PARENTNODESTYLE_ID)) {
      throw new NodeMoveException(toNodeGroupStyle + " cannot have " + toParentNode
                                  + " as a parent",
                                  null);
    }
    // check for nested parentage problems
    if (isChildOf(fromNode, toParentNode)) {
      throw new NodeMoveException(toParentNode + " is a child of " + fromNode, null);
    }
    // check for number count problems
    if (!permitsChildCreation(toParentNode.getId(), toNodeGroupStyle)) {
      throw new NodeMoveException(toParentNode + " doesn't permit creation of another page of type "
                                  + fromNodeStyle,
                                  null);
    }
    try (Transaction t = getNodeManager().getDb()
                                         .createTransaction(Db.DEFAULT_TIMEOUT,
                                                            Transaction.TRANSACTION_UPDATE,
                                                            getTransactionPassword(),
                                                            "moveNode")) {
      WritableRecord rwFromNode = lockNodeTree(t, fromNode);
      rwFromNode.setField(Node.PARENTNODE_ID, Integer.valueOf(toParentNode.getId()));
      if (!Strings.isNullOrEmpty(proposedFilename)) {
        rwFromNode.setField(Node.FILENAME, proposedFilename);
      }
      final RegionStyle tw_RegionStyle = getNodeManager().getRegionStyle();
      for (PersistentRecord regionStyle : fromNodeStyle.opt(NodeStyle.REGIONSTYLES, t)) {
        ContentCompiler contentCompiler = tw_RegionStyle.getContentCompiler(regionStyle);
        if (!(contentCompiler instanceof MoveableContentCompiler)) {
          throw new NodeMoveException(contentCompiler + " via " + regionStyle + " on " + fromNode
                                      + " is not a MoveableContentCompiler",
                                      null);
        }
        ((MoveableContentCompiler) contentCompiler).moveContentCompiler(t,
                                                                        rwFromNode,
                                                                        regionStyle,
                                                                        fromNodeParent,
                                                                        toParentNode);
      }
      for (PersistentRecord fromChildNode : rwFromNode.opt(Node.UNSORTEDCHILDRENNODES, t)) {
        if (!(fromChildNode instanceof WritableRecord)) {
          throw new NodeMoveException(fromChildNode + " is not in a locked state", null);
        }
        moveNode(t, rwFromNode, fromNodeParent, toParentNode, (WritableRecord) fromChildNode);
      }
      t.commit();
    } catch (MirrorException mEx) {
      throw new NodeMoveException("Move failed", mEx);
    }
    try {
      getNodeManager().getMovedNode().addMovedNode(fromNodeParent, fromNodeFilename, fromNode);
    } catch (MirrorException e) {
      e.printStackTrace();
    }
    return fromNode;
  }

  private void moveNode(Transaction transaction,
                        WritableRecord fromNode,
                        PersistentRecord fromParentNode,
                        PersistentRecord toParentNode,
                        WritableRecord fromChildNode)
      throws NodeMoveException
  {
    PersistentRecord fromChildNodeStyle = fromChildNode.comp(Node.NODESTYLE);
    final RegionStyle tw_RegionStyle = getNodeManager().getRegionStyle();
    for (PersistentRecord regionStyle : fromChildNodeStyle.opt(NodeStyle.REGIONSTYLES,
                                                               transaction)) {
      ContentCompiler contentCompiler = tw_RegionStyle.getContentCompiler(regionStyle);
      if (!(contentCompiler instanceof MoveableContentCompiler)) {
        throw new NodeMoveException(contentCompiler + " via " + regionStyle + " on " + fromChildNode
                                    + " is not a MoveableContentCompiler",
                                    null);
      }
      ((MoveableContentCompiler) contentCompiler).moveChildContentCompiler(transaction,
                                                                           fromNode,
                                                                           fromParentNode,
                                                                           toParentNode,
                                                                           fromChildNode,
                                                                           regionStyle);
    }
    for (PersistentRecord fromChildChildNode : fromChildNode.opt(Node.UNSORTEDCHILDRENNODES,
                                                                 transaction)) {
      if (!(fromChildChildNode instanceof WritableRecord)) {
        throw new NodeMoveException(fromChildChildNode + " is not in a locked state", null);
      }
      moveNode(transaction,
               fromNode,
               fromParentNode,
               toParentNode,
               (WritableRecord) fromChildChildNode);
    }
  }

  /**
   * Lock the specified node and all of the corresponding sub-tree under the given Transaction.
   * 
   * @param transaction
   * @param node
   * @throws MirrorTransactionTimeoutException
   */
  protected WritableRecord lockNodeTree(Transaction transaction,
                                        PersistentRecord node)
      throws MirrorTransactionTimeoutException
  {
    WritableRecord rwNode = transaction.edit(node, 100);
    for (PersistentRecord childNode : rwNode.opt(Node.UNSORTEDCHILDRENNODES, transaction)) {
      lockNodeTree(transaction, childNode);
    }
    return rwNode;
  }

  /**
   * Sweep across the sub-tree with fromNode at its head and validate that it is moveable. This
   * consists of ensuring that all ContentCompilers registered in the tree are
   * MoveableContentCompilers.
   * 
   * @see MoveableContentCompiler
   */
  public void assertIsMoveable(PersistentRecord fromNode)
      throws NodeMoveException
  {
    for (PersistentRecord regionStyle : fromNode.opt(Node.NODESTYLE).opt(NodeStyle.REGIONSTYLES)) {
      ContentCompiler contentCompiler =
          getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
      if (!(contentCompiler instanceof MoveableContentCompiler)) {
        throw new NodeMoveException(contentCompiler + " is registered on " + fromNode.opt(Node.PATH)
                                    + " and it is not a MoveableContentCompiler",
                                    null);
      }
    }
    for (PersistentRecord childNode : fromNode.opt(Node.UNSORTEDCHILDRENNODES)) {
      assertIsMoveable(childNode);
    }
  }

  /**
   * validate that the given record is in fact a Node and that it is of the required style.
   * 
   * @throws IllegalArgumentException
   *           if the record is not a node.
   */
  public void assertIsNodeOfNodeStyle(TransientRecord node,
                                      Integer nodeStyleId)
  {
    TransientRecord.assertIsTableNamed(node, TABLENAME);
    if (!node.opt(NODESTYLE_ID).equals(nodeStyleId)) {
      throw new IllegalArgumentException(String.format("%s is not of NodeStyle #%s: %s",
                                                       node,
                                                       nodeStyleId,
                                                       getNodeManager().getNodeStyle()
                                                                       .getTable()
                                                                       .getOptRecord(nodeStyleId)));
    }
  }

  /**
   * Update the values within tNode and its associated RegionStyles with the values contained in
   * params.
   */
  public boolean updateNode(TransientRecord tNode,
                            Transaction transaction,
                            Gettable inputs,
                            Map<Object, Object> outputs)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    if (tNode instanceof PersistentRecord) {
      if (tNode instanceof WritableRecord) {
        if (!transaction.contains((PersistentRecord) tNode)) {
          tNode = transaction.edit((PersistentRecord) tNode);
        }
      } else {
        tNode = transaction.edit((PersistentRecord) tNode);
      }
    }
    tNode.setFieldIfDefined(FILENAME, inputs, FILENAME);
    tNode.setFieldIfDefined(HTMLTITLE, inputs, HTMLTITLE);
    tNode.setFieldIfDefined(TITLE, inputs, TITLE);
    tNode.setFieldIfDefined(OWNER_ID, inputs, OWNER_ID);
    tNode.setFieldIfDefined(NUMBER, inputs, NUMBER);
    tNode.setFieldIfDefined(DESCRIPTION, inputs, DESCRIPTION);
    tNode.setFieldIfDefined(ISNOINDEX, inputs, ISNOINDEX);
    tNode.setFieldIfDefined(ISNOFOLLOW, inputs, ISNOFOLLOW);
    tNode.setFieldIfDefined(ISSEARCHABLE, inputs, ISSEARCHABLE);
    tNode.setFieldIfDefined(ISSEARCHABLE, inputs, ISSEARCHABLE);
    Boolean isPublished = inputs.getBoolean(ISPUBLISHED);
    if (isPublished != null) {
      tNode.setField(ISPUBLISHED, isPublished);
    }
    boolean isUpdated = tNode.getIsUpdated();
    isUpdated |= getNodeManager().invokeNodeUpdaters(tNode, inputs);
    if (tNode instanceof PersistentRecord) {
      isUpdated |=
          updateContentCompilers((PersistentRecord) tNode, null, transaction, inputs, outputs);
    }
    if (isUpdated) {
      tNode.setField(MODIFICATIONTIME, new Date());
    }
    return isUpdated;
  }

  public boolean updateContentCompilers(PersistentRecord node,
                                        PersistentRecord user,
                                        Transaction transaction,
                                        Gettable inputs,
                                        Map<Object, Object> outputs)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    boolean isUpdated = false;
    PersistentRecord nodeStyle = node.comp(NODESTYLE);
    RegionStyle regionStyle = getNodeManager().getRegionStyle();
    for (PersistentRecord rs : nodeStyle.opt(NodeStyle.REGIONSTYLES, transaction)) {
      if (user == null || regionStyle.isEditableBy(rs, user.getId())) {
        ContentCompiler cc = regionStyle.getContentCompiler(rs);
        Gettable namespacedParams = regionStyle.getNamespaceGettable(rs, inputs);
        isUpdated =
            isUpdated | cc.updateInstance(node, rs, transaction, namespacedParams, outputs, user);
      }
    }
    return isUpdated;
  }

  public final static RecordOperationKey<String> SHORTTITLE =
      RecordOperationKey.create("shortTitle", String.class);

  /**
   * Generate a fixed length (16 characters) version of the Node.TITLE (or Node.HTMLTITLE if
   * undefined) of a Node. This may then be used in pull-down menu's and the like to provide a
   * 'guaranteed' method of gettting things to not mess up your formatting. This should propably be
   * made parametric at some point in the future...
   */
  public static class ShortTitle
    extends SimpleRecordOperation<String>
  {
    public ShortTitle()
    {
      super(Node.SHORTTITLE);
    }

    @Override
    public String getOperation(TransientRecord node,
                               Viewpoint viewpoint)
    {
      String title = node.opt(TITLE);
      if (title.length() == 0) {
        title = node.opt(HTMLTITLE);
      } else if (title.length() > 16) {
        title = title.substring(0, 15) + "...";
      }
      return title;
    }
  }

  public boolean isChildOf(PersistentRecord parentNode,
                           PersistentRecord childNode)
  {
    if (!parentNode.mayFollowLink(PARENTNODE)) {
      return true;
    }
    while (childNode.mayFollowLink(PARENTNODE)) {
      if (parentNode.getId() == childNode.getId()) {
        return true;
      }
      childNode = childNode.comp(PARENTNODE);
    }
    return false;
  }

  /**
   * Check to see whether a given node is permitted to be deleted by a given user. This will check
   * whether the user passes the delete ACL for the Node, and whether deleting the Node would take
   * it below the minimum permitted cound as defined in its NodeGroupStyle.
   * 
   * @param node
   *          The node that you are trying to delete.
   * @param user
   *          The user that is requesting the deletion
   */
  public boolean isDeletable(PersistentRecord node,
                             PersistentRecord user,
                             boolean hasAuthDebugging,
                             String authDebuggingView)
  {
    TransientRecord.assertIsTable(node, this);
    TransientRecord.assertIsTable(user, getNodeManager().getUser());
    boolean isDeletableAuth = getNodeManager().getAcl()
                                              .acceptsAclUserOwner(node.compInt(DELETEACL_ID),
                                                                   user.getId(),
                                                                   node.compInt(OWNER_ID));
    if (hasAuthDebugging) {
      DebugConstants.auth(node, authDebuggingView, node.comp(DELETEACL), isDeletableAuth);
    }
    if (!isDeletableAuth) {
      return false;
    }
    PersistentRecord nodeGroupStyle = node.comp(NODEGROUPSTYLE);
    int minimum = nodeGroupStyle.compInt(NodeGroupStyle.MINIMUM);
    int currentCount = getChildNodesByNodeGroupStyle(node.compInt(PARENTNODE_ID),
                                                     nodeGroupStyle.getId()).getIdList().size();
    if (currentCount > minimum) {
      Optional<Boolean> isDeletablePredicate = _isDeletable.apply(node, user);
      if (isDeletablePredicate.isPresent()) {
        if (hasAuthDebugging) {
          DebugConstants.auth(node, DeleteNodeFactory.NAME, _isDeletable, isDeletablePredicate);
        }
        return isDeletablePredicate.get().booleanValue();
      }
      return true;
    }
    if (hasAuthDebugging) {
      DebugConstants.auth(node, DeleteNodeFactory.NAME, "minimum count", false);
    }
    return false;
  }

  private NodeUserPredicate _isDeletable = NodeUserPredicate.ABSENT;

  /**
   * Define the NodeUserPredicate that will be used to validate whether a Node is deletable. This
   * will only get invoked if the ACL and children count restrictions have been passed - ie it
   * provides additional levels of security, but it cannot bypass the existing security.
   * 
   * @param isDeletable
   *          The compulsory NodeUserPredicate that will be invoked by
   *          {@link #isDeletable(PersistentRecord, PersistentRecord, boolean, String)} if all else
   *          passes.
   */
  public void setIsDeletable(NodeUserPredicate isDeletable)
  {
    _isDeletable = Preconditions.checkNotNull(isDeletable);
  }

  private NodeUserPredicate _isEditable = NodeUserPredicate.ABSENT;

  /**
   * Define the NodeUserPredicate that will be used to validate whether a Node is deletable. This
   * will only get invoked if the ACL and children count restrictions have been passed - ie it
   * provides additional levels of security, but it cannot bypass the existing security.
   * 
   * @param isEditable
   *          The compulsory NodeUserPredicate that will be invoked by
   *          {@link #isDeletable(PersistentRecord, PersistentRecord, boolean, String)} if all else
   *          passes.
   */
  public void setIsEditable(NodeUserPredicate isEditable)
  {
    _isEditable = Preconditions.checkNotNull(isEditable);
  }

  public boolean isEditable(PersistentRecord node,
                            PersistentRecord user,
                            boolean hasAuthDebugging)
  {
    TransientRecord.assertIsTable(node, this);
    TransientRecord.assertIsTable(user, getNodeManager().getUser());
    boolean isEditableAcl = getNodeManager().getAcl()
                                            .acceptsAclUserOwner(node.compInt(EDITACL_ID),
                                                                 user.getId(),
                                                                 node.compInt(OWNER_ID));
    if (hasAuthDebugging) {
      DebugConstants.auth(node, EditFactory.NAME, node.comp(EDITACL), isEditableAcl);
    }
    if (!isEditableAcl) {
      return false;
    }
    Optional<Boolean> isEditablePredicate = _isEditable.apply(node, user);
    if (isEditablePredicate.isPresent()) {
      if (hasAuthDebugging) {
        DebugConstants.auth(node, EditFactory.NAME, _isEditable, isEditablePredicate);
      }
      return isEditablePredicate.get().booleanValue();
    }
    return isEditableAcl;
  }

  /**
   * If proposedName is non-unique, then append -1, -2 etc. to the name until it is unique, and then
   * return the unique name.
   */
  public String getUniqueChildName(PersistentRecord parentNode,
                                   String proposedName)
  {
    String result = proposedName;
    try {
      getChildNodeByFilename(parentNode, result, null);
      int i = 1;
      while (true) {
        result = String.format("%s-%d", proposedName, Integer.toString(i));
        getChildNodeByFilename(parentNode, result, null);
      }
    } catch (MirrorNoSuchRecordException ex) {
      // Ignore
    }
    return result;
  }

  /**
   * Get the Root Node (ie /) of the current system.
   * 
   * @see #ROOTNODE_ID
   */
  public PersistentRecord getRootNode(Viewpoint viewpoint)
  {
    return getTable().getCompRecord(ROOTNODE_ID, viewpoint);
  }

  /**
   * Check to see whether the given node and all of its parents is viewable by the given user id and
   * throw an AuthenticationException if this is not the case. The checks take place from the root
   * node downwards so the error will correspond to the highest level node that you don't have
   * permission to view.
   * 
   * @param node
   *          The compulsory node to check for
   * @param userId
   *          The compulsory userId to check against
   * @throws AuthenticationException
   *           if the node or any of its parents are not viewable by userId
   * @see ReadViewFactory#isAllowed(NodeManager, PersistentRecord, int)
   */
  public void assertIsViewable(PersistentRecord node,
                               int userId)
      throws AuthenticationException
  {
    PersistentRecord parent = node.opt(Node.PARENTNODE);
    if (parent != null) {
      assertIsViewable(parent, userId);
    }
    if (!ReadViewFactory.isAllowed(getNodeManager(), node, userId)) {
      throw new AuthenticationException(String.format("You do not have permission to view %s",
                                                      node.opt(Node.PATH)),
                                        node,
                                        null);
    }
  }

  /**
   * Is the given Node and all of its parents viewable by the given user id?
   */
  public boolean isViewable(PersistentRecord node,
                            int userId)
  {
    PersistentRecord parent = node.opt(Node.PARENTNODE);
    if (parent != null) {
      if (!isViewable(parent, userId)) {
        return false;
      }
    }
    return ReadViewFactory.isAllowed(getNodeManager(), node, userId);
  }

  public int getGeneration(TransientRecord node)
  {
    PersistentRecord parentNode = node.opt(Node.PARENTNODE);
    return parentNode == null ? 0 : 1 + getGeneration(parentNode);
  }
}
