package org.cord.node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.NodeUserManager.SessionTransaction;
import org.cord.node.NodeUserManager.UserSession;

/**
 * The ContestedLockFilterManager is responsible for the registration and application of the
 * ContestedlockFilter that may be registered on a Node system.
 * 
 * @see ContestedLockFilter
 */
public class ContestedLockFilterManager
{
  private List<ContestedLockFilter> _contestedLockFilters = new LinkedList<ContestedLockFilter>();

  private boolean _undecidedState;

  private NodeManager _nodeManager;

  /**
   * Value (false) of undecidedState that is taken as default when none is specified.
   */
  public final static boolean DEFAULT_UNDECIDEDSTATE = false;

  public ContestedLockFilterManager(NodeManager nodeManager,
                                    boolean undecidedState)
  {
    _nodeManager = nodeManager;
    _undecidedState = undecidedState;
  }

  public void shutdown()
  {
    _contestedLockFilters.clear();
    _contestedLockFilters = null;
    _nodeManager = null;
  }

  /**
   * @see #DEFAULT_UNDECIDEDSTATE
   */
  public ContestedLockFilterManager(NodeManager nodeManager)
  {
    this(nodeManager,
         DEFAULT_UNDECIDEDSTATE);
  }

  /**
   * Register a contestedLockFilter in the manager. The filter will be appended to the end of the
   * list of filters so will be called last.
   * 
   * @param contestedLockFilter
   *          The filter to register. If null, then no action is taken. No checks are performed to
   *          see if the filter is already registered.
   */
  public void register(ContestedLockFilter contestedLockFilter)
  {
    if (contestedLockFilter != null) {
      _contestedLockFilters.add(contestedLockFilter);
    }
  }

  /**
   * Query the registered ContestedLockFilters (if any) as to whether or not a NodeRequest may steal
   * the lock from an already locked Node. This is done by traversing the registered filters from
   * initially registered to most recently registered and invoking the resolveContestedLock(...)
   * method on each one. If the filter returns a definite result (MAINTAIN_LOCK or STEAL_LOCK) then
   * a result is returned, but otherwise (UNDECIDED) the next filter is queried. If all filters are
   * undecided then the undecidedState initialisation variable is returned. Please note that no
   * actions towards stealing the lock or otherwise are actually performed.
   * 
   * @param contestedRequest
   *          The NodeRequest that is being made by the requesting User. The Node that is being
   *          contested will be the target leaf node of the request.
   * @param lockedUser
   *          The User that currently has the lock on the contested node.
   * @param lockedTransaction
   *          The Transaction that relates is owned by the lockedUser.
   * @return lockedTransaction if it is OK to steal the Transaction and null if it is not.
   */
  public boolean isStealableLock(NodeRequest contestedRequest,
                                 PersistentRecord contestedRecord,
                                 PersistentRecord lockedUser,
                                 Transaction lockedTransaction)
  {
    Iterator<ContestedLockFilter> filters = _contestedLockFilters.iterator();
    while (filters.hasNext()) {
      ContestedLockFilter filter = filters.next();
      switch (filter.resolveContestedLock(contestedRequest,
                                          contestedRecord,
                                          lockedUser,
                                          lockedTransaction)) {
        case ContestedLockFilter.MAINTAIN_LOCK:
          return false;
        case ContestedLockFilter.STEAL_LOCK:
          return true;
      }
    }
    return _undecidedState;
  }

  /**
   * Deduce who currently holds the lock on a given contestedRecord and then query the
   * ContestedLockFilters to see if it can be stolen from them. The current owner of the lock is
   * found be traversing the tree of Sessions and Transactions that is maintained by the
   * NodeUserManager looking for someone who has the lock. If there is no User found with a lock on
   * the record, then it is assumed that the Transaction must be owned by a system level process
   * which is outside the scope of the User level management and therefore the contestedRequest is
   * not given the rights to steal the lock from the record (system level processes need to know
   * that they will not get interrupted, whereas user level processes should always be in threat of
   * termination).
   * 
   * @param contestedRequest
   *          The NodeRequest that is being made by the requesting User. The Node that is being
   *          contested will be the target leaf node of the request.
   * @param node
   *          The Node that we are interested in stealing the lock of.
   * @return If the Transaction is stealable by the requested User then return the
   *         SessionTransaction that is associated with the lock, otherwise return null.
   * @see NodeUserManager
   */
  public NodeUserManager.SessionTransaction getStealableSessionTransaction(NodeRequest contestedRequest,
                                                                           PersistentRecord node)
  {
    int nodeId = node.getId();
    Iterator<UserSession> userSessions = _nodeManager.getUserManager().getUserSessions();
    while (userSessions.hasNext()) {
      UserSession userSession = userSessions.next();
      Iterator<SessionTransaction> sessionTransactions = userSession.getSessionTransactions();
      while (sessionTransactions.hasNext()) {
        SessionTransaction sessionTransaction = sessionTransactions.next();
        Transaction transaction = sessionTransaction.getTransactionByNode(nodeId);
        if (transaction != null) {
          return (isStealableLock(contestedRequest, node, userSession.getUser(), transaction)
              ? sessionTransaction
              : null);
        }
      }
    }
    // if we haven't found the lock by now then it must be some kind
    // of odd automated process and not belonging to a User. As such
    // we don't let people steal it on the grounds that this might
    // become very unpredictable...
    return null;
  }
}
