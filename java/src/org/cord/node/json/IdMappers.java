package org.cord.node.json;

import java.util.Map;

import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class IdMappers
{
  private IdMappers()
  {
  }

  private final static String KEY = new Object().toString();

  /**
   * @see #getIdMapper(Gettable)
   */
  public static void putIdMapper(Settable settable,
                                 IdMapper idMapper)
      throws SettableException
  {
    Preconditions.checkNotNull(idMapper, "idMapper");
    settable.set(KEY, idMapper);
  }

  /**
   * Get the IdMapper that is contained within the deepest wrapped Gettable pointed to by the
   * private key as added by {@link #set(Settable, IdMapper)}
   * 
   * @return Either the contained IdMapper or {@link EmptyIdMapper} if no contained instance is
   *         found. Never null
   * @see #putIdMapper(Settable, IdMapper)
   */
  public static IdMapper getIdMapper(Gettable gettable)
  {
    gettable = GettableUtils.getWrappedGettable(gettable);
    return MoreObjects.firstNonNull((IdMapper) gettable.get(KEY), EmptyIdMapper.getInstance());
  }

  /**
   * Utility method to facilitate filtering an id through the IdMapper. The IdMapper is resolved
   * from the gettable, and then examined to see if it has a mapping for the from Object. If there
   * is a mapping then the mapped Integer is returned, otherwise the from object is returned.
   * 
   * @return the mapped Integer id if the mapping is defined, otherwise the original from Object
   *         which might be null.
   */
  public static Object map(Gettable gettable,
                           String tablename,
                           Object from)
  {
    Integer to = getIdMapper(gettable).getMapping(tablename, from);
    return to == null ? from : to;
  }

  // IdMappers.map(gettable, tablename, obj)

  /**
   * put the given IdMapper into the given settable under the appropriate private key for it to be
   * found by {@link #getIdMapper(Gettable)}
   */
  public static void set(Settable settable,
                         IdMapper idMapper)
      throws SettableException
  {
    settable.set(KEY, idMapper);
  }

  public static void put(Map<Object, Object> map,
                         IdMapper idMapper)
  {
    map.put(KEY, idMapper);
  }
}
