package org.cord.node.json;

public interface IdMapper
{
  /**
   * Apply the mapping if defined otherwise just return the original id.
   * 
   * @param from
   *          The optional id to be transformed from
   * @return the appropriately transformed id, or the original id if no transformation is found, or
   *         null if from was null.
   */
  public Integer map(String tableName,
                     int from);

  /**
   * Get the mapped id or null if there is no mapping defined
   */
  public Integer getMapping(String tableName,
                            Object from);

  public Integer getMapping(String tableName,
                            int from);

  /**
   * Add a new mapping to this IdMapper
   */
  public void putMapping(String table,
                         int from,
                         int to)
      throws UnsupportedOperationException;

  /**
   * Inform the IdMapper that you have started to work out the transformation of a mapping and that
   * you will inform it of the mapping at some point in the future.
   */
  public void initMapping(String table,
                          int from);

  /**
   * Has this IdMapper got a mapping that is either in the process of being built or which has been
   * built?
   */
  public boolean hasCommenced(String table,
                              int from);

}
