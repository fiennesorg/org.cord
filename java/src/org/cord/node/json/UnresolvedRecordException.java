package org.cord.node.json;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordReference;
import org.cord.mirror.TransientRecord;
import org.cord.node.Node;
import org.cord.node.RegionStyle;

import com.google.common.base.Preconditions;

/**
 * Exception to signify that it hasn't been possible to load the JSON for a particular Node because
 * it contains a reference to a Record that has yet to be loaded.
 * 
 * @author alex
 */
public class UnresolvedRecordException
  extends Exception
{
  private static final long serialVersionUID = 1L;
  private final RecordReference __recordReference;
  private final PersistentRecord __containingNode;
  private final PersistentRecord __regionStyle;

  public UnresolvedRecordException(PersistentRecord containingNode,
                                   PersistentRecord regionStyle,
                                   RecordReference recordReference,
                                   Throwable cause)
  {
    super("Cannot resolve " + recordReference + " on " + containingNode.opt(Node.PATH),
          cause);
    __containingNode = TransientRecord.assertIsTableNamed(containingNode, Node.TABLENAME);
    __regionStyle = TransientRecord.assertIsTableNamed(regionStyle, RegionStyle.TABLENAME);
    __recordReference = Preconditions.checkNotNull(recordReference, "recordReference");
  }

  public final RecordReference getRecordReference()
  {
    return __recordReference;
  }

  public final PersistentRecord getContainingNode()
  {
    return __containingNode;
  }

  public final PersistentRecord getRegionStyle()
  {
    return __regionStyle;
  }
}
