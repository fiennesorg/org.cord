package org.cord.node.json;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordReference;
import org.cord.mirror.TableWrapper;
import org.cord.node.FeedbackErrorException;
import org.cord.util.JSONUtils;
import org.cord.util.SettableException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.WritableJSON;
import org.json.WritableJSONArray;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

/**
 * To restore saved Node data from a JSONObject into a (potentially populated) Node structure.
 * 
 * @author alex
 */
public class JSONLoader
  extends JSONComponent
{
  private final IdMapper __idMapper;
  private final Set<RecordReference> __problemRecords = new HashSet<RecordReference>();

  protected JSONLoader(JSONMgr jMgr,
                       File dir)
  {
    super(jMgr,
          dir);
    __idMapper = new MutableIdMapper();
  }

  public IdMapper getIdMapper()
  {
    return __idMapper;
  }

  /**
   * Add a given Record to the list of problems that this loader has tried to process.
   */
  public boolean addProblemRecord(PersistentRecord record)
  {
    return __problemRecords.add(record.toRecordReference());
  }

  /**
   * Get an unmodifiable Set of the RecordReferences that have been tagged as problem records in the
   * context of this JSONLoader.
   */
  public Set<RecordReference> getProblemRecords()
  {
    return Collections.unmodifiableSet(__problemRecords);
  }

  /**
   * Sweep through the current list of exception records and try and load each one in turn.
   * 
   * @return an umodifiable Map of RecordReferences to Exceptions that have occurred during the
   *         loading of the problematic records. If there have been no further problems (or if there
   *         were no problematic records) then this Map will be empty.
   */
  public Map<RecordReference, Exception> processProblemRecords()
  {
    if (__problemRecords.size() == 0) {
      return Collections.emptyMap();
    }
    Map<RecordReference, Exception> exceptions = Maps.newHashMap();
    for (Iterator<RecordReference> i = __problemRecords.iterator(); i.hasNext();) {
      RecordReference ref = i.next();
      try {
        loadRecord(ref.getTable(), ref.getId(), true);
        i.remove();
      } catch (Exception ex) {
        ex.printStackTrace();
        exceptions.put(ref, ex);
      }
    }
    return Collections.unmodifiableMap(exceptions);
  }

  public PersistentRecord loadRecord(TableWrapper tableWrapper,
                                     int savedId)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, JSONException, SettableException,
      FeedbackErrorException
  {
    return loadRecord(tableWrapper, savedId, false);
  }

  /**
   * @return The appropriate deserialised record or null if there is no TableHandler for the given
   *         tableWrapper, savedId was zero, there is already an ongoing process to load this record
   */
  public PersistentRecord loadRecord(TableWrapper tableWrapper,
                                     int savedId,
                                     boolean isReload)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, JSONException, SettableException,
      FeedbackErrorException
  {
    Preconditions.checkNotNull(tableWrapper, "tableWrapper");
    if (savedId == 0) {
      return null;
    }
    // Integer liveId = getIdMapper().getMapping(tableWrapper.getTableName(),
    // savedId);
    if (getIdMapper().hasCommenced(tableWrapper.getTableName(), savedId) && !isReload) {
      return null;
    }
    final TableHandler handler = getJMgr().getTableHandlers().get(tableWrapper.getTableName());
    if (handler == null) {
      return null;
    }
    getIdMapper().initMapping(tableWrapper.getTableName(), savedId);
    PersistentRecord record = handler.loadRecord(this, savedId);
    getIdMapper().putMapping(tableWrapper.getTableName(), savedId, record.getId());
    return record;
  }

  /**
   * Load all of the saved records for a given TableWrapper. This will abandon if:
   * <ul>
   * <li>There is not a registered {@link TableHandler} for the given TableWrapper</li>
   * <li>there is no saved ids File for the TableWrapper - ie there are no records to be loaded</li>
   * </ul>
   * If there is a saved ids file but it is not readable as a {@link JSONArray} then a
   * {@link JSONException} will be thrown. Assuming that we get this far then
   * {@link #loadRecord(TableWrapper, int)} will be invoked for each id in the saved file in the
   * order that they are defined in the file. Any exceptions thrown while loading a single record
   * will stop the process, but any records loaded before then will remain.
   */
  public void loadRecords(TableWrapper tableWrapper)
      throws JSONException, IOException, MirrorTransactionTimeoutException, MirrorFieldException,
      MirrorInstantiationException, MirrorNoSuchRecordException, SettableException,
      FeedbackErrorException
  {
    Preconditions.checkNotNull(tableWrapper, "tableWrapper");
    final TableHandler handler = getJMgr().getTableHandlers().get(tableWrapper.getTableName());
    if (handler == null) {
      return;
    }
    File idsFile = getIdsFile(tableWrapper);
    if (!idsFile.exists()) {
      return;
    }
    WritableJSONArray savedIds = JSONUtils.readJSONArray(idsFile, WritableJSON.get());
    for (int i = 0; i < savedIds.length(); i++) {
      loadRecord(tableWrapper, savedIds.getInt(i));
    }
  }

  /**
   * Work out the mapping from a saved id to a live id for a record or throw an
   * UnresolvedRecordException if the appropriate record has yet to be loaded. Please note that this
   * method will not try and load the record for you.
   */
  public Integer mapId(PersistentRecord node,
                       PersistentRecord regionStyle,
                       TableWrapper tableWrapper,
                       Integer savedId)
      throws UnresolvedRecordException
  {
    if (savedId == null || savedId.intValue() <= 0) {
      return null;
    }
    Integer liveId = getIdMapper().getMapping(tableWrapper.getTableName(), savedId);
    if (liveId == null) {
      throw new UnresolvedRecordException(node,
                                          regionStyle,
                                          RecordReference.getInstance(tableWrapper,
                                                                      savedId.intValue()),
                                          null);
    }
    return liveId;
  }

  public Integer loadId(TableWrapper tableWrapper,
                        Integer savedId)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, JSONException, SettableException,
      FeedbackErrorException
  {
    if (savedId == null) {
      return null;
    }
    Integer liveId = getIdMapper().getMapping(tableWrapper.getTableName(), savedId);
    if (liveId != null) {
      return liveId;
    }
    return Integer.valueOf(loadRecord(tableWrapper, savedId.intValue()).getId());
  }
}
