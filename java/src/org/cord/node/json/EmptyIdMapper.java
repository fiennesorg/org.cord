package org.cord.node.json;

public class EmptyIdMapper
  implements IdMapper
{
  private static EmptyIdMapper INSTANCE = new EmptyIdMapper();

  public static EmptyIdMapper getInstance()
  {
    return INSTANCE;
  }

  private EmptyIdMapper()
  {
  }

  @Override
  public void putMapping(String table,
                         int from,
                         int to)
  {
    throw new UnsupportedOperationException("EmptyIdMapper is not mutable");
  }

  @Override
  public Integer getMapping(String tableName,
                            Object from)
  {
    return null;
  }

  @Override
  public Integer getMapping(String tableName,
                            int from)
  {
    return null;
  }

  @Override
  public Integer map(String tableName,
                     int from)
  {
    return Integer.valueOf(from);
  }

  @Override
  public boolean hasCommenced(String table,
                              int from)
  {
    return false;
  }

  @Override
  public void initMapping(String table,
                          int from)
  {
    throw new UnsupportedOperationException("EmptyIdMapper is not mutable");
  }

}
