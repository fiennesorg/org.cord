package org.cord.node.json;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cord.node.NodeManager;
import org.cord.util.IoUtil;

import com.google.common.base.Preconditions;

public class JSONMgr
{
  private Map<String, TableHandler> __tableHandlers = new HashMap<String, TableHandler>();

  private final NodeManager __nodeMgr;

  public JSONMgr(NodeManager nodeMgr)
  {
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
  }

  public final NodeManager getNodeMgr()
  {
    return __nodeMgr;
  }

  /**
   * Register a TableHandler. If there is already a TableHandler registered for the Table then it
   * will be replaced.
   * 
   * @param tableHandler
   *          Not null
   * @return The previous TableHandler for the Table or null if this is the first TableHandler for
   *         the Table.
   */
  public TableHandler register(TableHandler tableHandler)
  {
    Preconditions.checkNotNull(tableHandler, "tableHandler");
    return __tableHandlers.put(tableHandler.getTableName(), tableHandler);
  }

  /**
   * Get a view of all the TableHandlers that have been registered.
   * 
   * @return Unmodifiable view of the Map of table name to TableHandler
   */
  public Map<String, TableHandler> getTableHandlers()
  {
    return Collections.unmodifiableMap(__tableHandlers);
  }

  public static final String SAVE_DIR_PARENT = "/dyn/save";

  private File getParentDir()
  {
    return new File(getNodeMgr().getDocBase(), SAVE_DIR_PARENT);
  }

  public JSONSaver createSaver(String dirName)
      throws IOException
  {
    File saveDir = new File(getParentDir(), dirName);
    if (saveDir.exists()) {
      IoUtil.deleteRecursively(saveDir);
    }
    saveDir.mkdirs();
    return new JSONSaver(this, saveDir);
  }

  public JSONLoader createLoader(String dirName)
  {
    return new JSONLoader(this, new File(getParentDir(), dirName));
  }
}
