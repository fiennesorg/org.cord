package org.cord.node.json;

import java.io.File;
import java.io.IOException;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.ContentCompiler;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeStyle;
import org.cord.node.RegionStyle;
import org.cord.util.JSONUtils;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.SettableMap;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSON;
import org.json.WritableJSONObject;

import com.google.common.base.Preconditions;

public class Handler_Node
  implements TableHandler
{
  private final String __pwd;

  public Handler_Node(String pwd)
  {
    __pwd = Preconditions.checkNotNull(pwd, "pwd");
  }

  /**
   * @see Node#TABLENAME
   */
  @Override
  public final String getTableName()
  {
    return Node.TABLENAME;
  }

  private void jsonToSettable(JSONObject jObj,
                              Settable settable,
                              FieldKey<?>... keys)
      throws SettableException, JSONException
  {
    for (FieldKey<?> key : keys) {
      settable.set(key, jObj.get(key.toString()));
    }
  }

  @Override
  public PersistentRecord loadRecord(JSONLoader jLoader,
                                     int savedNodeId)
      throws IOException, JSONException, SettableException, MirrorTransactionTimeoutException,
      MirrorFieldException, MirrorInstantiationException, MirrorNoSuchRecordException,
      FeedbackErrorException
  {
    final Node tw_Node = jLoader.getNodeMgr().getNode();
    final File jsonNodeFile = jLoader.getRecordFile(tw_Node, savedNodeId);
    if (jsonNodeFile == null) {
      return null;
    }
    try {
      final JSONObject jsonNode =
          JSONUtils.readJSONObject(jsonNodeFile, WritableJSON.get());
      PersistentRecord liveNode = null;
      Settable settable = new SettableMap();
      jsonToSettable(jsonNode,
                     settable,
                     Node.FILENAME,
                     Node.TITLE,
                     Node.HTMLTITLE,
                     Node.ISPUBLISHED,
                     Node.OWNER_ID,
                     Node.DESCRIPTION);
      // Settable settable =
      // JSONUtils.toSettableMap(jsonNode, RegionStyle.NAMESPACE_SEPARATOR);
      final IdMapper idMapper = jLoader.getIdMapper();
      IdMappers.set(settable, idMapper);
      int nodeGroupStyle_id = jsonNode.getInt(Node.NODEGROUPSTYLE_ID.getKeyName());
      if (jsonNode.getInt(Node.ID.getKeyName()) == 1) {
        liveNode = tw_Node.getTable().getRecord(1);
      } else {
        int savedParentNode_id = jsonNode.getInt(Node.PARENTNODE_ID.getKeyName());
        Integer liveParentNode_id = idMapper.getMapping(Node.TABLENAME, savedParentNode_id);
        if (liveParentNode_id == null) {
          throw new RuntimeException(String.format("Cannot resolve parentNode_id of %s for %s",
                                                   Integer.valueOf(savedParentNode_id),
                                                   jsonNode));
        }
        PersistentRecord liveParentNode = tw_Node.getTable().getRecord(liveParentNode_id);
        String filename = jsonNode.getString(Node.FILENAME.getKeyName());
        liveNode = tw_Node.getOptNodeByFilename(liveParentNode, filename, null);
        if (liveNode == null) {
          liveNode = tw_Node.createNode(liveParentNode_id.intValue(), nodeGroupStyle_id, settable);
        }
      }
      triggerJSONLoaders(jLoader, jsonNode, liveNode, settable);
      tw_Node.updateNode(liveNode, settable, null, __pwd);
      return liveNode;
    } catch (IOException e) {
      throw new IOException(String.format("Unable to load Node from %s", jsonNodeFile), e);
    }
  }

  private void triggerJSONLoaders(JSONLoader jLoader,
                                  JSONObject jNode,
                                  PersistentRecord node,
                                  Settable settable)
      throws SettableException, JSONException, MirrorTransactionTimeoutException,
      MirrorFieldException, MirrorInstantiationException, MirrorNoSuchRecordException, IOException,
      FeedbackErrorException
  {
    final RegionStyle tw_RegionStyle = jLoader.getNodeMgr().getRegionStyle();
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES)) {
      ContentCompiler contentCompiler = tw_RegionStyle.getContentCompiler(regionStyle);
      if (contentCompiler instanceof HandlesJSON) {
        JSONObject jCC = jNode.getJSONObject(regionStyle.opt(RegionStyle.NAME));
        try {
          ((HandlesJSON) contentCompiler).loadJSON(jLoader,
                                                   node,
                                                   regionStyle,
                                                   jCC,
                                                   tw_RegionStyle.getNamespaceSettable(regionStyle,
                                                                                       settable));
        } catch (UnresolvedRecordException e) {
          jLoader.addProblemRecord(node);
        }
      }
    }
  }

  @Override
  public JSONObject saveRecord(JSONSaver jSaver,
                               PersistentRecord node)
      throws JSONException, IOException
  {
    PersistentRecord.assertIsTableNamed(node, Node.TABLENAME);
    WritableJSONObject jObj = node.asJSON();
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    RegionStyle tw_RegionStyle = jSaver.getNodeMgr().getRegionStyle();
    for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES)) {
      ContentCompiler contentCompiler = tw_RegionStyle.getContentCompiler(regionStyle);
      if (contentCompiler instanceof HandlesJSON) {
        try {
          jObj.put(regionStyle.opt(RegionStyle.NAME),
                   ((HandlesJSON) contentCompiler).saveJSON(jSaver, node, regionStyle));
        } catch (MirrorNoSuchRecordException mnsrEx) {
          throw new MirrorLogicException("Cannot find ContentCompiler for " + node + "/"
                                         + regionStyle,
                                         mnsrEx);
        }
      }
    }
    JSONUtils.write(jSaver.getRecordFile(node), jObj, 2);
    for (PersistentRecord childNode : node.opt(Node.CHILDRENNODES)) {
      jSaver.save(childNode);
    }
    return jObj;
  }

}
