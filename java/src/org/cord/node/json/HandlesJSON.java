package org.cord.node.json;

import java.io.IOException;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Interface to show that a ContentCompiler is able to save and load its content to and from a
 * particular Node as a JSONObject.
 */
public interface HandlesJSON
{
  /**
   * Request the ContentCompiler to convert a single instance of itself into a JSONObject in a
   * format that can be readable by
   * {@link #loadJSON(JSONLoader, PersistentRecord, PersistentRecord, JSONObject, Settable)} . If
   * this necesitates the saving of any shared information outside the Node hierarchy then this
   * should be added to the jsonSaver as appropriate during the execution of this method.
   */
  public JSONObject saveJSON(JSONSaver jsonSaver,
                             PersistentRecord node,
                             PersistentRecord regionStyle)
      throws JSONException, MirrorNoSuchRecordException, IOException;

  /**
   * Request the ContentCompiler populate the given NamespaceSettable in such a manner that if fed
   * to updateInstance then it would restore the state.
   * 
   * @param jCC
   *          the JSONObject that contains the information pertaining to one instance of this
   *          ContentCompiler. This will have been produced by
   *          {@link #saveJSON(JSONSaver, PersistentRecord, PersistentRecord)} in the same class.
   * @param settable
   *          The appropriately namespaced Settable that information can be put directly into.
   * @throws UnresolvedRecordException
   *           If there is a reference to another record inside the serialised information in jObj
   *           that has yet to be loaded. If it isn't loaded yet then it is unknown what id the
   *           record will have when it is finally loaded and therefore it is not possible to load
   *           the link to the record. The invoking code will catch this exception and once all of
   *           the Nodes have been loaded as completely as possible then it will retry the
   *           problematic Nodes that previously threw UnresolvedRecordException and hopefully all
   *           of the pending resources will be available.
   * @throws JSONException
   *           if the information in jObj contravenes the logic defined in the saveJSON method for
   *           this HandlesJSON ContentCompiler. An effort should be made to make things as
   *           forgiving as possible, but if the data is nonsensical then it is OK to through a
   *           JSONException.
   */
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jCC,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException;
}
