package org.cord.node.json;

import java.io.File;
import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.util.JSONUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.WritableJSON;

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;

public class JSONSaver
  extends JSONComponent
{
  SetMultimap<String, Integer> __savedRecords = LinkedHashMultimap.create();

  protected JSONSaver(JSONMgr jMgr,
                      File dir)
  {
    super(jMgr,
          dir);
  }

  public void saveIds()
      throws IOException, JSONException
  {
    for (String tablename : __savedRecords.keySet()) {
      File file = getIdsFile(tablename);
      JSONArray jArr = WritableJSON.get().toJSONArray(__savedRecords.get(tablename));
      JSONUtils.write(file, jArr);
    }
  }

  /**
   * Has the given record been saved within the context of this JSONSaver?
   */
  public boolean hasSaved(PersistentRecord record)
  {
    Preconditions.checkNotNull(record, "record");
    return __savedRecords.containsEntry(record.getTable().getName(),
                                        Integer.valueOf(record.getId()));
  }

  /**
   * Inform this JSONSaver that the given record is to be saved as part of the operation. If there
   * is no {@link TableHandler} registered for the record then nothing is done. If the record has
   * either previously been saved or is in the process of being saved (thereby avoiding recursive
   * loops) then do nothing.
   * 
   * @param record
   *          The optional record that should be saved
   */
  public void save(final PersistentRecord record)
      throws JSONException, IOException
  {
    if (record == null) {
      return;
    }
    final TableHandler handler = getJMgr().getTableHandlers().get(record.getTableName());
    if (handler == null) {
      return;
    }
    if (hasSaved(record)) {
      return;
    }
    __savedRecords.put(record.getTableName(), Integer.valueOf(record.getId()));
    handler.saveRecord(this, record);
  }
}
