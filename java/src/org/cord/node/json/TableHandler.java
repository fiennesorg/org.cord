package org.cord.node.json;

import java.io.IOException;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.util.SettableException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A TableHandler describes an Object that is capable of saving and loading state of records in a
 * given Table to and from JSON.
 * 
 * @author alex
 */
public interface TableHandler
{
  public String getTableName();

  public JSONObject saveRecord(JSONSaver jSaver,
                               PersistentRecord record)
      throws JSONException, IOException;

  public PersistentRecord loadRecord(JSONLoader jLoader,
                                     int savedId)
      throws IOException, JSONException, SettableException, MirrorTransactionTimeoutException,
      MirrorFieldException, MirrorInstantiationException, MirrorNoSuchRecordException,
      FeedbackErrorException;
}
