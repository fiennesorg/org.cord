package org.cord.node.json;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.Table;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;

/**
 * Container to store the mappings between two sets of ids for an arbitrary number of Tables.
 * 
 * @author alex
 */
public class MutableIdMapper
  implements IdMapper
{
  private Map<String, Int2IntMap> __mappings = new HashMap<String, Int2IntMap>();

  @Override
  public Integer map(String tableName,
                     int from)
  {
    Integer to = getMapping(tableName, from);
    return to == null ? Integer.valueOf(from) : to;
  }

  @Override
  public Integer getMapping(String table,
                            Object from)
  {
    Int2IntMap mapping = __mappings.get(table);
    return mapping == null ? null : mapping.get(from);
  }

  @Override
  public Integer getMapping(String tableName,
                            int from)
  {
    Int2IntMap mapping = __mappings.get(tableName);
    return mapping == null ? null : Integer.valueOf(mapping.get(from));
  }

  private Int2IntMap createMap(String table)
  {
    Int2IntMap mapping = __mappings.get(table);
    if (mapping == null) {
      mapping = new Int2IntOpenHashMap();
      __mappings.put(table, mapping);
    }
    return mapping;
  }

  @Override
  public void putMapping(String table,
                         int from,
                         int to)
  {
    Preconditions.checkNotNull(table, "table");
    Int2IntMap mapping = createMap(table);
    if (mapping.containsKey(from)) {
      throw new IllegalStateException(String.format("%s.%s --> %s is not permitted as --> %s is already defined",
                                                    table,
                                                    Integer.valueOf(from),
                                                    Integer.valueOf(to),
                                                    Integer.valueOf(mapping.get(from))));
    }
    mapping.put(from, to);
  }

  @Override
  public void initMapping(String table,
                          int from)
  {
    Preconditions.checkNotNull(table, "table");
    Int2IntMap mapping = createMap(table);
    mapping.put(from, mapping.defaultReturnValue());
  }

  @Override
  public boolean hasCommenced(String table,
                              int from)
  {
    Int2IntMap map = __mappings.get(table);
    if (map == null) {
      return false;
    }
    return map.containsKey(from);
  }

  public Set<String> getTables()
  {
    return Collections.unmodifiableSet(__mappings.keySet());
  }

  public Map<Integer, Integer> getMappings(Table table)
  {
    Map<Integer, Integer> mappings = __mappings.get(table);
    return mappings == null ? null : Collections.unmodifiableMap(mappings);
  }

}
