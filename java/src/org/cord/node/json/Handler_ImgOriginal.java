package org.cord.node.json;

import java.io.File;
import java.io.IOException;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.img.ImgOriginal;
import org.cord.util.JSONUtils;
import org.cord.util.NotImplementedException;
import org.cord.util.SettableException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSON;
import org.json.WritableJSONObject;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;

public class Handler_ImgOriginal
  implements TableHandler
{
  private final NodeManager __nodeMgr;

  public Handler_ImgOriginal(NodeManager nodeMgr)
  {
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
  }

  public final NodeManager getNodeMgr()
  {
    return __nodeMgr;
  }

  @Override
  public final String getTableName()
  {
    return ImgOriginal.TABLENAME;
  }

  @Override
  public PersistentRecord loadRecord(JSONLoader jLoader,
                                     int savedId)
      throws IOException, JSONException, SettableException, MirrorTransactionTimeoutException,
      MirrorFieldException, MirrorInstantiationException, MirrorNoSuchRecordException
  {
    final ImgOriginal tw_ImgOriginal = getNodeMgr().getImgMgr().getImgOriginal();
    final File file = jLoader.getRecordFile(tw_ImgOriginal, savedId);
    if (file == null) {
      return null;
    }
    final JSONObject js_ImgOriginal =
        JSONUtils.readJSONObject(file, WritableJSON.get());
    System.err.println(js_ImgOriginal.toString(2));
    throw new NotImplementedException();
  }

  @Override
  public JSONObject saveRecord(JSONSaver jSaver,
                               PersistentRecord imgOriginal)
      throws JSONException, IOException
  {
    PersistentRecord.assertIsTableNamed(imgOriginal, ImgOriginal.TABLENAME);
    WritableJSONObject jObj = imgOriginal.asJSON();
    final File sourceFile = getNodeMgr().getImgMgr().getImgOriginal().getFile(imgOriginal);
    final File targetFile = jSaver.getAssetField(imgOriginal.opt(ImgOriginal.WEBPATH));
    Files.createParentDirs(targetFile);
    Files.copy(sourceFile, targetFile);
    JSONUtils.write(jSaver.getRecordFile(imgOriginal), jObj, 2);
    return jObj;
  }

}
