package org.cord.node;

import java.util.HashMap;
import java.util.Iterator;

import org.cord.image.ImageMetaDataCache;
import org.cord.image.ImageTable;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.node.cc.BinaryAssetContentCompiler;
import org.cord.node.cc.HtmlContentCompiler;
import org.cord.node.cc.SimpleTextContentCompiler;
import org.cord.node.cc.SingleImageContentCompiler;
import org.cord.node.cc.StaticTemplateContentCompiler;
import org.cord.util.DuplicateNamedException;
import org.cord.util.LogicException;
import org.cord.util.NoSuchNamedException;

/**
 * Container Class that holds the registered ContentCompiler engines that are available for use in
 * this context.
 */
public class ContentCompilerFactory
{
  public final static String NAME_NTML = "NtmlDocument";

  public final static String NAME_HTML = "Html";

  public final static String NAME_SINGLEIMAGE = "SingleImage";

  public final static String NAME_SIMPLETEXT = "SimpleText";

  public final static String NAME_STATICTEMPLATE = "StaticTemplate";

  public final static String NAME_BINARYASSET = "BinaryAsset";

  public final static String NAME_HTMLMETATAG = "HtmlMetaTag";

  private NodeManager _nodeManager;

  private final HashMap<String, ContentCompiler> __contentCompilers =
      new HashMap<String, ContentCompiler>();

  public ContentCompilerFactory(NodeManager nodeManager) throws MirrorTableLockedException
  {
    _nodeManager = nodeManager;
    // register(new HtmlMetaTag(nodeManager,
    // nodeManager.getTransactionPassword()));
  }

  public void shutdown()
  {
    Iterator<ContentCompiler> contentCompilers = __contentCompilers.values().iterator();
    while (contentCompilers.hasNext()) {
      contentCompilers.next().shutdown();
    }
    __contentCompilers.clear();
    _nodeManager = null;
  }

  private synchronized ContentCompiler registerDefaultContentCompiler(NodeManager nodeManager,
                                                                      String transactionPassword,
                                                                      String name)
  {
    if (__contentCompilers.get(name) != null) {
      return getContentCompiler(name);
    }
    try {
      if (NAME_HTML.equals(name)) {
        return register(new HtmlContentCompiler(nodeManager, transactionPassword));
      } else if (NAME_SINGLEIMAGE.equals(name)) {
        SingleImageContentCompiler iscc =
            new SingleImageContentCompiler(nodeManager, transactionPassword);
        ImageMetaDataCache imdc = new ImageMetaDataCache();
        ImageTable.setImageMetaDataFactory(imdc);
        nodeManager.getDb().addExpirable(imdc);
        return register(iscc);
      } else if (NAME_SIMPLETEXT.equals(name)) {
        return register(new SimpleTextContentCompiler(nodeManager, transactionPassword));
      } else if (NAME_STATICTEMPLATE.equals(name)) {
        return register(new StaticTemplateContentCompiler(nodeManager));
      } else if (NAME_BINARYASSET.equals(name)) {
        return register(new BinaryAssetContentCompiler(nodeManager, transactionPassword));
      }
    } catch (MirrorTableLockedException mtlEx) {
      throw new LogicException("unexpected locking during initialisation of core ContentCompiler",
                               mtlEx);
    }
    throw new NoSuchNamedException(name, "Cannot find definition of ContentCompiler");
  }

  // #### 2DO migrate away from NoSuchNamedException into a more
  // #### demanding non-RuntimeException
  /**
   * @return the ContentCompiler that is currently registered under the given name, never null.
   * @throws NoSuchNamedException
   *           If the given name is not already registered and cannot be mapped to one of the
   *           original core ContentCompilers.
   */
  public ContentCompiler getContentCompiler(String name)
      throws NoSuchNamedException
  {
    Object result = __contentCompilers.get(name);
    if (result == null) {
      return registerDefaultContentCompiler(_nodeManager,
                                            _nodeManager.getTransactionPassword(),
                                            name);
    } else {
      return (ContentCompiler) result;
    }
  }

  /**
   * @return The ContentCompiler that was finally registered. If there is not a ContentCompiler
   *         registered under the name of the given ContentCompiler then this will be the passed
   *         contentCompiler, otherwise it will be the already registered ContentCompiler.
   */
  public synchronized ContentCompiler register(ContentCompiler contentCompiler)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    if (contentCompiler == null) {
      return null;
    }
    if (__contentCompilers.containsKey(contentCompiler.getName())) {
      return getContentCompiler(contentCompiler.getName());
    }
    _nodeManager.getDb().add(contentCompiler.getDbInitialisers());
    __contentCompilers.put(contentCompiler.getName(), contentCompiler);
    return contentCompiler;
  }

  public Iterator<String> getNames()
  {
    return __contentCompilers.keySet().iterator();
  }

  public Iterator<ContentCompiler> getContentCompilers()
  {
    return __contentCompilers.values().iterator();
  }
}
