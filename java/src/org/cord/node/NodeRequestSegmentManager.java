package org.cord.node;

import javax.servlet.ServletException;

import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.LogicException;
import org.webmacro.Context;

import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * The class that is responsible for managing the system whereby NodeRequestSegmentFactory Objects
 * or views are allocated to incoming NodeRequest Objects.
 */
public class NodeRequestSegmentManager
{
  // nodeStyle (Integer) --> NodeRequestSegmentFactoryFactory
  // private final HashMap<Integer, NodeRequestSegmentFactoryFactory> __factories =
  // new HashMap<Integer, NodeRequestSegmentFactoryFactory>();
  private final Int2ObjectMap<NodeRequestSegmentFactoryFactory> __factories =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<NodeRequestSegmentFactoryFactory>());

  private NodeRequestSegmentFactoryFactory _defaultFactory;

  private final NodeManager __nodeManager;

  /**
   * Create a new NodeRequestSegmentManager with a default NodeRequestSegmentFactoryFactory set up
   * under the default nodeStyleId of 0.
   */
  public NodeRequestSegmentManager(NodeManager nodeManager)
  {
    __nodeManager = nodeManager;
    try {
      _defaultFactory = new NodeRequestSegmentFactoryFactory(__nodeManager);
      _defaultFactory.registerDefaults();
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("impossible", mnsrEx);
    }
    __factories.put(NODESTYLEID_GLOBAL, _defaultFactory);
  }

  public static final int NODESTYLEID_GLOBAL = 0;

  public NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  public void shutdown()
  {
    for (NodeRequestSegmentFactoryFactory factory : __factories.values()) {
      factory.shutdown();
    }
    __factories.clear();
    _defaultFactory = null;
  }

  /**
   * Register the given factory only on the given class of NodeStyle.
   * 
   * @throws MirrorNoSuchRecordException
   *           if it is not possible to find nodeStyleId.
   */
  public synchronized NodeRequestSegmentFactory register(int nodeStyleId,
                                                         int transactionType,
                                                         NodeRequestSegmentFactory factory)
      throws MirrorNoSuchRecordException
  {
    return register(nodeStyleId, transactionType, factory, false);
  }

  public NodeRequestSegmentFactory compRegister(int nodeStyleId,
                                                int transactionType,
                                                NodeRequestSegmentFactory factory)
  {
    try {
      return register(nodeStyleId, transactionType, factory);
    } catch (MirrorNoSuchRecordException e) {
      throw new MirrorLogicException(String.format("Unable to resolve %s into NodeStyle",
                                                   Integer.valueOf(nodeStyleId)),
                                     e);
    }
  }

  public synchronized NodeRequestSegmentFactory register(int nodeStyleId,
                                                         int transactionType,
                                                         NodeRequestSegmentFactory factory,
                                                         boolean isDefaultFactory)
      throws MirrorNoSuchRecordException
  {
    NodeRequestSegmentFactoryFactory factoryFactory = getFactoryFactory(nodeStyleId, false);
    if (factoryFactory == null) {
      factoryFactory = new NodeRequestSegmentFactoryFactory(getNodeManager(), nodeStyleId);
      __factories.put(nodeStyleId, factoryFactory);
    }
    return factoryFactory.register(transactionType, factory, isDefaultFactory);
  }

  /**
   * Register the given factory only on the given class of NodeStyle with errors made sensible and
   * wrapped in a ServletException.
   * 
   * @throws ServletException
   *           If there is an error when invoking the nested register(...). The original error will
   *           be ServletException.getCause().
   * @see #register(int,int,NodeRequestSegmentFactory)
   */
  public NodeRequestSegmentFactory servletFriendlyRegister(int nodeStyleId,
                                                           int transactionType,
                                                           NodeRequestSegmentFactory factory)
      throws ServletException
  {
    try {
      return register(nodeStyleId, transactionType, factory);
    } catch (MirrorNoSuchRecordException badNodeStyleIdEx) {
      NodeServlet.throwNestedServletException("Error resolving nodeStyleId " + nodeStyleId
                                              + " when registering " + factory,
                                              badNodeStyleIdEx);
    }
    // never reached but required to compile.
    return null;
  }

  public NodeRequestSegmentFactory register(int transactionType,
                                            NodeRequestSegmentFactory factory)
  {
    try {
      return register(0, transactionType, factory);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("impossible to get missing default nodeStyleId", mnsrEx);
    }
  }

  // #### this is ugle and the multi-registration of factories should
  // #### be moved down to a lower class where it belongs.
  public void register(NodeRequestSegmentFactory factory)
  {
    register(Transaction.TRANSACTION_NULL, factory);
    register(Transaction.TRANSACTION_UPDATE, factory);
    register(Transaction.TRANSACTION_DELETE, factory);
  }

  private NodeRequestSegmentFactoryFactory getFactoryFactory(int nodeStyleId,
                                                             boolean utiliseDefault)
  {
    NodeRequestSegmentFactoryFactory factory = __factories.get(nodeStyleId);
    return factory == null ? (utiliseDefault ? _defaultFactory : null) : factory;
  }

  /**
   * @return The nodeStyle specific FactoryFactory or the default fallback FactoryFactory. This
   *         should never return null.
   */
  public NodeRequestSegmentFactoryFactory getFactoryFactory(int nodeStyleId)
  {
    return getFactoryFactory(nodeStyleId, true);
  }

  /**
   * @return the appropriate factory from the nodeStyleId specific FactoryFactory or the fallback
   *         factory from the default supplier. If no-one opts to support the named view in the
   *         context of the given transactionType then null is returned.
   */
  public NodeRequestSegmentFactory getFactory(int nodeStyleId,
                                              int transactionType,
                                              String view)
  {
    NodeRequestSegmentFactory nodeStyleIdFactory =
        getFactoryFactory(nodeStyleId).getFactory(transactionType, view);
    if (nodeStyleIdFactory != null) {
      return nodeStyleIdFactory;
    }
    nodeStyleIdFactory = _defaultFactory.getFactory(transactionType, view);
    return nodeStyleIdFactory;
  }

  public NodeRequestSegmentFactory getFactoryWithoutDefault(int nodeStyleId,
                                                            int transactionType,
                                                            String view)
  {
    NodeRequestSegmentFactoryFactory nrsff = getFactoryFactory(nodeStyleId, false);
    if (nrsff == null) {
      return null;
    }
    return nrsff.getFactoryWithoutDefault(transactionType, view);
  }

  public String getDefaultView(int transactionType)
  {
    return _defaultFactory.getDefaultView(transactionType);
  }

  /**
   * Calculate the appropriate NodeRequestSegmentFactory to handle the incoming request and return
   * the NodeRequestSegment that the factory generates. If the factory throws a
   * RecordPreLockedException in the getInstance(...) method then the NodeRequestSegment that is
   * returned will wrap the appropriate preLockedTemplate.
   * 
   * @see RecordPreLockedException
   * @see NodeRequestSegmentFactory#doWork(NodeRequest,PersistentRecord,Context, boolean)
   * @see NodeManager#getPreLockedTemplatePath()
   */
  public NodeRequestSegment getNodeRequestSegment(NodeRequest nodeRequest,
                                                  PersistentRecord node,
                                                  Context context,
                                                  int transactionType,
                                                  String view,
                                                  boolean isSummaryRole)
      throws NodeRequestException
  {
    if (Strings.isNullOrEmpty(view)) {
      view = getDefaultView(transactionType);
    }
    NodeRequestSegmentFactory factory =
        getFactory(node.compInt(Node.NODESTYLE_ID), transactionType, view);
    if (factory == null) {
      throw new FeedbackErrorException(null,
                                       node,
                                       "You have requested a view (%s) that this engine does not understand",
                                       view);
    }
    try {
      return factory.doWork(nodeRequest, node, context, isSummaryRole);
    } catch (RecordPreLockedException recordPreLockedException) {
      context.put("recordPreLockedException", recordPreLockedException);
      return new NodeRequestSegment(getNodeManager(),
                                    nodeRequest,
                                    node,
                                    context,
                                    getNodeManager().getPreLockedTemplatePath());
    }
  }
}
