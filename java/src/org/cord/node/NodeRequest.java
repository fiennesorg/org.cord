package org.cord.node;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.view.ReadViewFactory;
import org.cord.node.view.user.Login;
import org.cord.util.Dates;
import org.cord.util.DebugConstants;
import org.cord.util.EnumerationWrapper;
import org.cord.util.Gettable;
import org.cord.util.GettableMapWrapper;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.IoUtil;
import org.cord.util.LogicException;
import org.cord.util.NamespacedNameCache;
import org.cord.util.NamespacedNameCache.Ordering;
import org.cord.util.NoSuchNamedException;
import org.cord.util.PaddedGettable;
import org.cord.util.ReverseListIterator;
import org.cord.util.SessionDomainExpander;
import org.cord.util.SimpleTimer;
import org.cord.util.StringUtils;
import org.cord.webmacro.VarsTool;
import org.webmacro.Context;
import org.webmacro.servlet.WebContext;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Closeables;
import com.oreilly.servlet.MultipartRequest;

/**
 * NodeRequest is a wrapper around all the resolved information that the Node system has generated
 * in response to a single external request. It is composed of multiple NodeRequestSegments
 * corresponding to the individual nodes in the tree that are referenced by the request.
 */
public class NodeRequest
  implements Gettable, Iterable<NodeRequestSegment>
{
  public final static String CGI_VAR_VIEW = "view";

  public final static int LINKMODE_BIT_SELECTED = 1;

  public final static int LINKMODE_BIT_ACTIVE = 2;

  public final static String CGI_BOOLEAN_ACTIVE = "-active";

  /**
   * parameter containing the PersistentRecord from the Node table for the last interpreted Node in
   * a 404 situation.
   */
  public final static String WEBMACRO_LASTLEGALNODE = "lastLegalNode";

  /**
   * parameter containing the filename that was not recognised when trying to parse an incoming
   * NodeRequest that generated a 404 situation.
   */
  public final static String WEBMACRO_BADFILENAME = "badFilename";

  public final static String WEBMACRO_GETTABLECONTEXT = "GettableContext";

  public final static String ISDEFINED_PREFIX = "isDefined-";

  public final static NamespacedNameCache ISDEFINED_NAMECACHE =
      new NamespacedNameCache(ISDEFINED_PREFIX, Ordering.PREFIXNAMESPACE);

  public final static NamespacedNameCache BOOLEAN_ACTIVE_NAMECACHE =
      new NamespacedNameCache(CGI_BOOLEAN_ACTIVE, Ordering.POSTFIXNAMESPACE);

  // list of NodeRequestSegment objects in the order of parsing
  // (top-down)
  private final List<NodeRequestSegment> __nodeRequestSegments =
      new ArrayList<NodeRequestSegment>();

  private final HttpMethod __httpMethod;

  // set via addNodeRequestSegmento
  private NodeRequestSegment _leafNodeRequestSegment = null;

  private PersistentRecord _targetLeafNode = null;

  // the current index into __nodeRequestSegments that notifies the
  // template system as to the current location when nesting templates.
  private int _currentPosition = 0;

  // the name of the next child in the sequence. Used for rapid
  // calculation of link status. Automatically recalculated when
  // updating _currentPosition.
  private String _nextChildName;

  private String _rootTemplatePath = null;

  private String _virtualPath;

  private final HttpServletRequest __request;

  private final HttpServletResponse __response;

  private final MultipartRequest __multipartRequest;

  // private final ParameterParser __parameterParser;
  private final File __uploadDirectory;

  private final HttpSession __session;

  private final NodeManager __nodeManager;

  private String _view;

  private String _viewNotes;

  private NodeUserManager.UserSession _userSession;

  private String _errorMessage = "";

  private String _errorTitle = "";

  private final NodeUserManager.SessionTransaction __sessionTransaction;

  private final NodeServlet __nodeServlet;

  private final boolean __isAdmin;

  private final WebContext __context;

  private final Gettable __gettableContext;

  private Map<String, Cookie> _pendingCookies = null;

  private int _recursiveReloadCount = 0;

  public final static int RECURSIVERELOAD_MAXIMUM = 10;

  private boolean _isMovedNode = false;

  private byte[] _postBody = null;

  /**
   * 1 gigabyte
   */
  public static int MAXIMUM_MULTIPART_SIZE = 1024 * 1024 * 1024;

  public NodeRequest(NodeManager nodeManager,
                     NodeServlet nodeServlet,
                     WebContext context)
      throws MirrorNoSuchRecordException, MirrorTransactionTimeoutException, NoSuchNamedException,
      IOException, NodeRequestException
  {
    __nodeManager = nodeManager;
    __nodeServlet = nodeServlet;
    Node nodes = nodeManager.getNode();
    __context = context;
    __gettableContext = buildGettableWrapper(context);
    __context.put(WEBMACRO_GETTABLECONTEXT, __gettableContext);
    __request = context.getRequest();
    __request.setCharacterEncoding("UTF-8");
    __httpMethod = Enum.valueOf(HttpMethod.class, __request.getMethod());
    __response = context.getResponse();
    SessionDomainExpander sessionDomainExpander = __nodeManager.getSessionDomainExpander();
    __session = sessionDomainExpander == null
        ? __request.getSession(true)
        : sessionDomainExpander.expandDomain(__request, __response);
    // if (__session.isNew()) {
    // Cookie jsessionid = new Cookie("JSESSIONID", __session.getId());
    // jsessionid.setPath("/");
    // addCookie(jsessionid);
    // }
    String contentType = __request.getContentType();
    if (contentType != null && contentType.startsWith("multipart/form-data")) {
      __uploadDirectory =
          IoUtil.getTempDir(__nodeManager.getTemporaryDirectoryRoot(), "NodeRequest-", 10);
      __multipartRequest = new MultipartRequest(__request,
                                                __uploadDirectory.getAbsolutePath(),
                                                MAXIMUM_MULTIPART_SIZE,
                                                "UTF-8");
    } else {
      __uploadDirectory = null;
      __multipartRequest = null;
      switch (getHttpMethod()) {
        case POST:
          InputStream in = null;
          if (contentType != null && !contentType.startsWith("application/x-www-form-urlencoded")) {
            DebugConstants.DEBUG_OUT.println("Reading POST body for contentType: " + contentType);
            try {
              in = __request.getInputStream();
              _postBody = IoUtil.getBytes(__request.getInputStream());
            } finally {
              Closeables.close(in, false);
            }
            DebugConstants.DEBUG_OUT.println(" --> " + getPostBodyAsString());
          }
          break;
        case DELETE:
        case GET:
        case HEAD:
        case PUT:
      }
    }
    _view = Strings.nullToEmpty(getString(CGI_VAR_VIEW));
    // this should always be defined.
    // if new session then will be associated with ANONYMOUS
    _userSession = __nodeManager.getUserManager().getUserSession(__session);
    __isAdmin = getUserId() == 3;
    __sessionTransaction = _userSession.getSessionTransaction(__session);
    String virtualPath = __request.getPathInfo();
    _virtualPath = virtualPath == null ? "" : virtualPath;
    if (__nodeManager.hasRequestRewriters()) {
      RequestPath requestPath = new RequestPath(__request, __nodeManager);
      requestPath = __nodeManager.rewrite(requestPath);
      _virtualPath = requestPath.getVirtualPath();
    }
    nodeServlet.nodeRequestHook_PostAuthentication(context, __session, getUser());
    context.put(NodeServlet.WEBMACRO_NODEREQUEST, this);
    context.put(Login.TARGETURL, nodeManager.getHttpServletPath() + _virtualPath);
    Iterator<Object> nodeNames =
        new EnumerationWrapper<Object>(new StringTokenizer(_virtualPath, "/"));
    String nodeName = null;
    try {
      addNodeRequestSegment(__nodeManager, nodes.getRootNode(null), context, nodeNames.hasNext());
      while (nodeNames.hasNext()) {
        nodeName = (String) nodeNames.next();
        PersistentRecord node = null;
        try {
          node = nodes.getChildNodeByFilename(_leafNodeRequestSegment.getNode(), nodeName, null);
        } catch (MirrorNoSuchRecordException mnsrEx) {
          PersistentRecord movedNode =
              nodeManager.getMovedNode()
                         .getMovedNodeByFilename(_leafNodeRequestSegment.getNode(), nodeName, null);
          node = movedNode.comp(MovedNode.MOVEDNODE);
          _isMovedNode = true;
        }
        addNodeRequestSegment(__nodeManager, node, context, nodeNames.hasNext());
      }
    } catch (AuthenticationException authEx) {
      addNodeRequestSegment(__nodeManager, authEx, context);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      addNodeRequestSegment(__nodeManager, mnsrEx, nodeName, context);
    } catch (FeedbackErrorException feEx) {
      addNodeRequestSegment(__nodeManager, feEx, context);
    } catch (RedirectionException reEx) {
      appendViewNotes(" --> " + reEx.getTargetUrl());
      logAccess();
      throw reEx;
    }
    if (_isMovedNode) {
      throw new RedirectionException((PersistentRecord) null, getTargetLeafNode(), null);
    }
    logAccess();
    updateCurrentPosition();
    // updateSessionParams();
  }

  /**
   * Resolve the NodeRequest from the given Context, or the OuterVars if it isn't directly defined
   * within the Context.
   * 
   * @return The NodeRequest
   * @throws NullPointerException
   *           if for some reason the NodeRequest is not stored.
   * @see NodeServlet#WEBMACRO_NODEREQUEST
   */
  public static NodeRequest getNodeRequest(Map<Object, Object> context)
  {
    return Preconditions.checkNotNull((NodeRequest) VarsTool.delegatingGet(context,
                                                                           NodeServlet.WEBMACRO_NODEREQUEST),
                                      "Cannot resolve NodeRequest from Context %s",
                                      context);
  }

  /**
   * Get the body of a POST method as a raw byte array.
   * 
   * @return The appropriate byte array
   * @throws IllegalStateException
   *           If this NodeRequest is not a POST
   * @see #getHttpMethod()
   */
  public byte[] getPostBodyAsBytes()
      throws IllegalStateException
  {
    switch (getHttpMethod()) {
      case POST:
        return _postBody;
      default:
        throw new IllegalStateException(String.format("%s is a %s not a POST",
                                                      this,
                                                      getHttpMethod()));
    }
  }

  /**
   * Get the body of a POST method as a String utilising the character encoding specified in the
   * incoming request.
   * 
   * @return The appropriate String
   * @throws UnsupportedEncodingException
   *           If the request specified an unsupported encoding
   * @throws IllegalStateException
   *           If this NodeRequest is not a POST
   * @see #getHttpMethod()
   */
  public String getPostBodyAsString()
      throws UnsupportedEncodingException, IllegalStateException
  {
    return new String(_postBody, __request.getCharacterEncoding());
  }

  public final HttpMethod getHttpMethod()
  {
    return __httpMethod;
  }

  /**
   * Get the Gettable wrapper around the Context that this NodeRequest is utilising.
   * 
   * @return The appropriate Gettable. Not null. Cached.
   */
  public final Gettable getGettableContext()
  {
    return __gettableContext;
  }

  /**
   * Provide a Gettable view of the supplied Context. This will check to see whether or not there is
   * already a cached Gettable stored under WEBMACRO_GETTABLECONTEXT and if there is then it will
   * use it. If there isn't then a new wrapper will be created, but this isn't cached in the Map
   * because it may otherwise corrupt an unspecified source Map that may or may not be a Context.
   */
  public final static Gettable asGettable(Map<?, ?> context)
  {
    Gettable gettable = (Gettable) context.get(WEBMACRO_GETTABLECONTEXT);
    if (gettable == null) {
      gettable = buildGettableWrapper(context);
      // context.put(WEBMACRO_GETTABLECONTEXT, gettable);
    }
    return gettable;
  }

  @SuppressWarnings("unchecked")
  private static Gettable buildGettableWrapper(Map context)
  {
    return new GettableMapWrapper(context);
  }

  /**
   * Get a Gettable view of this NodeRequest where the majority of the Gettable access routes are
   * padded with default values if there is no value defined. Note that the default Object Gettable
   * will return "" if there is no Object defined so that you can use this in a webmacro context
   * without worrying about checking the validity of all parameters.
   * 
   * @return A view of this NodeRequest
   * @see PaddedGettable
   */
  public final Gettable getPaddedGettable()
  {
    return new PaddedGettable(this, "", null);
  }

  private static SimpleDateFormat LOGTIMESTAMP = new SimpleDateFormat("dd/MM HH:mm:ss");

  /**
   * Get "now" formatted in a manner suitable for logging.
   */
  public static String getLoggingTimestamp()
  {
    return Dates.format(LOGTIMESTAMP, new Date());
  }

  private void logAccess()
  {
    if (DebugConstants.DEBUG_ACCESSLOG) {
      StringBuilder buf = new StringBuilder();
      buf.append(getLoggingTimestamp()).append(" - ");
      buf.append(getNodeManager().getHostName()).append(": ");
      int userId = getUserId();
      if (User.ID_ANONYMOUS != userId) {
        buf.append(getUser().opt(User.LOGIN)).append(": ");
      }
      buf.append(_virtualPath);
      boolean hasView = !Strings.isNullOrEmpty(_view);
      boolean hasViewNotes = !Strings.isNullOrEmpty(_viewNotes);
      if (hasView | hasViewNotes) {
        buf.append(" (").append(_view);
        if (hasViewNotes) {
          if (hasView) {
            buf.append(": ");
          }
          buf.append(_viewNotes);
        }
        buf.append(") ");
      }
      DebugConstants.DEBUG_OUT.println(buf);
    }
  }

  public String getVirtualPath()
  {
    return _virtualPath;
  }

  /**
   * Reconstruct the String representing the RequestURL with the QueryString appended if necessary.
   */
  public String getFullURL()
  {
    HttpServletRequest request = getRequest();
    StringBuffer requestURL = request.getRequestURL();
    String queryString = request.getQueryString();
    if (queryString == null) {
      return requestURL.toString();
    } else {
      return requestURL.append('?').append(queryString).toString();
    }
  }

  /**
   * Get an incoming Cookie, utilising the outgoing Cookie cache if necessary. This will pick up
   * "genuine" Cookies that have been sent in by the client, and also "pending" Cookies that have
   * been set by a previous NodeRequestSegment that has been chained to the current
   * NodeRequestSegment via a ReloadException. Because individual NodeRequestSegmentFactories do not
   * know whether or not they have been chained, they should always use this method to retrieve
   * Cookies as the overhead is minimal.
   * 
   * @param name
   *          The name of the Cookie to return. null will return null.
   * @return The requested Cookie or null if it cannot be found in the pending cache or the incoming
   *         request. Note that the pending cache will take precendence over the incoming request in
   *         instances of multiple Cookies with the same name.
   * @see SessionDomainExpander#getCookie(HttpServletRequest,String)
   */
  public Cookie getCookie(String name)
  {
    if (name == null) {
      return null;
    }
    if (_pendingCookies != null) {
      Cookie cookie = _pendingCookies.get(name);
      if (cookie != null) {
        return cookie;
      }
    }
    return SessionDomainExpander.getCookie(__request, name);
  }

  public final void setViewNotes(String viewNotes)
  {
    _viewNotes = viewNotes;
  }

  public final void appendViewNotes(String viewNotes)
  {
    _viewNotes = Strings.nullToEmpty(_viewNotes) + viewNotes;
  }

  /**
   * Register another Cookie with the NodeRequest outgoing Cookie cache. This will create the cache
   * if required and drop the Cookie into the cache and the servlet response. It is only necessary
   * to utilise this operation if you are planning to issue a ReloadException and you want the
   * Cookie to be available to the next NodeRequestSegmentFactory in the chain. If in doubt then use
   * this method...
   * 
   * @param cookie
   *          The Cookie that is due to be registered. null will do nothing.
   * @see #getCookie(String)
   */
  public final void addCookie(Cookie cookie)
  {
    if (cookie == null) {
      return;
    }
    if (_pendingCookies == null) {
      _pendingCookies = new HashMap<String, Cookie>();
    }
    _pendingCookies.put(cookie.getName(), cookie);
    __response.addCookie(cookie);
  }

  public boolean isDeletable(PersistentRecord node)
  {
    return getNodeManager().getNode()
                           .isDeletable(node, getUser(), false, "NodeRequest.isDeletable");
  }

  /**
   * Invoke {@link Node#isEditable(PersistentRecord, PersistentRecord, boolean)} on the given Node
   * using the User encapsulated in this NodeRequest.
   */
  public boolean isEditable(PersistentRecord node)
  {
    return getNodeManager().getNode().isEditable(node, getUser(), false);
  }

  /**
   * Internal call-back method to permit the NodeUserManager to inform the NodeRequest that the
   * login has changed. I'm not really sure if I like working it in this way, but it will do for
   * now...
   */
  protected void setUserSession(NodeUserManager.UserSession userSession)
  {
    _userSession = userSession;
  }

  public WebContext getWebContext()
  {
    return __context;
  }

  @Deprecated
  public final boolean getIsAdmin()
  {
    return __isAdmin;
  }

  /**
   * Get the path the webmacro template that should be initially invoked to handle this NodeRequest.
   * This is defined as the value of NODE_ROOTTEMPLATEPATH for the youngest child that is defined.
   * If no Node has nominated a root template, then this is viewed as a LogicException.
   * 
   * @return String containing the path relative to the webmacro root template directory of the
   *         appropriate template to resolve.
   */
  public String getRootTemplatePath()
  {
    if (_rootTemplatePath == null) {
      throw new LogicException("No Nodes defined rootTemplatePath in " + this);
    }
    return _rootTemplatePath;
  }

  /**
   * @return The SessionTransaction that represents the ongoing session managed by the user
   *         responsible for this NodeRequest. This should never be null.
   */
  public final NodeUserManager.SessionTransaction getSessionTransaction()
  {
    return __sessionTransaction;
  }

  /**
   * Get the specified Node from the viewpoint of the Transaction currently associated with the
   * session.
   * 
   * @param nodeId
   *          The required ID
   * @return The requested node.
   */
  public final PersistentRecord getNode(int nodeId)
      throws MirrorNoSuchRecordException
  {
    return getSessionTransaction().getNode(nodeId);
  }

  /**
   * Filter the given node according to the Transaction currently associated with the session.
   * 
   * @param node
   *          The node to be filtered
   * @return The same PersistentRecord if this NodeRequest doesn't have a lock on the node, or the
   *         appropriate WritableRecord if the lock is held.
   */
  public final PersistentRecord getNode(PersistentRecord node)
  {
    Preconditions.checkNotNull(node, "NodeRequest.getNode - pre");
    node = getSessionTransaction().getNode(node);
    Preconditions.checkNotNull(node, "NodeRequest.getNode - post");
    return node;
  }

  public void debug(SimpleTimer timer)
  {
    debug("", timer);
  }

  public void debug(String text,
                    SimpleTimer timer)
  {
    DebugConstants.DEBUG_OUT.println(text + ":" + timer.getElapsedTime());
  }

  public final boolean getIsLoggedIn()
  {
    return User.ID_ANONYMOUS != getUserId();
  }

  public final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  public final NodeServlet getNodeServlet()
  {
    return __nodeServlet;
  }

  public final HttpSession getSession()
  {
    return __session;
  }

  public final HttpServletRequest getRequest()
  {
    return __request;
  }

  public final HttpServletResponse getResponse()
  {
    return __response;
  }

  public final int getUserId()
  {
    return _userSession.getUserId();
  }

  public final PersistentRecord getUser()
  {
    try {
      return getNodeManager().getUser().getTable().getRecord(getUserId());
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("User has disappeared (odd)", mnsrEx);
    }
  }

  /**
   * Get the human readable name of the current view that is being processed.
   * 
   * @return The name of the current view.
   */
  public final String getView()
  {
    return _view;
  }

  /**
   * Does this NodeRequest have either an empty view with new transactional lock or an explicit view
   * of "read"?
   * 
   * @see ReadViewFactory#NAME
   */
  public boolean isRead()
  {
    final String view = getView();
    return (Strings.isNullOrEmpty(view)
            && getTargetLeafNodeTransactionType() == Transaction.TRANSACTION_NULL)
           || ReadViewFactory.NAME.equals(view);
  }

  private final void setView(String view)
  {
    _view = view;
  }

  /**
   * Get the file handle for an uploaded file with a specified name from a multipart request. Please
   * note that clients should not do anything other than read this File as it may be required by
   * other elements in the request. It is particularly not necessary to delete the file as the
   * NodeRequest will deal with this during the finalise() method.
   * 
   * @param key
   *          The parameter name that the file was uploaded under. Note that this is not necessarily
   *          the same name as the file itself.
   * @return The file handle (in the created temporary directory) or null if there was no file
   *         uploaded with the specified name.
   */
  @Override
  public final File getFile(Object key)
  {
    return __multipartRequest == null ? null : __multipartRequest.getFile(key.toString());
  }

  /**
   * Get a String out of the NodeRequest which, unlike the conventional Gettable methods, will not
   * delegate to the user's HttpSession if the variable is not explicitly defined in the incoming
   * request.
   */
  public final String getStringIgnoringSession(String name)
  {
    Boolean isDefined = isExplicitlyDefined(name);
    if (Boolean.FALSE.equals(isDefined)) {
      return null;
    }
    return getStringFromRequest(name);
  }

  public enum MaybeNull {
    MAYBENULL,
    NOTNULL;
  }

  private String getStringFromSession(String name)
  {
    return StringUtils.toString(getSession().getAttribute(name));
  }

  private String getStringFromRequest(String name)
  {
    return getStringFromRequest(name, false);
  }

  private String getStringFromRequest(String name,
                                      boolean isSecondAttempt)
  {
    try {
      return __multipartRequest == null
          ? __request.getParameter(name)
          : __multipartRequest.getParameter(name);
    } catch (NullPointerException npEx) {
      if (isSecondAttempt) {
        System.err.println(this + ".getParameter(" + name + ",...): " + npEx
                           + ", second attempt --> null");
        return null;
      } else {
        System.err.println(this + ".getParameter(" + name + ",...): " + npEx + ", recursing");
        return getStringFromRequest(name, true);
      }
    }
  }

  public boolean hasParameterKey(Object key)
  {
    if (key == null) {
      return false;
    }
    String name = key.toString();
    if (__multipartRequest == null) {
      return __request.getParameterMap().containsKey(name);
    }
    for (Enumeration<?> e = __multipartRequest.getParameterNames(); e.hasMoreElements();) {
      if (name.equals(e.nextElement())) {
        return true;
      }
    }
    return false;
  }

  /**
   * Provide a hash-style accessor wrapper around getParameter(key, false).
   * 
   * @param key
   *          The name of the incoming CGI request parameter which will be converted into a String
   *          via .toString()
   * @return The given incoming parameter, or null if the parameter is not defined.
   * @see #getString(Object)
   */
  @Override
  public final Object get(Object key)
  {
    return getString(key);
  }

  @Override
  public boolean containsKey(Object key)
  {
    if (key == null) {
      return false;
    }
    return hasParameterKey(key) || isExplicitlyDefined(key) != null;
  }

  public static final String DEFAULTDEFINEDVALUE_POSTFIX = "-defaultDefinedValue";

  @Override
  public String getString(Object key)
  {
    if (key == null) {
      return null;
    }
    String name = key.toString();
    Boolean isDefined = isExplicitlyDefined(name);
    if (Boolean.FALSE.equals(isDefined)) {
      return null;
    }
    String value = getStringFromRequest(name);
    if (value != null) {
      return value;
    }
    if (Boolean.TRUE.equals(isDefined)) {
      return MoreObjects.firstNonNull(getStringFromRequest(name + DEFAULTDEFINEDVALUE_POSTFIX), "");
    }
    return getStringFromSession(name);
  }

  @Override
  public List<?> getValues(Object key)
  {
    String name = StringUtils.toString(key);
    String[] values = __multipartRequest == null
        ? __request.getParameterValues(name)
        : __multipartRequest.getParameterValues(name);
    if (values == null) {
      if ("true".equals(getString(name + HtmlUtilsTheme.ACTIVENAMEPOSTFIX))
          | Boolean.TRUE.equals(isExplicitlyDefined(name))) {
        return Collections.emptyList();
      }
      return null;
    }
    return ImmutableList.copyOf(values);
  }

  @Override
  public Collection<?> getPaddedValues(Object key)
  {
    Collection<?> values = getValues(key);
    return values == null ? ImmutableList.of() : values;
  }

  /**
   * Parse a given string into a boolean. null and "" --> null, "false" --> FALSE, everything else
   * goes to TRUE.
   */
  public static Boolean toBoolean(String value)
  {
    if (value == null) {
      return null;
    }
    if ("".equals(value)) {
      return null;
    }
    if ("false".equals(value)) {
      return Boolean.FALSE;
    }
    return Boolean.TRUE;
  }

  public Boolean isExplicitlyDefined(Object key)
  {
    return getExplicitBoolean(ISDEFINED_NAMECACHE.getNamespacedName(key));
  }

  public Boolean getExplicitBoolean(String name)
  {
    Boolean flag = toBoolean(getStringFromRequest(name));
    if (flag == null) {
      if (Boolean.TRUE.equals(toBoolean(getStringFromRequest(BOOLEAN_ACTIVE_NAMECACHE.getNamespacedName(name))))) {
        flag = Boolean.FALSE;
      }
    }
    return flag;
  }

  @Override
  public Boolean getBoolean(Object key)
  {
    if (key == null) {
      return null;
    }
    String name = key.toString();
    Boolean isDefined = isExplicitlyDefined(name);
    if (Boolean.FALSE.equals(isDefined)) {
      return null;
    }
    Boolean flag = getExplicitBoolean(name);
    if (flag == null && Boolean.TRUE.equals(isDefined)) {
      return Boolean.FALSE;
    }
    return flag;
  }

  /**
   * @see MultipartRequest#getParameterNames()
   * @see javax.servlet.ServletRequest#getParameterNames()
   */
  @Override
  public Collection<String> getPublicKeys()
  {
    Enumeration<?> e = null;
    if (__multipartRequest != null) {
      e = __multipartRequest.getParameterNames();
    } else {
      e = __request.getParameterNames();
    }
    if (!e.hasMoreElements()) {
      return Collections.emptyList();
    }
    List<String> names = new ArrayList<String>();
    while (e.hasMoreElements()) {
      names.add(StringUtils.toString(e.nextElement()));
    }
    return Collections.unmodifiableList(names);
  }

  private final NodeRequestSegment updateCurrentPosition()
  {
    return updateCurrentPosition(0);
  }

  private final NodeRequestSegment updateCurrentPosition(int relativePosition)
  {
    _currentPosition += relativePosition;
    _nextChildName = hasNextRequestSegment()
        ? getNodeRequestSegment(_currentPosition + 1).getNode().opt(Node.FILENAME)
        : "";
    return getNodeRequestSegment(_currentPosition);
  }

  private final void addNodeRequestSegment(NodeManager nodeManager,
                                           AuthenticationException authEx,
                                           WebContext context)
      throws NodeRequestException
  {
    try {
      setView(ReadViewFactory.NAME);
      addNodeRequestSegment(nodeManager,
                            nodeManager.getLoginNode(),
                            authEx.getTriggerNode(),
                            context,
                            false,
                            Transaction.TRANSACTION_NULL);
      _errorMessage = authEx.getMessage();
    } catch (AuthenticationException wierdEx) {
      wierdEx.printStackTrace();
    } catch (NodeRequestException nrEx) {
      nrEx.printStackTrace();
      throw nrEx;
    }
  }

  private final void addNodeRequestSegment(NodeManager nodeManager,
                                           FeedbackErrorException feEx,
                                           WebContext context)
      throws MirrorTransactionTimeoutException, NodeRequestException
  {
    if (DebugConstants.DEBUG_ACCESSLOG) {
      DebugConstants.DEBUG_OUT.println(getNodeManager().getHostName() + ':' + _virtualPath + ":\""
                                       + feEx.getMessage() + "\":" + feEx.getStackTrace()[0]);
      if (feEx.isLoggedCause() && DebugConstants.DEBUG_FEEDBACKEXCEPTION) {
        feEx.printStackTrace(DebugConstants.DEBUG_OUT);
      }
    }
    try {
      setView(ReadViewFactory.NAME);
      context.put("feedbackErrorException", feEx);
      setErrorMessage(feEx.getMessage());
      setErrorTitle(feEx.getTitle());
      addNodeRequestSegment(nodeManager,
                            nodeManager.getFeedbackErrorNode(),
                            feEx.getTriggerNode(),
                            context,
                            false,
                            Transaction.TRANSACTION_NULL);
    } catch (AuthenticationException wierdEx) {
      throw new LogicException("feedbackError node shouldn't throw authentication", wierdEx);
    }
  }

  private final void addNodeRequestSegment(NodeManager nodeManager,
                                           MirrorNoSuchRecordException mnsrEx,
                                           String badFilename,
                                           WebContext context)
      throws MirrorTransactionTimeoutException, NodeRequestException
  {
    setViewNotes("404");
    try {
      PersistentRecord lastLegalNode = getLastNodeRequestSegment().getNode();
      context.put(WEBMACRO_LASTLEGALNODE, lastLegalNode);
      context.put(WEBMACRO_BADFILENAME, badFilename);
      getResponse().setStatus(HttpServletResponse.SC_NOT_FOUND);
      addNodeRequestSegment(nodeManager,
                            nodeManager.getNode()
                                       .getTable()
                                       .getRecord(nodeManager.getFourOhFourNodeId()),
                            context,
                            false);
    } catch (MirrorNoSuchRecordException missing404Ex) {
      throw new LogicException("Lost 404 node", missing404Ex);
    } catch (AuthenticationException authEx) {
      addNodeRequestSegment(nodeManager, authEx, context);
    }
  }

  public final String getErrorMessage()
  {
    return _errorMessage;
  }

  public final String getErrorTitle()
  {
    return _errorTitle;
  }

  public final void setErrorMessage(String errorMessage)
  {
    _errorMessage = errorMessage;
  }

  public final void setErrorTitle(String errorTitle)
  {
    _errorTitle = errorTitle;
  }

  private boolean _isSummaryRole = true;

  /**
   * Check to see whether this NodeRequest is still in the process of building summary Nodes. This
   * will stay as true until it starts to render the final leaf Node in the NodeRequest. This can be
   * used in place of the isSummaryRole boolean variable that is passed to the various
   * addNodeRequestSegment methods but which is not always passed down the inheritance tree.
   * 
   * @return true if the NodeRequestSegment which is currently being built is not the target leaf
   *         node.
   */
  public boolean isSummaryRole()
  {
    return _isSummaryRole;
  }

  private final void addNodeRequestSegment(NodeManager nodeManager,
                                           PersistentRecord node,
                                           Context context,
                                           boolean isSummaryRole)
      throws NodeRequestException
  {
    _isSummaryRole = isSummaryRole;
    node = __sessionTransaction.getNode(node);
    addNodeRequestSegment(nodeManager,
                          node,
                          node,
                          context,
                          isSummaryRole,
                          __sessionTransaction.getTransactionTypeByNode(node.getId()));
  }

  private final void addNodeRequestSegment(NodeManager nodeManager,
                                           PersistentRecord renderNode,
                                           PersistentRecord targetNode,
                                           Context context,
                                           boolean isSummaryRole,
                                           int transactionType)
      throws NodeRequestException
  {
    NodeRequestSegmentManager manager = nodeManager.getNodeRequestSegmentManager();
    NodeRequestSegment segment = null;
    _targetLeafNode = targetNode;
    try {
      for (NodePreProcessor nodePreProcessor : nodeManager.getNodePreProcessorsForNodeStyle(Integer.valueOf(renderNode.compInt(Node.NODESTYLE_ID)))) {
        nodePreProcessor.preprocessNode(this,
                                        renderNode,
                                        targetNode,
                                        context,
                                        isSummaryRole,
                                        transactionType);
      }
      segment = manager.getNodeRequestSegment(this,
                                              renderNode,
                                              context,
                                              isSummaryRole
                                                  ? Transaction.TRANSACTION_NULL
                                                  : transactionType,
                                              isSummaryRole ? null : _view,
                                              isSummaryRole);
    } catch (RuntimeException runEx) {
      throw new FeedbackErrorException(null,
                                       renderNode,
                                       runEx,
                                       true,
                                       true,
                                       "There has been an unexpected error");
    } catch (ReloadException reloadEx) {
      String reloadView = reloadEx.getTargetView();
      if (getView().equals(reloadView)) {
        _recursiveReloadCount++;
        if (_recursiveReloadCount > RECURSIVERELOAD_MAXIMUM) {
          throw new LogicException("recursive reload exception after " + RECURSIVERELOAD_MAXIMUM
                                   + " requests for " + reloadView,
                                   reloadEx);
        }
      } else {
        _recursiveReloadCount = 0;
      }
      if (DebugConstants.DEBUG_ACCESSLOG) {
        DebugConstants.DEBUG_OUT.println(getNodeManager().getHostName() + ":   " + getView()
                                         + " --> " + reloadEx.getTargetView());
      }
      setView(reloadEx.getTargetView());
      addNodeRequestSegment(nodeManager,
                            renderNode,
                            targetNode,
                            context,
                            isSummaryRole,
                            __sessionTransaction.getTransactionTypeByNode(targetNode.getId()));
      return;
    }
    __nodeRequestSegments.add(segment);
    _leafNodeRequestSegment = segment;
    PersistentRecord nodeStyle = targetNode.comp(Node.NODESTYLE);
    if (nodeStyle.is(NodeStyle.ISRENDERRESET) || segment.getIsRenderReset()) {
      _currentPosition = __nodeRequestSegments.size() - 1;
    }
    String rootTemplatePath = segment.getIsRootTemplatePath()
        ? segment.getTemplatePath()
        : nodeStyle.opt(NodeStyle.ROOTTEMPLATEPATH);
    if (rootTemplatePath.length() > 0) {
      _rootTemplatePath = rootTemplatePath;
    }
  }

  /**
   * Get the Transaction type of the target leaf node of this NodeRequest.
   * 
   * @see #getTargetLeafNode()
   * @see Transaction#getType()
   */
  public int getTargetLeafNodeTransactionType()
  {
    Transaction t = __sessionTransaction.getTransactionByNode(getTargetLeafNode().getId());
    if (t == null) {
      return Transaction.TRANSACTION_NULL;
    }
    return t.getType();
  }

  /**
   * Get the NodeRequestSegment that currently represents the Leaf Node of this NodeRequest.
   * 
   * @return The Leaf Node!
   */
  public final NodeRequestSegment getLeafNodeRequestSegment()
  {
    return _leafNodeRequestSegment;
  }

  /**
   * Get the original target leaf node that this request points at. This may or may not be the same
   * as getLeafNodeRequestSegment().getNode() depending on whether or not the resolution system
   * swapped it out. Technically speaking this is a security hole because if the target leaf node
   * was rejected due to authentication issues, then the template author should not be permitted to
   * gain access to it by any means. However, I am prepared to let this slide because otherwise all
   * the other Db traversing routines would have to be re-written to include user authentication
   * which is unfeasible. Note that this only gives you read holes and the Transaction system is
   * still solid.
   * 
   * @return The PersistentRecord representing the Node of the original LeafNOdeRequestSegment.
   */
  public final PersistentRecord getTargetLeafNode()
  {
    return _targetLeafNode;
  }

  /**
   * Get the first NodeRequestSegment in this NodeRequest.
   * 
   * @return The first NodeRequestSegment. This should not be null.
   */
  public final NodeRequestSegment getFirstNodeRequestSegment()
  {
    return __nodeRequestSegments.get(0);
  }

  /**
   * Get the final NodeRequestSegment in this NodeRequest. Please note that this is different from
   * getLeafNodeRequestSegment().
   * 
   * @return The final NodeRequestSegment. This should not be null.
   */
  public final NodeRequestSegment getLastNodeRequestSegment()
  {
    return __nodeRequestSegments.get(__nodeRequestSegments.size() - 1);
  }

  public int getLinkMode(String childName)
  {
    return (isSelected(childName) ? LINKMODE_BIT_SELECTED : 0)
           | (isActiveLink(childName) ? LINKMODE_BIT_ACTIVE : 0);
  }

  public int getLinkMode()
  {
    return LINKMODE_BIT_SELECTED | (isActiveLink() ? LINKMODE_BIT_ACTIVE : 0);
  }

  /**
   * Check to see if the supplied filename is the filename of the next NodeRequestSegment (if any).
   * This corresponds to whether or not the supplied filename has been
   * 
   * @param childFilename
   *          The filename of the child to check for.
   * @return true if childFilename is the next filename.
   * @see Node#FILENAME
   */
  public boolean isSelected(String childFilename)
  {
    return (_nextChildName.equals(childFilename));
  }

  /**
   * Check to see if the current Node in this NodeRequest is selected. This is always true, but is
   * included for completeness sake.
   * 
   * @return true (of course).
   */
  public boolean isSelected()
  {
    return true;
  }

  /**
   * Return the filename of the next Node in the NodeRequest.
   * 
   * @return The filename of the next Node in the sequence, or an empty String ("") if the current
   *         Node is the Leaf Node.
   * @see Node#FILENAME
   */
  public String getNextChildName()
  {
    return _nextChildName;
  }

  /**
   * Check to see whether the given child filename should be an active link.
   * 
   * @param childName
   *          The filename to check for.
   * @return true if the given childName would be active as a link. The only hole in this is that it
   *         doesn't actually check for whether or not the childName is valid and therefore it could
   *         return a slightly wrong response, but it will be correct if fed with sensible input.
   */
  public boolean isActiveLink(String childName)
  {
    return ((__nodeRequestSegments.size() - _currentPosition) != 2 || !isSelected(childName));
  }

  /**
   * Check to see whether the current Node in this NodeRequest should be an active link if rendered.
   * 
   * @return true if there are any children Nodes beyond the current Node.
   */
  public boolean isActiveLink()
  {
    return (__nodeRequestSegments.size() - _currentPosition) != 1;
  }

  /**
   * Get a list of all the NodeRequestSegments that make up this parsed NodeRequest.
   * 
   * @return Iterator of NodeRequestSegment objects ordered from the root of the parse tree
   *         downwards.
   * @see NodeRequestSegment
   */
  public Iterator<NodeRequestSegment> getNodeRequestSegments()
  {
    return __nodeRequestSegments.iterator();
  }

  @Override
  public Iterator<NodeRequestSegment> iterator()
  {
    return getNodeRequestSegments();
  }

  /**
   * Get a list of all the NodeRequestSegments that make up this parsed NodeRequest in reverse order
   * from last to first.
   * 
   * @return Iterator of NodeRequestSegment objects ordered from the tail of the parse tree upwards.
   * @see NodeRequestSegment
   */
  public Iterator<NodeRequestSegment> getReverseNodeRequestSegments()
  {
    return new ReverseListIterator<NodeRequestSegment>(__nodeRequestSegments);
  }

  /**
   * Get a ListIterator of the all the NodeRequestSegments that make up this parsed NodeRequest,
   * with the current position in the ListIterator set to the current Node in this NodeRequest.
   * 
   * @return ListIterator of NodeRequestSegment Objects.
   */
  public ListIterator<NodeRequestSegment> getRemainingNodeRequestSegments()
  {
    return __nodeRequestSegments.listIterator(_currentPosition);
  }

  /**
   * Get the NodeRequestSegment with the specified absolute index.
   * 
   * @param index
   *          The depth of the requested NodeRequestSegment. 0 is the root node of the tree.
   * @return The specified node request segment.
   */
  public final NodeRequestSegment getNodeRequestSegment(int index)
  {
    return __nodeRequestSegments.get(index);
  }

  /**
   * Get the node that is found at the given generation in this NodeRequest from the viewpoint of
   * the user controlling the NodeRequest. This is equivalent to using
   * "$nodeRequest.getNodeRequestSegment(generation).Node" in webmacro space.
   * 
   * @param generation
   *          The generation of the targetted Node. 0 is the root node
   * @see #getNodeRequestSegment(int)
   * @see NodeRequestSegment#getNode()
   */
  public final PersistentRecord getNodeByGeneration(int generation)
  {
    return getNodeRequestSegment(generation).getNode();
  }

  /**
   * Get the current NodeRequestSegment. The current segment is defined using the
   * nextNodeRequestSegment() and previousNodeRequestSegment() methods.
   * 
   * @return The current NodeRequestSegment. Currently will throw a RuntimeException if it
   *         overshoots so some work is required as to work out the best course of action in this
   *         case.
   */
  public final NodeRequestSegment getCurrentNodeRequestSegment()
  {
    return getNodeRequestSegment(_currentPosition);
  }

  /**
   * Check to see if there is more NodeRequestSegments after the current segment.
   * 
   * @return A value of true means that a call to nextNodeRequestSegment will succeed, whereas a
   *         value of false implies that the current segment is the final segment.
   */
  public final boolean hasNextRequestSegment()
  {
    return _currentPosition < (__nodeRequestSegments.size() - 1);
  }

  /**
   * Get the number of NodeRequestSegments that are found in this NodeRequest.
   * 
   * @return The number of NodeRequestSegments. This will be >= 1.
   * @see #size()
   */
  public final int getNodeRequestSegmentCount()
  {
    return size();
  }

  /**
   * Get the number of NodeRequestSegments that are in this NodeRequest
   * 
   * @return The number of NodeRequestSegments. This will be >= 1.
   * @see #size()
   */
  public final int size()
  {
    return __nodeRequestSegments.size();
  }

  /**
   * Check to see if there is an earlier NodeRequestSegment before the current segment.
   * 
   * @return A value of true means that a call to previousNodeRequestSegment will succeed, whereas a
   *         value of false implies that the current segment is the first segment.
   */
  public final boolean hasPreviousRequestSegment()
  {
    return _currentPosition > 0;
  }

  /**
   * Advance the current NodeRequestSegment by one and return the new value.
   * 
   * @return The new value of the current NodeRequestSegment
   */
  public final NodeRequestSegment nextNodeRequestSegment()
  {
    return updateCurrentPosition(1);
  }

  /**
   * Retreat the current NodeRequestSegment by one and return the new value.
   * 
   * @return The new value of the current NodeRequestSegment.
   */
  public final NodeRequestSegment previousNodeRequestSegment()
  {
    return updateCurrentPosition(-1);
  }

  /**
   * Return the NodeRequestSegment with an index a specified distance from the current segment.
   * 
   * @param relativeIndex
   *          Amount to be added onto the current index for the purposes of this operation only.
   * @return The specified NodeRequestSegment.
   */
  public final NodeRequestSegment relativeNodeRequestSegment(int relativeIndex)
  {
    return getNodeRequestSegment(_currentPosition + relativeIndex);
  }

  @Override
  public String toString()
  {
    return "NodeRequest(" + _virtualPath + ")";
  }

  /**
   * Request the NodeRequest to clean up after itself. Any housekeeping should take place in this
   * method. Currently this is limited to:-
   * <ul>
   * <li>If a temporary directory was created to handle this request (i.e. if it was multipart
   * encoded and the attachments have been saved to somewhere before processing) then it is deleted
   * including all it's contents. This is safe because only this Object owns the reference to the
   * directory that is created to store the data in.
   * </ul>
   */
  protected void shutdown()
  {
    if (__uploadDirectory != null) {
      IoUtil.recursiveDelete(__uploadDirectory);
    }
  }

  /**
   * Request this NodeRequest for a Paginator that is booted from the variables that may have been
   * included in the NodeRequest. Please note that the Paginator that is returned will not contain
   * any data as yet, because it is not associated with a sourceQuery. it is up to the client to
   * inform the Paginator as to which Query to use with setSourceQuery(Query). This may be updated
   * in the future if we generate a safe method of serialising Query information across NodeRequest
   * parameters, but my feeling is that it is the responsiblity of the template to contain this
   * information and exposing it to the world at large is a security issue.
   * 
   * @param name
   *          The name of the Paginator that is to be returned. If null, then the default Paginator
   *          is returned. If the name is not found, then an undefined single page Paginator will be
   *          returned.
   * @return The requested Paginator. Never null.
   * @see Paginator#setSourceQuery(RecordSource)
   */
  public Paginator getPaginator(String name,
                                int defaultPageSize)
  {
    return new Paginator(name, this, defaultPageSize);
  }

  public AbcPaginator getAbcPaginator(String name)
  {
    return new AbcPaginator(name, this);
  }

  // ..........................................................................
  /**
   * Return whether the named view is allowed on the TargetLeafNode of this NodeRequest and whether
   * the parent nodes can be accessed using the default view.
   */
  public boolean isAllowed(String viewName)
  {
    return isAllowed(getTargetLeafNode(), viewName);
  }

  /**
   * Return whether the named view is allowed on the specified node and whether the parent nodes can
   * be accessed using the default view.
   */
  public boolean isAllowed(PersistentRecord node,
                           String viewName)
  {
    // TODO: (#254). isAllowed is using default views if it cannot find the
    // requested view
    return isAllowed(node,
                     viewName,
                     getNodeManager().getNodeRequestSegmentManager(),
                     __sessionTransaction.getTransactionTypeByNode(node.getId()));
  }

  /**
   * Return whether the default view is allowed on the specified node and whether the parent nodes
   * can be accessed using the default view.
   */
  public boolean isAllowed(PersistentRecord node)
  {
    return isAllowed(node,
                     null,
                     getNodeManager().getNodeRequestSegmentManager(),
                     __sessionTransaction.getTransactionTypeByNode(node.getId()));
  }

  private boolean isAllowed(PersistentRecord node,
                            String viewName,
                            NodeRequestSegmentManager nrsm,
                            int transactionType)
  {
    if (Login.NAME.equals(viewName))
      return true;
    if (viewName == null)
      viewName = nrsm.getDefaultView(transactionType);
    NodeRequestSegmentFactory view =
        nrsm.getFactory(node.compInt(Node.NODESTYLE_ID), transactionType, viewName);
    boolean allowed = view != null && view.isAllowed(node, this.getUserId());
    // Go up the parent path, making sure all the parent nodes are allowed to
    // use the default view.
    if (allowed && node.mayFollowLink(Node.PARENTNODE)) {
      return isAllowed(node.comp(Node.PARENTNODE), null, nrsm, transactionType);
    }
    return allowed;
  }

  // ..........................................................................
  /**
   * Check to see if the given node is one of the nodes that is referenced in this NodeRequest. This
   * will validate both against the stored leaf node and the original TargetLeafNode if there has
   * been a rewrite operation.
   */
  public boolean contains(PersistentRecord node)
  {
    TransientRecord.assertIsTableNamed(node, Node.TABLENAME);
    int generation = node.comp(Node.GENERATION).intValue();
    if (generation >= size()) {
      return false;
    }
    return (node.getId() == getNodeRequestSegment(generation).getNode().getId())
           | (node.getId() == _targetLeafNode.getId());
  }

  /**
   * Check to see if the nodes contain at least one node that is referenced in this NodeRequest.
   * 
   * @see #contains(PersistentRecord)
   */
  public boolean contains(RecordSource nodes)
  {
    RecordSources.assertIsTableNamed(nodes, Node.TABLENAME);
    for (PersistentRecord node : nodes) {
      if (contains(node)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check to see if there is at least one Node of the given NodeStyle in this NodeRequest
   * 
   * @param nodeStyleId
   *          The compulsory id to check for.
   * @return true if there is a Node of the required nodeStyleid
   * @throws NullPointerException
   *           if nodeStyleId is not defined.
   */
  public boolean containsNodeStyle(Integer nodeStyleId)
  {
    Preconditions.checkNotNull(nodeStyleId, "nodeStyleId");
    for (int i = 0; i < size(); i++) {
      if (nodeStyleId.equals(getNodeByGeneration(i).opt(Node.NODESTYLE_ID))) {
        return true;
      }
    }
    return false;
  }

  /**
   * Get the node at the given generation of this request if it isn't contained in the list of
   * shownNodes. shownNodes will normally be the childrenMenuNodes that have been already displayed
   * in the menu and therefore we can handle the display of non-menu nodes. If the generation is for
   * the end node in the nodeReqeuest then it is the target leaf node that is compared against
   * shownNodes rather than the actual node as this may have been dynamically rewritten.
   * 
   * @param parentNode
   *          The node that is the parent of the shownNodes. If this parentNode is not part of the
   *          NodeRequest then you must be looking at a different subsection of the node tree and
   *          therefore by definition the hiddenNode must be null.
   * @param shownNodes
   *          The nodes that have already been displayed in the navigation. If this is null then the
   *          current node must be hidden so it will be returned.
   * @return The appropriate node from the perspective of the current user or null if the node has
   *         already been shown or the generation is higher than the depth of this request..
   */
  public PersistentRecord getHiddenNode(PersistentRecord parentNode,
                                        RecordSource shownNodes)
  {
    if (!contains(parentNode)) {
      return null;
    }
    int parentGeneration = parentNode.comp(Node.GENERATION).intValue();
    int childGeneration = parentGeneration + 1;
    if (childGeneration < 0 || childGeneration >= size()) {
      return null;
    }
    int parentId = parentNode.getId();
    PersistentRecord node = null;
    if (childGeneration == size() - 1) {
      // DebugConstants.DEBUG_OUT.println("java: using TargetLeafNode");
      node = getTargetLeafNode();
    } else {
      NodeRequestSegment segment = getNodeRequestSegment(childGeneration);
      // DebugConstants.DEBUG_OUT.println("java: using getNodeRequestSegment("
      // + childGeneration + ")");
      node = segment.getNode();
    }
    // if (RecordSources.contains(shownNodes, node, null)) {
    // DebugConstants.DEBUG_OUT.println("java: already in shownNodes");
    // }
    // if (parentId.equals(node.getId())) {
    // DebugConstants.DEBUG_OUT.println("java:parentNode == node");
    // }
    // if (!parentId.equals(node.get(Node.PARENTNODE_ID))) {
    // DebugConstants.DEBUG_OUT.println(String
    // .format("java:%s is not a parent of %s", parentNode, node));
    // }
    if (RecordSources.contains(shownNodes, node, null) || parentId == node.getId()
        || parentId != node.compInt(Node.PARENTNODE_ID)) {
      return null;
    }
    return node;
  }

  /**
   * Get the CSS class that should be utilised for a UL or LI tags in the m_menu structure.
   * 
   * @param parentNode
   *          The node that is the conceptual parent of the nodes that will be contained in the list
   *          that this is going to generate CSS for. If the parentNode is part of this NodeRequest
   *          then the word "current" will be included in the class.
   * @param gen
   *          The generation of the UL that we are interested in
   * @param ul
   *          The nested UL count
   * @return The appropriate string to use as CSS class. This will contain a <em>genx</em>
   *         <em>ulx</em> and <em>relx</em> tag, and optionally a <em>current</em> tag
   * @see #contains(PersistentRecord)
   */
  public String getMenuClass(PersistentRecord parentNode,
                             int gen,
                             int ul)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("gen").append(gen);
    buf.append(" ul").append(ul);
    int rel = size() - 1 - gen;
    if (rel == 0) {
      buf.append(" rel0");
    } else if (rel < 0) {
      buf.append(" rel").append(-rel).append("c");
    } else {
      buf.append(" rel").append(rel).append("p");
    }
    // TODO: current is not a bug - close remine issue
    if (contains(parentNode)) {
      buf.append(" current");
    }
    return buf.toString();
  }

  /**
   * Get the word that describes the relationship of the given node to the TargetLeafNode of this
   * NodeRequest. The returned value will be one of the following:
   * <dl>
   * <dt>child</dt>
   * <dd>The node is younger than the TargetLeafNode. Note that this can be to any younger node, the
   * TargetLeafNode does <em>not</em> have to be a direct parent of the node.</dd>
   * <dt>active</dt>
   * <dd>The node <em>is</em> the TargetLeafNode</dd>
   * <dt>sibling</dt>
   * <dd>The node is of the same age as the TargetLeafNode. Note that it does not have to share a
   * common parent.</dd>
   * <dt>parent</dt>
   * <dd>The node is in the direct parentage of the TargetLeafNode</dd>
   * <dt>uncle</dt>
   * <dd>The node is older than the TargetLeafNode but it is not in the direct parentage line</dd>
   * </dl>
   * 
   * @see #getTargetLeafNode()
   */
  public String getRelationship(PersistentRecord node)
  {
    // DebugConstants.method(this, "getRelationship", node);
    // DebugConstants.DEBUG_OUT.println("nodeGeneration: "
    // + node.get(Node.GENERATION));
    // DebugConstants.DEBUG_OUT.println("size(): " + size());
    int nodeGeneration = node.comp(Node.GENERATION).intValue();
    if (nodeGeneration >= size()) {
      return "child";
    }
    if (nodeGeneration == size() - 1) {
      if (node.getId() == getTargetLeafNode().getId()) {
        return "active";
      }
      return "sibling";
    }
    if (contains(node)) {
      return "parent";
    }
    return "uncle";
  }

  /**
   * Resolve the remote IP address of the invoking request. This will automatically check for
   * X-Forwarded-For headers so that we get a sensible answer for when we are using mod_proxy with
   * jetty, and drops back to {@link ServletRequest#getRemoteAddr()} if this is not defined.
   */
  public String getRemoteAddr()
  {
    HttpServletRequest request = getRequest();
    @SuppressWarnings("unchecked")
    Enumeration<String> headers = request.getHeaders("X-Forwarded-For");
    if (headers.hasMoreElements()) {
      return headers.nextElement();
    }
    return request.getRemoteAddr();
  }

  /**
   * Get the URL represented by this NodeRequest including the query string (if any). This will be
   * pointing at the {@link #getTargetLeafNode()} rather than any of the redirects that were applied
   * to the system so that it matches what was originally asked for.
   */
  public String getUrl()
  {
    HttpServletRequest request = getRequest();
    StringBuilder requestURL = new StringBuilder();
    requestURL.append(getTargetLeafNode().comp(Node.URL));
    String queryString = request.getQueryString();
    if (queryString == null) {
      return requestURL.toString();
    } else {
      return requestURL.append('?').append(queryString).toString();
    }
  }

}
