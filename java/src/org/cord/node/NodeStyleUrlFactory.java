package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;

/**
 * Implementation of UrlFactory that will return the first Node that matches a given NodeStyle. This
 * will throw an exception if either the NodeStyle doesn't exist or if there are no Nodes matching
 * the NodeStyle in the system when resolution is attempted.
 * 
 * @author alex
 */
public class NodeStyleUrlFactory
  extends UrlFactory
{
  private final int __nodeStyleId;

  public NodeStyleUrlFactory(int nodeStyleId)
  {
    __nodeStyleId = nodeStyleId;
  }

  @Override
  protected int getNodeId(NodeManager nodeManager,
                          NodeRequest nodeRequest)
      throws MirrorNoSuchRecordException
  {
    return nodeManager.getNodeStyle().getFirstNodeId(__nodeStyleId);
  }
}
