package org.cord.node.search;

import java.io.PrintWriter;

import org.cord.mirror.DbInitialisers;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.util.EnglishNamed;
import org.cord.util.Gettable;
import org.cord.util.Named;

/**
 * Interface that describes objects that perform some of the work for the AdvancedSearch system.
 * 
 * @author alex
 * @param <T>
 *          The class of data object that this plugin manipulates.
 */
public interface AdvancedSearchPlugin<T>
  extends EnglishNamed, DbInitialisers
{
  /**
   * Is this plugin prepared to do any work at all with this ContentCompiler?
   */
  public boolean accepts(Named object);

  /**
   * What are the list of fields in the given ContentCompiler that this plugin will be indexing?
   * 
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named object);

  /**
   * What are the list of fields in the given ContentCompiler that this plugin will permit dedicated
   * searching against?
   * 
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named object);

  /**
   * Callback method from the AdvancedSearch item that this plugin has been registered with that
   * will be invoked after the AdvancedSearch has completely its internal init methods.
   */
  public void init(AdvancedSearch advancedSearch);

  public void deleteNodeIndex(PersistentRecord node)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException;

  /**
   * Rebuild any indexing information that this plugin currently has with respect to the given Node.
   */
  public void rebuildNode(PersistentRecord node)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException;

  /**
   * Perform the search appropriate to this plugin that is encapsulated in the NodeRequest and
   * return IdList that contains the intersection of the currentMatches and the matches from this
   * plugin. The matches should be in ascending order of id as otherwise the intersection code is
   * not guaranteed to work.
   * 
   * @param request
   *          The incoming Gettable that contains the encapsulated version of the search (this will
   *          often be a NodeRequest).
   * @param searchName
   *          The name of the search that is expected to be parsed within the NodeRequest. If there
   *          are multiple searches that reference this AdvancedSearchPlugin then this method will
   *          get invoked multiple times with a different searchName in each request.
   * @param currentMatches
   *          The current set of Node ids that have been matched by previous plugins. If this is
   *          null then it is assumed that this is the first plugin in the chain and there is no
   *          merging required. If it is defined then the IdList that is returned should be a subset
   *          of this list. If currentMatches is defined and this plugin is not able to perform the
   *          search (as opposed to finding no matches) then the currentMatches should be returned
   *          from the method.
   * @return The current list of cumulative matches that this plugin finds from its search request.
   *         A value of null implies that the currentMatches was null and this search had no
   *         parameters that enabled it to generate a Search and therefore had been ignored. Please
   *         note that this is different from an empty IdList as this implies that there has been a
   *         search performed and there are no matches. A value of null will continue searching
   *         through other plugins whereas an empty IdList will stop with no results after this
   *         plugin.
   */
  public IdList nodeSearch(Gettable request,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug);
}