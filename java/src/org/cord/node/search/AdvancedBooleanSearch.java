package org.cord.node.search;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.field.BooleanField;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;
import org.cord.util.Named;
import org.cord.util.Shutdownable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

/**
 * Implementation of AdvancedSearchPlugin that is responsible for the indexing of individual word
 * data.
 */
public class AdvancedBooleanSearch
  extends AdvancedAbstractSearch<Boolean>
  implements Shutdownable, DbInitialisers, AdvancedSearchPlugin<Boolean>
{
  public static Map<String, Boolean> addIndexableValue(String fieldName,
                                                       Boolean value,
                                                       Map<String, Boolean> existingValues)
  {
    if (existingValues == null) {
      existingValues = new HashMap<String, Boolean>();
    }
    if (value != null) {
      existingValues.put(fieldName, value);
    }
    return existingValues;
  }

  public static Map<String, Boolean> addIndexableValue(String fieldName,
                                                       boolean value,
                                                       Map<String, Boolean> existingValues)
  {
    return addIndexableValue(fieldName, Boolean.valueOf(value), existingValues);
  }

  public static Map<String, Boolean> addIndexableValues(Map<String, Boolean> existingValues,
                                                        Gettable gettable,
                                                        Iterable<String> fields)
  {
    for (String field : fields) {
      existingValues = addIndexableValue(field, gettable.getBoolean(field), existingValues);
    }
    return existingValues;
  }

  public static final String NAME = "advancedBooleanSearch";

  private final AsBoolean __asBoolean;

  public AdvancedBooleanSearch(NodeManager nodeManager,
                               String pwd)
  {
    super(NAME,
          "Boolean Search",
          nodeManager,
          pwd);
    // _nodeManager = nodeManager;
    __asBoolean = new AsBoolean();
  }

  // protected NodeManager getNodeManager()
  // {
  // return _nodeManager;
  // }

  public AsBoolean getAsBoolean()
  {
    return __asBoolean;
  }

  @Override
  public boolean accepts(Named named)
  {
    return named instanceof AdvancedBooleanSearchable;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedBooleanSearchable) named).getAbsIndexableFields();
    }
    return ImmutableList.of();
  }

  @Override
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedBooleanSearchable) named).getAbsSearchableFields();
    }
    return ImmutableList.of();
  }

  // public synchronized void init(AdvancedSearch advancedSearch)
  // {
  // Preconditions.checkNotNull(advancedSearch, "advancedSearch");
  // if (_advancedSearch != null) {
  // throw new IllegalStateException("Cannot register " + this + " on "
  // + advancedSearch + " when it is already registered on "
  // + _advancedSearch);
  // }
  // _advancedSearch = advancedSearch;
  // }

  // protected AdvancedSearch getAdvancedSearch()
  // {
  // return _advancedSearch;
  // }

  // private void rebuildNode(PersistentRecord node,
  // PersistentRecord regionStyle,
  // Named named)
  // throws MirrorNoSuchRecordException, MirrorFieldException,
  // MirrorInstantiationException
  // {
  // if (named instanceof AdvancedBooleanSearchable) {
  // AdvancedBooleanSearchable advancedBooleanSearchable =
  // (AdvancedBooleanSearchable) named;
  // // this will throw mnsrEx on non-init cc. Is this a problem?
  // getAdvancedSearch().getAsContentCompiler().getInstance(named, null);
  // Map<String, Boolean> allValues =
  // advancedBooleanSearchable.getAbsIndexableValues(node,
  // regionStyle,
  // null);
  // for (String fieldName : allValues.keySet()) {
  // // this will throw mnsrEx on non-init cc or fieldName. Is this a
  // // problem?
  // PersistentRecord asCcf =
  // (getAdvancedSearch().getAsContentCompilerField()
  // .getInstance(named, fieldName, null));
  // Boolean flag = allValues.get(fieldName);
  // TransientRecord asBoolean =
  // getAsBoolean().getTable().createTransientRecord();
  // asBoolean.setField(AsBoolean.ASCONTENTCOMPILERFIELD_ID, asCcf.getId());
  // asBoolean.setField(AsBoolean.NODE_ID, node.getId());
  // if (regionStyle != null) {
  // asBoolean.setField(AsBoolean.REGIONSTYLE_ID, regionStyle.getId());
  // }
  // asBoolean.setField(AsBoolean.FLAG, flag);
  // asBoolean.makePersistent(null, false);
  // }
  // }
  // }

  // public void rebuildNode(PersistentRecord node)
  // throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException,
  // MirrorFieldException, MirrorInstantiationException
  // {
  // getAsBoolean().clearDataItems(node);
  // PersistentRecord nodeStyle = node.followEnforcedLink(Node.NODESTYLE);
  // Iterator<PersistentRecord> regionStyles =
  // nodeStyle.followReferences(NodeStyle.REGIONSTYLES);
  // while (regionStyles.hasNext()) {
  // PersistentRecord regionStyle = regionStyles.next();
  // ContentCompiler contentCompiler =
  // getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
  // rebuildNode(node, regionStyle, contentCompiler);
  // }
  // // Iterator regions = node.followReferences(Node.REGIONS);
  // // while (regions.hasNext()) {
  // // PersistentRecord region = (PersistentRecord) regions.next();
  // // ContentCompiler cc = RegionContentCompiler.getContentCompiler(region,
  // // getNodeManager().getContentCompilerFactory());
  // // rebuildNode(node, region, cc);
  // // }
  // rebuildNode(node, null, getAdvancedSearch().getNodeStub());
  // }

  public static final String AS_FLAG = "flag";

  @Override
  public IdList nodeSearch(Gettable gettable,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug)
  {
    String flagKey = AdvancedSearch.createExplicitKey(searchName, AS_FLAG);
    if (debug != null) {
      debug.println("RAW:" + flagKey + ":" + gettable.get(flagKey));
    }
    Boolean flag = gettable.getBoolean(flagKey);
    if (flag == null) {
      return currentMatches;
    }
    if (debug != null) {
      debug.println(flagKey + ":" + flag);
    }
    Query baseQuery =
        getAdvancedSearch().getBaseQuery(gettable, searchName, getAsDataItem(), debug);
    StringBuilder filter = new StringBuilder();
    filter.append(" and (AsBoolean.flag=").append(flag.booleanValue() ? "1" : "0").append(")");
    Query booleanQuery =
        Query.transformQuery(baseQuery, filter.toString(), Query.TYPE_EXPLICIT, null);
    if (debug != null) {
      debug.println(booleanQuery);
    }
    if (currentMatches == null) {
      currentMatches = booleanQuery.getIdList();
    } else {
      currentMatches = booleanQuery.intersection(currentMatches);
    }
    return currentMatches;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    return Iterators.<DbInitialiser> singletonIterator(__asBoolean);
  }

  @Override
  public AdvancedSearch.AsDataItem<Boolean> getAsDataItem()
  {
    return __asBoolean;
  }

  public static class AsBoolean
    extends AdvancedSearch.AsDataItem<Boolean>
    implements DbInitialiser, TableWrapper
  {
    public final static String TABLENAME = "AsBoolean";

    public final static ObjectFieldKey<Boolean> FLAG = BooleanField.createKey("flag");

    private AsBoolean()
    {
      super(TABLENAME);
    }

    @Override
    public void init(Db db,
                     String transactionPassword,
                     Table asBoolean)
        throws MirrorTableLockedException
    {
      asBoolean.addField(new BooleanField(FLAG, "Flag value"));
    }

    @Override
    public FieldKey<Boolean> getDataFieldName()
    {
      return FLAG;
    }

    @Override
    public Boolean getDataField(PersistentRecord asDataItem)
    {
      return asDataItem.opt(getDataFieldName());
    }
  }

  @Override
  protected void buildLiveFieldsIndex(PersistentRecord node,
                                      FieldsIndex nodeIndex,
                                      PersistentRecord regionStyle,
                                      Named named)
      throws MirrorNoSuchRecordException
  {
    int regionStyleId = regionStyle == null ? 0 : regionStyle.getId();
    if (named instanceof AdvancedBooleanSearchable) {
      AdvancedBooleanSearchable searchable = (AdvancedBooleanSearchable) named;
      Map<String, Boolean> values = searchable.getAbsIndexableValues(node, regionStyle, null);
      if (values != null) {
        for (String fieldName : values.keySet()) {
          PersistentRecord asCcf =
              getAdvancedSearch().getAsContentCompilerField().getInstance(named, fieldName, null);
          DataItems dataItems = getDataItems(nodeIndex, asCcf.getId(), regionStyleId, true);
          dataItems.put(values.get(fieldName), DataItems.ID_MISSING);
        }
      }
    }
  }
}