package org.cord.node.search;

import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Interface that is used to descibe Objects that are capable of supplying indexable information and
 * meta-data to the AdvancedWordSearch plugin.
 * 
 * @see AdvancedWordSearch
 */
public interface AdvancedIntegerSearchable
  extends AdvancedSearchable
{
  /**
   * @return Map that holds Map(String, Set(Integer))
   * @see AdvancedIntegerSearch#addIndexableValues(Iterator, TransientRecord, Map)
   */
  public Map<RecordOperationKey<?>, IntSet> getAisIndexableValues(PersistentRecord node,
                                                                  PersistentRecord regionStyle,
                                                                  Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getAisIndexableFields();

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getAisSearchableFields();
}
