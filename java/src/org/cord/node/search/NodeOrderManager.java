package org.cord.node.search;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.ImmutableSet;

/**
 * Class to manage a collection of NodeOrder objects.
 */
public class NodeOrderManager
{
  private final Map<String, Map<String, NodeOrder>> __nodeOrderListings =
      new HashMap<String, Map<String, NodeOrder>>();

  private final Map<String, NodeOrder> __nodeOrders = new HashMap<String, NodeOrder>();

  public NodeOrder register(String listingName,
                            NodeOrder nodeOrder)
  {
    synchronized (__nodeOrderListings) {
      if (listingName != null) {
        Map<String, NodeOrder> map = __nodeOrderListings.get(listingName);
        if (map == null) {
          map = new HashMap<String, NodeOrder>();
          __nodeOrderListings.put(listingName, map);
        }
        synchronized (map) {
          map.put(nodeOrder.getName(), nodeOrder);
        }
      }
      synchronized (__nodeOrders) {
        return __nodeOrders.put(nodeOrder.getName(), nodeOrder);
      }
    }
  }

  public NodeOrder getNodeOrder(String name)
  {
    synchronized (__nodeOrders) {
      return __nodeOrders.get(name);
    }
  }

  public Iterator<NodeOrder> getNodeOrders(String listingName)
  {
    synchronized (__nodeOrderListings) {
      Map<String, NodeOrder> map = __nodeOrderListings.get(listingName);
      if (map == null) {
        return ImmutableSet.<NodeOrder> of().iterator();
      } else {
        return map.values().iterator();
      }
    }
  }

  public Iterator<NodeOrder> getNodeOrders()
  {
    return __nodeOrders.values().iterator();
  }
}