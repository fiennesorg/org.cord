package org.cord.node.search;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.node.search.AdvancedSearch.AsDataItem;
import org.cord.sql.ConnectionFacade;
import org.cord.util.CollectionUtil;
import org.cord.util.DebugConstants;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.LogicException;
import org.cord.util.Named;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

public abstract class AdvancedAbstractSearch<T>
  extends EnglishNamedImpl
  implements AdvancedSearchPlugin<T>
{
  private final NodeManager __nodeManager;

  private final String __transactionPassword;

  private AdvancedSearch _advancedSearch;

  public AdvancedAbstractSearch(String name,
                                String englishName,
                                NodeManager nodeManager,
                                String transactionPassword)
  {
    super(name,
          englishName);
    __nodeManager = nodeManager;
    __transactionPassword = transactionPassword;
  }

  protected final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  protected final String getTransactionPassword()
  {
    return __transactionPassword;
  }

  @Override
  public final synchronized void init(AdvancedSearch advancedSearch)
  {
    if (DebugConstants.DEBUG_ADVANCEDSEARCH) {
      DebugConstants.DEBUG_OUT.println(this + ".init(" + advancedSearch + ")");
    }
    Preconditions.checkNotNull(advancedSearch, "advancedSearch");
    if (_advancedSearch != null) {
      throw new IllegalStateException("Cannot register " + this + " on " + advancedSearch
                                      + " when it is already registered on " + _advancedSearch);
    }
    _advancedSearch = advancedSearch;
  }

  protected final AdvancedSearch getAdvancedSearch()
  {
    return _advancedSearch;
  }

  protected final FieldsIndex buildPersistentFieldsIndex(PersistentRecord node)
  {
    FieldsIndex fieldsIndex = new FieldsIndex();
    RecordSource asDataItemsQuery = getAsDataItem().getDataItems(node);
    // TODO: decide about setMayPreCacheDate(boolean) and RecordSource
    // asDataItemsQuery.setMayPreCacheData(true);
    for (PersistentRecord asDataItem : asDataItemsQuery) {
      DataItems dataItems = getDataItems(fieldsIndex,
                                         asDataItem.compInt(AsDataItem.ASCONTENTCOMPILERFIELD_ID),
                                         asDataItem.compInt(AsDataItem.REGIONSTYLE_ID),
                                         true);
      dataItems.put(getAsDataItem().getDataField(asDataItem), asDataItem.getId());
    }
    return fieldsIndex;
  }

  @Override
  public final void deleteNodeIndex(PersistentRecord node)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    FieldsIndex persFieldsIndex = buildPersistentFieldsIndex(node);
    deleteDataItems(persFieldsIndex);
  }

  @Override
  public final void rebuildNode(PersistentRecord node)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    FieldsIndex persFieldsIndex = buildPersistentFieldsIndex(node);
    FieldsIndex liveFieldsIndex = buildLiveFieldsIndex(node);
    stripDuplicates(persFieldsIndex, liveFieldsIndex);
    deleteDataItems(persFieldsIndex);
    insertDataItems(liveFieldsIndex, node.getId());
  }

  protected final DataItems getDataItems(FieldsIndex dataIndex,
                                         int asContentCompilerField_id,
                                         int regionStyle_id,
                                         boolean autoCreate)
  {
    RegionStylesIndex regionStyles = dataIndex.get(asContentCompilerField_id);
    if (regionStyles == null) {
      if (autoCreate) {
        regionStyles = new RegionStylesIndex();
        dataIndex.put(asContentCompilerField_id, regionStyles);
        DataItems dataItems = new DataItems();
        regionStyles.put(regionStyle_id, dataItems);
        return dataItems;
      } else {
        return null;
      }
    }
    DataItems dataItems = regionStyles.get(regionStyle_id);
    if (dataItems == null) {
      if (autoCreate) {
        dataItems = new DataItems();
        regionStyles.put(regionStyle_id, dataItems);
      } else {
        return null;
      }
    }
    return dataItems;
  }

  protected final FieldsIndex buildLiveFieldsIndex(PersistentRecord node)
      throws MirrorNoSuchRecordException
  {
    if (DebugConstants.DEBUG_ADVANCEDSEARCH_REBUILD) {
      DebugConstants.method(this, "buildLiveFieldsIndex", node);
    }
    FieldsIndex fieldsIndex = new FieldsIndex();
    for (PersistentRecord regionStyle : node.comp(Node.NODESTYLE).opt(NodeStyle.REGIONSTYLES)) {
      if (!getAdvancedSearch().isExemptRegionStyle(node, regionStyle)) {
        ContentCompiler contentCompiler =
            getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
        buildLiveFieldsIndex(node, fieldsIndex, regionStyle, contentCompiler);
      }
    }
    buildLiveFieldsIndex(node, fieldsIndex, null, getAdvancedSearch().getNodeStub());
    return fieldsIndex;
  }

  /**
   * Add data to the supplied FieldsIndex with any further indexing information that this indexing
   * module can supply for the given (node,regionStyle).
   * 
   * @param node
   * @param nodeIndex
   *          The index of the node as it stands so far. This will not be null and may already
   *          contain data from other search implementations or regionStyles.
   * @param regionStyle
   *          The RegionStyle that binds the ContentCompiler that implements the appropriate
   *          XxxSearchable interface to the given Node. If it is the core node properties that are
   *          being indexed then this will be null.
   * @param named
   *          Either the ContentCompiler that is referenced by the regionStyle, or the NodeStub if
   *          it is the node itself that is being indexed. This list may be extended with further
   *          elements in the future, but they will all implement Named.
   * @throws MirrorNoSuchRecordException
   */
  protected abstract void buildLiveFieldsIndex(PersistentRecord node,
                                               FieldsIndex nodeIndex,
                                               PersistentRecord regionStyle,
                                               Named named)
      throws MirrorNoSuchRecordException;

  protected final void deleteDataItems(FieldsIndex nodeIndex)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    if (nodeIndex != null && nodeIndex.size() > 0) {
      Transaction transaction =
          getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                     Transaction.TRANSACTION_DELETE,
                                                     getTransactionPassword(),
                                                     "AdvancedAbstractSearch.deleteDataItems");
      for (IntIterator i = nodeIndex.getAsContentCompilerFieldIds().iterator(); i.hasNext();) {
        int fieldId = i.nextInt();
        RegionStylesIndex regionStyles = nodeIndex.get(fieldId);
        for (IntIterator j = regionStyles.getRegionStyleIds().iterator(); j.hasNext();) {
          int regionStyleId = j.nextInt();
          DataItems valueMap = regionStyles.get(regionStyleId);
          for (IntIterator k = valueMap.values().iterator(); k.hasNext();) {
            int id = k.nextInt();
            if (id > 0) {
              transaction.delete(getAsDataItem().getTable().getRecord(id));
            }
          }
        }
      }
      transaction.commit();
    }
  }

  protected final void insertDataItems(FieldsIndex dataIndex,
                                       int nodeId)
      throws MirrorFieldException, MirrorInstantiationException
  {
    // TODO: AdvancedAbstractSearch should share PreparedStatement and should close it after use.
    if (dataIndex != null && dataIndex.size() > 0) {
      ConnectionFacade connectionFacade =
          getNodeManager().getDb().getThreadConnectionFacade().get();
      Connection connection = null;
      PreparedStatement preparedStatement = null;
      Integer nodeIdInteger = Integer.valueOf(nodeId);
      try {
        try {
          final Table asDataItemTable = getAsDataItem().getTable();
          final FieldKey<T> dataFieldName = getAsDataItem().getDataFieldName();
          connection = connectionFacade.checkOutConnection();
          preparedStatement = asDataItemTable.prepareInsertStatement(connection);
          for (Integer asCcfId : dataIndex.keySet()) {
            RegionStylesIndex regionStyles = dataIndex.get(asCcfId.intValue());
            for (Integer regionStyleId : regionStyles.keySet()) {
              DataItems valueMap = regionStyles.get(regionStyleId.intValue());
              for (T value : valueMap.keySet()) {
                TransientRecord asDataItem = asDataItemTable.createTransientRecord();
                asDataItem.setField(AsDataItem.NODE_ID, nodeIdInteger);
                asDataItem.setField(AsDataItem.ASCONTENTCOMPILERFIELD_ID, asCcfId);
                asDataItem.setField(AsDataItem.REGIONSTYLE_ID, regionStyleId);
                asDataItem.setField(dataFieldName, value);
                asDataItem.makePersistent(null, preparedStatement, false);
              }
            }
          }
        } finally {
          preparedStatement.close();
          connectionFacade.checkInWorkingConnection(connection);
        }
      } catch (Exception sqlEx) {
        throw new LogicException("Decide what to do with me?", sqlEx);
      }
    }
  }

  public abstract AdvancedSearch.AsDataItem<T> getAsDataItem();

  protected final void stripDuplicates(FieldsIndex persDataIndex,
                                       FieldsIndex liveDataIndex)
  {
    for (IntIterator i = persDataIndex.getAsContentCompilerFieldIds().iterator(); i.hasNext();) {
      int persAsCcf = i.nextInt();
      RegionStylesIndex liveRegionStyles = liveDataIndex.get(persAsCcf);
      if (liveRegionStyles != null) {
        RegionStylesIndex persRegionStyles = persDataIndex.get(persAsCcf);
        for (IntIterator j = persRegionStyles.getRegionStyleIds().iterator(); j.hasNext();) {
          int persRegionStyleId = j.nextInt();
          DataItems liveWordHashs = liveRegionStyles.get(persRegionStyleId);
          if (liveWordHashs != null) {
            DataItems persWordHashs = persRegionStyles.get(persRegionStyleId);
            CollectionUtil.removeIntersection(persWordHashs.keySet(), liveWordHashs.keySet());
          }
        }
      }
    }
  }

  /**
   * Map that stores the relationship between an item of data of class T (the key) and the id (the
   * value) of the appropriate AsDataItem record that represents this value. If this is representing
   * a set of live data then the ids may well be null as they will not be mapped onto a persistent
   * stored version yet.
   */
  public class DataItems
    extends Object2IntOpenHashMap<T>
  {
    private static final long serialVersionUID = 4802551363630775103L;

    public static final int ID_PENDING = -1;
    public static final int ID_MISSING = 0;

    public DataItems()
    {
      super();
      defaultReturnValue(ID_MISSING);
    }
  }

  /**
   * Map that relates the id of a RegionStyle with the DataItems that are associated with this
   * RegionStyle. This is essentially a mapping of (AsContentCompilerField.id --> (T -->
   * AsDataItem.id)) or (AsContentCompilerField.id --> DataItems)
   * 
   * @see DataItems
   */
  public class RegionStylesIndex
    extends Int2ObjectOpenHashMap<DataItems>
  {
    private static final long serialVersionUID = 2889054368640075427L;

    public IntSet getRegionStyleIds()
    {
      return keySet();
    }
  }

  /**
   * Map that relates the id of an AsContentCompilerField with the RegionStyles data about
   * information for each of the RegionStyle items that implement this field on the current Node.
   * This is essentially a mapping of (RegionStyle.id --> (AsContentCompilerField.id --> (T -->
   * AsDataItem.id))) or (RegionStyle.id --> RegionStylesIndex)
   * 
   * @see RegionStylesIndex
   */
  public class FieldsIndex
    extends Int2ObjectOpenHashMap<RegionStylesIndex>
  {
    private static final long serialVersionUID = -3132174256993174375L;

    public IntSet getAsContentCompilerFieldIds()
    {
      return keySet();
    }
  }
}
