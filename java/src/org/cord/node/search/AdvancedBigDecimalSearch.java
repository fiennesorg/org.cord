package org.cord.node.search;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.BigDecimalField;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;
import org.cord.util.Named;
import org.cord.util.Shutdownable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

/**
 * Implementation of AdvancedSearchPlugin that is responsible for the indexing of integer data.
 */
public class AdvancedBigDecimalSearch
  extends AdvancedAbstractSearch<BigDecimal>
  implements Shutdownable, DbInitialisers
{
  public static final String NAME = "advancedBigDecimalSearch";

  private final AsBigDecimal __asBigDecimal;

  public AdvancedBigDecimalSearch(NodeManager nodeManager,
                                  String transactionPassword)
  {
    super(NAME,
          "BigDecimal Search",
          nodeManager,
          transactionPassword);
    __asBigDecimal = new AsBigDecimal();
  }

  @Override
  protected void buildLiveFieldsIndex(PersistentRecord node,
                                      FieldsIndex dataIndex,
                                      PersistentRecord regionStyle,
                                      Named named)
      throws MirrorNoSuchRecordException
  {
    int regionStyleId = regionStyle == null ? 0 : regionStyle.getId();
    if (named instanceof AdvancedBigDecimalSearchable) {
      AdvancedBigDecimalSearchable searchable = (AdvancedBigDecimalSearchable) named;
      Map<String, Set<BigDecimal>> allValues =
          searchable.getIndexableBigDecimalValues(node, regionStyle, null);
      if (allValues != null) {
        for (String fieldName : allValues.keySet()) {
          PersistentRecord asCcf =
              getAdvancedSearch().getAsContentCompilerField().getInstance(named, fieldName, null);
          Set<BigDecimal> valueSet = allValues.get(fieldName);
          if (valueSet != null) {
            DataItems dataItems = getDataItems(dataIndex, asCcf.getId(), regionStyleId, true);
            for (BigDecimal value : valueSet) {
              dataItems.put(value, DataItems.ID_MISSING);
            }
          }
        }
      }
    }
  }

  public AsBigDecimal getAsBigDecimal()
  {
    return __asBigDecimal;
  }

  @Override
  public boolean accepts(Named named)
  {
    return named instanceof AdvancedBigDecimalSearchable;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedBigDecimalSearchable) named).getIndexableBigDecimalFields();
    }
    return ImmutableList.of();
  }

  @Override
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedBigDecimalSearchable) named).getSearchableBigDecimalFields();
    }
    return ImmutableList.of();
  }

  @Override
  public IdList nodeSearch(Gettable gettable,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug)
  {
    Collection<?> searchTerms =
        gettable.getValues(AdvancedSearch.createExplicitKey(searchName, "searchTerms"));
    Query baseQuery =
        getAdvancedSearch().getBaseQuery(gettable, searchName, getAsDataItem(), debug);
    if (searchTerms.size() > 0) {
      Iterator<?> i = searchTerms.iterator();
      while (i.hasNext()) {
        try {
          String filter =
              " and (AsFloatHash.FloatHash=" + Float.parseFloat((String) i.next()) + ")";
          Query bigDecimalQuery =
              Query.transformQuery(baseQuery, filter, Query.TYPE_EXPLICIT, null);
          if (currentMatches == null) {
            currentMatches = bigDecimalQuery.getIdList();
          } else {
            currentMatches = bigDecimalQuery.intersection(currentMatches);
          }
          if (currentMatches.size() == 0) {
            return currentMatches;
          }
        } catch (NumberFormatException nfEx) {
          // ignore this as it is not a valid searchable number
        }
      }
    }
    return currentMatches;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    return Iterators.<DbInitialiser> singletonIterator(__asBigDecimal);
  }

  public static Set<BigDecimal> addIndexableValue(BigDecimal value,
                                                  Set<BigDecimal> existingValues)
  {
    if (value != null) {
      existingValues = existingValues == null ? new HashSet<BigDecimal>() : existingValues;
      existingValues.add(value);
    }
    return existingValues;
  }

  public static Map<String, Set<BigDecimal>> addIndexableValue(String fieldName,
                                                               BigDecimal fieldValue,
                                                               Map<String, Set<BigDecimal>> existingValues)
  {
    Map<String, Set<BigDecimal>> map =
        existingValues == null ? new HashMap<String, Set<BigDecimal>>() : existingValues;
    synchronized (map) {
      Set<BigDecimal> values = map.get(fieldName);
      map.put(fieldName, addIndexableValue(fieldValue, values));
    }
    return map;
  }

  public static Map<String, Set<BigDecimal>> addIndexableValue(String fieldName,
                                                               TransientRecord source,
                                                               Map<String, Set<BigDecimal>> existingValues)
  {
    return addIndexableValue(fieldName, (BigDecimal) source.get(fieldName), existingValues);
  }

  public static Map<String, Set<BigDecimal>> addIndexableValues(Iterator<String> fieldNames,
                                                                TransientRecord source,
                                                                Map<String, Set<BigDecimal>> existingValues)
  {
    while (fieldNames.hasNext()) {
      existingValues = addIndexableValue(fieldNames.next(), source, existingValues);
    }
    return existingValues;
  }

  @Override
  public AdvancedSearch.AsDataItem<BigDecimal> getAsDataItem()
  {
    return __asBigDecimal;
  }

  public static class AsBigDecimal
    extends AdvancedSearch.AsDataItem<BigDecimal>
    implements DbInitialiser, TableWrapper
  {
    public final static String TABLENAME = "AsBigDecimal";

    public final static ObjectFieldKey<BigDecimal> VALUE =
        ObjectFieldKey.create("value", BigDecimal.class);

    private AsBigDecimal()
    {
      super(TABLENAME);
    }

    @Override
    public void init(Db db,
                     String transactionPassword,
                     Table asBigDecimal)
        throws MirrorTableLockedException
    {
      asBigDecimal.addField(new BigDecimalField(VALUE, "Indexed value"));
    }

    @Override
    public FieldKey<BigDecimal> getDataFieldName()
    {
      return VALUE;
    }

    @Override
    public BigDecimal getDataField(PersistentRecord asDataItem)
    {
      return asDataItem.opt(getDataFieldName());
    }
  }
}