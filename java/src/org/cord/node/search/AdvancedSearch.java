package org.cord.node.search;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.Dependencies;
import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.Query;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.node.Acl;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeFlagFactory;
import org.cord.node.NodeManager;
import org.cord.node.NodeManagerFactory;
import org.cord.node.NodePathFilter;
import org.cord.node.NodeRegionStyleFlagFactory;
import org.cord.node.NodeTableWrapper;
import org.cord.node.RegionStyle;
import org.cord.node.ReloadSuccessOperation;
import org.cord.node.cc.TableContentCompiler;
import org.cord.sql.SqlUtil;
import org.cord.task.Task;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.Named;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.cord.util.Shutdownable;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;

import it.unimi.dsi.fastutil.ints.IntListIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Management class that is responsible for holding the various components of the AdvancedSearch
 * system and the bindings between them. This needs to have init() invoked on it before it will be
 * usable. The normal place to do this would be during the NodeManagerFactory.lock() implementation.
 * 
 * @see #init()
 * @see NodeManagerFactory#lock()
 */
public class AdvancedSearch
  implements Shutdownable, DbInitialisers
{
  public final static String NODE_IDS = "node_ids";

  public static final String KEY_PREFIX = "AS_";
  public static final String KEY_SEPARATOR = "_";

  /**
   * parameter that contains a comma separated list of the names of all the search types that will
   * comprise a given search request.
   */
  public static final String AS_SEARCHNAMES = KEY_PREFIX + "SearchNames";

  /**
   * The name of the search specific parameter that is used to resolve the AdvancedSearchPlugin to
   * be used in a particular request. This will be of the form of AS_&lt;searchName&gt;_pluginName
   * 
   * @see #getValue(Gettable, String, String, PrintWriter)
   */
  public static final String PLUGINNAME = "pluginName";

  public static final String AS_PLUGINNAME = KEY_PREFIX + PLUGINNAME;

  public static final String AS_PATHHEADER = "AS_pathHeader";
  public static final String AS_PATHFOOTER = "AS_pathFooter";
  public static final String AS_MUSTMATCHHEADER = "AS_mustMatchHeader";
  public static final String AS_MUSTMATCHFOOTER = "AS_mustMatchFooter";

  public static final String AS_NODESTYLEIDS = KEY_PREFIX + "nodeStyleIds";

  public static final String AS_NODEORDER = KEY_PREFIX + "nodeOrder";

  /**
   * Config variable that lets you set whether or not return Nodes satisfy
   * {@link Node#ISPATHPUBLISHED}
   */
  public static final String AS_ISPUBLISHED = "AS_isPublished";

  private final AsContentCompiler __asContentCompiler;

  private final AsContentCompilerField __asContentCompilerField;

  private final Map<String, AdvancedSearchPlugin<?>> __advancedSearchPlugins =
      new HashMap<String, AdvancedSearchPlugin<?>>();

  private final Map<String, AdvancedSearchPlugin<?>> __unmodifiableAdvancedSearchPlugins =
      Collections.unmodifiableMap(__advancedSearchPlugins);

  private final NodeStub __nodeStub = new NodeStub();

  private NodeManager _nodeManager;

  private final NodeOrderManager __nodeOrderManager = new NodeOrderManager();

  private boolean _shouldRebuild = true;

  private final IntSet __exemptNodeStyleIds = new IntOpenHashSet();

  private final Set<NodeFlagFactory> __dynamicNodeExemptions = new HashSet<NodeFlagFactory>();

  private final IntSet __exemptRegionStyleIds = new IntOpenHashSet();
  private final Set<NodeRegionStyleFlagFactory> __dynamicRegionStyleExceptions = Sets.newHashSet();

  private final List<PostSearchPlugin> __postSearchPlugins = new ArrayList<PostSearchPlugin>();

  private boolean _requiresDebugTrace = DebugConstants.DEBUG_ADVANCEDSEARCH;

  private final List<RebuildTrigger> __rebuildTriggers = new ArrayList<RebuildTrigger>();

  private final RebuildThread __rebuildThread;

  public static final String ORDER_NODE_TITLE = "Node_title";
  public static final String ORDER_NODE_TITLE_DESC = "Node_title_desc";
  public static final String ORDER_NODE_MODIFICATIONTIME = "Node_modificationTime";
  public static final String ORDER_NODE_MODIFICATIONTIME_DESC = "Node_modificationTime_desc";
  public static final String ORDER_NODE_PATH = "path";

  public AdvancedSearch(NodeManager nodeMgr,
                        String pwd)
  {
    _nodeManager = nodeMgr;
    __asContentCompiler = new AsContentCompiler(this);
    __asContentCompilerField = new AsContentCompilerField(this);
    addAdvancedSearchPlugin(new AdvancedWordSearch(nodeMgr, pwd));
    addAdvancedSearchPlugin(new AdvancedDateSearch(nodeMgr, pwd));
    addAdvancedSearchPlugin(new AdvancedBooleanSearch(nodeMgr, pwd));
    addAdvancedSearchPlugin(new AdvancedIntegerSearch(nodeMgr, pwd));
    addAdvancedSearchPlugin(new AdvancedBigDecimalSearch(nodeMgr, pwd));
    // addAdvancedSearchPlugin(new WordDensityIndex("wordDensityIndex",
    // "Word Density Index",
    // nodeMgr,
    // 10000,
    // 9));
    getNodeOrderManager().register("Node",
                                   new PrimaryNodeOrder(ORDER_NODE_TITLE,
                                                        "Page Title",
                                                        nodeMgr.getDb(),
                                                        "Node.title"));
    getNodeOrderManager().register("Node",
                                   new PrimaryNodeOrder(ORDER_NODE_TITLE_DESC,
                                                        "Page Title Descending",
                                                        nodeMgr.getDb(),
                                                        "Node.title desc"));
    getNodeOrderManager().register("Node",
                                   new PrimaryNodeOrder(ORDER_NODE_MODIFICATIONTIME,
                                                        "Modification Time, Oldest First",
                                                        nodeMgr.getDb(),
                                                        "Node.modificationTime"));
    getNodeOrderManager().register("Node",
                                   new PrimaryNodeOrder(ORDER_NODE_MODIFICATIONTIME_DESC,
                                                        "Modification Time, Youngest First",
                                                        nodeMgr.getDb(),
                                                        "Node.modificationTime desc"));
    getNodeOrderManager().register("ContactItem",
                                   new AccNodeOrder("ContactItem_slug",
                                                    "Contact Item Slug",
                                                    nodeMgr.getDb(),
                                                    "ContactItem",
                                                    "ContactItem.slug"));
    getNodeOrderManager().register("Node",
                                   new PathNodeOrder(ORDER_NODE_PATH, "Order by URL", this));
    __rebuildThread = new RebuildThread(nodeMgr, this, null);
    try {
      __rebuildThread.readPendingNodeIds();
    } catch (IOException e) {
      System.err.println("Error reading previously saved pendingNodeIds");
      e.printStackTrace();
    }
    nodeMgr.getNodeRequestSegmentManager()
           .register(new RebuildIndex("rebuildAdvancedSearchIndex",
                                      nodeMgr,
                                      ReloadSuccessOperation.DEFAULT_RELOAD,
                                      Acl.COREADMINONLY,
                                      "You do not have permission to rebuild the AdvancedSearch index",
                                      this));
  }

  /**
   * Switch on or off the auto-rebuild process in the AdvancedSearch. This is useful for booting
   * processes where there are likely to be multiple successive trigger points on a single item and
   * rebuilding the index on each of these is innefficient. You therefore switch off the rebuilding
   * process until the booting is complete and then trigger a complete rebuild to ensure that the
   * index is in sync.
   */
  public synchronized void setShouldRebuild(boolean shouldRebuild)
  {
    _shouldRebuild = shouldRebuild;
  }

  /**
   * Find out whether or not the rebuilding process is currently on or off on this AdvancedSearch
   * module.
   */
  public synchronized boolean shouldRebuild()
  {
    return _shouldRebuild;
  }

  public NodeOrderManager getNodeOrderManager()
  {
    return __nodeOrderManager;
  }

  public NodeStub getNodeStub()
  {
    return __nodeStub;
  }

  @Override
  public void shutdown()
  {
    __rebuildThread.shutdown();
    _nodeManager = null;
  }

  public NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  public final void addRebuildTrigger(RebuildTrigger trigger)
  {
    Preconditions.checkNotNull(trigger, "trigger");
    __rebuildTriggers.add(trigger);
  }

  public final AdvancedSearchPlugin<?> addAdvancedSearchPlugin(AdvancedSearchPlugin<?> plugin)
  {
    Preconditions.checkNotNull(plugin, "plugin");
    synchronized (__advancedSearchPlugins) {
      return __advancedSearchPlugins.put(plugin.getName(), plugin);
    }
  }

  public Map<String, AdvancedSearchPlugin<?>> getAdvancedSearchPlugins()
  {
    return __unmodifiableAdvancedSearchPlugins;
  }

  public AdvancedSearchPlugin<?> getAdvancedSearchPlugin(String name)
  {
    return getAdvancedSearchPlugins().get(name);
  }

  /**
   * Is there at least one registered AdvancedSearchPlugin that is interested in this
   * ContentCompiler?
   */
  public boolean accepts(Named object)
  {
    if (object == null) {
      return false;
    }
    Iterator<AdvancedSearchPlugin<?>> plugins = getAdvancedSearchPlugins().values().iterator();
    while (plugins.hasNext()) {
      if (plugins.next().accepts(object)) {
        return true;
      }
    }
    return false;
  }

  public AsContentCompiler getAsContentCompiler()
  {
    return __asContentCompiler;
  }

  public AsContentCompilerField getAsContentCompilerField()
  {
    return __asContentCompilerField;
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    List<DbInitialiser> list = new ArrayList<DbInitialiser>();
    list.add(__asContentCompiler);
    list.add(__asContentCompilerField);
    Iterator<AdvancedSearchPlugin<?>> plugins = getAdvancedSearchPlugins().values().iterator();
    while (plugins.hasNext()) {
      AdvancedSearchPlugin<?> plugin = plugins.next();
      Iterator<DbInitialiser> initialisers = plugin.getDbInitialisers();
      while (initialisers.hasNext()) {
        list.add(initialisers.next());
      }
    }
    return list.iterator();
  }

  private void init(AdvancedSearchable advancedSearchable)
      throws MirrorInstantiationException, MirrorFieldException
  {
    if (accepts(advancedSearchable)) {
      getAsContentCompiler().createInstance(advancedSearchable, null);
      Iterator<AdvancedSearchPlugin<?>> plugins = getAdvancedSearchPlugins().values().iterator();
      while (plugins.hasNext()) {
        AdvancedSearchPlugin<?> plugin = plugins.next();
        for (RecordOperationKey<?> field : plugin.getIndexableFields(advancedSearchable)) {
          getAsContentCompilerField().createInstance(advancedSearchable, field.getKeyName(), null);
        }
      }
    }
  }

  private void init(Object object)
      throws MirrorInstantiationException, MirrorFieldException
  {
    if (object instanceof AdvancedSearchable) {
      init((AdvancedSearchable) object);
    }
  }

  public void init()
      throws MirrorInstantiationException, MirrorFieldException
  {
    for (Iterator<ContentCompiler> i =
        getNodeManager().getContentCompilerFactory().getContentCompilers(); i.hasNext();) {
      init(i.next());
    }
    init(__nodeStub);
    Iterator<AdvancedSearchPlugin<?>> plugins = getAdvancedSearchPlugins().values().iterator();
    while (plugins.hasNext()) {
      plugins.next().init(this);
    }
  }

  public void addExemptNodeStyle(int... ids)
  {
    for (int id : ids) {
      __exemptNodeStyleIds.add(id);
    }
  }

  /**
   * @param nodeExemption
   *          Object that is capable of deciding whether a node is exempt from indexing or not. If
   *          the NodeFlagFactory returns true then the Node is exempt and will not be indexed.
   */
  public void addDynamicNodeExemption(NodeFlagFactory nodeExemption)
  {
    if (nodeExemption == null) {
      throw new NullPointerException("nodeFlagFactory");
    }
    __dynamicNodeExemptions.add(nodeExemption);
  }

  public void addDynamicRegionStyleException(NodeRegionStyleFlagFactory nodeRegionStyleException)
  {
    Preconditions.checkNotNull(nodeRegionStyleException, "nodeRegionStyleException");
    __dynamicRegionStyleExceptions.add(nodeRegionStyleException);
  }

  /**
   * Check to see whether or not the node should be exempt from index. A node is exempt if either
   * its NodeStyle is on the list of exempt styles or if a dynamic node exemption has been
   * registered.
   */
  public boolean isExemptNode(TransientRecord node)
  {
    if (!node.is(Node.ISSEARCHABLE)) {
      return true;
    }
    int nodeStyleId = node.compInt(Node.NODESTYLE_ID);
    if (__exemptNodeStyleIds.contains(nodeStyleId)) {
      return true;
    }
    if (__dynamicNodeExemptions.size() != 0) {
      for (Iterator<NodeFlagFactory> i = __dynamicNodeExemptions.iterator(); i.hasNext();) {
        NodeFlagFactory exemption = i.next();
        if (Boolean.TRUE.equals(exemption.getFlag(node))) {
          return true;
        }
      }
    }
    return false;
  }

  public void addExemptRegionStylesById(int... ids)
  {
    for (int id : ids) {
      __exemptRegionStyleIds.add(id);
    }
  }

  /**
   * Add named RegionStyles to the list of non-indexed items with a
   * {@link MirrorNoSuchRecordException} if any records cannot be resolved.
   */
  public void addExemptRegionStylesByName(int nodeStyleId,
                                          String... regionStyleNames)
      throws MirrorNoSuchRecordException
  {
    for (String regionStyleName : regionStyleNames) {
      addExemptRegionStylesById(getNodeManager().getRegionStyle()
                                                .getInstance(nodeStyleId, regionStyleName, null)
                                                .getId());
    }
  }

  /**
   * Add named RegionStyles to the list of non-indexed items and skip any records that cannot be
   * resolved.
   */
  public void addOptExemptRegionStylesByName(int nodeStyleId,
                                             String... regionStyleNames)
  {
    for (String regionStyleName : regionStyleNames) {
      PersistentRecord regionStyle =
          getNodeManager().getRegionStyle().getOptInstance(nodeStyleId, regionStyleName, null);
      if (regionStyle != null) {
        addExemptRegionStylesById(regionStyle.getId());
      }
    }
  }

  public boolean isExemptRegionStyle(PersistentRecord node,
                                     PersistentRecord regionStyle)
  {
    if (__exemptRegionStyleIds.contains(regionStyle.getId())) {
      return true;
    }
    for (NodeRegionStyleFlagFactory nodeRegionStyleException : __dynamicRegionStyleExceptions) {
      if (Boolean.TRUE.equals(nodeRegionStyleException.getFlag(node, regionStyle))) {
        return true;
      }
    }
    return false;
  }

  private void triggerRebuildTriggers(PersistentRecord node,
                                      boolean isExempt)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    for (Iterator<RebuildTrigger> i = __rebuildTriggers.iterator(); i.hasNext();) {
      i.next().postRebuildIndex(node, isExempt);
    }
  }

  /**
   * Inform the indexing thread that the given node should be reindexed at the next convenient
   * moment. The node will probably not have been indexed when the method returns, but this will be
   * scheduled as soon as is appropriate.
   */
  public void rebuildIndex(PersistentRecord node)
  {
    __rebuildThread.rebuildIndex(node);
  }

  public static final long NODE_UPDATE_LATENCY = 250;

  protected void rebuildIndexNow(PersistentRecord node)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    if (DebugConstants.DEBUG_ADVANCEDSEARCH_REBUILD) {
      DebugConstants.method(this, "rebuildIndexNow", node);
    }
    while (System.currentTimeMillis()
           - node.opt(Node.MODIFICATIONTIME).getTime() < NODE_UPDATE_LATENCY) {
      try {
        Thread.sleep(NODE_UPDATE_LATENCY);
      } catch (InterruptedException e) {
      }
    }
    if (isExemptNode(node)) {
      for (AdvancedSearchPlugin<?> plugin : getAdvancedSearchPlugins().values()) {
        plugin.deleteNodeIndex(node);
      }
      triggerRebuildTriggers(node, true);
      return;
    }
    for (AdvancedSearchPlugin<?> plugin : getAdvancedSearchPlugins().values()) {
      plugin.rebuildNode(node);
    }
    triggerRebuildTriggers(node, false);
  }

  /**
   * Rebuild the entire index for all Nodes in the system. This will take some time depending on the
   * size and complexity of the Node deployment. It is also going to have issues if other pages are
   * editing at the same time, so using with caution is advised.
   * 
   * @param task
   *          The optional Task which this rebuild operation should is running under. If it is
   *          defined then status messages will be delivered to it at regular intervals during the
   *          update process.
   * @see #rebuildIndex(PersistentRecord)
   */
  public int rebuildIndex(Task task)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    RecordList nodes = getNodeManager().getDb().getTable(Node.TABLENAME).getRecordList();
    int nodeCount = nodes.size();
    int count = 0;
    for (PersistentRecord node : nodes) {
      if (task != null) {
        if (task.hasStopRequest()) {
          task.setStatusMessage("Stopped after " + count + " of " + nodeCount + " pages processed");
          return count;
        }
        if (count % 100 == 0) {
          task.setStatusMessage("~" + count + " of " + nodeCount + " pages indexed");
        }
      }
      rebuildIndex(node);
      count++;
    }
    if (task != null) {
      String completeMessage = "Rebuilt Index of " + count + " pages";
      task.setStatusMessage(completeMessage);
      task.getLog().println(completeMessage);
    }
    return count;
  }

  public int rebuildIndex()
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    return rebuildIndex((Task) null);
  }

  private final static Pattern __searchNamesSplitter = Pattern.compile(",");

  public void addPostSearchPlugin(PostSearchPlugin plugin)
  {
    if (plugin != null) {
      __postSearchPlugins.add(plugin);
      if (plugin.wantsDebugTrace()) {
        _requiresDebugTrace = true;
      }
    }
  }

  public AdvancedSearchResults nodeSearch(Gettable request,
                                          int userId)
  {
    StringWriter stringWriter = null;
    PrintWriter printWriter = null;
    if (_requiresDebugTrace) {
      stringWriter = new StringWriter();
      printWriter = new PrintWriter(stringWriter);
    }
    IdList nodeIds = nodeSearchInner(request, userId, printWriter);
    String debugTrace = null;
    if (_requiresDebugTrace) {
      debugTrace = stringWriter.toString();
      if (DebugConstants.DEBUG_ADVANCEDSEARCH) {
        DebugConstants.DEBUG_OUT.println(debugTrace);
      }
    }
    AdvancedSearchResults results = new AdvancedSearchResults(this, request, nodeIds, debugTrace);
    for (Iterator<PostSearchPlugin> i = __postSearchPlugins.iterator(); i.hasNext();) {
      i.next().processSearchResults(results);
    }
    return results;
  }

  private IdList nodeSearchInner(Gettable request,
                                 int userId,
                                 PrintWriter debug)
  {
    final Table nodeTable = getNodeManager().getNode().getTable();
    if (debug != null) {
      debug.println(this + ".nodeSearch(" + request + ")");
    }
    String allSearchNames = request.getString(AS_SEARCHNAMES);
    if (debug != null) {
      debug.println(AS_SEARCHNAMES + ":" + allSearchNames);
    }
    IdList nodeIds = null;
    if (allSearchNames != null) {
      String[] splitSearchNames = __searchNamesSplitter.split(allSearchNames);
      for (int i = 0; i < splitSearchNames.length; i++) {
        String searchName = splitSearchNames[i];
        String pluginName = getValue(request, searchName, PLUGINNAME, debug);
        AdvancedSearchPlugin<?> plugin = getAdvancedSearchPlugin(pluginName);
        if (debug != null) {
          debug.println("\n" + searchName + " --> " + plugin);
        }
        if (plugin != null) {
          nodeIds = plugin.nodeSearch(request, searchName, nodeIds, debug);
          if (nodeIds != null && nodeIds.size() == 0) {
            return nodeIds;
          }
        }
      }
    }
    if (nodeIds != null && nodeIds.size() > 0) {
      String in_nodeStyleIds = request.getString(AS_NODESTYLEIDS);
      if (!Strings.isNullOrEmpty(in_nodeStyleIds)) {
        IntSet nodeStyleIds = new IntOpenHashSet();
        for (String in_nodeStyleId : Splitter.on(',')
                                             .trimResults()
                                             .omitEmptyStrings()
                                             .split(in_nodeStyleIds)) {
          nodeStyleIds.add(NumberUtil.toInt(in_nodeStyleId));
        }
        IdList filteredNodeIds = new ArrayIdList(nodeTable);
        final int size = nodeIds.size();
        for (int i = 0; i < size; i++) {
          int id = nodeIds.getInt(i);
          try {
            PersistentRecord node = nodeTable.getRecord(id);
            if (nodeStyleIds.contains(node.compInt(Node.NODESTYLE_ID))) {
              filteredNodeIds.add(id);
            }
          } catch (MirrorNoSuchRecordException mnsrEx) {
            // node has been deleted between match and filtering
            // --> OK to ignore and not put into target results...
          }
        }
        nodeIds = filteredNodeIds;
      }
    }
    if (nodeIds != null && nodeIds.size() > 0) {
      String pathHeader = request.getString(AS_PATHHEADER);
      String pathFooter = request.getString(AS_PATHFOOTER);
      if (!Strings.isNullOrEmpty(pathHeader) || !Strings.isNullOrEmpty(pathFooter)) {
        NodePathFilter filter =
            new NodePathFilter(null,
                               pathHeader,
                               NumberUtil.booleanValue(request.getBoolean(AS_MUSTMATCHHEADER),
                                                       true),
                               pathFooter,
                               NumberUtil.booleanValue(request.getBoolean(AS_MUSTMATCHFOOTER),
                                                       true));
        int size = nodeIds.size();
        IdList filteredResult = new ArrayIdList(nodeTable);
        for (int i = 0; i < size; i++) {
          int id = nodeIds.getInt(i);
          try {
            PersistentRecord node = nodeTable.getRecord(id);
            if (!filter.shouldSkip(node)) {
              filteredResult.add(id);
            }
          } catch (MirrorNoSuchRecordException mnsrEx) {
            // node has been deleted between match and filtering
            // --> OK to ignore and not put into target results...
          }
        }
        nodeIds = filteredResult;
      }
    }
    final Boolean isPublished = request.getBoolean(AS_ISPUBLISHED);
    Predicate<PersistentRecord> isPublishedPredicate = isPublished == null
        ? Predicates.<PersistentRecord> alwaysTrue()
        : new Predicate<PersistentRecord>() {
          @Override
          public boolean apply(PersistentRecord node)
          {
            return isPublished.equals(node.comp(Node.ISPATHPUBLISHED));
          }
        };
    if (nodeIds != null) {
      final Node Node_ = getNodeManager().getNode();
      final ArrayIdList filteredIds = new ArrayIdList(nodeTable, nodeIds.size());
      for (IntListIterator i = nodeIds.iterator(); i.hasNext();) {
        int nodeId = i.nextInt();
        PersistentRecord node = nodeTable.getOptRecord(nodeId);
        if (Node_.isViewable(node, userId) & isPublishedPredicate.apply(node)) {
          filteredIds.add(nodeId);
        }
      }
      nodeIds = filteredIds;
    }
    return nodeIds;
  }

  /**
   * Get a mono-valued item from the NodeRequest, with searchName specific values taking preference
   * over global values.
   */
  public static String getValue(Gettable request,
                                String searchName,
                                String parameterName,
                                PrintWriter debug)
  {
    String explicitKey = createExplicitKey(searchName, parameterName);
    String value = request.getString(explicitKey);
    if (debug != null) {
      debug.println(explicitKey + ":" + value);
    }
    if (value == null) {
      String generalKey = createGeneralKey(parameterName);
      value = request.getString(generalKey);
      if (debug != null) {
        debug.println(generalKey + ":" + value);
      }
    }
    return value;
  }

  public static String createExplicitKey(String searchName,
                                         String parameterName)
  {
    return KEY_PREFIX + searchName + KEY_SEPARATOR + parameterName;
  }

  public static String createGeneralKey(String parameterName)
  {
    return KEY_PREFIX + parameterName;
  }

  /**
   * Get a multi-valued item from the NodeRequest, with searchName specific values taking preference
   * over global values. If the NodeRequest contains a single definition of the value, then check to
   * see whether or not this could be a comma-separated list of values and utilise this if so
   * required. Note that whitespace around the commas is preserved via this method. This therefore
   * gives us the choice of using genuine multi-value inputs like multi-selects and radio boxes, or
   * compound multi-value inputs like select boxes with comma separated lists of values defined for
   * each option.
   * 
   * @param splitToken
   *          The value that should be utilised to split single values into sub-values. For example
   *          a value of "," will split comma separated lists into sub values. A value of null or ""
   *          will not split values.
   * @return List of Strings. Never null (although sometimes empty).
   */
  public static Collection<String> getValues(Gettable request,
                                             String searchName,
                                             String parameterName,
                                             String splitToken,
                                             PrintWriter debug)
  {
    String explicitKey = AdvancedSearch.createExplicitKey(searchName, parameterName);
    Collection<String> values = GettableUtils.getValuesAsStrings(request, explicitKey);
    if (debug != null) {
      debug.println(explicitKey + ":" + values);
    }
    if (values == null) {
      String generalKey = AdvancedSearch.createGeneralKey(parameterName);
      values = GettableUtils.getValuesAsStrings(request, generalKey);
      if (debug != null) {
        debug.println(generalKey + ":" + values);
      }
    }
    if (values == null) {
      return Collections.emptyList();
    }
    if (values.size() == 1) {
      String value = StringUtils.toString(values.iterator().next());
      if (value.length() > 0) {
        if (!Strings.isNullOrEmpty(splitToken)) {
          String[] splitArray = value.split(splitToken);
          if (splitArray.length > 1) {
            List<String> splitList = new ArrayList<String>();
            for (int i = 0; i < splitArray.length; i++) {
              if (!Strings.isNullOrEmpty(splitArray[i])) {
                splitList.add(splitArray[i]);
              }
            }
            if (debug != null) {
              debug.println(" --> " + splitList);
            }
            return splitList;
          }
        }
      }
    }
    if (debug != null) {
      debug.println(" --> " + values);
    }
    return values;
  }

  public Query getBaseQuery(Gettable gettable,
                            String searchName,
                            AsDataItem<?> asDataItem,
                            PrintWriter debug)
  {
    return getBaseQuery(gettable, searchName, asDataItem.getTableName(), debug);
  }

  public static final String AS_REGIONSTYLENAMES = "regionStyleNames";
  public static final String AS_NODESTYLE_ID = "nodeStyle_id";
  public static final String AS_CCNAME = "ccName";
  public static final String AS_CCFIELDNAME = "ccFieldName";

  /**
   * Calculate the base query encapsulated in the given NodeRequest. This doesn't include any
   * filtering based around the implementation of AsDataItem but is instead intended to be used as a
   * base query for creation of multiple cloned queries via the QueryTransform set of objects.
   */
  public Query getBaseQuery(Gettable gettable,
                            String searchName,
                            String adiTablename,
                            PrintWriter debug)
  {
    StringBuilder filter = new StringBuilder();
    List<Table> tables = new ArrayList<Table>();
    Db db = getNodeManager().getDb();
    // String adiTablename = asDataItem.getTableName();
    tables.add(db.getTable(adiTablename));
    filter.append("(Node.id=").append(adiTablename).append(".node_id)");
    Collection<String> regionStyleNames =
        getValues(gettable, searchName, AS_REGIONSTYLENAMES, ",", debug);
    if (regionStyleNames.size() > 0) {
      tables.add(db.getTable(RegionStyle.TABLENAME));
      filter.append(" and (RegionStyle.id=").append(adiTablename).append(".regionStyle_id)");
      filter.append(" and (");
      Iterator<String> i = regionStyleNames.iterator();
      while (i.hasNext()) {
        filter.append("(RegionStyle.name=");
        SqlUtil.toSafeSqlString(filter, i.next());
        filter.append(")");
        if (i.hasNext()) {
          filter.append(" or ");
        }
      }
      filter.append(")");
    }
    Collection<String> nodeStyleIds = getValues(gettable, searchName, AS_NODESTYLE_ID, ",", debug);
    if (nodeStyleIds.size() > 0) {
      filter.append(" and (Node.nodeStyle_id in (");
      Iterator<String> i = nodeStyleIds.iterator();
      while (i.hasNext()) {
        filter.append(i.next());
        if (i.hasNext()) {
          filter.append(",");
        }
      }
      filter.append("))");
    }
    Collection<String> ccNames = getValues(gettable, searchName, AS_CCNAME, ",", debug);
    Collection<String> ccFieldNames = getValues(gettable, searchName, AS_CCFIELDNAME, ",", debug);
    if (ccNames.size() > 0 || ccFieldNames.size() > 0) {
      tables.add(getAsContentCompilerField().getTable());
      filter.append(" and (AsContentCompilerField.id=")
            .append(adiTablename)
            .append(".asContentCompilerField_id)");
      if (ccFieldNames.size() > 0) {
        filter.append(" and (");
        Iterator<String> i = ccFieldNames.iterator();
        while (i.hasNext()) {
          filter.append("(AsContentCompilerField.fieldName=");
          SqlUtil.toSafeSqlString(filter, i.next());
          filter.append(")");
          if (i.hasNext()) {
            filter.append(" or ");
          }
        }
        filter.append(")");
      }
      if (ccNames.size() > 0) {
        tables.add(getAsContentCompiler().getTable());
        filter.append(" and (AsContentCompiler.id=AsContentCompilerField.asContentCompiler_id)");
        filter.append(" and (");
        Iterator<String> i = ccNames.iterator();
        while (i.hasNext()) {
          filter.append("(AsContentCompiler.name=");
          SqlUtil.toSafeSqlString(filter, i.next());
          filter.append(")");
          if (i.hasNext()) {
            filter.append(" or ");
          }
        }
        filter.append(")");
      }
    }
    // appendUnpublishedNodeFilter(filter);
    return db.getTable(Node.TABLENAME)
             .getQuery(null,
                       filter.toString(),
                       TransientRecord.FIELD_ID.toString(),
                       "Node.id",
                       -1,
                       null,
                       Query.toArray(tables));
  }

  /**
   * Definitions of the AsContentCompiler Table that holds information about one ContentCompilers
   * relationship with the AdvancedSearch framework.
   */
  public static class AsContentCompiler
    extends NodeTableWrapper
  {
    public final static String TABLENAME = "AsContentCompiler";

    public final static ObjectFieldKey<String> NAME = StringField.createKey("name");

    public final static ObjectFieldKey<String> BACKENDTABLENAME =
        StringField.createKey("backendTableName");

    public final static RecordOperationKey<ContentCompiler> CONTENTCOMPILER =
        RecordOperationKey.create("contentCompiler", ContentCompiler.class);

    public final static RecordOperationKey<Table> BACKENDTABLE =
        RecordOperationKey.create("backendTable", Table.class);

    private final AdvancedSearch __advancedSearch;

    private AsContentCompiler(AdvancedSearch advancedSearch)
    {
      super(advancedSearch.getNodeManager(),
            TABLENAME);
      __advancedSearch = advancedSearch;
    }

    @Override
    public void initTable(final Db db,
                          final String pwd,
                          final Table table)
        throws MirrorTableLockedException
    {
      table.addField(new StringField(NAME, "Name"));
      table.addField(new StringField(BACKENDTABLENAME, "Backend Table name"));
      table.addRecordOperation(new SimpleRecordOperation<ContentCompiler>(CONTENTCOMPILER) {
        @Override
        public ContentCompiler getOperation(TransientRecord asContentCompiler,
                                            Viewpoint viewpoint)
        {
          return getContentCompiler(asContentCompiler);
        }
      });
      table.addRecordOperation(new SimpleRecordOperation<Table>(BACKENDTABLE) {
        @Override
        public Table getOperation(TransientRecord asContentCompiler,
                                  Viewpoint viewpoint)
        {
          return getBackendTable(asContentCompiler);
        }
      });
      Table nodeTable = db.getTable(Node.TABLENAME);
      nodeTable.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_TRANSACTION_COMMITTED,
                                                         false,
                                                         Node.TABLENAME) {
        @Override
        public void trigger(PersistentRecord node,
                            Transaction transaction,
                            int state)
            throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException,
            MirrorFieldException, MirrorInstantiationException
        {
          if (transaction.getType() == Transaction.TRANSACTION_UPDATE
              && transaction.isUpdated() & __advancedSearch.shouldRebuild()) {
            __advancedSearch.rebuildIndex(node);
          }
        }
      });
      nodeTable.addPostInstantiationTrigger(new PostInstantiationTrigger() {
        @Override
        public void triggerPost(PersistentRecord node,
                                Gettable params)
            throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException,
            MirrorFieldException, MirrorInstantiationException
        {
          if (__advancedSearch.shouldRebuild() & node.is(Node.ISPUBLISHED)) {
            __advancedSearch.rebuildIndex(node);
          }
        }

        @Override
        public void releasePost(PersistentRecord node,
                                Gettable params)
        {
        }

        @Override
        public void rollbackPost(PersistentRecord node,
                                 Gettable params)
        {
        }

        @Override
        public void shutdown()
        {
        }
      });
    }

    public Table getBackendTable(TransientRecord asContentCompiler)
    {
      return getNodeManager().getDb().getTable(asContentCompiler.opt(BACKENDTABLENAME));
    }

    /**
     * Resolve the AsContentCompiler record into the appropriate ContentCompiler that it represents.
     */
    public ContentCompiler getContentCompiler(TransientRecord asContentCompiler)
    {
      return getNodeManager().getContentCompilerFactory()
                             .getContentCompiler(asContentCompiler.opt(NAME));
    }

    public RecordSource search(String name)
    {
      return TableStringMatcher.search(getTable(), NAME, name, null);
    }

    public PersistentRecord getInstance(Named named,
                                        Transaction transaction)
        throws MirrorNoSuchRecordException
    {
      return RecordSources.getFirstRecord(search(named.getName()), transaction);
    }

    public PersistentRecord createInstance(Named named,
                                           Transaction transaction)
        throws MirrorInstantiationException, MirrorFieldException
    {
      RecordSource query = search(named.getName());
      RecordList records = query.getRecordList();
      switch (records.size()) {
        case 0:
          TransientRecord record = getTable().createTransientRecord();
          record.setField(NAME, named.getName());
          if (named instanceof TableContentCompiler) {
            record.setField(BACKENDTABLENAME,
                            ((TableContentCompiler) named).getInstanceTable().getName());
          } else if (named instanceof NodeStub) {
            record.setField(BACKENDTABLENAME, Node.TABLENAME);
          }
          return record.makePersistent(null);
        case 1:
          return records.get(0);
        default:
          if (DebugConstants.DEBUG_ADVANCEDSEARCH) {
            for (PersistentRecord r : records) {
              DebugConstants.DEBUG_OUT.println(r.debug());
            }
          }
          throw new LogicException(named + " has more than one match in AsContentCompiler: "
                                   + query);
      }
    }
  }

  /**
   * Definitions of the AsContentCompilerField Table that holds information about one field within a
   * ContentCompiler and it's relationship with the AdvancedSearch framework.
   */
  public static class AsContentCompilerField
    extends NodeTableWrapper
  {
    public final static String TABLENAME = "AsContentCompilerField";

    public final static IntFieldKey ASCONTENTCOMPILER_ID =
        LinkField.getLinkFieldName(AsContentCompiler.TABLENAME);

    public final static RecordOperationKey<PersistentRecord> ASCONTENTCOMPILER =
        LinkField.getLinkName(ASCONTENTCOMPILER_ID);

    public final static ObjectFieldKey<String> FIELDNAME = StringField.createKey("fieldName");

    public final static RecordOperationKey<Field<?>> FIELDFILTER =
        RecordOperationKey.create("fieldFilter", ObjectUtil.<Field<?>> castClass(Field.class));

    private final AdvancedSearch __advancedSearch;

    private AsContentCompilerField(AdvancedSearch advancedSearch)
    {
      super(advancedSearch.getNodeManager(),
            TABLENAME);
      __advancedSearch = advancedSearch;
    }

    @Override
    public void initTable(final Db db,
                          final String pwd,
                          final Table table)
        throws MirrorTableLockedException
    {
      table.addField(new LinkField(AsContentCompiler.TABLENAME,
                                   "Containing AsContentCompiler",
                                   pwd,
                                   Dependencies.LINK_ENFORCED));
      table.addField(new StringField(FIELDNAME, "Field name"));
      table.addRecordOperation(new SimpleRecordOperation<Field<?>>(FIELDFILTER) {
        @Override
        public Field<?> getOperation(TransientRecord asContentCompilerField,
                                     Viewpoint viewpoint)
        {
          return getFieldFilter(asContentCompilerField);
        }
      });
    }

    public Field<?> getFieldFilter(TransientRecord asContentCompilerField)
    {
      PersistentRecord asContentCompiler = asContentCompilerField.comp(ASCONTENTCOMPILER);
      Table backendTable =
          __advancedSearch.getAsContentCompiler().getBackendTable(asContentCompiler);
      return backendTable.getField(asContentCompilerField.opt(FIELDNAME));
    }

    public Query search(PersistentRecord asContentCompiler,
                        String name)
    {
      StringBuilder filter = new StringBuilder(93 + name.length());
      if (asContentCompiler != null) {
        filter.append("(AsContentCompilerField.asContentCompiler_id=")
              .append(asContentCompiler.getId())
              .append(") and ");
      }
      filter.append("(AsContentCompilerField.fieldName=");
      SqlUtil.toSafeSqlString(filter, name);
      filter.append(")");
      return getTable().getQuery(null, filter.toString(), null);
    }

    public PersistentRecord getInstance(Named named,
                                        String fieldName,
                                        Transaction transaction)
        throws MirrorNoSuchRecordException
    {
      return RecordSources.getFirstRecord(search(__advancedSearch.getAsContentCompiler()
                                                                 .getInstance(named, transaction),
                                                 fieldName),
                                          transaction);
    }

    public PersistentRecord createInstance(Named named,
                                           String fieldName,
                                           Transaction transaction)
        throws MirrorInstantiationException, MirrorFieldException
    {
      PersistentRecord asCc =
          __advancedSearch.getAsContentCompiler().createInstance(named, transaction);
      Query query = search(asCc, fieldName);
      RecordList records = query.getRecordList(transaction);
      switch (records.size()) {
        case 0:
          TransientRecord record = getTable().createTransientRecord();
          record.setField(ASCONTENTCOMPILER_ID, Integer.valueOf(asCc.getId()));
          record.setField(FIELDNAME, fieldName);
          return record.makePersistent(null);
        case 1:
          return records.get(0);
        default:
          throw new LogicException(named + " has more than one match in AsContentCompiler");
      }
    }
  }

  /**
   * Base functionality for the Tables that extend the AsDataItem structure which provides the
   * binding for the data between the Region and the ContentCompiler Field that the information
   * corresponds to. Each AdvancedSearchPlugin is expected to extend this Table definition with the
   * meta-data that it actually implements.
   */
  public static abstract class AsDataItem<T>
    extends AbstractTableWrapper
    implements DbInitialiser, TableWrapper
  {
    public final static IntFieldKey ASCONTENTCOMPILERFIELD_ID =
        LinkField.getLinkFieldName(AsContentCompilerField.TABLENAME);
    // "asContentCompilerField_id";

    public final static IntFieldKey NODE_ID = LinkField.getLinkFieldName(Node.TABLENAME);
    // "node_id";

    public final static IntFieldKey REGIONSTYLE_ID =
        LinkField.getLinkFieldName(RegionStyle.TABLENAME);

    // "regionStyle_id";

    protected AsDataItem(String tableName)
    {
      super(tableName);
    }

    public final void initTable(Db db,
                                String transactionPassword)
        throws MirrorTableLockedException
    {
    }

    @Override
    protected Table createTable(Db db,
                                String pwd)
        throws MirrorTableLockedException
    {
      return db.addSoftTable(this, getTableName(), null);
    }

    @Override
    protected void initTable(Db db,
                             String pwd,
                             Table asDataItem)
        throws MirrorTableLockedException
    {
      asDataItem.addField(new LinkField(AsContentCompilerField.TABLENAME,
                                        "Related AsContentCompilerField",
                                        pwd,
                                        Dependencies.LINK_ENFORCED));
      asDataItem.addField(new LinkField(Node.TABLENAME,
                                        "Related Node",
                                        pwd,
                                        Dependencies.LINK_ENFORCED));
      asDataItem.addField(new LinkField(RegionStyle.TABLENAME,
                                        "Related RegionStyle",
                                        pwd,
                                        Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
      init(db, pwd, asDataItem);
    }

    protected abstract void init(Db db,
                                 String transactionPassword,
                                 Table asDataItem)
        throws MirrorTableLockedException;

    protected RecordSource getDataItems(PersistentRecord node)
    {
      return node.opt(getTable().getReferencesName());
    }

    /**
     * @return The number of data items that were cleared
     */
    protected int clearDataItems(PersistentRecord node)
        throws MirrorTransactionTimeoutException
    {
      RecordSource dataItems = getDataItems(node);
      int count = dataItems.getIdList().size();
      if (count > 0) {
        try (Transaction transaction =
            getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                      Transaction.TRANSACTION_DELETE,
                                      getTransactionPassword(),
                                      this + ".clearDataItems(" + node + ")")) {
          transaction.delete(dataItems, Db.DEFAULT_TIMEOUT);
          transaction.commit();
        }
      }
      return count;
    }

    public abstract FieldKey<T> getDataFieldName();

    public abstract T getDataField(PersistentRecord asDataItem);
  }
}