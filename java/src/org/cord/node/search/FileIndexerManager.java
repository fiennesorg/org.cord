package org.cord.node.search;

import java.util.Map;

import org.cord.util.Maps;

public class FileIndexerManager
{
  private final Map<String, FileIndexer> __indexers = Maps.newConcurrentHashMap();

  public FileIndexerManager()
  {
  }

  public FileIndexer add(FileIndexer indexer)
  {
    return __indexers.put(indexer.getName(), indexer);
  }

  public FileIndexer get(String formatName)
  {
    return __indexers.get(formatName);
  }
}
