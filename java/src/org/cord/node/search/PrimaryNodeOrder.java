package org.cord.node.search;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.node.Node;
import org.cord.util.EnglishNamedImpl;

import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Implementation of NodeOrder that sorts according to a set of fields on the core Node table.
 */
public class PrimaryNodeOrder
  extends EnglishNamedImpl
  implements NodeOrder
{
  private final String __order;

  private final Db __db;

  public PrimaryNodeOrder(String name,
                          String englishName,
                          Db db,
                          String order)
  {
    super(name,
          englishName);
    __order = order;
    __db = db;
  }

  public Db getDb()
  {
    return __db;
  }

  public String getOrder()
  {
    return __order;
  }

  @Override
  public IdList order(IdList nodeIds,
                      int undefinedBehaviour)
  {
    if (nodeIds.size() == 0) {
      return nodeIds;
    }
    StringBuilder filter = new StringBuilder();
    filter.append("(Node.id in (");
    for (IntIterator i = nodeIds.iterator(); i.hasNext();) {
      filter.append(i.nextInt());
      if (i.hasNext()) {
        filter.append(',');
      }
    }
    filter.append("))");
    Query nodeQuery =
        getDb().getTable(Node.TABLENAME).getQuery(null, filter.toString(), getOrder());
    return nodeQuery.getIdList();
  }
}