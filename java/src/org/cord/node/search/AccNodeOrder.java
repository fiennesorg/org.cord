package org.cord.node.search;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.Query;
import org.cord.node.Node;
import org.cord.util.EnglishNamedImpl;

import it.unimi.dsi.fastutil.ints.IntListIterator;

/**
 * Implementation of NodeOrder that sorts according to a set of fields in a related
 * TableContentCompiler. If the node list includes node ids that don't have the corresponding
 * ContentCompilers then they will be automatically dropped from the ordered version of this method.
 */
public class AccNodeOrder
  extends EnglishNamedImpl
  implements NodeOrder
{
  private final String __instanceTable;

  private final String __order;

  private final Db __db;

  public AccNodeOrder(String name,
                      String englishName,
                      Db db,
                      String instanceTable,
                      String order)
  {
    super(name,
          englishName);
    __instanceTable = instanceTable;
    __order = order;
    __db = db;
  }

  public Db getDb()
  {
    return __db;
  }

  public String getOrder()
  {
    return __order;
  }

  @Override
  public IdList order(IdList nodeIds,
                      int undefinedBehaviour)
  {
    if (nodeIds.size() == 0) {
      return nodeIds;
    }
    StringBuilder filter = new StringBuilder();
    filter.append("(").append(__instanceTable).append(".node_id=Node.id) and (Node.id in (");
    IntListIterator i = nodeIds.iterator();
    while (i.hasNext()) {
      filter.append(i.next());
      if (i.hasNext()) {
        filter.append(',');
      }
    }
    filter.append("))");
    Query nodeQuery =
        (getDb().getTable(Node.TABLENAME)
                .getQuery(null, filter.toString(), getOrder(), getDb().getTable(__instanceTable)));
    return nodeQuery.getIdList();
  }
}