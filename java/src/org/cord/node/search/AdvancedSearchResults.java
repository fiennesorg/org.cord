package org.cord.node.search;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.node.AbcPaginator;
import org.cord.node.AbstractPaginator;
import org.cord.node.Node;
import org.cord.node.NodeRequest;
import org.cord.node.Paginator;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.listing.QueryListing;
import org.cord.node.listing.QueryListingFactory;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.PredicateInt;
import org.cord.util.SkippingIterator;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;

import it.unimi.dsi.fastutil.ints.IntIterator;

public class AdvancedSearchResults
{
  private final AdvancedSearch __advancedSearch;

  private final IdList __nodeIds;

  private final Table __nodeTable;

  private final Gettable __request;

  private final boolean __hadTerms;

  private final String __debugTrace;

  protected AdvancedSearchResults(AdvancedSearch advancedSearch,
                                  Gettable request,
                                  IdList nodeIds,
                                  String debugTrace)
  {
    __advancedSearch = advancedSearch;
    __request = request;
    __nodeTable = advancedSearch.getNodeManager().getDb().getTable(Node.TABLENAME);
    __hadTerms = nodeIds != null;
    __nodeIds = nodeIds == null
        ? advancedSearch.getNodeManager().getNode().getTable().getEmptyIds()
        : nodeIds;
    __debugTrace = debugTrace;
  }

  /**
   * Create a new version of this AdvancedSearchResults with the given {@link PredicateInt} applied
   * to all matching nodeIds. The original AdvancedSearchResults is unchanged. All of the
   * configuration params in the new AdvancedSearchResults will be informed, it may just contain a
   * subset of the original nodeIds.
   */
  public AdvancedSearchResults filterNodeIds(PredicateInt nodeIdFilter)
  {
    IdList filteredNodeIds = new ArrayIdList(__nodeTable, __nodeIds.size());
    IntIterator i = __nodeIds.iterator();
    while (i.hasNext()) {
      int nodeId = i.nextInt();
      if (nodeIdFilter.apply(nodeId)) {
        filteredNodeIds.add(nodeId);
      }
    }
    return new AdvancedSearchResults(getAdvancedSearch(), __request, filteredNodeIds, __debugTrace);
  }

  public String getDebugTrace()
  {
    return __debugTrace;
  }

  public QueryListing getQueryListing(QueryListingFactory factory,
                                      TableContentCompiler tcc,
                                      String cgiName,
                                      NodeRequest nodeRequest)
  {
    QueryListing queryListing =
        factory.getQueryListing(tcc.getInstancesFromNodes(getNodeIds(), null),
                                cgiName,
                                nodeRequest);
    return addHiddenFormFields(queryListing);
  }

  public QueryListing addHiddenFormFields(QueryListing queryListing)
  {
    for (Iterator<?> it = getRelatedFieldNames(); it.hasNext();)
      queryListing.addHiddenFormField(__request, StringUtils.toString(it.next()));
    return queryListing;
  }

  public boolean hadTerms()
  {
    return __hadTerms;
  }

  public AdvancedSearch getAdvancedSearch()
  {
    return __advancedSearch;
  }

  /**
   * Get the number of matching Nodes contained in this result.
   */
  public int getSize()
  {
    return getNodeIds().size();
  }

  public Iterator<PersistentRecord> getNodes(IdList nodeIds,
                                             Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    return new RecordList(nodeIds, transaction).iterator();
  }

  public IdList getNodeIds()
  {
    return __nodeIds;
  }

  /**
   * Get the matching Nodes from the search as a RecordSource object.
   */
  public RecordSource getRecordSource()
  {
    return new ConstantRecordSource(UnmodifiableIdList.getInstance(__nodeIds), null);
  }

  /**
   * Get the Node ids stored in this result set filtered by the given NodeOrder.
   * 
   * @param ordering
   *          The NodeOrder that should perform the filtering. If null then the ordering is
   *          unchanged.
   */
  public IdList getNodeIds(NodeOrder ordering,
                           int undefinedBehaviour)
  {
    return ordering == null ? __nodeIds : ordering.order(__nodeIds, undefinedBehaviour);
  }

  /**
   * Resolve the request NodeOrder by name and then use it to reorder the Node ids stored in this
   * result.
   */
  public IdList getNodeIds(String orderingName,
                           int undefinedBehaviour)
  {
    return getNodeIds(__advancedSearch.getNodeOrderManager().getNodeOrder(orderingName),
                      undefinedBehaviour);
  }

  public Iterator<PersistentRecord> getNodes(NodeOrder ordering,
                                             int undefinedBehaviour)
      throws MirrorNoSuchRecordException
  {
    return getNodes(ordering.order(__nodeIds, undefinedBehaviour), null);
  }

  public Iterator<PersistentRecord> iterator()
      throws MirrorNoSuchRecordException
  {
    return getNodes(__nodeIds, null);
  }

  /**
   * Get an initialised Paginator across the current data set held in these search results. The
   * ordering of the results will be set by the NodeOrdering named in the AS_nodeOrder parameter in
   * the incoming NodeRequest that generated the incoming request.
   */
  public Paginator getPaginator(int defaultPageSize)
  {
    Paginator paginator = new Paginator("asr", __request, defaultPageSize);
    String nodeOrderName = __request.getString(AdvancedSearch.AS_NODEORDER);
    paginator.setRawResults(__nodeTable,
                            getNodeIds(nodeOrderName, NodeOrder.UNDEFINEDBEHAVIOUR_DROP));
    for (Object key : __request.getPublicKeys()) {
      String parameterName = StringUtils.toString(key);
      if (parameterName.startsWith(AdvancedSearch.KEY_PREFIX)) {
        for (Object value : __request.getValues(parameterName)) {
          paginator.addAdditionalField(parameterName, value);
        }
      }
    }
    paginator.addAdditionalField("view", __request.getString("view"));
    return paginator;
  }

  public AbcPaginator getAbcPaginator()
  {
    AbcPaginator abcPaginator = new AbcPaginator("asr", __request);
    abcPaginator.setSourceQuery(getRecordSource());
    for (Object key : __request.getPublicKeys()) {
      String parameterName = StringUtils.toString(key);
      if (parameterName.startsWith(AdvancedSearch.KEY_PREFIX)) {
        Collection<?> values = __request.getValues(parameterName);
        if (values != null) {
          for (Object value : __request.getValues(parameterName)) {
            abcPaginator.addAdditionalField(parameterName, value);
          }
        }
      }
    }
    return abcPaginator;
  }

  public Iterator<String> getRelatedFieldNames()
  {
    return new SkippingIterator<String>(__request.getPublicKeys().iterator()) {
      @Override
      protected boolean shouldSkip(String name)
      {
        return (!(name.startsWith(AdvancedSearch.KEY_PREFIX) || name.equals("view")));
      }
    };
  }

  private void appendHiddenFormField(Appendable result,
                                     String name)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(result, __request.getString(name), name);
  }

  public String getHiddenFormFields(String omitValue)
  {
    StringBuilder result = new StringBuilder();
    try {
      for (Object key : __request.getPublicKeys()) {
        String parameterName = StringUtils.toString(key);
        if (parameterName.startsWith(AdvancedSearch.KEY_PREFIX)
            && !parameterName.equals(omitValue)) {
          appendHiddenFormField(result, parameterName);
        }
      }
      if (!"view".equals(omitValue)) {
        appendHiddenFormField(result, "view");
      }
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(result, ioEx);
    }
    return result.toString();
  }

  public String getCgiFields(String omitValue)
  {
    StringBuilder result = new StringBuilder();
    for (Object key : __request.getPublicKeys()) {
      String parameterName = StringUtils.toString(key);
      if (parameterName.startsWith(AdvancedSearch.KEY_PREFIX) && !parameterName.equals(omitValue)) {
        Collection<?> values = __request.getValues(parameterName);
        for (Object value : values) {
          AbstractPaginator.appendGetParam(result, parameterName, value.toString());
        }
      }
    }
    if (!"view".equals(omitValue)) {
      AbstractPaginator.appendGetParam(result, "view", __request.getString("view"));
    }
    return result.toString();
  }

  public String refineSearch(TransientRecord node)
  {
    return node.get("url") + "?" + getCgiFields("view");
  }

  public String getHiddenFormFields()
  {
    return getHiddenFormFields(null);
  }
}