package org.cord.node.search;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.TransientRecord;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;
import org.cord.util.Named;
import org.cord.util.Shutdownable;
import org.cord.util.StringUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Implementation of AdvancedSearchPlugin that is responsible for the indexing of integer data.
 */
public class AdvancedIntegerSearch
  extends AdvancedAbstractSearch<Integer>
  implements Shutdownable, DbInitialisers
{
  public static final String NAME = "advancedIntegerSearch";

  private final AsInteger __asInteger;

  public AdvancedIntegerSearch(NodeManager nodeManager,
                               String transactionPassword)
  {
    super(NAME,
          "Integer Search",
          nodeManager,
          transactionPassword);
    __asInteger = new AsInteger();
  }

  public AsInteger getAsInteger()
  {
    return __asInteger;
  }

  @Override
  public boolean accepts(Named named)
  {
    return named instanceof AdvancedIntegerSearchable;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedIntegerSearchable) named).getAisIndexableFields();
    }
    return ImmutableList.of();
  }

  @Override
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedIntegerSearchable) named).getAisSearchableFields();
    }
    return ImmutableList.of();
  }

  @Override
  protected void buildLiveFieldsIndex(PersistentRecord node,
                                      FieldsIndex dataIndex,
                                      PersistentRecord regionStyle,
                                      Named named)
      throws MirrorNoSuchRecordException
  {
    int regionStyleId = regionStyle == null ? 0 : regionStyle.getId();
    if (named instanceof AdvancedIntegerSearchable) {
      AdvancedIntegerSearchable searchable = (AdvancedIntegerSearchable) named;
      Map<RecordOperationKey<?>, IntSet> allValues =
          searchable.getAisIndexableValues(node, regionStyle, null);
      if (allValues != null) {
        for (RecordOperationKey<?> fieldName : allValues.keySet()) {
          PersistentRecord asCcf =
              getAdvancedSearch().getAsContentCompilerField()
                                 .getInstance(named, fieldName.getKeyName(), null);
          IntSet valueSet = allValues.get(fieldName);
          if (valueSet != null) {
            DataItems dataItems = getDataItems(dataIndex, asCcf.getId(), regionStyleId, true);
            for (Integer value : valueSet) {
              dataItems.put(value, DataItems.ID_MISSING);
            }
          }
        }
      }
    }
  }

  private Integer findInteger(Gettable gettable,
                              String searchName,
                              String intName)
  {
    String value = gettable.getString(AdvancedSearch.createExplicitKey(searchName, intName));
    if (value != null) {
      try {
        return Integer.valueOf(value);
      } catch (NumberFormatException nfEx) {
      }
    }
    if (intName.equals(gettable.get(AdvancedSearch.createExplicitKey(searchName, "intStyle")))) {
      value = gettable.getString(AdvancedSearch.createExplicitKey(searchName, "intValue"));
      if (value != null) {
        try {
          return Integer.valueOf(value);
        } catch (NumberFormatException nfEx) {
        }
      }
    }
    return null;
  }
  private Collection<Integer> toIntegers(Collection<?> values)
  {
    if (values == null || values.size() == 0) {
      return Collections.emptyList();
    }
    List<Integer> result = new ArrayList<Integer>();
    Iterator<?> i = values.iterator();
    while (i.hasNext()) {
      try {
        result.add(Integer.valueOf(StringUtils.toString(i.next())));
      } catch (NumberFormatException nfEx) {
        // just skip the badly formatted value...
      }
    }
    return Collections.unmodifiableList(result);
  }

  private Collection<Integer> findIntegers(Gettable gettable,
                                           String searchName,
                                           String intName,
                                           PrintWriter debug)
  {
    Collection<String> values = AdvancedSearch.getValues(gettable, searchName, intName, ",", debug);
    if (values.size() != 0) {
      return toIntegers(values);
    }
    // TODO: Work out what this searching functionality this is supposed to
    // do...
    if (intName.equals(gettable.get(AdvancedSearch.createExplicitKey(searchName, "intStyle")))) {
      return toIntegers(gettable.getValues(AdvancedSearch.createExplicitKey(searchName,
                                                                            "intValue")));
    }
    return Collections.emptyList();
  }

  private void appendFilter(StringBuilder filter,
                            Collection<Integer> integers,
                            String sqlOperator)
  {
    if (integers != null) {
      switch (integers.size()) {
        case 0:
          // no matches so do nothing....
          break;
        case 1:
          filter.append(" and (AsInteger.value ")
                .append(sqlOperator)
                .append(" (")
                .append(integers.iterator().next())
                .append("))");
          break;
        default:
          filter.append(" and (AsInteger.value ").append(sqlOperator).append(" (");         
          Iterator<Integer> i = integers.iterator();
          while (i.hasNext()) {
            filter.append(i.next());
            if (i.hasNext()) {
              filter.append(", ");
            }
          }
          filter.append("))");
      }
    }
  }

  private void appendFilter(StringBuilder filter,
                            Integer integer,
                            String sqlOperator)
  {
    if (integer != null) {
      filter.append(" and (AsInteger.value").append(sqlOperator).append(integer).append(")");
    }
  }

  public static final String AS_AND = "and";
  public static final String AS_MATCH = "match";
  public static final String AS_EXCLUDE = "exclude";
  public static final String AS_MINIMUM = "minimum";
  public static final String AS_MAXIMUM = "maximum";
  public static final String AS_MINIMUM_INCLUSIVE = "minimum_inclusive";
  public static final String AS_MAXIMUM_INCLUSIVE = "maximum_inclusive";

  @Override
  public IdList nodeSearch(Gettable gettable,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug)
  {
    currentMatches = performAndSearch(gettable, searchName, currentMatches, debug);
    if (currentMatches != null && currentMatches.size() == 0) {
      return currentMatches;
    }
    StringBuilder filter = new StringBuilder();
    appendFilter(filter, findIntegers(gettable, searchName, AS_MATCH, debug), "in");
    appendFilter(filter, findIntegers(gettable, searchName, AS_EXCLUDE, debug), "not in");
    appendFilter(filter, findInteger(gettable, searchName, AS_MINIMUM), ">");
    appendFilter(filter, findInteger(gettable, searchName, AS_MAXIMUM), "<");
    appendFilter(filter, findInteger(gettable, searchName, AS_MINIMUM_INCLUSIVE), ">=");
    appendFilter(filter, findInteger(gettable, searchName, AS_MAXIMUM_INCLUSIVE), "<=");
    if (filter.length() == 0) {
      return currentMatches;
    }
    Query baseQuery = getBaseQuery(gettable, searchName, debug);
    Query integerQuery =
        Query.transformQuery(baseQuery, filter.toString(), Query.TYPE_EXPLICIT, null);
    if (debug != null) {
      debug.println(this + ":query=" + integerQuery);
    }
    if (currentMatches == null) {
      return integerQuery.getIdList();
    } else {
      return integerQuery.intersection(currentMatches);
    }
  }

  private IdList performAndSearch(Gettable gettable,
                                  String searchName,
                                  IdList currentMatches,
                                  PrintWriter debug)
  {
    Collection<Integer> ids = findIntegers(gettable, searchName, AS_AND, debug);
    if (ids.size() == 0) {
      return currentMatches;
    }
    Query baseQuery = getBaseQuery(gettable, searchName, debug);
        
    for (Integer id : ids) {
      Query matchQuery = performMatchSearch(baseQuery, id);
      currentMatches = currentMatches == null
          ? matchQuery.getIdList()
          : matchQuery.intersection(currentMatches);
      if (currentMatches.size() == 0) {
        return currentMatches;
      }
    }
    return currentMatches;
  }
  
  private Query getBaseQuery(Gettable gettable, String searchName, PrintWriter debug)
  {
   return getAdvancedSearch().getBaseQuery(gettable, searchName, getAsDataItem(), debug); 
  }

  private Query performMatchSearch(Query baseQuery,
                                   Integer integer)
  {
    StringBuilder filter = new StringBuilder();
    appendFilter(filter, integer, "=");
    return Query.transformQuery(baseQuery, filter.toString(), Query.TYPE_EXPLICIT, null);
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    return Iterators.<DbInitialiser> singletonIterator(__asInteger);
  }

  public static IntSet addIndexableValue(Integer value,
                                         IntSet existingValues)
  {
    existingValues = existingValues == null ? new IntOpenHashSet() : existingValues;
    if (value != null) {
      existingValues.add(value);
    }
    return existingValues;
  }

  public static Map<RecordOperationKey<?>, IntSet> addIndexableValue(RecordOperationKey<?> fieldName,
                                                                     Integer fieldValue,
                                                                     Map<RecordOperationKey<?>, IntSet> existingValues)
  {
    Map<RecordOperationKey<?>, IntSet> map =
        existingValues == null ? new HashMap<RecordOperationKey<?>, IntSet>() : existingValues;
    synchronized (map) {
      IntSet values = map.get(fieldName);
      map.put(fieldName, addIndexableValue(fieldValue, values));
    }
    return map;
  }

  public static Map<RecordOperationKey<?>, IntSet> addIndexableValue(RecordOperationKey<?> fieldName,
                                                                     TransientRecord source,
                                                                     Map<RecordOperationKey<?>, IntSet> existingValues)
  {
    return addIndexableValue(fieldName, (Integer) source.get(fieldName), existingValues);
  }

  public static Map<RecordOperationKey<?>, IntSet> addIndexableValues(Iterator<RecordOperationKey<?>> fieldNames,
                                                                      TransientRecord source,
                                                                      Map<RecordOperationKey<?>, IntSet> existingValues)
  {
    while (fieldNames.hasNext()) {
      existingValues = addIndexableValue(fieldNames.next(), source, existingValues);
    }
    return existingValues;
  }

  @Override
  public AdvancedSearch.AsDataItem<Integer> getAsDataItem()
  {
    return __asInteger;
  }

  public static class AsInteger
    extends AdvancedSearch.AsDataItem<Integer>
    implements DbInitialiser, TableWrapper
  {
    public final static String TABLENAME = "AsInteger";

    public final static IntFieldKey VALUE = IntField.createKey("value");

    private AsInteger()
    {
      super(TABLENAME);
    }

    @Override
    public void init(Db db,
                     String transactionPassword,
                     Table asInteger)
        throws MirrorTableLockedException
    {
      asInteger.addField(new IntField(VALUE, "Integer value"));
    }

    @Override
    public FieldKey<Integer> getDataFieldName()
    {
      return VALUE;
    }

    @Override
    public Integer getDataField(PersistentRecord asDataItem)
    {
      return asDataItem.opt(getDataFieldName());
    }
  }
}