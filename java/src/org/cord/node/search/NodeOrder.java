package org.cord.node.search;

import org.cord.mirror.IdList;
import org.cord.util.EnglishNamed;

/**
 * Interface that describes an Object that is capable of taking a list of Nodes and providing a
 * custom Order to the list.
 */
public interface NodeOrder
  extends EnglishNamed
{
  public final static int UNDEFINEDBEHAVIOUR_FIRST = -1;

  public final static int UNDEFINEDBEHAVIOUR_DROP = 0;

  public final static int UNDEFINEDBEHAVIOUR_LAST = 1;

  /**
   * @param undefinedBehaviour
   *          The way that searches should handle Nodes for which the ordering is undefined
   *          according to the implemented ordering.
   * @see #UNDEFINEDBEHAVIOUR_FIRST
   * @see #UNDEFINEDBEHAVIOUR_DROP
   * @see #UNDEFINEDBEHAVIOUR_LAST
   */
  public IdList order(IdList nodeIds,
                      int undefinedBehaviour);
}