package org.cord.node.search;

import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.FastDateField;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;
import org.cord.util.Named;
import org.cord.util.NumberUtil;
import org.cord.util.Shutdownable;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

/**
 * Implementation of AdvancedSearchPlugin that is responsible for the indexing of individual word
 * data.
 */
public class AdvancedDateSearch
  extends AdvancedAbstractSearch<Date>
  implements Shutdownable, DbInitialisers
{
  public static Map<String, Date> addIndexableField(String fieldName,
                                                    Date fieldValue,
                                                    Map<String, Date> existingValues)
  {
    Map<String, Date> map = existingValues == null ? new HashMap<String, Date>() : existingValues;
    if (fieldName == null || fieldValue == null) {
      return map;
    }
    synchronized (map) {
      map.put(fieldName, fieldValue);
    }
    return map;
  }

  public static Map<String, Date> addIndexableField(String fieldName,
                                                    TransientRecord source,
                                                    Map<String, Date> existingValues)
  {
    // FastDateField fdff = (FastDateField) source.getFieldFilter(fieldName);

    // if (fdff.isDefined(source)) {
    return addIndexableField(fieldName, (Date) source.get(fieldName), existingValues);
    // }
    // return existingValues;
  }

  public static Map<String, Date> addIndexableFields(Iterator<String> fieldNames,
                                                     TransientRecord source,
                                                     Map<String, Date> existingValues)
  {
    while (fieldNames.hasNext()) {
      existingValues = addIndexableField(fieldNames.next(), source, existingValues);
    }
    return existingValues;
  }

  public static final String NAME = "advancedDateSearch";

  private final AsDate __asDate;

  public AdvancedDateSearch(NodeManager nodeManager,
                            String transactionPassword)
  {
    super("NAME",
          "Date Search",
          nodeManager,
          transactionPassword);
    __asDate = new AsDate();
  }

  @Override
  protected void buildLiveFieldsIndex(PersistentRecord node,
                                      FieldsIndex dataIndex,
                                      PersistentRecord regionStyle,
                                      Named named)
      throws MirrorNoSuchRecordException
  {
    int regionStyleId = regionStyle == null ? 0 : regionStyle.getId();
    if (named instanceof AdvancedDateSearchable) {
      AdvancedDateSearchable searchable = (AdvancedDateSearchable) named;
      Map<RecordOperationKey<?>, Date> allValues =
          searchable.getAdsIndexableValues(node, regionStyle, null);
      if (allValues != null) {
        for (RecordOperationKey<?> fieldName : allValues.keySet()) {
          PersistentRecord asCcf =
              getAdvancedSearch().getAsContentCompilerField()
                                 .getInstance(named, fieldName.getKeyName(), null);
          Date value = allValues.get(fieldName);
          if (value != null) {
            DataItems dataItems = getDataItems(dataIndex, asCcf.getId(), regionStyleId, true);
            dataItems.put(value, DataItems.ID_MISSING);
          }
        }
      }
    }
  }

  public AsDate getAsDate()
  {
    return __asDate;
  }

  @Override
  public boolean accepts(Named named)
  {
    return named instanceof AdvancedDateSearchable;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedDateSearchable) named).getAdsIndexableFields();
    }
    return ImmutableList.of();
  }

  @Override
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedDateSearchable) named).getAdsSearchableFields();
    }
    return ImmutableList.of();
  }

  public final static String EXPLICITDATEFORMAT = "dd/MM/yy";

  private final SimpleDateFormat __explicitDateFormat = new SimpleDateFormat(EXPLICITDATEFORMAT);

  private Date getDate(Gettable gettable,
                       String searchName,
                       String dateName,
                       PrintWriter debug)
  {
    String explicitDate = AdvancedSearch.getValue(gettable, searchName, dateName, debug);
    if (!Strings.isNullOrEmpty(explicitDate)) {
      try {
        // check for explicit unix timestamp value first
        return new Date(Long.parseLong(explicitDate));
      } catch (NumberFormatException nfEx) {
        // if that failed, then check for dd/MM/yy in a single value
        synchronized (__explicitDateFormat) {
          try {
            return __explicitDateFormat.parse(explicitDate);
          } catch (ParseException pEx) {
            // do nothing
          }
        }
      }
    }
    // and if that fails then try and build up the date from individual fields,
    // getting more accurate until a field is not defined.
    int year =
        NumberUtil.toInt(AdvancedSearch.getValue(gettable, searchName, dateName + "Year", debug),
                         -1);
    if (year > -1) {
      Calendar calendar = Calendar.getInstance();
      calendar.clear();
      calendar.set(Calendar.YEAR, year);
      int month =
          NumberUtil.toInt(AdvancedSearch.getValue(gettable, searchName, dateName + "Month", debug),
                           -1);
      if (month > -1) {
        calendar.set(Calendar.MONTH, month);
        int day =
            NumberUtil.toInt(AdvancedSearch.getValue(gettable, searchName, dateName + "Day", debug),
                             -1);
        if (day > -1) {
          calendar.set(Calendar.DAY_OF_MONTH, day);
          int hour = NumberUtil.toInt(AdvancedSearch.getValue(gettable,
                                                              searchName,
                                                              dateName + "Hour",
                                                              debug),
                                      -1);
          if (hour > -1) {
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            int minute = NumberUtil.toInt(AdvancedSearch.getValue(gettable,
                                                                  searchName,
                                                                  dateName + "Minute",
                                                                  debug),
                                          -1);
            if (minute > -1) {
              calendar.set(Calendar.MINUTE, minute);
              int second = NumberUtil.toInt(AdvancedSearch.getValue(gettable,
                                                                    searchName,
                                                                    dateName + "Second",
                                                                    debug),
                                            -1);
              if (second > -1) {
                calendar.set(Calendar.SECOND, second);
                int millisecond = NumberUtil.toInt(AdvancedSearch.getValue(gettable,
                                                                           searchName,
                                                                           dateName + "Millisecond",
                                                                           debug),
                                                   -1);
                if (millisecond > -1) {
                  calendar.set(Calendar.MILLISECOND, millisecond);
                }
              }
            }
          }
        }
      }
      return calendar.getTime();
    }
    return null;
  } // AS_<searchName>_minimum // AS_<searchName>_maximum

  // AS_<searchName>_exclude
  // AS_<searchName>_match
  // AS_<searchName>_dateStyle (minimum,maximum,match,exclude)
  // AS_<searchName>_dateValue
  private Date findDate(Gettable gettable,
                        String searchName,
                        String dateName,
                        PrintWriter debug)
  {
    Date date = getDate(gettable, searchName, dateName, debug);
    if (date != null) {
      return date;
    }
    String dateStyleKey = AdvancedSearch.createExplicitKey(searchName, "dateStyle");
    if (debug != null) {
      debug.println(dateName + "---" + gettable.get(dateStyleKey));
    }
    if (dateName.equals(gettable.get(dateStyleKey))) {
      return getDate(gettable, searchName, "dateValue", debug);
    }
    return null;
  }

  @Override
  public IdList nodeSearch(Gettable gettable,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug)
  {
    Date minimumDate = findDate(gettable, searchName, "minimum", debug);
    Date maximumDate = findDate(gettable, searchName, "maximum", debug);
    Date excludeDate = findDate(gettable, searchName, "exclude", debug);
    Date matchDate = findDate(gettable, searchName, "match", debug);
    if (debug != null) {
      debug.println("minimumDate --> " + minimumDate);
      debug.println("maximumDate --> " + maximumDate);
      debug.println("excludeDate --> " + excludeDate);
      debug.println("matchDate --> " + matchDate);
    }
    if (minimumDate == null && maximumDate == null && matchDate == null && excludeDate == null) {
      return currentMatches;
    }
    Query baseQuery =
        getAdvancedSearch().getBaseQuery(gettable, searchName, getAsDataItem(), debug);
    StringBuilder filter = new StringBuilder();
    if (minimumDate != null) {
      filter.append(" and (AsDate.fastdate >= ").append(minimumDate.getTime()).append(")");
    }
    if (maximumDate != null) {
      filter.append(" and (AsDate.fastdate <= ").append(maximumDate.getTime()).append(")");
    }
    if (excludeDate != null) {
      filter.append(" and (AsDate.fastdate != ").append(excludeDate.getTime()).append(")");
    }
    if (matchDate != null) {
      filter.append(" and (AsDate.fastdate == ").append(matchDate.getTime()).append(")");
    }
    Query dateQuery = Query.transformQuery(baseQuery, filter.toString(), Query.TYPE_EXPLICIT, null);
    if (debug != null) {
      debug.println(dateQuery);
    }
    if (currentMatches == null) {
      currentMatches = dateQuery.getIdList();
    } else {
      currentMatches = dateQuery.intersection(currentMatches);
    }
    return currentMatches;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    return Iterators.<DbInitialiser> singletonIterator(__asDate);
  }

  @Override
  public AdvancedSearch.AsDataItem<Date> getAsDataItem()
  {
    return __asDate;
  }

  public static class AsDate
    extends AdvancedSearch.AsDataItem<Date>
    implements DbInitialiser, TableWrapper
  {
    public final static String TABLENAME = "AsDate";

    public final static ObjectFieldKey<Date> FASTDATE = FastDateField.createKey("fastdate");

    private AsDate()
    {
      super(TABLENAME);
    }

    @Override
    public void init(Db db,
                     String transactionPassword,
                     Table asDate)
        throws MirrorTableLockedException
    {
      asDate.addField(new FastDateField(FASTDATE, "Date value"));
    }

    @Override
    public FieldKey<Date> getDataFieldName()
    {
      return FASTDATE;
    }

    @Override
    public Date getDataField(PersistentRecord asDataItem)
    {
      return asDataItem.opt(getDataFieldName());
    }
  }
}