package org.cord.node.search;

import java.io.File;
import java.util.Date;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.node.Node;
import org.cord.task.InvalidTaskInput;
import org.cord.task.Task;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.cord.util.Gettable;
import org.cord.util.Settable;

public class UpdateQueue
  extends TaskFactory
  implements DbInitialiser, TableWrapper
{
  public static final String TABLENAME = "AsUpdateQueue";

  public static final IntFieldKey NODE_ID = LinkField.getLinkFieldName(Node.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> NODE = LinkField.getLinkName(NODE_ID);

  public static final ObjectFieldKey<Date> TIMESTAMP = FastDateField.createKey("timestamp");

  private final AdvancedSearch __advancedSearch;

  private final Db __db;

  private final String __pwd;

  private Table _asUpdateQueue;

  private final Object __addNodeSync = new Object();

  public UpdateQueue(TaskManager taskManager,
                     File logDir,
                     String webLogDir,
                     AdvancedSearch advancedSearch,
                     Db db,
                     String pwd)
  {
    super(TABLENAME,
          "Index Rebuilder",
          taskManager,
          logDir,
          webLogDir,
          1,
          1);
    __advancedSearch = advancedSearch;
    __db = db;
    __pwd = pwd;
  }

  protected String getPwd()
  {
    return __pwd;
  }

  protected Db getDb()
  {
    return __db;
  }

  protected AdvancedSearch getAdvancedSearch()
  {
    return __advancedSearch;
  }

  public void run()
  {
  }

  protected PersistentRecord getNextEntry(Task task)
  {
    if (task.hasStopRequest()) {
      return null;
    }
    RecordList records = getTable().getRecordList();
    switch (records.size()) {
      case 0:
        synchronized (__addNodeSync) {
          try {
            task.setStatusMessage("Sleeping...");
            __addNodeSync.wait();
          } catch (InterruptedException e) {
          }
        }
        return getNextEntry(task);
      default:
        try {
          return records.get(0);
        } catch (MissingRecordException e) {
          return getNextEntry(task);
        }
    }
  }

  public void triggerUpdate()
  {
    synchronized (__addNodeSync) {
      __addNodeSync.notifyAll();
    }
  }

  public PersistentRecord addNode(PersistentRecord node)
      throws MirrorInstantiationException, MirrorFieldException
  {
    TransientRecord auq = getTable().createTransientRecord();
    auq.setField(NODE_ID, Integer.valueOf(node.getId()));
    PersistentRecord pAuq = auq.makePersistent(null);
    triggerUpdate();
    return pAuq;
  }

  @Override
  public void init(Db db,
                   String transactionPassword)
      throws MirrorTableLockedException
  {
    _asUpdateQueue = db.addTable(this, TABLENAME);
    _asUpdateQueue.addField(new LinkField(Node.TABLENAME,
                                          null,
                                          transactionPassword,
                                          Dependencies.LINK_ENFORCED));
    _asUpdateQueue.addField(new FastDateField(TIMESTAMP, "Timestamp"));
    _asUpdateQueue.setDefaultOrdering(TIMESTAMP.toString());
  }

  // public void shutdown()
  // {
  // _isRunning = false;
  // triggerUpdate();
  // _asUpdateQueue = null;
  // }
  @Override
  public String getTableName()
  {
    return TABLENAME;
  }

  @Override
  public Table getTable()
  {
    return _asUpdateQueue;
  }

  @Override
  protected void validateInput(Task task)
      throws InvalidTaskInput
  {
  }

  @Override
  protected void executeTask(Task task)
      throws Exception
  {
    while (!task.hasStopRequest()) {
      PersistentRecord entry = null;
      while ((entry = getNextEntry(task)) != null) {
        PersistentRecord node = entry.comp(NODE);
        try (Transaction transaction = getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                 Transaction.TRANSACTION_DELETE,
                                                                 getPwd(),
                                                                 "AsUpdateQueue")) {
          transaction.delete(entry);
          transaction.commit();
        } catch (MirrorTransactionTimeoutException e) {
          // TODO (#264) Issue deleting update queue record - what to do?
          e.printStackTrace();
        } 
        getAdvancedSearch().rebuildIndex(node);
      }
    }
    System.err.println("UpdateQueue shutting down");
  }

  @Override
  public Settable getNewInput(Gettable defaults)
  {
    return null;
  }

  @Override
  public void shutdown()
  {
  }
}
