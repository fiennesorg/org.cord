package org.cord.node.search;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.util.Named;
import org.cord.util.Shutdownable;

/**
 * Interface that describes an plugin object that is capable of extracting indexable information
 * from a registered BinaryAsset.
 */
public interface FileIndexer
  extends Named, Shutdownable
{
  public void appendIndexableContent(TransientRecord region,
                                     Viewpoint viewpoint,
                                     StringBuilder buffer,
                                     PersistentRecord instance);
}
