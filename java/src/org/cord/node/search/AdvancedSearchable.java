package org.cord.node.search;

import org.cord.util.Named;

/**
 * Interface that is used to descibe Objects that are capable of supplying indexable information and
 * meta-data to an AdvancedSearchPlugin. If you implement a new AdvancedWordSearchPlugin then you
 * should create extend the interface of AdvancedSearchable to mark objects that are capable of
 * working with your plugin. Any ContentCompiler that doesn't implement some form of
 * AdvancedSearchable will not be considered for indexing.
 * 
 * @author alex
 */
public interface AdvancedSearchable
  extends Named
{
}
