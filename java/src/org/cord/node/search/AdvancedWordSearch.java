package org.cord.node.search;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.node.NodeManager;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.Named;
import org.cord.util.Shutdownable;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.StringUtils;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Implementation of AdvancedSearchPlugin that is responsible for the indexing of individual word
 * data.
 */
public final class AdvancedWordSearch
  extends AdvancedAbstractSearch<Integer>
  implements Shutdownable, DbInitialisers
{
  public static final String NAME = "advancedWordSearch";

  public static final Pattern __wordPattern = Pattern.compile("\\w+");

  private final AsWordHash __asWordHash;

  public AdvancedWordSearch(NodeManager nodeManager,
                            String transactionPassword)
  {
    super(NAME,
          "Word Search",
          nodeManager,
          transactionPassword);
    __asWordHash = new AsWordHash();
  }

  public AsWordHash getAsWordHash()
  {
    return __asWordHash;
  }

  @Override
  public boolean accepts(Named named)
  {
    return named instanceof AdvancedWordSearchable;
  }

  /**
   * Utility method to create a correctly typed immutable Iterable to hold the fields for use in
   * {@link #getIndexableFields(Named)} and {@link #getSearchableFields(Named)}
   */
  public static final ImmutableList<RecordOperationKey<?>> fields(RecordOperationKey<?>... fields)
  {
    return ImmutableList.copyOf(fields);
  }

  @Override
  public Iterable<RecordOperationKey<?>> getIndexableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedWordSearchable) named).getAwsIndexableFields();
    }
    return ImmutableList.of();
  }

  @Override
  public Iterable<RecordOperationKey<?>> getSearchableFields(Named named)
  {
    if (accepts(named)) {
      return ((AdvancedWordSearchable) named).getAwsSearchableFields();
    }
    return ImmutableList.of();
  }

  @Override
  protected void buildLiveFieldsIndex(PersistentRecord node,
                                      FieldsIndex dataIndex,
                                      PersistentRecord regionStyle,
                                      Named named)
      throws MirrorNoSuchRecordException
  {
    if (DebugConstants.DEBUG_ADVANCEDSEARCH_REBUILD) {
      DebugConstants.method(this, "buildLiveFieldsIndex", node, dataIndex, regionStyle, named);
    }
    int regionStyleId = regionStyle == null ? 0 : regionStyle.getId();
    if (named instanceof AdvancedWordSearchable) {
      AdvancedWordSearchable searchable = (AdvancedWordSearchable) named;
      if (DebugConstants.DEBUG_ADVANCEDSEARCH_REBUILD) {
        DebugConstants.variable("searchable", searchable);
      }
      Gettable gettable = searchable.getAwsIndexableValues(node, regionStyle, null);
      if (gettable != null) {
        Map<String, IntSet> allValues =
            addIndexableValues(searchable.getAwsIndexableFields().iterator(), gettable, null);
        if (allValues != null) {
          for (String fieldName : allValues.keySet()) {
            PersistentRecord asCcf =
                getAdvancedSearch().getAsContentCompilerField().getInstance(named, fieldName, null);
            IntSet values = allValues.get(fieldName);
            if (values != null) {
              DataItems wordHashs = getDataItems(dataIndex, asCcf.getId(), regionStyleId, true);
              for (IntIterator i = values.iterator(); i.hasNext();) {
                wordHashs.put(Integer.valueOf(i.nextInt()), DataItems.ID_MISSING);
              }
            }
          }
        }
      }
    }
  }

  public static final String AS_SEARCHTERMS = "searchTerms";
  public static final String AS_STRIPPEDCHARS = "strippedChars";

  @Override
  public IdList nodeSearch(Gettable request,
                           String searchName,
                           IdList currentMatches,
                           PrintWriter debug)
  {
    Collection<String> searchTerms =
        AdvancedSearch.getValues(request, searchName, AS_SEARCHTERMS, null, debug);
    if (searchTerms != null && searchTerms.size() > 0) {
      String strippedChars = AdvancedSearch.getValue(request, searchName, AS_STRIPPEDCHARS, debug);
      Pattern stripper = null;
      if (!Strings.isNullOrEmpty(strippedChars)) {
        stripper = Pattern.compile(strippedChars);
      }
      Query baseQuery =
          getAdvancedSearch().getBaseQuery(request, searchName, getAsDataItem(), debug);
      TreeSet<String> sortedWords = new TreeSet<String>(LengthComparator.getInstance());
      for (String searchTerm : searchTerms) {
        if (stripper != null) {
          searchTerm = stripper.matcher(searchTerm).replaceAll("");
        }
        searchTerm = StringUtils.removeAccents(searchTerm).trim().toLowerCase();
        Matcher wordMatcher = __wordPattern.matcher(searchTerm);
        while (wordMatcher.find()) {
          sortedWords.add(wordMatcher.group());
        }
      }
      for (String word : sortedWords) {
        int hash = hash(word);
        String filter = " and (AsWordHash.wordHash=" + hash + ")";
        Query wordQuery = Query.transformQuery(baseQuery, filter, Query.TYPE_EXPLICIT, null);
        if (currentMatches == null) {
          currentMatches = wordQuery.getIdList();
        } else {
          currentMatches = wordQuery.intersection(currentMatches);
        }
        if (currentMatches.size() == 0) {
          return currentMatches;
        }
      }
    }
    return currentMatches;
  }

  public static int hash(String word)
  {
    return word.hashCode();
  }

  /**
   * Implementation of Comparator that sorts Strings based around their length with longer items
   * coming before longer items. Strings of the same length will be sorted by their natural
   * (alphabetical) ordering.
   */
  public final static class LengthComparator
    implements Comparator<String>, SingletonShutdownable
  {
    private static LengthComparator _instance = new LengthComparator();

    public static LengthComparator getInstance()
    {
      return _instance;
    }

    @Override
    public void shutdownSingleton()
    {
      _instance = null;
    }

    private LengthComparator()
    {
      SingletonShutdown.registerSingleton(this);
    }

    @Override
    public int compare(String s1,
                       String s2)
    {
      int l1 = s1.length();
      int l2 = s2.length();
      if (l1 < l2) {
        return 1;
      }
      if (l1 > l2) {
        return -1;
      }
      return s1.compareTo(s2);
    }

    @Override
    public boolean equals(Object obj)
    {
      return this == obj;
    }

    @Override
    public int hashCode()
    {
      return super.hashCode();
    }
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public Iterator<DbInitialiser> getDbInitialisers()
  {
    return Iterators.<DbInitialiser> singletonIterator(__asWordHash);
  }

  /**
   * Calculate the Set of word hashes contained in source, appending the result onto existingValues.
   * 
   * @param source
   *          The String that is to be parsed.
   * @param existingValues
   *          The Set of existing Hashes. If this is null then a new Set will be created.
   * @return The Set of Integers for the hashes of contained words.
   * @see #hash(String)
   */
  public static IntSet getIndexableValues(String source,
                                          IntSet existingValues)
  {
    existingValues = existingValues == null ? new IntOpenHashSet() : existingValues;
    if (Strings.isNullOrEmpty(source)) {
      return existingValues;
    } else {
      source = StringUtils.removeAccents(source.toLowerCase());
      Matcher m = __wordPattern.matcher(source);
      while (m.find()) {
        existingValues.add(hash(m.group()));
      }
    }
    return existingValues;
  }

  /**
   * Prepare the relationship between fieldName to fieldValue in a format suitable for the
   * getAwsIndexableValues method. If there is already data in existingValues about fieldName then
   * the passed fieldValue data is appended to the end of the existing data.
   */
  private static Map<String, IntSet> addIndexableValue(String fieldName,
                                                       String fieldValue,
                                                       Map<String, IntSet> existingValues)
  {
    Map<String, IntSet> map =
        existingValues == null ? new HashMap<String, IntSet>() : existingValues;
    synchronized (map) {
      IntSet values = map.get(fieldName);
      map.put(fieldName, getIndexableValues(fieldValue, values));
    }
    return map;
  }

  private static final SimpleDateFormat __dateIndex =
      new SimpleDateFormat("EEE EEEE dd MMM MMMM yy yyyy");

  private static Map<String, IntSet> addIndexableValues(String fieldName,
                                                        Gettable source,
                                                        Map<String, IntSet> existingValues)
  {
    Object value = source.get(fieldName);
    String stringValue = null;
    if (value instanceof Date) {
      synchronized (__dateIndex) {
        stringValue = __dateIndex.format((Date) value);
      }
    } else if (value instanceof Enum<?>) {
      stringValue = ((Enum<?>) value).name();
    } else {
      stringValue = StringUtils.toString(value);
    }
    return addIndexableValue(fieldName, stringValue, existingValues);
  }

  private static Map<String, IntSet> addIndexableValues(Iterator<RecordOperationKey<?>> recordOperationKeys,
                                                        Gettable source,
                                                        Map<String, IntSet> existingValues)
  {
    while (recordOperationKeys.hasNext()) {
      existingValues =
          addIndexableValues(recordOperationKeys.next().getKeyName(), source, existingValues);
    }
    return existingValues;
  }

  @Override
  public AdvancedSearch.AsDataItem<Integer> getAsDataItem()
  {
    return __asWordHash;
  }

  public static class AsWordHash
    extends AdvancedSearch.AsDataItem<Integer>
  {
    public final static String TABLENAME = "AsWordHash";

    public final static IntFieldKey WORDHASH = IntField.createKey("wordHash");

    private AsWordHash()
    {
      super(TABLENAME);
    }

    @Override
    public void init(Db db,
                     String transactionPassword,
                     Table asWordHash)
        throws MirrorTableLockedException
    {
      asWordHash.addField(new IntField(WORDHASH, "Hashed value of word"));
    }

    @Override
    public FieldKey<Integer> getDataFieldName()
    {
      return WORDHASH;
    }

    @Override
    public Integer getDataField(PersistentRecord asDataItem)
    {
      return asDataItem.opt(getDataFieldName());
    }
  }
}