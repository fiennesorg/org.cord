package org.cord.node.search;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.DelegatingGettable;
import org.cord.util.Gettable;
import org.cord.util.GettableTransform;
import org.cord.util.SettableMap;
import org.cord.util.StringTransform;
import org.cord.util.StringTransformMap;
import org.webmacro.Context;

import com.google.common.base.Preconditions;

/**
 * View that resolves an incoming AdvancedSearch request, drops the result into the Context and then
 * passes on control to a 3rd party module.
 */
public class AdvancedSearchView
  extends DynamicAclAuthenticatedFactory
{
  public static final String WEBMACRO_ADVANCEDSEARCHRESULTS = "advancedSearchResults";

  private final AdvancedSearch __advancedSearch;

  private final SettableMap __fixedParams = new SettableMap();

  private final StringTransformMap __transforms = new StringTransformMap();

  private boolean _isLocked = false;

  public AdvancedSearchView(String name,
                            NodeManager nodeManager,
                            PostViewAction postViewSuccessOperation,
                            Integer aclId,
                            String authenticationError,
                            AdvancedSearch advancedSearch)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
    __advancedSearch = Preconditions.checkNotNull(advancedSearch, "advancedSearch");
  }

  public final AdvancedSearch getAdvancedSearch()
  {
    return __advancedSearch;
  }

  protected Gettable mergeFixedParams(Gettable params)
  {
    if (__fixedParams.size() == 0) {
      return params;
    }
    DelegatingGettable dg = new DelegatingGettable(false);
    dg.addGettable(__fixedParams);
    dg.addGettable(params);
    return dg;
  }

  protected Gettable applyTransforms(Gettable params)
  {
    if (__transforms.size() == 0) {
      return params;
    }
    return new GettableTransform(params, __transforms);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    Gettable params = mergeFixedParams(nodeRequest);
    applyTransforms(params);
    nodeRequest.setViewNotes(getViewNotes(params));
    AdvancedSearchResults asr = getAdvancedSearch().nodeSearch(params, nodeRequest.getUserId());
    context.put(WEBMACRO_ADVANCEDSEARCHRESULTS, asr);
    return handleSuccess(nodeRequest, node, context, null);
  }

  protected String getViewNotes(Gettable params)
  {
    return null;
  }

  private void checkNotLocked()
  {
    if (_isLocked) {
      throw new IllegalStateException(this + " has already been locked");
    }
  }

  public void addFixedParam(String name,
                            String value)
  {
    checkNotLocked();
    __fixedParams.put(name, value);
  }

  public void addStringTransform(String name,
                                 StringTransform transform)
  {
    checkNotLocked();
    __transforms.registerTransform(name, transform);
  }

  public void lock()
  {
    _isLocked = true;
  }
}