package org.cord.node.search;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.node.Node;
import org.cord.util.Gettable;
import org.cord.util.NamedImpl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

/**
 * Class that represents information encapsulated in a Node rather than a ContentCompiler in a
 * format that is searchable.
 */
// TODO: should this implement binary and integer searchable as well?
public class NodeStub
  extends NamedImpl
  implements AdvancedWordSearchable, AdvancedDateSearchable
{
  private final List<RecordOperationKey<?>> AWS_FIELDS =
      ImmutableList.<RecordOperationKey<?>> builder()
                   .add(Node.TITLE, Node.HTMLTITLE, Node.FILENAME)
                   .build();

  public final static String NAME = Node.TABLENAME;

  protected NodeStub()
  {
    super(NAME);
  }

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord region,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return node;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return AWS_FIELDS;
  }

  private final List<RecordOperationKey<?>> ADS_FIELDS =
      ImmutableList.<RecordOperationKey<?>> builder()
                   .add(Node.CREATIONTIME, Node.MODIFICATIONTIME)
                   .build();

  @Override
  public Iterable<RecordOperationKey<?>> getAdsIndexableFields()
  {
    return ADS_FIELDS;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getAdsSearchableFields()
  {
    return ADS_FIELDS;
  }

  @Override
  public Map<RecordOperationKey<?>, Date> getAdsIndexableValues(PersistentRecord node,
                                                                PersistentRecord region,
                                                                Viewpoint viewpoint)
  {
    Map<RecordOperationKey<?>, Date> map = Maps.newHashMap();
    map.put(Node.CREATIONTIME, node.opt(Node.CREATIONTIME));
    map.put(Node.MODIFICATIONTIME, node.opt(Node.MODIFICATIONTIME));
    return map;
  }
}