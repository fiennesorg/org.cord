package org.cord.node.search;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.util.Gettable;

/**
 * Interface that is used to descibe Objects that are capable of supplying indexable information and
 * meta-data to the AdvancedWordSearch plugin.
 * 
 * @see AdvancedWordSearch
 */
public interface AdvancedWordSearchable
  extends AdvancedSearchable
{
  /**
   * Get the Gettable data mapping of the fields defined into getAwsIndexableFields to their String
   * values
   * 
   * @return the appropriate Gettable. If this AdvancedWordSearchable is not indexable at present
   *         for whatever reasons (because it is disabled?) then it is permissable to return null
   *         whereupon no content will be indexed.
   */
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  /**
   * Get the fields that are indexable by this searchable item
   */
  public Iterable<RecordOperationKey<?>> getAwsIndexableFields();

  /**
   * Get the fields that may be publically advertised as being searchable against on this searchable
   * item.
   */
  public Iterable<RecordOperationKey<?>> getAwsSearchableFields();
}
