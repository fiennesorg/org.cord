package org.cord.node.search;

import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Viewpoint;

/**
 * Interface that is used to descibe Objects that are capable of supplying indexable information and
 * meta-data to the AdvancedDateSearch plugin.
 * 
 * @see AdvancedDateSearch
 */
public interface AdvancedBooleanSearchable
  extends AdvancedSearchable
{
  /**
   * @return Map that holds Map(String, Boolean)
   */
  public Map<String, Boolean> getAbsIndexableValues(PersistentRecord node,
                                                    PersistentRecord regionStyle,
                                                    Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getAbsIndexableFields();

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getAbsSearchableFields();
}
