package org.cord.node.search;

/**
 * An Object that is triggered after a search is performed on the AdvancedSearch architecture. There
 * should be no side effects on the functionality of the search as this
 * 
 * @author alex
 */
public interface PostSearchPlugin
{
  /**
   * Do whatever work is required on the supplied AdvancedSearchResults. This should not throw any
   * exceptions if possible and should have no side effects that are relevant to the cycle of the
   * search itself.
   */
  public void processSearchResults(AdvancedSearchResults results);

  /**
   * Does this plugin want the AdvancedSearch to generate debugging information and make it
   * available to the plugin during the processSearchResults?
   */
  public boolean wantsDebugTrace();
}
