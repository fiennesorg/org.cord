package org.cord.node.search;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.util.Shutdownable;

import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntLinkedOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.ints.IntSortedSet;

public class RebuildThread
  implements Shutdownable
{
  private volatile boolean _isRunning = true;

  private final Object __wakeUp = new Object();

  private final IntSortedSet __nodeIds = new IntLinkedOpenHashSet();

  private final AdvancedSearch __advancedSearch;

  private final Table __nodes;

  private final File __pendingNodeIdsFile;

  public RebuildThread(NodeManager nodeManager,
                       AdvancedSearch advancedSearch,
                       File pendingNodeIdsFile)
  {
    __advancedSearch = advancedSearch;
    __nodes = nodeManager.getNode().getTable();
    __pendingNodeIdsFile = pendingNodeIdsFile;
    nodeManager.registerShutdownable(this);
    _thread.setPriority(Thread.NORM_PRIORITY - 1);
    _thread.start();
  }

  private void writePendingNodeIds()
      throws IOException
  {
    if (__pendingNodeIdsFile == null) {
      return;
    }
    PrintWriter out = null;
    synchronized (__nodeIds) {
      if (__nodeIds.size() == 0) {
        return;
      }
      try {
        out = new PrintWriter(new BufferedWriter(new FileWriter(__pendingNodeIdsFile)));
        for (IntIterator i = __nodeIds.iterator(); i.hasNext();) {
          out.println(i.nextInt());
        }
      } finally {
        if (out != null) {
          out.close();
        }
      }
    }
  }

  public void readPendingNodeIds()
      throws IOException
  {
    if (__pendingNodeIdsFile == null) {
      return;
    }
    BufferedReader in = null;
    try {
      in = new BufferedReader(new FileReader(__pendingNodeIdsFile));
      String line = null;
      synchronized (__nodeIds) {
        while ((line = in.readLine()) != null) {
          try {
            __nodeIds.add(Integer.parseInt(line));
          } catch (NumberFormatException nfEx) {
            IOException ioEx = new IOException("Badly formatted pendingNodeIds file");
            ioEx.initCause(nfEx);
            throw ioEx;
          }
        }
      }
    } finally {
      if (in != null) {
        in.close();
      }
    }
  }

  private RuntimeException _runEx = null;

  private Thread _thread = new Thread(new Runnable() {
    @Override
    public void run()
    {
      try {
        while (_isRunning) {
          synchronized (__wakeUp) {
            try {
              __wakeUp.wait();
            } catch (InterruptedException e) {
            }
          }
          int nodeId = 0;
          // Index all remaining nodes, unless we've been told to shutdown
          // _and_
          // there's somewhere to save our state
          __nodes.getDb().lockConnectionToThread();
          while ((_isRunning || __pendingNodeIdsFile == null) && (nodeId = peekNodeId()) != 0) {
            try {
              __advancedSearch.rebuildIndexNow(__nodes.getRecord(nodeId));
            } catch (MirrorException e) {
              e.printStackTrace();
            }
            synchronized (__nodeIds) {
              __nodeIds.remove(nodeId);
            }
          }
          __nodes.getDb().releaseConnectionFromThread();
        }
      } catch (RuntimeException runEx) {
        runEx.printStackTrace();
        _runEx = runEx;
      } finally {
        __nodes.getDb().releaseConnectionFromThread();
      }
      System.err.println("RebuildThread shutting down");
      try {
        writePendingNodeIds();
      } catch (IOException e) {
        System.err.println("Error storing pendingNodeIds: " + __nodeIds);
        e.printStackTrace();
      }
    }

  });

  private int peekNodeId()
  {
    synchronized (__nodeIds) {
      if (__nodeIds.size() == 0) {
        return 0;
      }
      return __nodeIds.firstInt();
    }
  }

  private void wakeUp()
  {
    synchronized (__wakeUp) {
      __wakeUp.notifyAll();
    }
  }

  /**
   * @return Unmodifiable IntSet
   */
  public IntSet getNodeIds()
  {
    return IntSets.unmodifiable(__nodeIds);
  }

  public void rebuildIndex(PersistentRecord node)
  {
    TransientRecord.assertIsTableNamed(node, Node.TABLENAME);
    rebuildIndex(node.getId());
  }

  public void rebuildIndex(int nodeId)
  {
    if (!_thread.isAlive()) {
      throw new RuntimeException("Search: Rebuild Thread is not running - run() exception set as cause",
                                 _runEx);
    }
    boolean isNewNode = false;
    synchronized (__nodeIds) {
      isNewNode = __nodeIds.add(nodeId);
    }
    if (isNewNode) {
      wakeUp();
    }
  }

  @Override
  public void shutdown()
  {
    _isRunning = false;
    wakeUp();
    try {
      _thread.join();
    } catch (InterruptedException e) {
    }
  }
}
