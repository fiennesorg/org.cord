package org.cord.node.search;

import org.cord.task.InvalidTaskInput;
import org.cord.task.Task;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.cord.util.Gettable;
import org.cord.util.Settable;

/**
 * TaskFactory that will launch a request to rebuild the entire contents of a given AdvancedSearch
 * in the background on a live site.
 * 
 * @author alex
 * @see AdvancedSearch#rebuildIndex(Task)
 */
public class RebuildIndexTaskFactory
  extends TaskFactory
{
  public static final String NAME = "RebuildIndex";

  public static final String ENGLISHNAME = "Rebuild Site Index";

  private final AdvancedSearch __advancedSearch;

  public RebuildIndexTaskFactory(TaskManager taskManager,
                                 AdvancedSearch advancedSearch)
  {
    super(NAME,
          ENGLISHNAME,
          taskManager,
          null,
          null,
          0,
          1);
    __advancedSearch = advancedSearch;
  }

  @Override
  protected void validateInput(Task task)
      throws InvalidTaskInput
  {
    // no input required so fine...
  }

  @Override
  protected void executeTask(Task task)
      throws Exception
  {
    __advancedSearch.rebuildIndex(task);
  }

  @Override
  public Settable getNewInput(Gettable defaults)
  {
    return null;
  }
}
