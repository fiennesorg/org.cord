package org.cord.node.search;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.task.Task;
import org.webmacro.Context;

/**
 * View that triggers a rebuildIndex on the system.
 * 
 * @see AdvancedSearch#rebuildIndex(Task)
 */
public class RebuildIndex
  extends DynamicAclAuthenticatedFactory
{
  private final AdvancedSearch __advancedSearch;

  public RebuildIndex(String name,
                      NodeManager nodeManager,
                      PostViewAction postViewSuccessOperation,
                      int aclId,
                      String authenticationError,
                      AdvancedSearch advancedSearch)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          Integer.valueOf(aclId),
          authenticationError);
    __advancedSearch = advancedSearch;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    try {
      __advancedSearch.rebuildIndex((Task) null);
    } catch (MirrorException mEx) {
      throw new FeedbackErrorException("Rebuild index",
                                       node,
                                       mEx,
                                       true,
                                       true,
                                       "There has been a db error while rebuilding the index");
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}