package org.cord.node.search;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;

/**
 * An Object that wants to be informed once a given Node has been re-indexed.
 * 
 * @author alex
 */
public interface RebuildTrigger
{
  public void postRebuildIndex(PersistentRecord node,
                               boolean isExempt)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException;
}
