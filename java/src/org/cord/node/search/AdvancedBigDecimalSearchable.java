package org.cord.node.search;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;

/**
 * Interface that is used to descibe Objects that are capable of supplying indexable information and
 * meta-data to the AdvancedWordSearch plugin.
 * 
 * @see AdvancedWordSearch
 */
public interface AdvancedBigDecimalSearchable
  extends AdvancedSearchable
{
  /**
   * @return Map that holds Map(String, Set(Integer))
   * @see AdvancedBigDecimalSearch#addIndexableValues(Iterator, TransientRecord, Map)
   */
  public Map<String, Set<BigDecimal>> getIndexableBigDecimalValues(PersistentRecord node,
                                                                   PersistentRecord regionStyle,
                                                                   Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getIndexableBigDecimalFields();

  /**
   * @return Iterator of Strings
   */
  public Iterable<RecordOperationKey<?>> getSearchableBigDecimalFields();
}
