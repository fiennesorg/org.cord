package org.cord.node.search;

import java.util.Set;
import java.util.TreeSet;

import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.node.Node;
import org.cord.node.NodePathComparator;
import org.cord.util.EnglishNamedImpl;

import it.unimi.dsi.fastutil.ints.IntIterator;

public class PathNodeOrder
  extends EnglishNamedImpl
  implements NodeOrder
{
  private final Table __nodeTable;

  public PathNodeOrder(String name,
                       String englishName,
                       AdvancedSearch advancedSearch)
  {
    super(name,
          englishName);
    __nodeTable = advancedSearch.getNodeManager().getDb().getTable(Node.TABLENAME);
  }

  @Override
  public IdList order(IdList nodeIds,
                      int undefinedBehaviour)
  {
    Set<PersistentRecord> set = new TreeSet<PersistentRecord>(NodePathComparator.getInstance());
    for (IntIterator i = nodeIds.iterator(); i.hasNext();) {
      try {
        set.add(__nodeTable.getRecord(i.nextInt()));
      } catch (MirrorNoSuchRecordException mnsrEx) {
        // lets just transparently drop this one for now...
      }
    }
    IdList result = new ArrayIdList(nodeIds.getTable());
    for (PersistentRecord record : set) {
      result.add(record.getId());
    }
    return result;
  }
}