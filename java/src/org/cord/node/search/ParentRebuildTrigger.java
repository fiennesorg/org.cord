package org.cord.node.search;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.node.NodeFilter;

import com.google.common.base.Preconditions;

/**
 * RebuildTrigger that causes the parent Node of the triggered node to be rebuilt.
 * 
 * @author alex
 */
public class ParentRebuildTrigger
  implements RebuildTrigger
{
  private final AdvancedSearch __advancedSearch;

  private final NodeFilter __nodeFilter;

  /**
   * @param nodeFilter
   *          The filter that describes which Nodes will cause this RebuildTrigger to fire.
   */
  public ParentRebuildTrigger(AdvancedSearch advancedSearch,
                              NodeFilter nodeFilter)
  {
    Preconditions.checkNotNull(advancedSearch, "advancedSearch");
    __advancedSearch = advancedSearch;
    Preconditions.checkNotNull(nodeFilter, "nodeFilter");
    __nodeFilter = nodeFilter;
  }

  @Override
  public void postRebuildIndex(PersistentRecord node,
                               boolean isExempt)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException, MirrorFieldException,
      MirrorInstantiationException
  {
    if (__nodeFilter.accepts(node)) {
      PersistentRecord parentNode = node.opt(Node.PARENTNODE);
      if (parentNode != null) {
        __advancedSearch.rebuildIndex(parentNode);
      }
    }
  }
}
