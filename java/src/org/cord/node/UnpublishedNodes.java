package org.cord.node;

import org.cord.mirror.IdList;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.IdLists;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceIdFilter;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.PostInstantiationTriggerImpl;
import org.cord.util.Gettable;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public class UnpublishedNodes
{
  private final NodeManager __nodeMgr;
  private final Table __nodeTable;

  private final IntSet __unpublished = new IntOpenHashSet();
  private String _unpublishedNodeIds = null;

  public UnpublishedNodes(NodeManager nodeMgr) throws MirrorTableLockedException
  {
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
    __nodeTable = getNodeMgr().getDb().getTable(Node.TABLENAME);
    __nodeTable.addStateTrigger(new NodeUpdateTrigger());
    __nodeTable.addStateTrigger(new NodeDeleteTrigger());
    __nodeTable.addPostInstantiationTrigger(new NodeCreateTrigger());
  }

  @Override
  public String toString()
  {
    return "UnpublishedNodes(" + __nodeMgr + ")";
  }

  public static final String UNPUBLISHEDNODES = "Node.isPublished=0";

  public void lock()
  {
    initUnpublishedNodeIds();
  }

  protected void initUnpublishedNodeIds()
  {
    for (PersistentRecord node : __nodeTable.getQuery(UNPUBLISHEDNODES,
                                                      UNPUBLISHEDNODES,
                                                      Query.NOSQLORDERING)) {
      add(node, false);
    }
  }

  protected final NodeManager getNodeMgr()
  {
    return __nodeMgr;
  }

  /**
   * Work out whether the given node is published taking into account the state of the parent and
   * doing no caching at all, ie we always traverse up the parentage until we either reach the top
   * or we find an unpublished parent.
   */
  public final boolean calculateIsPublished(final PersistentRecord node)
  {
    boolean isPublished = node.is(Node.ISPUBLISHED);
    if (!isPublished) {
      return false;
    }
    PersistentRecord parentNode = node.opt(Node.PARENTNODE);
    if (parentNode == null) {
      return isPublished;
    }
    return calculateIsPublished(parentNode);
  }

  /**
   * Check to see whether a Node is published using the fast internal cache of unpublished ids.
   */
  public boolean isPublished(final TransientRecord node)
  {
    return isPublished(node.getId());
  }

  /**
   * Check to see whether a Node.id is published using the fast internal cache of unpublished ids.
   * Note that this is only checking against the list of unpublished Nodes so if you supply an
   * illegal id then the result will be erroneously true.
   */
  public synchronized boolean isPublished(final int nodeId)
  {
    return !__unpublished.contains(nodeId);
  }

  protected void add(final PersistentRecord node)
  {
    add(node, true);
  }

  protected synchronized void add(final PersistentRecord node,
                                  boolean rebuildIndex)
  {
    addInner(node, rebuildIndex, !__unpublished.contains(node.compInt(Node.PARENTNODE_ID)));
  }

  private void addInner(final PersistentRecord node,
                        final boolean rebuildIndex,
                        final boolean inheritedIsPublished)
  {
    final int id = node.getId();
    final boolean isPublished = inheritedIsPublished && node.is(Node.ISPUBLISHED);
    if (__unpublished.contains(id) != isPublished) {
      return;
    }
    if (isPublished) {
      __unpublished.remove(id);
    } else {
      __unpublished.add(id);
    }
    _unpublishedNodeIds = null;
    if (rebuildIndex) {
      getNodeMgr().getSearchMgr().rebuildIndex(node);
    }
    for (PersistentRecord childNode : node.opt(Node.UNSORTEDCHILDRENNODES)) {
      addInner(childNode, rebuildIndex, isPublished);
    }
  }

  protected synchronized void remove(int id)
  {
    if (__unpublished.remove(id)) {
      _unpublishedNodeIds = null;
    }
  }

  public static final String NODEIDS_EMPTY = "(0)";

  public String getUnpublishedNodeIdListing()
  {
    String unpublishedNodeIds = _unpublishedNodeIds;
    if (unpublishedNodeIds == null) {
      if (__unpublished.size() == 0) {
        unpublishedNodeIds = NODEIDS_EMPTY;
      } else {
        StringBuilder buf = new StringBuilder(__unpublished.size() * 5);
        buf.append('(');
        IntIterator i = __unpublished.iterator();
        buf.append(i.nextInt());
        while (i.hasNext()) {
          buf.append(',').append(i.nextInt());
        }
        buf.append(')');
        unpublishedNodeIds = buf.toString();
      }
      _unpublishedNodeIds = unpublishedNodeIds;
    }
    return unpublishedNodeIds;
  }

  public RecordSource filterUnpublishedNodes(RecordSource nodes)
  {
    RecordSources.assertIsTable(nodes, __nodeTable);
    return new SingleRecordSourceIdFilter(nodes, null) {
      @Override
      protected boolean accepts(int id)
      {
        return !__unpublished.contains(id);
      }
    };
  }

  /**
   * @param nodeIds
   *          The compulsory list of ids from the Node table.
   * @return The list of ids from the Node table that are globally published.
   */
  public IdList filterUnpublishedNodes(IdList nodeIds)
  {
    IdLists.assertIsTable(nodeIds, __nodeTable);
    ArrayIdList filteredIds = new ArrayIdList(__nodeTable, nodeIds.size());
    for (IntIterator i = nodeIds.iterator(); i.hasNext();) {
      int id = i.nextInt();
      if (isPublished(id)) {
        filteredIds.add(id);
      }
    }
    return filteredIds;
  }

  class NodeCreateTrigger
    extends PostInstantiationTriggerImpl
  {
    private NodeCreateTrigger()
    {
    }

    @Override
    public void triggerPost(PersistentRecord node,
                            Gettable params)
        throws MirrorException
    {
      add(node);
    }
  }

  class NodeDeleteTrigger
    extends AbstractStateTrigger
  {
    private NodeDeleteTrigger()
    {
      super(STATE_PREDELETED_DELETED,
            false,
            Node.TABLENAME);
    }

    @Override
    public void trigger(PersistentRecord node,
                        Transaction transaction,
                        int state)
        throws Exception
    {
      remove(node.getId());
    }
  }

  class NodeUpdateTrigger
    extends AbstractStateTrigger
  {
    private NodeUpdateTrigger()
    {
      super(STATE_WRITABLE_UPDATED,
            false,
            Node.TABLENAME);
    }

    @Override
    public void trigger(PersistentRecord node,
                        Transaction transaction,
                        int state)
        throws Exception
    {
      add(node);
    }
  }
}
