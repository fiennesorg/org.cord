package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.util.OptionalInt;

public class AclAuthenticatedBase
  implements AclAuthenticated
{
  private final NodeManager __nodeManager;

  private int _aclId;

  private final int __setAclId;

  /**
   * @param aclId
   *          The id of the ACL that this object will initially authenticate against. A value of
   *          null is padded out to ACL_PUBLIC_ID thereby granting access to all.
   * @param setAclId
   *          The id of the ACL that must be satisfied to update the value of aclId. A value of null
   *          is padded to ACL_ROOTONLY_ID thereby making aclId immutable for normal usage.
   */
  public AclAuthenticatedBase(NodeManager nodeManager,
                              OptionalInt aclId,
                              OptionalInt setAclId)
  {
    __nodeManager = nodeManager;
    _aclId = aclId.or(Acl.ACL_PUBLIC_ID);
    __setAclId = setAclId.or(Acl.ACL_ROOTONLY_ID);
  }

  public final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  @Override
  public final PersistentRecord getAcl()
      throws MirrorNoSuchRecordException
  {
    return getNodeManager().getAcl().getTable().getRecord(getAclId());
  }

  public final int getSetAclId()
  {
    return __setAclId;
  }

  public final PersistentRecord getSetAcl()
      throws MirrorNoSuchRecordException
  {
    return getNodeManager().getAcl().getTable().getRecord(getSetAclId());
  }

  @Override
  public final int getAclId()
  {
    return _aclId;
  }

  @Override
  public final boolean setAclId(int userId,
                                OptionalInt ownerId,
                                OptionalInt aclId)
      throws MirrorNoSuchRecordException
  {
    if (accepts(getSetAcl(), userId, ownerId)) {
      _aclId = aclId.or(Acl.ACL_PUBLIC_ID);
      return true;
    }
    return false;
  }

  @Override
  public final boolean accepts(int userId,
                               OptionalInt ownerId)
      throws MirrorNoSuchRecordException
  {
    return accepts(getAcl(), userId, ownerId);
  }

  protected final boolean accepts(PersistentRecord acl,
                                  int userId,
                                  OptionalInt ownerId)
  {
    return getNodeManager().getAcl()
                           .acceptsAclUserOwner(acl.getId(), userId, ownerId.or(User.ID_ROOT));
  }
}
