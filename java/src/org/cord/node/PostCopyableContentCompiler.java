package org.cord.node;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;

/**
 * An interface that ContentCompilers can implement if they want to be copyable but they are not
 * capable of wrapping up their entire state into a single Gettable.
 * 
 * @author alex
 */
public interface PostCopyableContentCompiler
{
  /**
   * Copy the state of the ContentCompiler from the previous fromNode to the locked toNode under the
   * specified transaction. The toNode will have been initialised with the state of the fromNode,
   * and any of the existing {@link CopyableContentCompiler}s already.
   */
  public void copyContentCompiler(PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  WritableRecord toNode,
                                  Transaction t)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException;
}
