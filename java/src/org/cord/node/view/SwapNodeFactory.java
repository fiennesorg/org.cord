package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.TransientRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;

/**
 * View to swap the number of the invoking Node with the number of a sibling Node thereby
 * re-ordering the Nodes.
 */
public class SwapNodeFactory
  extends AbstractMoveFactory
{
  /**
   * The value ({@value} ) to be assigned to the CGI variable <i>view</i> to invoke this operation.
   */
  public final static String NAME = "swapNode";

  /**
   * The name ({@value} ) of the CGI variable that should contain the target Node that is to be
   * swapped with the invoking Node. This will only be honoured if the target Node and the invoking
   * Node share a common parentNodeGroup_id, thereby confirming that they are able to have their
   * number exchanged.
   */
  public final static String PARAM_CGI_TARGETNODEID = "targetNodeId";

  public SwapNodeFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  @Override
  protected RecordSource getSiblings(NodeRequest nodeRequest,
                                     PersistentRecord node)
  {
    int targetNodeId = NumberUtil.toInt(nodeRequest.get(PARAM_CGI_TARGETNODEID), -1);
    if (targetNodeId == -1) {
      throw new LogicException("Bad targetNodeId");
    }
    return (getNodeManager().getDb()
                            .getTable(Node.TABLENAME)
                            .getQuery(null,
                                      Node.PARENTNODE_ID + "=" + node.opt(Node.PARENTNODE_ID)
                                            + " AND " + TransientRecord.FIELD_ID + "="
                                            + targetNodeId,
                                      null));
  }
}
