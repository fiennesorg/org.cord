package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.node.AbcPaginator;
import org.cord.node.Acl;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.Paginator;
import org.cord.node.TemplateSuccessOperation;
import org.webmacro.Context;

public class ViewChildrenNodes
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "viewChildrenNodes";

  public static final String GETTABLE_ISMENUITEM = Node.ISMENUITEM.getKeyName();

  public static final String WEBMACRO_CHILDRENNODES = "childrenNodes";
  public static final String WEBMACRO_ABCPAGINATOR = "abcPaginator";

  public ViewChildrenNodes(NodeManager nodeMgr)
  {
    super(NAME,
          nodeMgr,
          new TemplateSuccessOperation(nodeMgr, "edit/viewChildrenNodes.wm.html"),
          Integer.valueOf(Acl.ACL_ANYLOGGEDIN_ID),
          "You must be logged in to invoke viewChildrenNodes.");
  }
  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    StringBuilder buf = new StringBuilder();
    buf.append("Node.parentNode_id=").append(node.getId());
    buf.append(" and Node.isPublished=1");
    Boolean isMenuItem = nodeRequest.getBoolean(GETTABLE_ISMENUITEM);
    AbcPaginator abcPaginator = nodeRequest.getAbcPaginator("cn");
    abcPaginator.setAbcField(Node.TITLE);
    abcPaginator.addAdditionalField(NodeRequest.CGI_VAR_VIEW, NAME);
    if (isMenuItem != null) {
      buf.append(" and Node.isMenuItem=").append(isMenuItem.booleanValue() ? '1' : '0');
      abcPaginator.addAdditionalField(GETTABLE_ISMENUITEM, isMenuItem);
    }
    String filter = buf.toString();
    RecordSource childrenNodes =
        getNodeManager().getNode().getTable().getQuery(filter, filter, Node.TITLE.getKeyName());
    context.put(WEBMACRO_CHILDRENNODES, childrenNodes);
    abcPaginator.setSourceQuery(childrenNodes);
    context.put(WEBMACRO_ABCPAGINATOR, abcPaginator);
    context.put(Paginator.WEBMACRO_ITEMNAME, "hidden page");
    context.put(Paginator.WEBMACRO_ITEMSNAME, "hidden pages");
    return handleSuccess(nodeRequest, node, context);
  }
}
