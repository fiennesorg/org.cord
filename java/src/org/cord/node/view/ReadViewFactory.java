package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.Authenticator;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.NodeStyle;
import org.cord.node.view.user.Login;
import org.cord.util.DebugConstants;
import org.webmacro.Context;

public class ReadViewFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "read";

  public ReadViewFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  public static NodeRequestSegment getInstance(NodeManager nodeManager,
                                               NodeRequest nodeRequest,
                                               PersistentRecord node,
                                               Context context)
      throws AuthenticationException
  {
    // check to see whether or not the login request bypasses the
    // authentication...
    if (!nodeRequest.getView().equals(Login.NAME)) {
      if (!isAllowed(nodeManager, node, nodeRequest.getUserId(), true)) {
        throw new AuthenticationException("You do not have permission to view this page",
                                          node,
                                          null);
      }
    }
    return new NodeRequestSegment(nodeManager,
                                  nodeRequest,
                                  node,
                                  context,
                                  getViewTemplatePath(node));
  }

  /**
   * Resolve the {@link NodeStyle#VIEWTEMPLATEPATH} from the appropriate {@link Node#NODESTYLE} for
   * the node.
   */
  public static String getViewTemplatePath(PersistentRecord node)
  {
    return node.comp(Node.NODESTYLE).comp(NodeStyle.VIEWTEMPLATEPATH);
  }

  /**
   * @return An instance of ReadViewSegment for the Node in question.
   * @throws AuthenticationException
   *           If the current session does not have read permissions on the requested node. Note
   *           that this is bypassed if the request is a login request as the current security would
   *           become outdated after the request has been processed.
   */
  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws AuthenticationException
  {
    return getInstance(getNodeManager(), nodeRequest, node, context);
  }

  /**
   * Check to see whether the given userId has permission to view the node. If the node is published
   * then the user must pass the VIEWACL and if the node is not published then the user must pass
   * the EDITACL. This method doesn't concern itself with the state of the parentage of the node and
   * only examines the permissions on the node itself.
   */
  public static boolean isAllowed(NodeManager nodeMgr,
                                  PersistentRecord node,
                                  int userId,
                                  boolean hasDebugging)
  {
    Authenticator authenticator = nodeMgr.getAuthenticator();
    Boolean isAllowedAuthenticator = authenticator.isAllowed(nodeMgr, node, NAME, userId);
    if (isAllowedAuthenticator != null) {
      if (hasDebugging) {
        DebugConstants.auth(node, ReadViewFactory.NAME, authenticator, isAllowedAuthenticator);
      }
      return isAllowedAuthenticator.booleanValue();
    }
    int aclId =
        node.is(Node.ISPUBLISHED) ? node.compInt(Node.VIEWACL_ID) : node.compInt(Node.EDITACL_ID);
    boolean isAllowedAcl =
        nodeMgr.getAcl().acceptsAclUserOwner(aclId, userId, node.compInt(Node.OWNER_ID));
    if (hasDebugging) {
      DebugConstants.auth(node,
                          ReadViewFactory.NAME,
                          nodeMgr.getAcl().getTable().getCompRecord(aclId),
                          isAllowedAcl);
    }
    return isAllowedAcl;
  }

  public static boolean isAllowed(NodeManager nodeManager,
                                  PersistentRecord node,
                                  int userId)
  {
    return isAllowed(nodeManager, node, userId, false);
  }

  @Override
  public boolean isAllowed(PersistentRecord node,
                           int userId)
  {
    return isAllowed(getNodeManager(), node, userId);
  }
}
