package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeQuestionManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * Remove a single proposed answer to a currently live NodeQuestion
 * 
 * @author alex
 * @see NodeQuestion
 * @see NodeQuestionManager
 */
public class RemoveAnswerNodeQuestion
  extends AbstractNodeQuestion
{
  public static final String NAME = "removeAnswerNodeQuestion";

  public RemoveAnswerNodeQuestion(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           NodeQuestion question)
      throws NodeRequestException
  {
    if (question != null) {
      question.removeAnswer(node);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
