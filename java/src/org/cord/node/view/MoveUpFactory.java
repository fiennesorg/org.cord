package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.operation.YoungerNodeSiblings;

public class MoveUpFactory
  extends AbstractMoveFactory
{
  public final static String NAME = "moveUp";

  public MoveUpFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  /**
   * @see YoungerNodeSiblings
   */
  @Override
  protected RecordSource getSiblings(NodeRequest nodeRequest,
                                     PersistentRecord node)
  {
    return node.opt(YoungerNodeSiblings.NAME);
  }
}
