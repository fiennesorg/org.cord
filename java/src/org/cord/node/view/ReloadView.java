package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.ReloadException;
import org.webmacro.Context;

import com.google.common.base.Preconditions;

public class ReloadView
  extends NodeRequestSegmentFactory
{
  private final String __targetView;

  private final boolean __leafNodesOnly;

  public ReloadView(String name,
                    NodeManager nodeManager,
                    String targetView,
                    boolean leafNodesOnly)
  {
    super(name,
          nodeManager);
    __targetView = Preconditions.checkNotNull(targetView, "ReloadView(targetView)");
    __leafNodesOnly = leafNodesOnly;
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws ReloadException, AuthenticationException
  {
    if (__leafNodesOnly && isSummaryRole) {
      return ReadViewFactory.getInstance(getNodeManager(), nodeRequest, node, context);
    }
    throw new ReloadException(node, __targetView, null);
  }
}
