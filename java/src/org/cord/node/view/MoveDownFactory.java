package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.operation.ElderNodeSiblings;

public class MoveDownFactory
  extends AbstractMoveFactory
{
  public final static String NAME = "moveDown";

  public MoveDownFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  /**
   * @see ElderNodeSiblings
   */
  @Override
  protected RecordSource getSiblings(NodeRequest nodeRequest,
                                     PersistentRecord node)
  {
    return node.opt(ElderNodeSiblings.NAME);
  }
}
