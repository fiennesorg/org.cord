package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * Factory that causes the current Node engine to drop all java-related state and restore itself to
 * a "just booted" status. This will have the effect of dumping all active sessions, active
 * transactions and any cached records. It is intended for when the database has been changed by an
 * external process and the node engine should be re-synchronised with the db without having to
 * reboot the process.
 */
public class ResyncFactory
  extends DynamicAclAuthenticatedFactory
{
  private final String __transactionPassword;

  public ResyncFactory(NodeManager nodeManager,
                       String transactionPassword,
                       Integer aclId)
  {
    super("resync",
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          aclId,
          "You do not have permissio to resync the database");
    __transactionPassword = transactionPassword;
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    getNodeManager().flushCaches(__transactionPassword);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
