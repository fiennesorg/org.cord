package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class InheritLogin
  extends NodeRequestSegmentFactory
{
  public static final String NAME = "inheritLogin";

  public static final String CGI_SESSIONID = "sessionId";

  public InheritLogin(NodeManager nodeManager,
                      PostViewAction postViewAction)
  {
    super(NAME,
          nodeManager,
          postViewAction);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws NodeRequestException
  {
    String sessionId = nodeRequest.getString(CGI_SESSIONID);
    if (!Strings.isNullOrEmpty(sessionId)) {
      getNodeManager().getUserManager().inheritLogin(nodeRequest.getSession(), sessionId);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
