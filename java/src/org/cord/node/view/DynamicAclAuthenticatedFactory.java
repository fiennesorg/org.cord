package org.cord.node.view;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.node.Acl;
import org.cord.node.AuthenticationException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.cord.util.StringUtils;
import org.webmacro.Context;

/**
 * Base class that supports a single constant Acl must be satisfied before allowing processing to
 * proceed.
 * 
 * @see #getInstance(NodeRequest,PersistentRecord,Context,PersistentRecord)
 */
public class DynamicAclAuthenticatedFactory
  extends NodeRequestSegmentFactory
{
  private final Table __aclTable;

  private final Integer __aclId;

  private final RecordOperationKey<PersistentRecord> __nodeFieldName;

  private final String __authenticationError;

  public static final int ACLID_DEFAULT = Acl.ACL_PUBLIC_ID;

  public static final String AUTHENTICATIONERROR_DEFAULT_FMT =
      "You do not have permission to invoke the '%s' view";

  private DynamicAclAuthenticatedFactory(String name,
                                         NodeManager nodeManager,
                                         PostViewAction postViewSuccessOperation,
                                         Integer aclId,
                                         RecordOperationKey<PersistentRecord> nodeFieldName,
                                         String authenticationError)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation);
    __aclTable = nodeManager.getDb().getTable(Acl.TABLENAME);
    __aclId = aclId;
    __nodeFieldName = nodeFieldName;
    __authenticationError =
        StringUtils.padEmpty(authenticationError,
                             String.format(AUTHENTICATIONERROR_DEFAULT_FMT, name));
  }

  /**
   * Create an instance that doesn't require any authentication. This enables sub-classes to easily
   * define systems that bypass the authentication at instantiation time rather than having to
   * include the overhead of passing the ACL_ANONYMOUS null authentication method. Instances created
   * in this way will never throw an AuthenticationException themselves and the value of acl passed
   * to getInstance will always be null.
   * 
   * @param name
   *          The name by which this view is to be invoked
   */
  public DynamicAclAuthenticatedFactory(String name,
                                        NodeManager nodeManager,
                                        PostViewAction postViewSuccessOperation)
  {
    this(name,
         nodeManager,
         postViewSuccessOperation,
         null,
         null,
         null);
  }

  /**
   * Create an instance that authenticates against an Acl that has a constant id.
   * 
   * @param name
   *          The name by which this view is to be invoked
   * @param aclId
   *          The Acl.id value for the Acl that must be satisfied before this request is permitted
   *          to be processed. Supplying a null or missing Acl.id will have always result in an
   *          AuthenticationException when invoking the Acl.
   * @param authenticationError
   *          The text that is supplied back to the user in the AuthenticationException if the Acl
   *          is not satisfied.
   */
  public DynamicAclAuthenticatedFactory(String name,
                                        NodeManager nodeManager,
                                        PostViewAction postViewSuccessOperation,
                                        Integer aclId,
                                        String authenticationError)
  {
    this(name,
         nodeManager,
         postViewSuccessOperation,
         aclId,
         null,
         authenticationError);
  }

  /**
   * Create a instance that authenticates against an Acl that is referrred to by a specified name on
   * the Node table. This will generally be one of NODE_EDITACL, NODE_DELETEACL or NODE_PUBLISHACL.
   * 
   * @param name
   *          The name by which this view is to be invoked
   * @param nodeFieldName
   *          The name of the field that is to be dynamically resolved into the Acl.
   * @param authenticationError
   *          The text that is supplied back to the user in the AuthenticationException if the Acl
   *          is not satisfied.
   * @see Node#EDITACL
   * @see Node#DELETEACL
   * @see Node#PUBLISHACL
   */
  public DynamicAclAuthenticatedFactory(String name,
                                        NodeManager nodeManager,
                                        PostViewAction postViewSuccessOperation,
                                        RecordOperationKey<PersistentRecord> nodeFieldName,
                                        String authenticationError)
  {
    this(name,
         nodeManager,
         postViewSuccessOperation,
         null,
         nodeFieldName,
         authenticationError);
  }

  public PersistentRecord authenticate(NodeManager nodeManager,
                                       NodeRequest nodeRequest,
                                       PersistentRecord node,
                                       Table aclTable,
                                       Integer aclId,
                                       RecordOperationKey<PersistentRecord> nodeFieldName,
                                       String authenticationError)
      throws AuthenticationException
  {
    return authenticate(node,
                        nodeRequest.getUserId(),
                        aclId,
                        aclTable,
                        nodeManager.getUserAcl().getTable(),
                        nodeFieldName,
                        authenticationError);
  }

  private PersistentRecord authenticate(PersistentRecord node,
                                        int userId,
                                        Integer aclId,
                                        Table aclTable,
                                        Table userAclTable,
                                        RecordOperationKey<PersistentRecord> nodeFieldName,
                                        String authenticationError)
      throws AuthenticationException
  {
    PersistentRecord acl = null;
    try {
      if (aclId != null) {
        acl = aclTable.getRecord(aclId.intValue());
      } else {
        if (nodeFieldName != null) {
          acl = node.opt(nodeFieldName);
        }
      }
    } catch (MirrorNoSuchRecordException badAclIdEx) {
      throw new AuthenticationException(authenticationError, node, badAclIdEx);
    }
    if (acl != null) {
      if (!getNodeManager().getAcl()
                           .acceptsAclUserOwner(acl.getId(), userId, node.compInt(Node.OWNER_ID))) {
        throw new AuthenticationException(authenticationError, node, null);
      }
    }
    return acl;
  }

  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node,
                                       Integer aclId,
                                       RecordOperationKey<PersistentRecord> nodeFieldName,
                                       String authenticationError)
      throws AuthenticationException
  {
    return authenticate(getNodeManager(),
                        nodeRequest,
                        node,
                        __aclTable,
                        aclId,
                        nodeFieldName,
                        authenticationError);
  }

  private PersistentRecord authenticate(NodeRequest nodeRequest,
                                        PersistentRecord node)
      throws AuthenticationException
  {
    return authenticate(nodeRequest, node, __aclId, __nodeFieldName, __authenticationError);
  }

  @Override
  public boolean isAllowed(PersistentRecord node,
                           int userId)
  {
    PersistentRecord acl = null;
    try {
      acl = authenticate(node,
                         userId,
                         __aclId,
                         getNodeManager().getAcl().getTable(),
                         getNodeManager().getUserAcl().getTable(),
                         __nodeFieldName,
                         __authenticationError);
    } catch (AuthenticationException e) {
    }
    return acl != null;
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              boolean isSummaryRole)
      throws NodeRequestException
  {
    PersistentRecord acl = authenticate(nodeRequest, node);
    return getInstance(nodeRequest, node, context, acl);
  }

  /**
   * Do the work associated with this view after it has been validated against the appropriate ACL.
   * This will be automatically invoked from the implementation of the parent's getInstance method.
   * The default implementation of this just invokes the handleSuccess method.
   * 
   * @param nodeRequest
   *          The incoming request that is being processed
   * @param node
   *          The PersistentRecord that is to be processed by this NodeRequestSegmentFactory. If the
   *          current Session has a lock on the Node in question, then this will be the
   *          WritableRecord version of the Node.
   * @param context
   *          The webmacro style context that is to be populated by the factory (if necessary).
   * @param acl
   *          The ACL that has been satisfied in advance of this method being called. If there is no
   *          ACL (ie this implementation does not require authentication) then this will be null.
   * @return The populated NodeRequestSegment. This should never be null and this should be treated
   *         as a non-recoverable error condition if this is the case.
   * @throws NodeRequestException
   *           If the NodeRequest is required to perform an action other than populating the
   *           templates.
   * @see #handleSuccess(NodeRequest,PersistentRecord,Context,Exception)
   * @see #getInstance(NodeRequest,PersistentRecord,Context, boolean)
   */
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
