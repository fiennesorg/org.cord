package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.NodeUserManager;
import org.cord.node.ReloadException;
import org.webmacro.Context;

/**
 * Attempt to steal the lock off a given Node from the User that already has the lock on the
 * Transaction.
 */
public class StealTransactionFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "stealTransaction";

  public StealTransactionFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws AuthenticationException, ReloadException
  {
    if (node.isLocked()) {
      NodeUserManager.SessionTransaction sessionTransaction =
          (getNodeManager().getContestedLockFilterManager()
                           .getStealableSessionTransaction(nodeRequest, node));
      if (sessionTransaction == null) {
        throw new AuthenticationException("You do not have the permission to acquire the lock",
                                          node,
                                          null);
      }
      Transaction transaction = sessionTransaction.getTransactionByNode(node.getId());
      sessionTransaction.removeTransaction(transaction);
      transaction.cancel();
    }
    String targetView = nodeRequest.getString("targetView");
    throw new ReloadException(node, targetView, null);
  }
}
