package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeQuestionManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RedirectionException;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

public class AnswerNodeQuestion
  extends AbstractNodeQuestion
{
  public static final String NAME = "answerNodeQuestion";

  public static final String CGI_SUBMITONLY = "submitOnly";

  public static final String CGI_SUBMITANSWERS = "submitAnswers";

  public AnswerNodeQuestion(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           NodeQuestion question)
      throws NodeRequestException
  {
    NodeQuestionManager questionManager = getNodeManager().getNodeQuestionManager();
    if (question != null) {
      Boolean submitOnly = nodeRequest.getBoolean(CGI_SUBMITONLY);
      if (submitOnly == null || !submitOnly.booleanValue()) {
        question.addAnswer(node);
      }
      if (question.maySubmit()) {
        if (question.shouldSubmit() || submitOnly == Boolean.TRUE
            || nodeRequest.getBoolean(CGI_SUBMITANSWERS) == Boolean.TRUE) {
          questionManager.removeQuestion(nodeRequest.getSession(), question);
          throw new RedirectionException(question.getAnswerUrl(), node, null);
        }
      }
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
