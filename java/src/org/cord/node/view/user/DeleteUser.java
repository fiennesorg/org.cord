package org.cord.node.view.user;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.node.UserStyle;
import org.webmacro.Context;

/**
 * Resolve a user by login, password and ipAddress and if successful then delete the user.
 */
public class DeleteUser
  extends UserView
{
  private final String __transactionPassword;

  public DeleteUser(String name,
                    String title,
                    NodeManager nodeManager,
                    PostViewAction postViewSuccessOperation,
                    Integer aclId,
                    String invokingAuthenticationException,
                    int resolveMethod,
                    PostViewAction failedResolveAction,
                    PostViewAction failedAuthenticationAction,
                    String transactionPassword)
  {
    super(name,
          title,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          invokingAuthenticationException,
          resolveMethod,
          failedResolveAction,
          failedAuthenticationAction);
    __transactionPassword = transactionPassword;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
    PersistentRecord userStyle = user.comp(User.USERSTYLE);
    if (!userStyle.is(UserStyle.MAYDELETE)) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       "%s is a protected user and may not be deleted",
                                       user);
    }
    try (Transaction transaction =
        (getNodeManager().getDb()
                         .createTransaction(Db.DEFAULT_TIMEOUT,
                                            Transaction.TRANSACTION_DELETE,
                                            __transactionPassword,
                                            "org.cord.node.view.RemoveUserFactory:" + user))) {
      transaction.delete(user, Db.DEFAULT_TIMEOUT);
      transaction.commit();
    } catch (MirrorTransactionTimeoutException lockEx) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       lockEx,
                                       false,
                                       true,
                                       "Error acquiring lock on requested user");
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}