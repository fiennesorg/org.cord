package org.cord.node.view.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.FeedbackSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.util.LogicException;
import org.webmacro.Context;

/**
 * @author alex
 * @see org.cord.node.NodeUserManager#confirmUser(NodeRequest, PersistentRecord, PersistentRecord)
 */
public class ConfirmUser
  extends UserView
{
  public final static String NAME = "confirmUser";

  public final static String TITLE = "Confirm user";

  public static final String CONTEXT_LOGIN = User.LOGIN.getKeyName();

  public static final String CONTEXT_SECRETCODE = User.SECRETCODE.getKeyName();

  public static ConfirmUser getInstanceByNodeStyle(NodeManager nodeManager,
                                                   PostViewAction postViewSuccessOperation,
                                                   int subscribeNodeStyleId)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    return new ConfirmUser(nodeManager,
                           postViewSuccessOperation,
                           nodeManager.getNodeStyle()
                                      .getFirstNode(subscribeNodeStyleId, null)
                                      .getId());
  }

  /**
   * @param subscribeNodeId
   *          The id of the node on which the confirmation process should be invoked. If this is < 1
   *          then there is no ConfirmationUrl RecordOperation registered on the User table.
   */
  public ConfirmUser(NodeManager nodeManager,
                     PostViewAction postViewSuccessOperation,
                     int subscribeNodeId)
      throws MirrorTableLockedException
  {
    super(NAME,
          TITLE,
          nodeManager,
          postViewSuccessOperation,
          null,
          null,
          RESOLVE_BYLOGIN,
          new FeedbackSuccessOperation(TITLE, "Unable to resolve user"),
          RESOLVE_NOAUTHENTICATION);
    if (subscribeNodeId > 0) {
      getNodeManager().getDb()
                      .getTable(User.TABLENAME)
                      .addRecordOperation(new ConfirmationUrl(getNodeManager(),
                                                              subscribeNodeId,
                                                              NAME));
    }
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl,
                                        PersistentRecord user,
                                        boolean isAuthenticated,
                                        boolean isCurrentUser)
      throws NodeRequestException
  {
    getNodeManager().getUserManager().confirmUser(nodeRequest, node, user);
    return handleSuccess(nodeRequest, node, context, null);
  }

  /**
   * RecordOperation named confirmationUrl that is registered on User by ConfirmUserFactory to
   * provide an address whereby the ConfirmUserFactory functionality can be invoked.
   * 
   * @author alex
   */
  public static class ConfirmationUrl
    extends SimpleRecordOperation<String>
  {
    public final static RecordOperationKey<String> NAME =
        RecordOperationKey.create("confirmationUrl", String.class);

    private final NodeManager __nodeMgr;

    private final int __subscribeNodeId;

    private final String __confirmViewName;

    protected ConfirmationUrl(NodeManager nodeMgr,
                              int subscribeNodeId,
                              String confirmViewName)
    {
      super(NAME);
      __nodeMgr = nodeMgr;
      __subscribeNodeId = subscribeNodeId;
      __confirmViewName = confirmViewName;
    }

    public NodeManager getNodeManager()
    {
      return __nodeMgr;
    }

    public void appendGetParam(StringBuilder request,
                               Object name,
                               Object value)
    {
      if (request.length() > 0 && request.charAt(request.length() - 1) != '?') {
        request.append('&');
      }
      try {
        request.append(URLEncoder.encode(name.toString(), getNodeManager().getCharacterEncoding()))
               .append('=')
               .append(URLEncoder.encode(value.toString(),
                                         getNodeManager().getCharacterEncoding()));
      } catch (UnsupportedEncodingException ueEx) {
        throw new LogicException("Character encoding not supported", ueEx);
      }
    }

    @Override
    public String getOperation(TransientRecord user,
                               Viewpoint viewpoint)
    {
      PersistentRecord subscribeNode = null;
      try {
        subscribeNode =
            (getNodeManager().getDb().getTable(Node.TABLENAME).getRecord(__subscribeNodeId,
                                                                         viewpoint));
      } catch (MirrorNoSuchRecordException missingSubscribeNodeEx) {
        throw new LogicException("Subscribe Node #" + __subscribeNodeId + " is missing",
                                 missingSubscribeNodeEx);
      }
      StringBuilder url = new StringBuilder();
      url.append(getNodeManager().getHttpServletPath())
         .append(subscribeNode.opt(Node.PATH))
         .append('?');
      appendGetParam(url, NodeRequest.CGI_VAR_VIEW, __confirmViewName);
      appendGetParam(url, User.LOGIN, user.opt(User.LOGIN));
      appendGetParam(url, User.SECRETCODE, user.opt(User.SECRETCODE));
      appendCustomGetParams(user, viewpoint, subscribeNode, url);
      return url.toString();
    }

    protected void appendCustomGetParams(TransientRecord user,
                                         Viewpoint viewpoint,
                                         PersistentRecord subscribeNode,
                                         StringBuilder url)
    {
    }
  }
}
