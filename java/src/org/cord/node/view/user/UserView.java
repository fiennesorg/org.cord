package org.cord.node.view.user;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.node.FeedbackSuccessOperation;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.node.operation.UserPasswordValidator;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.StringUtils;
import org.webmacro.Context;

import com.google.common.base.Strings;

/**
 * Base class of user factory view that in its default implementation resolves a second user which
 * is placed in the context and then the success operation invoked. The second user is optionally
 * authenticated against a supplied password. The user can be identified by by either their login or
 * their id and the password is identified with "password". Override the protected getInstance
 * method if you want some dedicated functionality in your subclass.
 */
public class UserView
  extends DynamicAclAuthenticatedFactory
{
  public final static String CGI_LOGIN = User.LOGIN.getKeyName();

  public final static String CGI_ID = TransientRecord.FIELD_ID.getKeyName();

  public final static String CGI_PASSWORD = User.PASSWORD.getKeyName();

  public final static String CONTEXT_USER = "user";

  public final static String CONTEXT_ISAUTHENTICATED = "isAuthenticated";

  public final static String CONTEXT_ISCURRENTUSER = "isCurrentUser";

  /**
   * The user is deduced by looking up the value of CGI_LOGIN and then matching that against
   * User.LOGIN in the database.
   */
  public static final int RESOLVE_BYLOGIN = 0;

  /**
   * The user is looked up by id which is read as an integer from CGI_ID
   */
  public static final int RESOLVE_BYID = 1;

  /**
   * The user is the current user that is currently associated with the incoming NodeRequest
   */
  public static final int RESOLVE_CURRENTUSER = 2;

  /**
   * The PersistentRecord User is expected to be found in the Context labelled as CONTEXT_USER. This
   * is intended for situations when the view is chained after a previous view that will have
   * resolved the User.
   */
  public static final int RESOLVE_FROMCONTEXT = 3;

  public static final PostViewAction RESOLVE_NOAUTHENTICATION = null;

  private final int __resolveMethod;

  private final PostViewAction __failedResolveAction;

  private final PostViewAction __failedAuthenticationAction;

  private final String __title;

  /**
   * @param failedAuthenticationAction
   *          The action that is invoked if the password is not correct for the user. If this is
   *          null then there will be no password authentication against the user. Note that if this
   *          is defined and acceptsCurrentUser is true then you will still have to re-authenticate
   *          as the current user despite already being logged in.
   */
  public UserView(String name,
                  String title,
                  NodeManager nodeManager,
                  PostViewAction successOperation,
                  Integer aclId,
                  String invokingAuthenticationException,
                  int resolveMethod,
                  PostViewAction failedResolveAction,
                  PostViewAction failedAuthenticationAction)
  {
    super(name,
          nodeManager,
          successOperation,
          aclId,
          invokingAuthenticationException);
    __title = StringUtils.padEmpty(title, name);
    __resolveMethod = resolveMethod;
    __failedResolveAction = failedResolveAction == null
        ? new FeedbackSuccessOperation(title, "Failed to resolve user")
        : failedResolveAction;
    __failedAuthenticationAction = failedAuthenticationAction;
  }

  public final String getTitle()
  {
    return __title;
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              PersistentRecord acl)
      throws NodeRequestException
  {
    PersistentRecord user = null;
    boolean isAuthenticated = false;
    boolean isCurrentUser = false;
    Table userTable = getNodeManager().getUser().getTable();
    try {
      switch (__resolveMethod) {
        case RESOLVE_BYID:
          user = userTable.getRecord(nodeRequest.get(CGI_ID));
          break;
        case RESOLVE_BYLOGIN:
          String login = nodeRequest.getString(CGI_LOGIN);
          if (!Strings.isNullOrEmpty(login)) {
            user = TableStringMatcher.firstMatch(userTable, User.LOGIN, login);
          }
          break;
        case RESOLVE_CURRENTUSER:
          user = nodeRequest.getUser();
          isCurrentUser = true;
          break;
        case RESOLVE_FROMCONTEXT:
          Object obj = context.get(CONTEXT_USER);
          if (obj != null) {
            if (obj instanceof PersistentRecord) {
              user = (PersistentRecord) obj;
              if (user.getTable() != userTable) {
                user = null;
              }
            } else {
              user = userTable.getRecord(obj);
            }
          }
          break;
      }
    } catch (MirrorNoSuchRecordException mnsrEx) {
      return __failedResolveAction.handleSuccess(nodeRequest, node, context, this, mnsrEx);
    }
    if (user == null) {
      return __failedResolveAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    if (__failedAuthenticationAction != null) {
      if (UserPasswordValidator.accepts(user,
                                        Strings.nullToEmpty(nodeRequest.getString(CGI_PASSWORD)),
                                        nodeRequest.getRequest().getRemoteAddr(),
                                        getNodeManager())) {
        isAuthenticated = true;
      } else {
        return __failedAuthenticationAction.handleSuccess(nodeRequest, node, context, this, null);
      }
    }
    context.put(CONTEXT_USER, user);
    context.put(CONTEXT_ISCURRENTUSER, Boolean.valueOf(isCurrentUser));
    context.put(CONTEXT_ISAUTHENTICATED, Boolean.valueOf(isAuthenticated));
    return getInstance(nodeRequest, node, context, acl, user, isAuthenticated, isCurrentUser);
  }

  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
