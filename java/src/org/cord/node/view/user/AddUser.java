package org.cord.node.view.user;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.node.ContextFeedbackSuccessOperation;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.node.UserStyle;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

/**
 * View to add a new Node User to the system. The system is going to expect the following CGI
 * inputs:-
 * <dl>
 * <dt>USER_LOGIN ("login")</dt>
 * <dd>The login that will be utilised for the user: this must be a well-formed email address unique
 * to the system</dd>
 * <dt>USER_USERSTYLE_ID ("userStyle_id")</dt>
 * <dd>The valid UserStyle id that this user will conform to. Valid options are:-
 * <ol>
 * <li>Paranoid</li>
 * <li>HighSecurity</li>
 * <li>MediumSecurity</li>
 * <li>LowSecurity</li>
 * </ol>
 * If the requested security level is lower than the minimumUserStyle_id (lower numbers are more
 * powerful) then it will be capped at minimumUserStyle_id. If no value is supplied then a value of
 * LowSecurity is assumed. If the value supplied doesn't match a registered UserStyle then the
 * request fails.</dd>
 * <dt>password1</dt>
 * <dd>The initial entry of the password for the system. An empty entry will be rejected if
 * acceptsEmptyPasswords is set to false.</dd>
 * <dt>password2</dt>
 * <dd>The second entry of the password for the system. If this does not match the value of
 * password1 then the request will be rejected</dd>
 * <dt>USER_NAME ("name")</dt>
 * <dd>The human-readable name by which the user is to be known. If not defined then this is set to
 * the email value.</dd>
 * </dl>
 */
public class AddUser
  extends DynamicAclAuthenticatedFactory
{
  public static final String CGI_LOGIN = User.LOGIN.getKeyName();

  public static final String CGI_PASSWORD = User.PASSWORD.getKeyName();

  public static final String CGI_CONFIRMPASSWORD = "confirmpassword";

  public static final String OUT_USER = "user";

  private final int __minimumUserStyle;

  private final boolean __acceptsEmptyPasswords;

  private final Integer __initialUserStatus;

  private final Table __userTable;

  private final PostViewAction __existingInitUserAction;

  private final PostViewAction __existingConfirmedUserAction;

  private final PostViewAction __existingSuspendedUserAction;

  private final PostViewAction __dataErrorAction;

  private final Gettable __makePersistentParams;

  /**
   * @param name
   *          The name by which this view is to be invoked.
   * @param nodeManager
   *          The NodeManager that this Factory will be registered in.
   * @param postViewSuccessOperation
   *          The view that will be invoked on sucessfull completion of the process.
   * @param aclId
   *          The id of the ACL that must be satisfied to invoke this view. If null then no
   *          authentication is required.
   * @param authenticationError
   *          The error message that is returned if the aclId is not met.
   * @param transactionPassword
   *          The transactionPassword for this system.
   * @param minimumUserStyle
   *          The value below which the UserStyle may not be initialised to.
   * @param acceptsEmptyPasswords
   *          If true then a user with no password may be created. If false then this will then
   *          dataErrorAction view will be invoked.
   * @param initialUserStatus
   *          The STATUS_xxx that the user will be initially created with.
   * @param makePersistentParams
   *          optional set of params that will be passed as the initialisation param to a new User
   *          record if created.
   * @param existingInitUserAction
   *          The action that is to be taken if the requested user is already subscribed, but has
   *          not yet confirmed their subscription yet.
   * @param existingConfirmedUserAction
   *          The action that is to be taken if the requested user has already subscribed and
   *          confirmed their subscription.
   * @param existingSuspendedUserAction
   *          The action that is to be taken if the requested user has been suspended in the past.
   * @param dataErrorAction
   *          The action that is to be taken if the data submitted to the system is invalid in some
   *          way making it impossible to process the application.
   * @see User#STATUS_INIT
   * @see User#STATUS_SUSPENDED
   * @see User#STATUS_CONFIRMED
   */
  public AddUser(String name,
                 NodeManager nodeManager,
                 PostViewAction postViewSuccessOperation,
                 Integer aclId,
                 String authenticationError,
                 String transactionPassword,
                 int minimumUserStyle,
                 boolean acceptsEmptyPasswords,
                 int initialUserStatus,
                 Gettable makePersistentParams,
                 PostViewAction existingInitUserAction,
                 PostViewAction existingConfirmedUserAction,
                 PostViewAction existingSuspendedUserAction,
                 PostViewAction dataErrorAction)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
    __minimumUserStyle = Math.max(minimumUserStyle, 1);
    __acceptsEmptyPasswords = acceptsEmptyPasswords;
    __initialUserStatus = Integer.valueOf(Math.min(Math.max(initialUserStatus, User.STATUS_INIT),
                                                   User.STATUS_SUSPENDED));
    __userTable = getNodeManager().getDb().getTable(User.TABLENAME);
    __existingInitUserAction = padWithDefault(existingInitUserAction);
    __existingConfirmedUserAction = padWithDefault(existingConfirmedUserAction);
    __existingSuspendedUserAction = padWithDefault(existingSuspendedUserAction);
    __dataErrorAction = padWithDefault(dataErrorAction);
    __makePersistentParams = makePersistentParams;
  }

  /**
   * Create a fully authenticated version of the add user operation that specifies the required
   * level of authentication and the action to complete afterwards.
   */
  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    String email = nodeRequest.getString(CGI_LOGIN);
    if (Strings.isNullOrEmpty(email)) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "You must define an email for your user account");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    PersistentRecord persistentUser = null;
    try {
      persistentUser = TableStringMatcher.firstMatch(__userTable, User.EMAIL, email);
      context.put("user", persistentUser);
      switch (persistentUser.compInt(User.STATUS)) {
        case User.STATUS_INIT:
          context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                      "There is already a user with the email address of " + email
                                                                   + ", but they haven't confirmed their subscription yet.");
          return __existingInitUserAction.handleSuccess(nodeRequest, node, context, this, null);
        case User.STATUS_CONFIRMED:
          context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                      "There is already an active user with the email address of " + email);
          return __existingConfirmedUserAction.handleSuccess(nodeRequest,
                                                             node,
                                                             context,
                                                             this,
                                                             null);
        case User.STATUS_SUSPENDED:
          context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                      "There is already a user with the email address of " + email
                                                                   + ", but their account is suspended.");
          return __existingSuspendedUserAction.handleSuccess(nodeRequest,
                                                             node,
                                                             context,
                                                             this,
                                                             null);
        default:
          throw new LogicException("Unknown user status on " + persistentUser + " = "
                                   + persistentUser.compInt(User.STATUS));
      }
    } catch (MirrorNoSuchRecordException newUserEx) {
      // we are not fussed about this and let execution continue...
    }
    TransientRecord transientUser = __userTable.createTransientRecord();
    try {
      transientUser.setField(User.USERSTYLE_ID,
                             Integer.valueOf(Math.max(NumberUtil.toInt(nodeRequest.get(User.USERSTYLE_ID),
                                                                       UserStyle.ID_LOW),
                                                      __minimumUserStyle)));
    } catch (MirrorFieldException badUserStyleEx) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME, "Bad user style:" + badUserStyleEx);
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    try {
      transientUser.setField(User.EMAIL, email);
    } catch (MirrorFieldException badEmailEx) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "The email address of \"" + email + "\" is already in use.  Please subscribe with a new email address.");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    try {
      transientUser.setField(User.LOGIN, email);
    } catch (MirrorFieldException badLoginEx) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "The email address of \"" + email + "\" is already in use.");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    String name = MoreObjects.firstNonNull(nodeRequest.getString(User.NAME), email);
    try {
      transientUser.setField(User.NAME, name);
    } catch (MirrorFieldException badNameEx) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "The name of " + name + " has been rejected");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    String password1 = Strings.nullToEmpty(nodeRequest.getString(CGI_PASSWORD));
    String password2 = Strings.nullToEmpty(nodeRequest.getString(CGI_CONFIRMPASSWORD));
    if (!password1.equals(password2)) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME, "The two passwords must match");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    if ("".equals(password1) && !__acceptsEmptyPasswords) {
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME, "You must provide a password");
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    try {
      transientUser.setField(User.PASSWORD, password1);
      transientUser.setField(User.STATUS, __initialUserStatus);
    } catch (MirrorFieldException mfEx) {
      mfEx.printStackTrace();
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "Your user instantiation variables where rejected:" + mfEx);
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    try {
      persistentUser = transientUser.makePersistent(__makePersistentParams);
    } catch (MirrorInstantiationException miEx) {
      miEx.printStackTrace();
      context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
                  "Your user instantiation variables where rejected:" + miEx);
      return __dataErrorAction.handleSuccess(nodeRequest, node, context, this, null);
    }
    context.put(OUT_USER, persistentUser);
    // Region
    // Iterator factories = getNodeManager().getUserRegionFactories();
    // if (factories.hasNext()) {
    // Transaction editTransaction = null;
    // editTransaction = (getNodeManager()
    // .getDb()
    // .createTransaction(Db.DEFAULT_TIMEOUT,
    // Transaction.TRANSACTION_UPDATE,
    // __transactionPassword,
    // __englishName));
    // try {
    // while (factories.hasNext()) {
    // UserRegionFactory factory = (UserRegionFactory) factories.next();
    // PersistentRecord region = factory.getRegion(persistentUser);
    // if (region != null) {
    // ContentCompiler contentCompiler = (ContentCompiler)
    // region.get(Region.CONTENTCOMPILER);
    // contentCompiler.updateInstance(editTransaction.edit(region,
    // Db.DEFAULT_TIMEOUT),
    // editTransaction,
    // nodeRequest);
    // }
    // }
    // editTransaction.commit();
    // editTransaction = null;
    // } catch (MirrorException mEx) {
    // context.put(ContextFeedbackSuccessOperation.MESSAGENAME,
    // "Problem booting new user: " + mEx);
    // return __dataErrorAction.handleSuccess(nodeRequest,
    // node,
    // nodeRole,
    // context,
    // this,
    // null);
    // } finally {
    // if (editTransaction != null) {
    // editTransaction.cancel();
    // }
    // }
    // }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
