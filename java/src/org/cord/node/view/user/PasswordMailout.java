package org.cord.node.view.user;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.FeedbackSuccessOperation;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.util.LogicException;
import org.cord.util.StringUtils;
import org.webmacro.Context;

/**
 * Resolve a user and then send them a copy of their password if permitted and confirmed.
 * 
 * @author alex
 */
public class PasswordMailout
  extends UserView
{
  public final static String EMAILTEMPLATEPATH_DEFAULT = "node/email/passwordMailout.wm.txt";

  public final static String SUBJECT_DEFAULT = "Password Reminder";

  private final String __emailTemplatePath;

  private final String __from;

  private final String __cc;

  private final String __bcc;

  private final String __subject;

  private final PostViewAction __initUserAction;

  private final PostViewAction __suspendedUserAction;

  public PasswordMailout(String name,
                         String title,
                         NodeManager nodeManager,
                         PostViewAction unknownLoginAction,
                         PostViewAction sentPasswordAction,
                         PostViewAction initUserAction,
                         PostViewAction suspendedUserAction,
                         String emailTemplatePath,
                         String from,
                         String cc,
                         String bcc,
                         String subject)
  {
    super(name,
          title,
          nodeManager,
          sentPasswordAction == null
              ? new FeedbackSuccessOperation(title, "The password reminder has been sent")
              : sentPasswordAction,
          null,
          null,
          RESOLVE_BYLOGIN,
          unknownLoginAction,
          RESOLVE_NOAUTHENTICATION);
    __initUserAction = initUserAction == null
        ? new FeedbackSuccessOperation(title, "The user has not been confirmed yet")
        : initUserAction;
    __suspendedUserAction = suspendedUserAction == null
        ? new FeedbackSuccessOperation(title, "The user has been suspended")
        : suspendedUserAction;
    __emailTemplatePath = StringUtils.padEmpty(emailTemplatePath, EMAILTEMPLATEPATH_DEFAULT);
    __from = StringUtils.padEmpty(from, nodeManager.getSiteAdminEmail());
    __cc = cc;
    __bcc = bcc;
    __subject = StringUtils.padEmpty(subject, SUBJECT_DEFAULT);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
    switch (user.compInt(User.STATUS)) {
      case User.STATUS_CONFIRMED:
        try {
          throw new LogicException("Refactor to use Mailgun API");
//          getNodeManager().getNodeMailManager().templateMailUser(user,
//                                                                 __cc,
//                                                                 __bcc,
//                                                                 context,
//                                                                 __emailTemplatePath,
//                                                                 __from,
//                                                                 __subject,
//                                                                 NodeMailManager.PRIORITY_HIGH);
        } catch (Exception e) {
          throw new FeedbackErrorException(getTitle(),
                                           node,
                                           e,
                                           true,
                                           true,
                                           "It has not been possible to send your password mailout");
        }
        // return handleSuccess(nodeRequest, node, context, null);
      case User.STATUS_INIT:
        return __initUserAction.handleSuccess(nodeRequest, node, context, this, null);
      case User.STATUS_SUSPENDED:
        return __suspendedUserAction.handleSuccess(nodeRequest, node, context, this, null);
      default:
        throw new LogicException("Unknown user status for " + user + " of "
                                 + user.opt(User.STATUS));
    }
  }
}
