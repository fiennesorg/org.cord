package org.cord.node.view.user;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationPostViewAction;
import org.cord.node.ConstantValueFactory;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.cord.node.RedirectionException;
import org.cord.node.ReloadException;
import org.cord.node.User;
import org.cord.util.LogicException;
import org.webmacro.Context;

public class Login
  extends UserView
{
  public final static String NAME = "login";

  public final static String TITLE = "Login";

  /**
   * The optional URL that should be redirected to once the Login functionality is completed. This
   * prevents you from being stuck back at the top level of the failed authentication point rather
   * than the page which you actually asked for. This will only be utilised if the postLoginAction
   * is not defined in the creator.
   */
  public final static String TARGETURL = "httpServletRequest_URL_query";

  private final PostViewAction __initUserAction;

  private final PostViewAction __suspendedUserAction;

  public Login(NodeManager nodeManager)
  {
    this(NAME,
         TITLE,
         nodeManager,
         null,
         null,
         null,
         null,
         null);
  }

  public Login(String name,
               String title,
               NodeManager nodeManager,
               PostViewAction postLoginAction,
               PostViewAction unknownLoginAction,
               PostViewAction wrongPasswordAction,
               PostViewAction initUserAction,
               PostViewAction suspendedUserAction)
  {
    super(name,
          title,
          nodeManager,
          postLoginAction == null ? new RedirectToTargetUrl() : postLoginAction,
          null,
          null,
          RESOLVE_BYLOGIN,
          unknownLoginAction == null
              ? new AuthenticationPostViewAction(nodeManager,
                                                 new ConstantValueFactory("Your login and / or your password is incorrect."))
              : unknownLoginAction,
          wrongPasswordAction == null
              ? new AuthenticationPostViewAction(nodeManager,
                                                 new ConstantValueFactory("Your login and / or your password is incorrect."))
              : wrongPasswordAction);
    __initUserAction = initUserAction == null
        ? new AuthenticationPostViewAction(nodeManager,
                                           new ConstantValueFactory("Your account has not been confirmed."))
        : initUserAction;
    __suspendedUserAction = suspendedUserAction == null
        ? new AuthenticationPostViewAction(nodeManager,
                                           new ConstantValueFactory("Your account has been suspended."))
        : suspendedUserAction;
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl,
                                        PersistentRecord user,
                                        boolean isAuthenticated,
                                        boolean isCurrentUser)
      throws NodeRequestException
  {
    switch (user.compInt(User.STATUS)) {
      case User.STATUS_INIT:
        return __initUserAction.handleSuccess(nodeRequest, node, context, this, null);
      case User.STATUS_SUSPENDED:
        return __suspendedUserAction.handleSuccess(nodeRequest, node, context, this, null);
      case User.STATUS_CONFIRMED:
        getNodeManager().getUserManager().changeLogin(nodeRequest, node, user);
        break;
      default:
        throw new LogicException("Unknown user status:" + user + ":" + user.opt(User.STATUS));
    }
    return handleSuccess(nodeRequest, node, context, null);
  }

  public static class RedirectToTargetUrl
    implements PostViewAction
  {
    @Override
    public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                            PersistentRecord node,
                                            Context context,
                                            NodeRequestSegmentFactory factory,
                                            Exception nestedException)
        throws NodeRequestException
    {
      String targetUrl = nodeRequest.getString(TARGETURL);
      if (targetUrl == null || targetUrl.indexOf("view=login") != -1) {
        throw new ReloadException(node, null);
      }
      throw new RedirectionException(targetUrl, node, null);
    }

    @Override
    public void shutdown()
    {
    }

  }
}
