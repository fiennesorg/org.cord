package org.cord.node.view.user;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.FeedbackSuccessOperation;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

/**
 * Resolve a user and then update their status according to a value passed in on the NodeRequest.
 * 
 * @author alex
 */
public class SetUserStatus
  extends UserView
{
  public static final String CGI_STATUS = User.STATUS.getKeyName();

  private final String __transactionPassword;

  private final int __minimumStatus;

  private final int __maximumStatus;

  public SetUserStatus(String name,
                       String title,
                       NodeManager nodeManager,
                       PostViewAction postViewSuccessOperation,
                       Integer aclId,
                       String authenticationError,
                       String transactionPassword,
                       int minimumStatus,
                       int maximumStatus)
  {
    super(name,
          title,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError,
          RESOLVE_BYLOGIN,
          new FeedbackSuccessOperation(title, "Unable to resolve user"),
          RESOLVE_NOAUTHENTICATION);
    __transactionPassword = transactionPassword;
    __minimumStatus = minimumStatus;
    __maximumStatus = maximumStatus;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           final PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
    int userStatus = NumberUtil.toInt(nodeRequest.get(CGI_STATUS), -1);
    if (userStatus == -1) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       "No target status defined for user: %s",
                                       user);
    }
    userStatus = NumberUtil.constrain(userStatus, __minimumStatus, __maximumStatus);
    try (Transaction transaction =
        getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                   Transaction.TRANSACTION_UPDATE,
                                                   __transactionPassword,
                                                   "SetUserStatus:" + user)) {
      WritableRecord writableUser = transaction.edit(user, Db.DEFAULT_TIMEOUT);
      writableUser.setField(User.STATUS, Integer.valueOf(userStatus));
      transaction.commit();
    } catch (MirrorFieldException mfEx) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       mfEx,
                                       true,
                                       true,
                                       "Unable to set user status: %s",
                                       mfEx);
    } catch (MirrorTransactionTimeoutException mttEx) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       mttEx,
                                       true,
                                       true,
                                       "Unable to get Db lock on user record: %s",
                                       user);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}