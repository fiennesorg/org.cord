package org.cord.node.view.user;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.util.LogicException;
import org.webmacro.Context;
import org.webmacro.WebMacroException;

/**
 * Class that attempts to send a templated mailout to a given user.
 */
public class MailUser
  extends UserView
{
  private final String __cc;

  private final String __bcc;

  private final String __templatePath;

  private final String __from;

  private final String __subject;

  private final int __priority;

  /**
   * @see UserView#UserView(String, String, NodeManager, PostViewAction, Integer, String, int,
   *      PostViewAction, PostViewAction)
   */
  public MailUser(String name,
                  String title,
                  NodeManager nodeManager,
                  PostViewAction postViewSuccessOperation,
                  Integer aclId,
                  String authenticationError,
                  int resolveMethod,
                  PostViewAction failedResolveAction,
                  PostViewAction failedAuthenticationAction,
                  String cc,
                  String bcc,
                  String templatePath,
                  String from,
                  String subject,
                  int priority)
  {
    super(name,
          title,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError,
          resolveMethod,
          failedResolveAction,
          failedAuthenticationAction);
    __cc = cc;
    __bcc = bcc;
    __templatePath = templatePath;
    __from = from;
    __subject = subject;
    __priority = priority;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
//    try {
      throw new LogicException("Refactor to use Mailgun API");
//      getNodeManager().getNodeMailManager().templateMailUser(user,
//                                                             __cc,
//                                                             __bcc,
//                                                             context,
//                                                             __templatePath,
//                                                             __from,
//                                                             __subject,
//                                                             __priority);
//    } catch (WebMacroException resEx) {
//      throw new FeedbackErrorException(null,
//                                       node,
//                                       resEx,
//                                       true,
//                                       true,
//                                       "Error constructing email: %s",
//                                       resEx);
//    }
//    return handleSuccess(nodeRequest, node, context, null);
  }
}
