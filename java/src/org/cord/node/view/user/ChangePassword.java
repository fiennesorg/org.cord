package org.cord.node.view.user;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class ChangePassword
  extends UserView
{
  public final static String NAME = "changePassword";

  public final static String TITLE = "Change password";

  public final static String CGI_NEWPASSWORD = "newpassword";

  public final static String CGI_CONFIRMNEWPASSWORD = "confirmnewpassword";

  private final String __transactionPassword;

  public ChangePassword(NodeManager nodeManager,
                        PostViewAction postViewSuccessOperation,
                        Integer aclId,
                        String transactionPassword,
                        int resolveMethod,
                        PostViewAction failedResolveAction,
                        PostViewAction failedAuthenticationAction)
  {
    super(NAME,
          TITLE,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          "You do not have permission to change the password",
          resolveMethod,
          failedResolveAction,
          failedAuthenticationAction);
    __transactionPassword = transactionPassword;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           final PersistentRecord user,
                                           boolean isAuthenticated,
                                           boolean isCurrentUser)
      throws NodeRequestException
  {
    String newPassword = Strings.nullToEmpty(nodeRequest.getString(CGI_NEWPASSWORD));
    String newPasswordConf = Strings.nullToEmpty(nodeRequest.getString(CGI_CONFIRMNEWPASSWORD));
    if (!newPassword.equals(newPasswordConf)) {
      throw new FeedbackErrorException("Change Password",
                                       node,
                                       "The password and the password confirmation do not match");
    }
    try (Transaction transaction =
        (getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                    Transaction.TRANSACTION_UPDATE,
                                                    __transactionPassword,
                                                    "ChangePassword"))) {
      WritableRecord rwUser = transaction.edit(user, Db.DEFAULT_TIMEOUT);
      rwUser.setField(User.PASSWORD, newPassword);
      transaction.commit();
    } catch (MirrorFieldException mfEx) {
      throw new FeedbackErrorException("Change Password",
                                       node,
                                       mfEx,
                                       false,
                                       true,
                                       "Your new password has been rejected");
    } catch (MirrorTransactionTimeoutException mttEx) {
      throw new FeedbackErrorException("Change Password",
                                       node,
                                       mttEx,
                                       false,
                                       true,
                                       "Another user currently has the lock on the password, please try later");
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
