package org.cord.node.view.user;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.cord.node.RedirectionSuccessOperation;
import org.webmacro.Context;

public class Logout
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "logout";

  public Logout(NodeManager nodeManager)
  {
    this(NAME,
         nodeManager,
         new RedirectionSuccessOperation(nodeManager));
  }

  public Logout(String name,
                NodeManager nodeManager,
                PostViewAction postViewSuccessOperation)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws NodeRequestException
  {
    getNodeManager().getUserManager().logout(nodeRequest);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
