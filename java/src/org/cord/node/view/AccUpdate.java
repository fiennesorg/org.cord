package org.cord.node.view;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.webmacro.Context;

/**
 * NodeUpdate that provides the added functionality of resolving a given region on the childNode
 * before passing it onto the subclass. Implementations of this are responsible for implementing
 * getNodeFilename, getRegionName and getInstance
 * 
 * @see #getRegionName(NodeRequest,PersistentRecord,PersistentRecord,PersistentRecord)
 * @see #getInstance(NodeRequest,PersistentRecord,PersistentRecord,WritableRecord,WritableRecord,Context,Transaction)
 */
public abstract class AccUpdate
  extends NodeUpdate
{
  // private final Table __regionTable;
  public AccUpdate(String name,
                   NodeManager nodeManager,
                   String transactionPassword,
                   Integer parentNodeId,
                   long editLockTimeout,
                   boolean stealContestedLocks)
  {
    super(name,
          nodeManager,
          transactionPassword,
          parentNodeId,
          editLockTimeout,
          stealContestedLocks);
    // __regionTable = nodeManager.getDb().getTable(Region.TABLENAME);
  }

  /**
   * Resolve the name of the TableContentCompiler region on childNode that this NodeRequest is
   * relevant to.
   */
  protected abstract String getRegionName(NodeRequest nodeRequest,
                                          PersistentRecord invokingNode,
                                          PersistentRecord parentNode,
                                          PersistentRecord childNode)
      throws NodeRequestException;

  /**
   * Resolve the TableContentCompiler region on childNode as given by getRegionName and then
   * delegate to getInstance. This is performed as follows:-
   * <ul>
   * <li>Get the regionName from getRegionName(...)
   * <li>Utilise the RegionResolver to extract the appropriate Region record.
   * <li>Utilise the RegionCompile method to compile the Region into the TableContentCompiler
   * Instance record. This will be a WritableRecord because of the lock held on the childNode by the
   * parent class
   * <li>Invoke getInstance(...)
   * </ul>
   * 
   * @see #getRegionName(NodeRequest,PersistentRecord,PersistentRecord,PersistentRecord)
   * @see #getInstance(NodeRequest,PersistentRecord,PersistentRecord,WritableRecord,WritableRecord,Context,Transaction)
   */
  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord invokingNode,
                                                 PersistentRecord parentNode,
                                                 WritableRecord childNode,
                                                 Context context,
                                                 Transaction transaction)
      throws NodeRequestException
  {
    String regionName = getRegionName(nodeRequest, invokingNode, parentNode, childNode);
    return getInstance(nodeRequest,
                       invokingNode,
                       parentNode,
                       childNode,
                       regionName,
                       context,
                       transaction);
  }

  protected final static WritableRecord getAccInstance(NodeManager nodeManager,
                                                       PersistentRecord node,
                                                       String regionName,
                                                       Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    return (WritableRecord) nodeManager.getNode().compile(node, regionName, null, transaction);
  }

  protected final WritableRecord getAccInstance(PersistentRecord invokingNode,
                                                PersistentRecord childNode,
                                                String regionName,
                                                Transaction transaction)
      throws FeedbackErrorException
  {
    try {
      return getAccInstance(getNodeManager(), childNode, regionName, transaction);
    } catch (MirrorNoSuchRecordException badRegionNameEx) {
      throw new FeedbackErrorException(null,
                                       invokingNode,
                                       badRegionNameEx,
                                       true,
                                       true,
                                       "Cannot resolve regionName: %s",
                                       regionName);
    }
  }

  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord invokingNode,
                                                 PersistentRecord parentNode,
                                                 WritableRecord childNode,
                                                 String regionName,
                                                 Context context,
                                                 Transaction transaction)
      throws NodeRequestException
  {
    WritableRecord instance = getAccInstance(invokingNode, childNode, regionName, transaction);
    return getInstance(nodeRequest,
                       invokingNode,
                       parentNode,
                       childNode,
                       instance,
                       context,
                       transaction);
  }

  /**
   * Perform any processing on the locked TableContentCompiler instance record that has been
   * supplied. The transaction should be committed at the end of the Transaction.
   */
  public abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord invokingNode,
                                                 PersistentRecord parentNode,
                                                 WritableRecord childNode,
                                                 WritableRecord instance,
                                                 Context context,
                                                 Transaction transaction)
      throws NodeRequestException;
}
