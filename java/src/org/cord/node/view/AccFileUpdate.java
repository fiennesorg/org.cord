package org.cord.node.view;

import java.io.File;
import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.webmacro.Context;

import com.google.common.collect.Iterators;

/**
 * Implementation of AccUpdate that utilises a constant region name and extracts the filename of the
 * Node from a filename of an uploaded File. Implementations are responsible for getInstance.
 * 
 * @see #getInstance(NodeRequest,PersistentRecord,PersistentRecord,WritableRecord,WritableRecord,Context,Transaction)
 */
public abstract class AccFileUpdate
  extends AccUpdate
{
  private final String __regionName;

  private final String __fileParameterName;

  public AccFileUpdate(String name,
                       NodeManager nodeManager,
                       String transactionPassword,
                       Integer parentNodeId,
                       long editLockTimeout,
                       boolean stealContestedLocks,
                       String regionName,
                       String fileParameterName)
  {
    super(name,
          nodeManager,
          transactionPassword,
          parentNodeId,
          editLockTimeout,
          stealContestedLocks);
    __regionName = regionName;
    __fileParameterName = fileParameterName;
  }

  /**
   * @return the regionName as passed to the constructor.
   */
  @Override
  protected final String getRegionName(NodeRequest nodeRequest,
                                       PersistentRecord invokingNode,
                                       PersistentRecord parentNode,
                                       PersistentRecord childNode)
  {
    return getRegionName();
  }

  protected final String getRegionName()
  {
    return __regionName;
  }

  /**
   * Return the File from the NodeRequest that is uploaded under the fileParameterName that is
   * passed to the constructor.
   * 
   * @see NodeRequest#getFile(Object)
   * @return The given File or null if it wasn't uploaded.
   */
  protected final File getFile(NodeRequest nodeRequest)
  {
    return nodeRequest.getFile(__fileParameterName);
  }

  @Override
  protected final Iterator<String> getNodeFilenames(NodeRequest nodeRequest,
                                                    PersistentRecord invokingNode,
                                                    PersistentRecord parentNode)
      throws NodeRequestException
  {
    File file = getFile(nodeRequest);
    if (file == null) {
      throw new FeedbackErrorException(null,
                                       invokingNode,
                                       "No file uploaded under parameter %s",
                                       __fileParameterName);
    }
    return Iterators.singletonIterator(getNodeFilename(file));
  }

  protected static final String getNodeFilename(File file)
  {
    String filename = file.getName();
    int dotIndex = filename.indexOf('.');
    return (dotIndex == -1 ? filename : filename.substring(0, dotIndex));
  }
}
