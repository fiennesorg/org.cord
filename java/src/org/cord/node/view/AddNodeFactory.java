package org.cord.node.view;

import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RelatedMirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeGroupStyle.CreateNodePolicy;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.NodeRequestSegmentFactoryFactory;
import org.cord.node.RedirectionException;
import org.cord.util.Assertions;
import org.cord.util.ExceptionUtil;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class AddNodeFactory
  extends NodeRequestSegmentFactory
{
  private static AddNodeFactory INSTANCE = null;

  public static AddNodeFactory register(NodeManager nodeMgr,
                                        NodeRequestSegmentFactoryFactory factory)
  {
    Preconditions.checkState(INSTANCE == null, "Cannot register AddNodeFactory more than once");
    INSTANCE = new AddNodeFactory(nodeMgr);
    factory.register(INSTANCE);
    return INSTANCE;
  }

  public static AddNodeFactory getInstance()
  {
    return Preconditions.checkNotNull(INSTANCE, "AddNodeFactory has not been registered");
  }

  /**
   * Cgi variable name ("nodeGroupName") for the name of the NodeGroupStyle that is being targetted
   * for creation of a new Node.
   */
  public final static String CGI_VAR_NODEGROUPNAME = "nodeGroupName";

  /**
   * Cgi variable name ("filename") that is used for creation of new Nodes, and the renaming of
   * existing Nodes.
   */
  public final static String CGI_VAR_FILENAME = Node.FILENAME.getKeyName();

  public final static String NAME = "addNode";

  public final static String ENGLISHNAME = "Add Node";

  private final Int2ObjectMap<PostAction> __postActionsByNodeGroupStyle =
      new Int2ObjectOpenHashMap<PostAction>();

  private final Int2ObjectMap<PostAction> __postActionsByNodeStyle =
      new Int2ObjectOpenHashMap<PostAction>();

  private final PostAction __defaultPostAction;

  private AddNodeFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
    __defaultPostAction = new InvokeViewAction(nodeManager, EditFactory.NAME);
  }

  /**
   * Register a PostAction that will be invoked whenever a Node of the given NodeGroupStyle is
   * created. If a Node is created and it doesn't have a NodeGroupStyle PostAction then it will
   * check for a NodeStyle action, and failing that invoke the default action.
   * 
   * @param nodeGroupStyle_id
   *          Not null
   * @param postAction
   *          Not null
   * @return The existing PostAction for the NodeGroupStyle or null
   */
  public PostAction registerPostActionByNodeGroupStyle(int nodeGroupStyle_id,
                                                       PostAction postAction)
  {
    Preconditions.checkNotNull(postAction, "postAction");
    return __postActionsByNodeGroupStyle.put(nodeGroupStyle_id, postAction);
  }

  /**
   * Register a PostAction that will be invoked whenever a Node of the given NodeStyle is created.
   * Is an appropriate NodeGroupStyle PostAction is registered then this will take priority over the
   * NodeGroupStyle PostAction.
   * 
   * @param nodeStyle_id
   *          Not null
   * @param postAction
   *          Not null
   * @return The existing PostAction for the NodeStyle or null
   */
  public PostAction registerPostActionByNodeStyle(int nodeStyle_id,
                                                  PostAction postAction)
  {
    Preconditions.checkNotNull(postAction, "postAction");
    return __postActionsByNodeStyle.put(nodeStyle_id, postAction);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws NodeRequestException
  {
    // TODO: work out whether or not we can have AddNodeFactory use the
    // Node.createNode methods
    String nodeGroupName = Strings.nullToEmpty(nodeRequest.getString(CGI_VAR_NODEGROUPNAME));
    PersistentRecord nodeGroupStyle = null;
    Transaction invokingNodeTransaction = getTransaction(nodeRequest, node);
    if (nodeGroupName.length() > 0) {
      try {
        nodeGroupStyle =
            getNodeManager().getNodeGroupStyle().getInstance(node.comp(Node.NODESTYLE).getId(),
                                                             nodeGroupName,
                                                             invokingNodeTransaction);
        CreateNodePolicy policy =
            getNodeManager().getNodeGroupStyle().getMayCreateNodeRejection(node,
                                                                           nodeGroupStyle,
                                                                           nodeRequest.getUser(),
                                                                           true);
        if (policy != null) {
          throw new FeedbackErrorException(ENGLISHNAME,
                                           node,
                                           "%s has rejected your request to create a %s Node",
                                           policy,
                                           nodeGroupName);
        }
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new FeedbackErrorException(ENGLISHNAME,
                                         node,
                                         "Unable to resolve a NodeGroupStyle named %s",
                                         nodeGroupName);
      }
    }
    // ok, if we've got this far then it is ok to actually perform the
    // operation...
    TransientRecord newNode = node.getTable().createTransientRecord();
    String formFilename = Strings.nullToEmpty(nodeRequest.getString(CGI_VAR_FILENAME));
    try {
      newNode.setField(Node.PARENTNODE_ID, Integer.valueOf(node.getId()));
      newNode.setField(Node.NODEGROUPSTYLE_ID, Integer.valueOf(nodeGroupStyle.getId()));
      newNode.setField(Node.TITLE, formFilename);
      newNode.setField(Node.HTMLTITLE, formFilename);
      int ownerId = NumberUtil.toInt(nodeRequest.get(Node.OWNER_ID.getKeyName()), -1);
      if (ownerId == -1) {
        newNode.setField(Node.OWNER_ID, Integer.valueOf(nodeRequest.getUser().getId()));
      } else {
        newNode.setField(Node.OWNER_ID, Integer.valueOf(ownerId));
      }
      if (!formFilename.equals("")) {
        newNode.setField(Node.FILENAME, formFilename);
      }
      newNode = newNode.makePersistent(nodeRequest);
    } catch (MirrorException mEx) {
      // this will check for the situation when mEx is a
      // MirrorInstantiationException wrapped around a
      // MirrorFieldException on the newNode which is currently only
      // thrown on a duplicate filename. If this is not the case,
      // then the duplicate file will not get found and the
      // conventional FeedbackErrorException will get thrown as per
      // normal.
      Throwable cause = ExceptionUtil.getOriginalCause(mEx);
      if (cause instanceof RelatedMirrorFieldException) {
        RelatedMirrorFieldException rmfEx = (RelatedMirrorFieldException) cause;
        if (rmfEx.getRecord() == newNode) {
          throw new RedirectionException(node, rmfEx.getRelatedRecord(), null);
        }
      }
      mEx.printStackTrace();
      throw new FeedbackErrorException(ENGLISHNAME,
                                       node,
                                       mEx,
                                       true,
                                       true,
                                       "Error creating page: %s",
                                       ExceptionUtil.getOriginalCause(mEx).getMessage());
    }
    // TODO: Why are we cancelling the transaction on the invoking node?
    cancelTransaction(nodeRequest, invokingNodeTransaction);
    // try {
    // createTransaction(nodeRequest,
    // (PersistentRecord) newNode,
    // Transaction.TRANSACTION_UPDATE);
    // newNode = updateNode(nodeRequest, (PersistentRecord) newNode);
    // try {
    // UpdateFactory.performUpdate(getNodeManager(),
    // nodeRequest,
    // (PersistentRecord) newNode);
    // } catch (MirrorFieldException mfEx) {
    // throw new FeedbackErrorException(ENGLISHNAME,
    // "Error initialising field on new page",
    // (PersistentRecord) newNode,
    // mfEx,
    // true);
    // }
    // } catch (RecordPreLockedException rplEx) {
    // // do nothing and redirect to the new node in view mode. This
    // // should never happen in theory unless the newly created node
    // // doesn't have edit rights, and this is not the end of the
    // // world if it is the case.
    // }
    // throw new RedirectionException(getNodeManager(), node, newNode, null);
    PostAction postAction = __postActionsByNodeGroupStyle.get(nodeGroupStyle.getId());
    if (postAction == null) {
      postAction = __postActionsByNodeStyle.get(newNode.opt(Node.NODESTYLE_ID));
      if (postAction == null) {
        postAction = __defaultPostAction;
      }
    }
    return postAction.getInstance(nodeRequest,
                                  node,
                                  (PersistentRecord) newNode,
                                  context,
                                  isSummaryRole);
  }

  /**
   * PostAction represents a unit of functionality that will get invoked after the node has been
   * created.
   * 
   * @see AddNodeFactory#registerPostActionByNodeGroupStyle(int, PostAction)
   * @see AddNodeFactory#registerPostActionByNodeStyle(int, PostAction)
   * @author alex
   */
  public static interface PostAction
  {
    public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                          PersistentRecord parentNode,
                                          PersistentRecord newNode,
                                          Context context,
                                          boolean isSummaryRole)
        throws NodeRequestException;
  }

  public static class InvokeViewAction
    implements AddNodeFactory.PostAction
  {
    private final NodeManager __nodeMgr;

    private final String __viewName;

    public InvokeViewAction(NodeManager nodeMgr,
                            String viewName)
    {
      __nodeMgr = nodeMgr;
      __viewName = Assertions.notEmpty(viewName, "viewName");
    }

    @Override
    public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                          PersistentRecord parentNode,
                                          PersistentRecord newNode,
                                          Context context,
                                          boolean isSummaryRole)
        throws RedirectionException
    {
      throw new RedirectionException(parentNode, newNode, __viewName, null);
    }
  }
}
