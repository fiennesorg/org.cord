package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * NodeRequestSegmentFactory that permits a user to cancel all of their currently active
 * Transactions without logging themselves out.
 */
public class CancelTransactionsFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "cancelTransactions";

  public CancelTransactionsFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws NodeRequestException
  {
    getNodeManager().getUserManager()
                    .getSessionTransaction(nodeRequest.getSession())
                    .cancelTransactions();
    return handleSuccess(nodeRequest, node, context, null);
  }
}
