package org.cord.node.view;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.webmacro.Context;

public class PrecommitFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "precommit";

  public PrecommitFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "You do not have permission to edit this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    try {
      UpdateFactory.performUpdate(getNodeManager(), nodeRequest, node);
    } catch (MirrorFieldException mfEx) {
      throw new FeedbackErrorException("Update Error",
                                       node,
                                       mfEx,
                                       true,
                                       true,
                                       "Database error while updating page: %s",
                                       mfEx.getMessage());
    }
    NodeRequestSegmentFactory factory =
        getNodeManager().getNodeRequestSegmentManager().getFactory(node.compInt(Node.NODESTYLE_ID),
                                                                   Transaction.TRANSACTION_NULL,
                                                                   ReadViewFactory.NAME);
    NodeRequestSegment segment = factory.doWork(nodeRequest, node, context, false);
    segment.setIsEditable(false);
    return segment;
  }
}
