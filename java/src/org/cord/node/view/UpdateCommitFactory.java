package org.cord.node.view;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

public class UpdateCommitFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "updateCommit";

  public UpdateCommitFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You do not have permission to edit this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    try {
      UpdateFactory.performUpdate(getNodeManager(), nodeRequest, node);
    } catch (MirrorFieldException mfEx) {
      throw new FeedbackErrorException("Update Error", node, mfEx, true, true, mfEx.getMessage());
    }
    commitTransaction(nodeRequest, node, getTransaction(nodeRequest, node));
    return handleSuccess(nodeRequest, node, context, null);
  }
}
