package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

public class CancelDeleteFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "cancelDelete";

  public CancelDeleteFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          new ReloadSuccessOperation(EditFactory.NAME),
          Node.DELETEACL,
          "You do not have permission to delete this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    cancelTransaction(nodeRequest, getTransaction(nodeRequest, node));
    node = updateNode(nodeRequest, node);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
