package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

/**
 * A View that purely invokes the PostViewAction once the appropriate authentication has been
 * satisfied.
 * 
 * @author alex
 */
public class EmptyAclView
  extends DynamicAclAuthenticatedFactory
{
  public EmptyAclView(String name,
                      NodeManager nodeManager,
                      PostViewAction postViewSuccessOperation,
                      Integer aclId,
                      String authenticationError)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
  }

  public EmptyAclView(String name,
                      NodeManager nodeManager,
                      PostViewAction postViewSuccessOperation,
                      RecordOperationKey<PersistentRecord> nodeFieldName,
                      String authenticationError)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          nodeFieldName,
          authenticationError);
  }

  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
