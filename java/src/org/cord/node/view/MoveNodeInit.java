package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * Select and validate the node that will be moved to a new location when the MoveNodeCommit view is
 * invoked.
 * 
 * @author alex
 */
public class MoveNodeInit
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "moveNodeInit";

  public static final String SESSION_MOVENODEID = "moveNodeId";

  public MoveNodeInit(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You must have permission to edit this page before you can move it");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    nodeRequest.getSession().removeAttribute(SESSION_MOVENODEID);
    try {
      getNodeManager().getNode().assertIsMoveable(node);
    } catch (NodeMoveException e) {
      throw new FeedbackErrorException("Move page",
                                       node,
                                       "Cannot move %s because %s",
                                       node.opt(Node.PATH),
                                       e.getMessage());
    }
    nodeRequest.getSession().setAttribute(SESSION_MOVENODEID, Integer.valueOf(node.getId()));
    return handleSuccess(nodeRequest, node, context, null);
  }
}
