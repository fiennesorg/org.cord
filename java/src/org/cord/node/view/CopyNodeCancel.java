package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

public class CopyNodeCancel
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "copyNodeCancel";

  public CopyNodeCancel(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You must be an editor before you can cancel a copy operation");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    nodeRequest.getSession().removeAttribute(CopyNodeInit.SESSION_COPYNODEID);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
