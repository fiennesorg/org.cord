package org.cord.node.view;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.cord.image.ImageTable;
import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadException;
import org.cord.node.cc.TableContentCompiler;
import org.webmacro.Context;

@Deprecated
public class UploadSingleImage
  extends AccFileUpdate
{
  private final Integer __imageFormatId;

  /**
   * "imageFile"
   */
  public final static String CGI_IMAGEFILE = "imageFile";

  public UploadSingleImage(String name,
                           NodeManager nodeManager,
                           String transactionPassword,
                           Integer parentNodeId,
                           long editLockTimeout,
                           boolean stealContestedLocks,
                           String regionName,
                           Integer imageFormatId)
  {
    super(name,
          nodeManager,
          transactionPassword,
          parentNodeId,
          editLockTimeout,
          stealContestedLocks,
          regionName,
          CGI_IMAGEFILE);
    __imageFormatId = imageFormatId;
  }

  public static void uploadSingleImage(NodeManager nodeManager,
                                       WritableRecord singleImageInstance,
                                       File uploadedFile,
                                       Transaction transaction,
                                       Integer imageFormatId)
      throws MirrorFieldException, IOException
  {
    singleImageInstance.setField(ImageTable.FORMAT_ID, imageFormatId);
    ImageTable.uploadFile(singleImageInstance, uploadedFile, nodeManager.getWebRoot());
    singleImageInstance.setSafeField(TableContentCompiler.ISDEFINED, Boolean.TRUE);
  }

  public static void uploadSingleImage(NodeManager nodeManager,
                                       PersistentRecord node,
                                       String regionName,
                                       File uploadedFile,
                                       Transaction transaction,
                                       Integer imageFormatId)
      throws MirrorFieldException, IOException, MirrorNoSuchRecordException
  {
    WritableRecord singleImageInstance = getAccInstance(nodeManager, node, regionName, transaction);
    uploadSingleImage(nodeManager, singleImageInstance, uploadedFile, transaction, imageFormatId);
  }

  public static void resolveAndUploadSingleImage(NodeManager nodeManager,
                                                 PersistentRecord parentNode,
                                                 String regionName,
                                                 File uploadedFile,
                                                 Transaction transaction,
                                                 Integer imageFormatId)
      throws MirrorFieldException, IOException, MirrorNoSuchRecordException,
      MirrorTransactionTimeoutException
  {
    String nodeFilename = getNodeFilename(uploadedFile);
    PersistentRecord childNode =
        nodeManager.getNode().getChildNodeByFilename(parentNode, nodeFilename, transaction);
    transaction.edit(childNode, Db.DEFAULT_TIMEOUT);
    uploadSingleImage(nodeManager, childNode, regionName, uploadedFile, transaction, imageFormatId);
  }

  protected void uploadSingleImage(PersistentRecord invokingNode,
                                   WritableRecord singleImageInstance,
                                   File uploadedFile,
                                   Transaction transaction)
      throws FeedbackErrorException
  {
    try {
      singleImageInstance.setField(ImageTable.FORMAT_ID, __imageFormatId);
    } catch (MirrorFieldException badFormatId) {
      throw new FeedbackErrorException(null,
                                       "Bad format id:" + __imageFormatId,
                                       invokingNode,
                                       badFormatId,
                                       true);
    }
    try {
      ImageTable.uploadFile(singleImageInstance, uploadedFile, getNodeManager().getWebRoot());
    } catch (Exception ex) {
      throw new FeedbackErrorException(null, "Error uploading image:" + ex, invokingNode, ex, true);
    }
    singleImageInstance.setSafeField(TableContentCompiler.ISDEFINED, Boolean.TRUE);
  }

  protected void uploadSingleImages(PersistentRecord invokingNode,
                                    Iterator<File> files)
      throws FeedbackErrorException
  {
    PersistentRecord parentNode = getParentNode(invokingNode);
    while (files.hasNext()) {
      File file = files.next();
      PersistentRecord childNode = getChildNode(invokingNode, parentNode, getNodeFilename(file));
      Transaction transaction = createTransaction();
      WritableRecord singleImageInstance =
          getAccInstance(invokingNode, childNode, getRegionName(), transaction);
      uploadSingleImage(invokingNode, singleImageInstance, file, transaction);
      commit(transaction, invokingNode);
    }
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord invokingNode,
                                        PersistentRecord parentNode,
                                        WritableRecord childNode,
                                        WritableRecord singleImageInstance,
                                        Context context,
                                        Transaction transaction)
      throws NodeRequestException
  {
    File uploadedFile = getFile(nodeRequest);
    uploadSingleImage(invokingNode, singleImageInstance, uploadedFile, transaction);
    commit(transaction, invokingNode);
    throw new ReloadException(invokingNode, null);
  }
}
