package org.cord.node.view;

import java.util.Iterator;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorAuthorisationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.ReloadException;
import org.webmacro.Context;

/**
 * NodeUpdate implements the core structure of an update process that works on a child node of a
 * given parent node with the child node set in the incoming NodeRequest. All you need to do is
 * implement getNodeFilename and getInstance and all should work fine...
 * 
 * @see #getInstance(NodeRequest,PersistentRecord,PersistentRecord,WritableRecord,Context,Transaction)
 */
public abstract class NodeUpdate
  extends NodeRequestSegmentFactory
{
  private final Table __nodeTable;

  private final String __transactionPassword;

  private final Integer __parentNodeId;

  private final long __editLockTimeout;

  private final boolean __stealContestedLocks;

  /**
   * @param name
   *          The name by which this View will be known.
   * @param parentNodeId
   *          The id of the parent node that this should resolve against. If null, then the Node
   *          that is utilised for the parentNode will be the Node that the view is invoked against.
   * @param editLockTimeout
   *          The number of ms to wait while trying to obtain an edit lock on the targetted record.
   * @param stealContestedLocks
   *          If true then the system will try and overrided any existing locks held on the system,
   *          cancelling any ongoing transactions.
   */
  public NodeUpdate(String name,
                    NodeManager nodeManager,
                    String transactionPassword,
                    Integer parentNodeId,
                    long editLockTimeout,
                    boolean stealContestedLocks)
  {
    super(name,
          nodeManager);
    __nodeTable = nodeManager.getDb().getTable(Node.TABLENAME);
    __transactionPassword = transactionPassword;
    __parentNodeId = parentNodeId;
    __editLockTimeout = editLockTimeout;
    __stealContestedLocks = stealContestedLocks;
  }

  protected final Table getNodeTable()
  {
    return __nodeTable;
  }

  /**
   * Extract the filename of the child node that is to be targetted by the incoming nodeRequest.
   * 
   * @param nodeRequest
   *          The incoming NodeRequest!
   * @param invokingNode
   *          The Node that the incoming NodeRequest was invoked on.
   * @param parentNode
   *          The Node which the filename returned should be a child of. This may or may not be the
   *          same as the invokingNode.
   * @return The filename of the child of parentNode that should be targetted.
   * @throws NodeRequestException
   *           If it is not possible to extract the filename from the NodeRequest. The particular
   *           action to be taken is left to the responsibility of the implementing class.
   */
  protected abstract Iterator<String> getNodeFilenames(NodeRequest nodeRequest,
                                                       PersistentRecord invokingNode,
                                                       PersistentRecord parentNode)
      throws NodeRequestException;

  protected final PersistentRecord getParentNode(PersistentRecord invokingNode,
                                                 Integer parentNodeId)
      throws FeedbackErrorException
  {
    try {
      return parentNodeId == null ? invokingNode : __nodeTable.getRecord(parentNodeId);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new FeedbackErrorException(null,
                                       invokingNode,
                                       mnsrEx,
                                       true,
                                       true,
                                       "Cannot resolve the parentNode (%s)",
                                       parentNodeId);
    }
  }

  protected final PersistentRecord getParentNode(PersistentRecord invokingNode)
      throws FeedbackErrorException
  {
    return getParentNode(invokingNode, __parentNodeId);
  }

  /**
   * Calculate the appropriate childNode, obtain an edit lock on it and then delegate the processing
   * of this to getInstance(nodeRequest, invokingNode, parentNode, childNode, context, transaction).
   * The parentNode is will either be the invokingNode or the parentNodeId if specified in the
   * constructor. If the delegated method does <i>not</i> commit or cancel the generated
   * Transaction, then it will be cancelled by this method before it completes. The overall process
   * is as follows:-
   * <ul>
   * <li>Resolve the parentNode
   * <li>Extract the filename from the sub-class
   * <li>Resolve the child node with the filename
   * <li>Obtain an edit lock on the child node
   * <li>Delegate to the sub-class's getInstance(...) method.
   * </ul>
   * 
   * @see #getInstance(NodeRequest,PersistentRecord,PersistentRecord,WritableRecord,Context,Transaction)
   */
  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord invokingNode,
                                              Context context,
                                              boolean isSummaryRole)
      throws NodeRequestException
  {
    PersistentRecord parentNode = getParentNode(invokingNode);
    Iterator<String> filenames = getNodeFilenames(nodeRequest, invokingNode, parentNode);
    while (filenames.hasNext()) {
      String filename = filenames.next();
      try {
        getInstance(nodeRequest, invokingNode, parentNode, filename, context);
      } catch (NodeRequestException nrEx) {
        nrEx.printStackTrace();
        throw nrEx;
      }
    }
    throw new ReloadException(invokingNode, null);
  }

  protected final PersistentRecord getChildNode(PersistentRecord invokingNode,
                                                PersistentRecord parentNode,
                                                String filename)
      throws FeedbackErrorException
  {
    try {
      return getNodeManager().getNode().getChildNodeByFilename(parentNode, filename, null);
    } catch (MirrorNoSuchRecordException badFilenameEx) {
      throw new FeedbackErrorException(null,
                                       invokingNode,
                                       badFilenameEx,
                                       true,
                                       true,
                                       "Unresolved filename: %s",
                                       filename);
    }
  }

  protected Transaction createTransaction()
  {
    return (getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                       Transaction.TRANSACTION_UPDATE,
                                                       __transactionPassword,
                                                       "NodeUpdate"));
  }

  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord invokingNode,
                                                 PersistentRecord parentNode,
                                                 String filename,
                                                 Context context)
      throws NodeRequestException
  {
    PersistentRecord childNode = getChildNode(invokingNode, parentNode, filename);
    try (Transaction transaction = createTransaction()) {
      WritableRecord editableChildNode = null;
      try {
        editableChildNode = (__stealContestedLocks
            ? transaction.editWithLockStealing(childNode, __editLockTimeout, __transactionPassword)
            : transaction.edit(childNode, __editLockTimeout));
      } catch (MirrorTransactionTimeoutException mttEx) {
        throw new FeedbackErrorException(null,
                                         invokingNode,
                                         mttEx,
                                         true,
                                         true,
                                         "Unable to get a lock on %s due to %s",
                                         childNode,
                                         mttEx);
      } catch (MirrorAuthorisationException maEx) {
        throw new FeedbackErrorException(null,
                                         invokingNode,
                                         maEx,
                                         true,
                                         true,
                                         "Bad transactionPassword: %s",
                                         maEx);
      }
      return getInstance(nodeRequest,
                         invokingNode,
                         parentNode,
                         editableChildNode,
                         context,
                         transaction);
    }
  }

  /**
   * Commit the given Transaction, throwing a FeedbackErrorException if it is not possible.
   */
  protected void commit(Transaction transaction,
                        PersistentRecord invokingNode)
      throws FeedbackErrorException
  {
    try {
      transaction.commit();
    } catch (MirrorTransactionTimeoutException mttEx) {
      throw new FeedbackErrorException(null,
                                       invokingNode,
                                       mttEx,
                                       true,
                                       true,
                                       "Error committing transaction: %s",
                                       mttEx);
    }
  }

  /**
   * Process the locked childNode record. It is the responsibility of the sub-class to commit the
   * transaction once it has completed any processing. If this doesn't happen then the transaction
   * will be cancelled after the method returns.
   */
  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord invokingNode,
                                                    PersistentRecord parentNode,
                                                    WritableRecord childNode,
                                                    Context context,
                                                    Transaction transaction)
      throws NodeRequestException;
}
