package org.cord.node.view;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeQuestionManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

public abstract class AskNodeQuestion
  extends DynamicAclAuthenticatedFactory
{
  public static final String CGI_QUESTIONNAME = "questionName";

  public static final String CGI_QUESTIONENGLISHNAME = "questionEnglishName";

  public static final String CGI_ANSWERVIEW = "answerView";

  public static final String CGI_ANSWERNODEPARAM = "answerNodeParam";

  public AskNodeQuestion(String name,
                         NodeManager nodeManager,
                         PostViewAction postViewSuccessOperation,
                         RecordOperationKey<PersistentRecord> nodeFieldName,
                         String authenticationError)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          nodeFieldName,
          authenticationError);
  }

  public AskNodeQuestion(String name,
                         NodeManager nodeManager,
                         PostViewAction postViewSuccessOperation,
                         Integer aclId,
                         String authenticationError)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    NodeQuestionManager questionManager = getNodeManager().getNodeQuestionManager();
    NodeQuestion nodeQuestion;
    try {
      nodeQuestion = createNodeQuestion(nodeRequest, node, context);
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(null,
                                       node,
                                       e,
                                       true,
                                       true,
                                       "It has been unable to create the requested question");
    }
    questionManager.addQuestion(nodeRequest.getSession(), nodeQuestion);
    return handleSuccess(nodeRequest, node, context, null);
  }

  protected abstract NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                     PersistentRecord node,
                                                     Context context)
      throws MirrorNoSuchRecordException;
}
