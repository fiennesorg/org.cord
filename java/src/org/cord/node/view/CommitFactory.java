package org.cord.node.view;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RedirectionException;
import org.cord.node.ReloadException;
import org.cord.node.TransactionExpiredException;
import org.webmacro.Context;

public class CommitFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "commit";

  public CommitFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "You do not have permission to edit this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws ReloadException, RedirectionException, TransactionExpiredException
  {
    PersistentRecord persistentNode = node.getPersistentRecord();
    String oldFilename = persistentNode.opt(Node.FILENAME);
    String newFilename = node.opt(Node.FILENAME);
    commitTransaction(nodeRequest, node, getTransaction(nodeRequest, node));
    return postProcessCommit(getNodeManager(), node, oldFilename, newFilename);
  }

  public static NodeRequestSegment postProcessCommit(NodeManager nodeMgr,
                                                     PersistentRecord node,
                                                     String oldFilename,
                                                     String newFilename)
      throws RedirectionException, ReloadException
  {
    if (!oldFilename.equals(newFilename)) {
      try {
        nodeMgr.getMovedNode().addMovedNode(node.comp(Node.PARENTNODE), oldFilename, node);
      } catch (MirrorException e) {
        e.printStackTrace();
      }
      throw new RedirectionException(node);
    }
    throw new ReloadException(node, ReadViewFactory.NAME, null);
  }
}
