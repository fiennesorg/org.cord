package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.NodeRequestSegmentManager;
import org.cord.node.TemplateSuccessOperation;
import org.webmacro.Context;

/**
 * view that causes the default template for the invoked Node to be replaced with a constant
 * template that is defined at instantiation time, with optional render reset. This makes it very
 * easy to add in multiple pages to a site without adding in multiple URLs, namely in situations
 * when the ordering of the pages is to be predetermined and you don't necessarily want people
 * bookmarking the sub-pages of a given URL (by utilising POST rather than GET on the submit forms).
 * If you only want this functionality to be available on a given NodeStyle then you should use the
 * selective register functionality.
 * 
 * @see NodeRequestSegmentManager#register(int, int, NodeRequestSegmentFactory)
 */
public class AlternativeTemplateFactory
  extends DynamicAclAuthenticatedFactory
{
  public AlternativeTemplateFactory(String name,
                                    NodeManager nodeManager,
                                    String templatePath,
                                    boolean isRenderReset,
                                    boolean isRootTemplatePath,
                                    Integer aclId,
                                    String authenticationError)
  {
    super(name,
          nodeManager,
          new TemplateSuccessOperation(nodeManager, templatePath),
          aclId,
          authenticationError);
    setIsRenderReset(isRenderReset);
    setIsRootTemplatePath(isRootTemplatePath);
  }

  /**
   * @param name
   *          The view name that will be used to invoke this functionality.
   * @param templatePath
   *          The webmacro path to the template that is to be utilised.
   * @param isRenderReset
   *          If true, then Node will drop the specified template outside the normal nested template
   *          structure, but within the normal root template.
   * @param isRootTemplatePath
   *          If true, then Node will drop the specified template with no framing at all, ie
   *          returned exactly as it is rendered.
   */
  public AlternativeTemplateFactory(String name,
                                    NodeManager nodeManager,
                                    String templatePath,
                                    boolean isRenderReset,
                                    boolean isRootTemplatePath)
  {
    this(name,
         nodeManager,
         templatePath,
         isRenderReset,
         isRootTemplatePath,
         null,
         null);
  }

  /**
   * Create an alternative template view that has isRootTemplatePath set to false.
   */
  public AlternativeTemplateFactory(String name,
                                    NodeManager nodeManager,
                                    String templatePath,
                                    boolean isRenderReset)
  {
    this(name,
         nodeManager,
         templatePath,
         isRenderReset,
         false);
  }

  /**
   * Create an alternative template view that has both isRenderReset and isRootTemplatePath set to
   * false.
   */
  public AlternativeTemplateFactory(String name,
                                    NodeManager nodeManager,
                                    String templatePath)
  {
    this(name,
         nodeManager,
         templatePath,
         false,
         false);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
