package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RedirectionException;
import org.cord.node.TransactionExpiredException;
import org.cord.util.LogicException;
import org.webmacro.Context;

public class ConfirmDeleteFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "confirmDelete";

  public ConfirmDeleteFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.DELETEACL,
          "You do not have permission to delete this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws TransactionExpiredException, RedirectionException, FeedbackErrorException
  {
    if (!getNodeManager().getNode().isDeletable(node, nodeRequest.getUser(), true, NAME)) {
      throw new FeedbackErrorException(null, node, "It is not possible to delete %s", node);
    }
    PersistentRecord parentNode = null;
    try {
      parentNode = node.comp(Node.PARENTNODE);
    } catch (RuntimeException noParentNodeEx) {
      throw new LogicException("Some loon has decided to delete the Root Node.  Eeeek!",
                               noParentNodeEx);
    }
    commitTransaction(nodeRequest, node, getTransaction(nodeRequest, node));
    throw new RedirectionException(node, parentNode, null);
  }
}
