package org.cord.node.view;

import java.util.Comparator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RecordPreLockedException;
import org.cord.util.GettableUtils;
import org.cord.util.SortedIterator;
import org.webmacro.Context;

public class DeleteNodeFactory
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "deleteNode";

  public static final String PARAM_TEMPLATEPATH = "DeleteNode.templatePath";

  public static final String PARAM_TEMPLATEPATH_DEFAULT = "node/node.delete.wm.html";

  private final String __deleteTemplatePath;

  public DeleteNodeFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance());
    __deleteTemplatePath =
        GettableUtils.get(nodeManager, PARAM_TEMPLATEPATH, PARAM_TEMPLATEPATH_DEFAULT);
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              PersistentRecord acl)
      throws RecordPreLockedException, FeedbackErrorException
  {
    if (!getNodeManager().getNode().isDeletable(node, nodeRequest.getUser(), true, NAME)) {
      throw new FeedbackErrorException(null, node, "It is not possible to delete %s", node);
    }
    System.err.println("DeleteNodeFactory: preCancel: " + node);
    cancelTransaction(nodeRequest, getTransaction(nodeRequest, node));
    node = nodeRequest.getNode(node);
    System.err.println("DeleteNodeFactory: postCancel: " + node);
    Transaction transaction = createDeleteTransaction(nodeRequest, node);
    context.put("lockedNodes",
                new SortedIterator<PersistentRecord>(transaction.getRecords(getNodeManager().getNode()
                                                                                            .getTable())
                                                                .iterator(),
                                                     new NodePathComparator()));
    node = nodeRequest.getNode(node);
    System.err.println("DeleteNodeFactory: postLock: " + node);
    return new NodeRequestSegment(getNodeManager(),
                                  nodeRequest,
                                  node,
                                  context,
                                  __deleteTemplatePath);
  }

  class NodePathComparator
    implements Comparator<PersistentRecord>
  {
    @Override
    public int compare(PersistentRecord node1,
                       PersistentRecord node2)
    {
      return node1.opt(Node.PATH).compareTo(node2.opt(Node.PATH));
    }
  }
}
