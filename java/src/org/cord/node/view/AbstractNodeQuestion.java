package org.cord.node.view;

import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeQuestionManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

/**
 * Abstract view that provides a base class for views that process a single `named NodeQuestion.
 * 
 * @author alex
 * @see NodeQuestion
 * @see NodeQuestionManager
 */
public abstract class AbstractNodeQuestion
  extends NodeRequestSegmentFactory
{
  public static final String CGI_QUESTIONNAME = "questionName";

  public AbstractNodeQuestion(String name,
                              NodeManager nodeManager,
                              PostViewAction success)
  {
    super(name,
          nodeManager,
          success);
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              boolean isSummaryRole)
      throws NodeRequestException
  {
    NodeQuestion question = null;
    Map<String, NodeQuestion> questions =
        getNodeManager().getNodeQuestionManager().getQuestions(nodeRequest.getSession());
    if (questions != null) {
      String questionName = nodeRequest.getString(CGI_QUESTIONNAME);
      question = questions.get(questionName);
    }
    return getInstance(nodeRequest, node, context, question);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    NodeQuestion question)
      throws NodeRequestException;
}
