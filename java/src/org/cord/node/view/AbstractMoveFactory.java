package org.cord.node.view;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Transaction;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RecordPreLockedException;
import org.cord.node.ReloadException;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * Abstract view that handles the operation of swapping the NODE_NUMBER of a given Node with the
 * NODE_NUMBER of another Node that shares the common NODE_PARENTNODE. This is an unusual operation
 * because it operates on two rather than one Nodes. This therefore means that it requires the user
 * to have the lock on both Nodes rather than the conventional single Node.
 * <p>
 * In order to identify which Node is to be exchanged with the invoking Node, subclasses are
 * responsible for supplying a Query of which the first record in the Query is taken as being the
 * Node that is to be swapped. This should be implemented via getSiblingsQuery(...).
 * <p>
 * The transactional model that is utilised is to utilise a single Transaction for the operation
 * which is created, updated and committed during the request. The reasoning behind this is:-
 * <ul>
 * <li>I don't like the idea of a single Transaction owning more than one Node as it breaks the
 * integrity of the Preview - Commit process because you will not be previewing all the changes that
 * will be commited when you press commit.
 * <li>It would be problematic making all listing operations that utilise the NODE_NUMBER parameter
 * in performing their listing operations utilise the various Transactions that may exist to perform
 * the operation as the updated values would not exist in SQL space and therefore would not be
 * available to the Query structure. Therefore you would not be able to see the results of the
 * operation until it had completed.
 * </ul>
 * <p>
 * This therefore implies that the operation as it stands is going to be non-reversible. This is
 * outside the normal scope of the Node operation, but as no data is created or destroyed, it is
 * relatively easy to manually reverse if so required. The only issue will be that the results of
 * this will be instantly publically available. However, if this is a problem and the data is very
 * sensitive, then the user should be working on the data in an unpublished format before committing
 * it to the world at large (either by publishing the parent node or by publishing the related child
 * nodes).
 * <p>
 * Implementations of this view are state agnostic, ie they can be invoked on both locked and
 * unlocked nodes. If the node is locked when the view is invoked then the locked Transaction will
 * be cancelled, the operation performed and then a fresh Transaction will be created on the target
 * Node, thereby ensuring that the end state of the Node is the same as the starting state. This of
 * course implies that any transient information contained in the original Transaction will be lost.
 * It is up to the site administrator to decide whether to provide the invocation method on the view
 * or the edit structure of the Node based on applicability and context..
 * 
 * @see #getSiblings(NodeRequest,PersistentRecord)
 */
public abstract class AbstractMoveFactory
  extends DynamicAclAuthenticatedFactory
{
  public AbstractMoveFactory(String name,
                             NodeManager nodeManager)
  {
    super(name,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You do not have permission to move this page");
  }

  /**
   * Get the Query which contains the target Node to be swapped as the first element in the Query.
   * 
   * @param nodeRequest
   *          The incoming NodeRequest
   * @param node
   *          The Node on which the operation is being invoked. This is the Node that will be
   *          swapped with the first record on the resulting Query.
   * @return The Query containing the target Node. If this is null, or if the first record is
   *         identical to the invoking node, or the first record doesn't share a common parent with
   *         the invoking node then a ReloadException will be invoked on the invoking Node and the
   *         transactional state of the system will be unchanged.
   */
  protected abstract RecordSource getSiblings(NodeRequest nodeRequest,
                                              PersistentRecord node);

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              PersistentRecord acl)
      throws NodeRequestException
  {
    RecordSource siblingsQuery = getSiblings(nodeRequest, node);
    RecordList records = siblingsQuery.getRecordList();
    if (siblingsQuery == null || records.size() == 0) {
      throw new ReloadException(node, null);
    }
    PersistentRecord sibling = null;
    try {
      sibling = records.get(0);
    } catch (MissingRecordException lostSiblingRecord) {
      throw new FeedbackErrorException(null,
                                       node,
                                       lostSiblingRecord,
                                       false,
                                       true,
                                       "The target page does not exist");
    }
    if (!sibling.getTable().equals(node.getTable()) || (sibling.getId() == node.getId())
        || !(sibling.opt(Node.PARENTNODE_ID).equals(node.opt(Node.PARENTNODE_ID)))) {
      throw new ReloadException(node, null);
    }
    if (sibling.isLocked()) {
      throw new RecordPreLockedException("The target page is currently already locked",
                                         node,
                                         sibling,
                                         getNodeManager().getContestedLockFilterManager()
                                                         .getStealableSessionTransaction(nodeRequest,
                                                                                         sibling),
                                         null);
    }
    Transaction transaction = getTransaction(nodeRequest, node);
    boolean hadEditTransaction = transaction != null;
    if (hadEditTransaction) {
      cancelTransaction(nodeRequest, transaction);
    }
    node = updateNode(nodeRequest, node);
    try (Transaction moveTransaction = createEditTransaction(nodeRequest, node)) {
      node = updateNode(nodeRequest, node);
      try {
        sibling = moveTransaction.edit(sibling, Db.DEFAULT_TIMEOUT);
      } catch (MirrorTransactionTimeoutException mttEx) {
        throw new RecordPreLockedException("Cannot lock target page",
                                           node,
                                           sibling,
                                           getNodeManager().getContestedLockFilterManager()
                                                           .getStealableSessionTransaction(nodeRequest,
                                                                                           sibling),
                                           mttEx);
      }
      try {
        Object nodeNumber = node.opt(Node.NUMBER);
        Object siblingNumber = sibling.opt(Node.NUMBER);
        node.setField(Node.NUMBER, siblingNumber);
        sibling.setField(Node.NUMBER, nodeNumber);
        moveTransaction.commit();
      } catch (MirrorFieldException badNumberEx) {
        throw new FeedbackErrorException(null,
                                         node,
                                         badNumberEx,
                                         true,
                                         true,
                                         "Unexpected field error: %s",
                                         badNumberEx);
      } catch (MirrorTransactionTimeoutException mttEx) {
        throw new FeedbackErrorException(null,
                                         node,
                                         mttEx,
                                         true,
                                         true,
                                         "Timed out while attempting to commit transaction");
      }
    }
    node = updateNode(nodeRequest, node);
    if (hadEditTransaction) {
      createEditTransaction(nodeRequest, node);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
