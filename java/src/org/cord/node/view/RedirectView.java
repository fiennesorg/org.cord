package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.webmacro.Context;

public class RedirectView
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "redirectView";

  public static final String TEMPLATEPATH = "node/Redirect/viewRedirects.wm.html";

  public RedirectView(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "Only editors can view the redirects");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    FeedbackErrorException feEx = new FeedbackErrorException("View Redirects", node, "");
    feEx.setWebmacroTemplate(TEMPLATEPATH);
    throw feEx;
  }
}
