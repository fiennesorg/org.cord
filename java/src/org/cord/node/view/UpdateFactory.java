package org.cord.node.view;

import java.util.Date;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.node.Acl;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeStyle;
import org.cord.node.TransactionExpiredException;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

public class UpdateFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "update";

  public final static String CGI_TABNAME = "tabName";

  public UpdateFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "You do not have permission to edit this page");
  }

  protected static Object updateNode(TransientRecord node,
                                     FieldKey<?> fieldName,
                                     Gettable params)
      throws MirrorFieldException
  {
    return updateNode(node, fieldName, params, null);
  }

  protected static <T> Object updateNode(TransientRecord node,
                                         FieldKey<T> fieldName,
                                         Gettable params,
                                         Object fallbackValue)
      throws MirrorFieldException
  {
    Object value = params.get(fieldName);
    if (value != null) {
      return node.setField(fieldName, value);
    } else {
      if (fallbackValue != null) {
        return node.setField(fieldName, fallbackValue);
      } else {
        return node.opt(fieldName);
      }
    }
  }

  public static void performUpdate(NodeManager nodeMgr,
                                   Gettable inputs,
                                   WritableRecord writableNode,
                                   Transaction nodeTransaction,
                                   PersistentRecord user,
                                   Map<Object, Object> outputs)
      throws TransactionExpiredException, MirrorFieldException, FeedbackErrorException
  {
    if (!writableNode.isLockedBy(nodeTransaction)) {
      DebugConstants.DEBUG_OUT.println("UpdateFactory.performUpdate(" + nodeMgr + "," + inputs + ","
                                       + writableNode + "," + nodeTransaction + ")");
      DebugConstants.DEBUG_OUT.println(nodeTransaction.debug());
      DebugConstants.DEBUG_OUT.println(writableNode.debug());
      throw new LogicException("Bad Lock Situation");
    }
    int userId = user.getId();
    int ownerId = writableNode.compInt(Node.OWNER_ID);
    final Acl acls = nodeMgr.getAcl();
    try {
      nodeTransaction.resetCreationTime();
      String title = (String) updateNode(writableNode, Node.TITLE, inputs);
      if (inputs.getString(Node.TITLE) != null) {
        updateNode(writableNode, Node.HTMLTITLE, inputs, title);
      }
      updateNode(writableNode, Node.NUMBER, inputs);
      if (acls.acceptsAclUserOwner(writableNode.compInt(Node.PUBLISHACL_ID), userId, ownerId)) {
        writableNode.setField(Node.ISPUBLISHED,
                              MoreObjects.firstNonNull(inputs.getBoolean(Node.ISPUBLISHED),
                                                       writableNode.opt(Node.ISPUBLISHED)));
      }
      PersistentRecord nodeStyle = writableNode.comp(Node.NODESTYLE);
      if (nodeStyle.is(NodeStyle.ISEDITABLEFILENAME)) {
        String proposedFilename = inputs.getString(Node.FILENAME);
        if (!Strings.isNullOrEmpty(proposedFilename)) {
          try {
            updateNode(writableNode, Node.FILENAME, inputs);
          } catch (MirrorFieldException mfEx) {
            PersistentRecord duplicate = nodeMgr.getNode().getRelatedNode(mfEx);
            if (duplicate == null) {
              throw mfEx;
            }
            throw new FeedbackErrorException("Duplicate Filename",
                                             writableNode,
                                             "Your new filename of \"%s\" has been rejected - "
                                                           + "It is a duplicate of the filename of the existing page %s",
                                             proposedFilename,
                                             duplicate.opt(Node.HREF, Node.TITLE.getKeyName()));
          }
        }
      }
      if (acls.acceptsAclUserOwner(nodeStyle.compInt(NodeStyle.UPDATEVIEWACL_ID),
                                   userId,
                                   ownerId)) {
        updateNode(writableNode, Node.VIEWACL_ID, inputs);
      }
      if (acls.acceptsAclUserOwner(nodeStyle.compInt(NodeStyle.UPDATEEDITACL_ID),
                                   userId,
                                   ownerId)) {
        updateNode(writableNode, Node.EDITACL_ID, inputs);
      }
      if (acls.acceptsAclUserOwner(nodeStyle.compInt(NodeStyle.UPDATEPUBLISHACL_ID),
                                   userId,
                                   ownerId)) {
        updateNode(writableNode, Node.PUBLISHACL_ID, inputs);
      }
      if (acls.acceptsAclUserOwner(nodeStyle.compInt(NodeStyle.UPDATEDELETEACL_ID),
                                   userId,
                                   ownerId)) {
        updateNode(writableNode, Node.DELETEACL_ID, inputs);
      }
      writableNode.setFieldIfDefined(Node.DESCRIPTION, inputs.get(Node.DESCRIPTION));
      writableNode.setFieldIfDefined(Node.ISNOFOLLOW, inputs.getBoolean(Node.ISNOFOLLOW));
      writableNode.setFieldIfDefined(Node.ISNOINDEX, inputs.getBoolean(Node.ISNOINDEX));
      writableNode.setFieldIfDefined(Node.ISMENUITEM, inputs.getBoolean(Node.ISMENUITEM));
      writableNode.setFieldIfDefined(Node.ISSEARCHABLE, inputs.getBoolean(Node.ISSEARCHABLE));
      boolean isUpdated = writableNode.getIsUpdated();
      isUpdated |= nodeMgr.invokeNodeUpdaters(writableNode, inputs);
      isUpdated |=
          nodeMgr.getNode()
                 .updateContentCompilers(writableNode, user, nodeTransaction, inputs, outputs);
      if (isUpdated) {
        writableNode.setField(Node.MODIFICATIONTIME, new Date());
      }
    } catch (MirrorInstantiationException miEx) {
      throw new LogicException("wierd update error", miEx);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("table linkage breakdown", mnsrEx);
    } catch (MirrorTransactionTimeoutException mttEx) {
      throw new TransactionExpiredException("Transaction expired during handleUpdate()",
                                            writableNode,
                                            mttEx);
    }
  }

  public static void performUpdate(NodeManager nodeMgr,
                                   NodeRequest nodeRequest,
                                   PersistentRecord node,
                                   Transaction nodeTransaction)
      throws TransactionExpiredException, MirrorFieldException, FeedbackErrorException
  {
    WritableRecord writableNode =
        ObjectUtil.castTo(updateNode(nodeRequest, node), WritableRecord.class);
    performUpdate(nodeMgr,
                  nodeRequest,
                  writableNode,
                  nodeTransaction,
                  nodeRequest.getUser(),
                  nodeRequest.getWebContext());
  }

  public static void performUpdate(NodeManager nodeManager,
                                   NodeRequest nodeRequest,
                                   PersistentRecord node)
      throws TransactionExpiredException, MirrorFieldException, FeedbackErrorException
  {
    Transaction nodeTransaction = getTransaction(nodeRequest, node);
    performUpdate(nodeManager, nodeRequest, node, nodeTransaction);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    try {
      performUpdate(getNodeManager(), nodeRequest, node);
    } catch (MirrorFieldException mfEx) {
      throw new FeedbackErrorException("Update Error", node, mfEx, true, true, mfEx.getMessage());
    }
    context.put(CGI_TABNAME, nodeRequest.get(CGI_TABNAME));
    return new NodeRequestSegment(getNodeManager(),
                                  nodeRequest,
                                  node,
                                  context,
                                  node.comp(Node.NODESTYLE).opt(NodeStyle.EDITTEMPLATEPATH));
    // return handleSuccess(nodeRequest, node, context, null);
  }
}
