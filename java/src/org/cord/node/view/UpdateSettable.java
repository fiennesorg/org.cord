package org.cord.node.view;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.TemplateSuccessOperation;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.webmacro.Context;

/**
 * Extension of ViewGettable that provides the functionality to update an item of information in one
 * of the registered Gettable sources providing it implements the Settable interface. No side
 * effects are generated within the view itself when the Settable properties are updated, so it is
 * the responsibility of the Settable target to trigger any side effects. There is not currently any
 * error reporting back from the side effect updates because the Settable interface doesn't define
 * any errors - maybe we should catch RuntimeExceptions and roll them back to the client?
 * 
 * @deprecated because not being utilised at present - pending review
 */
@Deprecated
public class UpdateSettable
  extends ViewGettable
{
  public final static String WEBMACRO_SETTABLENAME = "settableName";

  public final static String WEBMACRO_SETTABLEKEY = "settableKey";

  public final static String WEBMACRO_SETTABLEVALUE = "settableValue";

  public final static String WEBMACRO_SETTABLESOURCES = "settableSources";

  public final static String WEBMACRO_UPDATETEMPLATEPATH = "node/updateSettable.wm.html";

  private final Integer __updateAclId;

  private final Map<String, Gettable> __settableSources = new HashMap<String, Gettable>();

  private final Map<String, Gettable> __publicSettableSources =
      Collections.unmodifiableMap(__settableSources);

  /**
   * @param postViewSuccessOperation
   *          The operation to be invoked once the functionality has been completed. If null, then a
   *          default TemplateSuccessOperation will be registered pointing at
   *          WEBMACRO_UPDATETEMPLATEPATH.
   * @param viewAclId
   *          The id of the ACL that is necessary to pass to invoke the introspection viewing part
   *          of this operation. A value of null is interpretted as public functionality.
   * @param updateAclId
   *          The id of the ACL that is necessary to pass to invoke the updating of a value in a
   *          registered Settable item. Note that you must pass any definitions set up in viewAclId
   *          before you are authenticated against updateAclId. A value of null will result in no
   *          additional authentication above and beyond the viewAclId.
   * @see #WEBMACRO_UPDATETEMPLATEPATH
   */
  public UpdateSettable(String name,
                        NodeManager nodeManager,
                        PostViewAction postViewSuccessOperation,
                        Integer viewAclId,
                        Integer updateAclId)
  {
    super(name,
          nodeManager,
          (postViewSuccessOperation == null
              ? new TemplateSuccessOperation(nodeManager, WEBMACRO_UPDATETEMPLATEPATH)
              : postViewSuccessOperation),
          viewAclId);
    __updateAclId = updateAclId;
  }

  @Override
  public Gettable register(String name,
                           Gettable gettable)
  {
    synchronized (__settableSources) {
      Gettable result = super.register(name, gettable);
      if (gettable == null) {
        return __settableSources.remove(name);
      }
      if (gettable instanceof Settable) {
        return __settableSources.put(name, gettable);
      }
      return result;
    }
  }

  public Settable getSettable(String name)
  {
    return (Settable) __settableSources.get(name);
  }

  /**
   * @throws FeedbackErrorException
   *           if the targetted Settable rejects the updated value. The title will be the name of
   *           this view, and the message will be the getMessage from the SettableException.
   * @see SettableException#getMessage()
   */
  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String name = GettableUtils.get(nodeRequest, WEBMACRO_SETTABLENAME, null);
    Settable settable = getSettable(name);
    if (settable != null) {
      String key = GettableUtils.get(nodeRequest, WEBMACRO_SETTABLEKEY, null);
      String value = GettableUtils.get(nodeRequest, WEBMACRO_SETTABLEVALUE, null);
      authenticate(nodeRequest,
                   node,
                   __updateAclId,
                   null,
                   "You do not have permission to update the Settable properties");
      try {
        settable.set(key, value);
      } catch (SettableException badValueEx) {
        throw new FeedbackErrorException(getName(),
                                         node,
                                         badValueEx,
                                         true,
                                         true,
                                         badValueEx.getMessage());
      }
    }
    context.put(WEBMACRO_SETTABLESOURCES, __publicSettableSources);
    return super.getInstance(nodeRequest, node, context, acl);
  }
}
