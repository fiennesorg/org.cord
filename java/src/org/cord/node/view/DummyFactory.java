package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.util.LogicException;
import org.webmacro.Context;

public class DummyFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "changeFilename";

  public DummyFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
  {
    throw new LogicException("DummyFactory should never be invoked");
  }
}
