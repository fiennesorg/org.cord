package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

/**
 * Select a node that will be copied to a new location when the CopyNodeCommit view is invoked. This
 * just drops the current node id into the session so that we have something to target at t'other
 * end. You have to have edit rights on the node that you are copying otherwise the operation is not
 * permitted.
 * 
 * @author alex
 * @see org.cord.node.view.CopyNodeCommit
 */
public class CopyNodeInit
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "copyNodeInit";

  public static final String SESSION_COPYNODEID = "copyNodeId";

  public CopyNodeInit(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You must have permission to edit this page before you can copy it");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    nodeRequest.getSession().setAttribute(SESSION_COPYNODEID, Integer.valueOf(node.getId()));
    return handleSuccess(nodeRequest, node, context, null);
  }
}
