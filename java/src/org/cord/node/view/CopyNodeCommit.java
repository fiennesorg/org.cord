package org.cord.node.view;

import java.util.ArrayList;
import java.util.List;

import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeStyle;
import org.cord.node.RedirectionException;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

/**
 * Commit the copy operation that was set up with the CopyNodeInit view.
 * 
 * @author alex
 * @see org.cord.node.view.CopyNodeInit
 */
public class CopyNodeCommit
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "copyNodeCommit";

  public static final String TITLE = "Copy page";

  public static final String CGI_TONODEGROUPSTYLEID = "toNodeGroupStyleId";

  public static final String CGI_ISPUBLISHED = Node.ISPUBLISHED.getKeyName();

  public static final String CGI_FILENAME = Node.FILENAME.getKeyName();

  public static final String CGI_COPYCHILDREN = "copyChildren";

  public static final String INITPARM_COPYNODE_FALLBACKTONODESTYLEID =
      "CopyNode.fallbackToNodeStyleId";

  public CopyNodeCommit(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "You must have edit rights on a page before you can past a page into it");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord toParentNode,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    Integer copyNodeId =
        NumberUtil.toInteger(nodeRequest.getSession().getAttribute(CopyNodeInit.SESSION_COPYNODEID),
                             null);
    if (copyNodeId == null) {
      throw new FeedbackErrorException(TITLE, toParentNode, "You haven't selected a page to copy");
    }
    PersistentRecord fromNode = null;
    try {
      fromNode = getNodeManager().getNode().getTable().getRecord(copyNodeId.intValue());
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       "The page you have selected for copying is no longer found.  Has it been deleted?");
    }
    Integer toNodeGroupStyleId =
        NumberUtil.toInteger(nodeRequest.get(CGI_TONODEGROUPSTYLEID), null);
    PersistentRecord toNodeGroupStyle = null;
    if (toNodeGroupStyleId == null) {
      PersistentRecord toParentNodeStyle = toParentNode.comp(Node.NODESTYLE);
      for (PersistentRecord potentialNodeGroupStyle : toParentNodeStyle.opt(NodeStyle.CHILDRENNODEGROUPSTYLES)) {
        if (potentialNodeGroupStyle.opt(NodeGroupStyle.CHILDNODESTYLE_ID)
                                   .equals(fromNode.opt(Node.NODESTYLE_ID))
            && getNodeManager().getNodeGroupStyle().mayCreateNode(toParentNode,
                                                                  potentialNodeGroupStyle,
                                                                  nodeRequest.getUser())) {
          toNodeGroupStyle = potentialNodeGroupStyle;
          break;
        }
      }
      if (toNodeGroupStyle == null) {
        throw new FeedbackErrorException(TITLE,
                                         toParentNode,
                                         "Your selected page (%s) cannot be a child of this page",
                                         fromNode.opt(Node.PATH));
      }
    } else {
      try {
        toNodeGroupStyle =
            getNodeManager().getNodeGroupStyle().getTable().getRecord(toNodeGroupStyleId.intValue());
      } catch (MirrorNoSuchRecordException e) {
        throw new FeedbackErrorException(TITLE,
                                         toParentNode,
                                         e,
                                         false,
                                         true,
                                         "The NodeGroupStyle that you have selected to copy into is no longer found");
      }
    }
    List<String> successMessages = new ArrayList<String>();
    List<String> failureMessages = new ArrayList<String>();
    try {
      PersistentRecord toNode =
          getNodeManager().getNode()
                          .copyNode(fromNode,
                                    toParentNode,
                                    toNodeGroupStyle,
                                    Boolean.TRUE.equals(nodeRequest.getBoolean(CGI_COPYCHILDREN)),
                                    NumberUtil.toInteger(getNodeManager().getGettable()
                                                                         .get(INITPARM_COPYNODE_FALLBACKTONODESTYLEID),
                                                         null),
                                    nodeRequest.getString(CGI_FILENAME),
                                    nodeRequest.getBoolean(CGI_ISPUBLISHED),
                                    successMessages,
                                    failureMessages);
      System.err.println(successMessages);
      System.err.println(failureMessages);
      nodeRequest.getSession().removeAttribute(CopyNodeInit.SESSION_COPYNODEID);
      throw new RedirectionException(toParentNode, toNode, null);
    } catch (MirrorException e) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       e,
                                       true,
                                       true,
                                       "There has been a database error completing the copy operation");
    }
  }
}
