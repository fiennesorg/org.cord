package org.cord.node.view;

import java.util.Iterator;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Transaction;
import org.cord.node.Node;
import org.cord.node.NodeFilter;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.PostViewAction;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.LogicException;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class AskAccNodeQuestion
  extends AskNodeQuestion
{
  private final String __questionHeader;

  private final String __questionFooterNodeField;

  private final NodeFilter __eligibleNodeFilter;

  private final String __cgiTargetIdDefined;

  private final String __cgiTargetId;

  private final TableContentCompiler __sourceAcc;

  private final String __sourceAccName;

  private final TableContentCompiler __targetAcc;

  private final String __targetAccName;

  private final RecordOperationKey<RecordSource> __targetReferencesName;

  public AskAccNodeQuestion(String name,
                            NodeManager nodeManager,
                            PostViewAction postViewSuccessOperation,
                            Integer aclId,
                            String authenticationError,
                            String questionHeader,
                            String questionFooterNodeField,
                            NodeFilter eligibleNodeFilter,
                            String cgiTargetIdDefined,
                            String cgiTargetId,
                            TableContentCompiler sourceAcc,
                            String sourceAccName,
                            TableContentCompiler targetAcc,
                            String targetAccName,
                            RecordOperationKey<RecordSource> targetReferencesName)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
    __questionHeader = questionHeader;
    __questionFooterNodeField = questionFooterNodeField;
    __eligibleNodeFilter = eligibleNodeFilter;
    __cgiTargetIdDefined = cgiTargetIdDefined;
    __cgiTargetId = cgiTargetId;
    __sourceAcc = sourceAcc;
    __sourceAccName = sourceAccName;
    __targetAcc = targetAcc;
    __targetAccName = targetAccName;
    __targetReferencesName = targetReferencesName;
  }

  @Override
  protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                            final PersistentRecord node,
                                            Context context)
  {
    final Transaction transaction = getTransaction(nodeRequest, node);
    NodeQuestion nodeQuestion =
        new NodeQuestion(getName() + node.getId(),
                         __questionHeader + node.get(__questionFooterNodeField),
                         null,
                         node,
                         -1,
                         __eligibleNodeFilter,
                         transaction) {
          @Override
          public String getAnswerUrl()
          {
            StringBuilder url = new StringBuilder();
            url.append(node.opt(Node.URL));
            url.append("?view=update");
            if (!Strings.isNullOrEmpty(__cgiTargetIdDefined)) {
              url.append('&').append(__cgiTargetIdDefined).append("=true");
            }
            Iterator<PersistentRecord> selectedNodes = getSelectedNodes();
            while (selectedNodes.hasNext()) {
              try {
                PersistentRecord selectedNode = selectedNodes.next();
                PersistentRecord selectedInstance =
                    __targetAcc.getInstance(selectedNode, __targetAccName, null, transaction);
                url.append('&').append(__cgiTargetId).append('=').append(selectedInstance.getId());
              } catch (MirrorException mEx) {
                throw new LogicException("Error resolving target regions", mEx);
              }
            }
            return url.toString();
          }

          @Override
          public boolean maySubmit()
          {
            return true;
          }

          @Override
          public boolean shouldSubmit()
          {
            return false;
          }
        };
    try {
      PersistentRecord sourceInstance =
          __sourceAcc.getInstance(node, __sourceAccName, null, transaction);
      for (PersistentRecord targetInstance : sourceInstance.opt(__targetReferencesName,
                                                                transaction)) {
        nodeQuestion.addAnswer(targetInstance.comp(TableContentCompiler.NODE));
      }
    } catch (MirrorException mEx) {
      throw new LogicException("Unable to resolve currently defined targets", mEx);
    }
    return nodeQuestion;
  }
}
