package org.cord.node.view;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.RedirectionException;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class MoveNodeCommit
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "moveNodeCommit";

  public static final String TITLE = "Move page";

  private static final String CGI_TONODEGROUPSTYLEID = "toNodeGroupStyleId";
  private static final String CGI_FILENAME = "filename";

  public MoveNodeCommit(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance(),
          Node.EDITACL,
          "You must have edit rights on a page before you can move a page into it");
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord toParentNode,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    final NodeManager nodeMgr = getNodeManager();
    PersistentRecord movingNode = null;
    try {
      movingNode =
          nodeMgr.getNode()
                 .getTable()
                 .getRecord(nodeRequest.getSession().getAttribute(MoveNodeInit.SESSION_MOVENODEID));
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       e,
                                       true,
                                       true,
                                       "Unable to resolve the page that you are moving");
    }
    String toFilename = nodeRequest.getString(CGI_FILENAME);
    if (Strings.isNullOrEmpty(toFilename)) {
      throw new FeedbackErrorException(TITLE, toParentNode, "Target filename not specified");
    }
    PersistentRecord existingChildNode =
        nodeMgr.getNode().getOptNodeByFilename(toParentNode, toFilename, null);
    if (existingChildNode != null) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       "Cannot move %s to be a child of %s named %s as that conflicts with %s",
                                       movingNode,
                                       toParentNode,
                                       toFilename,
                                       existingChildNode);
    }
    PersistentRecord toNodeGroupStyle = null;
    try {
      toNodeGroupStyle =
          nodeMgr.getNodeGroupStyle().getTable().getRecord(nodeRequest.get(CGI_TONODEGROUPSTYLEID));
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       e,
                                       true,
                                       true,
                                       "Unable to resolve target NodeGroupStyle");
    }
    try {
      nodeMgr.getNode().moveNode(movingNode, toParentNode, toNodeGroupStyle, toFilename);
    } catch (NodeMoveException e) {
      throw new FeedbackErrorException(TITLE,
                                       toParentNode,
                                       e,
                                       true,
                                       true,
                                       "Unable to complete the move operation");
    }

    nodeRequest.getSession().removeAttribute(MoveNodeInit.SESSION_MOVENODEID);
    throw new RedirectionException(toParentNode, movingNode, null);
  }
}
