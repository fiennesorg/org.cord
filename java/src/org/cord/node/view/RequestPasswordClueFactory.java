package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.node.UserStyle;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class RequestPasswordClueFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "requestPasswordClue";

  public static final String CGI_LOGIN = User.LOGIN.getKeyName();

  public RequestPasswordClueFactory(NodeManager nodeManager,
                                    PostViewAction postViewAction)
  {
    super(NAME,
          nodeManager,
          postViewAction);
  }

  private String generateError(String login)
  {
    return "There is no password clue for \"" + login + "\".";
  }

  private void throwError(String login,
                          PersistentRecord node)
      throws FeedbackErrorException
  {
    throw new FeedbackErrorException("Request password clue", node, generateError(login));
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws NodeRequestException
  {
    String login = Strings.nullToEmpty(nodeRequest.getString(CGI_LOGIN));
    PersistentRecord user = null;
    user = (getNodeManager().getUserManager().getUser(node, login, generateError(login)));
    PersistentRecord userStyle = user.comp(User.USERSTYLE);
    if (!userStyle.is(UserStyle.HASPASSWORDCLUE)) {
      throwError(login, node);
    }
    String passwordClue = user.opt(User.PASSWORDCLUE);
    if (passwordClue.length() == 0) {
      throwError(login, node);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
