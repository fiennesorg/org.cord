package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadSuccessOperation;
import org.webmacro.Context;

public class CancelFactory
  extends DynamicAclAuthenticatedFactory
{
  public final static String NAME = "cancel";

  public CancelFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD,
          Node.EDITACL,
          "You do not have permission to edit this page");
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        PersistentRecord acl)
      throws NodeRequestException
  {
    Transaction transaction = getTransaction(nodeRequest, node);
    cancelTransaction(nodeRequest, transaction);
    node = updateNode(nodeRequest, node);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
