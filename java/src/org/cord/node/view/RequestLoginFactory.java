package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.webmacro.Context;

public class RequestLoginFactory
  extends NodeRequestSegmentFactory
{
  public final static String NAME = "requestLogin";

  public RequestLoginFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager);
  }

  @Override
  public NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                        PersistentRecord node,
                                        Context context,
                                        boolean isSummaryRole)
      throws AuthenticationException
  {
    throw new AuthenticationException("", node, null);
  }
}
