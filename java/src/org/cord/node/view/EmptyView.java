package org.cord.node.view;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

/**
 * Empty implementation of NodeRequestSegmentFactory that just invokes handleSuccess on invocation.
 * 
 * @author alex
 * @see org.cord.node.NodeRequestSegmentFactory#handleSuccess(NodeRequest, PersistentRecord,
 *      Context, Exception)
 */
public class EmptyView
  extends NodeRequestSegmentFactory
{
  public EmptyView(String name,
                   NodeManager nodeManager,
                   PostViewAction postViewSuccessOperation)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation);
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              boolean isSummaryRole)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
