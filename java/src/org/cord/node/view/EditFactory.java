package org.cord.node.view;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.ContentCompiler;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeStyle;
import org.cord.node.RecordPreLockedException;
import org.cord.node.ReloadException;
import org.cord.util.LogicException;
import org.webmacro.Context;

import com.google.common.base.Preconditions;

public class EditFactory
  extends DynamicAclAuthenticatedFactory
{
  private static EditFactory INSTANCE = null;

  public static EditFactory getInstance()
  {
    return Preconditions.checkNotNull(INSTANCE);
  }

  public final static String NAME = "edit";

  public EditFactory(NodeManager nodeManager)
  {
    super(NAME,
          nodeManager,
          InternalSuccessOperation.getInstance());
    if (INSTANCE != null) {
      throw new IllegalStateException("Cannot initialise EditFactory more than once");
    }
    INSTANCE = this;
  }

  @Override
  public final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                              PersistentRecord node,
                                              Context context,
                                              PersistentRecord acl)
      throws AuthenticationException, RecordPreLockedException, ReloadException
  {
    if (!getNodeManager().getNode().isEditable(node, nodeRequest.getUser(), true)) {
      throw new AuthenticationException("You do not have permission to edit this Page.",
                                        node,
                                        null);
    }
    if (node.is(Node.ISPUBLISHED) && !node.comp(Node.NODESTYLE).is(NodeStyle.MAYEDITPUBLISHED)) {
      throw new AuthenticationException("Published versions of this page are not editable",
                                        node,
                                        null);
    }
    Transaction t = createEditTransaction(nodeRequest, node);
    node = updateNode(nodeRequest, node);
    for (PersistentRecord regionStyle : node.comp(Node.NODESTYLE).opt(NodeStyle.REGIONSTYLES)) {
      try {
        ContentCompiler contentCompiler =
            getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
        try {
          contentCompiler.editInstance(node, regionStyle, t);
        } catch (MirrorNoSuchRecordException mnsrEx) {
          throw new LogicException(contentCompiler + " has lost connection to " + regionStyle,
                                   mnsrEx);
        }
      } catch (MirrorTransactionTimeoutException mttEx) {
        cancelTransaction(nodeRequest, t);
        throw new RecordPreLockedException(node + "," + regionStyle + " is already locked",
                                           node,
                                           regionStyle,
                                           null,
                                           mttEx);
      }
    }
    throw new ReloadException(node, UpdateFactory.NAME, null);
    // return new NodeRequestSegment(getNodeManager(), nodeRequest, node,
    // context, nodeStyle
    // .getStringField(NodeStyle.EDITTEMPLATEPATH));
  }

  public void editContentCompilers(WritableRecord node,
                                   Transaction updater)
      throws MirrorTransactionTimeoutException
  {
    for (PersistentRecord regionStyle : node.comp(Node.NODESTYLE).opt(NodeStyle.REGIONSTYLES)) {
      ContentCompiler contentCompiler =
          getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
      try {
        contentCompiler.editInstance(node, regionStyle, updater);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new LogicException(contentCompiler + " has lost connection to " + regionStyle,
                                 mnsrEx);
      }
    }
  }
}
