package org.cord.node.view;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.TemplateSuccessOperation;
import org.cord.util.Gettable;
import org.webmacro.Context;

public class ViewGettable
  extends DynamicAclAuthenticatedFactory
{
  public final static String WEBMACRO_GETTABLESOURCES = "gettableSources";

  public final static String WEBMACRO_VIEWTEMPLATEPATH = "node/viewGettable.wm.html";

  private Map<String, Gettable> __gettableSources = new HashMap<String, Gettable>();

  private Map<String, Gettable> __publicGettableSources =
      Collections.unmodifiableMap(__gettableSources);

  private boolean _isLocked = false;

  /**
   * @param postViewSuccessOperation
   *          The operation to be invoked once the functionality has been completed. If null, then a
   *          default TemplateSuccessOperation will be registered pointing at
   *          WEBMACRO_VIEWTEMPLATEPATH.
   * @see #WEBMACRO_VIEWTEMPLATEPATH
   */
  public ViewGettable(String name,
                      NodeManager nodeManager,
                      PostViewAction postViewSuccessOperation,
                      Integer aclId)
  {
    super(name,
          nodeManager,
          (postViewSuccessOperation == null
              ? new TemplateSuccessOperation(nodeManager, WEBMACRO_VIEWTEMPLATEPATH)
              : postViewSuccessOperation),
          aclId,
          "You do not have permission to view the Gettable properties");
  }

  protected Gettable getGettable(String name)
  {
    return __gettableSources.get(name);
  }

  public Gettable register(String name,
                           Gettable gettable)
  {
    synchronized (__gettableSources) {
      if (_isLocked) {
        throw new IllegalStateException(getName() + " has been locked");
      }
      if (gettable == null) {
        return __gettableSources.remove(name);
      }
      return __gettableSources.put(name, gettable);
    }
  }

  public void lock()
  {
    synchronized (__gettableSources) {
      _isLocked = true;
    }
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    context.put(WEBMACRO_GETTABLESOURCES, __publicGettableSources);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
