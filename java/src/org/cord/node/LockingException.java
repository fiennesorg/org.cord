package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * Exception that is thrown during the construction of a NodeRequest to imply that there has been a
 * Transaction problem with the NodeRequest. This is a 'polite' error because it can arise without
 * any kind of programming error (i.e. when two clients try and edit the same information) and as
 * such should be handled in a gentle fashion.
 * <p>
 * The situations when it this may be thrown are as follows:-
 * <ol>
 * <li>If the User requests a lock on a record that is currently locked by another User.
 * <li>If a User attempts to perform an action on a Transaction that has since timed out. Note that
 * this might get morphed into an AuthenticationException if the User has also let his or her
 * session expire as well.
 * </ol>
 */
public class LockingException
  extends NodeRequestException
{
  /**
   * 
   */
  private static final long serialVersionUID = -5623170120898957428L;

  /**
   * @param message
   *          The details of the exception
   */
  public LockingException(String message,
                          PersistentRecord triggerNode,
                          Exception nestedException)
  {
    super(message,
          triggerNode,
          nestedException);
  }
}
