package org.cord.node;

import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.util.SkippingIterator;

public class NodePathFilter
  extends SkippingIterator<PersistentRecord>
{
  private final String __pathHeader;

  private final boolean __mustMatchHeader;

  private final String __pathFooter;

  private final boolean __mustMatchFooter;

  /**
   * @param iterator
   *          The original Iterator of TransientRecord Node objects.
   * @param pathHeader
   *          The String component that will be compared to the start of the path of the given Node.
   *          If null, then no start comparison will be made.
   * @param mustMatchHeader
   *          If pathHeader is not null and mustMatchHeader is true then only Nodes that match the
   *          pathHeader will be passed. If mustMatchHeader is false then only Nodes which don't
   *          match the pathHeader will be passed. This enables the inclusion or exclusion of
   *          subtrees of Nodes.
   * @param pathFooter
   *          The String component that will be compared to the end of the path of a given Node. If
   *          null, then no end comparison will be made.
   * @param mustMatchFooter
   *          If pathFooter is not null and mustMatchFooter is true then only Nodes that have the
   *          trailing path matching pathFooter will be passed. If mustMatchFooter is false then
   *          only Nodes that don't have the specified pathFooter will be passed.
   */
  public NodePathFilter(Iterator<PersistentRecord> iterator,
                        String pathHeader,
                        boolean mustMatchHeader,
                        String pathFooter,
                        boolean mustMatchFooter)
  {
    super(iterator);
    __pathHeader = pathHeader;
    __mustMatchHeader = mustMatchHeader;
    __pathFooter = pathFooter;
    __mustMatchFooter = mustMatchFooter;
  }

  @Override
  public boolean shouldSkip(PersistentRecord node)
  {
    String path = node.opt(Node.PATH);
    if (__pathHeader != null) {
      if (path.startsWith(__pathHeader)) {
        if (!__mustMatchHeader) {
          return true;
        }
      } else {
        if (__mustMatchHeader) {
          return true;
        }
      }
    }
    if (__pathFooter != null) {
      if (path.endsWith(__pathFooter)) {
        if (!__mustMatchFooter) {
          return true;
        }
      } else {
        if (__mustMatchFooter) {
          return true;
        }
      }
    }
    return false;
  }
}
