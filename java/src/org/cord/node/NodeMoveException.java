package org.cord.node;

public class NodeMoveException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public NodeMoveException(String message,
                           Throwable cause)
  {
    super(message,
          cause);
  }
}
