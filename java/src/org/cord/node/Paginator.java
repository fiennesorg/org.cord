package org.cord.node;

import java.io.IOException;
import java.util.ListIterator;

import org.cord.mirror.IdList;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SubRecordSource;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.NumberUtil;

import com.google.common.collect.Multimap;

/**
 * The Paginator provides a convenient way of dividing up a Query into a series of discrete pages
 * while remaining on a single Node viewpoint.
 */
public class Paginator
  extends AbstractPaginator
{
  public final static String CGI_PAGEINDEX = "PageIndex";

  public final static String CGI_PAGESIZE = "PageSize";

  public final static String WEBMACRO_ITEMNAME = "itemName";
  public final static String WEBMACRO_ITEMSNAME = "itemsName";

  private Table _targetTable = null;

  private IdList _recordIdList = null;

  private int _pageSize = 0;

  /**
   * The name ("default") that will be used for the Paginator in the absence of a user supplied
   * name.
   */
  public final static String DEFAULT_NAME = "default";

  /**
   * @param pageIndex
   *          The index of the page that this Paginator is to represent.
   *          1<=pageIndex<=getPageCount()
   * @param pageSize
   *          The size of the pages that the Query is to be chopped up into. A value of <= 0 will be
   *          equivalent to a single Page with no Pagination required.
   * @see #getPageCount()
   */
  public Paginator(String name,
                   RecordSource sourceQuery,
                   int pageIndex,
                   int pageSize,
                   Multimap<String, String> additionalFields,
                   Table targetTable,
                   IdList recordIdList,
                   boolean hasExplicitPage)
  {
    super(name,
          sourceQuery,
          additionalFields,
          hasExplicitPage);
    setRawResults(targetTable, recordIdList);
    setPageSize(pageSize);
    setPageIndex(pageIndex);
  }

  public void setPageSize(int pageSize)
  {
    _pageSize = Math.max(pageSize, 1);
  }

  public boolean usesRawResults()
  {
    return _targetTable != null && _recordIdList != null;
  }

  /**
   * Inform this Paginator that it is not going to use a Query as its record source, but instead is
   * going to operate across a set of raw record ids. If the incoming items are defined, then the
   * source Query for this Paginator is set to null (if defined) which should be taken into
   * consideration when using getSourceQuery() in code.
   * 
   * @param targetTable
   *          The table that recordIdList is assumed to be taken from. If null then the command is
   *          ignored.
   * @param recordIdList
   *          IdList of record ids from targetTable. if null then the command is ignored.
   * @see #setSourceQuery(RecordSource)
   * @see #getSourceQuery()
   */
  public final void setRawResults(Table targetTable,
                                  IdList recordIdList)
  {
    if (targetTable != null && recordIdList != null) {
      super.setSourceQuery(null);
      _targetTable = targetTable;
      _recordIdList = recordIdList;
    } else {
      _targetTable = null;
      _recordIdList = null;
    }
  }

  @Override
  public Table getTable()
  {
    return _targetTable != null ? _targetTable : super.getTable();
  }

  public void setPageIndexByTarget(PersistentRecord targetRecord)
  {
    int id = targetRecord.getId();
    int pageIndex = -1;
    if (usesRawResults()) {
      pageIndex = _recordIdList.indexOf(id) / getPageSize();
    } else {
      pageIndex = getSourceQuery().getIdList().indexOf(id) / getPageSize();
    }
    if (pageIndex != -1) {
      setPageIndex(pageIndex + 1);
    }
  }

  public Table getTargetTable()
  {
    if (_targetTable != null) {
      return _targetTable;
    }
    RecordSource sourceQuery = getSourceQuery();
    if (sourceQuery != null) {
      return sourceQuery.getTable();
    }
    return null;
  }

  public IdList getRecordIdList()
  {
    if (_recordIdList != null) {
      return _recordIdList;
    }
    RecordSource sourceQuery = getSourceQuery();
    if (sourceQuery != null) {
      return sourceQuery.getIdList();
    }
    return null;
  }

  @Override
  public void setSourceQuery(RecordSource sourceQuery)
  {
    if (sourceQuery != null) {
      _targetTable = null;
      _recordIdList = null;
    }
    super.setSourceQuery(sourceQuery);
  }

  /**
   * Create a Paginator that takes the parameters from pageIndex and pageSize from the incoming
   * NodeRequest.
   */
  public Paginator(String name,
                   Gettable params,
                   int defaultPageSize)
  {
    this(name,
         null,
         GettableUtils.getInt(params, createParameterName(name, CGI_PAGEINDEX), PAGEINDEX_FIRST),
         GettableUtils.getInt(params, createParameterName(name, CGI_PAGESIZE), defaultPageSize),
         null,
         null,
         null,
         GettableUtils.get(params, createParameterName(name, CGI_PAGEINDEX), null) != null);
  }

  public String getGetParams()
  {
    StringBuilder result = new StringBuilder();
    appendGetParam(result, getAdditionalFields());
    appendGetParam(result, createParameterName(CGI_PAGEINDEX), NumberUtil.toString(getPageIndex()));
    appendGetParam(result, createParameterName(CGI_PAGESIZE), NumberUtil.toString(getPageSize()));
    return result.toString();
  }

  @Override
  public void appendCoreFormFields(Appendable buffer)
      throws IOException
  {
    appendFormField(buffer, createParameterName(CGI_PAGEINDEX), getPageIndex());
    appendFormField(buffer, createParameterName(CGI_PAGESIZE), getPageSize());
  }

  public final int getPageSize()
  {
    return _pageSize;
  }

  private int getStartIndex()
  {
    return (getPageIndex() - 1) * _pageSize;
  }

  public RecordSource getRecordSource()
  {
    IdList recordIdList = getRecordIdList();
    if (recordIdList != null) {
      if (recordIdList.size() == 0) {
        return getTable().getEmptyRecordSource();
      }
      int startIndex = getStartIndex();
      int endIndex = Math.min(startIndex + _pageSize, recordIdList.size());
      return ConstantRecordSource.fromIds(getTable(),
                                          null,
                                          recordIdList.subList(startIndex, endIndex));
    } else {
      RecordSource recordSource = getSourceQuery();
      if (recordSource == null) {
        return getTable().getEmptyRecordSource();
      }
      return new SubRecordSource(null, recordSource, getPageIndex(), getPageIndex() + _pageSize);
    }
  }

  public RecordSource getRecordSource(Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(getRecordSource(), viewpoint);
  }

  @Deprecated
  public ListIterator<PersistentRecord> getRecords(Viewpoint viewpoint)
  {
    return getRecordSource(viewpoint).getRecordList().listIterator();
  }

  @Deprecated
  public ListIterator<PersistentRecord> getRecords()
  {
    return getRecords(null);
  }

  /**
   * Get the Paginator that corresponds to the requested pageIndex
   */
  @Override
  public AbstractPaginator getPage(int pageIndex)
  {
    return new Paginator(getName(),
                         getSourceQuery(),
                         pageIndex,
                         _pageSize,
                         getAdditionalFields(),
                         _targetTable,
                         _recordIdList,
                         true);
  }

  /**
   * Get the Paginator that contains the record with the supplied id.
   * 
   * @return the appropriate Paginator or this current paginator if the requested id isn't found in
   *         the search query.
   */
  public AbstractPaginator getPageById(Integer id)
  {
    IdList recordIdList = getRecordIdList();
    int index = -1;
    if (recordIdList != null) {
      index = recordIdList.indexOf(id);
    } else {
      RecordSource sourceQuery = getSourceQuery();
      if (sourceQuery != null) {
        index = sourceQuery.getIdList().indexOf(id);
      }
    }
    return index == -1 ? this : getPage(index / _pageSize + 1);
  }

  /**
   * Get the number of discrete pages that the current sourceQuery divides up into. If the pageSize
   * is set to 0 (ie unlimited) then this will report that there is a single global page.
   * 
   * @return The number of pages as calculated from the size of the sourceQuery and the pageSize. If
   *         the pageSize is 0, then the result will be 1. If the sourceQuery is null then the
   *         pageSize will also be 1 (to provide consistency and because an empty list still has one
   *         empty page in it (i think)).
   */
  @Override
  public int getPageCount()
  {
    switch (_pageSize) {
      case 0:
        return 1;
      default:
        switch (getRecordCount()) {
          case 0:
            return 1;
          default:
            int pageCount = getRecordCount() / _pageSize;
            return (getRecordCount() % _pageSize > 0 ? pageCount + 1 : pageCount);
        }
    }
  }

  /**
   * @return the number of records contained in the source Query, or 0 if the source Query is
   *         undefined.
   */
  public int getRecordCount()
  {
    if (_recordIdList != null) {
      return _recordIdList.size();
    }
    return getSourceQuery() == null ? 0 : getSourceQuery().getIdList().size();
  }

  @Override
  protected boolean customEquals(AbstractPaginator paginator)
  {
    return (((Paginator) paginator).getPageSize() == getPageSize());
  }

  @Override
  public int subHashCode()
  {
    return getPageSize();
  }
}
