package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.webmacro.Context;

import com.google.common.base.Strings;

public abstract class TaskFactoryView
  extends TaskManagerView
{
  public static final String CGI_TASKFACTORYNAME = "taskFactoryName";

  private final String __taskFactoryName;

  public TaskFactoryView(String name,
                         NodeManager nodeManager,
                         PostViewAction postViewAction,
                         Integer aclId,
                         String authErr,
                         TaskManager taskManager,
                         String taskFactoryName)
  {
    super(name,
          nodeManager,
          postViewAction,
          aclId,
          authErr,
          taskManager);
    __taskFactoryName = taskFactoryName;
  }

  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl,
                                                 TaskManager taskManager)
      throws NodeRequestException
  {
    String taskFactoryName = Strings.isNullOrEmpty(__taskFactoryName)
        ? nodeRequest.getString(CGI_TASKFACTORYNAME)
        : __taskFactoryName;
    TaskFactory taskFactory = getTaskManager().getTaskFactory(taskFactoryName);
    if (taskFactory == null) {
      throw new FeedbackErrorException(null,
                                       node,
                                       "Unable to resolve taskFactory named: %s",
                                       taskFactoryName);
    }
    context.put("taskFactory", taskFactory);
    return getInstance(nodeRequest, node, context, acl, taskManager, taskFactory);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    TaskManager taskManager,
                                                    TaskFactory taskFactory)
      throws NodeRequestException;
}
