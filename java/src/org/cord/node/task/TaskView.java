package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.task.Task;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.webmacro.Context;

import com.google.common.base.Strings;

public abstract class TaskView
  extends TaskFactoryView
{
  public static final String CGI_TASKNAME = "taskName";

  private final String __taskName;

  public TaskView(String name,
                  NodeManager nodeManager,
                  PostViewAction postViewAction,
                  Integer aclId,
                  String authErr,
                  TaskManager taskManager,
                  String taskFactoryName,
                  String taskName)
  {
    super(name,
          nodeManager,
          postViewAction,
          aclId,
          authErr,
          taskManager,
          taskFactoryName);
    __taskName = taskName;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           TaskManager taskManager,
                                           TaskFactory taskFactory)
      throws NodeRequestException
  {
    String taskName =
        Strings.isNullOrEmpty(__taskName) ? nodeRequest.getString(CGI_TASKNAME) : __taskName;
    Task task = taskFactory.getTask(taskName);
    if (task == null) {
      throw new FeedbackErrorException(null,
                                       node,
                                       "Unable to resolve task named:%s in %s",
                                       taskName,
                                       taskFactory);
    }
    context.put("task", task);
    return getInstance(nodeRequest, node, context, acl, taskManager, taskFactory, task);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    TaskManager taskManager,
                                                    TaskFactory taskFactory,
                                                    Task task)
      throws NodeRequestException;
}
