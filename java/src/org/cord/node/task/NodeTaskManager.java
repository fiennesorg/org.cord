package org.cord.node.task;

import org.cord.node.NodeManager;
import org.cord.node.ReloadSuccessOperation;
import org.cord.task.TaskManager;

/**
 * Wrapper around the TaskManager that provides Node specific manipulation views.
 * 
 * @author alex
 * @see TaskManager
 */
public class NodeTaskManager
{
  private final NodeManager __nodeManager;

  private final TaskManager __taskManager;

  public NodeTaskManager(NodeManager nodeManager)
  {
    __nodeManager = nodeManager;
    __taskManager = new TaskManager(nodeManager.getGettable());
  }

  public TaskManager getTaskManager()
  {
    return __taskManager;
  }

  public void registerDefaultViews(Integer aclAdminId)
  {
    __nodeManager.getNodeRequestSegmentManager()
                 .register(new LaunchTask("launchTask",
                                          __nodeManager,
                                          ReloadSuccessOperation.DEFAULT_RELOAD,
                                          aclAdminId,
                                          "You do not have permission to launch tasks",
                                          getTaskManager(),
                                          null,
                                          null));
    __nodeManager.getNodeRequestSegmentManager()
                 .register(new StopTask("stopTask",
                                        __nodeManager,
                                        ReloadSuccessOperation.DEFAULT_RELOAD,
                                        aclAdminId,
                                        "You do not have permission to stop tasks",
                                        getTaskManager(),
                                        null,
                                        null));
  }
}
