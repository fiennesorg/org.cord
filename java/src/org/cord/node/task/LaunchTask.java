package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.task.Task;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.cord.util.DelegatingGettable;
import org.cord.util.Gettable;
import org.webmacro.Context;

public class LaunchTask
  extends TaskFactoryView
{
  private final Gettable __input;

  /**
   * @param taskFactoryName
   *          If defined then this will be the TaskFactory that is used to launch the Task. If not
   *          defined then it will be resolved from
   * @param input
   *          If this is defined then it will take priority over the NodeRequest in defining the
   *          launch behaviour of the task. If you define all of the fields required in the task in
   *          this Gettable then the NodeRequest will become irrelevant and the launch behaviour
   *          will become constant.
   */
  public LaunchTask(String name,
                    NodeManager nodeManager,
                    PostViewAction postViewAction,
                    Integer aclId,
                    String authErr,
                    TaskManager taskManager,
                    String taskFactoryName,
                    Gettable input)
  {
    super(name,
          nodeManager,
          postViewAction,
          aclId,
          authErr,
          taskManager,
          taskFactoryName);
    __input = input;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           TaskManager taskManager,
                                           TaskFactory taskFactory)
      throws NodeRequestException
  {
    Gettable input = nodeRequest;
    if (__input != null) {
      DelegatingGettable dg = new DelegatingGettable(false);
      dg.addGettable(__input);
      dg.addGettable(nodeRequest);
      input = dg;
    }
    try {
      Task task = taskFactory.launchTask(input);
      context.put("task", task);
    } catch (Exception e) {
      throw new FeedbackErrorException("Launch task",
                                       node,
                                       e,
                                       true,
                                       true,
                                       "Unable to start task: %s",
                                       e);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
