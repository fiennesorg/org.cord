package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.task.Task;
import org.cord.task.TaskFactory;
import org.cord.task.TaskManager;
import org.webmacro.Context;

public class StopTask
  extends TaskView
{
  public StopTask(String name,
                  NodeManager nodeManager,
                  PostViewAction postViewAction,
                  Integer aclId,
                  String authErr,
                  TaskManager taskManager,
                  String taskFactoryName,
                  String taskName)
  {
    super(name,
          nodeManager,
          postViewAction,
          aclId,
          authErr,
          taskManager,
          taskFactoryName,
          taskName);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           TaskManager taskManager,
                                           TaskFactory taskFactory,
                                           Task task)
      throws NodeRequestException
  {
    task.stopTask();
    return handleSuccess(nodeRequest, node, context, null);
  }
}
