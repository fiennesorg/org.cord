package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.task.TaskManager;
import org.webmacro.Context;

public abstract class TaskManagerView
  extends DynamicAclAuthenticatedFactory
{
  private final TaskManager __taskManager;

  public TaskManagerView(String name,
                         NodeManager nodeManager,
                         PostViewAction postViewAction,
                         Integer aclId,
                         String authErr,
                         TaskManager taskManager)
  {
    super(name,
          nodeManager,
          postViewAction,
          aclId,
          authErr);
    __taskManager = taskManager;
  }

  protected TaskManager getTaskManager()
  {
    return __taskManager;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    TaskManager taskManager = getTaskManager();
    context.put("taskManager", taskManager);
    return getInstance(nodeRequest, node, context, acl, taskManager);
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    TaskManager taskManager)
      throws NodeRequestException;
}
