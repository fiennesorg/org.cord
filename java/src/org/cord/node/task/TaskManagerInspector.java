package org.cord.node.task;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.TemplateSuccessOperation;
import org.cord.task.TaskManager;
import org.webmacro.Context;

/**
 * View that returns a template that can perform an inspection on the registered TaskManager object.
 * 
 * @author alex
 */
public class TaskManagerInspector
  extends TaskManagerView
{
  public TaskManagerInspector(String name,
                              NodeManager nodeManager,
                              String viewTemplate,
                              Integer aclId,
                              String authErr,
                              TaskManager taskManager)
  {
    super(name,
          nodeManager,
          new TemplateSuccessOperation(nodeManager, viewTemplate),
          aclId,
          authErr,
          taskManager);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           TaskManager taskManager)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }
}
