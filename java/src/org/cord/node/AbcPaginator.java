package org.cord.node;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.recordsource.operation.StartsWithFilter;
import org.cord.util.Gettable;

import com.google.common.base.Strings;
import com.google.common.collect.Multimap;

public class AbcPaginator
  extends AbstractPaginator
{
  public final static char ABCFILTER_START = 'A';

  public final static char ABCFILTER_END = 'Z';

  public final static char ABCFILTER_ALL = '*';

  public final static char ABCFILTER_NONE = '!';

  public final static String CGI_ABCFIELD = "AbcField";

  public final static String CGI_ABCFILTER = "AbcFilter";

  private FieldKey<String> _abcField;

  private char _abcFilter;

  public AbcPaginator(String name,
                      RecordSource sourceQuery,
                      FieldKey<String> abcField,
                      char abcFilter,
                      Multimap<String, String> additionalFields,
                      boolean hasExplicitPage)
  {
    super(name,
          sourceQuery,
          additionalFields,
          hasExplicitPage);
    setAbcField(abcField);
    setAbcFilter(abcFilter);
  }

  public AbcPaginator(String name,
                      RecordSource sourceQuery,
                      FieldKey<String> abcField,
                      String abcFilter,
                      Multimap<String, String> additionalFields,
                      boolean hasExplicitPage)
  {
    this(name,
         sourceQuery,
         abcField,
         abcFilter == null || abcFilter.length() == 0 ? ABCFILTER_ALL : abcFilter.charAt(0),
         additionalFields,
         hasExplicitPage);
  }

  public AbcPaginator(AbcPaginator sourcePaginator,
                      FieldKey<String> abcField,
                      char abcFilter)
  {
    super(sourcePaginator);
    setAbcField(abcField);
    setAbcFilter(abcFilter);
  }

  public AbcPaginator(String name,
                      Gettable nodeRequest)
  {
    this(name,
         null,
         optStringFieldKey(nodeRequest.getString(createParameterName(name, CGI_ABCFIELD))),
         nodeRequest.getString(createParameterName(name, CGI_ABCFILTER)),
         null,
         nodeRequest.getString(createParameterName(name, CGI_ABCFILTER)) != null);
    importAdditionalFields(nodeRequest);
  }

  private static ObjectFieldKey<String> optStringFieldKey(String key)
  {
    return key == null ? null : ObjectFieldKey.create(key, String.class);
  }

  public AbcPaginator getAllPage()
  {
    if (getAbcFilter() == ABCFILTER_ALL) {
      return this;
    }
    return new AbcPaginator(this, getAbcField(), ABCFILTER_ALL);
  }

  public AbcPaginator getNonePage()
  {
    if (getAbcFilter() == ABCFILTER_NONE) {
      return this;
    }
    return new AbcPaginator(this, getAbcField(), ABCFILTER_NONE);
  }

  public Paginator getFilteredPaginator(NodeRequest nodeRequest,
                                        int pageSize)
  {
    Paginator fp = new Paginator(getName() + "_p", nodeRequest, pageSize);
    fp.setSourceQuery(getFilteredQuery());
    fp.addAdditionalField(createParameterName(CGI_ABCFIELD), getAbcField());
    fp.addAdditionalField(createParameterName(CGI_ABCFILTER), Character.toString(getAbcFilter()));
    Multimap<String, String> additionalFields = getAdditionalFields();
    if (additionalFields != null) {
      Iterator<Map.Entry<String, String>> additionalFieldEntrys =
          additionalFields.entries().iterator();
      while (additionalFieldEntrys.hasNext()) {
        Map.Entry<String, String> additionalFieldEntry = additionalFieldEntrys.next();
        fp.addAdditionalField(additionalFieldEntry.getKey(), additionalFieldEntry.getValue());
      }
    }
    return fp;
  }

  @Override
  public String toString()
  {
    return getName() + "(" + getAbcField() + "," + getAbcFilter() + ")";
  }

  /**
   * @return 26
   */
  @Override
  public final int getPageCount()
  {
    return 26;
  }

  public String getGetParams()
  {
    StringBuilder result = new StringBuilder();
    appendGetParam(result, getAdditionalFields());
    appendGetParam(result, createParameterName(CGI_ABCFIELD), getAbcField().getKeyName());
    appendGetParam(result, createParameterName(CGI_ABCFILTER), Character.toString(getAbcFilter()));
    return result.toString();
  }

  public final void setAbcFilter(char abcFilter)
  {
    if (abcFilter == ABCFILTER_NONE) {
      _abcFilter = abcFilter;
      return;
    }
    if (!Character.isLetter(abcFilter)) {
      _abcFilter = ABCFILTER_ALL;
    } else {
      if (Character.isLowerCase(abcFilter)) {
        abcFilter = Character.toUpperCase(abcFilter);
      }
      _abcFilter = abcFilter;
      setPageIndex(abcFilter - ABCFILTER_START + PAGEINDEX_FIRST);
    }
  }

  public char getAbcFilter()
  {
    return _abcFilter;
  }

  public FieldKey<String> getAbcField()
  {
    return _abcField;
  }

  public final void setAbcField(FieldKey<String> abcField)
  {
    _abcField = abcField;
  }

  public final void setAbcField(String abcField)
  {
    setAbcField(optStringFieldKey(abcField));
  }

  private final StartsWithFilter __startsWithFilter = new StartsWithFilter();

  public RecordSource getFilteredQuery()
  {
    RecordSource recordSource = getSourceQuery();
    if (_abcFilter == ABCFILTER_ALL) {
      return recordSource;
    }
    if (_abcFilter == ABCFILTER_NONE) {
      return recordSource.getTable().getEmptyRecordSource();
    }
    return __startsWithFilter.calculate(recordSource,
                                        null,
                                        _abcField.getKeyName(),
                                        Character.toString(_abcFilter));
    // Query query = null;
    // if (recordSource instanceof QueryWrapper) {
    // query = ((QueryWrapper) recordSource).getQuery();
    // } else if (recordSource instanceof Query) {
    // query = (Query) recordSource;
    // } else {
    // return new SingleRecordSourceFilter(recordSource, null) {
    // @Override
    // protected boolean accepts(PersistentRecord record)
    // {
    // String value = record.getStringField(_abcField);
    // if (value.length() == 0) {
    // return false;
    // }
    // return value.charAt(0) == _abcFilter;
    // }
    // };
    // }
    // String filter = _abcField + " LIKE '" + _abcFilter + "%'";
    // return ConstantQueryTransform.transformQuery(query,
    // filter,
    // ConstantQueryTransform.TYPE_AND,
    // null);
  }

  public void setAbcFilter(String value)
  {
    setAbcFilter(Strings.isNullOrEmpty(value) ? ABCFILTER_ALL : value.charAt(0));
  }

  public void setAbcFilter(PersistentRecord targetRecord)
  {
    setAbcFilter(targetRecord.opt(_abcField));
  }

  @Override
  protected boolean customEquals(AbstractPaginator paginator)
  {
    return getAbcField().equals(((AbcPaginator) paginator).getAbcField());
  }

  @Override
  protected int subHashCode()
  {
    return getAbcField().hashCode();
  }

  @Override
  public AbstractPaginator getPage(int pageIndex)
  {
    if (pageIndex == getPageIndex()) {
      return this;
    }
    return new AbcPaginator(this,
                            getAbcField(),
                            (char) (ABCFILTER_START + pageIndex - PAGEINDEX_FIRST));
  }

  /**
   * Get the AbcPaginator from this AbcPaginator that would contain the filterString based around
   * the first character in the String. If the filterString has no contents, or if the filterString
   * starts with a character outside the range of this paginator (a-z cas insensitive), then it will
   * return a copy of the original AbcPaginator.
   * 
   * @param filterString
   *          The String whose first character is used to decide the target Paginator.
   * @return The appropriate AbcPaginator. Never null.
   */
  public AbcPaginator getPage(String filterString)
  {
    if (Strings.isNullOrEmpty(filterString)) {
      return this;
    }
    char firstChar = Character.toUpperCase(filterString.charAt(0));
    return (firstChar < 'A' || firstChar > 'Z'
        ? new AbcPaginator(this, getAbcField(), ABCFILTER_ALL)
        : new AbcPaginator(this, getAbcField(), firstChar));
  }

  @Override
  public void appendCoreFormFields(Appendable buffer)
      throws IOException
  {
    appendFormField(buffer, createParameterName(CGI_ABCFIELD), getAbcField());
    appendFormField(buffer, createParameterName(CGI_ABCFILTER), Character.toString(getAbcFilter()));
  }

  public boolean isAll()
  {
    return getAbcFilter() == ABCFILTER_ALL;
  }
}
