package org.cord.node;

/**
 * Enum that represents the different types of HTTP method that can be encapsulated within a
 * NodeRequest
 * 
 * @author alex
 */
public enum HttpMethod {
  GET,
  HEAD,
  PUT,
  DELETE,
  POST
}
