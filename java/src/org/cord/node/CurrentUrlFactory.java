package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

/**
 * ValueFactory that returns the url of the TargetLeafNode of the incoming NodeRequest.
 * 
 * @author alex
 * @see Node#URL
 * @see NodeRequest#getTargetLeafNode()
 */
public final class CurrentUrlFactory
  implements ValueFactory, SingletonShutdownable
{
  private static CurrentUrlFactory _instance = new CurrentUrlFactory();

  public static CurrentUrlFactory getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private CurrentUrlFactory()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
      throws MirrorNoSuchRecordException
  {
    return nodeRequest.getTargetLeafNode().opt(Node.URL);
  }

  @Override
  public void shutdown()
  {
  }
}
