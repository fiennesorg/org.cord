package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * NodeRequestException that signifies that a Record has already been locked by another User thereby
 * making the requested view impossible to complete. However, if the currently locked record is
 * stealable then the SessionTransaction that owns the record will be embedded in the Exception.
 */
public class RecordPreLockedException
  extends LockingException
{
  /**
   * 
   */
  private static final long serialVersionUID = -2572401465854179433L;

  private final PersistentRecord _lockedRecord;

  private final NodeUserManager.SessionTransaction _stealableSessionTransaction;

  public RecordPreLockedException(String message,
                                  PersistentRecord triggerNode,
                                  PersistentRecord lockedRecord,
                                  NodeUserManager.SessionTransaction stealableSessionTransaction,
                                  Exception nestedException)
  {
    super(message,
          triggerNode,
          nestedException);
    _lockedRecord = lockedRecord;
    _stealableSessionTransaction = stealableSessionTransaction;
  }

  public final PersistentRecord getLockedRecord()
  {
    return _lockedRecord;
  }

  public final NodeUserManager.SessionTransaction getStealableSessionTransaction()
  {
    return _stealableSessionTransaction;
  }
}
