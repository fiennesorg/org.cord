package org.cord.node;

import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;

/**
 * String constants used to define the Node database structure.
 */
public interface NodeDbConstants
{
  // *******************
  // GENERIC DEFINITIONS
  // *******************
  /**
   * Generic field name ("name") for name fields in the db definition.
   */
  public final static ObjectFieldKey<String> NAME = StringField.createKey("name");

  /**
   * Gemeroc field name ("named" for named field lookups in the the db definition.
   */
  public final static MonoRecordSourceOperationKey<String, RecordSource> NAMED =
      MonoRecordSourceOperationKey.create("named", String.class, RecordSource.class);

  /**
   * Generic field name ("number") for fields that imply an ordering onto a set of records.
   */
  public final static IntFieldKey NUMBER = IntField.createKey("number");

  /**
   * Generic field anem ("filename") for fields that contain a URL constructing filename. These will
   * all have an alphanumeric restriction placed on them that will restrict the valid character set
   * to "a-zA-Z_-".
   */
  public final static ObjectFieldKey<String> FILENAME = StringField.createKey("filename");

  /**
   * "compilerStyleName"
   */
  public final static ObjectFieldKey<String> COMPILERSTYLENAME =
      StringField.createKey("compilerStyleName");
}
