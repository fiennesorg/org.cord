package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.node.view.ReadViewFactory;

/**
 * The ReloadException is a NodeRequestException that should be thrown by a
 * NodeRequestSegmentFactory when it wishes to re-evaluate the current node, either utilising the
 * default or another view parameter. The reloading of the Node is taken place under the same http
 * request cycle without intervention from the client so is comparatively fast and transparent.
 */
public class ReloadException
  extends NodeRequestException
{
  /**
   * 
   */
  private static final long serialVersionUID = -8666796788146418103L;

  private final String _targetView;

  /**
   * Request that the Node is reloaded with an explicit view. This is the equivalent of Servlet
   * chaining, only applied to views on a particular Node. Please note that just because you ask for
   * a particular view doesn't mean that you will sidestep all of the normal security processes...
   * 
   * @param triggerNode
   *          The node which was being evaluated when the exception was thrown.
   * @param targetView
   *          The name of the view which you would like.
   */
  public ReloadException(PersistentRecord triggerNode,
                         String targetView,
                         Exception nestedException)
  {
    super(targetView,
          triggerNode,
          nestedException);
    _targetView = (targetView == null ? ReadViewFactory.NAME : targetView);
  }

  /**
   * Request that the Node is reloaded utilising the default view for the Node (that is determined
   * by the current lock status of the Node with respect to the original request).
   * 
   * @param triggerNode
   *          The node which was being evaluated when the exception was thrown.
   */
  public ReloadException(PersistentRecord triggerNode,
                         Exception nestedException)
  {
    this(triggerNode,
         null,
         nestedException);
  }

  public String getTargetView()
  {
    return _targetView;
  }
}
