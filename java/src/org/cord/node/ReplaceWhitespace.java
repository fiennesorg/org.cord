package org.cord.node;

import java.util.regex.Pattern;

import org.cord.util.StringUtils;

/**
 * Implementation of FilenameGenerator that replaces non "valid" URL characters with the string of
 * your choice. Sub-class implementation are provided for Dot, Hyphen and Underscore which can be
 * referenced from the properties file for your node application as
 * "org.cord.node.ReplaceWhitespace$Dot" in the filenameGeneratorClassName field.
 */
public class ReplaceWhitespace
  implements FilenameGenerator
{
  private final String __replacement;

  private final Pattern __deleteChars;

  private final Pattern __whitespace;

  /**
   * @param deleteRegex
   *          The regex that will have all occurences of itself deleted from the filename as the
   *          initial stage of processing. This is intended to permit the removal of "internal
   *          punctuation" and the like (eg "john's boat" --> "johns boat") and the removal of
   *          leading and trailing whitespace that doesn't want replacing (eg " dog " --> "dog").
   * @param whitespaceRegex
   *          The regex that will be taken to match any occurence of what may be taken as whitespace
   *          and which will be replace by the replacement String.
   * @param replacement
   *          The String that is to be taken as the single replacement value for any occurence of
   *          whitespaceRegex. This could be a multi-character string, but is normally just a single
   *          character.
   */
  public ReplaceWhitespace(String deleteRegex,
                           String whitespaceRegex,
                           String replacement)
  {
    __replacement = replacement;
    __deleteChars = Pattern.compile(deleteRegex);
    __whitespace = Pattern.compile(whitespaceRegex);
  }

  @Override
  public String generateFilename(String proposedFilename)
  {
    String result = StringUtils.removeAccents(proposedFilename);
    result = __deleteChars.matcher(result).replaceAll("");
    result = __whitespace.matcher(result).replaceAll(__replacement);
    return result;
  }

  public static class Dot
    extends ReplaceWhitespace
  {
    public Dot()
    {
      super("[']+|^[^\\w@-]+|[^\\w@-]+$",
            "[^\\w@-]+",
            ".");
    }
  }

  public static class Hyphen
    extends ReplaceWhitespace
  {
    public Hyphen()
    {
      super("[']+|^[^\\w\\.@]+|[^\\w\\.@]+$",
            "[^\\w\\.@]+",
            "-");
    }
  }

  public static class Underscore
    extends ReplaceWhitespace
  {
    public Underscore()
    {
      super("[']+|^[^\\w\\.@-]+|[^\\w\\.@-]+$",
            "[^\\w\\.@-]+",
            "_");
    }
  }

  public static void main(String[] args)
  {
    Dot dot = new Dot();
    Hyphen hyphen = new Hyphen();
    Underscore underscore = new Underscore();
    String test =
        "   before.after before . after before-after before - after before_after before - after   ";
    System.out.println(test);
    System.out.println(dot.generateFilename(test));
    System.out.println(hyphen.generateFilename(test));
    System.out.println(underscore.generateFilename(test));
    test = "The dog's bollox.  Normal sentence, with punctuation!";
    System.out.println(test);
    System.out.println(dot.generateFilename(test));
    System.out.println(hyphen.generateFilename(test));
    System.out.println(underscore.generateFilename(test));
  }
}