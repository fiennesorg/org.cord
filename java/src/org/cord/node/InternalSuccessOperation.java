package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.webmacro.Context;

/**
 * PostViewAction that is utilised when a view is expected to process the result internally and when
 * delegating to handleSuccess is viewed as an error which is reported back to the client as a
 * FeedbackErrorException.
 */
public final class InternalSuccessOperation
  implements PostViewAction, SingletonShutdownable
{
  private static InternalSuccessOperation _instance = new InternalSuccessOperation();

  public static InternalSuccessOperation getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private InternalSuccessOperation()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws FeedbackErrorException
  {
    throw new FeedbackErrorException("Internal Failure",
                                     node,
                                     nestedException,
                                     true,
                                     true,
                                     "The view invoked has failed to process the incoming data");
  }
}
