package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordFilter;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * RecordFilter that takes Node or NodeStyle records and compares them against a list of approved
 * NodeStyle id values.
 */
public class NodeStyleFilter
  implements RecordFilter
{
  private IntSet _validIds = new IntOpenHashSet();

  public NodeStyleFilter()
  {
  }

  public void addValidNodeId(int id)
  {
    _validIds.add(id);
  }

  @Override
  public boolean accepts(Table table,
                         int recordId)
  {
    return accepts(table.getOptRecord(recordId));
  }

  @Override
  public boolean accepts(PersistentRecord record)
  {
    if (record == null) {
      return false;
    }
    PersistentRecord nodeStyle = null;
    String tableName = record.getTable().getName();
    if (NodeStyle.TABLENAME.equals(tableName)) {
      nodeStyle = record;
    } else if (Node.TABLENAME.equals(tableName)) {
      nodeStyle = record.comp(Node.NODESTYLE);
    } else {
      return false;
    }
    return _validIds.contains(nodeStyle.getId());
  }
}