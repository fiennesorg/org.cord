package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.StringField;

public class UserStyle
  extends NodeTableWrapper
{
  public final static String TABLENAME = "UserStyle";

  public final static ObjectFieldKey<String> NAME = StringField.createKey("name");

  public final static ObjectFieldKey<Boolean> HASPLAINTEXTPASSWORD =
      BooleanField.createKey("hasPlainTextPassword");

  public final static ObjectFieldKey<Boolean> HASPASSWORDCLUE =
      BooleanField.createKey("hasPasswordClue");

  public final static ObjectFieldKey<Boolean> MAYDELETE = BooleanField.createKey("mayDelete");

  public final static ObjectFieldKey<Boolean> ISMULTISESSION =
      BooleanField.createKey("isMultiSession");

  public final static RecordOperationKey<RecordSource> USERS =
      RecordOperationKey.create("users", RecordSource.class);

  public final static int ID_PARANOID = 1;

  public final static int ID_HIGH = 2;

  public final static int ID_MEDIUM = 3;

  public final static int ID_LOW = 4;

  protected UserStyle(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NAME, "Name", 32, ""));
    table.addField(new BooleanField(HASPLAINTEXTPASSWORD,
                                    "Has plain text password?",
                                    Boolean.FALSE));
    table.addField(new BooleanField(HASPASSWORDCLUE, "Has password clue?", Boolean.FALSE));
    table.addField(new BooleanField(MAYDELETE, "May delete?", Boolean.TRUE));
    table.addField(new BooleanField(ISMULTISESSION, "Is multi-session?", Boolean.FALSE));
  }
}
