package org.cord.node;

/**
 * Interface that describes a plugin that is capable of sanitising a proposed filename to fit into a
 * given naming convention for nodes. It is not expected to be a node specific process as this is
 * handled by the NodeFilenameTrigger, but instead should be purely a String processing routine to
 * generate a proposed version of the new filename.
 * 
 * @see org.cord.node.trigger.NodeFilenameValidator
 */
public interface FilenameGenerator
{
  /**
   * Generate the sanitised version of the proposedFilename according to the rules that this
   * FilenameGenerator implements. These classes are loaded dynamically so there should always be an
   * empty constructor available. This might be changed to a factory method in the future once I
   * think through the appropriate pattern...
   * 
   * @param proposedFilename
   *          The filename that is being suggested as a candidate for the generated node.
   * @return The proposed filename, or null if there is no possible valid filename that this
   *         FilenameGenerator can create from the proposedFilename.
   */
  public String generateFilename(String proposedFilename);
}
