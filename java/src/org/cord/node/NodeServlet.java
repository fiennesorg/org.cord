package org.cord.node;

import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.SettableValue;
import org.cord.node.config.NodeConfig;
import org.cord.util.CollectionUtil;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.Maps;
import org.cord.util.Named;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableFactory;
import org.cord.util.SimpleTimer;
import org.cord.util.StringUtils;
import org.webmacro.Context;
import org.webmacro.InitException;
import org.webmacro.Template;
import org.webmacro.WM;
import org.webmacro.WebMacro;
import org.webmacro.servlet.HandlerException;
import org.webmacro.servlet.WMServlet;
import org.webmacro.servlet.WebContext;

@SuppressWarnings("serial")
public abstract class NodeServlet
  extends WMServlet
  implements SettableFactory
{
  /**
   * The WebMacro Context key that will be used for an Iterable list of URLs that will be registered
   * in google analytics before the primary page is registered. This is controlled by the
   * {@link NodeManager#addGoogleAnalyticsPageUrl(NodeRequest, String)} method.
   */
  public static final String WM_GA_PAGEURLS = "ga_pageURLs";

  /**
   * The WebMacro Context key that will be used as the page view URL to track for the currently
   * rendered page. If not defined then the URL being invoked will be tracked.
   */
  public static final String WM_GA_PAGEURL = "ga_pageURL";

  /**
   * {@value}
   */
  public final static String INITPARAM_DBLOGIN = "dbLogin";

  /**
   * {@value}
   */
  public final static String INITPARAM_DBPASSWORD = "dbPassword";

  public final static String INITPARAM_DBHOST = "dbHost";

  /**
   * {@value}
   */
  public final static String INITPARAM_DBNAME = "dbName";

  public final static String INITPARAM_WEBROOTPATH = "webRootPath";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the Login functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_LOGIN = "nodeStyleIdLogin";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the 404 functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_FOUROHFOUR = "nodeStyleIdFourOhFour";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the error reporting functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_FEEDBACKERROR = "nodeStyleIdFeedbackError";

  /**
   * Initialisation parameter ({@value} ) that lets an optional domain be specified for all asset
   * paths in templates. If this variable is not defined then it will be replaced with "". If it is
   * to be defined then the protocol should also be included along with the domain, e.g.
   * "http://assets.acme.com". This will be included in all contexts under the name of $assetDomain.
   * Therefore all asset paths should be stated as being "$assetDomain/etc/etc".
   */
  public final static String INITPARAM_ASSETDOMAIN = "assetDomain";

  /**
   * staticsDir, staticsBuildDir and staticsBuildNodeDir used to all be the same directory: statics.
   * However, to accomodate versioning all static resources, and having built artefacts in a
   * different tree, they are now configuration parameters.
   */
  public final static String INITPARAM_STATICSPATH = "staticsPath";
  public final static String INITPARAM_STATICSPATH_DEFAULT = "statics";
  public final static String INITPARAM_STATICSBUILDPATH = "staticsBuildPath";
  public final static String INITPARAM_STATICSBUILDNODEPATH = "staticsBuildNodePath";

  /**
   * {@value}
   */
  public final static String INITPARAM_HTTPSERVLETPATH = "httpServletPath";

  /**
   * {@value}
   */
  public final static String INITPARAM_HOSTNAME = "hostName";

  public final static String DEFAULT_ROOT_TEMPLATEPATH = "node/root.wm.html";

  /**
   * The optional fully qualified classname for the {@link Authenticator} that will be utilised in
   * the NodeManager that backs this NodeServlet. The Authenticator will be initialised using
   * {@link Authenticators#getOptInstance(Gettable, String)}.
   */
  public final static String INITPARAM_AUTHENTICATOR_CLASSNAME = "Authenticator.className";

  private NodeManager _nodeManager;

  private String _defaultRootTemplatePath = DEFAULT_ROOT_TEMPLATEPATH;

  private final String __transactionPassword;

  private String _assetDomain;

  private NodeManagerFactory _nodeManagerFactory;

  private Set<String> _passthroughParameters = null;

  private final Map<String, Object> __persistentParameters = Maps.newConcurrentHashMap();

  // Interface to the ServletConfig/ServletContext initParameters
  private Gettable _initParameters = null;

  private Settable _mutableInitParameters = null;

  private NodeConfig _nodeConfig = null;

  /**
   * Perform some standard global initialisation processes for the NodeServlet engine. Namely:-
   * <ul>
   * <li>Generate of a random transaction password. This is effectively uncrackable and will be
   * rebuilt on the rebooting of the NodeServlet in question with every restart.
   * <li>Checking the working directory of the java runtime (held in system property "user.dir"),
   * and if it is not writable by the user that the JVM is running under, then setting it to
   * "/tmp/wwwrun". This is designed to try and help with situations when 3rd party applications try
   * and drop temporary files into the current working directory.
   * </ul>
   * 
   * @param nodeManagerFactory
   *          The factory that will be utilised to produce the NodeManagerFactory. If null then an
   *          instance of the NullNodeManagerFactory will be automatically substituted.
   * @see NullNodeManagerFactory
   */
  public NodeServlet(NodeManagerFactory nodeManagerFactory) throws ServletException
  {
    __transactionPassword = Db.createTransactionPassword();
    _nodeManagerFactory =
        (nodeManagerFactory == null ? NullNodeManagerFactory.getInstance() : nodeManagerFactory);
    File workingDirectory = new File(System.getProperty("user.dir"));
    if (!workingDirectory.canWrite()) {
      System.setProperty("user.dir", "/tmp/wwwrun");
    }
  }

  /**
   * Override initWebMacro() to create and inject our own configuration properties.
   */
  @Override
  public WebMacro initWebMacro()
      throws InitException
  {
    _nodeConfig =
        new NodeConfig(this, DebugConstants.DEBUG_RESOURCES ? DebugConstants.DEBUG_OUT : null);
    _initParameters = _nodeConfig.getGettable(NodeConfig.Namespace.NODE);
    return new WM(this, _nodeConfig.getProperties(NodeConfig.Namespace.WEBMACRO));
  }

  /**
   * Get a Gettable Object that provides a view onto the init parameters declared in this
   * NodeServlet.
   * 
   * @return The GettableBridge onto the servlet bootup parameters or a replacement implementation
   *         supplied by setInitParameters(source). Never null.
   * @see #setInitParameters(Gettable)
   */
  @Override
  public final Gettable getGettable()
  {
    return _initParameters;
  }

  /**
   * Get the mutable version of the init parameters if one has been registered. The default
   * GettableBridge onto the init parameters does not implement Settable and therefore is read only.
   * However, if the subclass has extended this with a Settable wrapper such as SettableValue then
   * this will be returned from this method.
   * 
   * @return The Settable view onto the init parameters, or null if there is no Settable view. If
   *         not null then this will be the same value as returned from getInitParameters(), just a
   *         different view.
   * @see #getGettable()
   * @see #setInitParameters(Gettable)
   * @see SettableValue
   */
  @Override
  public final Settable getSettable()
  {
    return _mutableInitParameters;
  }

  /**
   * Inform this servlet that it is to utilise an alternative Gettable or Settable source for the
   * initParameters than the default Servlet.getInitParameter(...) bootup properties access method.
   * This should be utilised with caution, and it is advised that some form of delegating property
   * supplier that includes this NodeServlet as one of its data sources should be utilised as a
   * replacement to avoid (very) unpredictable results.
   * 
   * @param source
   *          The Gettable source that is to be utilised in calls to getInitParameter(...) from now
   *          on. If this implements Settable as well then it will atuomatically be registered in
   *          getMutableInitParameters() as well as getInitParameters().
   * @see SettableValue
   * @see #getGettable()
   * @see #getSettable()
   */
  protected synchronized void setInitParameters(Gettable source)
  {
    _initParameters = source;
    _mutableInitParameters =
        (_initParameters instanceof Settable ? (Settable) _initParameters : null);
  }

  /**
   * Utility method to wrap an Exception in a ServletException and throw it. This is necessary
   * because the ServletException doesn't provide a creator for the JDK 1.4 style nested exception
   * model.
   * 
   * @param message
   *          The message that will be registered on the ServletException.
   * @param nestedException
   *          The Exception that is going to be registered as the cause on the ServletException.
   * @see Throwable#initCause(Throwable)
   */
  public static void throwNestedServletException(String message,
                                                 Exception nestedException)
      throws ServletException
  {
    ServletException servletEx = new ServletException(message);
    servletEx.initCause(nestedException);
    throw servletEx;
  }

  /**
   * Get the AssetDomain as defined in the boot-up parameters.
   * 
   * @return the fully-qualified asset domain, or "" if it has not been defined. Never null.
   * @see #INITPARAM_ASSETDOMAIN
   */
  public String getAssetDomain(HttpServletRequest httpServletRequest)
  {
    if (_assetDomain == null) {
      try {
        String assetDomain = getInitParameter(INITPARAM_ASSETDOMAIN, false);
        _assetDomain = (assetDomain == null ? "" : assetDomain);
      } catch (ServletException sEx) {
        throw new LogicException("assetDomain is non-compulsory", sEx);
      }
    }
    return _assetDomain;
  }

  protected final String getTransactionPassword()
  {
    return __transactionPassword;
  }

  protected void setDefaultRootTemplatePath(String path)
  {
    _defaultRootTemplatePath = path;
  }

  protected final NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  /**
   * @return The requested parameter or null if the parameter is not defined.
   */
  public String getInitParameter(String name,
                                 String defaultValue,
                                 boolean isCompulsory,
                                 boolean obfuscateLog)
      throws ServletException
  {
    return getInitParameter(name, defaultValue, isCompulsory, obfuscateLog, getGettable());
  }

  /**
   * Wrap the logging GettableUtils retrieval method with a ServletException for undefined
   * compulsory parameters.
   * 
   * @return The requested parameter or null if the parameter is not defined.
   * @throws ServletException
   *           If isCompulsory is true and either the ParameterSupplier is null or the value is not
   *           found in the ParameterSupplier. This therefore ensures that excecution is halted in
   *           the situations when there is not a defined value found for compulsory parameters.
   * @see GettableUtils#get(Gettable,String,Object,PrintStream,Named,boolean)
   */
  public static String getInitParameter(String name,
                                        String defaultValue,
                                        boolean isCompulsory,
                                        boolean obfuscateLog,
                                        Gettable parameterSupplier)
      throws ServletException
  {
    String value = StringUtils.toString(GettableUtils.get(parameterSupplier,
                                                          name,
                                                          defaultValue,
                                                          System.err,
                                                          null,
                                                          obfuscateLog));
    if (isCompulsory && value == null) {
      throw new ServletException("No value defined for " + name);
    }
    return value;
  }

  /**
   * @return The requested parameter or null if the parameter is not defined.
   */
  public String getInitParameter(String name,
                                 boolean isCompulsory)
      throws ServletException
  {
    return getInitParameter(name, null, isCompulsory, false);
  }

  public Object get(Object key)
  {
    if (key == null) {
      return null;
    }
    return getInitParameter(key.toString());
  }

  public final static String INITPARAM_PUBLICPARAMETERS = "publicParameters";

  /**
   * Protected method to return the NodeManagerFactory that was used to generate the NodeManager
   * that is running this NodeServlet. This may well be useful if the NodeManagerFactory
   * implementation contains access points to extended functionality and it is initialised in the
   * creator of the NodeServlet, thereby making it difficult to store the reference to it.
   * 
   * @return The NodeManagerFactory as passed to the creator of this NodeServlet.
   */
  protected final NodeManagerFactory getNodeManagerFactory()
  {
    return _nodeManagerFactory;
  }

  @Override
  protected void start()
      throws ServletException
  {
    System.err.println(this + ".start()");
    try {
      _nodeManager =
          _nodeManagerFactory.createNodeManager(getTransactionPassword(), _nodeConfig, true, this);
      if (_nodeManager == null) {
        throw new LogicException("NodeManager has not been initialised in " + this);
      }
      lockConnectionToThread();
      initialise(_nodeManager);
      initialiseContentCompilers(_nodeManager);
      addPersistentParameter("tabName", "");
      _nodeManager.lock();
      releaseConnectionFromThread();
    } catch (RuntimeException rEx) {
      rEx.printStackTrace();
      throwNestedServletException("Runtime exception during booting:" + rEx, rEx);
    } catch (MirrorException mtlEx) {
      throwNestedServletException("MirrorException during booting:" + mtlEx, mtlEx);
    }
  }

  private void initialiseContentCompilers(NodeManager nodeManager)
  {
    for (Iterator<ContentCompiler> it =
        nodeManager.getContentCompilerFactory().getContentCompilers(); it.hasNext();) {
      ContentCompiler cc = it.next();
      cc.initialiseServlet(this);
    }
  }

  @Override
  protected void stop()
  {
    System.err.println(this + ".stop()");
    if (_nodeManager != null) {
      _nodeManager.shutdown();
      _nodeManager = null;
      _nodeManagerFactory = null;
      _passthroughParameters = null;
      __persistentParameters.clear();
      _initParameters = null;
      _mutableInitParameters = null;
    }
    super.stop();
  }

  protected abstract void initialise(NodeManager nodeManager)
      throws ServletException, MirrorTableLockedException;

  /**
   * Basic Date format for access logging.
   * 
   * @see #logAccess
   */
  private static final SimpleDateFormat __logAccessFmt =
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss: ");

  /**
   * Log the current acccess; originally written to help with debugging callback responses from
   * ecommerce systems, like Secure Trading's Payment Pages and PayPal. This will only be invoked
   * automatically if {@link DebugConstants#DEBUG_ACCESSLOG_CONTEXT} is set to true.
   * 
   * @see #__logAccessFmt
   */
  protected void logAccess(WebContext context)
  {
    StringBuilder buf = new StringBuilder();
    HttpServletRequest req = context.getRequest();
    buf.append(__logAccessFmt.format(new Date()) + req.getContextPath() + ": " + req.getRemoteAddr()
               + ": " + req.getMethod() + " " + req.getPathInfo());
    if (DebugConstants.DEBUG_HTTP_PARAMS) {
      for (Enumeration<?> e = req.getParameterNames(); e.hasMoreElements();) {
        String param = (String) e.nextElement();
        buf.append(" [" + param + "=" + req.getParameter(param) + "]");
      }
    }
    DebugConstants.DEBUG_OUT.println(buf);
  }

  private static SimpleDateFormat LOGTIMESTAMP = new SimpleDateFormat("dd/MM HH:mm:ss.SSS");

  private Map<HttpServletRequest, Long> __request_nanos = Maps.newConcurrentHashMap();

  @Override
  public final Template handle(WebContext context)
      throws HandlerException
  {
    if (DebugConstants.DEBUG_ACCESSLOG_STARTEND) {
      __request_nanos.put(context.getRequest(), Long.valueOf(System.nanoTime()));
    }
    NodeRequest nodeRequest = null;
    
    prepopulate(context);


    if (DebugConstants.DEBUG_ACCESSLOG_CONTEXT) {
      logAccess(context);
    }
    try {
      nodeRequestHook_PreConstruction(context);
      try {
        nodeRequest = new NodeRequest(_nodeManager, this, context);
      } catch (RedirectionException rEx) {
        return rEx.issueRedirect(context, this);
      }
      passthroughParameters(nodeRequest, context);
      processPersistentParameters(nodeRequest, context);
      String rootTemplatePath = nodeRequest.getRootTemplatePath();
      if (rootTemplatePath == null || rootTemplatePath.length() == 0) {
        rootTemplatePath = _defaultRootTemplatePath;
      }
      processGoogleAnalyticsPageUrls(nodeRequest, context);
      customiseContext(_nodeManager, context, nodeRequest);
      Template template = getTemplate(rootTemplatePath);
      // System.err.println(template.getParameters());
      // template.evaluateAsString(context);
      // System.err.println(template.getParameters());
      return template;
    } catch (BypassWebmacroException bypassEx) {
      return null;
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new HandlerException("Unexpected Error", ex);
    }
  }
  
  protected void prepopulate(WebContext context)
  {
    SimpleTimer timer = new SimpleTimer();
    context.put("timer", timer);
    String path = context.getRequest().getPathInfo();
    context.put("path", path);
    context.put("httpServletPath", _nodeManager.getHttpServletPath());
    context.put("db", _nodeManager.getDb());
    context.put("nodeManager", _nodeManager);
    context.put("nodeConfig", _initParameters);
    context.put("contextReference", new ContextReference(context));
    context.put(INITPARAM_ASSETDOMAIN, getAssetDomain(context.getRequest()));    
    // staticsDir and staticsBuildDir default to "statics", since this is
    // where everything used to live: node.properties sets the correct
    // locations in newer projects.
    String staticsDir =
        GettableUtils.get(this, INITPARAM_STATICSPATH, INITPARAM_STATICSPATH_DEFAULT);
    context.put(INITPARAM_STATICSPATH, staticsDir);
    context.put(INITPARAM_STATICSBUILDPATH,
                GettableUtils.get(this, INITPARAM_STATICSBUILDPATH, staticsDir));
    context.put(INITPARAM_STATICSBUILDNODEPATH,
                GettableUtils.get(this, INITPARAM_STATICSBUILDNODEPATH, staticsDir));
    context.put("StringUtils", StringUtils.getInstance());
    context.put("CollectionUtils", CollectionUtil.getInstance());
    context.put("GettableUtils", GettableUtils.getInstance());
  }

  private void processGoogleAnalyticsPageUrls(NodeRequest nodeRequest,
                                              WebContext context)
  {
    HttpSession session = nodeRequest.getSession();
    Object ga_pageURLs = session.getAttribute(WM_GA_PAGEURLS);
    if (ga_pageURLs != null) {
      DebugConstants.variable("ga_pageURLs", ga_pageURLs);
      context.put(WM_GA_PAGEURLS, ga_pageURLs);
      session.removeAttribute(WM_GA_PAGEURLS);
    }
  }

  public static final String WEBMACRO_NODEREQUEST = "nodeRequest";

  @Override
  public void destroyContext(WebContext webContext)
      throws HandlerException
  {
    Object o = webContext.get(WEBMACRO_NODEREQUEST);
    if (o != null) {
      ((NodeRequest) o).shutdown();
    }
    if (DebugConstants.DEBUG_ACCESSLOG_STARTEND) {
      Long nanos = __request_nanos.remove(webContext.getRequest());
      if (nanos != null) {
        DebugConstants.DEBUG_OUT.println(String.format("Rendered  : %.3f ms",
                                                       Double.valueOf(NumberUtil.nanosToMs(System.nanoTime()
                                                                                           - nanos.longValue()))));
      }
      DebugConstants.DEBUG_OUT.println();
    }
    super.destroyContext(webContext);
  }

  private void passthroughParameters(NodeRequest nodeRequest,
                                     WebContext context)
  {
    if (_passthroughParameters != null) {
      synchronized (_passthroughParameters) {
        Iterator<?> names = _passthroughParameters.iterator();
        while (names.hasNext()) {
          String name = (String) names.next();
          String nodeRequestValue = nodeRequest.getString(name);
          if (nodeRequestValue != null) {
            context.put(name, nodeRequestValue);
          }
        }
      }
    }
  }

  /**
   * Add a named parameter that is to be dropped back into the Context every time a new NodeRequest
   * is generated. The information will be automatically added to the Context every time that there
   * is an incoming NodeRequest that supplies the named variable. If the variable is not defined
   * then there is no value added to the Context. Please note that it is still up to the template
   * author to ensure that the information is provided back to the template engine as a hidden form
   * field otherwise the data will be dropped.
   */
  public final synchronized void addPassthroughParameter(String name)
  {
    if (name != null) {
      if (_passthroughParameters == null) {
        _passthroughParameters = new HashSet<String>();
      }
      synchronized (_passthroughParameters) {
        _passthroughParameters.add(name);
      }
    }
  }

  /**
   * Add a named parameter that will be dropped into the context of any incoming request for the
   * duration of the NodeServlet. This will occur regardless of whether or not the parameter is
   * defined in the incoming request (unlike the passthrough parameters which are just dropped back
   * in if they are sent from the input form). If the persistent parameter is defined in the
   * incoming NodeRequest then its value (if different from the defaultValue) will be stored in the
   * Session associated with the NodeRequest and will be permanently available to the client on
   * successive requests. There is not currently any functionality to prevent the client from
   * overriding this value. Please note that if this is invoked on a running system then it does not
   * currently replace any currently defined values of the name if it is a redefinition. This will
   * be implemented once the default behaviour has been defined, but for now it is intended that you
   * create the defaults at the initialiasation of the servlet.
   * 
   * @param name
   *          The name of the cgi field that this parameter represents
   * @param defaultValue
   *          the value that will be mapped onto name unless overridden at a later date. Note that
   *          although this can be any type, if it is not a String, then the stored type will be
   *          modified when it is overruled by an incoming NodeRequest parameter (which is always of
   *          type String).
   */
  public final synchronized Object addPersistentParameter(String name,
                                                          Object defaultValue)
  {
    if (name != null) {
      return __persistentParameters.put(name, defaultValue);
    }
    return null;
  }

  protected synchronized Object removePersistentParameter(NodeRequest nodeRequest,
                                                          String name)
  {
    if (name != null) {
      Object legacyValue = __persistentParameters.remove(name);
      if (legacyValue != null && nodeRequest != null) {
        nodeRequest.getSession().removeAttribute(name);
      }
      return legacyValue;
    }
    return null;
  }

  private void processPersistentParameters(NodeRequest nodeRequest,
                                           WebContext context)
  {
    Iterator<String> names = __persistentParameters.keySet().iterator();
    while (names.hasNext()) {
      String name = names.next();
      String nodeRequestValue = nodeRequest.getStringIgnoringSession(name);
      if (nodeRequestValue != null) {
        nodeRequest.getSession().setAttribute(name, nodeRequestValue);
        context.put(name, nodeRequestValue);
      } else if (context.containsKey(name)) {
        nodeRequest.getSession().setAttribute(name, context.get(name));
      } else {
        Object persistentValue = nodeRequest.getSession().getAttribute(name);
        if (persistentValue == null) {
          persistentValue = __persistentParameters.get(name);
          if (persistentValue != null) {
            nodeRequest.getSession().setAttribute(name, persistentValue);
          }
        }
        if (persistentValue != null) {
          context.put(name, persistentValue);
        }
      }
    }
  }

  /**
   * Define the value that is stored in the session for a persistent parameter.
   * 
   * @param nodeRequest
   *          The compulsory incoming request that wraps the session that the value will be stored
   *          inside.
   * @param name
   *          The compulsory name of the value that is to be stored. If the name isn't declared as a
   *          persistent parameter then an {@link IllegalArgumentException} will be raised.
   * @param value
   *          the optional value. A null will cause the persistent value to be removed from the
   *          session.
   */
  public void setPersistentParameterValue(NodeRequest nodeRequest,
                                          String name,
                                          Object value)
  {
    if (!__persistentParameters.containsKey(name)) {
      throw new IllegalArgumentException(name + " is not a PersistentParameter");
    }
    nodeRequest.getSession().setAttribute(name, value);
  }

  /**
   * Drop any custom Objects into the WebContext. This will be called as the last method in the
   * handle() operation before the template is resolved.
   * 
   * @param context
   *          The already populated WebContext that is to be resolved.
   * @param nodeRequest
   *          The resolved NodeRequest corresponding to the incoming request.
   */
  protected abstract void customiseContext(NodeManager nodeManager,
                                           WebContext context,
                                           NodeRequest nodeRequest);

  /**
   * Perform any actions that are necessary to prepare for the construction of the NodeRequest for
   * an incoming request. This method will be invoked immediately before the NodeRequest is invoked.
   * The default implementation is empty and can be overridden with alternative functionality as
   * required.
   * 
   * @throws HandlerException
   */
  protected void nodeRequestHook_PreConstruction(WebContext context)
      throws HandlerException
  {

  }

  protected void nodeRequestHook_PostAuthentication(Context context,
                                                    HttpSession session,
                                                    PersistentRecord user)
      throws NodeRequestException
  {

  }

  @Override
  protected void preDoRequest(HttpServletRequest req,
                              HttpServletResponse resp)
  {
    lockConnectionToThread();
  }

  protected void lockConnectionToThread()
  {
    getNodeManager().getDb().lockConnectionToThread();
  }

  @Override
  protected void postDoRequest(HttpServletRequest req,
                               HttpServletResponse resp)
  {
    releaseConnectionFromThread();
  }

  protected void releaseConnectionFromThread()
  {
    getNodeManager().getDb().releaseConnectionFromThread();
  }

}
