package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;
import org.cord.util.OptionalInt;

import com.google.common.base.Preconditions;

/**
 * LockFilter that uses a supplied ACL as the deciding factor as to whether or not someone is
 * allowed to steal a lock from another user.
 */
public class AclLockFilter
  extends NamedImpl
  implements ContestedLockFilter
{
  // public final static String NAME = "aclLockFilter";
  public final static int MODE_AND = 1;

  public final static int MODE_OR = 2;

  private final NodeManager __nodeMgr;

  private final OptionalInt __contestingAclId;

  private final OptionalInt __lockedAclId;

  private final int __mode;

  public AclLockFilter(NodeManager nodeMgr,
                       String name,
                       OptionalInt contestingAclId,
                       OptionalInt lockedAclId,
                       int mode)
  {
    super(name);
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
    __contestingAclId = Preconditions.checkNotNull(contestingAclId, "contestingAclId");
    __lockedAclId = Preconditions.checkNotNull(lockedAclId, "lockedAclId");
    __mode = mode;
  }

  private boolean checkAcl(OptionalInt aclId,
                           int userId)
  {
    return aclId.isPresent() ? __nodeMgr.getAcl().acceptsAclUser(aclId.get(), userId) : true;
  }

  @Override
  public int resolveContestedLock(NodeRequest contestedRequest,
                                  PersistentRecord contestedRecord,
                                  PersistentRecord lockedUser,
                                  Transaction lockedTransaction)
  {
    boolean contestingResult = checkAcl(__contestingAclId, contestedRequest.getUserId());
    boolean lockedResult = checkAcl(__lockedAclId, lockedUser.getId());
    switch (__mode) {
      case MODE_AND:
        return contestingResult && lockedResult ? STEAL_LOCK : UNDECIDED;
      case MODE_OR:
        return contestingResult || lockedResult ? STEAL_LOCK : UNDECIDED;
      default:
        throw new LogicException("Bad mode (" + __mode + ") in " + this);
    }
  }
}
