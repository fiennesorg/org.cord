package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;

/**
 * Implementation of NodeQuestion that only supports a single, auto-submitting answer.
 */
public abstract class MonoNodeQuestion
  extends NodeQuestion
{
  public MonoNodeQuestion(String name,
                          String englishName,
                          String title,
                          PersistentRecord answerNode,
                          NodeFilter eligibleNodeFilter,
                          Transaction transaction)
  {
    super(name,
          englishName,
          title,
          answerNode,
          1,
          eligibleNodeFilter,
          transaction);
  }

  @Override
  public final boolean maySubmit()
  {
    return getSelectedNodeIds().size() == 1;
  }

  @Override
  public final boolean shouldSubmit()
  {
    return maySubmit();
  }

}
