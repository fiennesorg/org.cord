package org.cord.node;

import org.cord.util.Gettable;
import org.cord.util.GettableFactory;
import org.cord.util.GettableUtils;
import org.cord.util.StringUtils;

/**
 * Implementation of ValueFactory that extracts variables from a Gettable or GettableFactory source
 * using the delegating name convention.
 * 
 * @see GettableUtils#delegatingGet(Gettable,Object,Object,Object)
 */
public class DelegatingGettableValueFactory
  implements ValueFactory
{
  private final GettableFactory __gettableFactory;

  private final Gettable __gettable;

  private final Object __baseName;

  private final Object __instanceName;

  private final Object __variableName;

  private final String __defaultValue;

  public DelegatingGettableValueFactory(GettableFactory gettableFactory,
                                        Object baseName,
                                        Object instanceName,
                                        Object variableName,
                                        String defaultValue)
  {
    __gettableFactory = gettableFactory;
    __gettable = null;
    __baseName = baseName;
    __instanceName = instanceName;
    __variableName = variableName;
    __defaultValue = defaultValue;
  }

  public DelegatingGettableValueFactory(Gettable gettable,
                                        Object baseName,
                                        Object instanceName,
                                        Object variableName,
                                        String defaultValue)
  {
    __gettableFactory = null;
    __gettable = gettable;
    __baseName = baseName;
    __instanceName = instanceName;
    __variableName = variableName;
    __defaultValue = defaultValue;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    Gettable gettable = (__gettableFactory == null ? __gettable : __gettableFactory.getGettable());
    return StringUtils.toString(GettableUtils.delegatingGet(gettable,
                                                            __baseName,
                                                            __instanceName,
                                                            __variableName,
                                                            __defaultValue));
  }
}
