package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.recordsource.RecordSources;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public class NodeFilterByNodeStyles
  extends NodeFilter
{
  private final IntSet __nodeStyleIds;

  /**
   * @param nodeStyleIds
   *          A collection of Integer ids of valid NodeStyles. This will be cloned into an internal
   *          HashSet inside the object so future changes in state will not be reflected in this
   *          class.
   */
  public NodeFilterByNodeStyles(IntCollection nodeStyleIds)
  {
    super();
    __nodeStyleIds = new IntOpenHashSet(nodeStyleIds);
  }

  @Override
  protected boolean acceptsNode(PersistentRecord node)
  {
    return __nodeStyleIds.contains(node.compInt(Node.NODESTYLE_ID));
  }

  public static NodeFilterByNodeStyles getInstanceFromNodeStyles(RecordSource nodeStyles)
  {
    RecordSources.assertIsTableNamed(nodeStyles, NodeStyle.TABLENAME);
    return new NodeFilterByNodeStyles(nodeStyles.getIdList());
  }
}
