package org.cord.node;

import java.util.Iterator;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractDependencies;
import org.cord.util.ObjectUtil;

import com.google.common.collect.ImmutableSet;

// FIXME: (#483) This code needs re-implmeneting or the records in
// NodeGroupStyle removing

/**
 * NodeLocker is responsible for spreading any locks acquired on a Node record into the associated
 * NodeGroups. This takes into account if the NODEGROUPSTYLE_ISUPDATEDLINKED or
 * NODEGROUPSTYLE_ISDELETELINKED flags are set on the associated NodeGroupStyle records. This class
 * is registered on the NODE table by the NodeDb class.
 * 
 * @see NodeGroupStyle#ISUPDATELINKED
 * @see NodeGroupStyle#ISDELETELINKED
 * @see NodeDb
 */
public class NodeLocker
  extends AbstractDependencies
{
  /**
   * "NodeLocker"
   */
  public final static RecordOperationKey<Iterator<PersistentRecord>> NAME =
      RecordOperationKey.create("NodeLocker",
                                ObjectUtil.<Iterator<PersistentRecord>> castClass(Iterator.class));

  public NodeLocker()
  {
    super(NAME);
  }

  /**
   * Resolve the dependent records. The rules used are as follows:-
   * <ul>
   * <li>If we are dealing with a TRANSACTION_UPDATE process:-
   * <ul>
   * <li>Grab all the NodeGroups that are children of the targetted Node.
   * <li>Resolve the NodeGroupStyle for each NodeGroup
   * <li>If the NODEGROUPSTYLE_ISUPDATELINKED flag is set then add the NodeGroup to the list of
   * dependent records.
   * <li>Perform the same action for the parent NodeGroup of the Node.
   * </ul>
   * <li>If we are dealing with a TRANSACTION_DELETE process:-
   * <ul>
   * <li>Grab the parent NodeGroup of the Node.
   * <li>Resolve to its NodeGroupStyle
   * <li>If the NODEGROUPSTYLE_ISDELETELINKED flag is set then put the NodeGroup in the list of
   * dependent records.
   * <li>Please note that it is not necessary to process the children NodeGroup records as they are
   * taken care of by the LINK_ENFORCED status of their relationship with the node.
   * </ul>
   * </ul>
   * 
   * @param node
   *          The Node which is about to be locked.
   * @param transactionType
   *          The format of the transactional lock being acquired.
   * @param viewpoint
   *          The transactional viewpoint onto the operation. This is largely irrelevant because all
   *          the referenced fields are immutable, but is in there for completeness.
   * @return Iterator of PersistentRecord objects taken from the NodeGroup table who have a direct
   *         relationship with node and which should be locked along with node.
   */
  @Override
  protected final Iterator<PersistentRecord> internalGetDependentRecords(PersistentRecord node,
                                                                         int transactionType,
                                                                         Viewpoint viewpoint)
  {
    // NodeGroup
    // Set dependentRecords = new HashSet();
    // switch (transactionType) {
    // case Transaction.TRANSACTION_UPDATE:
    // Iterator nodeGroups = node.followReferences(Node.CHILDRENNODEGROUPS,
    // transaction);
    // while (nodeGroups.hasNext()) {
    // processUpdate((PersistentRecord) nodeGroups.next(),
    // dependentRecords);
    // }
    // processUpdate(node.followEnforcedLink(Node.PARENTNODEGROUP),
    // dependentRecords);
    // break;
    // case Transaction.TRANSACTION_DELETE:
    // processUpdate(node.followEnforcedLink(Node.PARENTNODEGROUP),
    // dependentRecords);
    // break;
    // default:
    // return EmptyIterator.getInstance();
    // }
    // return dependentRecords.iterator();
    return ImmutableSet.<PersistentRecord> of().iterator();
  }

  // private final void processUpdate(PersistentRecord nodeGroup,
  // Set dependentRecords)
  // {
  // PersistentRecord nodeGroupStyle =
  // nodeGroup.followEnforcedLink(NodeGroup.NODEGROUPSTYLE);
  // if (nodeGroupStyle.getBooleanField(NodeGroupStyle.ISUPDATELINKED)) {
  // dependentRecords.add(nodeGroup);
  // }
  // }
  // private final void processDelete(PersistentRecord nodeGroup,
  // Set dependentRecords)
  // throws MirrorTransactionTimeoutException
  // {
  // PersistentRecord nodeGroupStyle
  // = nodeGroup.followEnforcedLink(NodeGroup.NODEGROUPSTYLE);
  // if (nodeGroupStyle.getBooleanField(NodeGroupStyle.ISDELETELINKED)) {
  // dependentRecords.add(nodeGroup);
  // }
  // }
  /**
   * @return true On the grounds that the only way to resolve whether or not this Node does have
   *         dependencies is to try and extract the relevant information. If we do this then we have
   *         to do it twice (there is no short cut) and therefore it is better to always return true
   *         and let internalGetDependentRecords(...) return an empty Iterator if it turns out that
   *         there is not any information to be returned.
   */
  @Override
  public final boolean mayHaveDependencies(PersistentRecord record,
                                           int transactionType)
  {
    return true;
  }
}
