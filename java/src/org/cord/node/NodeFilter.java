package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordFilter;
import org.cord.mirror.Table;

/**
 * Node-specific extension of RecordFilter for Objects that filter lists of Nodes. Any
 * PersistentRecord that arrives at the acceptsNode method is guaranteed to be from the Node table.
 * 
 * @author alex
 */
public abstract class NodeFilter
  implements RecordFilter
{
  /**
   * Implementation of NodeFilter that accepts all Nodes.
   */
  public static final NodeFilter ALLNODES = new NodeFilter() {
    @Override
    public boolean acceptsNode(PersistentRecord node)
    {
      return true;
    }
  };

  protected abstract boolean acceptsNode(PersistentRecord node);

  /**
   * @return false if the record is not from Node, otherwise the result of invoking
   *         acceptsNode(node)
   * @see #acceptsNode(PersistentRecord)
   */
  @Override
  public final boolean accepts(PersistentRecord record)
  {
    if (!Node.TABLENAME.equals(record.getTable().getName())) {
      return false;
    }
    return acceptsNode(record);
  }

  /**
   * @return false if recordId is not found in table, otherwise the results of invoking
   *         accepts(record
   * @see #accepts(PersistentRecord)
   */
  @Override
  public final boolean accepts(Table table,
                               int recordId)
  {
    try {
      return accepts(table.getRecord(recordId));
    } catch (MirrorNoSuchRecordException e) {
      // do nothing and hence return false...
    }
    return false;
  }
}
