package org.cord.node;

import java.util.Comparator;
import java.util.Iterator;

import org.cord.mirror.TransientRecord;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.SortedIterator;

/**
 * Singleton class that provides a Comparator which sorts a list of Node records according to their
 * path component.
 */
public final class NodePathComparator
  implements Comparator<TransientRecord>, SingletonShutdownable
{
  private static NodePathComparator _instance = new NodePathComparator();

  public static NodePathComparator getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  public static <T extends TransientRecord> Iterator<T> sortByPath(Iterator<T> nodes)
  {
    return new SortedIterator<T>(nodes, getInstance());
  }

  private NodePathComparator()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  public int compare(TransientRecord n1,
                     TransientRecord n2)
  {
    return n1.opt(Node.PATH).compareTo(n2.opt(Node.PATH));
  }
}
