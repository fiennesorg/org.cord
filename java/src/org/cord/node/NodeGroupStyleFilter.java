package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordFilter;
import org.cord.mirror.Table;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * RecordFilter that takes NodeGroup or NodeGroupStyle records and compares them against a list of
 * approved NodeGroupStyle id values.
 */
public class NodeGroupStyleFilter
  implements RecordFilter
{
  private IntSet _validIds = new IntOpenHashSet();

  public NodeGroupStyleFilter()
  {
  }

  public void addValidNodeGroupStyleId(int id)
  {
    _validIds.add(id);
  }

  @Override
  public boolean accepts(Table table,
                         int recordId)
  {
    try {
      return accepts(table.getRecord(recordId));
    } catch (MirrorNoSuchRecordException mnsrEx) {
      return false;
    }
  }

  @Override
  public boolean accepts(PersistentRecord record)
  {
    if (record == null) {
      return false;
    }
    String tableName = record.getTable().getName();
    if (NodeGroupStyle.TABLENAME.equals(tableName)) {
      return _validIds.contains(record.getId());
    }
    return false;
  }
}