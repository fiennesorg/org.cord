package org.cord.node.morph;

import java.util.List;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.NodeManager;
import org.cord.node.RegionStyle;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.Gettable;

import com.google.common.collect.ImmutableList;

public class RegionStyleContentCompiler
  extends RecordContentCompiler
{
  public final static String VIEWTEMPLATEPATH = "node/morph/regionStyle.view.wm.html";

  public final static String EDITTEMPLATEPATH = "node/morph/regionStyle.edit.wm.html";

  /**
   **/
  public final static List<FieldKey<?>> STRINGS = ImmutableList.<FieldKey<?>> builder()
                                                               .add(RegionStyle.NAME,
                                                                    RegionStyle.ALIASNAME,
                                                                    RegionStyle.TITLE,
                                                                    RegionStyle.COMPILERSTYLENAME)
                                                               .build();

  public RegionStyleContentCompiler(NodeManager nodeManager)
  {
    super(nodeManager,
          RegionStyle.TABLENAME,
          VIEWTEMPLATEPATH,
          EDITTEMPLATEPATH);
  }

  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                WritableRecord targetRegionStyle)
      throws MirrorFieldException
  {
    TableContentCompiler.uploadStringFields(params,
                                            targetRegionStyle,
                                            regionStyle,
                                            STRINGS.iterator(),
                                            true);
    return targetRegionStyle.getIsUpdated();
  }

  @Override
  public void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
  }
}
