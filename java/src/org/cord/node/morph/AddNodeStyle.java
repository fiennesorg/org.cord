package org.cord.node.morph;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.node.Acl;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeStyle;
import org.cord.node.PostViewAction;
import org.cord.node.User;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;

public class AddNodeStyle
  extends DynamicAclAuthenticatedFactory
{
  public final static String ENGLISHNAME = "Add Region Style";

  public final static String NAME = "AddNodeStyle";

  private final Table __nodeStyleTable;

  public AddNodeStyle(NodeManager nodeManager,
                      PostViewAction postViewSuccessOperation,
                      Integer aclId)
  {
    super(NAME,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          "You do not have permission to add NodeStyles.");
    __nodeStyleTable = nodeManager.getDb().getTable(NodeStyle.TABLENAME);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    TransientRecord nodeStyle = __nodeStyleTable.createTransientRecord();
    try {
      String name = (String) nodeStyle.setField(NodeStyle.NAME, nodeRequest.get(NodeStyle.NAME));
      nodeStyle.setField(NodeStyle.FILENAME,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.FILENAME), name));
      nodeStyle.setField(NodeStyle.ISEDITABLEFILENAME,
                         MoreObjects.firstNonNull(nodeRequest.getBoolean(NodeStyle.ISEDITABLEFILENAME),
                                                  Boolean.TRUE));
      nodeStyle.setField(NodeStyle.OWNER_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.OWNER_ID),
                                                  Integer.valueOf(User.ID_ROOT)));
      nodeStyle.setField(NodeStyle.VIEWACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.VIEWACL_ID),
                                                  Integer.valueOf(Acl.PUBLIC)));
      nodeStyle.setField(NodeStyle.UPDATEVIEWACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.UPDATEVIEWACL_ID),
                                                  Integer.valueOf(Acl.ROOTONLY)));
      nodeStyle.setField(NodeStyle.EDITACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.EDITACL_ID),
                                                  Integer.valueOf(Acl.COREADMINONLY)));
      nodeStyle.setField(NodeStyle.UPDATEEDITACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.UPDATEEDITACL_ID),
                                                  Integer.valueOf(Acl.ROOTONLY)));
      nodeStyle.setField(NodeStyle.PUBLISHACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.PUBLISHACL_ID),
                                                  Integer.valueOf(Acl.COREADMINONLY)));
      nodeStyle.setField(NodeStyle.UPDATEPUBLISHACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.UPDATEPUBLISHACL_ID),
                                                  Integer.valueOf(Acl.ROOTONLY)));
      nodeStyle.setField(NodeStyle.DELETEACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.DELETEACL_ID),
                                                  Integer.valueOf(Acl.COREADMINONLY)));
      nodeStyle.setField(NodeStyle.UPDATEDELETEACL_ID,
                         MoreObjects.firstNonNull(nodeRequest.get(NodeStyle.UPDATEDELETEACL_ID),
                                                  Integer.valueOf(Acl.ROOTONLY)));
      nodeStyle.setField(NodeStyle.CSSPATH, nodeRequest.get(NodeStyle.CSSPATH));
      nodeStyle.setField(NodeStyle.ROOTTEMPLATEPATH, nodeRequest.get(NodeStyle.ROOTTEMPLATEPATH));
      nodeStyle.setField(NodeStyle.VIEWTEMPLATEPATH, nodeRequest.get(NodeStyle.VIEWTEMPLATEPATH));
      nodeStyle.setField(NodeStyle.EDITTEMPLATEPATH, nodeRequest.get(NodeStyle.EDITTEMPLATEPATH));
      nodeStyle.setField(NodeStyle.ISRENDERRESET,
                         MoreObjects.firstNonNull(nodeRequest.getBoolean(NodeStyle.ISRENDERRESET),
                                                  Boolean.FALSE));
      nodeStyle.setField(NodeStyle.DEFAULTISPUBLISHED,
                         MoreObjects.firstNonNull(nodeRequest.getBoolean(NodeStyle.DEFAULTISPUBLISHED),
                                                  Boolean.FALSE));
      nodeStyle.setField(NodeStyle.MAYEDITPUBLISHED,
                         MoreObjects.firstNonNull(nodeRequest.getBoolean(NodeStyle.MAYEDITPUBLISHED),
                                                  Boolean.TRUE));
      nodeStyle.setField(NodeStyle.ISMENUITEM,
                         MoreObjects.firstNonNull(nodeRequest.getBoolean(NodeStyle.ISMENUITEM),
                                                  Boolean.TRUE));
      nodeStyle.makePersistent(null);
    } catch (MirrorException bootUpEx) {
      throw new FeedbackErrorException(ENGLISHNAME,
                                       node,
                                       bootUpEx,
                                       true,
                                       true,
                                       "Error Creating Region Style: %s",
                                       bootUpEx);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
