package org.cord.node.morph;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorRuntimeException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Cluster of classes that handle the creation and resolution of Nodes that act as the "Homepage"
 * for individual Records. The Homepage is a Node that tracks the status of the Record during it's
 * lifespan and can therefore have the ability to act as a point of entry for editing the item. The
 * following items of functionality are provided:-
 * <ul>
 * <li>The ability to auto create the Node that is to be linked to the Record on the creation of the
 * PersistentRecord. The filename will be set to the id of the new Record. It is recommended that
 * this is immutable to avoid breaking the link between the Node and the target Record.
 * </ul>
 */
public class RecordHomepage
{
  public final static RecordOperationKey<PersistentRecord> HOMEPAGE =
      RecordOperationKey.create("homepage", PersistentRecord.class);

  public final static RecordOperationKey<Boolean> HASHOMEPAGE =
      RecordOperationKey.create("hasHomepage", Boolean.class);

  private NodeManager _nodeManager;

  private final String __transactionPassword;

  private final int __homepageParentNodeId;

  private final int __homepageNodeGroupStyleId;

  private Table _targetTable;

  private final IntSet __nonHomepageRecordIds = new IntOpenHashSet();

  public RecordHomepage(NodeManager nodeManager,
                        String transactionPassword,
                        int homepageParentNodeId,
                        int homepageNodeGroupStyleId,
                        boolean homepageAutoCreates,
                        Table targetTable)
      throws MirrorTableLockedException
  {
    _nodeManager = Preconditions.checkNotNull(nodeManager, "nodeManager");
    __transactionPassword = Preconditions.checkNotNull(transactionPassword, "transactionPassword");
    __homepageParentNodeId = homepageParentNodeId;
    __homepageNodeGroupStyleId = homepageNodeGroupStyleId;
    _targetTable = Preconditions.checkNotNull(targetTable, "targetTable");
    targetTable.addPostInstantiationTrigger(new CreateRecordTrigger());
    targetTable.addRecordOperation(new HasHomepage());
    targetTable.addRecordOperation(new Homepage(homepageAutoCreates));
    __nonHomepageRecordIds.add(TransientRecord.ID_TRANSIENT);
  }

  /**
   * Inform the system that the given Record is exempt from the Homepage creation system. If the
   * given Record already has a homepage then it will be automatically removed.
   * 
   * @throws MirrorTransactionTimeoutException
   *           If there was an unresolved contested lock during the deletion of an existing homepage
   *           (unlikely).
   */
  public void addNonHomepageRecord(TransientRecord record)
      throws MirrorTransactionTimeoutException
  {
    if (record != null && !__nonHomepageRecordIds.contains(record.getId())) {
      deleteHomepage(record);
      __nonHomepageRecordIds.add(record.getId());
    }
  }

  /**
   * Sweep across all Records and initialise any Homepages that have not currently been initialised.
   */
  public void initialiseMissingHomepages()
      throws MirrorInstantiationException, MirrorFieldException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    for (PersistentRecord record : _targetTable) {
      createHomepage(record);
    }
  }

  /**
   * Get the Query that resolves to all the Nodes under the specified homepageNodeGroupId which have
   * their filename set to Node. This should be a Query with either zero or one matching records
   * because $node.filename should normally be unique.
   */
  public RecordSource getHomepageNodes(TransientRecord record)
  {
    return _nodeManager.getNode()
                       .getTable()
                       .getQuery(null,
                                 Node.PARENTNODE_ID + "=" + __homepageParentNodeId + " and "
                                       + Node.FILENAME + "=" + record.getId(),
                                 null);
  }

  /**
   * Create the appropriate Homepage Node for the record. This will only be performed if record is a
   * PersistentRecord and there is not already a homepage existing for the record.
   * 
   * @throws MirrorInstantiationException
   *           If the newly created Homepage fails during the creation process.
   * @throws MirrorFieldException
   *           If the parentNodeGroup_id, filename or owner_id field are rejected during
   *           initialisation of the Homepage.
   */
  public PersistentRecord createHomepage(TransientRecord triggerRecord)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    if (__nonHomepageRecordIds.contains(triggerRecord.getId())) {
      // no homepage permitted for this triggerRecord...
      return null;
    }
    PersistentRecord homepage =
        RecordSources.getOptFirstRecord(getHomepageNodes(triggerRecord), null);
    if (homepage != null) {
      return homepage;
    }
    return _nodeManager.getNode().createNode(__homepageParentNodeId,
                                             __homepageNodeGroupStyleId,
                                             Integer.toString(triggerRecord.getId()),
                                             null,
                                             null,
                                             true,
                                             null);
  }

  private void deleteHomepage(TransientRecord record)
      throws MirrorTransactionTimeoutException
  {
    RecordSource homepageNodes = getHomepageNodes(record);
    if (homepageNodes.getIdList().size() > 0) {
      try (Transaction transaction =
          _nodeManager.getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                 Transaction.TRANSACTION_DELETE,
                                                 __transactionPassword,
                                                 "RecordHomepage rollback")) {
        transaction.delete(homepageNodes, Db.DEFAULT_TIMEOUT);
        transaction.commit();
      }
    }
  }

  /**
   * RecordOperation on ($record.hasHomepage) that returns true if the Record has an initialised
   * homepage.
   */
  public class HasHomepage
    extends SimpleRecordOperation<Boolean>
  {
    /**
     * @see RecordHomepage#HASHOMEPAGE
     */
    public HasHomepage()
    {
      super(HASHOMEPAGE);
    }

    @Override
    public Boolean getOperation(TransientRecord record,
                                Viewpoint viewpoint)
    {
      return Boolean.valueOf(getHomepageNodes(record).getIdList(viewpoint).size() > 0);
    }
  }

  /**
   * RecordOperation on Record ($record.homepage) that returns the PersistentRecord Node that acts
   * as the Records homepage, optionally creating the homepage if necessary.
   */
  public class Homepage
    extends SimpleRecordOperation<PersistentRecord>
  {
    private final boolean __autoCreateHomepages;

    /**
     * @param autoCreateHomepages
     *          if true the invoking $record.homepage on a Record that does not yet have a homepage
     *          created will try and initialise the homepage automatically. if false then invoking
     *          $record.homepage on a non-initiliased Record will result in null and no homepage
     *          being created.
     * @see RecordHomepage#HOMEPAGE
     */
    public Homepage(boolean autoCreateHomepages)
    {
      super(HOMEPAGE);
      __autoCreateHomepages = autoCreateHomepages;
    }

    /**
     * @return The Node PersistentRecord that corresponds to the invoking record's homepage, or null
     *         if the homepage does not exist.
     * @throws MirrorRuntimeException
     *           If there is an error when auto creating the Homepage.
     */
    @Override
    public PersistentRecord getOperation(TransientRecord record,
                                         Viewpoint viewpoint)
    {
      PersistentRecord homepage =
          RecordSources.getOptFirstRecord(getHomepageNodes(record), viewpoint);
      if (homepage != null) {
        return homepage;
      }
      if (__autoCreateHomepages) {
        try {
          return createHomepage(record);
        } catch (MirrorException initialisationFailureEx) {
          throw new MirrorRuntimeException("Unable to autoCreateHomepage", initialisationFailureEx);
        }
      }
      return null;
    }
  }

  /**
   * PostInstantiationTrigger registered on Record that automatically creates a Homepage for the
   * Record once the record has been created. Please note that a corresponding delete linkage is not
   * required because the declaration of $node.owner_id is declared as LINK_ENFORCED therefore
   * attempting to delete the Record will automatically delete the Homepages as well.
   */
  public class CreateRecordTrigger
    implements PostInstantiationTrigger
  {
    @Override
    public void shutdown()
    {
      _nodeManager = null;
      _targetTable = null;
    }

    /**
     * Do nothing as there is no temporary storage of assets associated with record.
     */
    @Override
    public void releasePost(PersistentRecord record,
                            Gettable params)
    {
    }

    /**
     * Delete any of the homepage Nodes that where associated with record.
     */
    @Override
    public void rollbackPost(PersistentRecord record,
                             Gettable params)
        throws MirrorTransactionTimeoutException
    {
      deleteHomepage(record);
    }

    /**
     * Create the homepage for the record as required.
     * 
     * @see RecordHomepage#createHomepage(TransientRecord)
     */
    @Override
    public void triggerPost(PersistentRecord record,
                            Gettable params)
        throws MirrorInstantiationException, MirrorFieldException,
        MirrorTransactionTimeoutException, MirrorNoSuchRecordException
    {
      createHomepage(record);
    }
  }

  public class DeleteRecordTrigger
    implements StateTrigger
  {
    /**
     * @see #STATE_PERSISTENT_PREDELETED
     */
    @Override
    public int getDefaultState()
    {
      return STATE_PERSISTENT_PREDELETED;
    }

    @Override
    public void shutdown()
    {
    }

    /**
     * @return null, because this will be registered on whichever Table the RecordHomepage is
     *         targetted at.
     */
    @Override
    public String getTargetTableName()
    {
      return null;
    }

    /**
     * @return false
     */
    @Override
    public boolean isOverridableState()
    {
      return false;
    }

    @Override
    public void trigger(PersistentRecord record,
                        Transaction transaction,
                        int state)
        throws MirrorTransactionTimeoutException
    {
      transaction.delete(getHomepageNodes(record), Db.DEFAULT_TIMEOUT);
    }
  }
}
