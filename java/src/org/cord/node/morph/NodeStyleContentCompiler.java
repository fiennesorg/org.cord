package org.cord.node.morph;

import java.util.List;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.Gettable;

import com.google.common.collect.ImmutableList;

public class NodeStyleContentCompiler
  extends RecordContentCompiler
{
  public final static String VIEWTEMPLATEPATH = "node/morph/nodeStyle.view.wm.html";

  public final static String EDITTEMPLATEPATH = "node/morph/nodeStyle.edit.wm.html";

  /**
   **/
  public final static List<FieldKey<?>> STRINGS = ImmutableList.<FieldKey<?>> builder()
                                                               .add(NodeStyle.NAME,
                                                                    NodeStyle.OWNER_ID,
                                                                    NodeStyle.VIEWACL_ID,
                                                                    NodeStyle.EDITACL_ID,
                                                                    NodeStyle.PUBLISHACL_ID,
                                                                    NodeStyle.DELETEACL_ID)
                                                               .build();

  /**
   **/
  public final static List<FieldKey<Boolean>> BOOLEANS =
      ImmutableList.<FieldKey<Boolean>> builder().add(NodeStyle.ISEDITABLEFILENAME).build();

  public NodeStyleContentCompiler(NodeManager nodeManager)
  {
    super(nodeManager,
          NodeStyle.TABLENAME,
          VIEWTEMPLATEPATH,
          EDITTEMPLATEPATH);
  }

  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                WritableRecord targetNodeStyle)
      throws MirrorFieldException
  {
    TableContentCompiler.uploadStringFields(params,
                                            targetNodeStyle,
                                            node,
                                            STRINGS.iterator(),
                                            true);
    for (FieldKey<Boolean> name : BOOLEANS) {
      targetNodeStyle.setFieldIfDefined(name, params.getBoolean(name));
    }
    // uploadBooleanFields(params, targetNodeStyle, node, BOOLEANS.iterator());
    return targetNodeStyle.getIsUpdated();
  }

  @Override
  public void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
  }
}
