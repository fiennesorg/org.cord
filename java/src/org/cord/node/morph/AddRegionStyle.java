package org.cord.node.morph;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.RegionStyle;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.NumberUtil;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;

public class AddRegionStyle
  extends DynamicAclAuthenticatedFactory
{
  public final static String ENGLISHNAME = "Add Region Style";

  public final static String NAME = "AddRegionStyle";

  private final Table __regionStyleTable;

  public AddRegionStyle(NodeManager nodeManager,
                        PostViewAction postViewSuccessOperation,
                        Integer aclId)
  {
    super(NAME,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          "You do not have permission to add RegionStyles.");
    __regionStyleTable = nodeManager.getDb().getTable(RegionStyle.TABLENAME);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    TransientRecord regionStyle = __regionStyleTable.createTransientRecord();
    try {
      regionStyle.setField(RegionStyle.NODESTYLE_ID,
                           MoreObjects.firstNonNull(nodeRequest.get(RegionStyle.NODESTYLE_ID),
                                                    NumberUtil.INTEGER_ZERO));
      regionStyle.setField(RegionStyle.NUMBER, nodeRequest.get(RegionStyle.NUMBER));
      String name =
          (String) regionStyle.setField(RegionStyle.NAME,
                                        MoreObjects.firstNonNull(nodeRequest.get(RegionStyle.NAME),
                                                                 RegionStyle.TABLENAME + "#" + System.currentTimeMillis()));
      regionStyle.setField(RegionStyle.ALIASNAME, nodeRequest.get(RegionStyle.ALIASNAME));
      regionStyle.setField(RegionStyle.TITLE,
                           MoreObjects.firstNonNull(nodeRequest.get(RegionStyle.TITLE), name));
      regionStyle.setField(RegionStyle.COMPILERNAME, nodeRequest.get(RegionStyle.COMPILERNAME));
      regionStyle.setField(RegionStyle.COMPILERSTYLENAME,
                           nodeRequest.get(RegionStyle.COMPILERSTYLENAME));
      // regionStyle.setField(RegionStyle.ISFULL,
      // nodeRequest.getBooleanParameter(RegionStyle.ISFULL,
      // null,
      // true));
      // regionStyle.setField(RegionStyle.ISSUMMARY,
      // nodeRequest.getBooleanParameter(RegionStyle.ISSUMMARY,
      // null,
      // false));
      // regionStyle.setField(RegionStyle.MINIMUM,
      // nodeRequest.getParameter(RegionStyle.MINIMUM,
      // "1"));
      // regionStyle.setField(RegionStyle.MAXIMUM,
      // nodeRequest.getParameter(RegionStyle.MAXIMUM,
      // "1"));
      regionStyle.makePersistent(null);
    } catch (MirrorException bootUpEx) {
      throw new FeedbackErrorException(ENGLISHNAME,
                                       node,
                                       bootUpEx,
                                       true,
                                       true,
                                       "Error Creating Region Style: %s",
                                       bootUpEx);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}
