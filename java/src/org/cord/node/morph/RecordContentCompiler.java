package org.cord.node.morph;

import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.operation.ConstantRecordOperation;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;

import com.google.common.collect.Iterators;

/**
 * Implementation of ContentCompiler that targets a single instance of an editable Record in the
 * Morph structure. This record will generally be one of the following (although currently not
 * limited to):-
 * <ul>
 * <li>NodeGroupStyle
 * <li>NodeStyle
 * <li>RegionStyle
 * </ul>
 * It is assumed that the target Record will already have been initialised and have caused the
 * creation of the targetting Region (via RecordHomepage), rather than vice versa thereby requiring
 * no implementation of initialiseInstance(region).
 * 
 * @see RecordHomepage
 */
public abstract class RecordContentCompiler
  extends ContentCompiler
{
  // private final Db __db;
  private final Table __targetTable;

  public static final RecordOperationKey<String> VIEWTEMPLATEPATH =
      RecordOperationKey.create("viewTemplatePath", String.class);

  private final String __viewTemplatePath;

  public static final RecordOperationKey<String> EDITTEMPLATEPATH =
      RecordOperationKey.create("editTemplatePath", String.class);

  private final String __editTemplatePath;

  public RecordContentCompiler(NodeManager nodeManager,
                               String targetTableName,
                               String viewTemplatePath,
                               String editTemplatePath)
  {
    super(nodeManager,
          targetTableName);
    // __db = nodeManager.getDb();
    __targetTable = getDb().getTable(targetTableName);
    __viewTemplatePath = viewTemplatePath;
    __editTemplatePath = editTemplatePath;
  }

  /**
   * Return the appropriate RecordDbInitialiser instance.
   * 
   * @return New instance of the appropriate RecordDbInitialiser
   * @see RecordDbInitialiser
   */
  @Override
  public DbInitialisers getDbInitialisers()
  {
    return new DbInitialisers() {
      @Override
      public Iterator<DbInitialiser> getDbInitialisers()
      {
        return Iterators.<DbInitialiser> singletonIterator(new RecordDbInitialiser(getName()));
      }
    };
  }

  /**
   * Ultra simple DbInitialiser that registers the value of viewTemplatePath and editTemplatePath on
   * the target Table.
   */
  public class RecordDbInitialiser
    extends NamedImpl
    implements DbInitialiser
  {
    public RecordDbInitialiser(String tableName)
    {
      super("org.cord.node.morph.RecordContentCompiler." + tableName);
    }

    @Override
    public void shutdown()
    {
    }

    /**
     * Register the viewTemplatePath and editTemplatePath onto the targetTable as
     * ConstantRecordOperations.
     * 
     * @see ConstantRecordOperation
     */
    @Override
    public void init(Db db,
                     String transactionPassword)
        throws MirrorTableLockedException
    {
      __targetTable.addRecordOperation(ConstantRecordOperation.create(VIEWTEMPLATEPATH,
                                                                      __viewTemplatePath));
      __targetTable.addRecordOperation(ConstantRecordOperation.create(EDITTEMPLATEPATH,
                                                                      __editTemplatePath));
    }
  }

  /**
   * This implementation just adds the target Record to the deleteTransaction. This therefore means
   * that it is not necessary to make an explicit trigger that links the appropriate controlling
   * Node to the target Table. The method is final, so if additional functionality is required, then
   * it should be registered as a StateTrigger on the targetTable.
   * 
   * @see Transaction#delete(PersistentRecord,long)
   */
  @Override
  public final void destroyInstance(PersistentRecord node,
                                    PersistentRecord regionStyle,
                                    Transaction deleteTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    deleteTransaction.delete(getRecord(node, deleteTransaction), Db.DEFAULT_TIMEOUT);
  }

  /**
   * This implementation just adds the target Record to the editTransaction. This therefore means
   * that it is not necessary to make an explicit trigger that links the appropriate controlling
   * Node to the target Table. The method is final, so if additional functionality is required, then
   * it should be registered as a StateTrigger on the targetTable.
   * 
   * @see Transaction#edit(PersistentRecord,long)
   */
  @Override
  public final void editInstance(PersistentRecord node,
                                 PersistentRecord regionStyle,
                                 Transaction editTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    editTransaction.edit(getRecord(node, editTransaction), Db.DEFAULT_TIMEOUT);
  }

  public final PersistentRecord getRecord(PersistentRecord node,
                                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return __targetTable.getRecord(Integer.valueOf(node.opt(Node.FILENAME)), viewpoint);
  }

  public final PersistentRecord getOptRecord(PersistentRecord node,
                                             Viewpoint viewpoint)
  {
    return __targetTable.getOptRecord(Integer.valueOf(node.opt(Node.FILENAME)), viewpoint);
  }

  /**
   * Get the instance of the the record pointed at by the controlling Node from the targetTable.
   * 
   * @return The target PersistentRecord.
   */
  @Override
  public Gettable compile(PersistentRecord node,
                          PersistentRecord regionStyle,
                          NodeRequest nodeRequest,
                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return getRecord(node, viewpoint);
  }

  @Override
  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    return getOptRecord(node, viewpoint);
  }

  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                Map<Object, Object> outputs,
                                PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException
  {
    try {
      return updateInstance(node,
                            regionStyle,
                            editTransaction,
                            params,
                            (WritableRecord) getRecord(node, editTransaction));
    } catch (ClassCastException ccEx) {
      throw new LogicException("Target record appears not to be locked by editTransaction", ccEx);
    }
  }

  public abstract boolean updateInstance(PersistentRecord node,
                                         PersistentRecord regionStyle,
                                         Transaction editTransaction,
                                         Gettable params,
                                         WritableRecord targetRecord)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException;

  /**
   * Do nothing because the target record is supposed to have initialised the Node (and therefore
   * the Region and ContentCompiler) rather than vice versa.
   */
  @Override
  public void initialiseInstance(PersistentRecord node,
                                 PersistentRecord regionStyle,
                                 Gettable initParams,
                                 Map<Object, Object> outputs,
                                 Transaction transaction)
  {
  }
}
