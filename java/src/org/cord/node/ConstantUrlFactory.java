package org.cord.node;

/**
 * URL factory that returns the address of a constant node identified by id.
 * 
 * @author alex
 */
public class ConstantUrlFactory
  extends UrlFactory
{
  public static final ConstantUrlFactory ROOT = new ConstantUrlFactory(Node.ROOTNODE_ID);

  private final int __nodeId;

  public ConstantUrlFactory(int nodeId)
  {
    super();
    __nodeId = nodeId;
  }

  @Override
  protected int getNodeId(NodeManager nodeManager,
                          NodeRequest nodeRequest)
  {
    return __nodeId;
  }
}
