package org.cord.node;

import org.cord.util.Gettable;
import org.cord.util.GettableFactory;
import org.cord.util.StringUtils;

public class GettableValueFactory
  implements ValueFactory
{
  private GettableFactory _gettableFactory;

  private Gettable _gettable;

  private String _key;

  public GettableValueFactory(GettableFactory gettableFactory,
                              String key)
  {
    _gettableFactory = gettableFactory;
    _gettable = null;
    _key = key;
  }

  @Override
  public void shutdown()
  {
    _gettableFactory = null;
    _gettable = null;
    _key = null;
  }

  public GettableValueFactory(Gettable gettable,
                              String key)
  {
    _gettableFactory = null;
    _gettable = gettable;
    _key = key;
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    Gettable gettable = _gettableFactory == null ? _gettable : _gettableFactory.getGettable();
    return StringUtils.toString(gettable.get(_key));
  }
}
