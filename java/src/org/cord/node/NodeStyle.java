package org.cord.node;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.CachingSimpleRecordOperation;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorRuntimeException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableListener;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.ConstantRecordOperation;
import org.cord.mirror.operation.IdSpecificRecordOperation;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.NodeGroupStyle.CreateNodePolicy;
import org.cord.node.operation.PublishedNodes;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.cord.util.PrivateCacheKey;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntMaps;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntMaps;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

public class NodeStyle
  extends NodeTableWrapper
{
  /**
   * "NodeStyle"
   */
  public final static String TABLENAME = "NodeStyle";

  /**
   * @see NodeDbConstants#NAME
   */
  public final static ObjectFieldKey<String> NAME = NodeDbConstants.NAME.copy();

  /**
   * "named"
   */
  public final static MonoRecordSourceOperationKey<String, RecordSource> NAMED =
      NodeDbConstants.NAMED.copy();

  /**
   * @see NodeDbConstants#FILENAME
   */
  public final static ObjectFieldKey<String> FILENAME = NodeDbConstants.FILENAME.copy();

  /**
   * "isEditableFilename"
   */
  public final static ObjectFieldKey<Boolean> ISEDITABLEFILENAME =
      BooleanField.createKey("isEditableFilename");

  /**
   * "owner_id"
   */
  public final static IntFieldKey OWNER_ID = IntFieldKey.create("owner_id");

  public final static int OWNER_ID_INHERITED = -1;

  public final static int OWNER_ID_CUSTOM = 0;

  /**
   * "owner"
   */
  public final static RecordOperationKey<PersistentRecord> OWNER =
      RecordOperationKey.create("owner", PersistentRecord.class);

  /**
   * "viewAcl_id"
   * 
   * @see Acl#VIEWNODESTYLES
   */
  public final static IntFieldKey VIEWACL_ID = IntFieldKey.create("viewAcl_id");

  /**
   * "viewAcl"
   */
  public final static RecordOperationKey<PersistentRecord> VIEWACL =
      RecordOperationKey.create("viewAcl", PersistentRecord.class);

  /**
   * @see Acl#UPDATEVIEWNODESTYLES
   */
  public final static IntFieldKey UPDATEVIEWACL_ID = IntFieldKey.create("updateViewAcl_id");

  /**
   * "updateViewAcl"
   */
  public final static RecordOperationKey<PersistentRecord> UPDATEVIEWACL =
      RecordOperationKey.create("updateViewAcl", PersistentRecord.class);

  /**
   * @see Acl#EDITNODESTYLES
   */
  public final static IntFieldKey EDITACL_ID = IntFieldKey.create("editAcl_id");

  /**
   * "editAcl"
   */
  public final static RecordOperationKey<PersistentRecord> EDITACL =
      RecordOperationKey.create("editAcl", PersistentRecord.class);

  /**
   * @see Acl#UPDATEEDITNODESTYLES
   */
  public final static IntFieldKey UPDATEEDITACL_ID = IntFieldKey.create("updateEditAcl_id");

  /**
   * "updateEditAcl"
   */
  public final static RecordOperationKey<PersistentRecord> UPDATEEDITACL =
      RecordOperationKey.create("updateEditAcl", PersistentRecord.class);

  /**
   * "publishAcl_id"
   */
  public final static IntFieldKey PUBLISHACL_ID = IntFieldKey.create("publishAcl_id");

  /**
   * "publishAcl"
   */
  public final static RecordOperationKey<PersistentRecord> PUBLISHACL =
      RecordOperationKey.create("publishAcl", PersistentRecord.class);

  /**
   * "updatePublishAcl_id"
   */
  public final static IntFieldKey UPDATEPUBLISHACL_ID = IntFieldKey.create("updatePublishAcl_id");

  /**
   * "updatePublishAcl"
   */
  public final static RecordOperationKey<PersistentRecord> UPDATEPUBLISHACL =
      RecordOperationKey.create("updatePublishAcl", PersistentRecord.class);

  /**
   * "deleteAcl_id"
   * 
   * @see Acl#DELETENODESTYLES
   */
  public final static IntFieldKey DELETEACL_ID = IntFieldKey.create("deleteAcl_id");

  /**
   * "deleteAcl"
   */
  public final static RecordOperationKey<PersistentRecord> DELETEACL =
      RecordOperationKey.create("deleteAcl", PersistentRecord.class);

  /**
   * @see Acl#UPDATEDELETENODESTYLES
   */
  public final static IntFieldKey UPDATEDELETEACL_ID = IntFieldKey.create("updateDeleteAcl_id");

  /**
   * "updateDeleteAcl"
   */
  public final static RecordOperationKey<PersistentRecord> UPDATEDELETEACL =
      RecordOperationKey.create("updateDeleteAcl", PersistentRecord.class);

  /**
   * "cssPath"
   */
  public final static ObjectFieldKey<String> CSSPATH = StringField.createKey("cssPath");

  /**
   * "rootTemplatePath"
   */
  public final static ObjectFieldKey<String> ROOTTEMPLATEPATH =
      StringField.createKey("rootTemplatePath");

  /**
   * "viewTemplatePath"
   */
  public final static ObjectFieldKey<String> VIEWTEMPLATEPATH =
      StringField.createKey("viewTemplatePath");

  /**
   * "editTemplatePath"
   */
  public final static ObjectFieldKey<String> EDITTEMPLATEPATH =
      StringField.createKey("editTemplatePath");

  /**
   * "isRenderReset"
   */
  public final static ObjectFieldKey<Boolean> ISRENDERRESET =
      BooleanField.createKey("isRenderReset");

  /**
   * "defaultIsPublished"
   */
  public final static ObjectFieldKey<Boolean> DEFAULTISPUBLISHED =
      BooleanField.createKey("defaultIsPublished");

  /**
   * "mayEditPublished"
   */
  public final static ObjectFieldKey<Boolean> MAYEDITPUBLISHED =
      BooleanField.createKey("mayEditPublished");

  /**
   * "isMenuItem"
   */
  public final static ObjectFieldKey<Boolean> ISMENUITEM = BooleanField.createKey("isMenuItem");

  /**
   * "childrenNodeGroupStyles"
   */
  public final static RecordOperationKey<RecordSource> CHILDRENNODEGROUPSTYLES =
      RecordOperationKey.create("childrenNodeGroupStyles", RecordSource.class);

  /**
   * The RecordOperation that gives you the RecordSource of all NodeGroupStyle records that have
   * this NodeStyle as a child ("parentNodeGroupStyles").
   */
  public final static RecordOperationKey<RecordSource> PARENTNODEGROUPSTYLES =
      RecordOperationKey.create("parentNodeGroupStyles", RecordSource.class);

  /**
   * RecordOperation named "nodes" on NodeStyle records that returns a QueryWrapper around Node
   * records that implement this NodeStyle ordered by NODE_HTMLTITLE.
   * 
   * @see Node#NODESTYLE
   */
  public final static RecordOperationKey<RecordSource> NODES =
      RecordOperationKey.create("nodes", RecordSource.class);

  /**
   * RecordOperation ("publishedNodes") on NodeStyle records that returns a QueryWrapper of Node
   * records that have $node.isPublished set to true.
   * 
   * @see org.cord.node.operation.PublishedNodes
   */
  public final static RecordOperationKey<RecordSource> PUBLISHEDNODES =
      RecordOperationKey.create("publishedNodes", RecordSource.class);

  /**
   * RecordOperation named "regionStyles" on NodeStyle Table that returns QueryWrapper around
   * RegionStyle records that point to this NodeStyle.
   */
  public final static RecordOperationKey<RecordSource> REGIONSTYLES =
      RecordOperationKey.create("regionStyles", RecordSource.class);

  public final static RecordOperationKey<Object2IntMap<String>> REGIONSTYLESMAP =
      RecordOperationKey.create("regionStylesMap",
                                ObjectUtil.<Object2IntMap<String>> castClass(Object2IntMap.class));

  public final static RecordOperationKey<String> NAVTEMPLATEPATH =
      RecordOperationKey.create("navTemplatePath", String.class);

  public final static String NAVTEMPLATEPATH_DEFAULT =
      "evaluate:<a " + "class=\"generation$navNode.generation nodeStyle$navNode.nodeStyle_id navlink\" "
                                                       + "href=\"$navNode.url\">$navNode.htmlTitle</a>";

  public final static RecordOperationKey<String> NAVCURRENTTEMPLATEPATH =
      RecordOperationKey.create("navCurrentTemplatePath", String.class);

  public final static String NAVCURRENTTEMPLATEPATH_DEFAULT =
      "evaluate:<strong><em><a " + "class=\"generation$navNode.generation nodeStyle$navNode.nodeStyle_id navcurrent\""
                                                              + ">$navNode.htmlTitle</a></em></strong>";

  public final static RecordOperationKey<String> NAVPARENTTEMPLATEPATH =
      RecordOperationKey.create("navParentTemplatePath", String.class);

  public final static String NAVPARENTTEMPLATEPATH_DEFAULT =
      "evaluate:<strong><a " + "class=\"generation$navNode.generation nodeStyle$navNode.nodeStyle_id navparent\" "
                                                             + "href=\"$navNode.url\">$navNode.htmlTitle</a></strong>";

  public final static RecordOperationKey<String> LISTINGTEMPLATEPATH =
      RecordOperationKey.create("listingTemplatePath", String.class);

  public final static String LISTINGTEMPLATEPATH_DEFAULT = "evaluate:$listingNode.href.title";

  public final static int ROOTNODESTYLE = 1;

  public final static RecordOperationKey<Set<String>> CONTENTCOMPILERS =
      RecordOperationKey.create("contentCompilers", ObjectUtil.<Set<String>> castClass(Set.class));

  public final static MonoRecordOperationKey<String, PersistentRecord> REGIONSTYLE =
      MonoRecordOperationKey.create("regionStyle", String.class, PersistentRecord.class);

  public final static MonoRecordOperationKey<String, Boolean> HASREGIONSTYLE =
      MonoRecordOperationKey.create("hasRegionStyle", String.class, Boolean.class);

  private IdSpecificRecordOperation<String> _navTemplatePaths;

  private IdSpecificRecordOperation<String> _navCurrentTemplatePaths;

  private IdSpecificRecordOperation<String> _navParentTemplatePaths;

  private IdSpecificRecordOperation<String> _listingTemplatePaths;

  protected NodeStyle(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NAME, "Name", 32, ""));
    table.addField(new StringField(FILENAME, "Default filename(s)", 255, ""));
    table.addField(new BooleanField(ISEDITABLEFILENAME, "Are filenames editable?", Boolean.FALSE));
    table.addField(new LinkField(OWNER_ID,
                                 "Default owner",
                                 null,
                                 OWNER,
                                 User.TABLENAME,
                                 User.OWNEDNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.BIT_S_ON_T_DELETE));
    table.addField(new LinkField(VIEWACL_ID,
                                 "View rights",
                                 null,
                                 VIEWACL,
                                 Acl.TABLENAME,
                                 Acl.VIEWNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(UPDATEVIEWACL_ID,
                                 "Change view rights",
                                 null,
                                 UPDATEVIEWACL,
                                 Acl.TABLENAME,
                                 Acl.UPDATEVIEWNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(EDITACL_ID,
                                 "Edit rights",
                                 null,
                                 EDITACL,
                                 Acl.TABLENAME,
                                 Acl.EDITNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(UPDATEEDITACL_ID,
                                 "Change edit rights",
                                 null,
                                 UPDATEEDITACL,
                                 Acl.TABLENAME,
                                 Acl.UPDATEEDITNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(PUBLISHACL_ID,
                                 "Publish rights",
                                 null,
                                 PUBLISHACL,
                                 Acl.TABLENAME,
                                 Acl.PUBLISHNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(UPDATEPUBLISHACL_ID,
                                 "Change publish rights",
                                 null,
                                 UPDATEPUBLISHACL,
                                 Acl.TABLENAME,
                                 Acl.UPDATEPUBLISHNODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(DELETEACL_ID,
                                 "Delete rights",
                                 null,
                                 DELETEACL,
                                 Acl.TABLENAME,
                                 Acl.DELETENODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(UPDATEDELETEACL_ID,
                                 "Change delete rights",
                                 null,
                                 UPDATEDELETEACL,
                                 Acl.TABLENAME,
                                 Acl.UPDATEDELETENODESTYLES,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.setTitleOperationName(NAME);
    table.addField(new StringField(CSSPATH, "CSS web path"));
    table.addField(new StringField(ROOTTEMPLATEPATH, "Webmacro root template path"));
    table.addField(new StringField(VIEWTEMPLATEPATH, "Webmacro view template path"));
    table.addField(new StringField(EDITTEMPLATEPATH, "Webmacro edit template path"));
    table.addField(new BooleanField(ISRENDERRESET,
                                    "Should reset rendering stream?",
                                    Boolean.FALSE));
    table.addField(new BooleanField(DEFAULTISPUBLISHED,
                                    "Should new pages be published?",
                                    Boolean.FALSE));
    table.addField(new BooleanField(MAYEDITPUBLISHED,
                                    "Are published pages editable?",
                                    Boolean.TRUE));
    table.addField(new BooleanField(ISMENUITEM,
                                    "Should pages appear in navigation menus?",
                                    Boolean.TRUE));
    table.addFieldTrigger(new NodeStyleOwnerTrigger());
    table.addRecordOperation(new PublishedNodes(db));
    table.addRecordSourceOperation(new TableStringMatcher(NAMED,
                                                          NAME,
                                                          TableStringMatcher.SEARCH_EXACT,
                                                          null));
    // _nodeStyle.add(new RegionStyleResolver(_nodeManager));
    _navTemplatePaths =
        new IdSpecificRecordOperation<String>(NAVTEMPLATEPATH,
                                              ConstantRecordOperation.create(NAVTEMPLATEPATH,
                                                                             NAVTEMPLATEPATH_DEFAULT));
    table.addRecordOperation(_navTemplatePaths);
    _navCurrentTemplatePaths =
        new IdSpecificRecordOperation<String>(NAVCURRENTTEMPLATEPATH,
                                              ConstantRecordOperation.create(NAVCURRENTTEMPLATEPATH,
                                                                             NAVCURRENTTEMPLATEPATH_DEFAULT));
    table.addRecordOperation(_navCurrentTemplatePaths);
    _navParentTemplatePaths =
        new IdSpecificRecordOperation<String>(NAVPARENTTEMPLATEPATH,
                                              ConstantRecordOperation.create(NAVPARENTTEMPLATEPATH,
                                                                             NAVPARENTTEMPLATEPATH_DEFAULT));
    table.addRecordOperation(_navParentTemplatePaths);
    _listingTemplatePaths =
        new IdSpecificRecordOperation<String>(LISTINGTEMPLATEPATH,
                                              ConstantRecordOperation.create(LISTINGTEMPLATEPATH,
                                                                             LISTINGTEMPLATEPATH_DEFAULT));
    table.addRecordOperation(_listingTemplatePaths);
    // TODO: define "regionStyle" constant
    table.addRecordOperation(new MonoRecordOperation<String, PersistentRecord>(REGIONSTYLE,
                                                                               Casters.TOSTRING) {
      @Override
      public PersistentRecord calculate(TransientRecord nodeStyle,
                                        Viewpoint viewpoint,
                                        String regionStyleName)
      {
        try {
          return getNodeManager().getRegionStyle()
                                 .getInstance(nodeStyle.getId(),
                                              StringUtils.toString(regionStyleName),
                                              viewpoint);
        } catch (MirrorNoSuchRecordException mnsrEx) {
          throw new LogicException(nodeStyle + ".regionStyle." + regionStyleName
                                   + " does not resolve",
                                   mnsrEx);
        }
      }
    });
    table.addRecordOperation(new MonoRecordOperation<String, Boolean>(HASREGIONSTYLE,
                                                                      Casters.TOSTRING) {
      @Override
      public Boolean calculate(TransientRecord nodeStyle,
                               Viewpoint viewpoint,
                               String regionStyleName)
      {
        return Boolean.valueOf(nodeStyle.comp(NodeStyle.REGIONSTYLESMAP)
                                        .containsKey(regionStyleName));
      }
    });
    // _nodeStyle.addFieldTrigger(new ImmutableFieldTrigger());
    table.addRecordOperation(new SimpleRecordOperation<Set<String>>(CONTENTCOMPILERS) {
      @Override
      public Set<String> getOperation(TransientRecord nodeStyle,
                                      Viewpoint viewpoint)
      {
        return getContentCompilerNames(nodeStyle);
      }
    });
    RegionStylesMap regionStylesMap = new RegionStylesMap();
    table.addRecordOperation(regionStylesMap);
  }

  public class RegionStylesMap
    extends CachingSimpleRecordOperation<Object2IntMap<String>>
    implements TableListener, PostInstantiationTrigger
  {
    public static final String DATA_VERSION = "data_version";

    private int _data_version = 1;

    private RegionStylesMap()
    {
      super(REGIONSTYLESMAP);
    }

    @Override
    protected Object2IntMap<String> getOperation(TransientRecord nodeStyle,
                                                 Viewpoint viewpoint,
                                                 Map<Object, Object> cache)
    {
      if (viewpoint == null) {
        @SuppressWarnings("unchecked")
        Object2IntMap<String> map = (Object2IntMap<String>) cache.get(getKey());
        if (map != null && _data_version == map.getInt(DATA_VERSION)) {
          return map;
        }
      }
      Object2IntMap<String> map = new Object2IntOpenHashMap<String>();
      for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES, viewpoint)) {
        map.put(regionStyle.opt(RegionStyle.NAME), regionStyle.getId());
      }
      map.put(DATA_VERSION, _data_version);
      map = Object2IntMaps.unmodifiable(map);
      cache.put(getKey(), map);
      return map;
    }

    @Override
    protected void lockStatusChanged()
    {
      switch (getLockStatus()) {
        case LO_PRELOCKED: {
          Table regionStylesTable = NodeStyle.this.getNodeManager().getRegionStyle().getTable();
          regionStylesTable.addTableListener(this);
          try {
            regionStylesTable.addPostInstantiationTrigger(this);
          } catch (MirrorTableLockedException e) {
            throw new MirrorRuntimeException("failed to register RegionStylesMap on RegionStyle as PostInstantiationTrigger",
                                             e);
          }          
        }
      }
    }

    @Override
    public boolean tableChanged(Table table,
                                int recordId,
                                PersistentRecord persistentRecord,
                                int type)
    {
      _data_version++;
      return true;
    }

    @Override
    public void triggerPost(PersistentRecord record,
                            Gettable params)
        throws MirrorException, FeedbackErrorException
    {
      _data_version++;
    }

    @Override
    public void rollbackPost(PersistentRecord record,
                             Gettable params)
        throws MirrorException
    {
    }

    @Override
    public void releasePost(PersistentRecord record,
                            Gettable params)
    {
    }

  }

  private final PrivateCacheKey CACHEKEY_CONTENTCOMPILERS =
      new PrivateCacheKey("NodeStyle.contentCompilers");

  public Set<String> getContentCompilerNames(TransientRecord nodeStyle)
  {
    @SuppressWarnings("unchecked")
    Set<String> names = (Set<String>) nodeStyle.getCachedValue(CACHEKEY_CONTENTCOMPILERS); // unchecked
    if (names == null) {
      names = new HashSet<String>();
      for (PersistentRecord regionStyle : nodeStyle.opt(REGIONSTYLES)) {
        ContentCompiler compiler =
            getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
        names.add(compiler.getName());
      }
      nodeStyle.setCachedValue(CACHEKEY_CONTENTCOMPILERS, names);
    }
    return names;
  }

  public IdSpecificRecordOperation<String> getNavTemplatePaths()
  {
    return _navTemplatePaths;
  }

  public IdSpecificRecordOperation<String> getNavCurrentTemplatePaths()
  {
    return _navCurrentTemplatePaths;
  }

  public IdSpecificRecordOperation<String> getNavParentTemplatePaths()
  {
    return _navParentTemplatePaths;
  }

  public void setNavTemplatePaths(int nodeStyleId,
                                  String navTemplatePath,
                                  String navCurrentTemplatePath,
                                  String navParentTemplatePath)
  {
    getNavTemplatePaths().addRecordOperation(nodeStyleId,
                                             ConstantRecordOperation.create(NAVTEMPLATEPATH,
                                                                            navTemplatePath));
    getNavCurrentTemplatePaths().addRecordOperation(nodeStyleId,
                                                    ConstantRecordOperation.create(NAVCURRENTTEMPLATEPATH,
                                                                                   navCurrentTemplatePath));
    getNavParentTemplatePaths().addRecordOperation(nodeStyleId,
                                                   ConstantRecordOperation.create(NAVPARENTTEMPLATEPATH,
                                                                                  navParentTemplatePath));
  }

  public IdSpecificRecordOperation<String> getListingTemplatePaths()
  {
    return _listingTemplatePaths;
  }

  /**
   * @deprecated because it appears to throw a MirrorNoSuchRecordException if there is more than one
   *             instance in the database which isn't what it should do!
   */
  @Deprecated
  public static Integer getNodeStyleId(Table nodeStyleTable,
                                       String name)
      throws MirrorNoSuchRecordException
  {
    RecordSource nodeStyles = TableStringMatcher.search(nodeStyleTable, NodeStyle.NAME, name, null);
    IdList ids = nodeStyles.getIdList();
    if (ids.size() == 1) {
      return ids.get(0);
    }
    throw new MirrorNoSuchRecordException(String.format("NodeStyle.getNodeStyleId(%s) --> %s",
                                                        name,
                                                        nodeStyleTable),
                                          nodeStyleTable);
  }

  private final static String[] ZERO_STRING_ARRAY = new String[0];

  public static String[] getFilenames(String allFilenames)
  {
    if (Strings.isNullOrEmpty(allFilenames)) {
      return ZERO_STRING_ARRAY;
    }
    return allFilenames.split(",");
  }

  public static String calculateFilename(String[] filenames,
                                         int number,
                                         String defaultName)
  {
    if (filenames == null || filenames.length == 0) {
      return defaultName + "-" + number;
    }
    if (number >= 0 && number < filenames.length) {
      return filenames[number];
    }
    return filenames[filenames.length - 1] + "-" + number;
  }

  /**
   * Autogenerate a filename for a given node which is "node.nodestyle.name-node.pendingId"
   */
  public static String calculateFilename(TransientRecord node)
  {
    PersistentRecord nodeStyle = node.opt(Node.NODESTYLE);
    String nodeStyleFilename = nodeStyle.opt(NodeStyle.FILENAME);
    if (!Strings.isNullOrEmpty(nodeStyleFilename)) {
      PersistentRecord nodeGroupStyle = node.opt(Node.NODEGROUPSTYLE);
      if (nodeGroupStyle.compInt(NodeGroupStyle.MAXIMUM) == 1) {
        return nodeStyleFilename;
      }
    }
    return String.format("%s-%s",
                         nodeStyle.opt(NodeStyle.NAME),
                         Integer.valueOf(node.getPendingId()));
  }

  /**
   * Get the number of filenames encoded into a compound filename. This is used to decode compound
   * NODESTYLE_FILENAME fields.
   * 
   * @param allFilenames
   *          The (possibly) compound filename field.
   * @return The number of filenames that are inside the filename field.
   * @see NodeStyle#FILENAME
   */
  public static int getFilenameCount(String allFilenames)
  {
    return getFilenames(allFilenames).length;
  }

  /**
   * FieldTrigger on NodeStyle that updates the value of owner_id for implementing Nodes.
   */
  public class NodeStyleOwnerTrigger
    extends AbstractFieldTrigger<Integer>
  {
    public NodeStyleOwnerTrigger()
    {
      super(NodeStyle.OWNER_ID,
            false,
            false);
    }

    @Override
    public Object triggerField(TransientRecord nodeStyle,
                               String fieldName,
                               Transaction transaction)
        throws MirrorFieldException
    {
      Integer newOwnerId = nodeStyle.opt(NodeStyle.OWNER_ID);
      boolean isInheritedOwner = newOwnerId.intValue() == -1;
      for (PersistentRecord node : nodeStyle.opt(NodeStyle.NODES, transaction)) {
        if (isInheritedOwner) {
          PersistentRecord parentNode = node.opt(Node.PARENTNODE);
          if (parentNode == null) {
            throw new MirrorFieldException("owner_id cannot be set to inherited for root nodes",
                                           null,
                                           nodeStyle,
                                           NodeStyle.OWNER_ID);
          } else {
            node.setField(Node.OWNER_ID, parentNode.compInt(Node.OWNER_ID));
          }
        } else {
          node.setField(Node.OWNER_ID, newOwnerId);
        }
      }
      return null;
    }
  }

  /**
   * Get the id of the first Node that implements the given NodeStyle.
   * 
   * @throws MirrorNoSuchRecordException
   *           if there are no Nodes that implement this NodeStyle
   */
  public int getFirstNodeId(PersistentRecord nodeStyle)
      throws MirrorNoSuchRecordException
  {
    return RecordSources.getFirstRecord(nodeStyle.opt(NODES, null), null).getId();
  }

  /**
   * Get the id of the first Node that implements the given NodeStyle.
   * 
   * @throws MirrorNoSuchRecordException
   *           if either nodeStyleId cannot be resolved or there are no Nodes that implement this
   *           NodeStyle
   */
  public int getFirstNodeId(int nodeStyleId)
      throws MirrorNoSuchRecordException
  {
    return getFirstNodeId(getTable().getRecord(nodeStyleId));
  }

  /**
   * Get the first Node that implements the given NodeStyle
   * 
   * @throws MirrorNoSuchRecordException
   *           if there are no Nodes that implement this NodeStyle
   * @see #getFirstNodeId(PersistentRecord)
   */
  public PersistentRecord getFirstNode(PersistentRecord nodeStyle,
                                       Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return RecordSources.getFirstRecord(nodeStyle.opt(NODES, viewpoint), viewpoint);
  }

  /**
   * @return The first Node that implements nodeStyle. Never null
   * @throws MissingRecordException
   *           RuntimeException if nodeStyle has no nodes.
   */
  public PersistentRecord getCompFirstNode(PersistentRecord nodeStyle,
                                           Viewpoint viewpoint)
      throws MissingRecordException
  {
    return RecordSources.getCompFirstRecord(nodeStyle.opt(NODES, viewpoint), viewpoint, null);
  }

  public PersistentRecord getCompFirstNode(int nodeStyleId,
                                           Viewpoint viewpoint)
  {
    return getCompFirstNode(getTable().getCompRecord(nodeStyleId), viewpoint);
  }

  private final Int2IntMap __compOnlyNodes = Int2IntMaps.synchronize(new Int2IntOpenHashMap());

  public PersistentRecord getCompOnlyNode(PersistentRecord nodeStyle,
                                          Viewpoint viewpoint)
  {
    return getCompOnlyNode(nodeStyle.getId(), viewpoint);
  }

  /**
   * Resolve the only Node that implements the given NodeStyleId. This will cache the results so
   * that repeated requests will resolve very fast. It is worth noting that invoking this on a
   * NodeStyle that can have more than one Node but which currently only has one will lead to future
   * requests being inaccurate when more Nodes are created so you should utilise this against Nodes
   * that you are sure really are singletons in the hierarchy.
   * 
   * @throws MissingRecordException
   *           if the nodeStyleId cannot be resolved into a NodeStyle
   * @throws MirrorLogicException
   *           if the NodeStyle doesn't map to exactly one Node
   */
  public PersistentRecord getCompOnlyNode(int nodeStyleId,
                                          Viewpoint viewpoint)
      throws MissingRecordException, MirrorLogicException
  {
    int nodeId = __compOnlyNodes.get(nodeStyleId);
    if (nodeId != __compOnlyNodes.defaultReturnValue()) {
      return getNodeManager().getNode().getTable().getCompRecord(nodeId, viewpoint);
    }
    PersistentRecord nodeStyle = getTable().getCompRecord(nodeStyleId, viewpoint);
    PersistentRecord node =
        RecordSources.getOnlyRecord(nodeStyle.opt(NODES, viewpoint), viewpoint, null);
    __compOnlyNodes.put(nodeStyle.getId(), node.getId());
    return node;
  }

  /**
   * Resolve the only Node.id that implements the given NodeStyle.id. This will cache the results so
   * that repeated requests will resolve very fast. It is worth noting that invoking this on a
   * NodeStyle that can have more than one Node but which currently only has one will lead to future
   * requests being inaccurate when more Nodes are created so you should utilise this against Nodes
   * that you are sure really are singletons in the hierarchy.
   */
  public int getCompOnlyNodeId(int nodeStyleId,
                               Viewpoint viewpoint)
  {
    int nodeId = __compOnlyNodes.get(nodeStyleId);
    if (nodeId != __compOnlyNodes.defaultReturnValue()) {
      return nodeId;
    }
    PersistentRecord nodeStyle = getTable().getCompRecord(nodeStyleId, viewpoint);
    nodeId = RecordSources.getOnlyId(nodeStyle.opt(NODES, viewpoint), viewpoint);
    __compOnlyNodes.put(nodeStyle.getId(), nodeId);
    return nodeId;
  }

  /**
   * Get the first Node that implements the given NodeStyle
   * 
   * @throws MirrorNoSuchRecordException
   *           if either nodeStyleId cannot be resolved or there are no Nodes that implement this
   *           NodeStyle
   * @see #getFirstNodeId(int)
   */
  public PersistentRecord getFirstNode(int nodeStyleId,
                                       Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return getFirstNode(getTable().getRecord(nodeStyleId, viewpoint), viewpoint);
  }

  /**
   * Resolve a named NodeGroupStyle from a NodeStyle record.
   */
  public PersistentRecord getNodeGroupStyle(PersistentRecord nodeStyle,
                                            String name)
      throws MirrorNoSuchRecordException
  {
    StringBuilder buf = new StringBuilder();
    buf.append("RegionStyle.nodeStyle_id=")
       .append(nodeStyle.getId())
       .append(" and RegionStyle.name=");
    SqlUtil.toSafeSqlString(buf, name);
    String filter = buf.toString();
    return RecordSources.getFirstRecord(getNodeManager().getRegionStyle()
                                                        .getTable()
                                                        .getQuery(filter, filter, null),
                                        null);
  }

  /**
   * Register the appropriate CreateNodePolicy on all the parent NodeGroupStyles of the given
   * NodeStyle.
   * 
   * @param nodeStyle
   *          not null, must be NodeStyle record
   * @param createNodePolicy
   *          not null
   */
  public final void addCreateNodePolicy(PersistentRecord nodeStyle,
                                        CreateNodePolicy createNodePolicy)
  {
    TransientRecord.assertIsTableNamed(nodeStyle, NodeStyle.TABLENAME);
    Preconditions.checkNotNull(createNodePolicy, "createNodePolicy");
    for (PersistentRecord nodeGroupStyle : nodeStyle.opt(NodeStyle.PARENTNODEGROUPSTYLES)) {
      getNodeManager().getNodeGroupStyle()
                      .addCreateNodePolicy(Integer.valueOf(nodeGroupStyle.getId()),
                                           createNodePolicy);
    }
  }
}
