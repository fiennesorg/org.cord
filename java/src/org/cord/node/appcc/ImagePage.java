package org.cord.node.appcc;

import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.Copyables;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.RegionStyle;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ControlledIsPublished;
import org.cord.node.app.ControlledNumber;
import org.cord.node.app.ControlledTitle;
import org.cord.node.app.FieldSubCC;
import org.cord.node.app.JsonAppCc;
import org.cord.node.app.TemplatePaths;
import org.cord.node.trigger.ParentReferenceBooter;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.webmacro.Context;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public class ImagePage<A extends AppMgr>
  extends JsonAppCc<A>
  implements CopyableContentCompiler, MoveableContentCompiler
{
  public static final String NAME = "ImagePage";
  public static final String TABLENAME = NAME;
  public static final String REGIONSTYLENAME = "imagePage";

  public final static FieldKey<String> TITLE = ControlledTitle.TITLE();
  public final static FieldKey<Boolean> ISPUBLISHED = ControlledIsPublished.ISPUBLISHED();
  public final static IntFieldKey NUMBER = ControlledNumber.NUMBER();
  public static final IntFieldKey IMAGEPAGELISTING_ID =
      LinkField.getLinkFieldName(ImagePageListing.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> IMAGEPAGELISTING =
      LinkField.getLinkName(IMAGEPAGELISTING_ID);

  public static final ObjectFieldKey<Boolean> ISZOOMABLE = BooleanField.createKey("isZoomable");
  public static final ObjectFieldKey<Boolean> ISDOWNLOADABLE =
      BooleanField.createKey("isDownloadable");
  public static final ObjectFieldKey<Boolean> SHOWIMAGE = BooleanField.createKey("showImage");

  public static final List<FieldKey<?>> COPYABLES =
      ImmutableList.<FieldKey<?>> builder().add(ISZOOMABLE, ISDOWNLOADABLE).build();

  private final ZoomImagePage __zoomImagePage;

  private final ImagePageListing<A> __imagePageListing;

  public ImagePage(A appMgr,
                   ImagePageListing<A> imagePageListing,
                   String zoomImagePageTemplate)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/ImagePage.view.wm.html",
                                     "appcc/ImagePage.edit.wm.html"),
          REGIONSTYLENAME,
          null);
    __imagePageListing = Preconditions.checkNotNull(imagePageListing, "imagePageListing");
    __zoomImagePage = new ZoomImagePage(appMgr.getNodeMgr(), zoomImagePageTemplate);
  }

  public ImagePage(A appMgr,
                   ImagePageListing<A> imagePageListing)
  {
    this(appMgr,
         imagePageListing,
         "app/imagePageListing/imagePage.zoom.wm.html");
  }

  @Override
  protected void internalDeclare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.internalDeclare(regionStyle);
    getNodeMgr().getNodeRequestSegmentManager()
                .register(regionStyle.compInt(RegionStyle.NODESTYLE_ID),
                          Transaction.TRANSACTION_NULL,
                          __zoomImagePage);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances,
                      PureJsonObjectField data)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new ControlledTitle<A>(this));
    addSubCc(instances, new ControlledIsPublished<A>(this));
    addSubCc(instances, new ControlledNumber<A>(this));
    instances.addField(new LinkField(ImagePageListing.TABLENAME,
                                     "Containing ImagePageListing",
                                     pwd,
                                     Dependencies.LINK_ENFORCED));
    instances.addPreInstantiationTrigger(new ParentReferenceBooter(getNodeMgr(),
                                                                   IMAGEPAGELISTING_ID,
                                                                   ImagePageListing.REGIONSTYLENAME,
                                                                   false));
    addSubCc(instances,
             new FieldSubCC<A, Boolean>(this,
                                        new BooleanField(ISZOOMABLE,
                                                         "Is Zoomable?",
                                                         Boolean.TRUE)));
    addSubCc(instances,
             new FieldSubCC<A, Boolean>(this,
                                        new BooleanField(ISDOWNLOADABLE,
                                                         "Is Downloadable?",
                                                         Boolean.FALSE)));
    addSubCc(instances,
             new FieldSubCC<A, Boolean>(this,
                                        new BooleanField(SHOWIMAGE,
                                                         "Show image (uncheck for video)?",
                                                         Boolean.TRUE) {
                                          @Override
                                          public boolean shouldReloadOnChange()
                                          {
                                            return true;
                                          }
                                        }));
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getCompInstance(fromNode, null);
    Copyables.copy(instance, settable, COPYABLES);
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
    WritableRecord instance = (WritableRecord) getCompInstance(fromNode, transaction);
    PersistentRecord newImagePageListing =
        __imagePageListing.getCompInstance(toParentNode, transaction);
    try {
      instance.setField(IMAGEPAGELISTING_ID, newImagePageListing);
    } catch (MirrorFieldException e) {
      throw new NodeMoveException("Unable to update imagePageListing_id", e);
    }
  }

  public class ZoomImagePage
    extends NodeRequestSegmentFactory
  {
    public static final String NAME = "zoomImagePage";

    private final String __templatePath;

    public ZoomImagePage(NodeManager nodeMgr,
                         String templatePath)
    {
      super(NAME,
            nodeMgr);
      __templatePath = Preconditions.checkNotNull(templatePath, "templatePath");
    }

    @Override
    protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                             PersistentRecord node,
                                             Context context,
                                             boolean isSummaryRole)
        throws NodeRequestException
    {
      return new NodeRequestSegment(getNodeMgr(), nodeRequest, node, context, __templatePath);
    }

  }

}
