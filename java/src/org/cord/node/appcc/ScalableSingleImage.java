package org.cord.node.appcc;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.FileFormElement;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.Copyables;
import org.cord.node.FeedbackErrorException;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeMoveException;
import org.cord.node.app.AppCC;
import org.cord.node.app.AppMgr;
import org.cord.node.app.TemplatePaths;
import org.cord.node.img.ImgFormat;
import org.cord.node.img.ImgMgr;
import org.cord.node.img.ImgOriginal;
import org.cord.node.json.HandlesJSON;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;

import com.google.common.collect.ImmutableList;

public class ScalableSingleImage<A extends AppMgr>
  extends AppCC<A>
  implements HandlesJSON, MoveableContentCompiler, CopyableContentCompiler
{
  public static final String NAME = "ScalableSingleImage";

  public static final IntFieldKey IMGFORMAT_ID = LinkField.getLinkFieldName(ImgFormat.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> IMGFORMAT =
      LinkField.getLinkName(IMGFORMAT_ID);

  public static final IntFieldKey IMGORIGINAL_ID =
      LinkField.getLinkFieldName(ImgOriginal.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> IMGORIGINAL =
      LinkField.getLinkName(IMGORIGINAL_ID);
  public static final RecordOperationKey<RecordSource> IMGORIGINAL__SCALABLESINGLEIMAGES =
      LinkField.getReferencesKey(ScalableSingleImage.NAME);

  public static final ObjectFieldKey<String> ALT = StringField.createKey("alt");

  public static final List<RecordOperationKey<?>> COPYABLES =
      ImmutableList.<RecordOperationKey<?>> builder()
                   .add(IMGFORMAT_ID, IMGORIGINAL_ID, ALT)
                   .build();

  /**
   * RecordOperation that returns a Boolean that represents whether there is an instance of
   * ImgOriginal associated with this ScalableSingleImage
   */
  public static final RecordOperationKey<Boolean> ISUPLOADED =
      RecordOperationKey.create("isUploaded", Boolean.class);

  public static final RecordOperationKey<Object> FILE =
      RecordOperationKey.create("file", Object.class);
  public static final String CGI_FILE = FILE.getKeyName();
  public static final String CGI_OWNER = ImgOriginal.OWNER.getKeyName();

  public static final RecordOperationKey<RecordSource> IMGORIGINAL__SCALABLESINGLEIMAGENODES =
      RecordOperationKey.create("scalableSingleImageNodes", RecordSource.class);

  public ScalableSingleImage(A appMgr,
                             ScalableSingleImageStyle scalableSingleImageStyle)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/ScalableSingleImage.view.wm.html",
                                     "appcc/ScalableSingleImage.edit.wm.html"),
          scalableSingleImageStyle);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException
  {
    addFieldSubCC(instances,
                  new LinkField(ImgFormat.TABLENAME,
                                "Scaled Image Format",
                                pwd,
                                Dependencies.BIT_ENFORCED | Dependencies.BIT_OPTIONAL) {
                    private final String __filter = ImgFormat.ISDEFINEDFORMAT + "=1";

                    @Override
                    protected RecordSource getValidTargets(TransientRecord gntmlScalableImage)
                    {
                      return getTargetTable().getQuery(__filter, __filter, null);
                    }
                  });
    instances.addField(new LinkField(ImgOriginal.TABLENAME,
                                     "Original Image",
                                     pwd,
                                     Dependencies.BIT_ENFORCED | Dependencies.BIT_OPTIONAL));
    addFieldSubCC(instances, new StringField(ALT, "Text description of image"));
    instances.addRecordOperation(new FileFormElement(FILE, "Select image"));
    instances.addRecordOperation(new SimpleRecordOperation<Boolean>(ISUPLOADED) {
      @Override
      public Boolean getOperation(TransientRecord scalableSingleImage,
                                  Viewpoint viewpoint)
      {
        return Boolean.valueOf(scalableSingleImage.mayFollowLink(IMGORIGINAL));
      }
    });
    ImgOriginal ImgOriginal_ = getNodeMgr().getImgMgr().getImgOriginal();
    ImgOriginal_.getTable()
                .addRecordOperation(new SimpleRecordOperation<RecordSource>(IMGORIGINAL__SCALABLESINGLEIMAGENODES) {
                  @Override
                  public RecordSource getOperation(TransientRecord imgOriginal,
                                                   Viewpoint viewpoint)
                  {
                    IdList nodeIds = new ArrayIdList(getNodeMgr().getNode());
                    for (PersistentRecord scalableSingleImage : imgOriginal.comp(IMGORIGINAL__SCALABLESINGLEIMAGES,
                                                                                 viewpoint)) {
                      int nodeId = scalableSingleImage.compInt(ScalableSingleImage.NODE_ID);
                      if (!nodeIds.contains(nodeId)) {
                        nodeIds.add(nodeId);
                      }
                    }
                    return RecordSources.viewedFrom(new ConstantRecordSource(UnmodifiableIdList.getInstance(nodeIds),
                                                                             null),
                                                    viewpoint);
                  }
                });
    ImgOriginal_.registerNodeResolver(IMGORIGINAL__SCALABLESINGLEIMAGENODES);
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      FeedbackErrorException
  {
    boolean isChanged = super.updateTransientInstance(node,
                                                      regionStyle,
                                                      instance,
                                                      style,
                                                      transaction,
                                                      inputs,
                                                      outputs,
                                                      user);
    // JSONObject json = JsonStyleTableWrapper.getJSONObject(style);
    final ImgMgr imgMgr = getNodeMgr().getImgMgr();
    File file = inputs.getFile(CGI_FILE);
    if (file != null) {
      try {
        PersistentRecord imgOriginal =
            imgMgr.getImgOriginal().createInstance(file, inputs.getString(CGI_OWNER));
        instance.setField(IMGORIGINAL_ID, imgOriginal);
        isChanged = true;
      } catch (IOException ioEx) {
        throw new FeedbackErrorException("Upload Image",
                                         node,
                                         "It has not been possible to identify %s as an image file",
                                         file);
      }
    } else {
      instance.setFieldIfDefined(IMGORIGINAL_ID, inputs.get(IMGORIGINAL_ID));
      isChanged = isChanged | instance.getIsUpdated();
    }
    // PersistentRecord imgOriginal =
    // instance.followOptionalLink(IMGORIGINAL, transaction);
    // if (imgOriginal != null) {
    // PersistentRecord imgFormat =
    // imgMgr.getImgFormat().getTable().getOptRecord(params
    // .get(IMGFORMAT_ID));
    // if (imgFormat == null) {
    // imgFormat =
    // imgMgr.getImgFormat().getTable().getOptRecord(json
    // .opt(IMGFORMAT_ID));
    // if (imgFormat == null) {
    // imgFormat = imgMgr.getImgScaled().getDefaultImgFormat();
    // }
    // }
    // instance.setField(IMGFORMAT_ID, imgFormat);
    // // alt tags
    // }
    return isChanged;
  }

  @Override
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jCC,
                       Settable settable)
      throws SettableException, MirrorTransactionTimeoutException, MirrorFieldException,
      MirrorInstantiationException, MirrorNoSuchRecordException, JSONException, IOException,
      FeedbackErrorException
  {
    try {
      settable.set(IMGORIGINAL_ID,
                   jLoader.loadId(getNodeMgr().getImgMgr().getImgOriginal(),
                                  Integer.valueOf(jCC.optInt(IMGORIGINAL_ID.getKeyName()))));
    } catch (IOException e) {
      throw new IOException(String.format("Unable to load ImgOriginal from %s", jCC), e);
    }
    settable.set(IMGFORMAT_ID, Integer.valueOf(jCC.optInt(IMGFORMAT_ID.getKeyName())));
    settable.set(ALT, jCC.opt(ALT.getKeyName()));
  }

  @Override
  public JSONObject saveJSON(JSONSaver jsonSaver,
                             PersistentRecord node,
                             PersistentRecord regionStyle)
      throws JSONException, MirrorNoSuchRecordException, IOException
  {
    PersistentRecord instance = getInstance(node, regionStyle, null, null);
    WritableJSONObject jCC = new WritableJSONObject();
    if (instance.mayFollowLink(IMGORIGINAL)) {
      jCC.put(IMGORIGINAL_ID.getKeyName(), instance.opt(IMGORIGINAL_ID));
    }
    jCC.put(IMGFORMAT_ID.getKeyName(), instance.opt(IMGFORMAT_ID));
    jCC.put(ALT.getKeyName(), instance.opt(ALT));
    jsonSaver.save(instance.opt(IMGORIGINAL));
    return jCC;
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getInstance(fromNode, fromRegionStyle, null, null);
    Copyables.copy(instance, settable, COPYABLES);
  }
}
