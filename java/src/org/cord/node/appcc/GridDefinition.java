package org.cord.node.appcc;

import java.util.Date;

import org.cord.mirror.Db;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.app.AppMgr;
import org.cord.node.app.SingletonAppCc;
import org.cord.node.app.TemplatePaths;
import org.cord.node.grid.Grid;
import org.cord.util.Casters;
import org.cord.util.Gettable;

import com.google.common.base.Objects;

public class GridDefinition
  extends SingletonAppCc<AppMgr>
{
  public static final String NAME = "GridDefinition";
  public static final String REGIONSTYLENAME = "gridDefinition";

  public static final IntFieldKey COLUMNWIDTH = IntFieldKey.create("columnWidth");
  public static final IntFieldKey GUTTERSIZE = IntFieldKey.create("gutterSize");
  public static final ObjectFieldKey<Boolean> HASHPADSPACER =
      BooleanField.createKey("hasHPadSpacer");
  public static final IntFieldKey LINEHEIGHT = IntFieldKey.create("lineHeight");
  public static final ObjectFieldKey<Date> HASHSTAMP = FastDateField.createKey("hashstamp");

  /**
   * RecordOperation that generates a Long that represents the current state of the GridDefinition.
   * The operation takes a single parameter that lets you represent the state of the external
   * components that go to make up the current configuration of the CSS state of the system, eg
   * which user is logged in or some such thing. If this is null then it will be padded with a
   * constant value of 0. The result is guaranteed to be constant if the layout params of the
   * GridDefinition and the external var have not changed. This is intended to be used for
   * generating unique URLs for the current layout of the system which will therefore facilitate
   * client side caching of the generated CSS files.
   */
  public static final MonoRecordOperationKey<Object, Integer> HASH =
      MonoRecordOperationKey.create("hash", Object.class, Integer.class);

  /**
   * RecordOperation to generate an instance of {@link Grid} from a GridDefinition instance.
   */
  public static final RecordOperationKey<Grid> GRID = RecordOperationKey.create("grid", Grid.class);

  private final int __columnCount;

  public GridDefinition(AppMgr appMgr)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/GridDefinition.view.wm.html",
                                     "appcc/GridDefinition.edit.wm.html"),
          REGIONSTYLENAME);
    __columnCount = Grid.getInt(Grid.GET_COLS, appMgr.getNodeMgr().getGettable(), null);
  }

  public void initValues(Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    PersistentRecord instance = getInstance();
    try (Transaction t = getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                   Transaction.TRANSACTION_UPDATE,
                                                   getTransactionPassword(),
                                                   "GridDefinition.initValues")) {
      instance = t.edit(instance);
      instance.setField(COLUMNWIDTH, params.get(Grid.GET_GRID));
      instance.setField(GUTTERSIZE, params.get(Grid.GET_HPAD));
      instance.setField(LINEHEIGHT, params.get(Grid.GET_LINEHEIGHT));
      t.commit();
    }
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException
  {
    addFieldSubCC(instances, new IntField(COLUMNWIDTH, "Column Width (px)", 50));
    addFieldSubCC(instances, new IntField(GUTTERSIZE, "Gutter either side of column (px)", 10));
    addFieldSubCC(instances,
                  new BooleanField(HASHPADSPACER,
                                   "Has single pixel spacer between gutters?",
                                   Boolean.FALSE));
    addFieldSubCC(instances, new IntField(LINEHEIGHT, "Default line height (px)", 20));
    addFieldSubCC(instances, new FastDateField(HASHSTAMP, "Last Template change time"));
    instances.addRecordOperation(new SimpleRecordOperation<Grid>(GRID) {
      @Override
      public Grid getOperation(TransientRecord gridDefinition,
                               Viewpoint viewpoint)
      {
        return new Grid(__columnCount,
                        gridDefinition.compInt(COLUMNWIDTH),
                        gridDefinition.compInt(GUTTERSIZE),
                        gridDefinition.is(HASHPADSPACER),
                        gridDefinition.compInt(LINEHEIGHT));
      }
    });
    instances.addRecordOperation(new MonoRecordOperation<Object, Integer>(HASH, Casters.NULL) {
      @Override
      public Integer calculate(TransientRecord gridDefinition,
                               Viewpoint viewpoint,
                               Object hash)
      {
        return Integer.valueOf(Objects.hashCode(gridDefinition.opt(COLUMNWIDTH),
                                                gridDefinition.opt(GUTTERSIZE),
                                                gridDefinition.opt(HASHPADSPACER),
                                                gridDefinition.opt(LINEHEIGHT),
                                                gridDefinition.opt(HASHSTAMP),
                                                hash));
      }
    });
  }
}
