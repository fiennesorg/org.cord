package org.cord.node.appcc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.cord.mirror.TransientRecord;
import org.cord.util.Dates;
import org.cord.util.NthDateFormat;

/**
 * TimedPageDateFormatted that utilises {@link SimpleDateFormat} to format the dates and has no
 * clever logic for TimedPages with start and end values and just displays them twice with a
 * configurable separator.
 * 
 * @author alex
 */
public class TimedPageSimpleDateFormatter
  extends TimedPageDateFormatter
{
  private final DateFormat __eventsMultiDay;
  private final DateFormat __eventsMultiTime;
  private final DateFormat __eventsSingleDay;
  private final DateFormat __eventsSingleTime;
  private final String __multiSeparator;
  private final DateFormat __newsDate;

  public TimedPageSimpleDateFormatter(String eventsMultiDay,
                                      String eventsMultiTime,
                                      String multiSeparator,
                                      String eventsSingleDay,
                                      String eventsSingleTime,
                                      String newsDate)
  {
    __eventsMultiDay = NthDateFormat.getInstance(eventsMultiDay);
    __eventsMultiTime = NthDateFormat.getInstance(eventsMultiTime);
    __multiSeparator = multiSeparator;
    __eventsSingleDay = NthDateFormat.getInstance(eventsSingleDay);
    __eventsSingleTime = NthDateFormat.getInstance(eventsSingleTime);
    __newsDate = NthDateFormat.getInstance(newsDate);
  }

  @Override
  public String formatEventsMultiDay(TransientRecord timedPage)
  {
    return Dates.format(__eventsMultiDay, timedPage.opt(TimedPage.STARTTIME)) + __multiSeparator
           + Dates.format(__eventsMultiDay, timedPage.opt(TimedPage.ENDTIME));
  }

  @Override
  public String formatEventsMultiTime(TransientRecord timedPage)
  {
    return Dates.format(__eventsMultiTime, timedPage.opt(TimedPage.STARTTIME)) + __multiSeparator
           + Dates.format(__eventsMultiTime, timedPage.opt(TimedPage.ENDTIME));
  }

  @Override
  public String formatEventsSingleDay(TransientRecord timedPage)
  {
    return Dates.format(__eventsSingleDay, timedPage.opt(TimedPage.STARTTIME));
  }

  @Override
  public String formatEventsSingleTime(TransientRecord timedPage)
  {
    return Dates.format(__eventsSingleTime, timedPage.opt(TimedPage.STARTTIME));
  }

  @Override
  public String formatNewsDate(TransientRecord timedPage)
  {
    return Dates.format(__newsDate, timedPage.opt(TimedPage.STARTTIME));
  }

}
