package org.cord.node.appcc;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.EnumField;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSourceOrdering;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.recordsource.operation.SimpleFilterOperation;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.Copyables;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeMoveException;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ControlledIsPublished;
import org.cord.node.app.ControlledTitle;
import org.cord.node.app.JsonAppCc;
import org.cord.node.app.TemplatePaths;
import org.cord.node.appcc.TimedPageListing.ListingStyle;
import org.cord.node.trigger.ParentReferenceBooter;
import org.cord.util.Casters;
import org.cord.util.DateQuantiser;
import org.cord.util.DateQuantiser.Granularity;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.Today;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Macro;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class TimedPage<A extends AppMgr>
  extends JsonAppCc<A>
  implements CopyableContentCompiler, MoveableContentCompiler
{
  public static final String NAME = "TimedPage";
  public static final String TABLENAME = NAME;
  public static final String REGIONSTYLENAME = "timedPage";

  public final static FieldKey<String> TITLE = ControlledTitle.TITLE();
  public final static FieldKey<Boolean> ISPUBLISHED = ControlledIsPublished.ISPUBLISHED();
  public static final IntFieldKey TIMEDPAGELISTING_ID =
      LinkField.getLinkFieldName(TimedPageListing.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> TIMEDPAGELISTING =
      LinkField.getLinkName(TIMEDPAGELISTING_ID);
  public static final ObjectFieldKey<Date> STARTTIME = FastDateField.createKey("startTime");
  public static final ObjectFieldKey<Date> ENDTIME = FastDateField.createKey("endTime");
  /**
   * Field that holds a {@link TimedPageType} value that represents what style timing information is
   * held about this TimedPage record.
   */
  public static final ObjectFieldKey<TimedPageType> TIMEDPAGETYPE =
      EnumField.createKey("timedPageType", TimedPageType.class);

  public static final List<FieldKey<?>> COPYABLES =
      ImmutableList.<FieldKey<?>> builder().add(STARTTIME, ENDTIME, TIMEDPAGETYPE).build();

  public static final RecordSourceOperationKey<RecordSource> FUTUREEVENTS =
      RecordSourceOperationKey.create("futureEvents", RecordSource.class);
  public static final RecordSourceOperationKey<RecordSource> PASTEVENTS =
      RecordSourceOperationKey.create("pastEvents", RecordSource.class);
  public static final MonoRecordSourceOperationKey<String, RecordSource> ISLISTINGSTYLEOF =
      MonoRecordSourceOperationKey.create("isListingStyleOf", String.class, RecordSource.class);
  public static final RecordSourceOperationKey<RecordSource> ISFORWARDLOOKING =
      RecordSourceOperationKey.create("isForwardLooking", RecordSource.class);
  public static final RecordSourceOperationKey<RecordSource> ISBACKWARDSLOOKING =
      RecordSourceOperationKey.create("isBackwardsLooking", RecordSource.class);

  /**
   * Utilise the {@link TimedPageDateFormatter} that is registered on this TimedPage to format the
   * dates associated with this entry and return this as a String.
   */
  public static final RecordOperationKey<String> FORMATTEDDATES =
      RecordOperationKey.create("formattedDates", String.class);

  public static final RecordSourceOperationKey<List<List<PersistentRecord>>> SPLITBYMONTH =
      RecordSourceOperationKey.create("splitByMonth",
                                      ObjectUtil.<List<List<PersistentRecord>>> castClass(List.class));
  public static final RecordSourceOperationKey<List<List<PersistentRecord>>> SPLITBYYEAR =
      RecordSourceOperationKey.create("splitByYear",
                                      ObjectUtil.<List<List<PersistentRecord>>> castClass(List.class));

  public static final String STARTTIME_ASC = STARTTIME + " asc";
  public static final String STARTTIME_DESC = STARTTIME + " desc";

  /**
   * RecordSourceOperation that will filter out all all TimedPage records that have their date older
   * than one month before now.
   */
  public static final RecordSourceOperationKey<RecordSource> ISLASTMONTHONWARDS =
      RecordSourceOperationKey.create("isLastMonthOnwards", RecordSource.class);

  public static final RecordOrdering ORDERING_STARTTIME_ASC =
      RecordSourceOrdering.createRecordOrdering(TABLENAME, STARTTIME_ASC, "Sort Date Ascending");
  public static final RecordOrdering ORDERING_STARTTIME_DESC =
      RecordSourceOrdering.createRecordOrdering(TABLENAME, STARTTIME_DESC, "Sort Date Descending");

  private final TimedPageListing<A> __timedPageListing;

  private final TimedPageDateFormatter __timedPageDateFormatter;

  public TimedPage(A appMgr,
                   TimedPageListing<A> timedPageListing,
                   TimedPageDateFormatter timedPageDateFormatter)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/TimedPage.view.wm.html",
                                     "appcc/TimedPage.edit.wm.html"),
          REGIONSTYLENAME,
          null);
    __timedPageListing = Preconditions.checkNotNull(timedPageListing, "timedPageListing");
    if (timedPageDateFormatter != null) {
      __timedPageDateFormatter = timedPageDateFormatter;
    } else {
      String formatter =
          appMgr.getNodeMgr().getGettable().getString("TimedPage.TimedPageDateFormatter.Class");
      if (formatter == null) {
        __timedPageDateFormatter = new TimedPageSimpleDateFormatter("dd/MM/yy",
                                                                    "HH:mm dd/MM/yy",
                                                                    " - ",
                                                                    "dd/MM/yy",
                                                                    "HH:mm dd/MM/yy",
                                                                    "dd/MM/yy");
      } else {
        try {
          __timedPageDateFormatter =
              (TimedPageDateFormatter) Class.forName(formatter).newInstance();
        } catch (Exception e) {
          throw new IllegalArgumentException(String.format("Unable to initialise TimedPageDateFormatter of %s",
                                                           formatter),
                                             e);
        }
      }
    }
  }

  public TimedPageDateFormatter getDateFormatter()
  {
    return __timedPageDateFormatter;
  }

  public TimedPage(A appMgr,
                   TimedPageListing<A> timedPageListing)
  {
    this(appMgr,
         timedPageListing,
         null);
  }

  public enum TimedPageType {
    SINGLEDAY() {
      private final DateQuantiser __quantiser = new DateQuantiser();
      private final Calendar __calendar = Calendar.getInstance();

      @Override
      public String toString()
      {
        return "Event with start day";
      }

      @Override
      public Date filterStartTime(TransientRecord timedPage,
                                  Date startTime)
      {
        return __quantiser.byDay(startTime);
      }

      @Override
      public Date filterEndTime(TransientRecord timedPage,
                                Date endTime)
      {
        return endOfDay(__calendar, timedPage.opt(STARTTIME));
      }

      @Override
      public Macro getEventEditWidgets(final TransientRecord timedPage)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            HtmlUtils.dateSelector(out,
                                   context,
                                   "Date",
                                   timedPage.opt(STARTTIME),
                                   timedPage.opt(NAMEPREFIX),
                                   STARTTIME);
          }
        };
      }

      @Override
      public boolean updateInstance(TransientRecord timedPage,
                                    Gettable params)
          throws MirrorFieldException
      {
        Date startTime = HtmlUtils.parseDateSelector(params.get(STARTTIME));
        if (startTime != null) {
          timedPage.setField(STARTTIME, startTime);
        }
        return timedPage.getIsUpdated();
      }
    },
    TIMEDSTART() {
      private final DateQuantiser __quantiser = new DateQuantiser();

      @Override
      public String toString()
      {
        return "Event with start time";
      }

      @Override
      public Date filterStartTime(TransientRecord timedPage,
                                  Date startTime)
      {
        return __quantiser.byMinute(startTime);
      }

      @Override
      public Date filterEndTime(TransientRecord timedPage,
                                Date endTime)
      {
        return timedPage.opt(STARTTIME);
      }

      @Override
      public Macro getEventEditWidgets(final TransientRecord timedPage)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            HtmlUtils.dateTimeSelector(out,
                                       context,
                                       "Start Time",
                                       timedPage.opt(STARTTIME),
                                       timedPage.opt(NAMEPREFIX),
                                       STARTTIME);
          }
        };
      }

      @Override
      public boolean updateInstance(TransientRecord timedPage,
                                    Gettable params)
          throws MirrorFieldException
      {
        Date startTime = HtmlUtils.parseDateTimeSelector(params.get(STARTTIME));
        if (startTime != null) {
          timedPage.setField(STARTTIME, startTime);
        }
        return timedPage.getIsUpdated();
      }
    },
    TIMEDSTARTEND() {
      private final DateQuantiser __quantiser = new DateQuantiser();

      @Override
      public String toString()
      {
        return "Event with start and end time";
      }

      @Override
      public Date filterStartTime(TransientRecord timedPage,
                                  Date startTime)
      {
        return __quantiser.byMinute(startTime);
      }

      @Override
      public Date filterEndTime(TransientRecord timedPage,
                                Date endTime)
      {
        Date startTime = timedPage.opt(STARTTIME);
        if (startTime.after(endTime)) {
          return startTime;
        }
        return __quantiser.byMinute(endTime);
      }

      @Override
      public Macro getEventEditWidgets(final TransientRecord timedPage)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            HtmlUtils.dateTimeSelector(out,
                                       context,
                                       "Start Time",
                                       timedPage.opt(STARTTIME),
                                       timedPage.opt(NAMEPREFIX),
                                       STARTTIME);
            HtmlUtils.dateTimeSelector(out,
                                       context,
                                       "End Time",
                                       timedPage.opt(ENDTIME),
                                       timedPage.opt(NAMEPREFIX),
                                       ENDTIME);
          }
        };
      }

      @Override
      public boolean updateInstance(TransientRecord timedPage,
                                    Gettable params)
          throws MirrorFieldException
      {
        Date startTime = HtmlUtils.parseDateTimeSelector(params.get(STARTTIME));
        if (startTime != null) {
          timedPage.setField(STARTTIME, startTime);
        }
        Date endTime = HtmlUtils.parseDateTimeSelector(params.get(ENDTIME));
        if (endTime != null) {
          timedPage.setField(ENDTIME, endTime);
        }
        return timedPage.getIsUpdated();
      }

    },
    MULTIDAY() {
      private final Calendar __calendar = Calendar.getInstance();

      @Override
      public String toString()
      {
        return "Event with start and end date";
      }

      @Override
      public Date filterStartTime(TransientRecord timedPage,
                                  Date startTime)
      {
        return startOfDay(__calendar, startTime);
      }

      @Override
      public Date filterEndTime(TransientRecord timedPage,
                                Date endTime)
      {
        Date startTime = timedPage.opt(STARTTIME);
        if (startTime.after(endTime)) {
          return endTime = startTime;
        }
        return endOfDay(__calendar, endTime);
      }

      @Override
      public Macro getEventEditWidgets(final TransientRecord timedPage)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            HtmlUtils.dateSelector(out,
                                   context,
                                   "Start Date",
                                   timedPage.opt(STARTTIME),
                                   timedPage.opt(NAMEPREFIX),
                                   STARTTIME);
            HtmlUtils.dateSelector(out,
                                   context,
                                   "End Date",
                                   timedPage.opt(ENDTIME),
                                   timedPage.opt(NAMEPREFIX),
                                   ENDTIME);
          }
        };
      }

      @Override
      public boolean updateInstance(TransientRecord timedPage,
                                    Gettable params)
          throws MirrorFieldException
      {
        Date startTime = HtmlUtils.parseDateSelector(params.get(STARTTIME));
        if (startTime != null) {
          timedPage.setField(STARTTIME, startTime);
        }
        Date endTime = HtmlUtils.parseDateSelector(params.get(ENDTIME));
        if (endTime != null) {
          timedPage.setField(ENDTIME, endTime);
        }
        return timedPage.getIsUpdated();
      }
    };

    private static Date endOfDay(Calendar calendar,
                                 Date time)
    {
      synchronized (calendar) {
        calendar.setTime(time);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
      }
    }

    private static Date startOfDay(Calendar calendar,
                                   Date time)
    {
      synchronized (calendar) {
        calendar.setTime(time);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
      }
    }

    private TimedPageType()
    {
    }

    public final String formatDates(TransientRecord timedPage)
    {
      return timedPage.opt(FORMATTEDDATES);
    }

    public abstract Date filterStartTime(TransientRecord timedPage,
                                         Date startTime);
    public abstract Date filterEndTime(TransientRecord timedPage,
                                       Date endTime);
    public abstract Macro getEventEditWidgets(TransientRecord timedPage);

    public final Macro getEditWidgets(final PersistentRecord timedPage)
    {
      switch (getListingStyle(timedPage)) {
        case NEWS:
          return new AppendableParametricMacro() {
            @Override
            public void append(Appendable out,
                               Map<Object, Object> context)
                throws IOException
            {
              HtmlUtils.dateTimeSelector(out,
                                         context,
                                         "Publication Time",
                                         timedPage.opt(STARTTIME),
                                         timedPage.opt(NAMEPREFIX),
                                         STARTTIME);
            }
          };
        case EVENTS:
          return (getEventEditWidgets(timedPage));
        default:
          throw new LogicException("unexpected listingStyle");
      }
    }
    public abstract boolean updateInstance(TransientRecord timedPage,
                                           Gettable params)
        throws MirrorFieldException;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances,
                      PureJsonObjectField data)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new ControlledTitle<A>(this));
    addSubCc(instances, new ControlledIsPublished<A>(this));
    instances.addField(new LinkField(TimedPageListing.TABLENAME,
                                     "Containing TimedPageListing",
                                     pwd,
                                     Dependencies.LINK_ENFORCED));
    instances.addPreInstantiationTrigger(new ParentReferenceBooter(getNodeMgr(),
                                                                   TIMEDPAGELISTING_ID,
                                                                   TimedPageListing.REGIONSTYLENAME,
                                                                   false));
    instances.addField(new FastDateField(STARTTIME, "Start Time", Granularity.MINUTE));
    instances.addFieldTrigger(new AbstractFieldTrigger<Date>(STARTTIME, true, false) {
      @Override
      protected Object triggerField(TransientRecord record,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        switch (getListingStyle(record)) {
          case EVENTS:
            TimedPageType type = record.opt(TIMEDPAGETYPE);
            Date original = record.opt(STARTTIME);
            Date filtered = type.filterStartTime(record, original);
            record.setField(ENDTIME, record.opt(ENDTIME));
            return original.equals(filtered) ? null : filtered;
          default:
            return null;
        }
      }
    });
    instances.addField(new FastDateField(ENDTIME, "End Time", Granularity.MINUTE));
    instances.addFieldTrigger(new AbstractFieldTrigger<Date>(ENDTIME, true, false) {
      @Override
      protected Object triggerField(TransientRecord record,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        switch (getListingStyle(record)) {
          case EVENTS:
            TimedPageType type = record.opt(TIMEDPAGETYPE);
            Date original = record.opt(ENDTIME);
            Date filtered = type.filterEndTime(record, original);
            return original.equals(filtered) ? null : filtered;
        }
        return null;
      }
    });
    instances.addField(new EnumField<TimedPageType>(TIMEDPAGETYPE,
                                                    "Timing Model",
                                                    TimedPageType.class,
                                                    FieldValueSuppliers.of(TimedPageType.SINGLEDAY)) {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    instances.addFieldTrigger(new AbstractFieldTrigger<TimedPageType>(TIMEDPAGETYPE, true, false) {
      @Override
      protected Object triggerField(TransientRecord record,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        record.setField(STARTTIME, record.opt(STARTTIME));
        return null;
      }
    });
    instances.setDefaultOrdering(STARTTIME_DESC);
    instances.addRecordOperation(new SimpleRecordOperation<String>(FORMATTEDDATES) {
      @Override
      public String getOperation(TransientRecord timedPage,
                                 Viewpoint viewpoint)
      {
        return getDateFormatter().formatDates(timedPage);
      }
    });
    instances.addRecordSourceOperation(new SimpleFilterOperation(FUTUREEVENTS, true) {
      @Override
      protected boolean accepts(PersistentRecord record)
      {
        Date startTime = record.opt(STARTTIME);
        return startTime.compareTo(Today.getInstance().getDate()) >= 0;
      }

      @Override
      protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                      Viewpoint viewpoint)
      {
        return RecordSources.applyRecordOrdering(super.invokeRecordSourceBranch(recordSource,
                                                                                viewpoint),
                                                 ORDERING_STARTTIME_ASC);
      }

      @Override
      protected RecordSource invokeQueryBranch(Query query,
                                               Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(TABLENAME + "." + ENDTIME + ">=")
           .append(Today.getInstance().getDate().getTime());
        return Query.transformQuery(query, buf.toString(), Query.TYPE_AND, STARTTIME_ASC);
      }

    });
    instances.addRecordSourceOperation(new SimpleFilterOperation(PASTEVENTS, true) {
      @Override
      protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                      Viewpoint viewpoint)
      {
        return RecordSources.applyRecordOrdering(super.invokeRecordSourceBranch(recordSource,
                                                                                viewpoint),
                                                 ORDERING_STARTTIME_DESC);
      }

      @Override
      protected boolean accepts(PersistentRecord record)
      {
        Date startTime = record.opt(STARTTIME);
        return startTime.compareTo(Today.getInstance().getDate()) < 0;
      }

      @Override
      protected RecordSource invokeQueryBranch(Query query,
                                               Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(TABLENAME + "." + ENDTIME + "<").append(Today.getInstance().getDate().getTime());
        return Query.transformQuery(query, buf.toString(), Query.TYPE_AND, STARTTIME_DESC);
      }
    });
    instances.addRecordSourceOperation(new MonoRecordSourceOperation<String, RecordSource>(ISLISTINGSTYLEOF,
                                                                                           Casters.TOSTRING) {
      @Override
      public RecordSource calculate(RecordSource recordSource,
                                    Viewpoint viewpoint,
                                    String a)
      {
        final ListingStyle listingStyle =
            Preconditions.checkNotNull(ListingStyle.valueOf(a),
                                       "Unable to resolve %s as a ListingStyle",
                                       a);
        viewpoint = RecordSources.getViewpoint(recordSource, viewpoint);
        return new SingleRecordSourceFilter(recordSource, null) {
          @Override
          protected boolean accepts(PersistentRecord timedPage)
          {
            return listingStyle.equals(timedPage.comp(TIMEDPAGELISTING)
                                                .opt(TimedPageListing.LISTINGSTYLE));
          }
        };
      }
    });
    instances.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(ISFORWARDLOOKING) {
      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                      Viewpoint viewpoint)
      {
        return new SingleRecordSourceFilter(recordSource, viewpoint) {

          @Override
          protected boolean accepts(PersistentRecord timedPage)
          {
            ListingStyle listingStyle =
                timedPage.comp(TIMEDPAGELISTING).opt(TimedPageListing.LISTINGSTYLE);
            return listingStyle.isForwardLooking();
          }
        };
      }
    });
    instances.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(ISBACKWARDSLOOKING) {
      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource recordSource,
                                                      Viewpoint viewpoint)
      {
        return new SingleRecordSourceFilter(recordSource, viewpoint) {

          @Override
          protected boolean accepts(PersistentRecord timedPage)
          {
            ListingStyle listingStyle =
                timedPage.comp(TIMEDPAGELISTING).opt(TimedPageListing.LISTINGSTYLE);
            return !listingStyle.isForwardLooking();
          }
        };
      }
    });
    instances.addRecordSourceOperation(new SimpleFilterOperation(ISLASTMONTHONWARDS, true) {
      private final long DURATION_MONTH = DateQuantiser.DURATION_DAY * 31;

      public long getLastMonth()
      {
        return Today.getInstance().getDate().getTime() - DURATION_MONTH;
      }

      @Override
      protected boolean accepts(PersistentRecord timedPage)
      {
        Date startTime = timedPage.opt(STARTTIME);
        return startTime.getTime() >= getLastMonth();
      }

      @Override
      protected RecordSource invokeQueryBranch(Query query,
                                               Viewpoint viewpoint)
      {
        return Query.transformQuery(query, STARTTIME + ">=" + getLastMonth(), Query.TYPE_AND, null);
      }
    });
    instances.addRecordSourceOperation(new SimpleRecordSourceOperation<List<List<PersistentRecord>>>(SPLITBYMONTH) {
      private final Calendar __calendar = Calendar.getInstance();

      @Override
      public List<List<PersistentRecord>> invokeRecordSourceOperation(RecordSource timedPages,
                                                                      Viewpoint viewpoint)
      {
        RecordList timedPageRecords = timedPages.getRecordList(viewpoint);
        if (timedPageRecords.size() == 0) {
          return Collections.emptyList();
        }
        List<List<PersistentRecord>> months = Lists.newArrayList();
        List<PersistentRecord> month = null;
        int lastMonth = 0;
        for (PersistentRecord timedPage : timedPageRecords) {
          int currentMonth;
          synchronized (__calendar) {
            __calendar.setTime(timedPage.comp(STARTTIME));
            currentMonth = __calendar.get(Calendar.MONTH) + __calendar.get(Calendar.YEAR) * 12;
          }
          if (lastMonth != currentMonth) {
            month = Lists.newArrayList();
            months.add(Collections.unmodifiableList(month));
            lastMonth = currentMonth;
          }
          month.add(timedPage);
        }
        return Collections.unmodifiableList(months);
      }
    });
    instances.addRecordSourceOperation(new SimpleRecordSourceOperation<List<List<PersistentRecord>>>(SPLITBYYEAR) {
      private final Calendar __calendar = Calendar.getInstance();

      @Override
      public List<List<PersistentRecord>> invokeRecordSourceOperation(RecordSource timedPages,
                                                                      Viewpoint viewpoint)
      {
        RecordList timedPageRecords = timedPages.getRecordList(viewpoint);
        if (timedPageRecords.size() == 0) {
          return Collections.emptyList();
        }
        List<List<PersistentRecord>> years = Lists.newArrayList();
        List<PersistentRecord> year = null;
        int lastYear = 0;
        for (PersistentRecord timedPage : timedPageRecords) {
          int currentYear;
          synchronized (__calendar) {
            __calendar.setTime(timedPage.comp(STARTTIME));
            currentYear = __calendar.get(Calendar.YEAR);
          }
          if (lastYear != currentYear) {
            year = Lists.newArrayList();
            years.add(Collections.unmodifiableList(year));
            lastYear = currentYear;
          }
          year.add(timedPage);
        }
        return Collections.unmodifiableList(years);
      }
    });
  }
  /**
   * Get the ListingStyle that the containing TimedPageListing is configured as, or null if it is
   * not yet possible to resolve the containing LIstingType due to bootup ordering
   */
  public static ListingStyle getListingStyle(TransientRecord timedPage)
  {
    PersistentRecord timedPageListing = timedPage.opt(TIMEDPAGELISTING);
    if (timedPageListing == null) {
      return null;
    }
    return timedPageListing.opt(TimedPageListing.LISTINGSTYLE);
  }

  @Override
  protected boolean updatePersistentInstance(PersistentRecord node,
                                             PersistentRecord regionStyle,
                                             PersistentRecord timedPage,
                                             PersistentRecord style,
                                             Transaction transaction,
                                             Gettable inputs,
                                             Map<Object, Object> outputs,
                                             PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    boolean result = super.updatePersistentInstance(node,
                                                    regionStyle,
                                                    timedPage,
                                                    style,
                                                    transaction,
                                                    inputs,
                                                    outputs,
                                                    user);
    ListingStyle listingStyle = getListingStyle(timedPage);
    if (listingStyle != null) {
      timedPage = transaction.edit(timedPage);
      switch (listingStyle) {
        case NEWS:
          Date startTime = HtmlUtils.parseDateTimeSelector(inputs.get(STARTTIME));
          if (startTime != null) {
            timedPage.setField(STARTTIME, startTime);
            timedPage.setField(ENDTIME, startTime);
          }
          break;
        case EVENTS:
          uploadStringField(inputs, timedPage, regionStyle, TIMEDPAGETYPE, false);
          TimedPageType type = timedPage.opt(TIMEDPAGETYPE);
          result = result | type.updateInstance(timedPage, inputs);
          break;
        default:
          throw new LogicException("unexpected listingStyle: " + listingStyle);
      }
    }
    return result;
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getCompInstance(fromNode, null);
    Copyables.copy(instance, settable, COPYABLES);
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
    WritableRecord instance = (WritableRecord) getCompInstance(fromNode, transaction);
    PersistentRecord newTimedPageListing =
        __timedPageListing.getCompInstance(toParentNode, transaction);
    try {
      instance.setField(TIMEDPAGELISTING_ID, newTimedPageListing);
    } catch (MirrorFieldException e) {
      throw new NodeMoveException("Unable to update timedPageListing_id", e);
    }
  }

}
