package org.cord.node.appcc;

import org.cord.mirror.RecordValueFactory;
import org.cord.mirror.TransientRecord;
import org.cord.node.appcc.TimedPage.TimedPageType;
import org.cord.node.appcc.TimedPageListing.ListingStyle;
import org.cord.util.LogicException;

/**
 * Base class for objects that are capable of formatting the date information contained in an
 * instance of TimedPage. There must be one instance of this declared when TimedPage is initialised,
 * and if it is not passed in explicitly then the system will try and find one.
 * 
 * @author alex
 */
public abstract class TimedPageDateFormatter
  implements RecordValueFactory<String>
{
  public abstract String formatNewsDate(TransientRecord timedPage);
  public abstract String formatEventsSingleDay(TransientRecord timedPage);
  public abstract String formatEventsMultiDay(TransientRecord timedPage);
  public abstract String formatEventsSingleTime(TransientRecord timedPage);
  public abstract String formatEventsMultiTime(TransientRecord timedPage);

  public final String formatDates(TransientRecord timedPage)
  {
    ListingStyle listingStyle = TimedPage.getListingStyle(timedPage);
    switch (listingStyle) {
      case NEWS:
        return formatNewsDate(timedPage);
      case EVENTS:
        TimedPageType timedPageType = timedPage.opt(TimedPage.TIMEDPAGETYPE);
        switch (timedPageType) {
          case SINGLEDAY:
            return formatEventsSingleDay(timedPage);
          case MULTIDAY:
            return formatEventsMultiDay(timedPage);
          case TIMEDSTART:
            return formatEventsSingleTime(timedPage);
          case TIMEDSTARTEND:
            return formatEventsMultiTime(timedPage);
          default:
            throw new LogicException(String.format("Unknown TimedPageType of %s in %s",
                                                   timedPageType,
                                                   timedPage));
        }
      default:
        throw new LogicException(String.format("Unknown ListingStyle of %s in %s",
                                               listingStyle,
                                               timedPage));
    }
  }

  @Override
  public final String getRecordValue(TransientRecord timedPage)
  {
    return formatDates(timedPage);
  }
}
