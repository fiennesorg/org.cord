package org.cord.node.appcc;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.node.cc.JsonStyleTableWrapper;
import org.cord.node.cc.StyleTableWrapper;
import org.cord.node.img.ImgMgr;
import org.cord.node.img.ImgSearch;
import org.json.JSONObject;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class ScalableSingleImageStyle
  extends JsonStyleTableWrapper
{
  public static final String NAME = "ScalableSingleImageStyle";

  public static final String JSON_MINASPECTRATIO = "minAspectRatio";
  public static final String JSON_FIXEDASPECTRATIO = "fixedAspectRatio";
  public static final String JSON_MAXASPECTRATIO = "maxAspectRatio";

  private final ImgMgr __imgMgr;

  public ScalableSingleImageStyle(ImgMgr imgMgr)
  {
    super(NAME);
    __imgMgr = Preconditions.checkNotNull(imgMgr, "imgMgr");
  }

  public PersistentRecord getInstance(String name)
      throws MirrorNoSuchRecordException
  {
    return TableStringMatcher.onlyMatch(getTable(), StyleTableWrapper.NAME, name, null);
  }

  public ImgSearch createImgSearch(TransientRecord scalableSingleImageStyle,
                                   String nodeUrl,
                                   String successUrl,
                                   String owner)
  {
    JSONObject json = scalableSingleImageStyle.comp(ScalableSingleImageStyle.JSON);
    return __imgMgr.createImgSearch(nodeUrl,
                                    successUrl,
                                    null,
                                    null,
                                    null,
                                    owner,
                                    Optional.fromNullable(json.optDoubleObj(JSON_MINASPECTRATIO,
                                                                            null)),
                                    Optional.fromNullable(json.optDoubleObj(JSON_FIXEDASPECTRATIO,
                                                                            null)),
                                    Optional.fromNullable(json.optDoubleObj(JSON_MAXASPECTRATIO,
                                                                            null)));
  }
}
