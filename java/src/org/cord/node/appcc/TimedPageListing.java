package org.cord.node.appcc;

import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.EnglishNamedImplMapField;
import org.cord.mirror.field.EnumField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.Copyables;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeMoveException;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ControlledIsPublished;
import org.cord.node.app.ControlledTitle;
import org.cord.node.app.JsonAppCc;
import org.cord.node.app.TemplatePath;
import org.cord.node.app.TemplatePaths;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.collect.ImmutableList;

public class TimedPageListing<A extends AppMgr>
  extends JsonAppCc<A>
  implements CopyableContentCompiler, MoveableContentCompiler
{
  public static final String NAME = "TimedPageListing";
  public static final String TABLENAME = NAME;
  public static final String REGIONSTYLENAME = "timedPageListing";

  /**
   * The title of the TimedPageListing linked to the Node title
   */
  public final static FieldKey<String> TITLE = ControlledTitle.TITLE();

  /**
   * Whether the TimedPageListing is published, linked to Node isPublished.
   */
  public final static FieldKey<Boolean> ISPUBLISHED = ControlledIsPublished.ISPUBLISHED();

  /**
   * The number of items shown on the TimedPageListing cover page.
   */
  public static final IntFieldKey PAGESIZE = IntFieldKey.create("pageSize");

  /**
   * The name of a single item in the TimedPageListing.
   */
  public static final ObjectFieldKey<String> ITEMNAME = StringField.createKey("itemName");

  /**
   * The plural name of items in the TimedPageListing.
   */
  public static final ObjectFieldKey<String> ITEMSNAME = StringField.createKey("itemsName");

  /**
   * Which {@link ListingStyle} this TimedPageListing is modelled by.
   */
  public static final ObjectFieldKey<ListingStyle> LISTINGSTYLE =
      EnumField.createKey("listingStyle", ListingStyle.class);

  /**
   * Does this TimedPageListing have an RSS Feed?
   */
  public static final ObjectFieldKey<Boolean> HASRSSFEED = BooleanField.createKey("hasRssFeed");

  /**
   * The title of the RSS feed.
   */
  public static final ObjectFieldKey<String> RSSTITLE = StringField.createKey("rssTitle");

  /**
   * The title of the link to subscribe to the RSS feed.
   */
  public static final ObjectFieldKey<String> RSSSUBSCRIBETITLE =
      StringField.createKey("rssSubscribeTitle");

  /**
   * The optional URL that will be used to subscribe to the RSS feed. This can be defined to provide
   * 3rd party RSS caches such as Feedburner.
   */
  public static final ObjectFieldKey<String> PUBLICRSSURL = StringField.createKey("publicRssUrl");

  public static final RecordOperationKey<RecordSource> TIMEDPAGES =
      RecordOperationKey.create("timedPages", RecordSource.class);

  /**
   * RecordOperation that defined $timedPageListing.listingTimedPages which returns the RecordSource
   * of published TimedPage items that would be shown in the default listing taking into account the
   * News versus Events mode.
   */
  public static final RecordOperationKey<RecordSource> LISTINGTIMEDPAGES =
      RecordOperationKey.create("listingTimedPages", RecordSource.class);

  public static final List<FieldKey<?>> COPYABLES = ImmutableList.<FieldKey<?>> builder()
                                                                 .add(PAGESIZE,
                                                                      ITEMNAME,
                                                                      ITEMSNAME,
                                                                      LISTINGSTYLE,
                                                                      HASRSSFEED,
                                                                      RSSTITLE,
                                                                      PUBLICRSSURL)
                                                                 .build();

  /**
   * RecordOperation that returns null if hasRssFeed is false, or publicRssUrl if defined or
   * $timedPageListing.url/rss.xml.
   */
  public static final RecordOperationKey<String> RSSURL =
      RecordOperationKey.create("rssUrl", String.class);

  private EnglishNamedImplMapField _listingTemplatePath;

  public EnglishNamedImplMapField getListingTemplatePath()
  {
    return _listingTemplatePath;
  }

  private EnglishNamedImplMapField _itemTemplatePath;

  public EnglishNamedImplMapField getItemTemplatePath()
  {
    return _itemTemplatePath;
  }

  public TimedPageListing(A appMgr)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/TimedPageListing/TimedPageListing.view.wm.html",
                                     "appcc/TimedPageListing/TimedPageListing.edit.wm.html"),
          REGIONSTYLENAME,
          null);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances,
                      PureJsonObjectField data)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new ControlledTitle<A>(this));
    addSubCc(instances, new ControlledIsPublished<A>(this));
    addFieldSubCC(instances, new IntField(PAGESIZE, "Items per page (0 for unlimited)", 6));
    addFieldSubCC(instances,
                  new StringField(ITEMNAME,
                                  "Singular item name",
                                  StringField.LENGTH_VARCHAR,
                                  "item"));
    addFieldSubCC(instances,
                  new StringField(ITEMSNAME,
                                  "Plural items name",
                                  StringField.LENGTH_VARCHAR,
                                  "items"));
    addFieldSubCC(instances,
                  new EnumField<ListingStyle>(LISTINGSTYLE,
                                              "Type",
                                              ListingStyle.class,
                                              FieldValueSuppliers.ofDefined(ListingStyle.NEWS)));
    addFieldSubCC(instances, new BooleanField(HASRSSFEED, "Has RSS feed?", Boolean.TRUE) {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    addFieldSubCC(instances, new StringField(RSSTITLE, "Title of RSS Feed"));
    addFieldSubCC(instances, new StringField(RSSSUBSCRIBETITLE, "Title of subscribe link"));
    instances.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord timedPageListing,
                             Gettable params)
          throws MirrorException
      {
        String title = timedPageListing.comp(NODE).opt(Node.TITLE);
        timedPageListing.setField(RSSTITLE, title);
        timedPageListing.setField(RSSSUBSCRIBETITLE, title);
      }
    });
    addFieldSubCC(instances,
                  new StringField(PUBLICRSSURL,
                                  "Public RSS URL (eg google feedreader)",
                                  StringField.LENGTH_VARCHAR,
                                  null,
                                  ""));
    instances.addRecordOperation(new SimpleRecordOperation<String>(RSSURL) {
      @Override
      public String getOperation(TransientRecord timedPageListing,
                                 Viewpoint viewpoint)
      {
        if (!timedPageListing.is(HASRSSFEED)) {
          return null;
        }
        String publicRssUrl = timedPageListing.opt(PUBLICRSSURL);
        return publicRssUrl != null ? publicRssUrl : timedPageListing.opt(URL) + "/rss.xml";
      }
    });
    instances.addRecordOperation(new SimpleRecordOperation<RecordSource>(LISTINGTIMEDPAGES) {
      @Override
      public RecordSource getOperation(TransientRecord timedPageListing,
                                       Viewpoint viewpoint)
      {
        RecordSource timedPages = timedPageListing.opt(TIMEDPAGES, viewpoint);
        timedPages = new SingleRecordSourceFilter(timedPages, null) {
          @Override
          protected boolean accepts(PersistentRecord timedPage)
          {
            return timedPage.is(ISPUBLISHED);
          }
        };
        if (timedPageListing.opt(LISTINGSTYLE).isForwardLooking()) {
          timedPages = timedPages.opt(TimedPage.FUTUREEVENTS, viewpoint);
        } else {
          timedPages =
              RecordSources.applyRecordOrdering(timedPages, TimedPage.ORDERING_STARTTIME_DESC);
        }
        return timedPages;
      }
    });
    addSubCc(instances,
             TemplatePath.createJsonField(this,
                                          DATA,
                                          "Grouping Template",
                                          RecordOperationKey.create("groupingTemplate",
                                                                    TemplatePath.class),
                                          false,
                                          getNodeMgr().getGettable(),
                                          "TimedPageListing.groupingTemplate"));
    addSubCc(instances,
             TemplatePath.createJsonField(this,
                                          DATA,
                                          "Listing Template",
                                          RecordOperationKey.create("listingTemplate",
                                                                    TemplatePath.class),
                                          false,
                                          getNodeMgr().getGettable(),
                                          "TimedPageListing.listingTemplate"));
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getCompInstance(fromNode, null);
    Copyables.copy(instance, settable, COPYABLES);
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
  }

  public enum ListingStyle {
    NEWS("News/Blog", "appcc/TimedPageListing/TimedPageListing.NEWS.view.wm.html", false),
    EVENTS("Events", "appcc/TimedPageListing/TimedPageListing.EVENTS.view.wm.html", true);

    private final String __viewTemplatePath;
    private final String __toString;
    private final boolean __isForwardLooking;

    private ListingStyle(String toString,
                         String viewTemplatePath,
                         boolean isForwardLooking)
    {
      __toString = toString;
      __viewTemplatePath = viewTemplatePath;
      __isForwardLooking = isForwardLooking;
    }

    public String getViewTemplatePath()
    {
      return __viewTemplatePath;
    }

    public boolean isForwardLooking()
    {
      return __isForwardLooking;
    }

    @Override
    public String toString()
    {
      return __toString;
    }
  }
}
