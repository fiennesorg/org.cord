package org.cord.node.appcc;

import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.Copyables;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeMoveException;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ControlledIsPublished;
import org.cord.node.app.ControlledTitle;
import org.cord.node.app.FieldSubCC;
import org.cord.node.app.JsonAppCc;
import org.cord.node.app.TemplatePath;
import org.cord.node.app.TemplatePaths;
import org.cord.util.Casters;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.collect.ImmutableList;

public class ImagePageListing<A extends AppMgr>
  extends JsonAppCc<A>
  implements CopyableContentCompiler, MoveableContentCompiler
{
  public static final String NAME = "ImagePageListing";
  public static final String TABLENAME = NAME;
  public static final String REGIONSTYLENAME = "imagePageListing";

  public final static FieldKey<String> TITLE = ControlledTitle.TITLE();
  public final static FieldKey<Boolean> ISPUBLISHED = ControlledIsPublished.ISPUBLISHED();
  public static final IntFieldKey LISTINGROWS = IntFieldKey.create("listingRows");
  public static final ObjectFieldKey<Boolean> HASRSSFEED = BooleanField.createKey("hasRssFeed");
  public static final ObjectFieldKey<String> RSSTITLE = StringField.createKey("rssTitle");
  public static final ObjectFieldKey<String> PUBLICRSSURL = StringField.createKey("publicRssUrl");
  public static final ObjectFieldKey<String> ITEMNAME = StringField.createKey("itemName");
  public static final ObjectFieldKey<String> ITEMSNAME = StringField.createKey("itemsName");
  public static final List<FieldKey<?>> COPYABLES =
      ImmutableList.<FieldKey<?>> builder()
                   .add(LISTINGROWS, HASRSSFEED, RSSTITLE, PUBLICRSSURL)
                   .build();

  /**
   * RecordOperation that takes the number of columns in the listing being used and returns the
   * number of elements that should be included in the paginator. If the system is setup for no
   * pagination then this will return Integer.MAX_VALUE which is effectively the same thing.
   */
  public static final MonoRecordOperationKey<Integer, Integer> PAGINATORSIZE =
      MonoRecordOperationKey.create("paginatorSize", Integer.class, Integer.class);

  /**
   * RecordOperation that returns null if hasRssFeed is false, or publicRssUrl if defined or
   * $imagePageListing.url/rss.xml.
   */
  public static final RecordOperationKey<String> RSSURL =
      RecordOperationKey.create("rssUrl", String.class);

  public ImagePageListing(A appMgr)
  {
    super(appMgr,
          NAME,
          new TemplatePaths.Explicit("appcc/ImagePageListing.view.wm.html",
                                     "appcc/ImagePageListing.edit.wm.html"),
          REGIONSTYLENAME,
          null);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances,
                      PureJsonObjectField data)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new ControlledTitle<A>(this));
    addSubCc(instances, new ControlledIsPublished<A>(this));
    addSubCc(instances,
             new FieldSubCC<A, Integer>(this,
                                        new IntField(LISTINGROWS,
                                                     "Rows per page (0 is unlimited)",
                                                     3)));
    instances.addRecordOperation(new MonoRecordOperation<Integer, Integer>(PAGINATORSIZE,
                                                                           Casters.TOINT) {
      @Override
      public Integer calculate(TransientRecord record,
                               Viewpoint viewpoint,
                               Integer cols)
      {
        int listingRows = record.compInt(LISTINGROWS);
        return Integer.valueOf(listingRows < 1 ? Integer.MAX_VALUE : listingRows * cols.intValue());
      }
    });
    addSubCc(instances,
             new FieldSubCC<A, Boolean>(this,
                                        new BooleanField(HASRSSFEED,
                                                         "Has RSS feed of Image Pages?",
                                                         Boolean.FALSE) {
                                          @Override
                                          public boolean shouldReloadOnChange()
                                          {
                                            return true;
                                          }
                                        }));
    addSubCc(instances,
             new FieldSubCC<A, String>(this, new StringField(RSSTITLE, "Title of RSS Feed")));
    instances.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord imagePageListing,
                             Gettable params)
          throws MirrorException
      {
        imagePageListing.setField(RSSTITLE, imagePageListing.comp(NODE).opt(Node.TITLE));
      }
    });
    addSubCc(instances,
             new FieldSubCC<A, String>(this,
                                       new StringField(PUBLICRSSURL,
                                                       "Public RSS URL (eg google feedreader)",
                                                       StringField.LENGTH_VARCHAR,
                                                       null,
                                                       "")));
    instances.addRecordOperation(new SimpleRecordOperation<String>(RSSURL) {
      @Override
      public String getOperation(TransientRecord imagePageListing,
                                 Viewpoint viewpoint)
      {
        if (!imagePageListing.is(HASRSSFEED)) {
          return null;
        }
        String publicRssUrl = imagePageListing.opt(PUBLICRSSURL);
        return publicRssUrl != null ? publicRssUrl : imagePageListing.opt(URL) + "/rss.xml";
      }
    });
    addFieldSubCC(instances,
                  new StringField(ITEMNAME,
                                  "Singular item name",
                                  StringField.LENGTH_VARCHAR,
                                  "picture"));
    addFieldSubCC(instances,
                  new StringField(ITEMSNAME,
                                  "Plural items name",
                                  StringField.LENGTH_VARCHAR,
                                  "pictures"));
    addSubCc(instances,
             TemplatePath.createJsonField(this,
                                          DATA,
                                          "Listing Template",
                                          RecordOperationKey.create("listingTemplate",
                                                                    TemplatePath.class),
                                          false,
                                          getNodeMgr().getGettable(),
                                          "ImagePageListing.listingTemplate"));
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getCompInstance(fromNode, null);
    Copyables.copy(instance, settable, COPYABLES);
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
  }
}
