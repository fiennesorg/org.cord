package org.cord.node;

import org.cord.util.NamedImpl;

public class NodeRole
  extends NamedImpl
{
  public final static String NODE_ROLE_SUMMARY_NAME = "summary";

  public final static NodeRole NODE_ROLE_SUMMARY =
      new NodeRole(NODE_ROLE_SUMMARY_NAME, false, true);

  public final static String NODE_ROLE_FULL_NAME = "full";

  public final static NodeRole NODE_ROLE_FULL = new NodeRole(NODE_ROLE_FULL_NAME, true, false);

  public final static String NODE_ROLE_ALL_NAME = "all";

  public final static NodeRole NODE_ROLE_ALL = new NodeRole(NODE_ROLE_ALL_NAME, true, true);

  private final boolean _isFull;

  private final boolean _isSummary;

  private NodeRole(String name,
                   boolean isFull,
                   boolean isSummary)
  {
    super(name);
    _isFull = isFull;
    _isSummary = isSummary;
  }

  public final boolean isFull()
  {
    return _isFull;
  }

  public final boolean isSummary()
  {
    return _isSummary;
  }
}
