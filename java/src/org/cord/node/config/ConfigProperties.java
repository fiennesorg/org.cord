package org.cord.node.config;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.util.AbstractGettable;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

/**
 * <p>
 * ConfigProperties models a tree of properties, allowing for properties nearer the tree's root to
 * be inherited by nodes lower down. For example, given the properties:
 * </p>
 * 
 * <pre>
 * org.fiennes.node.connector = mysql
 * org.fiennes.node.user = david
 * org.fiennes.node.db.user = henry
 * org.fiennes.node.db.password = iamiam
 * </pre>
 * <p>
 * ...{@link #get(String)} will have the following results:
 * </p>
 * <table>
 * <tr>
 * <th>{@link #get(String)} parameter</th>
 * <th>Result</th>
 * </tr>
 * <tr>
 * <td>org.fiennes.node.db.connector</td>
 * <td>mysql</td>
 * </tr>
 * <tr>
 * <td>org.fiennes.node.db.user</td>
 * <td>henry</td>
 * </tr>
 * <tr>
 * <td>org.fiennes.node.db.password</td>
 * <td>iamiam</td>
 * </tr>
 * </table>
 * <p>
 * A particular node in the tree can be obtained using {@link #getTree(String)}.
 * {@link #get(String)} called with a qualified name (i.e. one with any '.' characters in it) on any
 * non-root node will search for that qualified name in all the parent nodes. For example, given the
 * properties:
 * </p>
 * 
 * <pre>
 * org.fiennes.node.db.connector = mysql
 * org.fiennes.node.template.loader = file
 * org.fiennes.node.development.db.user = terry
 * org.fiennes.node.development.db.connector = mysql-debug
 * org.fiennes.node.production.db.user = henry
 * </pre>
 * <p>
 * ...calling {@link #get(String)} on <code>getTree("org.fiennes.node.production")</code> will have
 * the following results:
 * </p>
 * <table>
 * <tr>
 * <th>{@link #get(String)} parameter</th>
 * <th>Result</th>
 * </tr>
 * <tr>
 * <td>db.connector</td>
 * <td>mysql</td>
 * </tr>
 * <tr>
 * <td>connector</td>
 * <td><em>null</em></td>
 * </tr>
 * <tr>
 * <td>template.loader</td>
 * <td>file</td>
 * </tr>
 * </table>
 * <p>
 * A Gettable interface can be acquired using {@link #getGettable()}, and a Properties class via
 * {@link #getProperties()}.
 * </p>
 * <p>
 * Other properties can be referred to by using a simple reference syntax: see {@link #get(String)}
 * </p>
 * <h4>Usage</h4>
 * <p>
 * The intent of the inheritance mechanism is to allow for sets of operational defaults to be
 * specified, while allowing specific configurations that vary some of them to be set up. The second
 * example above specifies default settings for db.connector and template.loader. Calling
 * <code>getTree("org.fiennes.node.production")</code> will return a tree that overrides some of
 * those defaults for production usage.
 * </p>
 */

public class ConfigProperties
{
  // ==========================================================================
  // public

  // --------------------------------------------------------------------------
  // CREATORS

  public ConfigProperties()
  {
    __name = "";
    __parent = null;
  }

  // --------------------------------------------------------------------------
  // ACCESSORS

  /**
   * Get the value of key, after expanding any references of the form ${ref} in value. These are
   * resolved as if get("ref") was called on this ConfigProperties instance. The resulting value is
   * checked for references, and if any are found, the process continues until there are no more
   * references, or until {@link #MAX_EXPANSIONS} have occurred. In the latter case, it is assumed
   * that a recursive reference has been made and a LogicException is thrown. Default values can be
   * specified using ${ref:-default}. An absolute path can be specified by preceding it with "::"
   * e.g. ${::ref}. ${::tree::ref} is equivalent to calling getRoot().getTree("tree").get("ref").
   */
  public String get(String key)
  {
    return get(key, 0);
  }

  /**
   * Get the youngest existing tree specified by the key. For example, if key is foo.bar.baz.qux but
   * foo.bar.baz does not exist, then this will return the tree at foo.bar
   */
  public ConfigProperties getTree(String key)
  {
    ConfigProperties props = this;

    String[] keys = key.split("\\.");

    for (int i = 0; i != keys.length && props.__props.get(keys[i]) != null; ++i) {
      props = props.__props.get(keys[i]);
    }

    debug("ConfigProperties(%s).getTree(%s) -> %s\n",
          getQualifiedName(),
          key,
          props.getQualifiedName());

    return props;
  }

  /**
   * Create the specified tree and return it; different to getTree("a.b.c.") in that after calling
   * this a.b.c is guaranteed to exist.
   */
  public ConfigProperties createTree(String key)
  {
    ConfigProperties props = this;

    String[] keys = key.split("\\.");

    for (String k : keys) {
      ConfigProperties next = props.__props.get(k);
      if (next == null) {
        next = new ConfigProperties(k, props);
        props.__props.put(k, next);
      }
      props = next;
    }

    return props;
  }

  @Override
  public String toString()
  {
    StringWriter sw = new StringWriter();
    PrintWriter out = new PrintWriter(sw);
    write(out, this);
    return sw.toString();
  }

  /** Return the tree at this ConfigProperties as a Properties class. */
  public Properties getProperties()
  {
    StringReader sr = new StringReader(toString());
    Properties result = null;
    if (__parent != null) {
      result = new Properties(__parent.getProperties());
    } else {
      result = new Properties();
    }

    try {
      result.load(sr);
    } catch (IOException ex) {
      throw new LogicException("Unexpected serialisation failure", ex);
    }
    return result;
  }

  /**
   * Provide a Gettable interface onto this instance: {@link Gettable#get(Object)} is translated
   * into a call to {@link #get(String)}.
   */
  public Gettable getGettable()
  {
    return new _Gettable(this);
  }

  // --------------------------------------------------------------------------
  // MANIPULATORS

  /** Assign the value to key. */
  public void put(String key,
                  String value,
                  String source)
  {
    String[] keys = key.split("\\.");
    ConfigProperties props = this;

    for (int i = 0; i != keys.length && props != null; ++i) {
      if (i == keys.length - 1) {
        props.__vals.put(keys[i], new Val(props, getQualifiedName(key), value, source));
      } else {
        ConfigProperties child = props.__props.get(keys[i]);
        if (child == null) {
          child = new ConfigProperties(keys[i], props);
          props.__props.put(keys[i], child);
        }
        props = child;
      }
    }
  }

  public static final String UNKNOWN_SOURCE = "<unknown>";

  /** Calls put(key, value, UNKNOWN_SOURCE) */
  public void put(String key,
                  String value)
  {
    put(key, value, UNKNOWN_SOURCE);
  }

  /** Merge all the keys in src into this tree */
  public void merge(ConfigProperties src)
  {
    if (src == this || isChildOf(src))
      return;

    __vals.putAll(src.__vals);
    for (Map.Entry<String, ConfigProperties> entry : src.__props.entrySet()) {
      ConfigProperties props = __props.get(entry.getKey());

      if (props == null) {
        props = new ConfigProperties(entry.getKey(), this);
        __props.put(entry.getKey(), props);
      }

      props.merge(entry.getValue());
    }
  }

  /**
   * Link tgt into this tree using key. The parent of the target is unchanged. This allows for
   * certain trees to be made available at well-known places in the hierarchy, for example
   * config.deployment could be made to point to the result of calling getTree(machine + app + user)
   */
  public void link(String key,
                   ConfigProperties tgt)
  {
    ConfigProperties props = this;

    String[] keys = key.split("\\.");

    for (int i = 0; i != keys.length - 1; ++i) {
      String k = keys[i];
      ConfigProperties next = props.__props.get(k);
      if (next == null) {
        next = new ConfigProperties(k, props);
        props.__props.put(k, next);
      }
      props = next;
    }

    props.__props.put(keys[keys.length - 1], tgt);
  }

  /**
   * Enables debugging output on the specified PrintWriter; if null is passed, then disables
   * debugging output.
   */
  public void setDebug(PrintWriter out)
  {
    _debug = out;
  }

  // ==========================================================================
  // private

  private final String __name;
  private final ConfigProperties __parent;
  private PrintWriter _debug = null;

  private static class Val
  {
    final ConfigProperties owner;
    final String value;
    final String source;
    final String key;

    public Val(ConfigProperties owner_,
               String key_,
               String value_,
               String source_)
    {
      owner = owner_;
      key = key_;
      value = value_;
      source = source_;
    }
  }

  // Map of key -> Val
  private final HashMap<String, Val> __vals = new HashMap<String, Val>();

  // Map of key -> child properties
  private final HashMap<String, ConfigProperties> __props = new HashMap<String, ConfigProperties>();

  // --------------------------------------------------------------------------
  // CREATORS

  private ConfigProperties(String name,
                           ConfigProperties parent)
  {
    __name = name;
    __parent = parent;
  }

  // --------------------------------------------------------------------------
  // ACCESSORS

  private class _Gettable
    extends AbstractGettable
  {
    private final ConfigProperties __props;

    public _Gettable(ConfigProperties props)
    {
      __props = props;
    }

    @Override
    public Object get(Object key)
    {
      return __props.get(key.toString());
    }

    @Override
    public String toString()
    {
      return "_Gettable(" + ConfigProperties.this + ")";
    }
  }

  private PrintWriter writeQualifiedName(PrintWriter out,
                                         ConfigProperties root)
  {
    if (this != root) {
      if (__parent != null && __parent != root) {
        __parent.writeQualifiedName(out, root).append('.');
      }
      out.append(__name);
    }
    return out;
  }

  private ConfigProperties getRoot()
  {
    return __parent == null ? this : __parent.getRoot();
  }

  private String getQualifiedName()
  {
    StringWriter sw = new StringWriter();
    writeQualifiedName(new PrintWriter(sw), getRoot());
    return sw.toString();
  }

  private String getQualifiedName(String key)
  {
    StringWriter sw = new StringWriter();
    ConfigProperties root = getRoot();
    if (this != root) {
      writeQualifiedName(new PrintWriter(sw), root).append('.');
    }
    sw.append(key);
    return sw.toString();
  }

  private PrintWriter write(PrintWriter out,
                            ConfigProperties root)
  {
    String name = "";
    if (this != root) {
      StringWriter sw = new StringWriter();
      writeQualifiedName(new PrintWriter(sw), root).append('.');
      name = sw.toString();
    }

    for (Iterator<Map.Entry<String, Val>> it = __vals.entrySet().iterator(); it.hasNext();) {
      Map.Entry<String, Val> entry = it.next();
      out.printf("# source = %s\n%s%s = %s\n",
                 entry.getValue().source,
                 name,
                 entry.getKey(),
                 entry.getValue().value);
    }

    for (Iterator<ConfigProperties> it = __props.values().iterator(); it.hasNext();) {
      it.next().write(out, root);
    }

    return out;
  }

  /**
   * Perform a search for the qualified name represented by keys, first in this ConfigProperties and
   * then in each parent up the chain. If the key is found, returns [actual-key, value, source]
   */
  private Val getQualified(String[] keys)
  {
    Val result = null;
    ConfigProperties props = this;

    // Search in our children
    for (int i = 0; i != keys.length && props != null; ++i) {
      if (i == keys.length - 1) {
        result = props.__vals.get(keys[i]);
      } else {
        props = props.__props.get(keys[i]);
      }
    }

    // Search the parent chain
    if (result == null && __parent != null) {
      result = __parent.getQualified(keys);
    }

    return result;
  }

  /**
   * Perform a search for the unqualified key, first in this ConfigProperties and then in each
   * parent up the chain.
   */
  private Val getUnqualified(String key)
  {
    Val result = __vals.get(key);
    if (result == null && __parent != null) {
      result = __parent.getUnqualified(key);
    }
    return result;
  }

  /**
   * Perform a search for the qualified name represented by keys, first in this ConfigProperties and
   * then in each parent of the leafmost property in keys.
   */
  private Val getUnqualified(String[] keys)
  {
    Val result = null;
    ConfigProperties props = this;

    // Search in our children
    for (int i = 0; i != keys.length && props != null; ++i) {
      if (i == keys.length - 1) {
        result = props.__vals.get(keys[i]);
      } else {
        props = props.__props.get(keys[i]);
      }
    }

    // If props exists and the value wasn't found in it,
    // search the parent chain of props.
    if (result == null && props != null) {
      result = props.getUnqualified(keys[keys.length - 1]);
    }

    return result;
  }

  private boolean isChildOf(ConfigProperties parent)
  {
    boolean isChild = false;

    for (ConfigProperties props = this.__parent; !isChild && props != null; props = props.__parent)
      isChild = props == parent;

    return isChild;
  }

  private void debug(String format,
                     Object... args)
  {
    if (_debug == null) {
      if (__parent != null)
        __parent.debug(format, args);
    } else {
      _debug.printf(format, args);
    }
  }

  // --------------------------------------------------------------------------
  // Property access and reference expansion

  private final static Pattern PROPERTY_RE = Pattern.compile("\\$\\{(" +
  // A reference is a sequence of
  // ::tree::tree, with the
  // first :: optional.
                                                             "(?:(?:::)?[^\\$\\{\\}:]*)"
                                                             + "(?:::[^\\$\\{\\}:]*)*"
                                                             + ")(?::-([^\\$\\{\\}]*))?\\}");

  private String expandOnce(String value,
                            int depth)
  {
    Matcher matcher = PROPERTY_RE.matcher(value);
    StringBuilder buf = new StringBuilder();

    int append = 0;
    while (matcher.find()) {
      String ref = matcher.group(1);
      String def = matcher.group(2);
      String val = get(ref, depth + 1);
      if (val != null)
        ref = val;
      else if (def != null)
        ref = def;
      else
        ref = matcher.group();

      buf.append(value.subSequence(append, matcher.start())).append(ref);
      append = matcher.end();
    }
    buf.append(value.subSequence(append, value.length()));

    return buf.toString();
  }

  private static final int MAX_EXPANSIONS = 20;

  @SuppressWarnings("serial")
  private class ExpansionException
    extends RuntimeException
  {
  }

  private String expand(String value,
                        int depth)
  {
    if (depth > MAX_EXPANSIONS)
      throw new ExpansionException();

    String input = value;
    String output = input;
    boolean expanded = false;

    do {
      input = output;
      output = expandOnce(input, depth);
      expanded = expanded || !input.equals(output);

    } while (!input.equals(output));

    if (expanded)
      debug("ConfigProperties(%s).expand(%s) -> %s\n", getQualifiedName(), value, output);

    return output;
  }

  private String get(String key,
                     int depth)
  {
    Val result = null;

    ConfigProperties props = this;

    // If there are tree components, navigate to the appropriate tree.
    String[] treeparts = key.split("::");
    if (treeparts.length > 1) {
      key = treeparts[treeparts.length - 1];
      for (int i = 0; i != treeparts.length - 1; ++i) {
        if (treeparts[i].isEmpty())
          props = getRoot();
        else {
          props = props.getTree(props.expand(treeparts[i], depth));
        }
      }
      return props.get(key, depth);
    }

    // Split the key into its separate components, then do a qualified search
    // then an unqualified one if the former fails.

    String[] keys = key.split("\\.");
    result = getQualified(keys);
    if (result == null) {
      result = getUnqualified(keys);
    }

    String val = null;

    if (result != null) {
      if (depth == 0) {
        try {
          val = result.owner.expand(result.value, depth);
        } catch (ExpansionException ex) {
          throw new LogicException(String.format("ConfigProperties(%s).get(%s) -> %s: %s: "
                                                 + "expansion limit (%d) reached; perhaps a "
                                                 + "recursive definition?",
                                                 getQualifiedName(),
                                                 key,
                                                 result.source,
                                                 result.key,
                                                 Integer.valueOf(MAX_EXPANSIONS)));
        }
      } else {
        val = result.owner.expand(result.value, depth);
      }
      debug("ConfigProperties(%s).get(%s) -> %s: %s = %s\n",
            getQualifiedName(),
            key,
            result.source,
            result.key,
            val);
    } else {
      debug("ConfigProperties(%s).get(%s) -> null\n", getQualifiedName(), key);
    }

    return val;
  }

}
