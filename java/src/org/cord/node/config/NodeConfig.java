package org.cord.node.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.cord.node.NodeServlet;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;
import org.cord.util.LogicException;
import org.cord.util.StringUtils;
import org.cord.util.XmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.webmacro.servlet.WMServlet;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;

// ============================================================================
/**
 * Class that manages all of a node-based application's Configuration.
 * <h4>Configuration sources</h4>
 * <p>
 * It does this by initialising a ConfigProperties tree with a single value of docBase set to the
 * construction parameter, then it reads the following files in the specified order in the web
 * application directory:
 * </p>
 * <ul>
 * <li>all files matching the pattern WEB-INF/node/conf/*.properties</li>
 * <li>all files matching the pattern WEB-INF/app/conf/*.properties</li>
 * </ul>
 * <p>
 * These are standard {@link java.util.Properties#load(java.io.Reader) java Properties} files. All
 * the files in WEB-INF/node/conf are read and their properties inserted in the ConfigProperties
 * tree, and then all the files in WEB-INF/app/conf are read and their properties inserted in the
 * ConfigProperties tree, overwriting any already existing properties.
 * </p>
 * <p>
 * The basename of each file is prefixed to each of its properties prior to inserting in the
 * ConfigProperties tree.
 * </p>
 * <p>
 * In addition, it reads the <em>init-param</em> settings in WEB-INF/web.xml, either by parsing the
 * file directly (in {@link #NodeConfig(File)}) or by asking the ServletConfig and the
 * ServletContext (in {@link #NodeConfig(ServletConfig)}). These properties are all placed in the
 * <em>node</em> namespace <em>This is very much intended to support legacy usage: it would be
 * preferable to avoid having to have multiple web.xml files.</em>
 * </p>
 * <p>
 * <strong>NB:</strong> This doesn't override existing mechanisms outside of org.cord.node, like the
 * {@link WMServlet} reading WEB-INF/WebMacro.properties. The safest thing to do is to use
 * <em>either</em> WEB-INF/WebMacro.properties <em>or</em>
 * WEB-INF/(node|app)/conf/webmacro.properties.
 * </p>
 * <h4>Configuration access</h4>
 * <p>
 * The properties are made available through the {@link #getGettable(String)} and
 * {@link #getProperties(String)} methods, which provide a {@link Gettable} interface and a
 * {@link Properties} class onto the properties in the specified namespace.
 * </p>
 * <p>
 * Well-known namespaces are defined in the {@link Namespace} enum, and can be used with the
 * {@link #getGettable(Namespace)} and {@link #getProperties(Namespace)} methods.
 * </p>
 * <h4>Usage</h4>
 * <p>
 * The intended usage is that node/conf will be populated by a build script with standard properties
 * for node and webmacro:
 * </p>
 * 
 * <pre>
 * node/conf/node.properties
 * node/conf/webmacro.properties
 * </pre>
 * <p>
 * The application can then override selected properties and add new ones by providing its own files
 * in app/conf:
 * </p>
 * 
 * <pre>
 * app/conf/node.properties
 * app/conf/webmacro.properties
 * app/conf/...
 * </pre>
 * <p>
 * ...where <code>node.site-conf.properties</code> defines variables that pick which tree of
 * properties under the <em>node</em> namespace to use
 * </p>
 * <p>
 * Properties specific to a particular deployment which you don't want under version control can
 * (and should) go into deploy/conf
 * </p>
 * <p>
 * See {@link ConfigProperties} for more on how the tree inheritance mechanism works.
 * </p>
 * <h4>Deployment Environments</h4>
 * <p>
 * Configurations can be provided for multiple deployment environments by placing them in sub-trees
 * under the name of the environment. For example, node.properties could contain:
 * </p>
 * 
 * <pre>
 * production.NodeDb.database = prodDb
 * development.NodeDb.database = devDb
 * </pre>
 * <p>
 * ...and prodDb would get used in production environments, whilst devDb would be used in
 * development environments
 * </p>
 * <h5>Setting the environment directly</h5>
 * <p>
 * The deployment environment is retrieved from {@link #ENVIRONMENT_KEY}; this allows unambiguous
 * one-off overriding of the production environment by setting this value in
 * deploy/conf/node.properties:
 * </p>
 * 
 * <pre>
 * environment = production
 * </pre>
 * <p>
 * (...although you should map machine names to environments using the technique below in the long
 * term).
 * </p>
 * <h5>Setting the environment based on external factors</h5>
 * <p>
 * The default configuration of node/conf/node.properties defines environment as follows:
 * </p>
 * 
 * <pre>
 * environment = ${config.environment:-development}
 * </pre>
 * <p>
 * Everything in the config property tree is set by NodeConfig itself. NodeConfig first works out
 * the deployment environment, using the machine hostname and the node application hostname in
 * {@link #getDeploymentEnvironment()}, which looks up the "environment" key in the tree
 * node.deployment.MACHINENAME.HOSTNAME.USER. This value is then copied into "config.environment".
 * You can thus set the deployment environment using a single file called
 * app/conf/node.deployment.properties, with a single key in it:
 * 
 * <pre>
 * environment = production
 * </pre>
 * <p>
 * This doesn't gain anything over setting the environment in app/conf/node.properties, however.
 * More advanced usage would use the hostname to decide which environment to use:
 * <p>
 * 
 * <pre>
 * myProductionServer.environment = production
 * myDevelopmentServer.environment = development
 * </pre>
 * <p>
 * If you're deploying multiple instances of the same application on the same machine, you will need
 * to change the application name to ensure that logging messages are annotated with the correct
 * instance, and that different databases get used as well. You can do this by creating a single
 * file app/conf/node.app.properties, with a single key in it:
 * <p>
 * 
 * <pre>
 * hostName = myApplication
 * </pre>
 * <p>
 * ...and then either using this value in node.deployment.properties with appropriate settings in
 * node.properties:
 * </p>
 * 
 * <pre>
 * # node.deployment.properties
 * myProductionServer.myApplicationA.environment = productionA
 * myProductionServer.myApplicationB.environment = productionB
 * myDevelopmentServer.environment = development
 * 
 * # node.properties
 * productionA.NodeDb.database = prodDbA
 * productionB.NodeDb.database = prodDbB
 * development.NodeDb.database = devDb
 * </pre>
 * <p>
 * ...or (more simply) just create unique names in node.properties based on the value of
 * ${app.hostName}:
 * </p>
 * 
 * <pre>
 * # node.properties
 * NodeDb.database = ${app.hostName}DB
 * </pre>
 * 
 * @see ConfigProperties
 */
// ============================================================================

public class NodeConfig
{
  // =========================================================================
  // public

  /** Convenient list of well-known namespaces. */
  public enum Namespace {
    /** Configuration namespace that NodeConfig populates */
    CONFIG("config"),
    /** Configuration namespace for the node engine */
    NODE("node"),
    /** Configuration namespace for the mirror engine */
    MIRROR("mirror"),
    /** Configuration namespace for WebMacro */
    WEBMACRO("webmacro");

    String name;

    Namespace(String name)
    {
      this.name = name;
    }
    @Override
    public String toString()
    {
      return name;
    }
  }

  public static final String UNKNOWN = "unknown";
  public static final String INITPARAM_USERNAME = "userName";
  public static final String INITPARAM_MACHINENAME = "machineName";
  public static final String INITPARAM_MACHINEADDR = "machineAddr";
  public static final String INITPARAM_DOCBASE = "docBase";
  public static final String INITPARAM_ENVIRONMENT = "environment";

  // --------------------------------------------------------------------------
  // CREATORS

  /**
   * Initialise the NodeConfig using:
   * <ul>
   * <li>docBase/WEB-INF/web.xml;</li>
   * <li>and the property files in CONF_PATHS.</li>
   * </ul>
   * <p>
   * If debug is set, then all debugging output is sent to it until after all the files have been
   * read, at which point if <code>node.log.config.level != debug</code> then debugging output is
   * switched off.
   * </p>
   * <p>
   * Sets the following properties before reading any config data:
   * </p>
   * 
   * <pre>
   * userName, config.userName = {@link #getUserName()}
   * machineName, config.machineName = {@link #getMachineName()}
   * machineAddr, config.machineAddr = {@link #getMachineAddr()}
   * environment, config.environment = {@link #getDeploymentEnvironment()}
   * docBase, config.docBase = [the filesystem path represented by docBase]
   * </pre>
   * 
   * ...and sets the following ones after reading all config data:
   * 
   * <pre>
   * hostName, node.hostName = {@link #getHostName()}
   * </pre>
   * 
   * It also links the tree used by {@link #getDeploymentEnvironment()} into the config.deployment
   * namespace.
   */
  public NodeConfig(File docBase,
                    PrintWriter debug)
  {
    __docBase = docBase.getAbsolutePath();
    preinit(__docBase, debug);
    try {
      for (String path : CONF_PATHS) {
        File[] confFiles = (new File(docBase, path)).listFiles();
        if (confFiles != null) {
          Arrays.sort(confFiles);
          for (File confFile : confFiles) {
            String namespace = confFileNamespace(confFile.getPath());
            if (namespace != null) {
              loadPropertyFile(namespace,
                               confFile.toURI().toURL(),
                               new File(path, confFile.getName()).getPath());
            }
          }
        }
      }
      loadWebXml(new File(docBase, "WEB-INF/web.xml"), "WEB-INF/web.xml");
    } catch (Exception e) {
      throw new LogicException("NodeConfig(File)", e);
    }
    postinit();
  }

  public NodeConfig(File docBase)
  {
    this(docBase,
         null);
  }

  /**
   * Initialise the NodeConfig using confFiles, and nothing else. This is here to allow the
   * node-starter project to support legacy node apps that don't have any {@link #CONF_PATHS}
   */
  public NodeConfig(File docBase,
                    PrintWriter debug,
                    File[] confFiles)
  {
    __docBase = docBase.getAbsolutePath();
    preinit(__docBase, debug);
    try {
      for (File confFile : confFiles) {
        String namespace = confFileNamespace(confFile.getPath());
        if (namespace != null) {
          loadPropertyFile(namespace, confFile.toURI().toURL(), confFile.getCanonicalPath());
        }
      }
    } catch (Exception e) {
      throw new LogicException("NodeConfig(File)", e);
    }
    postinit();
  }

  /**
   * Initialise the NodeConfig using:
   * <ul>
   * <li>The {@link ServletConfig};</li>
   * <li>the {@link ServletContext};</li>
   * <li>and the property files in #CONF_PATHS.</li>
   * </ul>
   * <p>
   * If debug is set, then all debugging output is sent to it until after all the files have been
   * read, at which point if <code>node.log.config.level != debug</code> then debugging output is
   * switched off.
   * </p>
   * <p>
   * Sets the following properties before reading any config data:
   * </p>
   * 
   * <pre>
   * userName, config.userName = {@link #getUserName()}
   * machineName, config.machineName = {@link #getMachineName()}
   * machineAddr, config.machineAddr = {@link #getMachineAddr()}
   * environment, config.environment = {@link #getDeploymentEnvironment()}
   * config, config.docBase = [the filesystem path represented by docBase]
   * </pre>
   * 
   * ...and sets the following ones after reading all config data:
   * 
   * <pre>
   * hostName, node.hostName = {@link #getHostName()}
   * </pre>
   * 
   * It also links the tree used by {@link #getDeploymentEnvironment()} into the config.deployment
   * namespace.
   */
  public NodeConfig(ServletConfig config,
                    PrintWriter debug)
  {
    __docBase = config.getServletContext().getRealPath("");
    preinit(__docBase, debug);
    try {
      loadPropertyFiles(config.getServletContext());
      loadServletParams(config);
      loadServletParams(config.getServletContext());
    } catch (Exception e) {
      throw new LogicException("NodeConfig(ServletConfig)", e);
    }
    postinit();
  }

  /**
   * Does for FilterConfig what {@link #NodeConfig(ServletConfig, PrintWriter)} does for
   * ServletConfig
   */
  public NodeConfig(FilterConfig config,
                    PrintWriter debug)
  {
    __docBase = config.getServletContext().getRealPath("");
    preinit(__docBase, debug);
    try {
      loadPropertyFiles(config.getServletContext());
      loadServletParams(config);
      loadServletParams(config.getServletContext());
    } catch (Exception e) {
      throw new LogicException("NodeConfig(FilterConfig)", e);
    }
    postinit();
  }

  public NodeConfig(ServletConfig config)
  {
    this(config,
         null);
  }

  // --------------------------------------------------------------------------
  // ACCESSORS

  /** Return the root of the web application directory */
  public String getDocBase()
  {
    return __docBase;
  }

  /**
   * <p>
   * Return the deployment environment; defaults to the value of {@link #DEFAULT_ENVIRONMENT} if no
   * value can be found using the method below.
   * </p>
   * <p>
   * The environment is found by getting the key "environment" from the configuration tree
   * "node.deployment.MACHINENAME.USERNAME.HOSTNAME", where MACHINENAME, USERNAME and HOSTNAME are
   * the respective return values of {@link #getMachineName()}, {@link #getUserName()} and
   * {@link #getHostName()}.
   * </p>
   */
  public String getDeploymentEnvironment()
  {
    String depenv = getDeploymentTree().get("environment");

    return depenv == null ? DEFAULT_ENVIRONMENT : depenv;
  }

  /**
   * Get the hostname of this computer, defaulting to {@link #UNKNOWN} if it can't be discovered.
   */
  public static String getMachineName()
  {
    String result = UNKNOWN;
    try {
      result = InetAddress.getLocalHost().getHostName();
      int dot = result.indexOf('.');
      if (dot != -1)
        result = result.substring(0, dot);
    } catch (UnknownHostException ex) {
      // Ignore;
    }
    return result;
  }

  /**
   * Get the IP Address of this computer, defaulting to {@link InetAddress#getByName(String)} if it
   * can't be discovered.
   */
  public static String getMachineAddr()
  {
    InetAddress result = null;

    try {
      List<NetworkInterface> interfaces = new ArrayList<NetworkInterface>();
      for (Enumeration<NetworkInterface> it =
          NetworkInterface.getNetworkInterfaces(); it.hasMoreElements() && result == null;) {
        NetworkInterface intf = it.nextElement();
        if (intf.isUp() && intf.getName().startsWith("e")) {
          interfaces.add(intf);
        }
      }

      Collections.sort(interfaces, new Comparator<NetworkInterface>() {
        @Override
        public int compare(NetworkInterface a,
                           NetworkInterface b)
        {
          return a.getName().compareTo(b.getName());
        }
      });

      for (NetworkInterface intf : interfaces) {
        for (Enumeration<InetAddress> ait = intf.getInetAddresses(); ait.hasMoreElements()
                                                                     && result == null;) {
          InetAddress addr = ait.nextElement();
          if (addr instanceof Inet4Address && !addr.isLoopbackAddress()) {
            result = addr;
          }
        }
      }
    } catch (SocketException ex) {
      throw new LogicException("Couldn't get machine address", ex);
    }

    if (result == null) {
      try {
        result = InetAddress.getByName(null);
      } catch (UnknownHostException ex) {
        throw new LogicException("Couldn't get loopback address", ex);
      }
    }

    return result.getHostAddress();
  }

  /**
   * <p>
   * Get the name of this application; defaults to the value of {@link #UNKNOWN} if no value can be
   * found using the method below.
   * </p>
   * <p>
   * The name is found by using the first found of the following keys in the "node" namespace:
   * </p>
   * <ul>
   * <li>app.hostName</li>
   * <li>hostName</li>
   * </ul>
   */
  public String getHostName()
  {
    ConfigProperties nodeProps = __props.getTree(Namespace.NODE.toString());
    String result = nodeProps.getTree("app").get(NodeServlet.INITPARAM_HOSTNAME);
    return result == null ? UNKNOWN : result;
  }

  /**
   * Get the name of the user who owns this application. This is done by running the first
   * occurrence of the executable shell script {@link #OWNER_SCRIPT} found in CONF_PATHS
   */
  public String getUserName()
  {
    String result = null;

    for (String path : CONF_PATHS) {
      File owner = new File(new File(__docBase, path), OWNER_SCRIPT);
      if (owner.exists()) {
        if (owner.canExecute()) {
          try {
            result = IoUtil.exec(owner.getPath(), null, 0, true);
            break;
          } catch (IOException ex) {
            throw new LogicException(String.format("Couldn't run \"%s\"", owner.getAbsolutePath()),
                                     ex);
          }
        } else {
          throw new LogicException(String.format("Couldn't run \"%s\"", owner.getAbsolutePath()));
        }
      }
    }

    return result == null ? UNKNOWN : result.trim();
  }

  /**
   * Return a Gettable view of the specified namespace.
   * 
   * @see #getGettable(String)
   */
  public Gettable getGettable(Namespace namespace)
  {
    return getGettable(namespace.name);
  }

  /**
   * <p>
   * Return a Gettable view of the specified namespace with the return value of get(
   * {@link #ENVIRONMENT_KEY}) appended. For example, getGettable("node") will return the namespace
   * called "node." + get({@link #ENVIRONMENT_KEY})
   * </p>
   * <p>
   * If the namespace doesn't exist, then the longest existing parent namespace is returned: for
   * example, if you ask for "node.foo.bar.baz" and only "node.foo" exists, then this will return a
   * Gettable rooted at "node.foo". Equally, if the deployment environment is "production", but
   * "node.production" doesn't exist, then "node" will be returned.
   * </p>
   */
  public Gettable getGettable(String namespace)
  {
    return getTree(namespace, __props.get(ENVIRONMENT_KEY)).getGettable();
  }

  /**
   * Return a Properties view of the specified namespace.
   * 
   * @see #getProperties(String)
   */
  public Properties getProperties(Namespace namespace)
  {
    return getProperties(namespace.name);
  }

  /**
   * Identical to {@link #getGettable(String)}, except that it returns a Properties view instead of
   * a Gettable()
   */
  public Properties getProperties(String namespace)
  {
    Properties result = null;
    ConfigProperties props = getTree(namespace, __props.get(ENVIRONMENT_KEY));
    if (props != null) {
      result = props.getProperties();
    } else {
      result = new Properties();
    }
    return result;
  }

  // =========================================================================
  // private

  /**
   * The configuration paths that will be read looking for property files. The ordering is in order
   * of evaluation so the later entries will take priority over the earlier ones.
   */
  public final static ImmutableList<String> CONF_PATHS = ImmutableList.of("/WEB-INF/node/conf",
                                                                          "/WEB-INF/app-2/conf",
                                                                          "/WEB-INF/app-1/conf",
                                                                          "/WEB-INF/app/conf",
                                                                          "/WEB-INF/deploy/conf");
  public final static String DEFAULT_ENVIRONMENT = "development";
  public final static String ENVIRONMENT_KEY = "node.environment";
  private final static String OWNER_SCRIPT = "owner";

  private final static String CONF_SUFFIX = ".properties";

  private final static String confFileNamespace(String confFile)
  {
    String namespace = null;
    if (confFile.endsWith(CONF_SUFFIX)) {
      int slash = confFile.lastIndexOf('/');
      namespace = confFile.substring(slash != -1 ? slash + 1 : 0, confFile.lastIndexOf('.'));
    }
    return namespace;
  }

  private final String __docBase;
  private final ConfigProperties __props = new ConfigProperties();

  // MANIPULATORS
  private void loadWebXml(File webXml,
                          String source)
      throws SAXException, IOException, ParserConfigurationException
  {
    if (!webXml.exists())
      return;

    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setValidating(false);
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    Document document = documentBuilder.parse(webXml);
    NodeList initParams = document.getElementsByTagName("init-param");
    for (int i = 0; i < initParams.getLength(); ++i) {
      Element initParam = (Element) initParams.item(i);
      String paramName = XmlUtils.getTextElement(initParam, "param-name");
      String paramValue = XmlUtils.getTextElement(initParam, "param-value");
      addProp(Namespace.NODE.name, paramName, paramValue, source);
    }
  }

  @SuppressWarnings("serial")
  private void loadPropertyFile(String namespace,
                                URL url,
                                String source)
      throws Exception
  {
    final ArrayList<Object> keys = new ArrayList<Object>();
    // Evil hack to make sure that once we've used Properties.load to parse the
    // file we then pull the properties out again in the same order they were
    // read; without this, the ${ref} syntax won't work reliably.
    Properties props = new Properties() {
      @Override
      public synchronized Object put(Object key,
                                     Object value)
      {
        keys.add(key);
        return super.put(key, value);
      }

      @Override
      // If the implementation changes, it's better to fail fast rather than
      // suffer the weird problems that reading and expanding values in a
      // random order will engender.
      public synchronized void putAll(Map<? extends Object, ? extends Object> t)
      {
        throw new LogicException("The implementation of " + "java.util.Properties.load() no longer "
                                 + "behaves as expected by NodeConfig");
      }
    };

    Reader urlStream = new InputStreamReader(url.openStream(), Charsets.ISO_8859_1);
    try {
      props.load(urlStream);
      for (Object key : keys) {
        addProp(namespace, key.toString(), props.get(key).toString(), source);
      }
    } finally {
      urlStream.close();
    }
  }

  private void loadPropertyFiles(ServletContext context)
      throws Exception
  {
    for (String path : CONF_PATHS) {
      String[] respaths = null;
      Set<?> respathsSet = context.getResourcePaths(path);
      if (respathsSet != null) {
        respaths = respathsSet.toArray(new String[respathsSet.size()]);
        Arrays.sort(respaths);
        for (String confFile : respaths) {
          String namespace = confFileNamespace(confFile);
          if (namespace != null) {
            loadPropertyFile(namespace, context.getResource(confFile), confFile);
          }
        }
      }
    }
  }

  private void addProp(String namespace,
                       String key,
                       String value,
                       String source)
  {
    __props.put(String.format("%s.%s", namespace, key), value, source);
  }

  private void loadServletParams(FilterConfig config)
  {
    for (Enumeration<?> it = config.getInitParameterNames(); it.hasMoreElements();) {
      String key = StringUtils.toString(it.nextElement());
      addProp(Namespace.NODE.name, key, config.getInitParameter(key), "filter-config");
    }
  }

  private void loadServletParams(ServletConfig config)
  {
    for (Enumeration<?> it = config.getInitParameterNames(); it.hasMoreElements();) {
      String key = StringUtils.toString(it.nextElement());
      addProp(Namespace.NODE.name, key, config.getInitParameter(key), "servlet-config");
    }
  }

  private void loadServletParams(ServletContext config)
  {
    for (Enumeration<?> it = config.getInitParameterNames(); it.hasMoreElements();) {
      String key = StringUtils.toString(it.nextElement());
      addProp(Namespace.NODE.name, key, config.getInitParameter(key), "servlet-context");
    }
  }

  private ConfigProperties getTree(String namespace,
                                   String deploymentEnvironment)
  {
    return __props.getTree(namespace + "." + deploymentEnvironment);
  }

  private ConfigProperties getTree(String namespace,
                                   String machineName,
                                   String hostName,
                                   String userName)
  {
    return __props.getTree(namespace + "." + machineName + "." + hostName + "." + userName);
  }

  private ConfigProperties getDeploymentTree()
  {
    return getTree("node.deployment", getMachineName(), getHostName(), getUserName());
  }

  private void preinit(String docBase,
                       PrintWriter debug)
  {
    setDebug(debug);
    debug("Loading NodeConfig from %s...\n", docBase);

    ConfigProperties configTree = __props.createTree(Namespace.CONFIG.toString());

    configTree.put(INITPARAM_DOCBASE, docBase, "NodeConfig");
    __props.put(INITPARAM_DOCBASE, docBase, "NodeConfig");

    configTree.put(INITPARAM_USERNAME, getUserName(), "NodeConfig");
    __props.put(INITPARAM_USERNAME, getUserName(), "NodeConfig");

    configTree.put(INITPARAM_MACHINENAME, getMachineName(), "NodeConfig");
    __props.put(INITPARAM_MACHINENAME, getMachineName(), "NodeConfig");

    configTree.put(INITPARAM_MACHINEADDR, getMachineAddr(), "NodeConfig");
    __props.put(INITPARAM_MACHINEADDR, getMachineAddr(), "NodeConfig");
  }

  private void postinit()
  {
    ConfigProperties nodeTree = __props.getTree(Namespace.NODE.toString());

    nodeTree.put(NodeServlet.INITPARAM_HOSTNAME, getHostName(), "NodeConfig");
    __props.put(NodeServlet.INITPARAM_HOSTNAME, getHostName(), "NodeConfig");

    ConfigProperties configTree = __props.createTree(Namespace.CONFIG.toString());

    __props.put(INITPARAM_ENVIRONMENT, getDeploymentEnvironment(), "NodeConfig");
    configTree.put(INITPARAM_ENVIRONMENT, getDeploymentEnvironment(), "NodeConfig");
    configTree.link("deployment", getDeploymentTree());

    String logConfigLevel = nodeTree.get("log.config.level");

    debug("Loading NodeConfig...done\n", __docBase);

    if (!"debug".equals(logConfigLevel)) {
      setDebug(null);
    }
  }

  // --------------------------------------------------------------------------
  // Debug methods

  private PrintWriter _debug = null;

  private void setDebug(PrintWriter debug)
  {
    _debug = debug;
    __props.setDebug(debug);
  }

  private void debug(String format,
                     Object... args)
  {
    if (_debug != null) {
      _debug.printf(format, args);
    }
  }

  // --------------------------------------------------------------------------

  public static void main(String[] args)
  {
    NodeConfig conf = new NodeConfig(new File("."));
    System.err.println("---");
    System.err.println(conf.__props.get("config.deployment.environment"));
  }
}
