package org.cord.node.listing;

import org.cord.mirror.FieldKey;
import org.cord.mirror.RecordOrdering;
import org.cord.node.AclAuthenticatedBase;
import org.cord.node.NodeManager;
import org.cord.util.OptionalInt;

/**
 * An ordering that may be also used for abc filtering and which may be restricted to a given ACL as
 * to who may utilise it.
 * 
 * @author alex
 */
public class QueryOrderBy
  extends AclAuthenticatedBase
{
  private final RecordOrdering __recordOrdering;

  private final String __englishName;

  private final FieldKey<String> __abcField;

  public QueryOrderBy(NodeManager nodeManager,
                      RecordOrdering recordOrdering,
                      String englishName,
                      FieldKey<String> abcField,
                      OptionalInt aclId)
  {
    super(nodeManager,
          aclId,
          OptionalInt.absent());
    __recordOrdering = recordOrdering;
    // __sql = sql;
    __englishName = englishName;
    __abcField = abcField;
  }

  /**
   * Create a QueryOrderBy that takes its English Name from the English Name of the contained
   * RecordOrdering.
   */
  public QueryOrderBy(NodeManager nodeManager,
                      RecordOrdering recordOrdering,
                      FieldKey<String> abcField,
                      OptionalInt aclId)
  {
    this(nodeManager,
         recordOrdering,
         recordOrdering.getEnglishName(),
         abcField,
         aclId);
  }

  public final String getEnglishName()
  {
    return __englishName;
  }

  public final RecordOrdering getRecordOrdering()
  {
    return __recordOrdering;
  }

  public final FieldKey<String> getAbcField()
  {
    return __abcField;
  }

  @Override
  public String toString()
  {
    return "QueryOrderBy(" + __recordOrdering + "," + __englishName + ")";
  }
}
