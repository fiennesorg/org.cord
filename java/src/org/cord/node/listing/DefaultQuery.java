package org.cord.node.listing;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.NodeRequest;
import org.cord.node.NodeStyle;

public class DefaultQuery
  implements DefaultIdListFactory
{
  private final Db __db;

  private final String __targetTableName;

  private final String __filter;

  private final String __order;

  private final int __listingNodeId;

  public DefaultQuery(Db db,
                      String targetTableName,
                      String filter,
                      String order,
                      int listingNodeId)
  {
    __db = db;
    __targetTableName = targetTableName;
    __filter = filter;
    __order = order;
    __listingNodeId = listingNodeId;
  }

  @Override
  public String getTargetTableName()
  {
    return __targetTableName;
  }

  @Override
  public IdListing getList(NodeRequest nodeRequest,
                           Integer targetId)
  {
    IdList list = __db.getTable(__targetTableName).getQuery(null, __filter, __order).getIdList();
    return new IdListing(__db, __targetTableName, list, list.indexOf(targetId), __listingNodeId);
  }

  /**
   * @throws MirrorNoSuchRecordException
   *           if there is not any Nodes implementing the referenced NodeStyle, or if the referenced
   *           NodeStyle does not exist.
   */
  public static DefaultQuery getByNodeStyle(Db db,
                                            String targetTableName,
                                            String filter,
                                            String order,
                                            Integer listingNodeStyleId)
      throws MirrorNoSuchRecordException
  {
    return new DefaultQuery(db,
                            targetTableName,
                            filter,
                            order,
                            RecordSources.getFirstRecord(db.getTable(NodeStyle.TABLENAME)
                                                           .getRecord(listingNodeStyleId)
                                                           .opt(NodeStyle.NODES),
                                                         null)
                                         .getId());
  }

  /**
   * Get a DefaultQuery instance that returns all records from the targetTable where isPublished is
   * true using the default Table ordering.
   * 
   * @throws MirrorNoSuchRecordException
   *           if there is not any Nodes implementing the referenced NodeStyle, or if the referenced
   *           NodeStyle does not exist.
   */
  public static DefaultQuery getIsPublished(Db db,
                                            String targetTableName,
                                            Integer listingNodeStyleId)
      throws MirrorNoSuchRecordException
  {
    return getByNodeStyle(db,
                          targetTableName,
                          "(" + targetTableName + ".isPublished=1)",
                          null,
                          listingNodeStyleId);
  }
}