package org.cord.node.listing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.StringUtils;
import org.webmacro.Context;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Abstract view that grabs a list of integer ids from the NodeRequest and then passes this onto the
 * implementation to perform some processing.
 * 
 * @author alex
 */
public abstract class AbstractListingView
  extends DynamicAclAuthenticatedFactory
{
  public static final String CGI_RECORDSIDS_DEFAULT = "recordIds";

  private final Pattern __integers = Pattern.compile("[0-9]+");

  private final Table __targetTable;

  public final Table getTargetTable()
  {
    return __targetTable;
  }

  private final String __cgiRecordsIds;

  private final boolean __testIds;

  private final boolean __failOnBadIds;

  /**
   * @param cgiRecordIds
   *          The name of the parameter that will contain the incoming ids. The parameter should be
   *          formatted as integers separated by whatever you like (ie "[0-9]+").
   * @param testIds
   *          if true then each id in cgiRecordIds will be resolved against targetTable before the
   *          subclass' getInstance method is invoked. Note that there may well still be broken
   *          links in the chain due to concurrent deletions, but it is much less likely...
   * @param failOnBadIds
   *          if testIds is true and an id fails then the view will throw a FeedbackErrorException
   *          rather than invoking the subclass' getInstance method. If this is false then the
   *          erroneous ids will be transparently discarded from the list.
   */
  public AbstractListingView(String name,
                             NodeManager nodeManager,
                             PostViewAction postViewSuccessOperation,
                             Integer aclId,
                             String authenticationError,
                             Table targetTable,
                             String cgiRecordIds,
                             boolean testIds,
                             boolean failOnBadIds)
  {
    super(name,
          nodeManager,
          postViewSuccessOperation,
          aclId,
          authenticationError);
    __targetTable = Preconditions.checkNotNull(targetTable, "targetTable");
    __cgiRecordsIds = StringUtils.padEmpty(cgiRecordIds, CGI_RECORDSIDS_DEFAULT);
    __testIds = testIds;
    __failOnBadIds = failOnBadIds;
  }

  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    String recordIds = Strings.nullToEmpty(nodeRequest.getString(__cgiRecordsIds));
    Matcher matcher = __integers.matcher(recordIds);
    IdList ids = new ArrayIdList(getTargetTable());
    int id = 0;
    while (matcher.find()) {
      try {
        id = Integer.parseInt(matcher.group());
        if (__testIds) {
          getTargetTable().getRecord(id);
        }
        ids.add(id);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        if (__failOnBadIds) {
          throw new FeedbackErrorException(null,
                                           node,
                                           mnsrEx,
                                           false,
                                           false,
                                           "Unable to resolve id #%s",
                                           Integer.valueOf(id));
        }
      }
    }
    return getInstance(nodeRequest,
                       node,
                       context,
                       acl,
                       new ConstantRecordSource(UnmodifiableIdList.getInstance(ids), null));
  }

  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl,
                                                    RecordSource records)
      throws NodeRequestException;
}
