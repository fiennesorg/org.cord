package org.cord.node.listing;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.RecordSource;
import org.cord.node.NodeRequest;
import org.cord.util.NumberUtil;

/**
 * Object that is placed into the context for pages on or below /[section]/[dbType] that is used to
 * enable the generation of PersistentRecordIdList settings in the context of the Session, and
 * retrieval of the same.
 */
public class IdListingManager
{
  private final Db __db;

  private final Map<String, DefaultIdListFactory> __defaultFactories =
      new HashMap<String, DefaultIdListFactory>();

  public IdListingManager(Db db)
  {
    __db = db;
  }

  public void addDefaultFactory(DefaultIdListFactory defaultFactory)
  {
    synchronized (__defaultFactories) {
      __defaultFactories.put(defaultFactory.getTargetTableName(), defaultFactory);
    }
  }

  public void setList(NodeRequest nodeRequest,
                      String targetTableName,
                      RecordSource targetRecords,
                      int index)
  {
    setList(nodeRequest, targetTableName, targetRecords, index, null);
  }

  public void setList(NodeRequest nodeRequest,
                      String targetTableName,
                      RecordSource targetRecords,
                      int index,
                      Integer listingNodeId)
  {
    setList(nodeRequest, targetTableName, targetRecords.getIdList(), index, listingNodeId);
  }

  public void setList(NodeRequest nodeRequest,
                      String targetTableName,
                      IdList targetIds,
                      int index)
  {
    setList(nodeRequest, targetTableName, targetIds, index, null);
  }

  /**
   * Initialise a persistent named IdList that is optionally currently pointing at an explicitly
   * defined index.
   * 
   * @param index
   *          The index that the IdList will be pointing at when it is created. If this is set to
   *          -1, then the session will be checked to see if there is an existing IdList already
   *          declared under the targetTableName and the index will be extracted from that list. If
   *          the index is pointing to a record that is not found in the current targetIds then it
   *          will be set to 0.
   */
  public void setList(NodeRequest nodeRequest,
                      String targetTableName,
                      IdList targetIds,
                      int index,
                      Integer listingNodeId)
  {
    HttpSession session = nodeRequest.getSession();
    String sessionName = "pril" + targetTableName;
    // TODO (#266) Should we be doing something with this existing list?
    // if (index == -1) {
    // IdList existingList = (IdList) session.getAttribute(sessionName);
    // }
    IdListing pril = new IdListing(__db,
                                   targetTableName,
                                   targetIds,
                                   index,
                                   listingNodeId == null
                                       ? nodeRequest.getTargetLeafNode().getId()
                                       : listingNodeId.intValue());
    session.setAttribute(sessionName, pril);
  }

  /**
   * Get the requested IdList associated with this NodeRequest. If there is no IdList associated, or
   * if the current dbItemId is not on the current list, then the list is reset to the default "all
   * items" point in the list.
   */
  public IdListing getList(NodeRequest nodeRequest,
                           String targetTableName,
                           Integer targetId)
      throws MirrorNoSuchRecordException
  {
    HttpSession session = nodeRequest.getSession();
    IdListing pril = (IdListing) session.getAttribute("pril" + targetTableName);
    int index = pril == null ? -1 : pril.getTargetIds().indexOf(targetId);
    if (pril == null || index == -1) {
      DefaultIdListFactory defaultFactory = null;
      synchronized (__defaultFactories) {
        defaultFactory = __defaultFactories.get(targetTableName);
      }
      if (defaultFactory != null) {
        return defaultFactory.getList(nodeRequest, targetId);
      }
      return null;
    } else {
      pril.setIndex(index);
    }
    return pril;
  }

  public IdListing getList(NodeRequest nodeRequest,
                           String targetTableName)
  {
    HttpSession session = nodeRequest.getSession();
    IdListing pril = (IdListing) session.getAttribute("pril" + targetTableName);
    if (pril == null) {
      DefaultIdListFactory defaultFactory = null;
      synchronized (__defaultFactories) {
        defaultFactory = __defaultFactories.get(targetTableName);
      }
      if (defaultFactory != null) {
        return defaultFactory.getList(nodeRequest, NumberUtil.INTEGER_ZERO);
      }
      return null;
    }
    return pril;
  }
}