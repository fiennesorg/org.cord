package org.cord.node.listing;

import java.util.ListIterator;

import org.cord.mirror.MissingRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.webmacro.Context;

public class StandaloneListingView
  extends AbstractListingView
{
  private final String __contextQueryName;

  private final String __templatePath;

  public StandaloneListingView(String name,
                               NodeManager nodeManager,
                               Integer aclId,
                               String authenticationError,
                               Table targetTable,
                               String contextQueryName,
                               String templatePath)
  {
    super(name,
          nodeManager,
          null,
          aclId,
          authenticationError,
          targetTable,
          null,
          true,
          true);
    __contextQueryName = contextQueryName;
    __templatePath = templatePath;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl,
                                           RecordSource recordSource)
      throws NodeRequestException
  {
    ListIterator<PersistentRecord> records = recordSource.getRecordList().listIterator();
    try {
      Query query = null;
      if (records.hasNext()) {
        StringBuilder filter = new StringBuilder();
        filter.append(getTargetTable().getName()).append(".id in (");
        while (records.hasNext()) {
          filter.append(records.next().getId());
          if (records.hasNext()) {
            filter.append(",");
          }
        }
        filter.append(')');
        query = getTargetTable().getQuery(null, filter.toString(), null);
      }
      context.put(__contextQueryName, query);
      return new NodeRequestSegment(getNodeManager(),
                                    nodeRequest,
                                    node,
                                    context,
                                    __templatePath).setIsRootTemplatePath(true);
    } catch (MissingRecordException mrEx) {
      throw new FeedbackErrorException(null,
                                       node,
                                       mrEx,
                                       true,
                                       true,
                                       "Unable to find requested id#%s",
                                       mrEx.getId());
    }
  }
}
