package org.cord.node.listing;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.node.Node;

/**
 * Data structure that can be put into the Session scope and which represents a list of
 * PersistentRecord id instances and the current position within the list. Back to listing - store
 * as node.id or url or something else? Probably easiest to store as node id as we are realistically
 * going to have to work this out to generate the url and therefore we might as well not fix the url
 * in the session as this permits page renaming without breaking things...
 */
public class IdListing
{
  private final Table __targetTable;

  private final Table __nodeTable;

  private final IdList __targetIds;

  private int _index;

  private final int __listingNodeId;

  public IdListing(Db db,
                   String targetTableName,
                   IdList targetIds,
                   int index,
                   int listingNodeId)
  {
    __targetTable = db.getTable(targetTableName);
    __nodeTable = db.getTable(Node.TABLENAME);
    __targetIds = targetIds;
    _index = Math.max(index, 0);
    __listingNodeId = listingNodeId;
  }

  /**
   * Get the PersistentRecord that this IdList is currently pointing at.
   */
  public PersistentRecord getTargetRecord()
      throws MirrorNoSuchRecordException
  {
    return __targetTable.getRecord(__targetIds.getInt(_index));
  }

  @Override
  public String toString()
  {
    try {
      return "IdList(" + __targetTable + "x" + __targetIds.size() + "-->"
             + __nodeTable.getRecord(__listingNodeId).get("path") + ")";
    } catch (MirrorNoSuchRecordException mnsrEx) {
      return "IdList(" + __targetTable + "x" + __targetIds.size() + "-->" + __listingNodeId + ")";
    }
  }

  public int getListingNodeId()
  {
    return __listingNodeId;
  }

  public PersistentRecord getListingNode()
      throws MirrorNoSuchRecordException
  {
    return __nodeTable.getRecord(__listingNodeId);
  }

  public IdList getTargetIds()
  {
    return __targetIds;
  }

  public int getIndex()
  {
    return _index;
  }

  public void setIndex(int index)
  {
    _index = index;
  }

  @Deprecated
  public boolean hasPrevious()
  {
    return _index > 0;
  }

  /**
   * @return The next record or null if there are no more records
   */
  public PersistentRecord getPrevious()
      throws MirrorNoSuchRecordException
  {
    for (int i = _index; i >= 0; i--) {
      PersistentRecord record = __targetTable.getOptRecord(__targetIds.getInt(i));
      if (record != null) {
        return record;
      }
    }
    return null;
  }

  @Deprecated
  public boolean hasNext()
  {
    return _index < (__targetIds.size() - 1);
  }

  /**
   * @return The next record or null if there are no more records
   */
  public PersistentRecord getNext()
  {
    for (int i = _index; i < __targetIds.size(); i++) {
      PersistentRecord record = __targetTable.getOptRecord(__targetIds.getInt(i));
      if (record != null) {
        return record;
      }
    }
    return null;
  }
}