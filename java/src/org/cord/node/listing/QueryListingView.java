package org.cord.node.listing;

import org.cord.node.AclAuthenticatedBase;
import org.cord.node.NodeManager;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.OptionalInt;

/**
 * A object representing a presentational style for a QueryListing that may be optionally restricted
 * to a given ACL.
 * 
 * @author alex
 */
public class QueryListingView
  extends AclAuthenticatedBase
{
  private final String __templatePath;

  private final String __englishName;

  private final int __pageSize;

  public QueryListingView(NodeManager nodeManager,
                          String templatePath,
                          String englishName,
                          int pageSize,
                          OptionalInt aclId)
  {
    super(nodeManager,
          aclId,
          OptionalInt.absent());
    __templatePath = templatePath;
    __englishName = englishName;
    __pageSize = pageSize;
  }

  public QueryListingView(NodeManager nodeManager,
                          Gettable gettable,
                          String name)
  {
    this(nodeManager,
         (String) gettable.get("QueryListing." + name + ".templatePath"),
         (String) gettable.get("QueryListing." + name + ".visibleName"),
         GettableUtils.getInt(gettable, "QueryListing." + name + ".pageSize", 10),
         GettableUtils.getOptionalInt(gettable, "QueryListing." + name + ".acl"));
  }

  public String getTemplatePath()
  {
    return __templatePath;
  }

  public String getEnglishName()
  {
    return __englishName;
  }

  public int getPageSize()
  {
    return __pageSize;
  }

  @Override
  public String toString()
  {
    return "QueryListingView(" + __templatePath + "," + __englishName + "," + __pageSize + ")";
  }
}