package org.cord.node.listing;

import org.cord.node.NodeRequest;

/**
 * Interface that models Objects that are capable of producing default PersistentRecordIdList lists
 * for a given PersistentRecord in a given Table. This permits people to have the default behaviour
 * ommit unpublished items etc and initialise the value of the listing node to a suitable location.
 */
public interface DefaultIdListFactory
{
  public String getTargetTableName();

  public IdListing getList(NodeRequest nodeRequest,
                           Integer targetId);
}