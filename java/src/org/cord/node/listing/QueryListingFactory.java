package org.cord.node.listing;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.node.Acl;
import org.cord.node.NodeRequest;
import org.cord.node.NodeServlet;
import org.cord.node.User;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.OptionalInt;
import org.cord.util.StringUtils;

/**
 * Factory class that produces QueryListing items associated with a given Table.
 * 
 * @author alex
 */
public class QueryListingFactory
  extends EnglishNamedImpl
{
  public final static String CGI_ORDERBY = "orderBy";

  public final static String CGI_TEMPLATEPATH = "templatePath";

  private final Db __db;

  private final String __tableName;

  private final Map<String, QueryListingView> __views =
      new LinkedHashMap<String, QueryListingView>();

  private final Map<String, QueryListingView> __roViews = Collections.unmodifiableMap(__views);

  private QueryListingView _defaultView;

  private final Map<String, QueryOrderBy> __orderBys = new LinkedHashMap<String, QueryOrderBy>();

  private final Map<String, QueryOrderBy> __roOrderBys = Collections.unmodifiableMap(__orderBys);

  private QueryOrderBy _defaultOrderBy;

  private final FieldKey<String> __defaultAbcField;

  private final String __cgiOrderBy;

  private final String __cgiTemplatePath;

  public QueryListingFactory(String name,
                             String englishName,
                             Db db,
                             String tableName,
                             FieldKey<String> defaultAbcField)
  {
    super(name,
          englishName);
    __db = db;
    __tableName = tableName;
    __defaultAbcField = defaultAbcField;
    __cgiOrderBy = CGI_ORDERBY + name;
    __cgiTemplatePath = CGI_TEMPLATEPATH + name;
  }

  public void addPersistentParameters(NodeServlet nodeServlet)
  {
    nodeServlet.addPersistentParameter(getCgiTemplatePath(), _defaultView.getTemplatePath());
    nodeServlet.addPersistentParameter(getCgiOrderBy(),
                                       _defaultOrderBy.getRecordOrdering().getName());
  }

  public String getCgiOrderBy()
  {
    return __cgiOrderBy;
  }

  public String getCgiTemplatePath()
  {
    return __cgiTemplatePath;
  }

  public final FieldKey<String> getDefaultAbcField()
  {
    return __defaultAbcField;
  }

  public final Table getTable()
  {
    return __db.getTable(__tableName);
  }

  /**
   * Get a filtered Iterator of QueryListingViews that are appropriate for the given user and owner.
   * 
   * @return Immutable Iterator of QueryListingView
   */
  public final Iterator<QueryListingView> getViews(final int userId,
                                                   final OptionalInt ownerId)
  {
    return Acl.acceptsAclAuthenticated(__roViews.values().iterator(), userId, ownerId);
  }

  /**
   * Get a filtered Iterator of QueryListingViews that are suitable for anonymous users.
   */
  public final Iterator<QueryListingView> getViews()
  {
    return getViews(User.ID_ANONYMOUS, OptionalInt.absent());
  }

  /**
   * Get an unfiltered Iterator of QueryListingViews registered on this factory.
   * 
   * @return Immutable Iterator of QueryListingView
   * @see QueryListingView
   */
  public final Iterator<QueryListingView> getAllViews()
  {
    return __roViews.values().iterator();
  }

  public final QueryListingView getView(String templatePath)
  {
    QueryListingView view = __views.get(templatePath);
    return view == null ? getDefaultView() : view;
  }

  public void addView(QueryListingView view,
                      boolean isDefault)
  {
    __views.put(view.getTemplatePath(), view);
    if (isDefault || __views.size() == 1) {
      _defaultView = view;
    }
  }

  public QueryListingView getDefaultView()
  {
    return _defaultView;
  }

  /**
   * Get a filtered Iterator of QueryOrderBy that are appropriate for the given user and owner.
   * 
   * @return Immutable Iterator of QueryListingView
   */
  public final Iterator<QueryOrderBy> getOrderBys(final int userId,
                                                  final OptionalInt ownerId)
  {
    return Acl.acceptsAclAuthenticated(__roOrderBys.values().iterator(), userId, ownerId);
  }

  public final Iterator<QueryOrderBy> getOrderBys()
  {
    return getOrderBys(User.ID_ANONYMOUS, OptionalInt.absent());
  }

  public final Iterator<QueryOrderBy> getAllOrderBys()
  {
    return __roOrderBys.values().iterator();
  }

  public void addOrderBy(QueryOrderBy orderBy,
                         boolean isDefault)
  {
    __orderBys.put(orderBy.getRecordOrdering().getName(), orderBy);
    if (isDefault || __orderBys.size() == 1) {
      _defaultOrderBy = orderBy;
    }
  }

  public QueryOrderBy getOrderBy(String sql)
  {
    QueryOrderBy orderBy = __orderBys.get(sql);
    return orderBy == null ? getDefaultOrderBy() : orderBy;
  }

  public final QueryOrderBy getDefaultOrderBy()
  {
    return _defaultOrderBy;
  }

  public QueryListing getQueryListing(RecordSource recordSource,
                                      String cgiName,
                                      QueryOrderBy orderBy,
                                      NodeRequest nodeRequest,
                                      QueryListingView view)
  {
    orderBy = orderBy == null ? getOrderBy(nodeRequest.getString(getCgiOrderBy())) : orderBy;
    view = view == null ? getView(nodeRequest.getString(getCgiTemplatePath())) : view;
    return new QueryListing(this,
                            cgiName,
                            preProcessQueryWrapper(recordSource, orderBy, view),
                            orderBy,
                            nodeRequest,
                            view);
  }

  public QueryListing getQueryListing(RecordSource queryWrapper,
                                      String cgiName,
                                      String orderByName,
                                      NodeRequest nodeRequest,
                                      String viewName)
  {
    orderByName = StringUtils.padEmpty(orderByName, nodeRequest.getString(getCgiOrderBy()));
    QueryOrderBy orderBy = getOrderBy(orderByName);
    viewName = StringUtils.padEmpty(viewName, nodeRequest.getString(getCgiTemplatePath()));
    QueryListingView view = getView(viewName);
    return new QueryListing(this,
                            cgiName,
                            preProcessQueryWrapper(queryWrapper, orderBy, view),
                            orderBy,
                            nodeRequest,
                            view);
  }

  public QueryListing getQueryListing(RecordSource queryWrapper,
                                      String cgiName,
                                      NodeRequest nodeRequest)
  {
    return getQueryListing(queryWrapper,
                           cgiName,
                           (QueryOrderBy) null,
                           nodeRequest,
                           (QueryListingView) null);
  }

  /**
   * Utility method that is invoked when a new QueryListing is being created to provide an filtering
   * of the displayed RecordSource if necessary. The default implementation does nothing and just
   * returns the original QueryWrapper. It may be useful to override this method if your ordering
   * requirements for the generated listings necessitate a join to a related table.
   */
  protected RecordSource preProcessQueryWrapper(RecordSource recordSource,
                                                QueryOrderBy orderBy,
                                                QueryListingView view)
  {
    return recordSource;
  }
}
