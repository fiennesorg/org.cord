package org.cord.node.listing;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.AbcPaginator;
import org.cord.node.AbstractPaginator;
import org.cord.node.NodeRequest;
import org.cord.node.Paginator;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class QueryListing
  implements Iterable<PersistentRecord>
{
  public static final String CGI_QUERYLISTINGVIEW = "queryListingView";

  private final QueryListingFactory __factory;

  private final NodeRequest __nodeRequest;

  private final RecordSource __queryWrapper;

  private final QueryListingView __view;

  private QueryOrderBy _orderBy;

  private final Map<String, String> __hiddenFields = new HashMap<String, String>();

  private final String __cgiName;

  private RecordOrdering _lockedOrderBy;

  protected QueryListing(QueryListingFactory factory,
                         String cgiName,
                         RecordSource recordSource,
                         QueryOrderBy orderBy,
                         NodeRequest nodeRequest,
                         QueryListingView view)
  {
    __factory = factory;
    __cgiName = cgiName;
    __nodeRequest = nodeRequest;
    _orderBy = orderBy == null
        ? factory.getOrderBy(nodeRequest.getString(factory.getCgiOrderBy()))
        : orderBy;
    recordSource = RecordSources.applyRecordOrdering(recordSource, orderBy.getRecordOrdering());
    __view =
        view == null ? factory.getView(nodeRequest.getString(factory.getCgiTemplatePath())) : view;
    __queryWrapper = recordSource;
    if (nodeRequest != null
        && !Strings.isNullOrEmpty(nodeRequest.getString(CGI_QUERYLISTINGVIEW))) {
      String queryListingView = nodeRequest.getString(CGI_QUERYLISTINGVIEW);
      addHiddenFormField(CGI_QUERYLISTINGVIEW, queryListingView);
      addHiddenFormField("view", queryListingView);
    }
  }

  public String getCgiTemplatePath()
  {
    return __factory.getCgiTemplatePath();
  }

  public String getCgiOrderBy()
  {
    return __factory.getCgiOrderBy();
  }

  public boolean hasOrderBy()
  {
    return _lockedOrderBy == null;
  }

  /**
   * Lock the Order By clause of this QueryListing to the specified SQL clause.
   * 
   * @return this
   */
  public QueryListing lockOrderBy(RecordOrdering orderBy)
  {
    _lockedOrderBy = orderBy;
    return this;
  }

  /**
   * Lock the Order By clause of this QueryListing to the order currently defined in the wrapper
   * Query.
   * 
   * @return this
   */
  public QueryListing lockOrderBy()
  {
    return lockOrderBy(getQueryWrapper().getRecordOrdering());
  }

  public QueryListingView getView()
  {
    return __view;
  }

  public QueryListingFactory getFactory()
  {
    return __factory;
  }

  public void setOrderBy(QueryOrderBy orderBy)
  {
    Preconditions.checkNotNull(orderBy, "orderBy");
    _orderBy = orderBy;
  }

  public final void addHiddenFormField(String name,
                                       String value)
  {
    if (!Strings.isNullOrEmpty(name) && !name.startsWith(__cgiName)
        && !Strings.isNullOrEmpty(value)) {
      __hiddenFields.put(name, value);
    }
  }

  public final void addHiddenFormField(Gettable gettable,
                                       String name)
  {
    addHiddenFormField(name, gettable.getString(name));
  }

  public QueryOrderBy getOrderBy()
  {
    return _orderBy;
  }

  private AbcPaginator _abcPaginator = null;

  private void registerFields(AbstractPaginator paginator)
  {
    paginator.setSourceQuery(orderedQueryWrapper());
    paginator.addAdditionalField(__factory.getCgiOrderBy(),
                                 getOrderBy().getRecordOrdering().getName());
    paginator.addAdditionalField(__factory.getCgiTemplatePath(), getView().getTemplatePath());
    Iterator<Map.Entry<String, String>> i = __hiddenFields.entrySet().iterator();
    while (i.hasNext()) {
      Map.Entry<String, String> e = i.next();
      String value = StringUtils.toString(e.getValue());
      if (!Strings.isNullOrEmpty(value)) {
        paginator.addAdditionalField(StringUtils.toString(e.getKey()), value);
      }
    }
    // String view = __nodeRequest.getParameter(NodeRequest.CGI_VAR_VIEW,
    // false);
    // if (!Strings.isNullOrEmpty(view)) {
    // paginator.addAdditionalField(NodeRequest.CGI_VAR_VIEW, view);
    // }
  }

  public AbcPaginator getAbcPaginator(FieldKey<String> abcField)
  {
    _abcPaginator = __nodeRequest.getAbcPaginator(__cgiName);
    if (abcField == null) {
      abcField = getOrderBy().getAbcField();
      if (abcField == null) {
        abcField = getFactory().getDefaultAbcField();
      }
    }
    _abcPaginator.setAbcField(abcField);
    registerFields(_abcPaginator);
    return _abcPaginator;
  }

  /**
   * Get an AbcPaginator across the nested QueryWrapper using the default Abc Field defined in the
   * parent QueryListingFactory.
   */
  public AbcPaginator getAbcPaginator()
  {
    return getAbcPaginator(null);
  }

  public Paginator getPaginator()
  {
    if (_abcPaginator != null) {
      return _abcPaginator.getFilteredPaginator(__nodeRequest, getView().getPageSize());
    }
    Paginator paginator = __nodeRequest.getPaginator(__cgiName, getView().getPageSize());
    registerFields(paginator);
    return paginator;
  }

  public RecordSource getQueryWrapper()
  {
    return __queryWrapper;
  }

  /**
   * Get the Records that are held inside the nested QueryWrapper
   * 
   * @return Iterator of PersistentRecord
   */
  @Override
  public Iterator<PersistentRecord> iterator()
  {
    return getQueryWrapper().iterator();
  }

  /** @deprecated Nothing seems to call this --- GBK */
  @Deprecated
  protected Table getTable()
  {
    return getFactory().getTable();
  }

  private RecordSource orderedQueryWrapper()
  {
    if (_lockedOrderBy == null) {
      return orderedBy(__queryWrapper, _orderBy);
    } else {
      return orderedBy(__queryWrapper, _lockedOrderBy);
    }
  }

  private RecordSource orderedBy(RecordSource queryWrapper,
                                 QueryOrderBy orderBy)
  {
    if (orderBy == null) {
      return queryWrapper;
    }
    return orderedBy(queryWrapper, orderBy.getRecordOrdering());
  }

  private RecordSource orderedBy(RecordSource queryWrapper,
                                 RecordOrdering orderBy)
  {
    return RecordSources.applyRecordOrdering(queryWrapper, orderBy);
  }

  private void appendFormFields(StringBuilder result,
                                String view)
  {
    Iterator<Map.Entry<String, String>> hiddenFields = __hiddenFields.entrySet().iterator();
    while (hiddenFields.hasNext()) {
      Map.Entry<String, String> hiddenField = hiddenFields.next();
      String key = StringUtils.toString(hiddenField.getKey());
      AbstractPaginator.appendGetParam(result,
                                       key,
                                       view != null && NodeRequest.CGI_VAR_VIEW.equals(key)
                                           ? view
                                           : StringUtils.toString(hiddenField.getValue()));
    }
  }

  public String getFormFields(String view)
  {
    StringBuilder result = new StringBuilder();
    appendFormFields(result, view);
    return result.toString();
  }

  private void appendHiddenInput(StringBuilder result,
                                 String name,
                                 String value)
  {
    try {
      HtmlUtilsTheme.hiddenInput(result, value, name);
    } catch (IOException e) {
      throw new StringBuilderIOException(result, e);
    }
  }

  private String getHiddenInputs(String view,
                                 boolean hideTemplatePath,
                                 boolean hideOrderBy)
  {
    StringBuilder result = new StringBuilder();
    for (Map.Entry<String, String> entry : __hiddenFields.entrySet()) {
      String name = entry.getKey();
      String value = entry.getValue();
      if (NodeRequest.CGI_VAR_VIEW.equals(name)) {
        if (view != null) {
          value = view;
        }
      } else if (hideTemplatePath && name.startsWith("templatePath")) {
        value = null;
      } else if (hideOrderBy && name.startsWith("orderBy")) {
        value = null;
      }
      if (value != null) {
        appendHiddenInput(result, name, value);
      }
    }
    return result.toString();
  }

  public String getHiddenInputs(String view)
  {
    return getHiddenInputs(view, false, false);
  }

  public String getHiddenInputs()
  {
    return getHiddenInputs(null, false, false);
  }

  public String getHiddenMenuInputs()
  {
    return getHiddenInputs(null, true, true);
  }

  public String getFormFields()
  {
    return getFormFields(null);
  }

  public String getAlternativeView(String baseUrl,
                                   String view)
  {
    StringBuilder result = new StringBuilder();
    appendFormFields(result, view);
    result.insert(0, '?');
    result.insert(0, baseUrl);
    return result.toString();
  }
}