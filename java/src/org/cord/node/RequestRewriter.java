package org.cord.node;

import org.cord.util.Named;

public interface RequestRewriter
  extends Named
{
  public RequestPath rewrite(RequestPath requestPath);
}
