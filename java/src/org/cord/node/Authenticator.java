package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * An Authenticator is an Object that may be able to express an opinion on whether a User can
 * execute a particular action.
 */
public interface Authenticator
{
  /**
   * Is the User issuing the current NodeRequest allowed to invoke the contained view on the target
   * leaf node?
   * 
   * @return True or False if we have an opinion or null if we want someone else to take the
   *         decision.
   */
  public Boolean isAllowed(NodeRequest nodeRequest);

  /**
   * Is the User issuing the current NodeRequest allowed to invoke the contained view on the given
   * Node?
   * 
   * @return True or False if we have an opinion or null if we want someone else to take the
   *         decision.
   */
  public Boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node);

  /**
   * Is the give userId allowed to invoke the given view on the given Node?
   * 
   * @return True or False if we have an opinion or null if we want someone else to take the
   *         decision.
   */
  public Boolean isAllowed(NodeManager nodeMgr,
                           PersistentRecord node,
                           String view,
                           int userId);
}
