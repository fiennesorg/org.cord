package org.cord.node;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.cord.image.ImageMetaData;
import org.cord.mirror.Db;
import org.cord.mirror.LockableOperation;
import org.cord.mirror.MirrorAuthorisationException;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.DateFormatter;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.ReplaceWhitespace.Hyphen;
import org.cord.node.config.NodeConfig;
import org.cord.node.img.ImgMgr;
import org.cord.node.json.JSONMgr;
import org.cord.node.links.RelatedPagesManager;
import org.cord.node.operation.UserStyleSpecificNodeRecordOperation;
import org.cord.node.search.AdvancedSearch;
import org.cord.node.task.NodeTaskManager;
import org.cord.node.trigger.StyleSpecificNodePreBooter;
import org.cord.util.Assertions;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.Maps;
import org.cord.util.NamespacedNameCacheManager;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.cord.util.SessionDomainExpander;
import org.cord.util.Settable;
import org.cord.util.SettableFactory;
import org.cord.util.Shutdownable;
import org.cord.util.StringUtils;
import org.webmacro.WebMacro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;

/**
 * NodeManager is the central place for holding all the information about a particular instance of
 * the node system. This is designed to be as implementation agnostic as possible in order to try
 * and seperate out the role of the Node system from that of a servlet integrated system. Note sure
 * if this is completely necessary, but seems to be quite a nice idea in principle (but then again,
 * where else <i>other</i> than a servlet system would I be interested in using the Node system???).
 */
public class NodeManager
  implements SettableFactory
{
  /**
   * sanitise a given webPathDir so starts and ends with / and has at least one character inside it.
   * 
   * @return the sanitised value
   * @throws IllegalArgumentException
   *           if the trimmed webPathDir is null or empty.
   */
  public static String toValidWebPathDir(String webPathDir)
  {
    Preconditions.checkArgument(!Strings.isNullOrEmpty(StringUtils.trim(webPathDir)),
                                "%s is not a valid webPathDir",
                                webPathDir);
    webPathDir = webPathDir.startsWith("/") ? webPathDir : "/" + webPathDir;
    webPathDir = webPathDir.endsWith("/") ? webPathDir : webPathDir + "/";
    return webPathDir;
  }

  /**
   * Does the given User have permission to invoke the viewName action on the current transactional
   * state of Node? If you have access to the NodeRequest then it is better to invoke
   * {@link #mayInvoke(NodeRequest, PersistentRecord, String)} because some authentication models
   * will not be able to make a decision purely from the user. If the view is not found then false
   * will be returned and a log message will be written to aid debugging.
   */
  public boolean mayInvoke(PersistentRecord user,
                           PersistentRecord node,
                           String viewName)
  {
    PersistentRecord.assertIsTableNamed(node, Node.TABLENAME);
    NodeRequestSegmentFactory factory = getFactoryWithoutDefault(node, viewName);
    PersistentRecord.assertIsTableNamed(user, User.TABLENAME);
    return factory == null ? false : factory.isAllowed(node, user.getId());
  }

  /**
   * Does the given NodeRequest have permission to invoke the viewName action on the current
   * transactional state of Node?
   * 
   * @return true if the permission is granted. If the view is not found then false will be returned
   *         and a log message will be written to aid debugging.
   */
  public boolean mayInvoke(NodeRequest nodeRequest,
                           PersistentRecord node,
                           String viewName)
  {
    PersistentRecord.assertIsTableNamed(node, Node.TABLENAME);
    NodeRequestSegmentFactory factory = getFactoryWithoutDefault(node, viewName);
    return factory == null ? false : factory.isAllowed(node, nodeRequest);
  }

  private NodeRequestSegmentFactory getFactoryWithoutDefault(PersistentRecord node,
                                                             String viewName)
  {
    NodeRequestSegmentFactory factory =
        getNodeRequestSegmentManager().getFactoryWithoutDefault(node.compInt(Node.NODESTYLE_ID),
                                                                node.getTransactionType(),
                                                                viewName);
    if (factory == null) {
      System.err.println(this + ".mayInvoke([id]," + node + "," + viewName
                         + ") cannot resolve NodeRequestSegmentFactory");
    }
    return factory;
  }

  /**
   * The servlet init parameter ({@value} ) that is the MySQL hashing function that should be used
   * to encrypt the passwords supplied as logins to the Node system. The values should be as
   * follows:-
   * <ul>
   * <li><strong>MySQL &lt; 4.1</strong>: "password" for legacy sites, otherwise use "md5" or "sha1"
   * if using version 4.0.2 or greater.</li>
   * <li><strong>MySQL &gt;= 4.1</strong>: "old_password" for legacy sites that where built using
   * older implementations of MySQL's password function. Otherwise "md5" or "sha1" (sha1 is stronger
   * and more recommended)</li>
   * </ul>
   */
  public final static String INITPARAM_PASSWORD_HASHFUNCTION = "password.hashFunction";

  public final static String INITPARAM_PASSWORD_HASHFUNCTION_DEFAULT = "md5";

  public final static String PARAM_CHARACTERENCODING = "CharacterEncoding";

  public final static String CHARACTERENCODING_DEFAULT = "ISO-8859-1";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the Login functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_LOGIN = "nodeStyleIdLogin";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the 404 functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_FOUROHFOUR = "nodeStyleIdFourOhFour";

  /**
   * The servlet init parameter ({@value} ) that contains the id of the NodeStyle record that holds
   * the error reporting functionality.
   */
  public final static String INITPARAM_NODESTYLE_ID_FEEDBACKERROR = "nodeStyleIdFeedbackError";

  /**
   * Init param that contains the classname of the FilenameGenerator that is to be utilised for all
   * Node filenames for the lifespan of this NodeManager.
   * 
   * @see #INITPARAM_FILENAMEGENERATORCLASSNAME_DEFAULT
   */
  public final static String INITPARAM_FILENAMEGENERATORCLASSNAME = "filenameGeneratorClassName";

  /**
   * The default class to utilise for the FilenameGenerator if none is specified in the bootup
   * parameters which gives you hyphens instead of whitespace.
   * 
   * @see #INITPARAM_FILENAMEGENERATORCLASSNAME
   * @see Hyphen
   */
  public final static String INITPARAM_FILENAMEGENERATORCLASSNAME_DEFAULT =
      "org.cord.node.ReplaceWhitespace$Hyphen";

  public final static String INITPARAM_TRANSACTIONTIMEOUT = "transaction.timeout";

  public final static long INITPARAM_TRANSACTIONTIMEOUT_DEFAULT = 1800000;

  public static final String INITPARAM_PRELOCKEDTEMPLATEPATH = "PreLocked.templatePath";

  public static final String INITPARAM_PRELOCKEDTEMPLATEPATH_DEFAULT = "node/preLocked.wm.html";

  /**
   * The Gettable key to the flag as to whether or not this NodeManager will support the concept of
   * back links.
   * 
   * @see #supportsBackLinks()
   * @see #INITPARAM_SUPPORTSBACKLINKS_DEFAULT
   */
  public static final String INITPARAM_SUPPORTSBACKLINKS = "supportsBackLinks";

  /**
   * False
   * 
   * @see #supportsBackLinks()
   * @see #INITPARAM_SUPPORTSBACKLINKS
   */
  public static final boolean INITPARAM_SUPPORTSBACKLINKS_DEFAULT = false;

  private final String __preLockedTemplatePath;

  private final String __transactionPassword;

  private final File __temporaryDirectoryRoot;

  private final Integer __loginNodeId;

  private final Integer __fourOhFourNodeId;

  private final Integer __feedbackErrorNodeId;

  private final String __httpServletPath;

  private final String __hostName;

  private final File __webRoot;

  private final NodeTaskManager __nodeTaskManager;

  private boolean _utiliseBooleanParamChecks = true;

  // private List _userRegionFactories = new ArrayList();
  private StyleSpecificNodePreBooter _styleSpecificNodePreBooter;

  private NodeRequestSegmentManager _nodeRequestSegmentManager;

  private NodeQuestionManager _nodeQuestionManager;

  private WebMacro _webMacro;

  private final String __siteAdminEmail;

  private FilenameGenerator _filenameGenerator;

  private SettableFactory _settableFactory;

  private List<RequestRewriter> _requestRewriters = new LinkedList<RequestRewriter>();

  // private UnpublishedNodeTracker _unpublishedNodeTracker;

  private UnpublishedNodes _unpublishedNodes;

  private SessionDomainExpander _sessionDomainExpander = null;

  private ContestedLockFilterManager _contestedLockFilterManager =
      new ContestedLockFilterManager(this);

  private Db _db;

  private ContentCompilerFactory _contentCompilerFactory;

  private NodeUserManager _userManager;

  private long _transactionTimeout;

  private UserStyleSpecificNodeRecordOperation<RecordSource> _childrenMenuNodes;

  private int _lockStatus = LockableOperation.LO_INITIALISED;

  private final NodeManagerFactory __nodeManagerFactory;

  private final String __passwordHashFunction;

  private final NamespacedNameCacheManager __namespacedNameCacheManager =
      new NamespacedNameCacheManager();

  private final List<NodeUpdater> __nodeUpdaters = Lists.newCopyOnWriteArrayList();

  private final Multimap<Integer, NodePreProcessor> __nodePreProcessors = HashMultimap.create();

  public Iterable<NodePreProcessor> getNodePreProcessorsForNodeStyle(Integer nodeStyleId)
  {
    return Collections.unmodifiableCollection(__nodePreProcessors.get(nodeStyleId));
  }

  public void registerNodePreProcessor(NodePreProcessor nodePreProcessor)
  {
    checkNotNull(nodePreProcessor);
    __nodePreProcessors.put(Integer.valueOf(nodePreProcessor.getNodeStyleId()), nodePreProcessor);
  }

  /**
   * @return unmodifiable List of all the {@link NodeUpdater} registered on this NodeManager.
   */
  public List<NodeUpdater> getNodeUpdaters()
  {
    return Collections.unmodifiableList(__nodeUpdaters);
  }

  public void registerNodeUpdater(NodeUpdater nodeUpdater)
  {
    __nodeUpdaters.add(Preconditions.checkNotNull(nodeUpdater, "nodeUpdater"));
  }

  /**
   * @return true if the state of the database has been changed by any of the NodeUpdaters
   */
  public boolean invokeNodeUpdaters(TransientRecord node,
                                    Gettable in)
      throws MirrorFieldException
  {
    boolean result = false;
    for (NodeUpdater nodeUpdater : __nodeUpdaters) {
      result |= nodeUpdater.updateNode(node, in);
    }
    return result;
  }

  public final NamespacedNameCacheManager getNamespacedNameCacheManager()
  {
    return __namespacedNameCacheManager;
  }

  private Acl _acl;

  private UserAcl _userAcl;

  private Acl_Acl _Acl_Acl;

  private User _user;

  private UserStyle _userStyle;

  private MovedNode _movedNode;

  private Node _node;

  private NodeStyle _nodeStyle;

  private NodeGroupStyle _nodeGroupStyle;

  private RegionStyle _regionStyle;

  private final boolean __isBooted;

  private final RelatedPagesManager __relatedPagesManager;

  private final boolean __supportsBackLinks;

  private final ImgMgr __imgMgr;

  private final JSONMgr __jMgr;

  private final AdvancedSearch __advancedSearch;

  private final Authenticator __authenticator;

  private final Map<String, DateFormatter> __dateFormatters = Maps.newConcurrentHashMap();

  /**
   * @param authenticator
   *          The Authenticator that this NodeManager should use for custom authentication. If null
   *          then an {@link EmptyAuthenticator} will be used instead.
   */
  public NodeManager(NodeManagerFactory nodeManagerFactory,
                     SettableFactory settableFactory,
                     Db db,
                     String transactionPassword,
                     File temporaryDirectoryRoot,
                     String hostName,
                     String httpServletPath,
                     String siteAdminEmail,
                     String webRootPath,
                     WebMacro webMacro,
                     boolean isBooted,
                     Authenticator authenticator)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    try {
      Locale.setDefault(Locale.UK);
      __authenticator = MoreObjects.firstNonNull(authenticator, EmptyAuthenticator.getInstance());
      __isBooted = isBooted;
      _settableFactory = settableFactory;
      __passwordHashFunction = GettableUtils.get(this,
                                                 INITPARAM_PASSWORD_HASHFUNCTION,
                                                 INITPARAM_PASSWORD_HASHFUNCTION_DEFAULT);
      __nodeManagerFactory = Preconditions.checkNotNull(nodeManagerFactory, "nodeManagerFactory");
      // __parameterManager.addParameter(parameterSupplier);
      ImageMetaData.setImageMagickPath(getGettable());
      _db = db;
      __transactionPassword = transactionPassword;
      __supportsBackLinks =
          GettableUtils.get(this, INITPARAM_SUPPORTSBACKLINKS, INITPARAM_SUPPORTSBACKLINKS_DEFAULT);
      __relatedPagesManager = new RelatedPagesManager();
      __siteAdminEmail = Assertions.notEmpty(siteAdminEmail, "siteAdminEmail");
      __preLockedTemplatePath = GettableUtils.get(this,
                                                  INITPARAM_PRELOCKEDTEMPLATEPATH,
                                                  INITPARAM_PRELOCKEDTEMPLATEPATH_DEFAULT);
      _contentCompilerFactory = new ContentCompilerFactory(this);
      _transactionTimeout = GettableUtils.get(this,
                                              INITPARAM_TRANSACTIONTIMEOUT,
                                              INITPARAM_TRANSACTIONTIMEOUT_DEFAULT);
      String fgClassName = GettableUtils.get(this,
                                             INITPARAM_FILENAMEGENERATORCLASSNAME,
                                             INITPARAM_FILENAMEGENERATORCLASSNAME_DEFAULT);
      try {
        Class<?> fgClass = Class.forName(fgClassName);
        _filenameGenerator = (FilenameGenerator) fgClass.newInstance();
      } catch (Exception ex) {
        throw new LogicException("Error dynamically linking FilenameGenerator: " + fgClassName, ex);
      }
      __jMgr = new JSONMgr(this);
      NodeDb nodeDb = new NodeDb(this);
      _acl = _db.add(new Acl(this));
      _userAcl = _db.add(new UserAcl(this));
      _Acl_Acl = _db.add(new Acl_Acl(this));
      _user = _db.add(new User(this));
      _userStyle = _db.add(new UserStyle(this));
      try {
        _node = _db.add(new Node(this, this.getGettable()));
      } catch (Exception e) {
        throw new IllegalArgumentException("Unable to boot Node table", e);
      }
      _movedNode = _db.add(new MovedNode(this));
      _nodeStyle = _db.add(new NodeStyle(this));
      _nodeGroupStyle = _db.add(new NodeGroupStyle(this));
      _regionStyle = _db.add(new RegionStyle(this));
      _db.add(nodeDb);
      _styleSpecificNodePreBooter = _node.getStyleSpecificNodePreBooter();
      _childrenMenuNodes = _node.getChildrenMenuNodes();
      _userManager = new NodeUserManager(this);
      __temporaryDirectoryRoot = temporaryDirectoryRoot;
      Integer loginNodeStyleId = isBooted
          ? new Integer(GettableUtils.get(settableFactory, INITPARAM_NODESTYLE_ID_LOGIN, "1000"))
          : null;
      __loginNodeId = calculateNodeId(loginNodeStyleId);
      Integer fourOhFourStyleId = isBooted
          ? new Integer(GettableUtils.get(settableFactory,
                                          INITPARAM_NODESTYLE_ID_FOUROHFOUR,
                                          "1001"))
          : null;
      __fourOhFourNodeId = calculateNodeId(fourOhFourStyleId);
      Integer feedbackErrorStyleId = isBooted
          ? new Integer(GettableUtils.get(settableFactory,
                                          INITPARAM_NODESTYLE_ID_FEEDBACKERROR,
                                          "1002"))
          : null;
      __feedbackErrorNodeId = calculateNodeId(feedbackErrorStyleId);
      __hostName = hostName;
      __httpServletPath = Assertions.notEmpty(httpServletPath, "httpServletPath");
      __webRoot = new File(webRootPath);
      if (!(__webRoot.exists() && __webRoot.isDirectory())) {
        throw new IllegalArgumentException("webRootPath does not exist or is not a directory:"
                                           + __webRoot);
      }
      _nodeRequestSegmentManager = new NodeRequestSegmentManager(this);
      _webMacro = webMacro;
      _nodeQuestionManager = new NodeQuestionManager();
      __advancedSearch = new AdvancedSearch(this, transactionPassword);
      // _unpublishedNodeTracker = new UnpublishedNodeTracker(db);
      _unpublishedNodes = new UnpublishedNodes(this);
      __nodeTaskManager = new NodeTaskManager(this);
      __imgMgr = ImgMgr.getInstance(this, transactionPassword);
      _db.add(__advancedSearch);
    } catch (RuntimeException rEx) {
      rEx.printStackTrace();
      try {
        shutdown();
      } catch (Exception ex) {
        System.err.println("Error shutting down the NodeManager due to initialisation error");
        ex.printStackTrace();
      }
      throw rEx;
    }
  }

  /**
   * Get the DateFormatter via the key that is was registered under. This can then be utilised to
   * format static dates outside the context of a record.
   * 
   * @return The appropriate DateFormatter or null if the key wasn't mapped to a DateFormatter.
   */
  public DateFormatter getDateFormatter(String key)
  {
    return __dateFormatters.get(key);
  }

  void registerDateFormatter(DateFormatter dateFormatter)
  {
    __dateFormatters.put(dateFormatter.getKey().getKeyName(), dateFormatter);
  }

  public AdvancedSearch getSearchMgr()
  {
    return __advancedSearch;
  }

  public final JSONMgr getJMgr()
  {
    return __jMgr;
  }

  public final ImgMgr getImgMgr()
  {
    return __imgMgr;
  }

  /**
   * Does this NodeManager support the concept of backlinks?
   * 
   * @see #INITPARAM_SUPPORTSBACKLINKS
   * @see #INITPARAM_SUPPORTSBACKLINKS_DEFAULT
   */
  public final boolean supportsBackLinks()
  {
    return __supportsBackLinks;
  }

  public final RelatedPagesManager getRelatedPagesManager()
  {
    return __relatedPagesManager;
  }

  public final NodeTaskManager getNodeTaskManager()
  {
    return __nodeTaskManager;
  }

  public NodeQuestionManager getNodeQuestionManager()
  {
    return _nodeQuestionManager;
  }

  public boolean isBooted()
  {
    return __isBooted;
  }

  /**
   * @return The MySQL function name for hashing passwords, never null
   * @see #INITPARAM_PASSWORD_HASHFUNCTION
   */
  public String getPasswordHashFunction()
  {
    return __passwordHashFunction;
  }

  public UserStyle getUserStyle()
  {
    return _userStyle;
  }

  public Acl getAcl()
  {
    return _acl;
  }

  public Acl_Acl getAcl_Acl()
  {
    return _Acl_Acl;
  }

  public UserAcl getUserAcl()
  {
    return _userAcl;
  }

  public User getUser()
  {
    return _user;
  }

  public MovedNode getMovedNode()
  {
    return _movedNode;
  }

  public Node getNode()
  {
    return _node;
  }

  public NodeStyle getNodeStyle()
  {
    return _nodeStyle;
  }

  public NodeGroupStyle getNodeGroupStyle()
  {
    return _nodeGroupStyle;
  }

  public RegionStyle getRegionStyle()
  {
    return _regionStyle;
  }

  /**
   * @return The NodeManagerFactory that produces this NodeManager. Never null.
   */
  public NodeManagerFactory getNodeManagerFactory()
  {
    return __nodeManagerFactory;
  }

  /**
   * Get the plugin architecture that enables you to provide custom menu structures for specific
   * NodeStyle or User combinations.
   * 
   * @see Node#getChildrenMenuNodes()
   */
  public UserStyleSpecificNodeRecordOperation<RecordSource> getChildrenMenuNodes()
  {
    return _childrenMenuNodes;
  }

  // public UnpublishedNodeTracker getUnpublishedNodeTracker()
  // {
  // return _unpublishedNodeTracker;
  // }

  public UnpublishedNodes getUnpublishedNodes()
  {
    return _unpublishedNodes;
  }

  public final FilenameGenerator getFilenameGenerator()
  {
    return _filenameGenerator;
  }

  /**
   * @return The Gettable that the SettableFactory that was passed to the constructor is wrapping.
   *         This will normally be the web.xml
   */
  @Override
  public final Gettable getGettable()
  {
    return _settableFactory.getGettable();
  }

  /**
   * @return The Settable that the SettableFactory that was passed to the constructor is wrapping.
   *         This will normally be the web.xml
   */
  @Override
  public final Settable getSettable()
  {
    return _settableFactory.getSettable();
  }

  /**
   * Check to see whether or not this NodeManager is configured to utilise boolean parameter checks
   * for safer flag uploading.
   * 
   * @return true if this NodeManager will be checking for active hidden parameter fields when
   *         resolving false boolean parameters.
   * @see #setUtiliseBooleanParamChecks(boolean)
   */
  public boolean utiliseBooleanParamChecks()
  {
    return _utiliseBooleanParamChecks;
  }

  /**
   * Set the behaviour when resolving incoming boolean parameters from the NodeRequest.
   * 
   * @param flag
   *          The proposed behaviour for boolean parameter checks. If true then active hidden fields
   *          are checked for.
   * @see #utiliseBooleanParamChecks()
   */
  public void setUtiliseBooleanParamChecks(boolean flag)
  {
    _utiliseBooleanParamChecks = flag;
  }

  /**
   * Get the template path for rendering the user feedback when there is a lock contention over a
   * targetted Node.
   * 
   * @see #INITPARAM_PRELOCKEDTEMPLATEPATH
   */
  public String getPreLockedTemplatePath()
  {
    return __preLockedTemplatePath;
  }

  public ContestedLockFilterManager getContestedLockFilterManager()
  {
    return _contestedLockFilterManager;
  }

  /**
   * @return The SessionDomainExpander or null if there is no registered instance.
   * @see #setSessionDomainExpander(SessionDomainExpander)
   */
  public SessionDomainExpander getSessionDomainExpander()
  {
    return _sessionDomainExpander;
  }

  /**
   * @see #getSessionDomainExpander()
   */
  public void setSessionDomainExpander(SessionDomainExpander sessionDomainExpander)
  {
    _sessionDomainExpander = sessionDomainExpander;
  }

  public void register(RequestRewriter requestRewriter)
  {
    _requestRewriters.add(requestRewriter);
  }

  public boolean hasRequestRewriters()
  {
    return _requestRewriters.size() > 0;
  }

  public RequestPath rewrite(RequestPath requestPath)
  {
    if (_requestRewriters.size() == 0) {
      return requestPath;
    }
    for (RequestRewriter requestRewriter : _requestRewriters) {
      RequestPath updatedRequestPath = requestRewriter.rewrite(requestPath);
      requestPath = updatedRequestPath == null ? requestPath : updatedRequestPath;
    }
    return requestPath;
  }

  /**
   * @see Db#checkTransactionPassword(String, String, Object...)
   */
  public void checkTransactionPassword(String transactionPassword,
                                       String details,
                                       Object... detailsParams)
      throws MirrorAuthorisationException
  {
    getDb().checkTransactionPassword(transactionPassword, details, detailsParams);
  }

  /**
   * Invoke preLock() on the Node Db.
   * 
   * @see Db#lock()
   */
  public void preLock()
  {
    getDb().preLock();
    _lockStatus = LockableOperation.LO_PRELOCKED;
  }

  /**
   * Inform the NodeManager that all the associated ContentCompilers have been registered. This will
   * have the following effects on the system:-
   * <ul>
   * <li>The Db will have pre-lock invoked upon itself.</li>
   * <li>A single sweep across all of the records in the RegionStyle Table will be performed. Each
   * RegionStyle will have it's ContentCompiler resolved and then the fact that the ContentCompiler
   * is in use in the RegionStyle via the declare(...) function.</li>
   * <li>The RegionStyle records will be validated to ensure that there are no pairs which share a
   * common NodeStyle parent but which have the same name</li>
   * </ul>
   * 
   * @see Db#preLock()
   * @see RegionStyle#getContentCompiler(TransientRecord)
   * @see ContentCompiler#declare(PersistentRecord)
   * @throws MirrorLogicException
   *           If the RegionStyle table contains records with duplicate names under a single
   *           NodeStyle
   */
  public void lock()
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    if (_lockStatus == LockableOperation.LO_LOCKED) {
      return;
    }
    preLock();
    final Table regionStyles = getDb().getTable(RegionStyle.TABLENAME);
    try {
      regionStyles.validateFields();
    } catch (SQLException e) {
      throw new LogicException("RegionStyle SQL problem: " + e, e);
    }
    SetMultimap<Integer, String> nodeStyleNames = HashMultimap.create();
    for (PersistentRecord regionStyle : regionStyles) {
      Integer nodeStyle_id = regionStyle.opt(RegionStyle.NODESTYLE_ID);
      String regionStyle_name = regionStyle.opt(RegionStyle.NAME);
      if (nodeStyleNames.containsEntry(nodeStyle_id, regionStyle_name)) {
        throw new MirrorLogicException(String.format("%s has a duplicate name of %s under %s",
                                                     regionStyle,
                                                     regionStyle_name,
                                                     regionStyle.opt(RegionStyle.NODESTYLE)),
                                       null);

      }
      nodeStyleNames.put(nodeStyle_id, regionStyle_name);
      ContentCompiler contentCompiler = getRegionStyle().getContentCompiler(regionStyle);
      contentCompiler.declare(regionStyle);
    }
    preLock();
    try {
      getSearchMgr().init();
    } catch (MirrorException e) {
      throw new LogicException("Error botting SearchMgr", e);
    }
    getDb().lock();
    getNodeManagerFactory().lock();
    getUnpublishedNodes().lock();
    _lockStatus = LockableOperation.LO_LOCKED;
  }

  /**
   * Get the site administrators email address as passed to the creator. This will be the default
   * sending address when automated emails are produced by the system.
   */
  public String getSiteAdminEmail()
  {
    return __siteAdminEmail;
  }

  /**
   * Get the name of the host that is currently managing this Node system as passed to the
   * constructor.
   */
  public String getHostName()
  {
    return __hostName;
  }

  private LinkedHashSet<Shutdownable> __shutdownables = new LinkedHashSet<Shutdownable>();

  public void registerShutdownable(Shutdownable shutdownable)
  {
    Preconditions.checkNotNull(shutdownable, "Shutdownable");
    synchronized (__shutdownables) {
      __shutdownables.add(shutdownable);
    }
  }

  public void shutdown()
  {
    DebugConstants.method(this, "shutdown");
    NodeTaskManager nodeTaskManager = getNodeTaskManager();
    if (nodeTaskManager != null) {
      nodeTaskManager.getTaskManager().shutdown();
    }
    synchronized (__shutdownables) {
      for (Shutdownable s : __shutdownables) {
        s.shutdown();
      }
    }
    if (_contentCompilerFactory != null) {
      _contentCompilerFactory.shutdown();
    }
    _contentCompilerFactory = null;
    if (_nodeRequestSegmentManager != null) {
      _nodeRequestSegmentManager.shutdown();
    }
    _nodeRequestSegmentManager = null;
    _userManager.shutdown();
    _userManager = null;
    Db db = getDb();
    if (db != null) {
      db.shutdown(true);
    }
    _db = null;
    // _unpublishedNodeTracker.shutdown();
    // _unpublishedNodeTracker = null;
    if (_contestedLockFilterManager != null) {
      _contestedLockFilterManager.shutdown();
    }
    _contestedLockFilterManager = null;
    _sessionDomainExpander = null;
    _requestRewriters = null;
    _settableFactory = null;
    _filenameGenerator = null;
    _webMacro = null;
    _styleSpecificNodePreBooter = null;
  }
  /**
   * Get the WebMacro that was passed in the constructor.
   */
  public WebMacro getWebMacro()
  {
    return _webMacro;
  }

  public NodeRequestSegmentManager getNodeRequestSegmentManager()
  {
    return _nodeRequestSegmentManager;
  }

  public final StyleSpecificNodePreBooter getStyleSpecificNodePreBooter()
  {
    return _styleSpecificNodePreBooter;
  }

  // /**
  // * @return Iterator of UserRegionFactory objects.
  // * @see UserRegionFactory
  // **/
  // public Iterator getUserRegionFactories()
  // {
  // return _userRegionFactories.iterator();
  // }
  // public boolean register(UserRegionFactory factory)
  // {
  // synchronized (_userRegionFactories) {
  // if (_userRegionFactories.contains(factory)) {
  // return false;
  // }
  // _userRegionFactories.add(factory);
  // return true;
  // }
  // }
  public Integer getFeedbackErrorNodeId()
  {
    return __feedbackErrorNodeId;
  }

  public PersistentRecord getFeedbackErrorNode()
  {
    try {
      return _db.getTable(Node.TABLENAME).getRecord(getFeedbackErrorNodeId());
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("I've lost the feedback error node???", mnsrEx);
    }
  }

  /**
   * Get the WebRoot as specified in the constuctor.
   * 
   * @return The File that represents the absolute root of the
   */
  public File getWebRoot()
  {
    return __webRoot;
  }

  protected final long getTransactionTimeout()
  {
    return _transactionTimeout;
  }

  protected final String getTransactionPassword()
  {
    return __transactionPassword;
  }

  /**
   * Get the absolute http path to the mount point for the servlet that is handling the Node system.
   * If you take the HttpServletPath and append a virtual path on this then you will have a complete
   * Node request path. The HttpServletPath is not expected to have a trailing slash. This is passed
   * to the creator of NodeManager and is currently immutable. Future work should look at making it
   * dynamic to handle sub-domain mount points.
   * 
   * @return The mount point for the servlet.
   */
  public final String getHttpServletPath()
  {
    return __httpServletPath;
  }

  /**
   * @see #calculateNodeId(Db,Integer)
   * @deprecated because this should be inside NodeStyle instead
   * @see NodeStyle#getFirstNodeId(int)
   * @see NodeStyle#getFirstNode(PersistentRecord, Viewpoint)
   */
  @Deprecated
  public final Integer calculateNodeId(Integer nodeStyleId)
      throws MirrorNoSuchRecordException
  {
    return calculateNodeId(_db, nodeStyleId);
  }

  public final PersistentRecord calculateNode(Integer nodeStyleId)
      throws MirrorNoSuchRecordException
  {
    return getDb().getTable(Node.TABLENAME).getRecord(calculateNodeId(nodeStyleId));
  }

  /**
   * @param nodeStyleId
   *          The id that we are going to look up. If null then null is returned.
   * @deprecated because we don't like static methods that work on the Node table and we are going
   *             off passing ids around rather than records.
   */
  @Deprecated
  public final static Integer calculateNodeId(Db db,
                                              Integer nodeStyleId)
      throws MirrorNoSuchRecordException
  {
    if (nodeStyleId == null) {
      return null;
    }
    try {
      return Integer.valueOf(RecordSources.getFirstRecord(db.getTable(NodeStyle.TABLENAME)
                                                            .getRecord(nodeStyleId)
                                                            .opt(NodeStyle.NODES),
                                                          null)
                                          .getId());
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new MirrorNoSuchRecordException("Cannot resolve nodeStyleId(" + nodeStyleId
                                            + ") into NodeId",
                                            mnsrEx.getTable(),
                                            mnsrEx.getId(),
                                            mnsrEx);
    } catch (RuntimeException e) {
      throw new RuntimeException(String.format("Unable to calculateNodeId(%s,%s)", db, nodeStyleId),
                                 e);
    }
  }

  public final Integer getLoginNodeId()
  {
    return __loginNodeId;
  }

  public final Integer getFourOhFourNodeId()
  {
    return __fourOhFourNodeId;
  }

  public final PersistentRecord getLoginNode()
  {
    try {
      return _db.getTable(Node.TABLENAME).getRecord(__loginNodeId);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("I've lost the Login Node???", mnsrEx);
    }
  }

  public final PersistentRecord getFourOhFourNode()
  {
    try {
      return _db.getTable(Node.TABLENAME).getRecord(getFourOhFourNodeId());
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("I've lost the 404 node???", mnsrEx);
    }
  }

  /**
   * Get the File that represents the root of the filesystem scratchpad as passed to the
   * constructore of the NodeManager.
   */
  public final File getTemporaryDirectoryRoot()
  {
    return __temporaryDirectoryRoot;
  }

  public final Db getDb()
  {
    return _db;
  }

  /**
   * Get the factory class that is responsible for handling the ContentCompilers that are registered
   * on this NodeManager.
   * 
   * @return the appropriate factory. never null.
   */
  public final ContentCompilerFactory getContentCompilerFactory()
  {
    return _contentCompilerFactory;
  }

  public final NodeUserManager getUserManager()
  {
    return _userManager;
  }

  public void flushCaches(String transactionPassword)
  {
    checkTransactionPassword(transactionPassword,
                             this + ".flushCaches(" + transactionPassword + ")");
    getUserManager().logoutAll();
    getDb().flushCaches(transactionPassword);
  }

  public final String getCharacterEncoding()
  {
    return GettableUtils.get(getGettable(), PARAM_CHARACTERENCODING, CHARACTERENCODING_DEFAULT);
  }

  public void initialiseNodeTree(String rootHtmlTitle,
                                 String rootTitle)
      throws Exception
  {
    getNodeManagerFactory().preInitialiseNodeTree(this);
    lock();
    // TODO: (#255) Migrate initialiseNodeTree to pass parameters in via a
    // Gettable
    // params method...
    TransientRecord rootNode = getDb().getTable(Node.TABLENAME).createTransientRecord();
    if (!Strings.isNullOrEmpty(rootHtmlTitle)) {
      rootNode.setField(Node.HTMLTITLE, rootHtmlTitle);
    }
    if (!Strings.isNullOrEmpty(rootTitle)) {
      rootNode.setField(Node.TITLE, rootTitle);
    }
    rootNode.setField(Node.NODEGROUPSTYLE_ID, NumberUtil.INTEGER_ONE);
    rootNode.setField(Node.NODESTYLE_ID, NumberUtil.INTEGER_ONE);
    rootNode.makePersistent(null);
    getNodeManagerFactory().postInitialiseNodeTree(this);
  }

  public File getDocBase()
  {
    return new File(getGettable().getString(NodeConfig.INITPARAM_DOCBASE));
  }

  /**
   * Get an optional File that is contained under the DocBase for the project
   * 
   * @param path
   *          The optional path to the file relative to the DocBase
   * @param definedFileError
   *          The error message which will be used if path is defined but it either points to a File
   *          that is not readable or is a directory.
   * @return The appropriate File or null if path is not defined.
   */
  public File getFileFromDocBase(String path,
                                 String definedFileError)
  {
    if (path != null) {
      File file = new File(getDocBase(), path);
      if (file.canRead() && file.isFile()) {
        return file;
      }
      throw new IllegalArgumentException(String.format(definedFileError, file));
    }
    return null;
  }

  /**
   * @return the Authenticator in use. Never null
   */
  public final Authenticator getAuthenticator()
  {
    return __authenticator;
  }

  @SuppressWarnings("unchecked")
  public List<String> addGoogleAnalyticsPageUrl(NodeRequest nodeRequest,
                                                String pageUrl)
  {
    HttpSession session = nodeRequest.getSession();
    Object obj = session.getAttribute(NodeServlet.WM_GA_PAGEURLS);
    List<String> pageUrls;
    if (obj == null) {
      pageUrls = Lists.newArrayList();
      session.setAttribute(NodeServlet.WM_GA_PAGEURLS, pageUrls);
    } else {
      pageUrls = (List<String>) ObjectUtil.castTo(obj, List.class);
    }
    pageUrls.add(prependGoogleAnalyticsVirtualHost(pageUrl));
    return pageUrls;
  }

  public String prependGoogleAnalyticsVirtualHost(String pageUrl)
  {
    Optional<String> virtualHost = getGoogleAnalyticsVirtualHost();
    return virtualHost.isPresent() ? "/" + virtualHost.get() + pageUrl : pageUrl;
  }

  public static final String CONF_GOOGLE_ANALYTICS_VIRTUALHOST = "google.analytics.virtualHost";

  private Optional<String> _googleAnalyticsVirtualHost = null;

  public Optional<String> getGoogleAnalyticsVirtualHost()
  {
    if (_googleAnalyticsVirtualHost == null) {
      _googleAnalyticsVirtualHost =
          Optional.fromNullable(getGettable().getString(CONF_GOOGLE_ANALYTICS_VIRTUALHOST));
    }
    return _googleAnalyticsVirtualHost;
  }

}
