package org.cord.node;

import java.util.List;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class NodeGroupStyles
{
  private NodeGroupStyles()
  {
  }

  public static PersistentRecord create(NodeManager nodeMgr,
                                        PersistentRecord parentNodeStyle,
                                        PersistentRecord childNodeStyle,
                                        PersistentRecord createChildAcl,
                                        int number,
                                        int minimum,
                                        int maximum)
      throws MirrorInstantiationException, MirrorFieldException
  {
    Preconditions.checkArgument(!nodeMgr.isBooted(), "Cannot invoke on a booted NodeManager");
    TransientRecord nodeGroupStyle = nodeMgr.getNodeGroupStyle().getTable().createTransientRecord();
    nodeGroupStyle.setField(NodeGroupStyle.PARENTNODESTYLE_ID, parentNodeStyle);
    nodeGroupStyle.setField(NodeGroupStyle.CHILDNODESTYLE_ID, childNodeStyle);
    nodeGroupStyle.setField(NodeGroupStyle.CREATECHILDACL_ID, createChildAcl);
    nodeGroupStyle.setField(NodeGroupStyle.UPDATECREATECHILDACL_ID, Integer.valueOf(Id_Acl.NOONE));
    nodeGroupStyle.setField(NodeGroupStyle.NUMBER, Integer.valueOf(number));
    nodeGroupStyle.setField(NodeGroupStyle.MINIMUM, Integer.valueOf(minimum));
    nodeGroupStyle.setField(NodeGroupStyle.MAXIMUM, Integer.valueOf(maximum));
    nodeGroupStyle.setField(NodeGroupStyle.NAME, childNodeStyle.opt(NodeStyle.NAME));
    return nodeGroupStyle.makePersistent(null);
  }

  public static void create(NodeManager nodeMgr,
                            PersistentRecord parentNodeStyle,
                            List<Integer> childNodeStyleIds,
                            PersistentRecord createChildAcl,
                            int number,
                            int minimum,
                            int maximum)
      throws MirrorInstantiationException, MirrorFieldException, MirrorNoSuchRecordException
  {
    Table nodeStyles = nodeMgr.getNodeStyle().getTable();
    for (Integer id : childNodeStyleIds) {
      create(nodeMgr,
             parentNodeStyle,
             nodeStyles.getRecord(id),
             createChildAcl,
             number,
             minimum,
             maximum);
    }
  }

  public static void create(NodeManager nodeMgr,
                            Predicate<Integer> validParentNodeStyleIds,
                            List<Integer> childNodeStyleIds,
                            PersistentRecord createChildAcl,
                            int number,
                            int minimum,
                            int maximum)
      throws MirrorInstantiationException, MirrorFieldException, MirrorNoSuchRecordException
  {
    Table nodeStyles = nodeMgr.getNodeStyle().getTable();
    for (PersistentRecord nodeStyle : nodeStyles) {
      if (validParentNodeStyleIds.apply(Integer.valueOf(nodeStyle.getId()))) {
        create(nodeMgr, nodeStyle, childNodeStyleIds, createChildAcl, number, minimum, maximum);
      }
    }
  }
}
