package org.cord.node;

import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IdList;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.node.trigger.NodeGroupStylePostBooter;
import org.cord.node.view.AddNodeFactory;
import org.cord.sql.SqlUtil;
import org.cord.util.DebugConstants;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;

public class NodeGroupStyle
  extends NodeTableWrapper
{
  /**
   * "NodeGroupStyle"
   */
  public final static String TABLENAME = "NodeGroupStyle";

  /**
   * @see NodeDbConstants#NAME
   */
  public final static ObjectFieldKey<String> NAME = NodeDbConstants.NAME.copy();

  /**
   * "parentNodeStyle_id"
   */
  public final static IntFieldKey PARENTNODESTYLE_ID = IntFieldKey.create("parentNodeStyle_id");

  /**
   * "parentNodeStyle"
   */
  public final static RecordOperationKey<PersistentRecord> PARENTNODESTYLE =
      LinkField.getLinkName(PARENTNODESTYLE_ID);

  /**
   * "childNodeStyle_id"
   */
  public final static IntFieldKey CHILDNODESTYLE_ID = IntFieldKey.create("childNodeStyle_id");

  /**
   * "childNodeStyle"
   */
  public final static RecordOperationKey<PersistentRecord> CHILDNODESTYLE =
      LinkField.getLinkName(CHILDNODESTYLE_ID);

  /**
   * "createChildAcl_id"
   * 
   * @see Acl#CREATENODEGROUPSTYLES
   */
  public final static IntFieldKey CREATECHILDACL_ID = IntFieldKey.create("createChildAcl_id");

  /**
   * "createChildAcl"
   */
  public final static RecordOperationKey<PersistentRecord> CREATECHILDACL =
      LinkField.getLinkName(CREATECHILDACL_ID);

  /**
   * "updateCreateChildAcl_id"
   * 
   * @see Acl#UPDATECREATENODEGROUPSTYLES
   */
  public final static IntFieldKey UPDATECREATECHILDACL_ID =
      IntFieldKey.create("updateCreateChildAcl_id");

  /**
   * "updateCreateChildAcl"
   */
  public final static RecordOperationKey<PersistentRecord> UPDATECREATECHILDACL =
      LinkField.getLinkName(UPDATECREATECHILDACL_ID);

  /**
   * @see NodeDbConstants#NUMBER
   */
  public final static IntFieldKey NUMBER = NodeDbConstants.NUMBER.copy();

  /**
   * "minimum"
   */
  public final static IntFieldKey MINIMUM = IntField.createKey("minimum");

  /**
   * IntegerFieldFilter ("maximum") that contains the maximum number of Nodes permitted in an
   * implementation of the NodeGroup. If the count is MAXIMUM_UNLIMITED then the number is
   * unlimited.
   */
  public final static IntFieldKey MAXIMUM = IntField.createKey("maximum");

  /**
   * Value (-1) for MAXIMUM that denotes that an unlimited number of children may be created in this
   * style.
   * 
   * @see #MAXIMUM
   */
  public final static int MAXIMUM_UNLIMITED = -1;

  /**
   * "isUpdateLinked"
   */
  public final static ObjectFieldKey<Boolean> ISUPDATELINKED =
      BooleanField.createKey("isUpdateLinked");

  /**
   * "isDeleteLinked"
   */
  public final static ObjectFieldKey<Boolean> ISDELETELINKED =
      BooleanField.createKey("isDeleteLinked");

  /**
   * @see NodeDbConstants#NAMED
   */
  public final static MonoRecordSourceOperationKey<String, RecordSource> NAMED =
      NodeDbConstants.NAMED.copy();

  // /**
  // * RecordOperation ("nodeGroups") on NODEGROUPSTYLE that resolves to
  // * a QueryWrapper containing containing all the NODEGROUP records
  // * that implement this style.
  // **/
  // public final static String NODEGROUPS = "nodeGroups";
  public final static RecordOperationKey<RecordSource> NODES =
      RecordOperationKey.create("nodes", RecordSource.class);

  /**
   * The id of the NodeGroupStyle record that is taken as the root node. This is the only
   * NodeGroupStyle record that is permitted to have a non-linked value of PARENTNODESTYLE_ID.
   * 
   * @see #PARENTNODESTYLE_ID
   */
  public final static int ROOTNODE = 1;

  /**
   * Construct the NodeGroupStyle TableWrapper and register the default CreateNodePolicy elements.
   * The policies that are registered enforce that the user that is requesting the creation passes
   * the NodeGroupStyle.createChildAcl and also that the creation of the child doesn't go over the
   * limits set in NodeGroupStyle.maximum
   * 
   * @param nodeManager
   *          Compulsory
   * @see #addCreateNodePolicy(Integer, CreateNodePolicy)
   */
  protected NodeGroupStyle(final NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
    addCreateNodePolicy(null, new CreateNodePolicy() {
      @Override
      public Boolean isPermitted(PersistentRecord parentNode,
                                 PersistentRecord nodeGroupStyle,
                                 PersistentRecord user)
      {
        if (user == null) {
          return null;
        }
        return Boolean.valueOf(nodeManager.getAcl()
                                          .acceptsAclUserOwner(nodeGroupStyle.compInt(CREATECHILDACL_ID),
                                                               user.getId(),
                                                               parentNode.compInt(Node.OWNER_ID)));
      }

      @Override
      public String getRejectionMessage(PersistentRecord parentNode,
                                        PersistentRecord nodeGroupStyle,
                                        PersistentRecord user)
      {
        return null;
      }

      @Override
      public String toString()
      {
        return "NodeGroupStyle.createChild_acl";
      }

      @Override
      public String toString(PersistentRecord parentNode,
                             PersistentRecord nodeGroupStyle)
      {
        return toString() + "(" + nodeGroupStyle.comp(CREATECHILDACL) + ")";
      }

    });
    addCreateNodePolicy(null, new CreateNodePolicy() {
      @Override
      public Boolean isPermitted(PersistentRecord parentNode,
                                 PersistentRecord nodeGroupStyle,
                                 PersistentRecord user)
      {
        int maximum = nodeGroupStyle.compInt(MAXIMUM);
        if (maximum == NodeGroupStyle.MAXIMUM_UNLIMITED) {
          return Boolean.TRUE;
        }
        return Boolean.valueOf(nodeManager.getNode()
                                          .getChildNodesByNodeGroupStyle(parentNode.getId(),
                                                                         nodeGroupStyle.getId())
                                          .getIdList()
                                          .size() < maximum);
      }

      @Override
      public String getRejectionMessage(PersistentRecord parentNode,
                                        PersistentRecord nodeGroupStyle,
                                        PersistentRecord user)
      {
        return null;
      }

      @Override
      public String toString()
      {
        return "NodeGroupStyle.maximum";
      }

      @Override
      public String toString(PersistentRecord parentNode,
                             PersistentRecord nodeGroupStyle)
      {
        return toString() + "(" + nodeGroupStyle.compInt(MAXIMUM) + ")";
      }
    });
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NodeGroupStyle.NAME, "Name", 32, ""));
    table.addField(new LinkField(PARENTNODESTYLE_ID,
                                 "Is found inside pages of NodeStyle",
                                 null,
                                 PARENTNODESTYLE,
                                 NodeStyle.TABLENAME,
                                 NodeStyle.CHILDRENNODEGROUPSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED).setIgnoreId(NodeGroupStyle.ROOTNODE));
    table.addField(new LinkField(CHILDNODESTYLE_ID,
                                 "Will contain child pages of NodeStyle",
                                 null,
                                 CHILDNODESTYLE,
                                 NodeStyle.TABLENAME,
                                 NodeStyle.PARENTNODEGROUPSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(CREATECHILDACL_ID,
                                 "Create pages rights",
                                 null,
                                 CREATECHILDACL,
                                 Acl.TABLENAME,
                                 Acl.CREATENODEGROUPSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(UPDATECREATECHILDACL_ID,
                                 "Change create pages rights",
                                 null,
                                 UPDATECREATECHILDACL,
                                 Acl.TABLENAME,
                                 Acl.UPDATECREATENODEGROUPSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new IntField(NUMBER, "Ordering number"));
    table.addField(new IntField(MINIMUM, "Minimum children count"));
    table.addField(new IntField(MAXIMUM, "Maximum children count"));
    table.addField(new BooleanField(ISUPDATELINKED,
                                    "Is Transaction linked for updates?",
                                    Boolean.FALSE));
    table.addField(new BooleanField(ISDELETELINKED,
                                    "Is Transaction linked for deletions?",
                                    Boolean.FALSE));
    table.addRecordSourceOperation(new TableStringMatcher(NAMED,
                                                          NAME,
                                                          TableStringMatcher.SEARCH_EXACT,
                                                          null));
    table.addPostInstantiationTrigger(new NodeGroupStylePostBooter(getNodeManager(), pwd));
    table.addFieldTrigger(ImmutableFieldTrigger.create(PARENTNODESTYLE_ID));
    table.setTitleOperationName(NAME);
    table.setDefaultOrdering(TABLENAME + "." + PARENTNODESTYLE_ID + ", " + TABLENAME + "." + NUMBER
                             + ", " + TABLENAME + "." + NAME);
  }

  /**
   * Get the id of the first node that implements the given NodeGroupStyle. Equivalent to
   * $db.NodeGroupStyle.getRecord(id).nodes.getFirstRecordId()
   */
  public int getFirstNodeId(int nodeGroupStyle_id)
      throws MirrorNoSuchRecordException
  {
    return RecordSources.getFirstRecord(getTable().getRecord(nodeGroupStyle_id).opt(NODES), null)
                        .getId();
  }

  public static Integer getNodeGroupStyleId(Table nodeGroupStyle,
                                            String name)
      throws MirrorNoSuchRecordException
  {
    RecordSource nodeGroupStyles =
        TableStringMatcher.search(nodeGroupStyle, NodeGroupStyle.NAME, name, null);
    IdList ids = nodeGroupStyles.getIdList();
    if (ids.size() == 1) {
      return Integer.valueOf(ids.getInt(0));
    }
    throw new MirrorNoSuchRecordException(String.format("NodeGroupStyle.getNodeGroupStyleId(%s) --> %s",
                                                        name,
                                                        ids),
                                          nodeGroupStyle);
  }

  /**
   * Resolve the Query that finds all NodeGroupStyles that have a given parentNodeStyle_id and a
   * given childNodeStyle_id. This is utilised in the various getFirstInstance methods. The bulk of
   * the times in normally configured node sites, the Query will resolve to either zero or one
   * NodeGroupStyles. In situations where there is more than one matching NodeGroupStyle, the Query
   * will order by the default Table ordering.
   */
  public RecordSource getInstances(int parentNodeStyleId,
                                   int childNodeStyleId)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(TABLENAME + "." + PARENTNODESTYLE_ID + "=").append(parentNodeStyleId);
    buf.append(" and " + TABLENAME + "." + CHILDNODESTYLE_ID + "=").append(childNodeStyleId);
    String filter = buf.toString();
    return getTable().getQuery(filter, filter, null);
  }

  /**
   * Get the optional first NodeGroupStyle that binds parentNodeStyle_id and childNodeStyle_id
   * together, or null if there is no such record.
   * 
   * @return The appropriate NodeGroupStyle PersistentRecord or null if it doesn't exist.
   * @see #getCompFirstInstance(int, int, Viewpoint)
   */
  public PersistentRecord getOptFirstInstance(int parentNodeStyleId,
                                              int childNodeStyleId,
                                              Viewpoint viewpoint)
  {
    return RecordSources.getOptFirstRecord(getInstances(parentNodeStyleId, childNodeStyleId),
                                           viewpoint);
  }

  /**
   * Get the compulsory first NodeGroupStyle that binds parentNodeStyle_id and childNodeStyle_id
   * together, and throw a MissingRecordException if this isn't possible.
   * 
   * @return The appropriate NodeGroupStyle record. Never null
   * @throws MissingRecordException
   *           if the record cannot be found. clients are not expected to catch this.
   * @see #getOptFirstInstance(int, int, Viewpoint)
   */
  public PersistentRecord getCompFirstInstance(int parentNodeStyleId,
                                               int childNodeStyleId,
                                               Viewpoint viewpoint)
      throws MissingRecordException
  {
    return RecordSources.getCompFirstRecord(getInstances(parentNodeStyleId, childNodeStyleId),
                                            viewpoint,
                                            "No NodeGroupStyle found: parentNodeStyle_id=%s, childNodeStyle_id=%s",
                                            Integer.valueOf(parentNodeStyleId),
                                            Integer.valueOf(childNodeStyleId));
  }

  public PersistentRecord getInstance(int parentNodeStyleId,
                                      String name,
                                      Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    StringBuilder filter = new StringBuilder();
    filter.append("(NodeGroupStyle.parentNodeStyle_id=")
          .append(parentNodeStyleId)
          .append(") and (NodeGroupStyle.name=");
    SqlUtil.toSafeSqlString(filter, name);
    filter.append(")");
    PersistentRecord nodeGroupStyle =
        RecordSources.getOptFirstRecord(getTable().getQuery(null, filter.toString(), null),
                                        viewpoint);
    if (nodeGroupStyle == null) {
      throw new MirrorNoSuchRecordException(String.format("Cannot find NodeGroupStyle named \"%s\" under %s",
                                                          name,
                                                          Integer.toString(parentNodeStyleId)),
                                            getTable());
    }
    return nodeGroupStyle;
  }

  /**
   * Get a Query of all the permissable NodeGroupStyles that could bind the given parent and child
   * NodeStyle ids together.
   * 
   * @param parentNodeStyleId
   *          The id of the parent NodeStyle. Not null
   * @param childNodeStyleId
   *          The id of the child NodeStyle Node null
   * @return Query of NodeGroupStyles. This will normally have either 0 or 1 records in, but it may
   *         have more in very unusual NodeStyle hierarchies
   */
  public RecordSource getChildrenNodeGroupStyles(int parentNodeStyleId,
                                                 int childNodeStyleId)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("NodeGroupStyle.parentNodeStyle_id=")
       .append(parentNodeStyleId)
       .append(" and NodeGroupStyle.childNodeStyle_id=")
       .append(childNodeStyleId);
    String filter = buf.toString();
    return getTable().getQuery(null, filter, null);
  }

  private final ListMultimap<Integer, CreateNodePolicy> __createNodePolicies =
      ArrayListMultimap.create();

  /**
   * @return unmodifiable view of the CreateNodePolicies registered for each NodeStyle, with all
   *         NodeStyles represented by null.
   */
  public final ListMultimap<Integer, CreateNodePolicy> getCreateNodePolicies()
  {
    return Multimaps.unmodifiableListMultimap(__createNodePolicies);
  }

  // private final MapToList<Integer, CreateNodePolicy> __createNodePolicies =
  // new MapToList<Integer, CreateNodePolicy>();

  /**
   * Find which is the first registered CreateNodePolicy that has an expressed an interest in
   * nodeGroupStyle and which actively rejects the proposed new node. Please note that this doesn't
   * mean that this is the only CreateNodePolicy that rejects it, just the first...
   * 
   * @param parentNode
   *          compulsory
   * @param nodeGroupStyle
   *          compulsory
   * @param user
   *          optional
   * @param hasAuthDebugging
   *          set whether or not
   *          {@link DebugConstants#auth(PersistentRecord, String, Object, Boolean)} will be invoked
   *          - normally set it to false for methods that are just enquiries and true for methods
   *          that are for actually creating the Node to avoid log noise. Note that this will not
   *          actually log anything unless it is compiled with {@link DebugConstants#DEBUG_AUTH}
   *          enabled.
   * @return The actively rejecting CreateNodePolicy or null if all of the appropriate
   *         CreateNodePolicies either explicitly approve or have no opinion.
   */
  public CreateNodePolicy getMayCreateNodeRejection(PersistentRecord parentNode,
                                                    PersistentRecord nodeGroupStyle,
                                                    PersistentRecord user,
                                                    boolean hasAuthDebugging)
  {
    TransientRecord.assertIsTable(parentNode, getNodeManager().getNode());
    TransientRecord.assertIsTable(nodeGroupStyle, this);
    if (user != null) {
      TransientRecord.assertIsTable(user, getNodeManager().getUser());
    }
    List<CreateNodePolicy> policies = __createNodePolicies.get(null);
    if (policies != null) {
      for (CreateNodePolicy policy : policies) {
        Boolean isPermitted = policy.isPermitted(parentNode, nodeGroupStyle, user);
        if (hasAuthDebugging) {
          DebugConstants.auth(parentNode,
                              AddNodeFactory.NAME,
                              policy.toString(parentNode, nodeGroupStyle),
                              isPermitted);
        }
        if (Boolean.FALSE.equals(isPermitted)) {
          return policy;
        }
      }
    }
    policies = __createNodePolicies.get(Integer.valueOf(nodeGroupStyle.getId()));
    if (policies != null) {
      for (CreateNodePolicy policy : policies) {
        Boolean isPermitted = policy.isPermitted(parentNode, nodeGroupStyle, user);
        if (hasAuthDebugging) {
          DebugConstants.auth(parentNode,
                              AddNodeFactory.NAME,
                              policy.toString(parentNode, nodeGroupStyle),
                              isPermitted);
        }
        if (Boolean.FALSE.equals(isPermitted)) {
          return policy;
        }
      }
    }
    return null;
  }

  /**
   * Invoke
   * {@link #getMayCreateNodeRejection(PersistentRecord, PersistentRecord, PersistentRecord, boolean)}
   * with no AUTH debugging.
   */
  public CreateNodePolicy getMayCreateNodeRejection(PersistentRecord parentNode,
                                                    PersistentRecord nodeGroupStyle,
                                                    PersistentRecord user)
  {
    return getMayCreateNodeRejection(parentNode, nodeGroupStyle, user, false);
  }

  /**
   * Check to see if all the registered CreateNodePolicy elements that have expressed an interest in
   * nodeGroupStyle approve of user adding a new Node under parentNode.
   * 
   * @param parentNode
   *          not null
   * @param nodeGroupStyle
   *          not null
   * @param user
   *          optional
   * @return false if any of the policies explicitly reject the new Node
   */
  public boolean mayCreateNode(PersistentRecord parentNode,
                               PersistentRecord nodeGroupStyle,
                               PersistentRecord user)
  {
    try {
      return getMayCreateNodeRejection(parentNode, nodeGroupStyle, user, false) == null;
    } catch (RuntimeException rEx) {
      rEx.printStackTrace();
      throw rEx;
    }
  }

  /**
   * Add a given CreateNodePolicy to the list of active policies that will be asked for approval
   * when a Node wants to be created.
   * 
   * @param targettedNodeGroupStyleId
   *          The NodeGroupStyle that the policy wants to be asked about new Node creations. If this
   *          is null then the policy will be asked about all Node creations.
   * @param policy
   *          The policy in question. Not null.
   */
  public void addCreateNodePolicy(Integer targettedNodeGroupStyleId,
                                  CreateNodePolicy policy)
  {
    Preconditions.checkNotNull(policy, "policy");
    __createNodePolicies.put(targettedNodeGroupStyleId, policy);
  }

  /**
   * Interface describing Objects that are capable of having a policy as to whether a new Node in a
   * given nodeGroupStyle under a given parentNode is allowed to be create by a given user. These
   * are managed by NodeGroupStyle and new Nodes are only permitted if all the relevant
   * CreateNodePolicy either return null (no opinion) or TRUE from isPermitted. If any return FALSE
   * then the request is rejected.
   * 
   * @author alex
   * @see #isPermitted(PersistentRecord, PersistentRecord, PersistentRecord)
   * @see NodeGroupStyle#addCreateNodePolicy(Integer, CreateNodePolicy)
   * @see NodeGroupStyle#mayCreateNode(PersistentRecord, PersistentRecord, PersistentRecord)
   * @see NodeGroupStyle#getMayCreateNodeRejection(PersistentRecord, PersistentRecord,
   *      PersistentRecord)
   */
  public interface CreateNodePolicy
  {
    /**
     * Evaluate whether this CreateNodePolicy permits the creation of a new Node in the given
     * circumstances
     * 
     * @param parentNode
     *          The parent Node of the proposed new Node. Not null.
     * @param nodeGroupStyle
     *          The NodeGroupStyle which the new Node wants to be created in. Not null.
     * @param user
     *          The User who is performing the creation. May be null in situations when the Node
     *          creation is taking place internally to the system (as opposed to an operation that
     *          has been triggered by an external User)
     * @return TRUE if the creation is explicitly OK. FALSE if this Policy forbids the creation.
     *         null if this Policy has no opinion on the circumstances.
     */
    public Boolean isPermitted(PersistentRecord parentNode,
                               PersistentRecord nodeGroupStyle,
                               PersistentRecord user);

    /**
     * If this CreateNodePolicy has a message that should be imparted to users as to why they are
     * not going to be given an opportunity to create the requested node then it should be generated
     * by this method.
     * 
     * @param parentNode
     *          The parent Node of the proposed new Node. Not null.
     * @param nodeGroupStyle
     *          The NodeGroupStyle which the new Node wants to be created in. Not null.
     * @param user
     *          The User who is performing the creation. May be null in situations when the Node
     *          creation is taking place internally to the system (as opposed to an operation that
     *          has been triggered by an external User)
     * @return The message that should be returned to the user as to why this isn't permitted or
     *         null if you don't have any message to pass across.
     */
    public String getRejectionMessage(PersistentRecord parentNode,
                                      PersistentRecord nodeGroupStyle,
                                      PersistentRecord user);

    /**
     * Return a String representation of the CreateNodePolicy that includes any of the elements on
     * parentNode and nodeGroupStyle that would be taken into consideration when the policy is
     * invoked. This is intended for
     * {@link DebugConstants#auth(PersistentRecord, String, Object, Boolean)} generation.
     */
    public String toString(PersistentRecord parentNode,
                           PersistentRecord nodeGroupStyle);

  }
}
