package org.cord.node;

import org.cord.util.Gettable;
import org.cord.util.ObjectUtil;

/**
 * Static utility class for doing useful things with Authenticator implementations.
 */
public class Authenticators
{
  /**
   * Create a new instance of the Authenticator with the given className. This will fail if the
   * Class is not found, it isn't an Authenticator or if it doesn't have a zero argument
   * constructor.
   */
  public static Authenticator getInstance(String className)
      throws ClassCastException, ClassNotFoundException, InstantiationException,
      IllegalAccessException
  {
    return ObjectUtil.castTo(Class.forName(className).newInstance(), Authenticator.class);
  }

  /**
   * Look up the className for the Authenticator in the Gettable and then initialise the
   * Authenticator if it is defined. If it is defined but cannot be initialised then an exception
   * will be thrown.
   * 
   * @return The Authenticator if the key maps to a class name, or null if there is no defined
   *         class.
   */
  public static Authenticator getOptInstance(Gettable gettable,
                                             String key)
      throws ClassCastException, ClassNotFoundException, InstantiationException,
      IllegalAccessException
  {
    String className = gettable.getString(key);
    return className == null ? null : getInstance(className);
  }
}
