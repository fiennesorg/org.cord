package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.servlet.http.HttpSession;

import org.cord.node.app.AppComponentImpl;
import org.cord.node.app.AppMgr;
import org.cord.util.ObjectUtil;
import org.webmacro.ResourceException;
import org.webmacro.Template;

public class MessageMgr<A extends AppMgr>
  extends AppComponentImpl<A>
{
  private final String __messageWrapperTemplatePath;

  public MessageMgr(A appMgr,
                    String messageWrapperTemplatePath)
  {
    super(appMgr);
    __messageWrapperTemplatePath = checkNotNull(messageWrapperTemplatePath);
  }

  public static final String SESSION_MESSAGES = "MessageMgr__Messages";

  /**
   * Resolve the Messages from the HttpSession, creating and storing a new instance if none exists.
   * 
   * @param session
   *          The compulsory HttpSession that wraps the Messages for the current user
   * @return The Messages stack, never null
   */
  public Messages<A> getMessages(HttpSession session)
  {
    Messages<A> messages = ObjectUtil.castTo(session.getAttribute(SESSION_MESSAGES),
                                             ObjectUtil.<Messages<A>> castClass(Messages.class));
    if (messages == null) {
      messages = createMessages();
      session.setAttribute(SESSION_MESSAGES, messages);
    }
    return messages;
  }

  public Messages<A> createMessages()
  {
    return new Messages<A>(this);
  }

  public String getMessageWrapperTemplatePath()
  {
    return __messageWrapperTemplatePath;
  }

  public Template getMessageWrapperTemplate()
      throws ResourceException
  {
    return getNodeMgr().getWebMacro().getTemplate(getMessageWrapperTemplatePath());
  }
}
