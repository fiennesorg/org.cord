package org.cord.node.messages;

public enum MessageStatus {
  ERROR,
  WARNING,
  MESSAGE;
}
