package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.regex.Pattern;

import org.cord.node.app.AppMgr;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

public abstract class Message<A extends AppMgr>
  extends MessageComponent<A>
{
  public static final String GROUPINGKEY_GLOBAL = "GroupingKey__Global";
  
  private final MessageStatus __messageStatus;
  private final String __groupingKey;
  private final String __title;

  public Message(MessageMgr<A> messageMgr,
                 MessageStatus messageStatus,
                 String groupingKey,
                 String title)
  {
    super(messageMgr);
    __messageStatus = checkNotNull(messageStatus);
    __groupingKey = checkNotNull(groupingKey);
    __title = title;
  }

  public final String getTitle()
  {
    return __title;
  }

  public final String getGroupingKey()
  {
    return __groupingKey;
  }

  public final boolean matchesGroupingKey(Pattern pattern)
  {
    return pattern.matcher(getGroupingKey()).matches();
  }

  public MessageStatus getMessageStatus()
  {
    return __messageStatus;
  }

  public abstract String renderContents(Context context)
      throws PropertyException, ResourceException;

  public static final String WM_MESSAGE = "Message";

  public String renderWrapper(Context context)
      throws PropertyException, ResourceException
  {
    context.put(WM_MESSAGE, this);
    String wrappedMessage = getMessageMgr().getMessageWrapperTemplate().evaluateAsString(context);
    context.remove(WM_MESSAGE);
    return wrappedMessage;
  }
}
