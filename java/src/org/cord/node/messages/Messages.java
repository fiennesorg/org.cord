package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.cord.node.app.AppMgr;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class Messages<A extends AppMgr>
extends MessageComponent<A>
{
private List<Message<A>> __messages = Lists.newLinkedList();

public Messages(MessageMgr<A> messageMgr)
{
  super(messageMgr);
}

public int size()
{
  return __messages.size();
}

public void addMessage(Message<A> message)
{
  __messages.add(checkNotNull(message));
}

@Override
public String toString()
{
  return MoreObjects.toStringHelper(this).add("Messages", __messages).toString();
}

public List<Message<A>> getAllMessages()
{
  return Collections.unmodifiableList(__messages);
}

public List<Message<A>> removeAllMessages()
{
  List<Message<A>> messages = Lists.newArrayList(__messages);
  __messages.clear();
  return messages;
}

public String renderAllMessages(Context context)
    throws PropertyException, ResourceException
{
  return render(removeAllMessages(), context);
}

public List<Message<A>> getFilteredMessages(String groupingKeyRegex)
{
  final Pattern groupingKeyPattern = Pattern.compile(groupingKeyRegex);
  List<Message<A>> messages =
      Lists.newArrayList(Iterables.filter(getAllMessages(), new Predicate<Message<A>>() {
        @Override
        public boolean apply(Message<A> message)
        {
          return message.matchesGroupingKey(groupingKeyPattern);
        }
      }));
  return messages;
}

public List<Message<A>> removeFilteredMessages(String groupingKeyRegex)
{
  List<Message<A>> messages = getFilteredMessages(groupingKeyRegex);
  __messages.removeAll(messages);
  return messages;
}

public String renderFilteredMessages(String groupingKeyRegex,
                                     Context context)
    throws PropertyException, ResourceException
{
  return render(removeFilteredMessages(groupingKeyRegex), context);
}

private String render(Iterable<Message<A>> messages,
                      Context context)
    throws PropertyException, ResourceException
{
  StringBuilder buf = new StringBuilder();
  for (Message<?> message : messages) {
    buf.append(message.renderWrapper(context));
  }
  return buf.toString();
}
}
