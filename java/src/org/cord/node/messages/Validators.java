package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.node.app.AppMgr;
import org.cord.node.app.AppView;
import org.cord.util.DebugConstants;
import org.cord.util.EmailValidator;
import org.cord.util.NumberUtil;

import com.google.common.base.Enums;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;

public class Validators
{
  /**
   * Create a new Predicate that returns true if the input is null, "" or passes the
   * definedPredicate. This lets you implement filters that support optional values but if they are
   * defined then they must be of a given format.
   */
  public static Predicate<String> isUndefinedOr(final Predicate<Object> definedPredicate)
  {
    checkNotNull(definedPredicate);
    return new Predicate<String>() {
      @Override
      public boolean apply(String input)
      {
        if (Strings.isNullOrEmpty(input)) {
          return true;
        }
        return definedPredicate.apply(input);
      }
    };
  }

  public static Predicate<String> ISNOTNULLOREMPTYSTRING = new Predicate<String>() {
    @Override
    public boolean apply(String input)
    {
      return input == null ? false : !Strings.isNullOrEmpty(input);
    }

    @Override
    public String toString()
    {
      return "Text with at least 1 character";
    };
  };

  public static Predicate<String> ISVALIDEMAIL = new Predicate<String>() {
    @Override
    public boolean apply(String input)
    {
      return input == null ? false : EmailValidator.getDottedInstance().isValid(input);
    }

    @Override
    public String toString()
    {
      return "Valid email address";
    };
  };

  public static Predicate<Object> ISINTEGER = new Predicate<Object>() {
    @Override
    public boolean apply(Object input)
    {
      return NumberUtil.toInteger(input, null) != null;
    }

    @Override
    public String toString()
    {
      return "Integer";
    };
  };

  public static Predicate<Object> ISPOSITIVEINTEGER = new Predicate<Object>() {
    @Override
    public boolean apply(Object input)
    {
      return NumberUtil.toInt(input, 0) > 0;
    }

    @Override
    public String toString()
    {
      return "Integer greater than 0";
    };
  };

  public static Predicate<Object> ISNOTNEGATIVEINTEGER = new Predicate<Object>() {
    @Override
    public boolean apply(Object input)
    {
      return NumberUtil.toInt(input, -1) >= 0;
    }

    @Override
    public String toString()
    {
      return "Integer of 0 or greater";
    }
  };

  public static Predicate<Object> ISDOUBLE = new Predicate<Object>() {
    @Override
    public boolean apply(Object input)
    {
      return NumberUtil.toDouble(input, null) != null;
    }

    @Override
    public String toString()
    {
      return "Decimal";
    };
  };

  public static <T extends Enum<T>> Predicate<Object> ISVALIDENUM(final Class<T> enumClass)
  {
    return new Predicate<Object>() {
      @Override
      public boolean apply(Object input)
      {
        if (input == null) {
          return false;
        }
        if (input instanceof Enum<?>) {
          input = ((Enum<?>) input).name();
        }
        Optional<T> value = Enums.getIfPresent(enumClass, input.toString());
        return value.isPresent();
      }
    };
  }

  public static Predicate<Object> ISINTEGERRANGE(final Optional<Integer> minimum,
                                                 final Optional<Integer> maximum)
  {
    return new Predicate<Object>() {
      @Override
      public boolean apply(Object input)
      {
        Integer integer = NumberUtil.toInteger(input, null);
        if (integer != null) {
          if (minimum.isPresent() && integer.intValue() < minimum.get().intValue()) {
            return false;
          }
          if (maximum.isPresent() && integer.intValue() > maximum.get().intValue()) {
            return false;
          }
          return true;
        }
        return false;
      }
    };
  }

  /**
   * @param messageValueText
   *          The text that will be returned in the error Message if the validation fails. This will
   *          be parametric formatted with value available as %s.
   */
  public static <A extends AppMgr, T> T validate(T value,
                                                 Predicate<T> isValid,
                                                 Messages<A> messages,
                                                 String messageGroupingKey,
                                                 String messageTitle,
                                                 String messageValueText)
  {
    DebugConstants.method("Validators", "validate", value, isValid, messages, messageGroupingKey, messageValueText);
    if (!isValid.apply(value)) {
      DebugConstants.DEBUG_OUT.println("FAILED");
      messages.addMessage(new StringMessage<A>(messages.getMessageMgr(),
                                               MessageStatus.ERROR,
                                               messageGroupingKey,
                                               messageTitle,
                                               String.format(messageValueText, value)));
    }
    return value;
  }

  /**
   * @param messageValueText
   *          The text that will be returned in the error Message if the validation fails. This will
   *          be parametric formatted with value available as %s.
   */
  public static <A extends AppMgr, T> boolean isValid(T value,
                                                      Predicate<T> isValid,
                                                      Messages<A> messages,
                                                      String messageGroupingKey,
                                                      String messageTitle,
                                                      String messageValueText)
  {
    if (!isValid.apply(value)) {
      messages.addMessage(new StringMessage<A>(messages.getMessageMgr(),
                                               MessageStatus.ERROR,
                                               messageGroupingKey,
                                               messageTitle,
                                               String.format(messageValueText, value)));
      return false;
    }
    return true;
  }

  public static <A extends AppMgr> String getMessageGroupingKey(AppView<A> appView,
                                                                String key)
  {
    return appView.getName() + "-" + key;
  }
}
