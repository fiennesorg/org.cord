package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.node.app.AppMgr;
import org.cord.util.ExceptionUtil;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects;

public class ExceptionMessage<A extends AppMgr>
  extends Message<A>
{
  private final String __text;
  private final Exception __exception;

  public ExceptionMessage(MessageMgr<A> messageMgr,
                          MessageStatus messageStatus,
                          String groupingKey,
                          String title,
                          String text,
                          Exception exception)
  {
    super(messageMgr,
          messageStatus,
          groupingKey,
          title);
    __text = checkNotNull(text);
    __exception = checkNotNull(exception);
  }

  @Override
  public String renderContents(Context context)
      throws PropertyException, ResourceException
  {
    StringBuilder buf = new StringBuilder();
    buf.append(__text)
       .append("<br/>\n<pre>")
       .append(ExceptionUtil.printStackTrace(__exception))
       .append("</pre>");
    return buf.toString();
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("groupingKey", getGroupingKey())
                      .add("text", __text)
                      .add("exception", __exception)
                      .toString();
  }
}
