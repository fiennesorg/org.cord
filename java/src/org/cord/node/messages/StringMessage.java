package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.node.app.AppMgr;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;

import com.google.common.base.MoreObjects;

public class StringMessage<A extends AppMgr>
  extends Message<A>
{
  private final String __text;

  public StringMessage(MessageMgr<A> messageMgr,
                       MessageStatus messageStatus,
                       String groupingKey,
                       String title,
                       String text)
  {
    super(messageMgr,
          messageStatus,
          groupingKey,
          title);
    __text = checkNotNull(text);
  }

  @Override
  public String renderContents(Context context)
      throws PropertyException, ResourceException
  {
    return __text;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("groupingKey", getGroupingKey())
                      .add("text", __text)
                      .toString();
  }
}
