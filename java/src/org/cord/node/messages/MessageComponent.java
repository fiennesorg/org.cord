package org.cord.node.messages;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.node.app.AppComponentImpl;
import org.cord.node.app.AppMgr;

public class MessageComponent<A extends AppMgr>
extends AppComponentImpl<A>
{
private final MessageMgr<A> __messageMgr;

public MessageComponent(MessageMgr<A> messageMgr)
{
  super(messageMgr.getAppMgr());
  __messageMgr = checkNotNull(messageMgr);
}

public final MessageMgr<A> getMessageMgr()
{
  return __messageMgr;
}
}
