package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.NamedImpl;

/**
 * ContestedLockFilter that permits anyone logged in as a given User to steal the lock from another
 * session that is logged in as the same User.
 */
public class SameUserLockFilter
  extends NamedImpl
  implements ContestedLockFilter
{
  public final static String NAME = "sameUserLockFilter";

  private final boolean _appliesToAnonymous;

  public SameUserLockFilter(boolean appliesToAnonymous)
  {
    super(NAME);
    _appliesToAnonymous = appliesToAnonymous;
  }

  public SameUserLockFilter()
  {
    this(false);
  }

  /**
   * @return STEAL_LOCK if the requesting and locked Users have the same id, otherwise UNDECIDED
   */
  @Override
  public int resolveContestedLock(NodeRequest contestedRequest,
                                  PersistentRecord contestedRecord,
                                  PersistentRecord lockedUser,
                                  Transaction lockedTransaction)
  {
    if (lockedUser.getId() == contestedRequest.getUserId()) {
      if (lockedUser.getId() != User.ID_ANONYMOUS | _appliesToAnonymous) {
        return STEAL_LOCK;
      }
    }
    return UNDECIDED;
  }
}
