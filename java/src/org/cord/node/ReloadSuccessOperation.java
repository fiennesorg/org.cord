package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

public class ReloadSuccessOperation
  implements PostViewAction
{
  private final ValueFactory __reloadView;

  /**
   * Singleton ReloadSuccessOperation that causes the reload to invoke the default view for the
   * system. This is equivalent to creating a new ReloadSuccessOperation(null).
   */
  public final static ReloadSuccessOperation DEFAULT_RELOAD =
      new ReloadSuccessOperation((String) null);

  public final static String CGI_RELOADVIEW = "reloadView";

  /**
   * Singleton ReloadSuccessOperation that causes the reload to invoke a view passed in as a
   * parameter named CGI_RELOADVIEW on the incoming NodeRequest. If it is not defined then it is
   * functionally identical to DEFAULT_RELOAD.
   * 
   * @see #CGI_RELOADVIEW
   * @see #DEFAULT_RELOAD
   */
  public final static ReloadSuccessOperation DEFAULT_NODEREQUEST_RELOAD =
      new ReloadSuccessOperation(new NodeRequestValueFactory(CGI_RELOADVIEW, null));

  public ReloadSuccessOperation(ValueFactory reloadView)
  {
    __reloadView = reloadView;
  }

  public ReloadSuccessOperation(String reloadView)
  {
    this(new ConstantValueFactory(reloadView));
  }

  @Override
  public void shutdown()
  {
  }

  /**
   * @throws ReloadException
   *           Always, with the defined reloadView as specified in the creator.
   */
  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws NodeRequestException
  {
    try {
      throw new ReloadException(node,
                                __reloadView.getValue(nodeRequest.getNodeManager(), nodeRequest),
                                nestedException);
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(null,
                                       node,
                                       e,
                                       true,
                                       true,
                                       "It has not been possible to resolve the target URL in this redirection");
    }
  }
}
