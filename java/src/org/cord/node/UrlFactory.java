package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;

public abstract class UrlFactory
  implements ValueFactory
{
  public UrlFactory()
  {
  }

  protected abstract int getNodeId(NodeManager nodeManager,
                                   NodeRequest nodeRequest)
      throws MirrorNoSuchRecordException;

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
      throws MirrorNoSuchRecordException
  {
    return nodeManager.getNode()
                      .getTable()
                      .getRecord(getNodeId(nodeManager, nodeRequest))
                      .comp(Node.URL);
  }

  @Override
  public void shutdown()
  {
  }
}
