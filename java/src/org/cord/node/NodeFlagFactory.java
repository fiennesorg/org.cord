package org.cord.node;

import org.cord.mirror.TransientRecord;

/**
 * An object that is capable of returning a flag based around some current state of a given Node.
 * 
 * @author alex
 */
public interface NodeFlagFactory
{
  /**
   * @return Boolean.TRUE or Boolean.FALSE if this NodeFlagFactory has an opinion on the node,
   *         otherwise null if there is no information.
   */
  public Boolean getFlag(TransientRecord node);
}
