package org.cord.node.grid;

import java.io.IOException;

import org.cord.util.StringUtils;

public abstract class AbstractStringBuilder
{
  public static final void stringsLn(Appendable buf,
                                     String s)
      throws IOException
  {
    buf.append(s);
    ln(buf);
  }

  public static final void stringsLn(Appendable buf,
                                     String s1,
                                     String s2)
      throws IOException
  {
    buf.append(s1).append(s2);
    ln(buf);
  }

  public static final void stringsLn(Appendable buf,
                                     String s1,
                                     String s2,
                                     String s3)
      throws IOException
  {
    buf.append(s1).append(s2).append(s3);
    ln(buf);
  }

  public static final void stringsLn(Appendable buf,
                                     String s1,
                                     String s2,
                                     String s3,
                                     String s4)
      throws IOException
  {
    buf.append(s1).append(s2).append(s3).append(s4);
    ln(buf);
  }

  public static final void stringsLn(Appendable buf,
                                     String s1,
                                     String s2,
                                     String s3,
                                     String s4,
                                     String s5)
      throws IOException
  {
    buf.append(s1).append(s2).append(s3).append(s4).append(s5);
    ln(buf);
  }

  public static final void ln(Appendable buf)
      throws IOException
  {
    buf.append('\n');
  }

  public static final void stringsLn(Appendable buf,
                                     Object... objs)
      throws IOException
  {
    for (int i = 0; i < objs.length; i++) {
      buf.append(StringUtils.toString(objs[i]));
    }
    ln(buf);
  }

}
