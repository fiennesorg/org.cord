package org.cord.node.grid;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;

import org.cord.util.HtmlUtilsTheme;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Macro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class GridDimensionMgr
{
  public static final String WM_GRID_WIDTH = "grid_width";
  public static final String WM_GRID_DIMENSION = "grid_dimension";
  public static final String WM_GRID_DIMENSIONMGR = "grid_dimensionMgr";

  private final LinkedList<GridDimension> __dimensions = new LinkedList<GridDimension>();

  private final Grid __grid;

  public GridDimensionMgr(Grid grid,
                          int contentWidth)
  {
    __grid = Preconditions.checkNotNull(grid, "grid");
    __dimensions.add(GridDimension.getInstance(this, null, 0, contentWidth, 0, 0, null));
  }

  public final Grid getGrid()
  {
    return __grid;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).addValue(__dimensions).toString();
  }

  public Macro open(int marginLeft,
                    int contentWidth,
                    int marginRight)
  {
    final GridDimension gridDimension = GridDimension.getInstance(this,
                                                                  getCurrent(),
                                                                  marginLeft,
                                                                  contentWidth,
                                                                  marginRight,
                                                                  0,
                                                                  null);
    __dimensions.addLast(gridDimension);
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        gridDimension.put(context);
      }
    };
  }

  public Macro openPadded(int marginLeft,
                          int contentWidth,
                          int marginRight,
                          int padding)
  {
    final GridDimension gridDimension = GridDimension.getInstance(this,
                                                                  getCurrent(),
                                                                  marginLeft,
                                                                  contentWidth,
                                                                  marginRight,
                                                                  0,
                                                                  Integer.valueOf(padding));
    __dimensions.addLast(gridDimension);
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        gridDimension.put(context);
      }
    };
  }

  public Macro openPadded(int contentWidth,
                          int padding)
  {
    return openPadded(0, contentWidth, 0, padding);
  }

  /**
   * Open a new dimension level that has the same contentWidth as it's immediate parent with the
   * addition of the specified amount of padding. This will fail if the immediate parent already
   * defines any explicit padding.
   */
  public Macro openPadded(int padding)
  {
    return openPadded(getCurrent().getContentWidth(), padding);
  }

  private String getGridDivClass(int width)
  {
    return "grid_" + width;
  }

  private String getTableGridDivClass(int width)
  {
    return "gntml_grid_" + width;
  }

  /**
   * Create an empty spacer div of the given number of grid units wide.
   */
  public String spacerDiv(int contentWidth)
  {
    return "<div class=\"grid_" + contentWidth + "\"></div>\n";
  }

  public Macro openDivDiv(int contentWidth,
                          final String outerDivClass,
                          final String innerDivClass)
  {
    final GridDimension gridDimension = GridDimension.getInstance(this,
                                                                  getCurrent(),
                                                                  0,
                                                                  contentWidth,
                                                                  0,
                                                                  innerDivClass == null ? 1 : 2,
                                                                  null);
    __dimensions.addLast(gridDimension);
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        gridDimension.put(context);
        String c = getGridDivClass(gridDimension.getContentWidth());
        if (outerDivClass != null) {
          c = c + " " + outerDivClass;
        }
        HtmlUtilsTheme.openDivOfClass(out, c);
        if (innerDivClass != null) {
          HtmlUtilsTheme.openDivOfClass(out, innerDivClass);
        }
      }
    };
  }

  public Macro openDiv(int contentWidth,
                       String outerDivClass)
  {
    return openDivDiv(contentWidth, outerDivClass, null);
  }

  public Macro openDiv(int contentWidth)
  {
    return openDivDiv(contentWidth, null, null);
  }

  public Macro openDivDiv(int contentWidth,
                          String innerClass)
  {
    return openDivDiv(contentWidth, null, innerClass);
  }

  public Macro openInnerRowDiv(int contentWidth)
  {
    return openDivDiv(contentWidth, null, Grid.GRID_INNERROW);
  }

  public GridDimension getCurrent()
  {
    return __dimensions.getLast();
  }

  public Macro open(int contentWidth)
  {
    return open(0, contentWidth, 0);
  }

  public Macro close()
  {
    if (__dimensions.size() == 1) {
      throw new NoSuchElementException("You are not permitted to close the containing GridDimension");
    }
    final GridDimension closed = getCurrent();
    __dimensions.removeLast();
    final GridDimension current = getCurrent();
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        for (int i = 0; i < closed.getDivCount(); i++) {
          out.append("</div>\n");
        }
        context.put(WM_GRID_WIDTH, Integer.valueOf(current.getContentWidth()));
        context.put(WM_GRID_DIMENSION, current);
      }
    };
  }
}
