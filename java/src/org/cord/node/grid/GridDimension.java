package org.cord.node.grid;

import java.util.Map;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class GridDimension
{
  protected static GridDimension getInstance(GridDimensionMgr gridDimensionMgr,
                                             GridDimension container,
                                             int marginLeft,
                                             int contentWidth,
                                             int marginRight,
                                             int divCount,
                                             Integer padding)
  {
    Preconditions.checkState(marginLeft >= 0,
                             "%s is an invalid marginLeft",
                             Integer.valueOf(marginLeft));
    Preconditions.checkState(contentWidth >= 1,
                             "%s is an invalid contentWidht",
                             Integer.valueOf(contentWidth));
    Preconditions.checkState(marginRight >= 0,
                             "%s is an invalid marginRight",
                             Integer.valueOf(marginRight));
    Preconditions.checkState(divCount >= 0, "%s is an invalid divCount", Integer.valueOf(divCount));
    if (container != null) {
      Preconditions.checkState(container.getPadding() == null,
                               "You cannot nest a GridDimension inside %s which has defined padding",
                               container);
      int totalWidth = getTotalWidth(marginLeft, contentWidth, marginRight);
      while (totalWidth > container.getTotalWidth()) {
        if (marginLeft > 0 & marginLeft >= marginRight) {
          marginLeft--;
        } else if (marginRight > 0 & marginRight >= marginLeft) {
          marginRight--;
        } else if (contentWidth > 1) {
          contentWidth--;
        }
        totalWidth = getTotalWidth(marginLeft, contentWidth, marginRight);
      }
    }
    return new GridDimension(gridDimensionMgr,
                             marginLeft,
                             contentWidth,
                             marginRight,
                             divCount,
                             padding);
  }

  public static int getTotalWidth(int marginLeft,
                                  int contentWidth,
                                  int marginRight)
  {
    return marginLeft + contentWidth + marginRight;
  }

  private final GridDimensionMgr __gridDimensionMgr;
  private final int __contentWidth;
  private final int __marginLeft;
  private final int __marginRight;
  private final int __divCount;
  private final Integer __padding;

  private GridDimension(GridDimensionMgr gridDimensionMgr,
                        int marginLeft,
                        int contentWidth,
                        int marginRight,
                        int divCount,
                        Integer padding)
  {
    __gridDimensionMgr = gridDimensionMgr;
    __marginLeft = marginLeft;
    __contentWidth = contentWidth;
    __marginRight = marginRight;
    __divCount = divCount;
    __padding = padding;
  }

  /**
   * @return the total amount of padding if explicitly defined or null if there is no padding.
   */
  public final Integer getPadding()
  {
    return __padding;
  }

  public final GridDimensionMgr getGridDimensionMgr()
  {
    return __gridDimensionMgr;
  }

  public void put(Map<Object, Object> context)
  {
    context.put(GridDimensionMgr.WM_GRID_WIDTH, Integer.valueOf(getContentWidth()));
    context.put(GridDimensionMgr.WM_GRID_DIMENSION, this);
  }

  @Override
  public String toString()
  {
    if (__padding != null) {
      return MoreObjects.toStringHelper(this)
                        .addValue(__marginLeft)
                        .addValue(__contentWidth)
                        .add("padding", __padding)
                        .addValue(__marginRight)
                        .toString();
    }
    return MoreObjects.toStringHelper(this)
                      .addValue(__marginLeft)
                      .addValue(__contentWidth)
                      .addValue(__marginRight)
                      .toString();
  }

  public int getContentWidth()
  {
    return __contentWidth;
  }

  /**
   * Get the width of the content region of this GridDimension in pixels according to the Grid that
   * was passed to the GridDimensionMgr when it was created taking into account any defined padding
   * on this GridDimension.
   */
  public int getContentWidthPx()
  {
    int w = getGridDimensionMgr().getGrid().getWidth(getContentWidth());
    Integer padding = getPadding();
    return padding == null ? w : w - padding.intValue();
  }

  public int getMarginLeft()
  {
    return __marginLeft;
  }

  public int getMarginRight()
  {
    return __marginRight;
  }

  public int getTotalWidth()
  {
    return getTotalWidth(__marginLeft, __contentWidth, __marginRight);
  }

  public int getDivCount()
  {
    return __divCount;
  }
}
