package org.cord.node.grid;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.StringBuilderIOException;

public class GntmlGrid
  extends AbstractStringBuilder
{
  public final Grid __grid;
  private final int __zoomCols;

  private int _gntmlStyle_id = 0;

  public GntmlGrid(Grid grid,
                   int zoomCols)
  {
    __grid = grid;
    __zoomCols = zoomCols;
  }

  /**
   * Boot the GntmlGrid from the NodeConfig with the namespace of "node.grid". The zoomCols will be
   * defined by "node.grid.zoomCols"
   * 
   * @see Grid#Grid(Gettable, Gettable)
   */
  public GntmlGrid(Gettable params,
                   Gettable defaults)
  {
    this(new Grid(params, defaults),
         Grid.getInt(Grid.GET_HEADER + "zoomCols", params, defaults));
  }

  @Override
  public String toString()
  {
    return String.format("GntmlGrid(%s)", __grid);
  }

  public String generateSql()
  {
    StringBuilder buf = new StringBuilder();
    try {
      // appendSingleImageStyles(buf);
      appendGntmlStyles(buf);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  // public int toSingleImageStyleId(int cols)
  // {
  // return cols + 100;
  // }

  // private void appendSingleImageStyle(StringBuilder buf,
  // int cols)
  // throws IOException
  // {
  // int width = __grid.getWidth(cols);
  // stringLn(buf,
  // String.format("insert into SingleImageStyle values
  // (%s,'grid_%s',1,0,'default.jpg',%s,%s,%s,%s,3,'/dyn/','',1,5,0,'img_%s');",
  // toSingleImageStyleId(cols),
  // cols,
  // width,
  // Math.min(width, 100),
  // width,
  // width * 2,
  // cols));
  // }

  // private void appendSingleImageStyles(StringBuilder buf)
  // throws IOException
  // {
  // for (int i = 1; i <= __grid.getColumnCount(); i++) {
  // appendSingleImageStyle(buf, i);
  // }
  // ln(buf);
  // }

  // private void appendGntmlImageStyle(StringBuilder buf,
  // int cols,
  // String prefix)
  // throws IOException
  // {
  // stringLn(buf, String.format(" insert into GntmlImageStyle values (0,%s,'%s_%s',%s,%s);",
  // _gntmlStyle_id,
  // prefix,
  // cols,
  // toSingleImageStyleId(cols),
  // toSingleImageStyleId(getZoomCols(cols))));
  // }

  private int getZoomCols(int cols)
  {
    return __zoomCols < cols ? 0 : __zoomCols;
  }

  private final Map<String, Double> __gntmlPlusBlockStyles = new HashMap<String, Double>();

  public void addGntmlPlusBlockStyle(String name,
                                     Double paddingScalar)
  {
    __gntmlPlusBlockStyles.put(name, paddingScalar);
  }

  private void appendGntmlStyle(StringBuilder buf,
                                int cols,
                                boolean isCompulsory)
      throws IOException
  {
    _gntmlStyle_id++;
    String s = NumberUtil.toString(cols);
    stringsLn(buf,
              String.format("insert into GntmlStyle values (%s,'grid_%s_%s',%s,%s,'gntml_%s_edit','gntml_%s_view', 0,0,1,'{ gridWidth: %s }');",
                            Integer.toString(_gntmlStyle_id),
                            s,
                            isCompulsory ? "comp" : "opt",
                            isCompulsory ? "0" : "1",
                            isCompulsory ? "1" : "0",
                            s,
                            s,
                            s));
    // toSingleImageStyleId(cols),
    // toSingleImageStyleId(getZoomCols(cols))));
    for (Map.Entry<String, Double> i : __gntmlPlusBlockStyles.entrySet()) {
      int padding = (int) (__grid.getHPad() * i.getValue().doubleValue());
      stringsLn(buf,
                String.format("  insert into GntmlPlusBlockStyle values (0,%s,'%s',%s,%s);",
                              Integer.toString(_gntmlStyle_id),
                              i.getKey(),
                              Integer.toString(padding),
                              Integer.toString(padding)));
    }
    // for (int i = 1; i < cols; i++) {
    // appendGntmlImageStyle(buf, i, "left");
    // appendGntmlImageStyle(buf, i, "right");
    // appendGntmlImageStyle(buf, i, "grid");
    // if (i % 2 == cols % 2) {
    // appendGntmlImageStyle(buf, i, "center");
    // }
    // }
    ln(buf);
  }

  private void appendGntmlStyle(StringBuilder buf,
                                int cols)
      throws IOException
  {
    appendGntmlStyle(buf, cols, false);
    appendGntmlStyle(buf, cols, true);
  }

  private void appendGntmlStyles(StringBuilder buf)
      throws IOException
  {
    for (int i = 1; i <= __grid.getColumnCount(); i++) {
      appendGntmlStyle(buf, i);
    }
  }

  public static void main(String[] args)
  {
    GntmlGrid gg = new GntmlGrid(new Grid(12, 60, 10, false, 12), 9);
    System.out.println(gg.generateSql());
  }
}
