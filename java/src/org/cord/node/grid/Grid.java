package org.cord.node.grid;

import java.io.IOException;
import java.util.Map;

import org.cord.node.NodeServlet;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.cord.util.StringBuilderIOException;
import org.cord.webmacro.AppendableConstantMacro;
import org.cord.webmacro.CssTool;
import org.webmacro.Macro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class Grid
  extends AbstractStringBuilder
{
  private final int __cols;
  private final int __grid;
  private final int __hPad;
  private final boolean __hasHPadSpacer;
  private final int __lineHeight;

  public static final String GET_HEADER = "Grid-";

  /**
   * The number of columns in the Grid
   * 
   * @see #GET_HEADER
   */
  public static final String GET_COLS = GET_HEADER + "cols";
  /**
   * The width of the grid unit in pixels
   * 
   * @see #GET_HEADER
   */
  public static final String GET_GRID = GET_HEADER + "grid";
  /**
   * The gutter either side of a grid unit in pixels
   * 
   * @see #GET_HEADER
   */
  public static final String GET_HPAD = GET_HEADER + "hPad";

  public static final String GET_HASHPADSPACER = GET_HEADER + "hasHPadSpacer";

  /**
   * The vertical line-height rhythm in pixels
   * 
   * @see #GET_HEADER
   */
  public static final String GET_LINEHEIGHT = GET_HEADER + "lineHeight";

  /**
   * The name under which the current Grid should be saved inside the Context
   */
  public static final String WEBMACRO_GRID = "Grid";

  /**
   * Get the Grid that has been stored in Context out or throw a RuntimeException if this is not
   * possible
   * 
   * @return The Grid, never null
   * @see #WEBMACRO_GRID
   */
  public static Grid get(Map<Object, Object> context)
  {
    return ObjectUtil.castTo(context.get(WEBMACRO_GRID), Grid.class);
  }

  /**
   * Put the Grid in the Context.
   * 
   * @see #WEBMACRO_GRID
   */
  public static void put(Map<Object, Object> context,
                         Grid grid)
  {
    Preconditions.checkNotNull(grid, "grid");
    context.put(WEBMACRO_GRID, grid);
  }

  public Grid(int cols,
              int grid,
              int hPad,
              boolean hasHPadSpacer,
              int lineHeight)
  {
    __cols = cols;
    __grid = grid;
    __hPad = hPad;
    __hasHPadSpacer = hasHPadSpacer;
    __lineHeight = lineHeight;
  }

  public Grid(Gettable params,
              Gettable defaults)
  {
    this(getInt(GET_COLS, params, defaults),
         getInt(GET_GRID, params, defaults),
         getInt(GET_HPAD, params, defaults),
         getBoolean(GET_HASHPADSPACER, params, defaults),
         getInt(GET_LINEHEIGHT, params, defaults));
  }

  public Grid withCols(Object cols)
  {
    return new Grid(NumberUtil.toInt(cols, __cols), __grid, __hPad, __hasHPadSpacer, __lineHeight);
  }

  public Grid withGridWidth(Object gridWidth)
  {
    return new Grid(__cols,
                    NumberUtil.toInt(gridWidth, __grid),
                    __hPad,
                    __hasHPadSpacer,
                    __lineHeight);
  }

  public Grid withHPad(Object hPad)
  {
    return new Grid(__cols, __grid, NumberUtil.toInt(hPad, __hPad), __hasHPadSpacer, __lineHeight);
  }

  public Grid withLineHeight(Object lineHeight)
  {
    return new Grid(__cols,
                    __grid,
                    __hPad,
                    __hasHPadSpacer,
                    NumberUtil.toInt(lineHeight, __lineHeight));
  }

  /**
   * Get the total number of columns in this Grid layout.
   */
  public final int getColumnCount()
  {
    return __cols;
  }

  /**
   * Get the width of a single column with no gutters in pixels
   */
  public final int getColumnWidth()
  {
    return __grid;
  }

  /**
   * Get the amount of horizontal padding (gutter) in pixels that is applied to each side of a grid
   * unit. The space between 2 grid units is therefore 2 x this measurement.
   */
  public final int getHPad()
  {
    return __hPad;
  }

  /**
   * Get the spacing in pixels between two columns in the grid taking into account HPadSpacer if
   * defined.
   */
  public final int getColumnSpacing()
  {
    if (__hasHPadSpacer) {
      return __hPad * 2 + 1;
    }
    return __hPad * 2;
  }

  public final int getColumnSpacing(int multiplier)
  {
    return getColumnSpacing() * multiplier;
  }

  public final int getLineHeight()
  {
    return __lineHeight;
  }

  public final int getLineHeight(int lines)
  {
    return getLineHeight() * lines;
  }

  public final int getFontSize(int lineHeight,
                               int percentage)
  {
    return getLineHeight(lineHeight) * percentage / 100;
  }

  /**
   * Return the given height rounded up to the nearest whole number of line heights.
   */
  public final int roundUpToLineHeight(int height)
  {
    int error = height % __lineHeight;
    switch (error) {
      case 0:
        return height;
      default:
        return height + __lineHeight - error;
    }
  }

  public static int getInt(String name,
                           Gettable params,
                           Gettable defaults)
  {
    try {
      Object obj = params.get(name);
      if (obj == null && defaults != null) {
        obj = defaults.get(name);
      }
      return Integer.parseInt(obj.toString());
    } catch (RuntimeException rEx) {
      throw new IllegalArgumentException(String.format("Unable to resolve Grid.getInt(%s, %s, %s)",
                                                       name,
                                                       params,
                                                       defaults),
                                         rEx);
    }
  }

  public static boolean getBoolean(String name,
                                   Gettable params,
                                   Gettable defaults)
  {
    Boolean b = params.getBoolean(name);
    if (b != null) {
      return b.booleanValue();
    }
    if (defaults != null) {
      b = defaults.getBoolean(name);
      if (b != null) {
        return b.booleanValue();
      }
    }
    throw new IllegalArgumentException(String.format("Unable to resolve Grid.getBoolean(%s, %s, %s)",
                                                     name,
                                                     params,
                                                     defaults));
  }

  public void addPersistentParameters(NodeServlet nodeServlet)
  {
    nodeServlet.addPersistentParameter(GET_COLS, Integer.valueOf(__cols));
    nodeServlet.addPersistentParameter(GET_GRID, Integer.valueOf(__grid));
    nodeServlet.addPersistentParameter(GET_HPAD, Integer.valueOf(__hPad));
    nodeServlet.addPersistentParameter(GET_LINEHEIGHT, Integer.valueOf(__lineHeight));
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("cols", __cols)
                      .add("width", __grid)
                      .add("hPad", __hPad)
                      .toString();
  }

  protected void appendRow(Appendable buf)
      throws IOException
  {
    stringsLn(buf, ".grid_row {");
    stringsLn(buf, "margin-left: auto;");
    stringsLn(buf, "margin-right: auto;");
    int width = getWidth(__cols) + 2 * __hPad;
    int hPad = __hPad;
    if (__hasHPadSpacer) {
      width += 2;
      hPad += 1;
    }
    stringsLn(buf, "width:", Integer.toString(width), "px;");
    stringsLn(buf, "padding: 0 ", Integer.toString(hPad), "px;");
    stringsLn(buf, "}");
    CssTool.clearfix(buf, ".grid_row");
    CssTool.clearfix(buf, ".grid_row>.i");
    ln(buf);
  }

  public static final String GRID_INNERROW = "grid_innerRow";

  private int getMarginLeft()
  {
    return __hPad;
  }

  private int getMarginRight()
  {
    return __hasHPadSpacer ? __hPad + 1 : __hPad;
  }

  public static final String GNTML_GRID_X = "gntml_grid_x";

  protected void appendInnerRow(Appendable buf)
      throws IOException
  {
    for (int i = 1; i <= this.__cols; i++) {
      stringsLn(buf, ".grid_", NumberUtil.toString(i), "." + GRID_INNERROW + ",");
    }
    stringsLn(buf, "." + GRID_INNERROW + " {");
    stringsLn(buf, "margin-left: -", Integer.toString(getMarginLeft()), "px;");
    stringsLn(buf, "margin-right: -", Integer.toString(getMarginRight()), "px;");
    stringsLn(buf, "}");
    CssTool.clearfix(buf, ".grid_innerRow");
    CssTool.clearfix(buf, ".grid_innerRow>.i");
    ln(buf);
    stringsLn(buf,
              "." + GNTML_GRID_X + " { display: table; margin-left: -",
              Integer.toString(getMarginLeft()),
              "px; margin-right: -",
              Integer.toString(getMarginRight()),
              "px; }");
    stringsLn(buf,
              ".lt-ie9 ." + GNTML_GRID_X + " { display: block; margin-left: 0; margin-right: 0; }");
    stringsLn(buf, "." + GNTML_GRID_X + ">.i { display: table-row; }");
    stringsLn(buf, ".lt-ie9 ." + GNTML_GRID_X + ">.i { display: block; }");
    ln(buf);
  }

  protected void appendClearfix(Appendable buf)
      throws IOException
  {
    CssTool.clearfix(buf, ".grid_clearfix");
  }

  protected void appendCommonGrids(Appendable buf)
      throws IOException
  {
    buf.append(".grid_1");
    for (int i = 2; i <= this.__cols; i++) {
      buf.append(", .grid_").append(NumberUtil.toString(i));
    }
    stringsLn(buf, " {");
    stringsLn(buf, "display:inline;");
    stringsLn(buf, "float: left;");
    stringsLn(buf, "position:relative;");
    stringsLn(buf, "margin-left:", NumberUtil.toString(getMarginLeft()), "px;");
    stringsLn(buf, "min-height: ", NumberUtil.toString(getLineHeight()), "px;");
    stringsLn(buf, "margin-right:", NumberUtil.toString(getMarginRight()), "px;");
    stringsLn(buf, "}");
    stringsLn(buf,
              ".gntml_grid { display: table-cell; position: relative; min-height: ",
              NumberUtil.toString(getLineHeight()),
              "px; padding-left: ",
              NumberUtil.toString(getMarginRight()),
              "px; padding-right: ",
              NumberUtil.toString(getMarginLeft()),
              "px; }");
    stringsLn(buf,
              ".lt-ie9 .gntml_grid { display: block; position: relative; min-height: ",
              NumberUtil.toString(getLineHeight()),
              "px; padding-left: 0; padding-right: 0; }");
    ln(buf);
  }

  protected void appendCommonLefts(Appendable buf)
      throws IOException
  {
    stringsLn(buf,
              ".gntml_left { display: inline; float: left; clear: left; position: relative; margin: 0 ",
              NumberUtil.toString(getColumnSpacing()),
              "px 0 0; }");
    stringsLn(buf, ".gntml_left>.i { margin-bottom: ", NumberUtil.toString(__lineHeight), "px; }");
    ln(buf);
  }

  protected void appendCommonRights(Appendable buf)
      throws IOException
  {
    stringsLn(buf,
              ".gntml_right { display: inline; float: right; clear: right; position: relative; margin: 0 0 0 ",
              NumberUtil.toString(getColumnSpacing()),
              "px; }");
    stringsLn(buf, ".gntml_right>.i { margin-bottom: ", NumberUtil.toString(__lineHeight), "px; }");
    ln(buf);
  }

  protected void appendCommonCenters(Appendable buf)
      throws IOException
  {
    stringsLn(buf, ".gntml_center { margin-bottom: ", NumberUtil.toString(__lineHeight), "px; }");
    ln(buf);
  }

  protected void appendUniqueCenters(Appendable buf)
      throws IOException
  {
    for (int w = 3; w <= __cols; w++) {
      for (int c = 1; c < w; c++) {
        int l = (w - c) / 2;
        int r = w - c - l;
        stringsLn(buf,
                  ".body_",
                  NumberUtil.toString(w),
                  ">.gntml_center_",
                  NumberUtil.toString(c),
                  " { padding-right: ",
                  Integer.toString(getIndent(r)),
                  r > 0 ? "px" : "",
                  "; padding-left: ",
                  Integer.toString(getIndent(l)),
                  l > 0 ? "px" : "",
                  "; }");
      }
      ln(buf);
    }
  }

  /**
   * Get the total width of the Grid layout. This is equivalent to calling
   * getWidth(getColumnCount())
   * 
   * @return The width in pixels
   */
  public int getWidth()
  {
    return getWidth(getColumnCount());
  }

  public int getWidth(int cols)
  {
    if (__hasHPadSpacer) {
      return cols * __grid + (cols - 1) * (__hPad * 2 + 1);
    }
    return cols * __grid + (cols - 1) * __hPad * 2;
  }

  public int getContainer(int cols)
  {
    return getWidth(cols) + getColumnSpacing(2);
  }

  public int getContainer()
  {
    return getContainer(getColumnCount());
  }

  /**
   * Get the number of pixels a block should be indented in from an existing grid block to align
   * with the next appropriate column boundary.
   */
  public int getIndent(int cols)
  {
    if (cols == 0) {
      return 0;
    }
    if (__hasHPadSpacer) {
      return getWidth(cols) + __hPad * 2 + 1;
    }
    return getWidth(cols) + __hPad * 2;
  }

  public int getWidth(int cols,
                      int hBorder)
  {
    return getWidth(cols) - hBorder;
  }

  protected void appendUniqueGrid(Appendable buf,
                                  int cols)
      throws IOException
  {
    String colsString = NumberUtil.toString(cols);
    stringsLn(buf,
              ".grid_",
              colsString,
              ", .gntml_left_",
              colsString,
              ", .gntml_right_",
              colsString,
              ", .gntml_grid_",
              colsString,
              " { width:",
              Integer.toString(getWidth(cols)),
              "px; }");
  }

  protected void appendUniqueGrids(Appendable buf)
      throws IOException
  {
    for (int i = 1; i <= this.__cols; i++) {
      appendUniqueGrid(buf, i);
    }
    ln(buf);
  }

  protected void appendAlphaOmega(Appendable buf)
      throws IOException
  {
    stringsLn(buf, ".alpha, .gntml_plus_alpha { margin-left: 0; clear: left; }");
    stringsLn(buf, ".omega, .gntml_plus_omega { margin-right: 0; }");
    stringsLn(buf, ".gntml_clear { clear: both; }");
  }

  protected void appendLeftIndent(Appendable buf,
                                  int cols)
      throws IOException
  {
    stringsLn(buf,
              ".grid_i_l_",
              NumberUtil.toString(cols),
              " { margin-left: ",
              Integer.toString(getIndent(cols)),
              "px; }");
  }

  protected void appendRightIndent(Appendable buf,
                                   int cols)
      throws IOException
  {
    stringsLn(buf,
              ".grid_i_r_",
              NumberUtil.toString(cols),
              " { margin-right: ",
              Integer.toString(getIndent(cols)),
              "px; }");
  }

  protected void appendIndents(Appendable buf)
      throws IOException
  {
    for (int i = 1; i <= this.__cols; i++) {
      appendLeftIndent(buf, i);
      appendRightIndent(buf, i);
    }
    ln(buf);
  }

  protected void appendCss(Appendable buf)
      throws IOException
  {
    stringsLn(buf, "/* START: org.cord.node.grid.Grid */");
    appendRow(buf);
    appendClearfix(buf);
    appendCommonGrids(buf);
    appendCommonLefts(buf);
    appendCommonRights(buf);
    appendCommonCenters(buf);
    appendUniqueCenters(buf);
    appendUniqueGrids(buf);
    appendAlphaOmega(buf);
    appendInnerRow(buf);
    appendIndents(buf);
    stringsLn(buf, "/* END: org.cord.node.grid.Grid */");
  }

  public String generateCss()
  {
    StringBuilder buf = new StringBuilder();
    try {
      appendCss(buf);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  public Macro streamCss()
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        appendCss(out);
      }
    };
  }
}
