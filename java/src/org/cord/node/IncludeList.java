package org.cord.node;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * Representation of the IncludeFiles that are necessary to process a single NodeRequest.
 * 
 * @author alex
 */
@Deprecated
public class IncludeList
{
  private final IncludeMgr __includeMgr;
  private final List<IncludeFile> __includeFiles = Lists.newArrayList();
  private final List<IncludeFile> __roIncludeFiles = Collections.unmodifiableList(__includeFiles);

  protected IncludeList(IncludeMgr includeMgr)
  {
    __includeMgr = includeMgr;
  }

  /**
   * Look up an IncludeFile by key and then add it and its dependcies to this IncludeList.
   */
  public void addFile(String key)
  {
    IncludeFile includeFile = Preconditions.checkNotNull(getIncludeMgr().getIncludeFiles().get(key),
                                                         "Unable to resolve key of %s",
                                                         key);
    if (__includeFiles.contains(includeFile)) {
      return;
    }
    for (String dependency : includeFile.getDependencies()) {
      addFile(dependency);
    }
    __includeFiles.add(includeFile);
  }

  public String toHtml()
  {
    StringBuilder buf = new StringBuilder();
    for (IncludeFile includeFile : __includeFiles) {
      buf.append(includeFile.toHtml()).append('\n');
    }
    return buf.toString();
  }

  /**
   * @return Unmodifiable List of IncludeFiles
   */
  public final List<IncludeFile> getIncludeFiles()
  {
    return __roIncludeFiles;
  }

  public final IncludeMgr getIncludeMgr()
  {
    return __includeMgr;
  }

  @Override
  public String toString()
  {
    return "IncludeList(" + getIncludeFiles() + ")";
  }
}
