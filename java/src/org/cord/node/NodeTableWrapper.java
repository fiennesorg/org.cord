package org.cord.node;

import org.cord.mirror.AbstractTableWrapper;

import com.google.common.base.Preconditions;

/**
 * Implementation of TableWrapper that stores the NodeManager as well and is used to initialise the
 * Node Tables.
 * 
 * @author alex
 */
public abstract class NodeTableWrapper
  extends AbstractTableWrapper
{
  private final NodeManager __nodeManager;

  public NodeTableWrapper(NodeManager nodeManager,
                          String tableName)
  {
    super(tableName);
    __nodeManager = Preconditions.checkNotNull(nodeManager, "nodeManager");
  }

  protected final NodeManager getNodeManager()
  {
    return __nodeManager;
  }
}
