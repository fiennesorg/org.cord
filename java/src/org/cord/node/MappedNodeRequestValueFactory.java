package org.cord.node;

import java.util.HashMap;
import java.util.Map;

/**
 * ValueFactory that looks a value up in the NodeRequest and resolves it as a key against a Map to
 * obtain the final value.
 * 
 * @author alex
 */
public class MappedNodeRequestValueFactory
  implements ValueFactory
{
  private final String __key;

  private final String __defaultValue;

  private final Map<Object, String> __mappings = new HashMap<Object, String>();

  private boolean _isLocked = false;

  public MappedNodeRequestValueFactory(String key,
                                       String defaultValue)
  {
    __key = key;
    __defaultValue = defaultValue;
  }

  @Override
  public void shutdown()
  {
    __mappings.clear();
  }

  public void lock()
  {
    _isLocked = true;
  }

  public boolean isLocked()
  {
    return _isLocked;
  }

  public String addMapping(Object key,
                           String value)
  {
    if (_isLocked) {
      throw new IllegalStateException("mappings are locked");
    }
    return __mappings.put(key, value);
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    if (nodeRequest == null) {
      return __defaultValue;
    }
    Object value = nodeRequest.get(__key);
    return __mappings.get(value);
  }
}
