package org.cord.node;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.cord.util.AbstractGettable;
import org.cord.util.XmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Gettable wrapper around the web.xml class that is used to store the configuration for a tomcat
 * deployment. This therefore lets us utilise a single set of bootup parameters that can be accessed
 * from non-servlet contexts in order to provide database manipulations.
 */
public class GettableWebXml
  extends AbstractGettable
{
  private final Map<String, String> __values = new HashMap<String, String>();

  /**
   * @param stripDtd
   *          If true then a regex will be applied to the contents of the webXml file that will
   *          remove the reference to the externally defined Sun DTD for the web-app structure of
   *          the file. This will mean that the file is not checked for validity to the structure,
   *          but will enable the file to be cleanly resolved in situations where the Sun DTD is not
   *          available, for example running in an offline situation when there is not a network
   *          connection to the SUN server.
   */
  public GettableWebXml(File webXml,
                        boolean stripDtd)
      throws IOException, ParserConfigurationException, SAXException
  {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setValidating(false);
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    StringBuilder xmlBuffer = new StringBuilder();
    BufferedReader bufferedReader = null;
    try {
      bufferedReader = new BufferedReader(new FileReader(webXml));
      String nextLine = null;
      while ((nextLine = bufferedReader.readLine()) != null) {
        xmlBuffer.append(nextLine);
      }
    } finally {
      if (bufferedReader != null) {
        bufferedReader.close();
      }
    }
    String xmlString = xmlBuffer.toString();
    if (stripDtd) {
      xmlString = (Pattern.compile("<!DOCTYPE[^>]*>").matcher(xmlString).replaceAll(""));
    }
    Document document = documentBuilder.parse(new InputSource(new StringReader(xmlString)));
    NodeList initParams = document.getElementsByTagName("init-param");
    for (int i = 0; i < initParams.getLength(); i++) {
      Element initParam = (Element) initParams.item(i);
      String paramName = XmlUtils.getTextElement(initParam, "param-name");
      String paramValue = XmlUtils.getTextElement(initParam, "param-value");
      __values.put(paramName, paramValue);
    }
  }

  @Override
  public Collection<String> getPublicKeys()
  {
    return __values.keySet();
  }

  @Override
  public Object get(Object key)
  {
    return __values.get(key.toString());
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __values.containsKey(key);
  }

}
