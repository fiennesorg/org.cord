package org.cord.node.field;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.IntFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordStringFactory;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.node.Node;
import org.cord.node.NodeFilter;
import org.cord.util.HtmlUtils;

/**
 * Specialised version of LinkField that points at Node records and which provides additional
 * abilities to support maintaining these links.
 * 
 * @author alex
 */
public class NodeLinkField
  extends LinkField
{
  private final NodeFilter __acceptableTargets;

  private final RecordStringFactory __browseParams;

  /**
   * @param acceptableTargets
   *          The NodeFilter that will only let through Nodes that are appropriate targets for this
   *          NodeLinkField to point at. If null then accepts(node) will always return true, ie all
   *          nodes are valid targets
   * @param browseParams
   *          The factory that will produce the necessary CGI parameters to open a NodeQuestion to
   *          look for the target of this NodeLinkField.
   * @see #accepts(PersistentRecord)
   */
  public NodeLinkField(IntFieldKey linkFieldName,
                       String label,
                       Integer defaultValue,
                       RecordOperationKey<PersistentRecord> linkName,
                       RecordOperationKey<RecordSource> referencesName,
                       String reverseOrderClause,
                       String transactionPassword,
                       int linkStyle,
                       NodeFilter acceptableTargets,
                       RecordStringFactory browseParams)
  {
    super(linkFieldName,
          label,
          defaultValue,
          linkName,
          Node.TABLENAME,
          referencesName,
          reverseOrderClause,
          transactionPassword,
          linkStyle);
    __acceptableTargets = acceptableTargets;
    __browseParams = browseParams;
  }

  /**
   * @param acceptableTargets
   *          The NodeFilter that will only let through Nodes that are appropriate targets for this
   *          NodeLinkField to point at. If null then accepts(node) will always return true, ie all
   *          nodes are valid targets
   * @param browseParams
   *          The factory that will produce the necessary CGI parameters to open a NodeQuestion to
   *          look for the target of this NodeLinkField.
   * @see #accepts(PersistentRecord)
   * @see LinkField#LinkField(String, String, String, int)
   */
  public NodeLinkField(String label,
                       String transactionPassword,
                       int linkStyle,
                       NodeFilter acceptableTargets,
                       RecordStringFactory browseParams)
  {
    super(Node.TABLENAME,
          label,
          transactionPassword,
          linkStyle);
    __acceptableTargets = acceptableTargets;
    __browseParams = browseParams;
  }

  /**
   * Is the given node a valid target for this link?
   * 
   * @return true if the acceptableTargets NodeFilter accepts the node. If acceptableTargets isn't
   *         defined then return true (ie we accept all nodes)
   */
  public final boolean accepts(PersistentRecord node)
  {
    return __acceptableTargets == null ? true : __acceptableTargets.accepts(node);
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    StringBuilder value = new StringBuilder();
    PersistentRecord targetNode = record.opt(getLinkName());
    if (targetNode == null) {
      value.append("undefined");
    } else {
      value.append(targetNode.opt(Node.HREF, Node.PATH.getKeyName()));
    }
    value.append("<br />\n");
    value.append("<a href=\"?")
         .append(__browseParams.getValue(record))
         .append("\">Browse for target</a>");
    HtmlUtils.value(buf, value.toString(), namePrefix, getName());
  }
}
