package org.cord.node;

import java.util.List;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.util.Settable;
import org.cord.util.SettableException;

/**
 * An interface that should be implemented by ContentCompilers if they expect to be able to transfer
 * their contents when a Node is being copied.
 * 
 * @author alex
 * @see org.cord.node.Node#copyNode(PersistentRecord, PersistentRecord, PersistentRecord, boolean,
 *      Integer, String, Boolean, List, List)
 */
public interface CopyableContentCompiler
{
  /**
   * Populate the namespaced Settable with the parameters that would cause an update on a new Node
   * of the same NodeStyle as fromNode to recreate the state of this ContentCompiler.
   * 
   * @param namespacedSettable
   *          The Settable that should be populated with the parameters that the ContentCompiler
   *          would expect to upload. The Settable will be namespaced in the same way that the
   *          update Gettable is namespaced so the whole procedure should be symetrical.
   */
  public void copyContentCompiler(Settable namespacedSettable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException;
}
