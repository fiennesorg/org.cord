package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.LogicException;

/**
 * Implementation of NodeQuestion for situations where only a single Node is required as an answer.
 * Subclasses only need implement getAnswerUrl(selectedNode) to complete the implementation.
 * 
 * @author alex
 * @see #getAnswerUrl(PersistentRecord)
 */
public abstract class NodeQuestionSingleAnswer
  extends NodeQuestion
{
  public NodeQuestionSingleAnswer(String name,
                                  String englishName,
                                  String title,
                                  PersistentRecord answerNode,
                                  NodeFilter eligibleNodeFilter,
                                  Transaction transaction)
  {
    super(name,
          englishName,
          title,
          answerNode,
          1,
          eligibleNodeFilter,
          transaction);
  }

  /**
   * Resolve the currently selected answer node and pass this through to subclass' getAnswerUrl
   * implementation.
   * 
   * @see #getAnswerUrl(PersistentRecord)
   */
  @Override
  public final String getAnswerUrl()
  {
    PersistentRecord selectedNode;
    try {
      selectedNode = getSelectedNode();
    } catch (MirrorNoSuchRecordException e) {
      throw new LogicException("Can't resolve selected node", e);
    }
    return getAnswerUrl(selectedNode);
  }

  protected abstract String getAnswerUrl(PersistentRecord selectedNode);

  /**
   * @return true if there is exactly one answer node selected
   */
  @Override
  public final boolean maySubmit()
  {
    return getSelectedNodeIds().size() == 1;
  }

  /**
   * @return true if there is exactly one answer node selected
   */
  @Override
  public final boolean shouldSubmit()
  {
    return maySubmit();
  }
}
