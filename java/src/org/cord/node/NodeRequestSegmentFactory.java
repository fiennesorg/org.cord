package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;
import org.cord.util.StringUtils;
import org.webmacro.Context;

/**
 * Base class for view engines for the Node system. If you want to implement a new view, then all
 * you have to do is extend this class and implement getInstance(...). Then you register your class
 * in the NodeRequestSegmentFactoryFactory and you may now invoke it from the template by utilising
 * the <i>name</i> of the factory in the <i>view</i> parameter of a submission form.
 * 
 * @see #getInstance(NodeRequest, PersistentRecord, Context, boolean)
 */
public abstract class NodeRequestSegmentFactory
  extends NamedImpl
{
  private final NodeManager __nodeManager;

  private final PostViewAction __postViewAction;

  private boolean _isRenderReset = false;

  private boolean _isRootTemplatePath = false;

  /**
   * @param postViewAction
   *          The success operation that will be invoked by invoked by the handleSuccess(...) method
   *          for this view. If this is null, then it will be replaced with the
   *          InternalSuccessOperation, implying that the subclass must handle the success
   *          conditions manually.
   * @see InternalSuccessOperation
   */
  public NodeRequestSegmentFactory(String name,
                                   NodeManager nodeManager,
                                   PostViewAction postViewAction)
  {
    super(name);
    __nodeManager = nodeManager;
    __postViewAction =
        postViewAction == null ? InternalSuccessOperation.getInstance() : postViewAction;
  }

  /**
   * Create an instance with a default PostViewAction of InternalSuccessOperation. This means that
   * if you don't handle things correctly inside the getInstance operation that the process will
   * throw an error.
   * 
   * @see InternalSuccessOperation
   */
  public NodeRequestSegmentFactory(String name,
                                   NodeManager nodeManager)
  {
    this(name,
         nodeManager,
         null);
  }

  /**
   * Returns whether the user could invoke this view on the node. It does not take account of the
   * path to the node: if you need to take that into account, call NodeRequest.isAllowed(node,
   * viewName)
   */
  public boolean isAllowed(PersistentRecord node,
                           int userId)
  {
    return true;
  }

  /**
   * Returns whether the given nodeRequest could invoke this view on the node.
   * 
   * @return The result of invoking {@link #isAllowed(PersistentRecord, int)} using the userId
   *         contained in the nodeRequest. If your authentication uses the current lock state of the
   *         target which is associated with the nodeRequest rather than just the userId then you
   *         should override this method.
   */
  public boolean isAllowed(PersistentRecord node,
                           NodeRequest nodeRequest)
  {
    return isAllowed(node, nodeRequest.getUserId());
  }

  public void shutdown()
  {
    if (__postViewAction != null) {
      __postViewAction.shutdown();
    }
  }

  /**
   * If the supplied action is null, then return a ContextFeedbackSuccessOperation, otherwise return
   * the action. This therefore gives a solution that is guaranteed to work in all non-defined
   * situations without requiring additional deployment-specific templates or resources (although it
   * might look a little bit ugly).
   * 
   * @param action
   *          The action that is going to be tested for null'ness
   * @return The action if defined, otherwise a ContextFeedbackSuccessOperation instance.
   * @see ContextFeedbackSuccessOperation#getInstance()
   */
  protected PostViewAction padWithDefault(PostViewAction action)
  {
    return (action == null ? ContextFeedbackSuccessOperation.getInstance() : action);
  }

  public boolean getIsRenderReset()
  {
    return _isRenderReset;
  }

  /**
   * Set whether or not this NodeRequestSegmentFactory will reset the rendering tree to the template
   * referred to by this level, thereby ignoring all templates of parent items.
   */
  public NodeRequestSegmentFactory setIsRenderReset(boolean flag)
  {
    _isRenderReset = flag;
    return this;
  }

  public boolean getIsRootTemplatePath()
  {
    return _isRootTemplatePath;
  }

  /**
   * Set whether or not this NodeRequestSegmentFactory will reset the rendering tree entirely and
   * use the TemplatePath as the root template for the output when constructing the final operation.
   */
  public NodeRequestSegmentFactory setIsRootTemplatePath(boolean flag)
  {
    _isRootTemplatePath = flag;
    return this;
  }

  protected NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                             PersistentRecord node,
                                             Context context,
                                             Exception nestedException)
      throws NodeRequestException
  {
    if (__postViewAction != null) {
      return __postViewAction.handleSuccess(nodeRequest, node, context, this, nestedException);
    } else {
      throw new LogicException("no defined PostViewAction", nestedException);
    }
  }

  protected NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                             PersistentRecord node,
                                             Context context)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context, null);
  }

  /**
   * Rescan the Node using the transactional viewpoint to determine if it has changed state.
   */
  protected final static PersistentRecord updateNode(NodeRequest nodeRequest,
                                                     PersistentRecord node)
  {
    try {
      return nodeRequest.getSessionTransaction().getNode(node.getId());
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException(node
                               + " has dissappeared - if it has been deleted then don't use the reference...",
                               mnsrEx);
    }
  }

  /**
   * Resolve the given PersistentRecord Node into corresponding WritableRecord assuming that the
   * NodeRequest has already obtained the appropriate lock status. This should only be used in
   * situation when you are sure that you already have the appropriate lock.
   * 
   * @param nodeRequest
   *          The NodeRequest that should have the lock.
   * @param node
   *          The node which is already locked by the NodeRequest and whose WritableRecord should be
   *          returned.
   * @throws LogicException
   *           If the Node is not currently locked by the NodeRequest.
   */
  public static WritableRecord getWritableNode(NodeRequest nodeRequest,
                                               PersistentRecord node)
  {
    if (node instanceof WritableRecord) {
      return (WritableRecord) node;
    }
    try {
      return (WritableRecord) updateNode(nodeRequest, node);
    } catch (ClassCastException notLockedEx) {
      throw new LogicException("Current user doesn't have lock on " + node, notLockedEx);
    }
  }

  /**
   * Utility method to attempt to lock a sub-tree of the Node system. The method will recursively
   * try and obtain an update lock on all the children of the invoking Node.
   * 
   * @param transaction
   *          The transaction that should become the owner of the sub-tree
   * @param rootNode
   *          The pre-locked node that is the top-level of the target sub-tree. This node "should"
   *          be locked by <code>transaction</code> but if it isn't then the method will still
   *          function, you will just be left with a slightly bizarre lock status for the sub-tree
   *          (root node locked by a different transaction to all the children nodes). However, this
   *          is not banned because there may be situations when it is necessary to perform this
   *          action.
   * @throws MirrorTransactionTimeoutException
   *           If any of the Nodes in the sub-tree are pre-locked by a 3rd party transaction that
   *           doesn't release the lock within the specified timeout of the target transaction.
   */
  protected final static void lockNodeTree(Transaction transaction,
                                           WritableRecord rootNode)
      throws MirrorTransactionTimeoutException
  {
    lockNodeTree(transaction, rootNode, 0);
  }

  /**
   * Utility method to attempt to lock a sub-tree with specific ownership restrictions of the Node
   * system. The method will recursively try and obtain an update lock on all the children of the
   * invoking Node.
   * 
   * @param transaction
   *          The transaction that should become the owner of the sub-tree
   * @param rootNode
   *          The pre-locked node that is the top-level of the target sub-tree. This node "should"
   *          be locked by <code>transaction</code> but if it isn't then the method will still
   *          function, you will just be left with a slightly bizarre lock status for the sub-tree
   *          (root node locked by a different transaction to all the children nodes). However, this
   *          is not banned because there may be situations when it is necessary to perform this
   *          action.
   * @param nodeStyleOwnerId
   *          The value of $node.nodeStyle.owner_id that should be locked. Only children Nodes that
   *          have a NodeStyle that matches this owner id will be locked. This permits the
   *          auto-locking of sub-trees that inherit ownership from a root node (== -1) in
   *          preparation of an ownership change.
   * @throws MirrorTransactionTimeoutException
   *           If any of the Nodes in the sub-tree are pre-locked by a 3rd party transaction that
   *           doesn't release the lock within the specified timeout of the target transaction.
   */
  protected final static void lockNodeTree(Transaction transaction,
                                           WritableRecord rootNode,
                                           int nodeStyleOwnerId)
      throws MirrorTransactionTimeoutException
  {
    for (PersistentRecord childNode : rootNode.opt(Node.UNSORTEDCHILDRENNODES, transaction)) {
      if (nodeStyleOwnerId == 0
          || (childNode.comp(Node.NODESTYLE).compInt(NodeStyle.OWNER_ID) == nodeStyleOwnerId)) {
        WritableRecord writableChildNode = transaction.edit(childNode, Db.DEFAULT_TIMEOUT);
        lockNodeTree(transaction, writableChildNode);
      }
    }
  }

  protected void commitTransaction(NodeRequest nodeRequest,
                                   PersistentRecord node,
                                   Transaction transaction)
      throws TransactionExpiredException
  {
    try {
      transaction.commit();
    } catch (MirrorTransactionTimeoutException expiredTransactionEx) {
      throw new TransactionExpiredException("Failed to commit " + transaction,
                                            node,
                                            expiredTransactionEx);
    } finally {
      __nodeManager.getUserManager().removeTransaction(nodeRequest.getSession(), transaction);
    }
  }

  /**
   * Get the NodeManager that was passed to the constructor.
   * 
   * @return The appropriate NodeManager.
   */
  protected final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  /**
   * Get the Transaction (if any) that is currently held by the given User on a given Node or
   * NodeGroup. Please note that if the targetted object is locked by a <i>different</i> user then
   * this is not picked up by the system.
   * 
   * @param nodeRequest
   *          The NodeRequest that is being made by the User which is to be checked for.
   * @param node
   *          The Node that is to have the Transaction checked for.
   * @return The locking Transaction, or null if the targetted item is not locked <i>or</i> it is
   *         locked by another User (which the current user doesn't, by definition, have access to).
   */
  protected static final Transaction getTransaction(NodeRequest nodeRequest,
                                                    TransientRecord node)
  {
    return nodeRequest.getSessionTransaction().getTransactionByNode(node.getId());
  }

  protected final void cancelTransaction(NodeRequest nodeRequest,
                                         Transaction transaction)
  {
    if (transaction != null) {
      transaction.cancel();
      getNodeManager().getUserManager().removeTransaction(nodeRequest.getSession(), transaction);
    }
  }

  /**
   * Create (if necessary) the appropriate Transaction and check to appropriate Node into the
   * Transaction. This will of course check in any dependent records as well as the existing record.
   * 
   * @param transactionType
   *          The style of Transaction to create and apply to the targetted record. Valid values are
   *          TRANSACTION_UPDATE or TRANSACTION_DELETE.
   * @return A Transaction that has the lock on the requested Node.
   * @throws RecordPreLockedException
   *           If the targetted record or any of its dependent records are currently locked by
   *           another Transaction. Please note that this will clean up and remove the new
   *           Transaction.
   * @throws LogicException
   *           If a Transaction appropriate for the isNodeTransaction already exists, but it is of a
   *           different type to transactionType. This will not affect the status of the existing
   *           Transaction.
   * @throws LogicException
   *           If the requested transactionType is not TRANSACTION_UPDATE or TRANSACTION_DELETE, or
   *           if the Node is currently already locked by the current User but the lock type is of a
   *           different type to the requested lock type.
   * @see Transaction#TRANSACTION_UPDATE
   * @see Transaction#TRANSACTION_DELETE
   */
  @Deprecated
  protected Transaction createTransaction(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          int transactionType)
      throws RecordPreLockedException
  {
    Transaction existingTransaction = getTransaction(nodeRequest, node);
    if (existingTransaction != null) {
      if (existingTransaction.getType() != transactionType) {
        throw new LogicException("Attempted to create "
                                 + Transaction.TRANSACTION_TYPES.get(transactionType)
                                 + " when there is already an instance of "
                                 + Transaction.TRANSACTION_TYPES.get(existingTransaction.getType()));
      }
      return existingTransaction;
    }
    // we need to create a new Transaction...
    Transaction newTransaction = null;
    // ### 2DO Make the timeout period on update transactions into a
    // ### configurable value.
    newTransaction =
        (getNodeManager().getDb().createTransaction(getNodeManager().getTransactionTimeout(),
                                                    transactionType,
                                                    getNodeManager().getTransactionPassword(),
                                                    "Node transaction:" + transactionType));
    getNodeManager().getUserManager().addTransaction(nodeRequest.getSession(), newTransaction);
    try {
      switch (transactionType) {
        case Transaction.TRANSACTION_DELETE:
          newTransaction.delete(node, Node.LOCK_DEFAULTTIMEOUT);
          break;
        case Transaction.TRANSACTION_UPDATE:
          newTransaction.edit(node, Node.LOCK_DEFAULTTIMEOUT);
          break;
        default:
          cancelTransaction(nodeRequest, newTransaction);
          throw new LogicException(this + ".createTransaction(" + transactionType
                                   + ") Unsupported transaction type");
      }
    } catch (MirrorTransactionTimeoutException recordLockedEx) {
      cancelTransaction(nodeRequest, newTransaction);
      NodeUserManager.SessionTransaction sessionTransaction =
          (__nodeManager.getContestedLockFilterManager().getStealableSessionTransaction(nodeRequest,
                                                                                        node));
      throw new RecordPreLockedException(this + ".createTransaction(" + transactionType + ")",
                                         node,
                                         node,
                                         sessionTransaction,
                                         recordLockedEx);
    }
    return newTransaction;
  }

  protected Transaction createEditTransaction(NodeRequest nodeRequest,
                                              PersistentRecord node)
      throws RecordPreLockedException
  {
    return createTransaction(nodeRequest, node, Transaction.TRANSACTION_UPDATE);
  }

  protected Transaction createDeleteTransaction(NodeRequest nodeRequest,
                                                PersistentRecord node)
      throws RecordPreLockedException
  {
    return createTransaction(nodeRequest, node, Transaction.TRANSACTION_DELETE);
  }

  /**
   * Request the factory to perform whatever work is necessary for the request in question, and
   * either returning a populated NodeRequestSegment or throwing a NodeRequestException as a result.
   * 
   * @param nodeRequest
   *          The incoming request that is being processed
   * @param node
   *          The PersistentRecord that is to be processed by this NodeRequestSegmentFactory. If the
   *          current Session has a lock on the Node in question, then this will be the
   *          WritableRecord version of the Node.
   * @param context
   *          The webmacro style context that is to be populated by the factory (if necessary).
   * @param isSummaryRole
   *          True if this is the last node in the path
   * @return The populated NodeRequestSegment. If this is null then it means that the view has
   *         completed cleanly and handleSuccess should be invoked by the factory thereby chaining
   *         this view onto the next view. If the desired result is a direct rendering of a template
   *         then have the postViewSuccessAction set to TemplateSuccessOperation will provide this
   *         functionality while keeping the possibility of extending the behaviour in the future
   *         without recoding the existing class.
   * @throws NodeRequestException
   *           If the NodeRequest is required to perform an action other than populating the
   *           templates.
   * @see TemplateSuccessOperation
   */
  protected abstract NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    boolean isSummaryRole)
      throws NodeRequestException;

  /**
   * Invoke the implementation of getInstance and then delegate the result to handleSuccess if
   * getInstance doesn't handle it directly.
   * 
   * @return The NodeRequestSegment that should be rendered as the output of this view. This should
   *         never be null.
   * @throws NodeRequestException
   *           If an action other than rendering a template is required.
   * @see #getInstance(NodeRequest, PersistentRecord, Context, boolean)
   */
  public final NodeRequestSegment doWork(NodeRequest nodeRequest,
                                         PersistentRecord node,
                                         Context context,
                                         boolean isSummaryRole)
      throws NodeRequestException
  {
    NodeRequestSegment segment = getInstance(nodeRequest, node, context, isSummaryRole);
    if (segment == null) {
      return handleSuccess(nodeRequest, node, context, null);
    }
    return segment;
  }

  /**
   * Generate a URL which invokes this view on the given node. No parameters are passed in other
   * than the view name, and no validation is made as to whether or not the node is a suitable
   * target for the view.
   */
  public String getURL(PersistentRecord node)
  {
    return getURL(node, getName());
  }

  /**
   * Generate a URL which invokes a named view on a given node.
   */
  public static String getURL(PersistentRecord node,
                              String viewName)
  {
    return node.comp(Node.URL) + "?" + NodeRequest.CGI_VAR_VIEW + '='
           + StringUtils.urlEncodeUtf8(viewName);
  }
}
