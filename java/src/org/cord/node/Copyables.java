package org.cord.node;

import org.cord.mirror.RecordOperationKey;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

/**
 * Utility methods for assistance in {@link CopyableContentCompiler} implementations.
 * 
 * @author alex
 */
public class Copyables
{
  public static void copy(Gettable namespacedGettable,
                          Settable namespacedSettable,
                          Iterable<? extends RecordOperationKey<?>> fields)
      throws SettableException
  {
    for (RecordOperationKey<?> field : fields) {
      namespacedSettable.set(field, namespacedGettable.get(field));
    }
  }

  public static void copy(Gettable namespacedGettable,
                          Settable namespacedSettable,
                          RecordOperationKey<?>... fields)
      throws SettableException
  {
    for (RecordOperationKey<?> field : fields) {
      namespacedSettable.set(field, namespacedGettable.get(field));
    }
  }
}
