package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * Implementation of Authenticator that has no opinion on any of the requests. This is utilised as
 * the fallback Authenticator if you do not define one for your specific application and you
 * effectively get the behaviour of the system pre-Authenticator.
 * 
 * @author alex
 */
public class EmptyAuthenticator
  implements Authenticator
{
  private final static EmptyAuthenticator INSTANCE = new EmptyAuthenticator();

  public static final EmptyAuthenticator getInstance()
  {
    return INSTANCE;
  }

  private EmptyAuthenticator()
  {
  }

  @Override
  public Boolean isAllowed(NodeRequest nodeRequest)
  {
    return null;
  }

  @Override
  public Boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return null;
  }

  @Override
  public Boolean isAllowed(NodeManager nodeMgr,
                           PersistentRecord node,
                           String view,
                           int userId)
  {
    return null;
  }
}
