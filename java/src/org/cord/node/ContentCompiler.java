package org.cord.node;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.DateFieldExtractor;
import org.cord.sql.SqlUtil;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;

/**
 * Abstract class for representing items that provide content for a Region.
 */
public abstract class ContentCompiler
  extends EnglishNamedImpl
{
  private static Calendar __calendar = Calendar.getInstance();

  public final static RecordOperationKey<String> VIEWTEMPLATEPATH =
      RecordOperationKey.create("viewTemplatePath", String.class);

  public final static RecordOperationKey<String> EDITTEMPLATEPATH =
      RecordOperationKey.create("editTemplatePath", String.class);

  private NodeManager _nodeManager;

  public ContentCompiler(NodeManager nodeManager,
                         String name,
                         String englishName)
  {
    super(name,
          englishName);
    _nodeManager = nodeManager;
  }

  public void shutdown()
  {
    _nodeManager = null;
  }

  public final NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  public final Db getDb()
  {
    return getNodeManager().getDb();
  }

  public ContentCompiler(NodeManager nodeManager,
                         String name)
  {
    this(nodeManager,
         name,
         name);
  }

  private String _nodeStylesFilter = null;

  /**
   * Get all of the NodeStyle records that contain one or more RegionStyle records that reference
   * this ContentCompiler
   * 
   * @return Query of NodeStyles
   */
  public RecordSource getNodeStyles()
  {
    if (_nodeStylesFilter == null) {
      StringBuilder buf = new StringBuilder();
      buf.append("(NodeStyle.id=RegionStyle.nodeStyle_id) and (RegionStyle.compilerName=");
      SqlUtil.toSafeSqlString(buf, getName());
      buf.append(')');
      _nodeStylesFilter = buf.toString();
    }
    return getNodeManager().getNodeStyle()
                           .getTable()
                           .getQuery(_nodeStylesFilter,
                                     _nodeStylesFilter,
                                     null,
                                     getNodeManager().getRegionStyle().getTable());
  }

  /**
   * Get the name by which this ContentCompiler prefers to be known as when registered in a
   * RegionStyle. This is for guidelines only and is not enforced anywhere. The default
   * implementation is getName() with a lowercase first letter. Override this if you have a
   * different point of view.
   */
  public String getDefaultRegionStyleName()
  {
    String name = getName();
    return Character.toLowerCase(name.charAt(0)) + name.substring(1);
  }

  /**
   * Request the ContentCompiler to initialise a new instance itself for a specified node. If there
   * is already an instance existing for the (nodeId, regionId) tuple, then a new instance should
   * not be created.
   * 
   * @param regionStyle
   *          The Region which the instance is going to be attached to.
   * @param inputs
   *          The source of any configuration parameters that are passed to the ContentCompiler to
   *          enable resources to be pre-allocated at creation time.
   * @param outputs
   *          The optional Map that any non-blocking outputs of this method can be placed into.
   * @throws MirrorFieldException
   *           If the information in regionId or some ContentCompiler specific information was
   *           rejected during the intialisation. This will normally be triggered by the enforced
   *           link requirements on the fields in the backend content compiler table.
   * @throws MirrorInstantiationException
   *           If any of the triggers put into place by the ContentCompiler fail during
   *           instantiation of the record after it has been initialised with (nodeId, regionId).
   * @throws MirrorNoSuchRecordException
   *           If the style referenced in the region is not a defined style on the ContentCompiler.
   */
  public abstract void initialiseInstance(PersistentRecord node,
                                          PersistentRecord regionStyle,
                                          Gettable inputs,
                                          Map<Object, Object> outputs,
                                          Transaction transaction)
      throws MirrorFieldException, MirrorNoSuchRecordException, MirrorInstantiationException,
      MirrorTransactionTimeoutException, FeedbackErrorException;

  /**
   * Request the ContentCompiler to remove any resources associated with the given Region, utilising
   * the given Transaction if possible. This is not roll-backable (at present) so it is not
   * necessary to cache any of the information that is deleted when implementing this method.
   * 
   * @param regionStyle
   *          The Region which the instance is attached to.
   * @param deleteTransaction
   *          The Transactional viewpoint to add any mirror transactions to in order to delete the
   *          requested instance.
   * @throws MirrorTransactionTimeoutException
   *           If the deleteTransaction expires or if there are locking issues grabbing the records
   *           to be deleted.
   * @throws MirrorNoSuchRecordException
   *           If the region doesn't reference this ContentCompiler or this ContentCompiler is
   *           unable to handle this region.
   */
  public abstract void destroyInstance(PersistentRecord node,
                                       PersistentRecord regionStyle,
                                       Transaction deleteTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException;

  /**
   * Request the ContentCompiler to remove any resources associated with the given Region. This
   * process should be instantaneous and complete before the method returns. The default
   * implementation invokes destroyInstance(region, transaction) with a null Transaction. If
   * transactions are required to complete the process then the subclass should create the
   * appropriate Transaction by overriding this method.
   * 
   * @param regionStyle
   *          The Region that is being destroyed.
   * @throws MirrorTransactionTimeoutException
   *           If the operation could not be completed due to locking issues.
   * @throws MirrorNoSuchRecordException
   *           If the region doesn't reference this ContentCompiler or this ContentCompiler is
   *           unable to handle this region.
   */
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    destroyInstance(node, regionStyle, null);
  }

  /**
   * Request the ContentCompiler to obtain an edit lock on any of the resources necessary to enter
   * into an edit cycle.
   * 
   * @param regionStyle
   *          The Region that this ContentCompiler implements.
   * @param editTransaction
   *          The Transaction that this ContentCompiler should be included into.
   * @throws MirrorTransactionTimeoutException
   *           If it is not possible to get an edit lock on the appropriate records in time.
   * @throws MirrorNoSuchRecordException
   *           If regionId has no record for this ContentCompiler.
   */
  public abstract void editInstance(PersistentRecord node,
                                    PersistentRecord regionStyle,
                                    Transaction editTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException;

  /**
   * Request the ContentCompiler to update the values in any WritableRecords using data held in the
   * current nodeRequest.
   * 
   * @param regionStyle
   *          The Region that is to be updated.
   * @param editTransaction
   *          The Transaction that was previouly passed to editInstance(...) and therefore should
   *          have the lock on this ContentCompiler.
   * @param inputs
   *          The current request that contains the information that is to be folded into this
   *          ContentCompiler. This will have been appropriately namespaced already so the
   *          information contained within will be available directly.
   * @param outputs
   *          The optional Map that non-blocking output messages can be placed into as a result of
   *          invoking this method.
   * @return true if the state of the page was affected by this operation.
   * @throws MirrorTransactionTimeoutException
   *           If the editTransaction has timed out or any additional locks required cannot be
   *           obtained within the permitted time.
   * @throws MirrorFieldException
   *           If any of the values contained in the nodeRequest are rejected by the back-end
   *           records.
   * @throws MirrorInstantiationException
   *           If any additional records need to be created based on the data in nodeRequest and
   *           this causes an error.
   * @throws MirrorNoSuchRecordException
   *           If regionId has no record for this ContentCompiler.
   */
  public abstract boolean updateInstance(PersistentRecord node,
                                         PersistentRecord regionStyle,
                                         Transaction editTransaction,
                                         Gettable inputs,
                                         Map<Object, Object> outputs,
                                         PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, FeedbackErrorException;

  /**
   * The webmacro template path ({@value} ) that is registered as the viewTemplatePath on the Map
   * that is returned from getPopulatedInstance() if it is not overridden by the subclass.
   */
  public final static String CONTENTCOMPILER_VIEWTEMPLATEPATH = "node/null.view.wm.html";

  /**
   * The webmacro template path ({@value} ) that is registered as the editTemplatePath on the Map
   * that is returned from getPopulatedInstance() if it is not overridden by the subclass.
   */
  public final static String CONTENTCOMPILER_EDITTEMPLATEPATH = "node/null.edit.wm.html";

  /**
   * Request the ContentCompiler to generate a populated Webmacro introspectable Object that
   * contains any information that is necessary to render the information for the requested
   * instance. The Object should have the the following webmacro compatible methods registered on
   * it:-
   * <dl>
   * <dt>viewTemplatePath
   * <dd>The webmacro path to the template that is required to render the read only version of the
   * data in this ContentCompiler.
   * <dt>editTemplatePath
   * <dd>The webmacro path to the template that is required to render the edit version of the data
   * in this ContentCompiler.
   * </dl>
   * The type of the returned object is not strongly set due to the flexibility that webmacro has in
   * resolving templates leading to a situation whereby there are multiple ways that these demands
   * may be satisfied.
   * 
   * @param regionStyle
   *          The Region that is being requested.
   * @param nodeRequest
   *          The NodeRequest that is making the query. This can therefore be used to extract
   *          additional parameters that are not discernable from the region.
   * @param viewpoint
   *          The transactional viewpoint (if any) that is held by the User making this request on
   *          the Node that owns the given Region.
   * @return The Object that is populated with the necessary information to render the template.
   * @see #CONTENTCOMPILER_VIEWTEMPLATEPATH
   * @see #CONTENTCOMPILER_EDITTEMPLATEPATH
   */
  @Deprecated
  public final Object getPopulatedInstance(PersistentRecord node,
                                           PersistentRecord regionStyle,
                                           NodeRequest nodeRequest,
                                           Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return compile(node, regionStyle, nodeRequest, viewpoint);
  }

  /**
   * Invoke this ContentCompiler against (node,regionStyle,nodeRequest) with a checked exception if
   * the compilation fails. The default implementation just invokes compileOpt and generates an
   * exception if the result is null. If you have a more elegant implementation that gives a more
   * informative error message then you should override this method.
   */
  public Gettable compile(PersistentRecord node,
                          PersistentRecord regionStyle,
                          NodeRequest nodeRequest,
                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    Gettable g = compileOpt(node, regionStyle, nodeRequest, viewpoint);
    if (g != null) {
      return g;
    }
    throw new MirrorNoSuchRecordException(String.format("Cannot compile %s on %s with %s",
                                                        regionStyle,
                                                        node,
                                                        this),
                                          (Table) null);
  }

  public abstract Gettable compileOpt(PersistentRecord node,
                                      PersistentRecord regionStyle,
                                      NodeRequest nodeRequest,
                                      Viewpoint viewpoint);

  protected Map<Object, Object> getDefaultPopulatedInstance(PersistentRecord node,
                                                            PersistentRecord regionStyle,
                                                            Viewpoint viewpoint)
  {
    Map<Object, Object> map = new HashMap<Object, Object>();
    map.put("node", node);
    map.put("regionStyle", regionStyle);
    map.put("viewpoint", viewpoint);
    map.put("ContentCompiler", this);
    map.put(VIEWTEMPLATEPATH, CONTENTCOMPILER_VIEWTEMPLATEPATH);
    map.put(EDITTEMPLATEPATH, CONTENTCOMPILER_EDITTEMPLATEPATH);
    return map;
  }

  /**
   * Get the DbInitialisers that this ContentCompiler needs registering with the Db in order to
   * function.
   * 
   * @return empty DbInitialisers. Subclasses should override this as appropriate.
   * @see DbInitialisers#EMPTY
   */
  public DbInitialisers getDbInitialisers()
  {
    return DbInitialisers.EMPTY;
  }

  /**
   * Inform the ContentCompiler that a RegionStyle has declared that it references the
   * ContentCompiler. This is performed at boot-up during the declaration of the NodeDb and will be
   * invoked for every record in the RegionStyle Table. Implementations of ContentCompiler may
   * choose to register any specific NodeStyle related triggers at this point in the knowledge of
   * which locations they are registered on. The default implementation of this does nothing.
   * 
   * @param regionStyle
   *          The RegionStyle record which is referencing the ContentCompiler.
   * @throws MirrorTableLockedException
   *           If the ContentCompiler is declared once the Db has been set into a locked state.
   * @throws MirrorNoSuchRecordException
   *           If the regionStyle references a contentCompilerStyle that is not defined in the
   *           back-end system. If the back-end storage is not based around a Table based system
   *           then a MirrorNoSuchRecordException should still be utilised, but with table and id
   *           set to null.
   */
  public abstract void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException;

  /**
   * Ask the ContentCompiler if it takes notice of the contents of the NodeRequest when processing
   * getPopulatedInstance(...). The value returned should be a constant for the duration of the
   * ContentCompiler. If the ContentCompiler occasionally honours NodeRequest then it should return
   * true.
   * 
   * @return True if the ContentCompiler renders different output based on the contents of the
   *         NodeRequest. False implies that the ContentCompiler does not mind whether or not
   *         NodeRequest is null or not. The base implementation returns false as a default,
   *         override if different behaviour is required.
   */
  public boolean utilisesNodeRequest()
  {
    return false;
  }

  /**
   * Inform the ContentCompiler that a value of $regionStyle.compilerStyleName wishes to be updated.
   * The method must decide if this action is valid. This will only be invoked with RegionStyle
   * records that reference this ContentCompiler. If the operation is sucessfull, then the
   * ContentCompiler is responsible for ensuring that all instances of itself that implement the
   * given RegionStyle should have been migrated to the new style. The default implementation of
   * ContentCompiler does not support style updates and will always throw a MirrorFieldException and
   * rollback the value.
   * 
   * @param regionStyle
   *          The RegionStyle that references this ContentCompiler and which has had it's
   *          RegionStyle record updated.
   * @throws MirrorFieldException
   *           If the updated value of compilerStyleName is rejected for any reason.
   */
  public void updateStyle(TransientRecord regionStyle)
      throws MirrorFieldException
  {
    throw new MirrorFieldException("ContentCompiler does not support regionStyle updates",
                                   null,
                                   regionStyle,
                                   RegionStyle.COMPILERSTYLENAME,
                                   regionStyle.opt(RegionStyle.COMPILERSTYLENAME),
                                   null);
  }

  /**
   * @param calendar
   *          The Calendar Object that is to be utilised for parsing the date information that is
   *          uploaded. This will be synchronized on during parsing to preserve thread safety, so it
   *          is recommended that you utilise a reference to a Calendar Object that is unlikely to
   *          be contested. Probably good value is a single value of Calendar for each instance of
   *          TableContentCompiler that exists.
   */
  public static Object uploadFastDateField(Gettable params,
                                           TransientRecord instance,
                                           PersistentRecord regionStyle,
                                           FieldKey<Date> fieldKeyDate,
                                           Calendar calendar)
      throws MirrorFieldException
  {
    Object get = params.get(fieldKeyDate.getKeyName());
    if (get != null && get instanceof Date) {
      return instance.setField(fieldKeyDate, get);
    }
    Field<Date> fieldDate = instance.getTable().getField(fieldKeyDate);
    Date date = getDate(params, instance, fieldKeyDate, calendar);
    if (fieldDate.getSupportsNull()) {
      if (params.containsKey(fieldKeyDate.getKeyName())) {
        if (get == null) {
          return instance.setField(fieldKeyDate, (Object) null);
        } else {
          if (date == null) {
            return instance.setField(fieldKeyDate, fieldDate.getDefaultDefinedValue(instance));
          } else {
            return instance.setField(fieldKeyDate, date);
          }
        }
      } else {
        return null;
      }
    } else {
      if (date != null) {
        return instance.setField(fieldKeyDate, date);
      }
    }
    return null;
  }

  public static Date getDate(Gettable params,
                             TransientRecord instance,
                             FieldKey<Date> fieldKey,
                             Calendar calendar)
  {
    Object value = params.get(fieldKey.getKeyName());
    if (value != null) {
      if (value instanceof Date) {
        return (Date) value;
      }
      value = null;
    }
    String year = params.getString(fieldKey + DateFieldExtractor.FE_YEAR);
    if (year == null) {
      return null;
    }
    calendar = calendar == null ? __calendar : calendar;
    return parseDate(params, instance.opt(fieldKey), fieldKey, calendar);
  }

  /**
   * Static utility method for extracting a Date from an IndexedFastDate update form.
   * 
   * @param calendar
   *          The Calendar that is to be used to parse the date information. If null then the static
   *          instance for ContentCompiler will be utilised. This will be synchronised on with
   *          respect to the parsing, so high demand sites should define localised Calendar
   *          instances for each sub-section to reduce the number of contested locks.
   * @deprecated This should eventually use FastDateField.getValue(Gettable), but the migration path
   *             isnot yet defined cleanly.
   */
  @Deprecated
  public static Date parseDate(Gettable params,
                               Date currentDate,
                               FieldKey<Date> fieldName,
                               Calendar calendar)
  {
    String year = params.getString(fieldName + DateFieldExtractor.FE_YEAR);
    String realMonth = params.getString(fieldName + DateFieldExtractor.FE_MONTH);
    String day = params.getString(fieldName + DateFieldExtractor.FE_DAY);
    String hour = params.getString(fieldName + DateFieldExtractor.FE_HOUR);
    String minute = params.getString(fieldName + DateFieldExtractor.FE_MINUTE);
    String second = params.getString(fieldName + DateFieldExtractor.FE_SECOND);
    calendar = calendar == null ? __calendar : calendar;
    synchronized (calendar) {
      calendar.setTime(currentDate);
      setField(calendar, Calendar.YEAR, year);
      setField(calendar, Calendar.MONTH, realMonth, -1);
      setField(calendar, Calendar.DAY_OF_MONTH, day);
      setField(calendar, Calendar.HOUR_OF_DAY, hour);
      setField(calendar, Calendar.MINUTE, minute);
      setField(calendar, Calendar.SECOND, second);
      return calendar.getTime();
    }
  }

  private static void setField(Calendar calendar,
                               int field,
                               String value)
  {
    setField(calendar, field, value, 0);
  }

  private static void setField(Calendar calendar,
                               int field,
                               String value,
                               int offset)
  {
    if (value != null) {
      try {
        calendar.set(field, Integer.parseInt(value) + offset);
      } catch (NumberFormatException nfEx) {
      }
    }
  }

  /**
   * Hook method to allow the content compiler in at the servlet initialisation phase. Called after
   * NodeServlet.initialise(NodeManager). This is the place to add calls to
   * NodeServlet.addPersistentParameter(), for example. No ordering is guaranteed between content
   * compilers.
   * 
   * @param servlet
   *          The servlet to be initialised
   */
  protected void initialiseServlet(NodeServlet servlet)
  {
  }
}
