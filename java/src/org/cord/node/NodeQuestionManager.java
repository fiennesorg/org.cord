package org.cord.node;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.cord.util.Maps;

/**
 * Manager that is responsible for tracking and allocating all of the active NodeQuestion objects in
 * a running Node system.
 * 
 * @author alex
 * @see NodeQuestion
 */
public class NodeQuestionManager
{
  private final Map<HttpSession, Map<String, NodeQuestion>> __activeQuestions =
      Maps.newConcurrentHashMap();

  protected NodeQuestionManager()
  {
  }

  private Map<String, NodeQuestion> getRwQuestions(HttpSession session)
  {
    return __activeQuestions.get(session);
  }

  private Map<String, NodeQuestion> getOrCreateRwQuestions(HttpSession session)
  {
    Map<String, NodeQuestion> questions = getRwQuestions(session);
    if (questions == null) {
      questions = Maps.newConcurrentHashMap();
      __activeQuestions.put(session, questions);
    }
    return questions;
  }

  public NodeQuestion addQuestion(HttpSession session,
                                  NodeQuestion question)
  {
    Map<String, NodeQuestion> questions = getOrCreateRwQuestions(session);
    return questions.put(question.getName(), question);
  }

  public boolean removeQuestion(HttpSession session,
                                NodeQuestion question)
  {
    Map<String, NodeQuestion> questions = getRwQuestions(session);
    if (questions == null) {
      return false;
    }
    return questions.remove(question.getName()) != null;
  }

  public Map<String, NodeQuestion> getQuestions(HttpSession session)
  {
    Map<String, NodeQuestion> questions = getRwQuestions(session);
    if (questions == null) {
      return Collections.emptyMap();
    }
    Iterator<Map.Entry<String, NodeQuestion>> entries = questions.entrySet().iterator();
    while (entries.hasNext()) {
      Entry<String, NodeQuestion> entry = entries.next();
      NodeQuestion question = entry.getValue();
      if (question.isClosed()) {
        entries.remove();
      }
    }
    return Collections.unmodifiableMap(questions);
  }
}
