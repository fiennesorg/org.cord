package org.cord.node.href;

import org.cord.mirror.TransientRecord;

public class None
  implements NodeLinkTitleGenerator
{

  /**
   * @return ""
   */
  @Override
  public String generateLinkTitle(TransientRecord node,
                                  String linkContents)
  {
    return "";
  }

}
