package org.cord.node.href;

import org.cord.mirror.TransientRecord;
import org.cord.node.Node;

public class OptTitleDescription
  implements NodeLinkTitleGenerator
{

  @Override
  public String generateLinkTitle(TransientRecord node,
                                  String linkContents)
  {
    String nodeTitle = node.opt(Node.TITLE);
    String nodeDescription = node.opt(Node.DESCRIPTION);
    if (nodeTitle.equals(linkContents)) {
      return nodeDescription;
    }
    if (nodeDescription.length() == 0) {
      return nodeTitle;
    }
    return nodeTitle + " - " + nodeDescription;
  }

}
