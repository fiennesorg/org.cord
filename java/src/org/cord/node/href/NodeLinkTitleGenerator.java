package org.cord.node.href;

import org.cord.mirror.TransientRecord;

public interface NodeLinkTitleGenerator
{
  public String generateLinkTitle(TransientRecord node,
                                  String linkContents);
}
