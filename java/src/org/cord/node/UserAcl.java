package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorAuthorisationException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableListener;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.RecordSources;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;

public class UserAcl
  extends NodeTableWrapper
  implements TableListener
{
  // *******
  // USERACL
  // *******
  /**
   * "UserAcl"
   */
  public final static String TABLENAME = "UserAcl";

  /**
   * "user_id"
   */
  public final static IntFieldKey USER_ID = LinkField.getLinkFieldName(User.TABLENAME);

  /**
   * "user"
   */
  public final static RecordOperationKey<PersistentRecord> USER = LinkField.getLinkName(USER_ID);

  /**
   * "acl_id"
   */
  public final static IntFieldKey ACL_ID = LinkField.getLinkFieldName(Acl.TABLENAME);

  /**
   * "acl"
   */
  public final static RecordOperationKey<PersistentRecord> ACL = LinkField.getLinkName(ACL_ID);

  private final Int2ObjectMap<IntSet> __acls =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<IntSet>());

  // private final ConcurrentHashMap<Integer, ImmutableSet<Integer>> __acls =
  // Maps.newConcurrentHashMap();

  protected UserAcl(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(USER_ID,
                                 "User",
                                 null,
                                 USER,
                                 User.TABLENAME,
                                 User.USERACLS,
                                 USER_ID,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(ACL_ID,
                                 "ACL",
                                 null,
                                 ACL,
                                 Acl.TABLENAME,
                                 Acl.USERACLS,
                                 USER_ID,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    // table.addFieldTrigger(new ImmutableFieldTrigger());
    table.addTableListener(this);
  }

  public boolean setLink(int userId,
                         int aclId,
                         boolean isDefined,
                         String pwd)
      throws MirrorAuthorisationException, MirrorInstantiationException, MirrorFieldException,
      MirrorTransactionTimeoutException
  {
    if (isDefined) {
      return defineLink(userId, aclId, pwd);
    } else {
      return removeLink(userId, aclId, pwd);
    }
  }

  /**
   * Define the LinkRecord between the given User and Acl if it doesn't already exist.
   * 
   * @return true if the record was created, false if it already existed.
   */
  public boolean defineLink(int userId,
                            int aclId,
                            String pwd)
      throws MirrorAuthorisationException, MirrorInstantiationException, MirrorFieldException
  {
    IntSet userIds = getUsers(aclId);
    if (userIds.contains(userId)) {
      return false;
    }
    Integer userIdInteger = Integer.valueOf(userId);
    Integer aclIdInteger = Integer.valueOf(aclId);
    getNodeManager().checkTransactionPassword(pwd,
                                              "UserAcl.defineLink(%s,%s,...)",
                                              userIdInteger,
                                              aclIdInteger);
    TransientRecord userAcl = getTable().createTransientRecord();
    userAcl.setField(USER_ID, userIdInteger);
    userAcl.setField(ACL_ID, aclIdInteger);
    userAcl.makePersistent(null);
    return true;
  }

  public boolean removeLink(int userId,
                            int aclId,
                            String transactionPassword)
      throws MirrorAuthorisationException, MirrorTransactionTimeoutException
  {
    RecordSource userAcls = getLinkQuery(aclId, userId);
    PersistentRecord userAcl = RecordSources.getOptOnlyRecord(userAcls, null);
    if (userAcl == null) {
      return false;
    }
    try (Transaction t = getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                    Transaction.TRANSACTION_DELETE,
                                                                    transactionPassword,
                                                                    "UserAcl.removeLink")) {
      t.delete(userAcl);
      t.commit();
    }
    return true;
  }

  /**
   * Get the ImmutableSet of User ids that are contained within the given Acl. The data will be
   * cached and discarded when the UserAcl table is updated so this method can be called frequently.
   */
  public IntSet getUsers(int aclId)
  {
    IntSet userIds = __acls.get(aclId);
    if (userIds != null) {
      return userIds;
    }
    StringBuilder buf = new StringBuilder();
    buf.append("acl_id=").append(aclId);
    String filter = buf.toString();
    RecordSource userAcls = getTable().getQuery(filter, filter, null);
    userIds = new IntOpenHashSet();
    for (PersistentRecord userAcl : userAcls) {
      userIds.add(userAcl.compInt(USER_ID));
    }
    __acls.put(aclId, IntSets.unmodifiable(userIds));
    return userIds;
  }

  public boolean containsLink(int aclId,
                              int userId)
  {
    return getUsers(aclId).contains(userId);
  }

  public RecordSource getLinkQuery(int aclId,
                                   int userId)
  {
    StringBuilder buf = new StringBuilder(32);
    buf.append("acl_id=").append(aclId).append(" and user_id=").append(userId);
    String filter = buf.toString();
    return getTable().getQuery(filter, filter, null);
  }

  /**
   * Drop all cached information about the relationship between Users and Acls as the data might be
   * out of date. We might make this more specialised in future, but for now it will re-query the
   * database as the relationships are asked for again so it isn't the end of the world.
   */
  @Override
  public boolean tableChanged(Table table,
                              int recordId,
                              PersistentRecord persistentRecord,
                              int type)
  {
    __acls.clear();
    return true;
  }
}
