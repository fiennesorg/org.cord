package org.cord.node;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.node.operation.BreadcrumbTrail;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.SortedIterator;

public final class BreadcrumbNodeStyleComparator
  implements Comparator<TransientRecord>, SingletonShutdownable
{
  private static BreadcrumbNodeStyleComparator _instance = new BreadcrumbNodeStyleComparator();

  public static BreadcrumbNodeStyleComparator getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private BreadcrumbNodeStyleComparator()
  {
    SingletonShutdown.registerSingleton(this);
  }

  public static Iterator<TransientRecord> sortByNodeStyle(Iterator<TransientRecord> nodes)
  {
    return new SortedIterator<TransientRecord>(nodes, getInstance());
  }

  @Override
  public int compare(TransientRecord n1,
                     TransientRecord n2)
  {
    if (n1.getId() == n2.getId()) {
      return 0;
    }
    List<TransientRecord> trail1 = BreadcrumbTrail.getNodes(n1);
    List<TransientRecord> trail2 = BreadcrumbTrail.getNodes(n2);
    int shortestSize = Math.min(trail1.size(), trail2.size());
    for (int i = 0; i < shortestSize; i++) {
      PersistentRecord parentNode1 = (PersistentRecord) trail1.get(i);
      PersistentRecord parentNode2 = (PersistentRecord) trail2.get(i);
      if (parentNode1.getId() != parentNode2.getId()) {
        PersistentRecord parentNodeStyle1 = parentNode1.comp(Node.NODESTYLE);
        PersistentRecord parentNodeStyle2 = parentNode2.comp(Node.NODESTYLE);
        int parentNodeStyleComparison =
            Integer.compare(parentNodeStyle1.getId(), parentNodeStyle2.getId());
        switch (parentNodeStyleComparison) {
          case 0:
            return parentNode1.comp(Node.TITLE).compareTo(parentNode2.comp(Node.TITLE));
          default:
            return parentNodeStyleComparison;
        }
      }
    }
    return (trail1.size() == trail2.size()
        ? n1.comp(Node.TITLE).compareTo(n2.comp(Node.TITLE))
        : (trail1.size() > trail2.size() ? 1 : -1));
  }
}
