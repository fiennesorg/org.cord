package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

/**
 * PostViewAction that will cause a RedirectionException to be thrown to the original node.
 */
public class SelfRedirectionSuccessOperation
  implements PostViewAction
{
  private static final SelfRedirectionSuccessOperation INSTANCE = new SelfRedirectionSuccessOperation();

  public static SelfRedirectionSuccessOperation getInstance()
  {
    return INSTANCE;
  }
  
  private SelfRedirectionSuccessOperation()
  {
  }

  @Override
  public void shutdown()
  {
  }

  /**
   * @throws RedirectionException
   *           to node, always.
   */
  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws RedirectionException
  {
    throw new RedirectionException(node);
  }

}
