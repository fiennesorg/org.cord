package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

public class FeedbackSuccessOperation
  implements PostViewAction
{
  private final String __title;

  private final String __message;

  public FeedbackSuccessOperation(String title,
                                  String message)
  {
    __title = title;
    __message = message;
  }

  @Override
  public void shutdown()
  {
  }

  public FeedbackSuccessOperation(String message)
  {
    this(null,
         message);
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws FeedbackErrorException
  {
    throw new FeedbackErrorException(__title, node, nestedException, false, false, __message);
  }
}
