package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.StringUtils;
import org.webmacro.Context;

/**
 * Generate and throw a FeedbackErrorException with a dynamically generated title and message. The
 * title and message are expected to be in the Context that is passed to the handleSuccess(...)
 * method. If the title is not defined, then the title of the node that the operation is invoked on
 * is used instead. All views that are currently throwing FeedbackErrorExceptions manually inside
 * their handling methods should substitute this with a critical error plugin and ensure that the
 * messages are placed inside the context before invoking the plugin, using the TITLENAME and
 * MESSAGENAME variables. Then make the default action for the critical error plugin set to
 * ContextFeedbackSuccessOperation.getInstance() and you should have a functionally identical system
 * which permits alternative error handling as and when required.
 * <p>
 * There is nothing to stop you registering a different plugin for each error condition if you feel
 * that the benefits on this outweigh the added programmatic weight for clients. If you are going
 * this way, then I would advise having an alternative constructor that uses a single critical error
 * definition as well for run-of-the-mill operations.
 */
public final class ContextFeedbackSuccessOperation
  implements PostViewAction, SingletonShutdownable
{
  /**
   * The name of the Context variable that is expected to contain the title of the
   * FeedbackErrorException that is thrown by the default getInstance() implementation.
   */
  public final static String TITLENAME = "ContextFeedbackSuccessOperation_title";

  /**
   * The name of the Context variable that is expected to contain the message of the
   * FeedbackErrorException that is thrown by the default getInstance() implementation.
   */
  public final static String MESSAGENAME = "ContextFeedbackSuccessOperation_message";

  private static ContextFeedbackSuccessOperation _instance =
      new ContextFeedbackSuccessOperation(TITLENAME, MESSAGENAME);
  static {
    SingletonShutdown.registerSingleton(_instance);
  }

  /**
   * Get the Singleton method that utilises the TITLENAME and MESSAGENAME configuration of this
   * class. This should be the default expected behaviour that should be assumed when coding views.
   * The only time that one would expect to implement dedicated versions of
   * ContextFeedbackSuccessOperation would be when there is a need to have multiple messages stashed
   * in the Context simultaneously, but this is by definition a very custom requirement.
   * 
   * @see #TITLENAME
   * @see #MESSAGENAME
   */
  public static ContextFeedbackSuccessOperation getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private final String __titleContextName;

  private final String __messageContextName;

  public ContextFeedbackSuccessOperation(String titleContextName,
                                         String messageContextName)
  {
    __titleContextName = titleContextName;
    __messageContextName = messageContextName;
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws FeedbackErrorException
  {
    String title =
        __titleContextName == null ? null : StringUtils.toString(context.get(__titleContextName));
    title = title == null ? node.opt(Node.TITLE) : title;
    String message = StringUtils.toString(context.get(__messageContextName));
    throw new FeedbackErrorException(title, node, nestedException, false, false, message);
  }

  @Override
  public void shutdown()
  {
  }
}