package org.cord.node;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;

/**
 * RecordOperation name "isGloballyPublished" that checks to see if this Node and all its parents
 * have NODE_ISPUBLISHED set to true. This basically guarantees a clean and public route to the
 * node.
 * <P>
 * 2DO: The isPublished thing should really take into account the viewAcl on each of the nodes
 * rather than just the flag.
 */
public class NodeIsGloballyPublished
  extends SimpleRecordOperation<Boolean>
{
  public final static RecordOperationKey<Boolean> NAME =
      RecordOperationKey.create("isGloballyPublished", Boolean.class);

  public NodeIsGloballyPublished()
  {
    super(NAME);
  }

  @Override
  public final Boolean getOperation(TransientRecord node,
                                    Viewpoint viewpoint)
  {
    if (!node.is(Node.ISPUBLISHED)) {
      return Boolean.FALSE;
    }
    node = node.opt(Node.PARENTNODE, viewpoint);
    if (node != null) {
      return node.opt(NodeIsGloballyPublished.NAME);
    }
    return Boolean.TRUE;
  }
}
