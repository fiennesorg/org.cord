package org.cord.node;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.cord.util.DebugConstants;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;
import com.google.common.collect.MapMaker;

/**
 * The Manager for the Include system that is responsible for keeping track of what files are
 * available, refreshing this list and creating new empty IncludeList items.
 * 
 * @author alex
 */
@Deprecated
public class IncludeMgr
{
  private final Map<String, IncludeFile> __includeFiles = new MapMaker().makeMap();
  private final Map<String, IncludeFile> __roIncludeFiles =
      Collections.unmodifiableMap(__includeFiles);

  private final File __webRoot;

  public IncludeMgr(File webRoot)
  {
    __webRoot = Preconditions.checkNotNull(webRoot);
  }

  public final File getWebRoot()
  {
    return __webRoot;
  }

  public synchronized IncludeFile register(String key,
                                           String webPath,
                                           String... dependencies)
  {
    Preconditions.checkNotNull(key, "key");
    Preconditions.checkNotNull(webPath, "webPath");
    for (String dependency : dependencies) {
      if (!__includeFiles.containsKey(dependency)) {
        throw new IllegalArgumentException(String.format("%s has an unresolved dependency of %s",
                                                         key,
                                                         dependency));
      }
    }
    IncludeFile includeFile = new IncludeFile(this, key, webPath, dependencies);
    __includeFiles.put(key, includeFile);
    return includeFile;
  }

  public IncludeFile register(String config)
  {
    try {
      Iterator<String> i = Splitter.on(" ").omitEmptyStrings().split(config).iterator();
      return register(i.next(), i.next(), Iterators.toArray(i, String.class));
    } catch (RuntimeException e) {
      throw new IllegalArgumentException(String.format("Unable to parse \"%s\"", config), e);
    }
  }

  public void registerAll(File configFile,
                          boolean debugFiles)
      throws IOException
  {
    if (debugFiles) {
      DebugConstants.method(this, "registerAll", configFile);
    }
    if (!configFile.isFile()) {
      if (debugFiles) {
        DebugConstants.DEBUG_OUT.println(" --> file not found");
      }
      return;
    }
    BufferedReader br = new BufferedReader(new FileReader(configFile));
    try {
      String line = null;
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (!(line.length() == 0 || line.startsWith("#"))) {
          IncludeFile includeFile = register(line);
          if (debugFiles) {
            DebugConstants.DEBUG_OUT.println(" --> " + includeFile);
          }
        }
      }
    } finally {
      br.close();
    }

  }

  public Map<String, IncludeFile> getIncludeFiles()
  {
    return __roIncludeFiles;
  }

  public IncludeList newIncludeList()
  {
    return new IncludeList(this);
  }
}
