package org.cord.node;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.Transaction;
import org.cord.node.view.AddNodeFactory;
import org.cord.node.view.AnswerNodeQuestion;
import org.cord.node.view.CancelDeleteFactory;
import org.cord.node.view.CancelFactory;
import org.cord.node.view.CancelTransactionsFactory;
import org.cord.node.view.CommitFactory;
import org.cord.node.view.ConfirmDeleteFactory;
import org.cord.node.view.CopyNodeCancel;
import org.cord.node.view.CopyNodeCommit;
import org.cord.node.view.CopyNodeInit;
import org.cord.node.view.DeleteNodeFactory;
import org.cord.node.view.EditFactory;
import org.cord.node.view.MoveDownFactory;
import org.cord.node.view.MoveNodeCancel;
import org.cord.node.view.MoveNodeCommit;
import org.cord.node.view.MoveNodeInit;
import org.cord.node.view.MoveUpFactory;
import org.cord.node.view.PrecommitFactory;
import org.cord.node.view.ReadViewFactory;
import org.cord.node.view.RedirectView;
import org.cord.node.view.RemoveAnswerNodeQuestion;
import org.cord.node.view.RequestLoginFactory;
import org.cord.node.view.StealTransactionFactory;
import org.cord.node.view.SwapNodeFactory;
import org.cord.node.view.UpdateCommitFactory;
import org.cord.node.view.UpdateFactory;
import org.cord.node.view.ViewChildrenNodes;
import org.cord.node.view.user.ChangePassword;
import org.cord.node.view.user.Login;
import org.cord.node.view.user.Logout;
import org.cord.util.DebugConstants;

/**
 * Container class responsible for holding the registered NodeRequestSegmentFactories for all
 * Transaction types under a single NodeStyle.
 */
public class NodeRequestSegmentFactoryFactory
{
  // name (String) --> NodeRequestSegmentFactory
  private final Map<String, NodeRequestSegmentFactory> __transactionNoneFactories =
      new HashMap<String, NodeRequestSegmentFactory>();

  public Map<String, NodeRequestSegmentFactory> getTransactionNoneFactories()
  {
    return Collections.unmodifiableMap(__transactionNoneFactories);
  }

  private NodeRequestSegmentFactory _defaultFactoryNone = null;

  private final Map<String, NodeRequestSegmentFactory> __transactionUpdateFactories =
      new HashMap<String, NodeRequestSegmentFactory>();

  private NodeRequestSegmentFactory _defaultFactoryUpdate = null;

  private final Map<String, NodeRequestSegmentFactory> __transactionDeleteFactories =
      new HashMap<String, NodeRequestSegmentFactory>();

  private NodeRequestSegmentFactory _defaultFactoryDelete = null;

  private final int __nodeStyleId;

  private NodeManager _nodeManager;

  /**
   * Create a new factory supplier that is associated with a specific NodeStyle.
   * 
   * @param nodeManager
   *          the manager for this system.
   * @param nodeStyleId
   *          The value of NodeStyle.id that this factory supplier is to be associated with.
   * @throws MirrorNoSuchRecordException
   *           If the value of nodeStyleId was not found inside NodeStyle.
   * @see NodeStyle#TABLENAME
   */
  public NodeRequestSegmentFactoryFactory(NodeManager nodeManager,
                                          int nodeStyleId)
      throws MirrorNoSuchRecordException
  {
    if (!(nodeStyleId == NodeRequestSegmentManager.NODESTYLEID_GLOBAL | nodeStyleId >= 1)) {
      throw new IllegalArgumentException("Invalid nodeStyleId: " + nodeStyleId);
    }
    __nodeStyleId = nodeStyleId;
    _nodeManager = nodeManager;
    if (nodeStyleId != NodeRequestSegmentManager.NODESTYLEID_GLOBAL) {
      _nodeManager.getDb().getTable(NodeStyle.TABLENAME).getRecord(nodeStyleId);
    }
  }
  public NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  public void shutdown()
  {
    shutdownFactories(__transactionNoneFactories);
    shutdownFactories(__transactionUpdateFactories);
    shutdownFactories(__transactionDeleteFactories);
    _nodeManager = null;
  }

  private void shutdownFactories(Map<String, NodeRequestSegmentFactory> factoryMap)
  {
    for (NodeRequestSegmentFactory factory : factoryMap.values()) {
      factory.shutdown();
    }
    factoryMap.clear();
  }

  @Override
  public String toString()
  {
    return "NodeRequestSegmentFactoryFactory#" + __nodeStyleId;
  }

  /**
   * Create a new style agnostic factory supplier. This will have getNodeStyleId() == 0. It will
   * also have the effect of preregistering all of the standard views defined in NodeCgiNames whos
   * definitions lie in org.cord.node.view. All of these views are integral to the smooth operation
   * of the node system and can be configured through the manipulation of the NodeStyle structure.
   * If however it is so required then they may be replaced with additional views (or null
   * implementations) at a later date.
   */
  public NodeRequestSegmentFactoryFactory(NodeManager nodeManager)
      throws MirrorNoSuchRecordException
  {
    this(nodeManager,
         0);
  }

  public void registerDefaults()
  {
    // #### check transactional validity of AddNodeFactory
    AddNodeFactory.register(_nodeManager, this);
    register(new Login(_nodeManager));
    register(new Logout(_nodeManager));
    register(new RequestLoginFactory(_nodeManager));
    // register(new RequestPasswordClueFactory(_nodeManager));
    // register(new RequestPasswordMailoutFactory(_nodeManager));
    register(new CancelTransactionsFactory(_nodeManager));
    register(new MoveUpFactory(_nodeManager));
    register(new MoveDownFactory(_nodeManager));
    register(new SwapNodeFactory(_nodeManager));
    register(new AnswerNodeQuestion(_nodeManager));
    register(new RemoveAnswerNodeQuestion(_nodeManager));
    // register(TRANSACTION_UPDATE,
    // new ChangeFilenameFactory(_nodeManager));
    EditFactory editFactory = new EditFactory(_nodeManager);
    register(Transaction.TRANSACTION_UPDATE, new DeleteNodeFactory(_nodeManager));
    register(Transaction.TRANSACTION_UPDATE, new PrecommitFactory(_nodeManager));
    register(Transaction.TRANSACTION_UPDATE, new UpdateFactory(_nodeManager));
    register(Transaction.TRANSACTION_UPDATE, new CancelFactory(_nodeManager));
    register(Transaction.TRANSACTION_UPDATE, new CommitFactory(_nodeManager));
    register(Transaction.TRANSACTION_UPDATE, editFactory, true);
    register(Transaction.TRANSACTION_UPDATE, new UpdateCommitFactory(_nodeManager));
    register(Transaction.TRANSACTION_DELETE, new ConfirmDeleteFactory(_nodeManager));
    register(Transaction.TRANSACTION_DELETE, new CancelDeleteFactory(_nodeManager));
    register(Transaction.TRANSACTION_DELETE, new DeleteNodeFactory(_nodeManager), true);
    // register(Transaction.TRANSACTION_NULL,
    // new AddUserFactory(_nodeManager,
    // _nodeManager.getTransactionPassword()));
    // register(Transaction.TRANSACTION_NULL,
    // new ConfirmUserFactory(_nodeManager));
    register(Transaction.TRANSACTION_NULL, editFactory);
    register(Transaction.TRANSACTION_NULL, new ReadViewFactory(_nodeManager), true);
    register(Transaction.TRANSACTION_NULL, new StealTransactionFactory(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new CopyNodeInit(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new CopyNodeCommit(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new CopyNodeCancel(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new MoveNodeInit(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new MoveNodeCommit(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new MoveNodeCancel(_nodeManager));
    register(Transaction.TRANSACTION_NULL, new RedirectView(_nodeManager));
    register(Transaction.TRANSACTION_NULL,
             new ChangePassword(_nodeManager,
                                new FeedbackSuccessOperation("Change Password",
                                                             "Your password has been updated"),
                                null,
                                _nodeManager.getTransactionPassword(),
                                ChangePassword.RESOLVE_CURRENTUSER,
                                null,
                                new FeedbackSuccessOperation("Change Password",
                                                             "Your current password was incorrect")));
    register(new ViewChildrenNodes(_nodeManager));
  }

  /**
   * Get the value of NodeStyle.id that this factory supplier is associated with.
   * 
   * @return The value of NodeStyle.id or an Integer wrapper around (0) that corresponds to the
   *         generic factory supplier. This is guaranteed not to be null and if not zero then will
   *         have corresponded to a valid NodeStyle record when the factory supplier was booted.
   */
  public int getNodeStyleId()
  {
    return __nodeStyleId;
  }

  /**
   * @return true if object is a NodeRequestSegmentFactoryFactory and it has the same value of
   *         getNodeStyleId() as this. No attempt is made to compare the registered contents of the
   *         container.
   */
  @Override
  public final boolean equals(Object object)
  {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    NodeRequestSegmentFactoryFactory factory = (NodeRequestSegmentFactoryFactory) object;
    return __nodeStyleId == factory.getNodeStyleId();
  }

  /**
   * @return the value of getNodeStyleId()
   * @see #getNodeStyleId()
   */
  @Override
  public final int hashCode()
  {
    return __nodeStyleId;
  }

  public String getDefaultView(int transactionType)
  {
    NodeRequestSegmentFactory defaultFactory = null;
    switch (transactionType) {
      case Transaction.TRANSACTION_NULL:
        defaultFactory = _defaultFactoryNone;
        break;
      case Transaction.TRANSACTION_UPDATE:
        defaultFactory = _defaultFactoryUpdate;
        break;
      case Transaction.TRANSACTION_DELETE:
        defaultFactory = _defaultFactoryDelete;
        break;
      default:
        throw new IllegalArgumentException("Unknown transactiontype:" + transactionType);
    }
    return (defaultFactory == null ? "" : defaultFactory.getName());
  }

  /**
   * Request the NodeRequestSegmentFactory that has been registered to handle the named view under
   * the given transaction type.
   * 
   * @param transactionType
   *          The transaction type corresponding to the transactional lock (if any) that the
   *          requesting session has over the leaf node.
   * @param view
   *          The name of the view being requested. If this is null then the method will always
   *          return null.
   * @return The registered NodeRequestSegmentFactory or null if there is not a registered factory
   *         for the given view and transaction type.
   */
  public final NodeRequestSegmentFactory getFactory(int transactionType,
                                                    String view)
  {
    NodeRequestSegmentFactory result = null;
    switch (transactionType) {
      case Transaction.TRANSACTION_NULL:
        result = __transactionNoneFactories.get(view);
        if (result == null) {
          if (DebugConstants.DEBUG_ACCESSLOG && _defaultFactoryNone != null) {
            DebugConstants.DEBUG_OUT.println(getNodeManager().getHostName() + ":   unknown:" + view
                                             + " --> " + _defaultFactoryNone.getName());
          }
          return _defaultFactoryNone;
        } else {
          return result;
        }
      case Transaction.TRANSACTION_UPDATE:
        result = __transactionUpdateFactories.get(view);
        if (result == null) {
          if (DebugConstants.DEBUG_ACCESSLOG && _defaultFactoryUpdate != null) {
            DebugConstants.DEBUG_OUT.println(getNodeManager().getHostName() + ":   unknown:" + view
                                             + " --> " + _defaultFactoryUpdate.getName());
          }
          return _defaultFactoryUpdate;
        } else {
          return result;
        }
      case Transaction.TRANSACTION_DELETE:
        result = __transactionDeleteFactories.get(view);
        if (result == null) {
          if (DebugConstants.DEBUG_ACCESSLOG && _defaultFactoryDelete != null) {
            DebugConstants.DEBUG_OUT.println(getNodeManager().getHostName() + ":   unknown:" + view
                                             + " --> " + _defaultFactoryDelete.getName());
          }
          return _defaultFactoryDelete;
        } else {
          return result;
        }
      default:
        throw new IllegalArgumentException("Unknown transactionType:" + transactionType);
    }
  }

  public final NodeRequestSegmentFactory getFactoryWithoutDefault(int transactionType,
                                                                  String view)
  {
    switch (transactionType) {
      case Transaction.TRANSACTION_NULL:
        return __transactionNoneFactories.get(view);
      case Transaction.TRANSACTION_UPDATE:
        return __transactionUpdateFactories.get(view);
      case Transaction.TRANSACTION_DELETE:
        return __transactionDeleteFactories.get(view);
      default:
        throw new IllegalArgumentException("Unknown transactionType:" + transactionType);
    }
  }

  /**
   * Register a NodeRequestSegmentFactory under this factory supplier for a given transaction type.
   * Please note that you may register the same factory under multiple transaction types if the
   * factory accepts requests under different situations. If a factory is already registered under
   * the supplied name then it will be replaced. This therefore lets plug-and-play replacement of
   * functionality in the future if the default handlers are not sufficient for the specific purpose
   * of a page.
   * 
   * @param transactionType
   *          The transaction type that this factory is prepared to handle.
   * @param factory
   *          The factory that is to be registered. If null, then the request is ignored.
   * @return The previous factory (if any) that was registered under the name of the factory for the
   *         given transactionType.
   */
  protected final NodeRequestSegmentFactory register(int transactionType,
                                                     NodeRequestSegmentFactory factory,
                                                     boolean isDefaultFactory)
  {
    if (factory != null) {
      switch (transactionType) {
        case Transaction.TRANSACTION_NULL:
          if (isDefaultFactory) {
            _defaultFactoryNone = factory;
          }
          return __transactionNoneFactories.put(factory.getName(), factory);
        case Transaction.TRANSACTION_UPDATE:
          if (isDefaultFactory) {
            _defaultFactoryUpdate = factory;
          }
          return __transactionUpdateFactories.put(factory.getName(), factory);
        case Transaction.TRANSACTION_DELETE:
          if (isDefaultFactory) {
            _defaultFactoryDelete = factory;
          }
          return __transactionDeleteFactories.put(factory.getName(), factory);
        default:
          throw new IllegalArgumentException("Unknown transactionType:" + transactionType);
      }
    }
    return null;
  }

  public final NodeRequestSegmentFactory register(int transactionType,
                                                  NodeRequestSegmentFactory factory)
  {
    return register(transactionType, factory, false);
  }

  /**
   * Register the given factory under all supported transaction types. This therefore implies that
   * this factory will always be supplied by this object when asked for by name regardless of
   * transactional context. This will overwrite any instances of factories under any transactional
   * types that have the same name.
   * 
   * @param factory
   *          The factory that is to be blanket registered.
   */
  public final void register(NodeRequestSegmentFactory factory)
  {
    register(Transaction.TRANSACTION_DELETE, factory);
    register(Transaction.TRANSACTION_UPDATE, factory);
    register(Transaction.TRANSACTION_NULL, factory);
  }
}
