package org.cord.node;

import java.util.Date;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.sql.SqlUtil;

public final class MovedNode
  extends NodeTableWrapper
{
  public static final String TABLENAME = "MovedNode";

  //
  // fields...
  //
  public static final IntFieldKey PARENTNODE_ID = IntFieldKey.create("parentNode_id");

  public static final RecordOperationKey<PersistentRecord> PARENTNODE =
      LinkField.getLinkName(PARENTNODE_ID);

  public static final RecordOperationKey<RecordSource> CHILDMOVEDNODES =
      RecordOperationKey.create("childMovedNodes", RecordSource.class);

  public static final ObjectFieldKey<String> OLDFILENAME = StringField.createKey("oldFilename");

  public static final IntFieldKey MOVEDNODE_ID = IntFieldKey.create("movedNode_id");

  public static final RecordOperationKey<PersistentRecord> MOVEDNODE =
      LinkField.getLinkName(MOVEDNODE_ID);

  public static final RecordOperationKey<RecordSource> OLDMOVEDNODES =
      RecordOperationKey.create("oldMovedNodes", RecordSource.class);

  public static final ObjectFieldKey<Date> TIMESTAMP = FastDateField.createKey("timestamp");

  public MovedNode(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(PARENTNODE_ID,
                                 "Parent node",
                                 null,
                                 PARENTNODE,
                                 Node.TABLENAME,
                                 CHILDMOVEDNODES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new StringField(OLDFILENAME, "Old filename"));
    table.addField(new LinkField(MOVEDNODE_ID,
                                 "Old locations",
                                 null,
                                 MOVEDNODE,
                                 Node.TABLENAME,
                                 OLDMOVEDNODES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new FastDateField(TIMESTAMP, "Creation timestamp"));
    table.setDefaultOrdering(TABLENAME + ".id desc");
  }

  /**
   * @return PersistentRecord from MovedNode if one exists
   * @throws MirrorNoSuchRecordException
   *           if there isn't a movedNode with the given oldFilename below parentNode
   */
  public PersistentRecord getMovedNodeByFilename(TransientRecord parentNode,
                                                 String oldFilename,
                                                 Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    StringBuilder buf = new StringBuilder();
    buf.append(TABLENAME + "." + PARENTNODE_ID + "=").append(parentNode.getId());
    buf.append(" and " + TABLENAME + "." + OLDFILENAME + "=");
    SqlUtil.toSafeSqlString(buf, oldFilename);
    Query query = getTable().getQuery(null, buf.toString(), null);
    return RecordSources.getFirstRecord(query, viewpoint);
  }

  public PersistentRecord addMovedNode(PersistentRecord parentNode,
                                       String oldFilename,
                                       PersistentRecord movedNode)
      throws MirrorInstantiationException, MirrorFieldException
  {
    TransientRecord instance = getTable().createTransientRecord();
    instance.setField(PARENTNODE_ID, Integer.valueOf(parentNode.getId()));
    instance.setField(OLDFILENAME, oldFilename);
    instance.setField(MOVEDNODE_ID, Integer.valueOf(movedNode.getId()));
    getNodeManager().getNode().getChildNamed().invalidateCache(parentNode, oldFilename);
    PersistentRecord pers = instance.makePersistent(null);
    return pers;
  }
}
