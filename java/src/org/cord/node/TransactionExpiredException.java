package org.cord.node;

import org.cord.mirror.PersistentRecord;

public class TransactionExpiredException
  extends LockingException
{
  /**
   * 
   */
  private static final long serialVersionUID = 2144635463784889723L;

  public TransactionExpiredException(String message,
                                     PersistentRecord triggerNode,
                                     Exception nestedException)
  {
    super(message,
          triggerNode,
          nestedException);
  }
}
