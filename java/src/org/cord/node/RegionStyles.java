package org.cord.node;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class RegionStyles
{
  private RegionStyles()
  {
  }

  public static void create(NodeManager nodeMgr,
                            Predicate<Integer> validNodeStyles,
                            int number,
                            String name,
                            String aliasName,
                            String title,
                            String compilerName,
                            String compilerStyleName,
                            Integer editAcl_id,
                            Integer updateEditAcl_id)
      throws MirrorInstantiationException, MirrorFieldException
  {
    Preconditions.checkArgument(!nodeMgr.isBooted(), "Cannot invoke on a booted NodeManager");
    Table regionStyleTable = nodeMgr.getDb().getTable(RegionStyle.TABLENAME);
    for (PersistentRecord nodeStyle : nodeMgr.getDb().getTable(NodeStyle.TABLENAME)) {
      if (validNodeStyles.apply(Integer.valueOf(nodeStyle.getId()))) {
        TransientRecord regionStyle = regionStyleTable.createTransientRecord();
        regionStyle.setField(RegionStyle.NODESTYLE_ID, nodeStyle);
        regionStyle.setField(RegionStyle.NUMBER, Integer.valueOf(number));
        regionStyle.setField(RegionStyle.NAME, name);
        regionStyle.setField(RegionStyle.ALIASNAME, aliasName);
        regionStyle.setField(RegionStyle.TITLE, title);
        regionStyle.setField(RegionStyle.COMPILERNAME, compilerName);
        regionStyle.setField(RegionStyle.COMPILERSTYLENAME, compilerStyleName);
        regionStyle.setFieldIfDefined(RegionStyle.EDITACL_ID, editAcl_id);
        regionStyle.setFieldIfDefined(RegionStyle.UPDATEEDITACL_ID, updateEditAcl_id);
        regionStyle.makePersistent(null);
      }
    }
  }

  public static void create(NodeManager nodeMgr,
                            Predicate<Integer> validNodeStyles,
                            String name,
                            String title,
                            String compilerName,
                            String compilerStyleName)
      throws MirrorInstantiationException, MirrorFieldException
  {
    create(nodeMgr,
           validNodeStyles,
           1,
           name,
           "",
           title,
           compilerName,
           compilerStyleName,
           null,
           null);
  }
}
