package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

/**
 * Implementation of PostViewAction that throws an AuthenticationException with a dynamically
 * generated message.
 * 
 * @author alex
 */
public class AuthenticationPostViewAction
  implements PostViewAction
{
  private final NodeManager __nodeManager;

  private final ValueFactory __message;

  public AuthenticationPostViewAction(NodeManager nodeManager,
                                      ValueFactory message)
  {
    __message = message;
    __nodeManager = nodeManager;
  }

  protected NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws NodeRequestException
  {
    try {
      throw new AuthenticationException(__message.getValue(getNodeManager(), nodeRequest),
                                        node,
                                        null);
    } catch (MirrorNoSuchRecordException e) {
      throw new AuthenticationException("Unable to resolve message: " + e, node, e);
    }
  }

  @Override
  public void shutdown()
  {
  }
}
