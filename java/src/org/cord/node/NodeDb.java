package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Field;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AsInitials;
import org.cord.mirror.operation.DateFormatter;
import org.cord.mirror.operation.ExpandField;
import org.cord.mirror.operation.Iso8601DateTime;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.Opt;
import org.cord.mirror.operation.RFC822DateFormatter;
import org.cord.mirror.recordsource.operation.AsListOf;
import org.cord.mirror.recordsource.operation.BooleanFilter;
import org.cord.mirror.recordsource.operation.BooleanNotFilter;
import org.cord.mirror.recordsource.operation.Excluding;
import org.cord.mirror.recordsource.operation.FirstRecord;
import org.cord.mirror.recordsource.operation.FollowLink;
import org.cord.mirror.recordsource.operation.LastRecord;
import org.cord.mirror.recordsource.operation.Limit;
import org.cord.mirror.recordsource.operation.NumericalEquals;
import org.cord.mirror.recordsource.operation.NumericalGreaterThan;
import org.cord.mirror.recordsource.operation.NumericalLessThan;
import org.cord.mirror.recordsource.operation.NumericalNotEquals;
import org.cord.mirror.recordsource.operation.OrderedBy;
import org.cord.mirror.recordsource.operation.RandomOrder;
import org.cord.mirror.recordsource.operation.RandomRecord;
import org.cord.mirror.recordsource.operation.Reverse;
import org.cord.mirror.recordsource.operation.Size;
import org.cord.mirror.recordsource.operation.SqlFilter;
import org.cord.mirror.recordsource.operation.StartsWithFilter;
import org.cord.mirror.recordsource.operation.StringMatcherFilter;
import org.cord.mirror.recordsource.operation.StripDuplicates;
import org.cord.mirror.recordsource.operation.Union;
import org.cord.node.operation.HtmlDebug;
import org.cord.node.operation.InputValue;
import org.cord.node.operation.Nbsp;
import org.cord.node.operation.NoHtml;
import org.cord.node.operation.UrlEncode;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.NamedImpl;
import org.cord.util.StringUtils;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;

/**
 * Class to construct the Mirror structure for the Node system.
 */
public class NodeDb
  extends NamedImpl
  implements DbInitialiser
{
  public final static String NAME = "org.cord.node.NodeDb";

  private NodeManager _nodeManager;

  public NodeDb(NodeManager nodeManager)
  {
    super(NAME);
    _nodeManager = nodeManager;
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  public NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  public static final String DATEFORMATTER_HEADER = "DateFormatter.";
  public static final String DATEFORMATTER_NODE = "node.";
  public static final String DATEFORMATTER_APP = "app.";
  public static final String DATEFORMATTER_KEYS = "keys";

  private void addDateFormatters(Db db,
                                 Gettable gettable,
                                 String keysPrefix)
      throws MirrorTableLockedException
  {
    String keys = StringUtils.trim(gettable.getString(DATEFORMATTER_HEADER + keysPrefix
                                                      + DATEFORMATTER_KEYS));
    if (!Strings.isNullOrEmpty(keys)) {
      for (String key : Splitter.on(',').trimResults().omitEmptyStrings().split(keys)) {
        DateFormatter dateFormatter =
            new DateFormatter(MonoRecordOperationKey.create(key, Object.class, String.class),
                              gettable.getString(DATEFORMATTER_HEADER + key));
        db.add(dateFormatter);
        getNodeManager().registerDateFormatter(dateFormatter);
      }
    }
  }

  private void addDateFormatters(Db db,
                                 Gettable gettable)
      throws MirrorTableLockedException
  {
    addDateFormatters(db, gettable, DATEFORMATTER_NODE);
    addDateFormatters(db, gettable, DATEFORMATTER_APP);
  }

  public static final MonoRecordOperationKey<String, String> TOSTRING =
      MonoRecordOperationKey.create("toString", String.class, String.class);

  /**
   * $record.noHtml.operation
   * 
   * @see StringUtils#noHtml(Object)
   */
  public static final MonoRecordOperationKey<String, String> NOHTML = NoHtml.NOHTML();

  /**
   * @see BooleanFilter
   * @see BooleanNotFilter
   * @see FirstRecord
   * @see LastRecord
   * @see Limit
   * @see NumericalEquals
   * @see NumericalGreaterThan
   * @see NumericalLessThan
   * @see NumericalNotEquals
   * @see OrderedBy
   * @see RandomOrder
   * @see RandomRecord
   * @see Reverse
   * @see Size
   * @see StartsWithFilter
   * @see StringMatcherFilter
   * @see SqlFilter
   * @see ExpandField
   * @see AsInitials
   * @see HtmlDebug
   * @see Nbsp
   * @see UrlEncode
   * @see InputValue
   */
  @Override
  public final void init(Db db,
                         String transactionPassword)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    db.addRecordSourceOperation(new BooleanFilter());
    db.addRecordSourceOperation(new BooleanNotFilter());
    db.addRecordSourceOperation(new Excluding());
    db.addRecordSourceOperation(new FirstRecord());
    db.addRecordSourceOperation(new FollowLink());
    db.addRecordSourceOperation(new LastRecord());
    db.addRecordSourceOperation(new Limit());
    db.addRecordSourceOperation(new NumericalEquals());
    db.addRecordSourceOperation(new NumericalGreaterThan());
    db.addRecordSourceOperation(new NumericalLessThan());
    db.addRecordSourceOperation(new NumericalNotEquals());
    db.addRecordSourceOperation(new OrderedBy());
    db.addRecordSourceOperation(new RandomOrder());
    db.addRecordSourceOperation(new RandomRecord());
    db.addRecordSourceOperation(new Reverse());
    db.addRecordSourceOperation(new Size());
    db.addRecordSourceOperation(new StartsWithFilter());
    db.addRecordSourceOperation(new StringMatcherFilter());
    db.addRecordSourceOperation(new StripDuplicates());
    db.addRecordSourceOperation(new SqlFilter());
    db.addRecordSourceOperation(new Union());
    db.addRecordSourceOperation(new AsListOf());
    db.add(new Opt());
    db.add(new ExpandField());
    db.add(new AsInitials());
    db.add(new HtmlDebug());
    db.add(new Nbsp());
    db.add(new UrlEncode());
    db.add(new InputValue());
    db.add(new RFC822DateFormatter());
    db.add(new Iso8601DateTime());
    db.add(new MonoRecordOperation<String, String>(TOSTRING, Casters.TOSTRING) {
      @Override
      public String calculate(TransientRecord record,
                              Viewpoint viewpoint,
                              String name)
      {
        Field<?> field = record.getTable().getField(name);
        if (field != null) {
          return field.toString(record);
        }
        return record.getString(name);
      }
    });
    db.add(NoHtml.getInstance());
    addDateFormatters(db, getNodeManager().getGettable());
    db.preLock();
  }
}
