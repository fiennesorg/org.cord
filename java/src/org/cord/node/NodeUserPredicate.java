package org.cord.node;

import org.cord.mirror.PersistentRecord;

import com.google.common.base.Optional;

/**
 * A NodeUserPredicate is an Object that is capable os stating whether a combination of a given Node
 * and User is permitted to perform an action.
 * 
 * @author alex
 */
public interface NodeUserPredicate
{
  /**
   * Constant NodeUserPredicate that always returns true.
   */
  public static final NodeUserPredicate TRUE = new NodeUserPredicate() {
    private final Optional<Boolean> TRUE = Optional.of(Boolean.TRUE);

    @Override
    public Optional<Boolean> apply(PersistentRecord node,
                                   PersistentRecord user)
    {
      return TRUE;
    }
  };

  /**
   * Constant NodeUserPredicate that always returns false.
   */
  public static final NodeUserPredicate FALSE = new NodeUserPredicate() {
    private final Optional<Boolean> FALSE = Optional.of(Boolean.FALSE);

    @Override
    public Optional<Boolean> apply(PersistentRecord node,
                                   PersistentRecord user)
    {
      return FALSE;
    }

  };

  /**
   * Constant NodeUserPredicate that always returns {@link Optional#absent()} - ie it doesn't have
   * an opinion.
   */
  public static final NodeUserPredicate ABSENT = new NodeUserPredicate() {
    private final Optional<Boolean> ABSENT = Optional.absent();

    @Override
    public Optional<Boolean> apply(PersistentRecord node,
                                   PersistentRecord user)
    {
      return ABSENT;
    }
  };

  /**
   * @return An Optional wrapper around true or false if this NodeUserPredicate has a definitive
   *         statement to make about the inputs, or {@link Optional#absent()} if the request
   *         concerns situations which this NodeUserPredicate has no opinion on.
   */
  public Optional<Boolean> apply(PersistentRecord node,
                                 PersistentRecord user);
}
