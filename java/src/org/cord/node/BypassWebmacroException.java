package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * NodeRequestException that represents a situation where the view handler has written to the
 * {@link NodeRequest#getResponse()} directly and therefore no webmacro templating needs to take
 * place. If you are going to throw this then you need to have written all of the data and headers
 * that you need before you throw it because the {@link NodeServlet} will not take any
 * responsibility for any of this.
 */
@SuppressWarnings("serial")
public class BypassWebmacroException
  extends NodeRequestException
{
  public BypassWebmacroException(String message,
                                 PersistentRecord triggerNode,
                                 Exception nestedException)
  {
    super(message,
          triggerNode,
          nestedException);
  }
}
