package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.CompoundUniqueFieldTrigger;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.node.NodeGroupStyle.CreateNodePolicy;
import org.cord.node.trigger.RegionStylePostBooter;
import org.cord.util.Casters;
import org.cord.util.DelegatingNamespaceGettable;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NamespaceGettable;
import org.cord.util.NamespaceSettable;
import org.cord.util.NamespacedNameCache;
import org.cord.util.NoSuchNamedException;
import org.cord.util.Settable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.objects.Object2IntMap;

public class RegionStyle
  extends NodeTableWrapper
{
  // ***********
  // REGIONSTYLE
  // ***********
  /**
   * "RegionStyle"
   */
  public final static String TABLENAME = "RegionStyle";

  /**
   * "nodeStyle_id"
   */
  public final static IntFieldKey NODESTYLE_ID = LinkField.getLinkFieldName(NodeStyle.TABLENAME);

  /**
   * @see NodeDbConstants#NUMBER
   */
  public final static IntFieldKey NUMBER = NodeDbConstants.NUMBER.copy();

  /**
   * "nodeStyle"
   */
  public final static RecordOperationKey<PersistentRecord> NODESTYLE =
      LinkField.getLinkName(NODESTYLE_ID);

  /**
   * @see NodeDbConstants#NAME
   */
  public final static ObjectFieldKey<String> NAME = NodeDbConstants.NAME.copy();

  /**
   * "aliasName"
   */
  public final static ObjectFieldKey<String> ALIASNAME = StringField.createKey("aliasName");

  /**
   * "title"
   */
  public final static ObjectFieldKey<String> TITLE = StringField.createKey("title");

  /**
   * "compilerName"
   */
  public final static ObjectFieldKey<String> COMPILERNAME = StringField.createKey("compilerName");

  public final static ObjectFieldKey<String> COMPILERSTYLENAME =
      NodeDbConstants.COMPILERSTYLENAME.copy();

  /**
   * $regionStyle.namespacedNameCache will return NamespacedNameCache set up to prepend Gettable
   * keys with "$regionStyle.name-" thereby making it ideal for generating ContentCompiler specific
   * NamespaceGettables. The NamespacedNameCache is retrieved from the NodeManager's
   * NamespacedNameCacheManager and is further cached on the RegionStyle.
   * 
   * @see #getNamespaceGettable(PersistentRecord, Gettable)
   * @see NamespacedNameCache
   * @see NodeManager#getNamespacedNameCacheManager()
   */
  public final static RecordOperationKey<NamespacedNameCache> NAMESPACEDNAMECACHE =
      RecordOperationKey.create("namespacedNameCache", NamespacedNameCache.class);

  public static final IntFieldKey EDITACL_ID = IntFieldKey.create("editAcl_id");

  public static final RecordOperationKey<PersistentRecord> EDITACL =
      LinkField.getLinkName(EDITACL_ID);

  public static final IntFieldKey UPDATEEDITACL_ID = IntFieldKey.create("updateEditAcl_id");

  /**
   * The name ("contentCompiler") of the RecordOperation that returns an instance of the
   * ContentCompiler as referenced by COMPILERNAME in this REGION.
   * 
   * @see ContentCompiler
   */
  public final static RecordOperationKey<ContentCompiler> CONTENTCOMPILER =
      RecordOperationKey.create("contentCompiler", ContentCompiler.class);

  public final static String NAMESPACE_SEPARATOR = "-";

  public final static MonoRecordOperationKey<Integer, Boolean> ISEDTABLEBY =
      MonoRecordOperationKey.create("isEditableBy", Integer.class, Boolean.class);

  protected RegionStyle(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(NODESTYLE_ID,
                                 "Containing NodeStyle",
                                 null,
                                 NODESTYLE,
                                 NodeStyle.TABLENAME,
                                 NodeStyle.REGIONSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new IntField(NUMBER, "Ordering number"));
    table.addField(new StringField(NAME, "Name", 32, ""));
    table.addFieldTrigger(new CompoundUniqueFieldTrigger<String>(table,
                                                                 NAME,
                                                                 false).addField(NODESTYLE_ID));
    table.addField(new StringField(ALIASNAME, "Controller name", 32, ""));
    table.addField(new StringField(TITLE, "Visible title"));
    table.addField(new StringField(COMPILERNAME, "ContentCompiler name"));
    table.addField(new StringField(COMPILERSTYLENAME, "ContentCompiler style name"));
    table.addField(new LinkField(EDITACL_ID,
                                 "Edit ACL",
                                 null,
                                 null,
                                 Acl.TABLENAME,
                                 Acl.EDITREGIONSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
    table.addField(new LinkField(UPDATEEDITACL_ID,
                                 "Update Edit ACL",
                                 null,
                                 null,
                                 Acl.TABLENAME,
                                 Acl.UPDATEEDITREGIONSTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
    // _regionStyle.add(new
    // ContentCompilerResolver(_nodeManager.getContentCompilerFactory()));
    table.addPostInstantiationTrigger(new RegionStylePostBooter(getNodeManager()));
    table.addFieldTrigger(new RegionStyleCompilerStyleTrigger());
    table.addRecordOperation(new SimpleRecordOperation<ContentCompiler>(CONTENTCOMPILER) {
      @Override
      public ContentCompiler getOperation(TransientRecord regionStyle,
                                          Viewpoint viewpoint)
      {
        return getContentCompiler(regionStyle);
      }
    });
    table.addFieldTrigger(ImmutableFieldTrigger.create(NODESTYLE_ID));
    table.addFieldTrigger(ImmutableFieldTrigger.create(COMPILERNAME));
    table.addRecordOperation(new MonoRecordOperation<Integer, Boolean>(ISEDTABLEBY, Casters.TOINT) {
      @Override
      public Boolean calculate(TransientRecord regionStyle,
                               Viewpoint viewpoint,
                               Integer userId)
      {
        return Boolean.valueOf(getNodeManager().getRegionStyle().isEditableBy(regionStyle,
                                                                              userId.intValue()));
      }
    });
    table.addRecordOperation(new DirectCachingSimpleRecordOperation<NamespacedNameCache>(NAMESPACEDNAMECACHE) {
      @Override
      public NamespacedNameCache calculate(TransientRecord regionStyle,
                                           Viewpoint viewpoint)
      {
        return getNodeManager().getNamespacedNameCacheManager()
                               .getPrefixNamespacedNameCache(regionStyle.opt(NAME)
                                                             + NAMESPACE_SEPARATOR);
      }
    });
    table.setTitleOperationName(NAME);
    table.setDefaultOrdering(TABLENAME + "." + NODESTYLE_ID + "," + TABLENAME + "." + NUMBER + ","
                             + TABLENAME + "." + ID);
  }

  public boolean isEditableBy(TransientRecord regionStyle,
                              int userId)
  {
    if (regionStyle.mayFollowLink(EDITACL_ID)) {
      return getNodeManager().getAcl().acceptsAclUser(regionStyle.compInt(EDITACL_ID), userId);
    }
    return true;
  }

  /**
   * FieldTrigger that sits on RegionStyle.COMPILERSTYLENAME which queries the appropriate
   * ContentCompiler for the RegionStyle as to whether or not a change to the ContentCompiler Style
   * is supported.
   * 
   * @see ContentCompiler#updateStyle(TransientRecord)
   */
  public class RegionStyleCompilerStyleTrigger
    extends AbstractFieldTrigger<String>
  {
    public RegionStyleCompilerStyleTrigger()
    {
      super(COMPILERSTYLENAME,
            false,
            false);
    }

    @Override
    public Object triggerField(TransientRecord regionStyle,
                               String fieldName,
                               Transaction transaction)
        throws MirrorFieldException
    {
      try {
        getNodeManager().getContentCompilerFactory()
                        .getContentCompiler(regionStyle.opt(COMPILERNAME))
                        .updateStyle(regionStyle);
      } catch (NoSuchNamedException badCompilerNameEx) {
        throw new MirrorFieldException("Cannot resolve ContentCompiler name",
                                       badCompilerNameEx,
                                       regionStyle,
                                       COMPILERNAME);
      }
      return null;
    }
  }

  /**
   * Resolve the given named RegionStyle on a NodeStyle.
   * 
   * @return RegionStyle record. Never null
   * @throws MirrorNoSuchRecordException
   *           if it is either not possible to resolve nodeStyle_id or if the given NodeStyle
   *           doesn't have a RegionStyle named name registered on it.
   */
  public PersistentRecord getInstance(int nodeStyle_id,
                                      String name,
                                      Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getOptInstance(nodeStyle_id, name, viewpoint);
    if (instance != null) {
      return instance;
    }
    throw new MirrorNoSuchRecordException(String.format("Unknown RegionStyle: %s.%s",
                                                        getNodeManager().getNodeStyle()
                                                                        .getTable()
                                                                        .getRecord(nodeStyle_id,
                                                                                   viewpoint),
                                                        name),
                                          getTable());
  }

  /**
   * Possibly resolve the given named RegionStyle on a NodeStyle.
   * 
   * @return RegionStyle record or null
   */
  public PersistentRecord getOptInstance(int nodeStyle_id,
                                         String name,
                                         Viewpoint viewpoint)
  {
    NodeStyle nodeStyles = getNodeManager().getNodeStyle();
    PersistentRecord nodeStyle = nodeStyles.getTable().getOptRecord(nodeStyle_id, viewpoint);
    if (nodeStyle == null) {
      return null;
    }
    Object2IntMap<String> regionStylesMap = nodeStyle.comp(NodeStyle.REGIONSTYLESMAP);
    int regionStyleId = regionStylesMap.getInt(name);
    if (regionStyleId == regionStylesMap.defaultReturnValue()) {
      return null;
    }
    return getTable().getOptRecord(regionStyleId, viewpoint);
  }

  /**
   * Get the RegionStyle that the given region style is aliased to via the aliasName field.
   * 
   * @return The appropriate RegionStyle or null if aliasName isn't defined
   * @throws LogicException
   *           if the named alias cannot be resolved under the current NodeStyle
   */
  public PersistentRecord getAlias(PersistentRecord regionStyle,
                                   Viewpoint viewpoint)
  {
    String aliasName = regionStyle.opt(ALIASNAME);
    if (Strings.isNullOrEmpty(aliasName)) {
      return null;
    }
    try {
      return getInstance(regionStyle.compInt(NODESTYLE_ID), aliasName, viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      throw new LogicException(regionStyle + " references an aliasName (" + aliasName
                               + ") which is not found under " + regionStyle.opt(NODESTYLE),
                               e);
    }
  }

  /**
   * Resolve the given RegionStyle record into the ContentCompiler named in the compilerName field.
   */
  public ContentCompiler getContentCompiler(TransientRecord regionStyle)
  {
    String compilerName = regionStyle.comp(COMPILERNAME);
    try {
      return getNodeManager().getContentCompilerFactory().getContentCompiler(compilerName);
    } catch (NoSuchNamedException e) {
      throw new NoSuchNamedException(compilerName,
                                     String.format("Unable to resolve ContentCompiler named \"%s\" for %s",
                                                   compilerName,
                                                   regionStyle));
    }
  }

  /**
   * Wrap the supplied Gettable in a namespace wrapper so that you can get items relating to the
   * given RegionStyle directly from the Gettable. If the RegionStyle supports aliasing to another
   * RegionStyle then this is automatically reflected with a DelegatingNamespaceGettable to the
   * entire chain of aliased RegionStyles.
   */
  public Gettable getNamespaceGettable(PersistentRecord regionStyle,
                                       Gettable gettable)
  {
    if (Strings.isNullOrEmpty(regionStyle.opt(ALIASNAME))) {
      return new NamespaceGettable(regionStyle.opt(NAMESPACEDNAMECACHE), gettable);
    }
    DelegatingNamespaceGettable dng =
        new DelegatingNamespaceGettable(getNodeManager().getNamespacedNameCacheManager(),
                                        gettable,
                                        false);
    do {
      dng.addNamespace(regionStyle.opt(NAMESPACEDNAMECACHE));
    } while ((regionStyle = getAlias(regionStyle, null)) != null);
    return dng;
  }

  public Settable getNamespaceSettable(PersistentRecord regionStyle,
                                       Settable settable)
  {
    return new NamespaceSettable(regionStyle.opt(NAMESPACEDNAMECACHE), settable);
  }

  /**
   * Register the given CreateNodePolicy against the NodeStyle that contains the given RegionStyle.
   * 
   * @param regionStyle
   *          Compulsory RegionStyle record
   * @param createNodePolicy
   *          Compulsory policy
   */
  public void addCreateNodePolicy(PersistentRecord regionStyle,
                                  CreateNodePolicy createNodePolicy)
  {
    TransientRecord.assertIsTable(regionStyle, getTable());
    Preconditions.checkNotNull(createNodePolicy, "createNodePolicy");
    PersistentRecord nodeStyle = regionStyle.comp(RegionStyle.NODESTYLE);
    getNodeManager().getNodeStyle().addCreateNodePolicy(nodeStyle, createNodePolicy);
  }
}
