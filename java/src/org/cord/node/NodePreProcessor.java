package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

/**
 * NodePreProcessors are invoked for each Node in a NodeRequest before the NodeRequestSegmentFactory
 * is asked to generate the NodeRequestSegment for the Node. They are bound to a single NodeStyle -
 * if you have an implementation that should be invoked on multiple NodeStyles then you should
 * create multiple instances. It is intended that you use this to do any work related to the fact
 * that a Node of a given NodeStyle has been part of a request regardless of what view was invoked
 * on the Node. If you have more than one preprocessor for a given NodeStyle then they are all
 * invoked in the order that they were registered.
 * 
 * @author alex
 */
public interface NodePreProcessor
{
  public int getNodeStyleId();

  public void preprocessNode(NodeRequest nodeRequest,
                             PersistentRecord renderNode,
                             PersistentRecord targetNode,
                             Context context,
                             boolean isSummaryRole,
                             int transactionType);
}
