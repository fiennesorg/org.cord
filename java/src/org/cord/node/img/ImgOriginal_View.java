package org.cord.node.img;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.webmacro.Context;

public class ImgOriginal_View
  extends ImgMgrImgView
{
  public static final String NAME = "ImgOriginal_View";

  public static final String TEMPLATEPATH = "ImgMgr/ImgOriginal_View.wm.html";

  public ImgOriginal_View(NodeManager nodeMgr,
                          Integer aclId,
                          String wrapperTemplatePath)
  {
    super(NAME,
          "View Image",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          TEMPLATEPATH);
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context,
                        PersistentRecord imgOriginal)
      throws NodeRequestException
  {
  }
}
