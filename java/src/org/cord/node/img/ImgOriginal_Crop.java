package org.cord.node.img;

import static com.google.common.base.Preconditions.checkNotNull;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.RedirectionException;
import org.cord.util.NumberUtil;
import org.cord.util.SettableException;
import org.cord.util.SettableMap;
import org.webmacro.Context;

public class ImgOriginal_Crop
  extends ImgMgrImgView
{
  public static final String NAME = "ImgOriginal_Crop";

  public static final String IN_HASRENDEREDFORM = "hasRenderedForm";
  public static final String IN_X = "x";
  public static final String IN_Y = "y";
  public static final String IN_WIDTH = "width";
  public static final String IN_HEIGHT = "height";

  public ImgOriginal_Crop(NodeManager nodeMgr,
                          Integer aclId,
                          String wrapperTemplatePath)
  {
    super(NAME,
          "Crop Image",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          "ImgMgr/ImgOriginal_Crop.wm.html");
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context,
                        PersistentRecord imgOriginal)
      throws NodeRequestException
  {
    if (Boolean.TRUE.equals(nodeRequest.getBoolean(IN_HASRENDEREDFORM))) {
      Object x = nodeRequest.get(IN_X);
      Object y = nodeRequest.get(IN_Y);
      Object width = nodeRequest.get(IN_WIDTH);
      Object height = nodeRequest.get(IN_HEIGHT);
      PersistentRecord croppedImgOriginal;
      try {
        croppedImgOriginal =
            getImgMgr().getImgOriginal()
                       .createCroppedInstance(imgOriginal,
                                              checkNotNull(NumberUtil.toInteger(x, null),
                                                           "invalid x: \"%s\"",
                                                           x).intValue(),
                                              checkNotNull(NumberUtil.toInteger(y, null),
                                                           "invalid y: \"%s\"",
                                                           y).intValue(),
                                              checkNotNull(NumberUtil.toInteger(width, null),
                                                           "invalid width: \"%s\"",
                                                           width).intValue(),
                                              checkNotNull(NumberUtil.toInteger(height, null),
                                                           "invalid height: \"%s\"",
                                                           height).intValue(),
                                              getImgMgr().getImgFormat()
                                                         .getDefaultConfiguration("PNG"));
      } catch (Exception e) {
        throw new FeedbackErrorException(getTitle(), node, e, "Unexpected Cropping Error");
      }
      SettableMap params = new SettableMap();
      try {
        imgSearch.addParams(params);
      } catch (SettableException e) {
        throw new FeedbackErrorException(getTitle(), node, e, "Unexpected Redirction Error");
      }
      params.put(NodeRequest.CGI_VAR_VIEW, ImgOriginal_View.NAME);
      params.put(ImgOriginal_View.IN_IMGORIGINAL_ID, Integer.valueOf(croppedImgOriginal.getId()));
      throw new RedirectionException(node, node, params, null);
    }
  }

}
