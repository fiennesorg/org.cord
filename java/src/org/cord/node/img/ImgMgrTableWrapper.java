package org.cord.node.img;

import org.cord.mirror.AbstractTableWrapper;

import com.google.common.base.Preconditions;

public abstract class ImgMgrTableWrapper
  extends AbstractTableWrapper
{
  private final ImgMgr __imgMgr;

  public ImgMgrTableWrapper(ImgMgr imgMgr,
                            String tableName)
  {
    super(tableName);
    __imgMgr = Preconditions.checkNotNull(imgMgr, "imgMgr");
  }

  public final ImgMgr getImgMgr()
  {
    return __imgMgr;
  }

}
