package org.cord.node.img;

import java.io.File;

import javax.servlet.http.HttpSession;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegmentManager;
import org.cord.node.json.Handler_ImgOriginal;
import org.cord.util.Assertions;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;
import org.cord.util.NumberUtil;
import org.cord.util.StringUtils;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public final class ImgMgr
{
  public static final String CONF_IMGMGR_ACL_EDITOR = "ImgMgr.acl.editor";
  public static final String CONF_IMGMGR_ACL_REPLACER = "ImgMgr.acl.replacer";

  public static final String CONF_IMGMGR_WRAPPERTEMPLATEPATH = "ImgMgr.wrapperTemplatePath";

  public static ImgMgr getInstance(final NodeManager nodeMgr,
                                   String pwd)
      throws MirrorTableLockedException
  {
    Db db = nodeMgr.getDb();
    Gettable params = nodeMgr.getGettable();
    String originalWebDir = params.getString("ImgMgr.originalWebDir");
    String scaledWebDir = params.getString("ImgMgr.scaledWebDir");
    Integer defaultImgFormat_id = NumberUtil.toInteger(params.get("ImgMgr.defaultImgFormat_id"));
    nodeMgr.getJMgr().register(new Handler_ImgOriginal(nodeMgr));
    ImgMgr imgMgr = new ImgMgr(db,
                               nodeMgr.getDocBase(),
                               originalWebDir,
                               scaledWebDir,
                               defaultImgFormat_id,
                               new Supplier<String>() {
                                 @Override
                                 public String get()
                                 {
                                   return nodeMgr.getHttpServletPath();
                                 }
                               });
    NodeRequestSegmentManager nrsm = nodeMgr.getNodeRequestSegmentManager();
    String wrapperTemplatePath =
        MoreObjects.firstNonNull(params.getString(CONF_IMGMGR_WRAPPERTEMPLATEPATH),
                                 "ImgMgr/ImgMgrView.wrapper.wm.html");
    Object editorObj = params.get(CONF_IMGMGR_ACL_EDITOR);
    if (editorObj != null) {
      Integer aclId = NumberUtil.toInteger(editorObj);
      nrsm.register(new ImgOriginal_View(nodeMgr, aclId, wrapperTemplatePath));
      nrsm.register(new ImgOriginal_Annotate(nodeMgr, pwd, aclId, wrapperTemplatePath));
      nrsm.register(new ImgOriginal_Create(nodeMgr, aclId, wrapperTemplatePath));
      nrsm.register(new ImgOriginal_Search(nodeMgr, aclId, wrapperTemplatePath));
      nrsm.register(new ImgOriginal_Crop(nodeMgr, aclId, wrapperTemplatePath));
    }
    Object replacerObj = params.get(CONF_IMGMGR_ACL_REPLACER);
    if (replacerObj != null) {
      Integer aclId = NumberUtil.toInteger(replacerObj);
      nrsm.register(new ImgOriginal_Delete(nodeMgr, pwd, aclId, wrapperTemplatePath));
    }
    return imgMgr;
  }

  public ImgSearch createImgSearch(String nodeUrl,
                                   String successUrl,
                                   String title,
                                   String description,
                                   String filename,
                                   String owner,
                                   Optional<Double> minAspectRatio,
                                   Optional<Double> fixedAspectRatio,
                                   Optional<Double> maxAspectRatio)
  {
    return new ImgSearch(this,
                         nodeUrl,
                         successUrl,
                         title,
                         description,
                         filename,
                         owner,
                         minAspectRatio,
                         fixedAspectRatio,
                         maxAspectRatio);
  }

  public ImgSearch createImgSearch(String nodeUrl,
                                   String successUrl,
                                   String owner)
  {
    return createImgSearch(nodeUrl,
                           successUrl,
                           null,
                           null,
                           null,
                           owner,
                           Optional.<Double> absent(),
                           Optional.<Double> absent(),
                           Optional.<Double> absent());
  }

  /**
   * Create an ImgSearch which is invoked on {@link NodeRequest#getTargetLeafNode()} and which has
   * an Owner resolved via {@link #getImgOriginalOwner(NodeRequest)}
   * 
   * @see #createImgSearch(String, String, String)
   */
  public ImgSearch createImgSearch(String successUrl,
                                   NodeRequest nodeRequest)
  {
    return createImgSearch(nodeRequest.getTargetLeafNode().comp(Node.URL),
                           successUrl,
                           getImgOriginalOwner(nodeRequest));
  }

  private final ImgOriginal __imgOriginal;
  private final ImgScaled __imgScaled;
  private final ImgFormat __imgFormat;

  // /Users/Alex/cvsroot/acme/www
  private final File __docBase;
  private final Supplier<String> __httpServletPath;

  /**
   * The session key that is utilised to store the value of {@link ImgOriginal#OWNER} when creating
   * new {@link ImgOriginal} records as part of {@link ImgOriginal_Create}. If not defined then the
   * value will be "".
   */
  public static final String SESSION_IMGORIGINAL_OWNER = "ImgOriginal.owner";

  /**
   * Boot the Img system and register it on the given db.
   */
  public ImgMgr(final Db db,
                final File docBase,
                final String originalWebDir,
                final String scaledWebDir,
                final Integer defaultImgFormat_id,
                final Supplier<String> httpServletPath)
      throws MirrorTableLockedException
  {
    Assertions.isTrue(docBase != null && docBase.isDirectory(),
                      "%s is not a valid docBase",
                      docBase);
    __docBase = docBase;
    __httpServletPath = Preconditions.checkNotNull(httpServletPath, "httpServletPath");
    __imgFormat = new ImgFormat(this);
    db.add(__imgFormat);
    __imgOriginal = new ImgOriginal(this, originalWebDir);
    db.add(__imgOriginal);
    __imgScaled = new ImgScaled(this, scaledWebDir, defaultImgFormat_id);
    db.add(__imgScaled);
  }

  /**
   * Look up the ImgOriginal Owner value from the NodeRequest's session which if defined will be
   * stored under {@link #SESSION_IMGORIGINAL_OWNER}.
   * 
   * @return The saved Owner id or an empty String if there is no value defined.
   */
  public String getImgOriginalOwner(NodeRequest nodeRequest)
  {
    return getImgOriginalOwner(nodeRequest.getSession());
  }

  /**
   * Look up the ImgOriginal Owner value from the session which if defined will be stored under
   * {@link #SESSION_IMGORIGINAL_OWNER}.
   * 
   * @return The saved Owner id or an empty String if there is no value defined.
   */
  public String getImgOriginalOwner(HttpSession session)
  {
    return StringUtils.padEmpty(StringUtils.toString(session.getAttribute(SESSION_IMGORIGINAL_OWNER)),
                                "");
  }

  public String getHttpServletPath()
  {
    return __httpServletPath.get();
  }

  protected final File getDocBase()
  {
    return __docBase;
  }

  /**
   * Convert a file into a webpath string relative to the docBase.
   * 
   * @param file
   *          The compulsory file that must be a child of docBase
   * @return the appropriate webPath string
   */
  protected final String fileToWebPath(File file)
  {
    return IoUtil.getRelativePath(getDocBase(), file);
  }

  /**
   * Convert a webPath expression into a File relative to the docBase.
   */
  protected final File webPathToFile(String webPath)
  {
    return new File(getDocBase(), webPath);
  }

  public final ImgOriginal getImgOriginal()
  {
    return __imgOriginal;
  }

  public final ImgScaled getImgScaled()
  {
    return __imgScaled;
  }

  public final ImgFormat getImgFormat()
  {
    return __imgFormat;
  }
}
