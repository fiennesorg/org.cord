package org.cord.node.img;

import org.cord.mirror.Db;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.json.JSONObject;
import org.webmacro.Context;

public class ImgOriginal_Annotate
  extends ImgMgrImgView
{
  public static final String NAME = "ImgOriginal_Annotate";

  public static final String IN_IMGORIGINAL_ID = "imgOriginal_id";
  public static final String IN_TITLE = ImgOriginal.TITLE.getKeyName();
  public static final String IN_DESCRIPTION = ImgOriginal.DESCRIPTION.getKeyName();

  public static final String IN_ALT = ImgOriginal.DATA__ALT;
  public static final String IN_COPYRIGHT = ImgOriginal.DATA__COPYRIGHT;

  private final String __pwd;

  public ImgOriginal_Annotate(NodeManager nodeMgr,
                              String pwd,
                              Integer aclId,
                              String wrapperTemplatePath)
  {
    super(NAME,
          "Annotate Image",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          "ImgMgr/ImgOriginal_View.wm.html");
    __pwd = pwd;
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context,
                        PersistentRecord imgOriginal)
      throws FeedbackErrorException
  {
    try (Transaction t = getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                    Transaction.TRANSACTION_UPDATE,
                                                                    __pwd,
                                                                    NAME)) {
      WritableRecord r = t.edit(imgOriginal);
      r.setField(ImgOriginal.TITLE, nodeRequest.get(IN_TITLE));
      r.setField(ImgOriginal.DESCRIPTION, nodeRequest.get(IN_DESCRIPTION));
      JSONObject data = r.comp(ImgOriginal.DATA);
      data.put(ImgOriginal.DATA__ALT, nodeRequest.getString(IN_ALT));
      data.put(ImgOriginal.DATA__COPYRIGHT, nodeRequest.getString(IN_COPYRIGHT));
      // r.setField(ImgOriginal.DATASRC, data.toString());
      t.commit();
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(), node, e, "DB error annotating %s", imgOriginal);
    }
  }
}
