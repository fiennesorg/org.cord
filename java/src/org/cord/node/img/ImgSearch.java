package org.cord.node.img;

import java.io.IOException;

import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.TransientRecord;
import org.cord.node.AbstractPaginator;
import org.cord.node.Node;
import org.cord.node.NodeRequest;
import org.cord.sql.SqlUtil;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Strings;

/**
 * ImgSearch is a lightweight object that represents the current factors being filtered for, and
 * what to do with an ImgOriginal if one is selected.
 * 
 * @author alex
 */
public class ImgSearch
{
  public static final String SUCCESSURL = "successUrl";

  public static final String SEARCH_TITLE = ImgOriginal_Search.IN_SEARCH_TITLE;
  public static final String SEARCH_DESCRIPTION = ImgOriginal_Search.IN_SEARCH_DESCRIPTION;
  public static final String SEARCH_FILENAME = ImgOriginal_Search.IN_SEARCH_FILENAME;
  public static final String SEARCH_OWNER = ImgOriginal_Search.IN_SEARCH_OWNER;
  public static final String SEARCH_MINASPECTRATIO = ImgOriginal_Search.IN_SEARCH_MINASPECTRATIO;
  public static final String SEARCH_FIXEDASPECTRATIO =
      ImgOriginal_Search.IN_SEARCH_FIXEDASPECTRATIO;
  public static final String SEARCH_MAXASPECTRATIO = ImgOriginal_Search.IN_SEARCH_MAXASPECTRATIO;

  private final ImgMgr __imgMgr;
  public final String title;
  public final String description;
  public final String filename;
  public final String owner;

  private final Optional<Double> __minAspectRatio;
  private final Optional<Double> __fixedAspectRatio;
  private final Optional<Double> __maxAspectRatio;

  private final String __successUrl;

  // private final String __nodeUrl;
  private final String __queryBase;
  private final String __viewBase;

  /**
   * @param nodeUrl
   *          The URL of the Node which this ImgSearch is being invoked on
   * @param successUrl
   *          The URL which if postfixed with an ImgOriginal.id will answer whatever query is being
   *          asked.
   * @param title
   *          optional search terms
   * @param description
   *          optional search terms
   * @param filename
   *          optional search terms
   */
  public ImgSearch(ImgMgr imgMgr,
                   String nodeUrl,
                   String successUrl,
                   String title,
                   String description,
                   String filename,
                   String owner,
                   Optional<Double> minAspectRatio,
                   Optional<Double> fixedAspectRatio,
                   Optional<Double> maxAspectRatio)
  {
    __imgMgr = imgMgr;
    // __nodeUrl = Preconditions.checkNotNull(nodeUrl, "nodeUrl");
    __successUrl = successUrl;
    this.title = Strings.nullToEmpty(title).trim();
    this.description = Strings.nullToEmpty(description).trim();
    this.filename = Strings.nullToEmpty(filename).trim();
    this.owner = Strings.nullToEmpty(owner).trim();
    __minAspectRatio = minAspectRatio;
    __fixedAspectRatio = fixedAspectRatio;
    __maxAspectRatio = maxAspectRatio;
    StringBuilder buf = new StringBuilder();
    buf.append(nodeUrl).append("?");
    appendParam(buf, SEARCH_TITLE, this.title);
    appendParam(buf, SEARCH_DESCRIPTION, this.description);
    appendParam(buf, SEARCH_FILENAME, this.filename);
    appendParam(buf, SEARCH_OWNER, this.owner);
    appendParam(buf, "successUrl", __successUrl);
    if (minAspectRatio.isPresent()) {
      appendParam(buf, SEARCH_MINASPECTRATIO, minAspectRatio.get().toString());
    }
    if (fixedAspectRatio.isPresent()) {
      appendParam(buf, SEARCH_FIXEDASPECTRATIO, fixedAspectRatio.get().toString());
    }
    if (maxAspectRatio.isPresent()) {
      appendParam(buf, SEARCH_MAXASPECTRATIO, maxAspectRatio.get().toString());
    }
    __queryBase = buf.toString();
    buf.append("&view=" + ImgOriginal_View.NAME + "&" + ImgOriginal_View.IN_IMGORIGINAL_ID + "=");
    __viewBase = buf.toString();
  }

  public ImgMgr getImgMgr()
  {
    return __imgMgr;
  }

  private void appendSearchTerms(StringBuilder buf,
                                 FieldKey<String> fieldKey,
                                 String searchTerms)
  {
    if (searchTerms.length() > 0) {
      if (buf.length() > 0) {
        buf.append(" and ");
      }
      buf.append(fieldKey.getKeyName()).append(" like '%");
      SqlUtil.escapeQuotes(buf, searchTerms);
      buf.append("%'");
    }
  }

  public RecordSource getMatchingImgOriginals()
  {
    if (title.length() == 0 & description.length() == 0 & filename.length() == 0
        & owner.length() == 0) {
      return getImgMgr().getImgOriginal().getTable().getAllRecordsQuery();
    }
    StringBuilder buf = new StringBuilder();
    appendSearchTerms(buf, ImgOriginal.TITLE, title);
    appendSearchTerms(buf, ImgOriginal.DESCRIPTION, description);
    appendSearchTerms(buf, ImgOriginal.FILENAME, filename);
    if (owner.length() > 0) {
      if (buf.length() > 0) {
        buf.append(" and ");
      }
      buf.append(ImgOriginal.OWNER.getKeyName()).append(" like '");
      SqlUtil.escapeQuotes(buf, owner);
      buf.append("%'");
    }
    String filter = buf.toString();
    return getImgMgr().getImgOriginal().getTable().getQuery(filter, filter, null);
  }

  public Optional<Double> getMinAspectRatio()
  {
    return __minAspectRatio;
  }

  public Optional<Double> getMaxAspectRatio()
  {
    return __maxAspectRatio;
  }

  public Optional<Double> getFixedAspectRatio()
  {
    return __fixedAspectRatio;
  }

  public boolean isValidAspectRatio(double width,
                                    double height)
  {
    return isValidAspectRatio(width / height);
  }

  public static final double ASPECT_EQUALITY_ERROR = 0.005d;

  public boolean isValidAspectRatio(double aspectRatio)
  {
    Optional<Double> fixedAspectRatio = getFixedAspectRatio();
    if (fixedAspectRatio.isPresent()) {
      // System.err.println(aspectRatio
      // + "::"
      // + fixedAspectRatio.get().doubleValue()
      // + "::"
      // + (Math.abs(fixedAspectRatio.get().doubleValue() - aspectRatio))
      // + "-->"
      // + (Math.abs(fixedAspectRatio.get().doubleValue() - aspectRatio) < ASPECT_EQUALITY_ERROR));
      return Math.abs(fixedAspectRatio.get().doubleValue() - aspectRatio) < ASPECT_EQUALITY_ERROR;
    }
    Optional<Double> minAspectRatio = getMinAspectRatio();
    if (minAspectRatio.isPresent() && aspectRatio < minAspectRatio.get().doubleValue()) {
      return false;
    }
    Optional<Double> maxAspectRatio = getMaxAspectRatio();
    if (maxAspectRatio.isPresent() && aspectRatio > maxAspectRatio.get().doubleValue()) {
      return false;
    }
    return true;
  }
  public boolean isValidAspectRatio(TransientRecord imgOriginal)
  {
    return isValidAspectRatio(imgOriginal.comp(ImgOriginal.ASPECT).doubleValue());
  }

  public void addParams(Settable settable)
      throws SettableException
  {
    settable.set(SUCCESSURL, __successUrl);
    settable.set(SEARCH_TITLE, title);
    settable.set(SEARCH_DESCRIPTION, description);
    settable.set(SEARCH_FILENAME, filename);
    settable.set(SEARCH_OWNER, owner);
    if (__minAspectRatio.isPresent()) {
      settable.set(SEARCH_MINASPECTRATIO, __minAspectRatio.get());
    }
    if (__fixedAspectRatio.isPresent()) {
      settable.set(SEARCH_FIXEDASPECTRATIO, __fixedAspectRatio.get());
    }
    if (__maxAspectRatio.isPresent()) {
      settable.set(SEARCH_MAXASPECTRATIO, __maxAspectRatio.get());
    }
  }

  public void addParams(AbstractPaginator paginator)
  {
    if (this.title.length() > 0) {
      paginator.addAdditionalField(SEARCH_TITLE, this.title);
    }
    if (this.description.length() > 0) {
      paginator.addAdditionalField(SEARCH_DESCRIPTION, this.description);
    }
    if (this.filename.length() > 0) {
      paginator.addAdditionalField(SEARCH_FILENAME, this.filename);
    }
    if (this.owner.length() > 0) {
      paginator.addAdditionalField(SEARCH_OWNER, this.owner);
    }
    if (__successUrl != null) {
      paginator.addAdditionalField(SUCCESSURL, __successUrl);
    }
    if (__minAspectRatio.isPresent()) {
      paginator.addAdditionalField(SEARCH_MINASPECTRATIO, __minAspectRatio.get());
    }
    if (__fixedAspectRatio.isPresent()) {
      paginator.addAdditionalField(SEARCH_FIXEDASPECTRATIO, __fixedAspectRatio.get());
    }
    if (__maxAspectRatio.isPresent()) {
      paginator.addAdditionalField(SEARCH_MAXASPECTRATIO, __maxAspectRatio.get());
    }
  }

  public void addSearchParams(AbstractPaginator paginator)
  {
    addParams(paginator);
    paginator.addAdditionalField(NodeRequest.CGI_VAR_VIEW, ImgOriginal_Search.NAME);
  }

  private void appendSharedHiddenInputs(StringBuilder buf)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(buf, __successUrl, SUCCESSURL);    
    HtmlUtilsTheme.hiddenInput(buf, owner, SEARCH_OWNER);
    if (__minAspectRatio.isPresent()) {
      HtmlUtilsTheme.hiddenInput(buf, __minAspectRatio.get(), SEARCH_MINASPECTRATIO);
    }
    if (__fixedAspectRatio.isPresent()) {
      HtmlUtilsTheme.hiddenInput(buf, __fixedAspectRatio.get(), SEARCH_FIXEDASPECTRATIO);
    }
    if (__maxAspectRatio.isPresent()) {
      HtmlUtilsTheme.hiddenInput(buf, __maxAspectRatio.get(), SEARCH_MAXASPECTRATIO);
    }
  }
  
  public String getHiddenInputsSearch()
  {
    StringBuilder buf = new StringBuilder();
    try {
      appendSharedHiddenInputs(buf);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  public String getHiddenInputs()
  {
    StringBuilder buf = new StringBuilder();
    try {
      appendSharedHiddenInputs(buf);
      HtmlUtilsTheme.hiddenInput(buf, title, SEARCH_TITLE);
      HtmlUtilsTheme.hiddenInput(buf, description, SEARCH_DESCRIPTION);
      HtmlUtilsTheme.hiddenInput(buf, filename, SEARCH_FILENAME);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  public ImgSearch(ImgMgr imgMgr,
                   NodeRequest nodeRequest)
  {
    this(imgMgr,
         nodeRequest.getTargetLeafNode().comp(Node.URL),
         nodeRequest.getString(SUCCESSURL),
         nodeRequest.getString(SEARCH_TITLE),
         nodeRequest.getString(SEARCH_DESCRIPTION),
         nodeRequest.getString(SEARCH_FILENAME),
         nodeRequest.getString(SEARCH_OWNER),
         Optional.fromNullable(NumberUtil.toDouble(nodeRequest.get(SEARCH_MINASPECTRATIO), null)),
         Optional.fromNullable(NumberUtil.toDouble(nodeRequest.get(SEARCH_FIXEDASPECTRATIO), null)),
         Optional.fromNullable(NumberUtil.toDouble(nodeRequest.get(SEARCH_MAXASPECTRATIO), null)));
  }

  private void appendParam(StringBuilder buf,
                           String name,
                           String value)
  {
    if (value != null && value.length() != 0) {
      if (buf.charAt(buf.length() - 1) != '?') {
        buf.append('&');
      }
      buf.append(name).append('=').append(StringUtils.urlEncodeUtf8(value));
    }
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add(SUCCESSURL, __successUrl)
                      .add(SEARCH_TITLE, title)
                      .add(SEARCH_DESCRIPTION, description)
                      .add(SEARCH_FILENAME, filename)
                      .add(SEARCH_OWNER, owner)
                      .add(SEARCH_MINASPECTRATIO, __minAspectRatio)
                      .add(SEARCH_FIXEDASPECTRATIO, __fixedAspectRatio)
                      .add(SEARCH_MAXASPECTRATIO, __maxAspectRatio)
                      .toString();
  }

  public boolean hasSuccessUrl()
  {
    return __successUrl != null;
  }

  public String getSuccessUrl(int imgOriginalId)
  {
    return __successUrl + imgOriginalId;
  }

  public String getSuccessUrl(PersistentRecord imgOriginal)
  {
    return getSuccessUrl(imgOriginal.getId());
  }

  public String getViewUrl(int imgOriginalId)
  {
    return __viewBase + imgOriginalId;
  }

  public String getViewUrl(PersistentRecord imgOriginal)
  {
    return getViewUrl(imgOriginal.getId());
  }

  public String getUrl(String view)
  {
    return getQueryBase() + "&view=" + StringUtils.urlEncodeUtf8(view);
  }

  public String getQueryBase()
  {
    return __queryBase;
  }
}
