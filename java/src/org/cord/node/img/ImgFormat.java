package org.cord.node.img;

import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.sql.SqlUtil;

import com.google.common.base.Strings;

public class ImgFormat
  extends ImgMgrTableWrapper
{
  public static final String TABLENAME = "ImgFormat";

  public static final ObjectFieldKey<String> NAME = StringField.createKey("name");
  public static final ObjectFieldKey<String> ENGLISHNAME = StringField.createKey("englishName");
  public static final ObjectFieldKey<String> IMPARAMETER = StringField.createKey("imParameter");
  public static final ObjectFieldKey<String> IMFORMAT = StringField.createKey("imFormat");
  public static final ObjectFieldKey<String> EXTENSION = StringField.createKey("extension");
  public static final ObjectFieldKey<Boolean> ISDEFAULTCONFIG =
      BooleanField.createKey("isDefaultConfig");
  public static final ObjectFieldKey<Boolean> ISDEFINEDFORMAT =
      BooleanField.createKey("isDefinedFormat");

  public final static int ID_PNG = 1;
  public final static int ID_GIF_DITH = 2;
  public final static int ID_GIF_NODITH = 3;
  public final static int ID_JPEG_LOW = 4;
  public final static int ID_JPEG_MED = 5;
  public final static int ID_JPEG_HIGH = 6;
  public final static int ID_UNKNOWN = 7;

  public ImgFormat(ImgMgr imgMgr)
  {
    super(imgMgr,
          TABLENAME);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NAME, "Name"));
    table.addField(new StringField(ENGLISHNAME, "English Name"));
    table.setTitleOperationName(ENGLISHNAME);
    table.addField(new StringField(IMPARAMETER, "ImageMagick parameters"));
    table.addField(new StringField(IMFORMAT, "ImageMagick format"));
    table.addField(new StringField(EXTENSION, "Filename extension (including .)"));
    table.addField(new BooleanField(ISDEFAULTCONFIG, "Is default config for format?"));
    table.addField(new BooleanField(ISDEFINEDFORMAT, "Is defined format?"));
  }

  private Map<String, Integer> __defaultFormats = new HashMap<String, Integer>();

  /**
   * Get the default configuration for the given ImageMagick format. If the format is unknown or
   * there is not a default format then the ID_UNKNOWN instance will be returned.
   * 
   * @see #ID_UNKNOWN
   */
  public PersistentRecord getDefaultConfiguration(String format)
  {
    if (Strings.isNullOrEmpty(format)) {
      return getTable().getCompRecord(ID_UNKNOWN);
    }
    Integer id = __defaultFormats.get(format);
    if (id != null) {
      return getTable().getCompRecord(id);
    }
    StringBuilder buf = new StringBuilder();
    buf.append("ImgFormat.imFormat=");
    SqlUtil.toSafeSqlString(buf, format);
    buf.append(" and ImgFormat.isDefaultConfig=1");
    String filter = buf.toString();
    PersistentRecord instance =
        RecordSources.getOptOnlyRecord(getTable().getQuery(filter, filter, null), null);
    if (instance != null) {
      __defaultFormats.put(format, Integer.valueOf(instance.getId()));
      return instance;
    }
    __defaultFormats.put(format, Integer.valueOf(ID_UNKNOWN));
    return getTable().getCompRecord(ID_UNKNOWN);
  }

  public PersistentRecord getInstance(Object o)
  {
    PersistentRecord imFormat = null;
    if (o instanceof PersistentRecord) {
      imFormat = (PersistentRecord) o;
      PersistentRecord.assertIsTableNamed(imFormat, ImgFormat.TABLENAME);
      return imFormat;
    }
    if (o instanceof Number) {
      return getTable().getOptRecord(o);
    }
    StringBuilder buf = new StringBuilder();
    buf.append("ImgFormat.name=");
    SqlUtil.toSafeSqlString(buf, o);
    String filter = buf.toString();
    return RecordSources.getOptOnlyRecord(getTable().getQuery(filter, filter, null), null);
  }
}
