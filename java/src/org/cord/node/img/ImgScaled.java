package org.cord.node.img;

import java.io.File;
import java.io.IOException;

import org.cord.image.ImageMetaData;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.RecordSources;

import com.google.common.base.Preconditions;

public class ImgScaled
  extends ImgTableWrapper
{
  public static final String TABLENAME = "ImgScaled";

  public static final IntFieldKey IMGORIGINAL_ID =
      LinkField.getLinkFieldName(ImgOriginal.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> IMGORIGINAL =
      LinkField.getLinkName(IMGORIGINAL_ID);
  public static final RecordOperationKey<RecordSource> IMGORIGINAL__IMGSCALEDS =
      LinkField.getReferencesKey(ImgScaled.TABLENAME);

  private final Integer __defaultImgFormat_id;

  public ImgScaled(ImgMgr imgMgr,
                   String webPathDir,
                   Integer defaultImgFormat_id)
  {
    super(imgMgr,
          TABLENAME,
          webPathDir);
    __defaultImgFormat_id = Preconditions.checkNotNull(defaultImgFormat_id, "defaultImgFormat_id");
  }

  public final Integer getDefaultImgFormat_id()
  {
    return __defaultImgFormat_id;
  }

  public final PersistentRecord getDefaultImgFormat()
  {
    return getImgMgr().getImgFormat().getTable().getCompRecord(getDefaultImgFormat_id());
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    super.initTable(db, pwd, table);
    table.addField(new LinkField(ImgOriginal.TABLENAME,
                                 "Source image",
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
  }

  public PersistentRecord getExistingInstanceByWidth(int imageOriginal_id,
                                                     int width,
                                                     int imgFormat_id)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(TABLENAME + "." + IMGORIGINAL_ID + "=")
       .append(imageOriginal_id)
       .append(" and " + TABLENAME + "." + WIDTH + "=")
       .append(width)
       .append(" and " + TABLENAME + "." + IMGFORMAT_ID + "=")
       .append(imgFormat_id);
    String filter = buf.toString();
    return RecordSources.getOptFirstRecord(getTable().getQuery(filter, filter, Query.NOSQLORDERING),
                                           null);
  }

  public PersistentRecord getExistingInstanceByWidthHeight(int imgOriginal_id,
                                                           int width,
                                                           int height,
                                                           int imgFormat_id)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(TABLENAME + "." + IMGORIGINAL_ID + "=")
       .append(imgOriginal_id)
       .append(" and " + TABLENAME + "." + WIDTH + "=")
       .append(width)
       .append(" and " + TABLENAME + "." + HEIGHT + "=")
       .append(height)
       .append(" and " + TABLENAME + "." + IMGFORMAT_ID + "=")
       .append(imgFormat_id);
    String filter = buf.toString();
    return RecordSources.getOptFirstRecord(getTable().getQuery(filter, filter, Query.NOSQLORDERING),
                                           null);
  }

  public PersistentRecord getExistingInstanceByHeight(int imageOriginal_id,
                                                      int height,
                                                      int imgFormat_id)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(TABLENAME + "." + IMGORIGINAL_ID + "=")
       .append(imageOriginal_id)
       .append(" and " + TABLENAME + "." + HEIGHT + "=")
       .append(height)
       .append(" and " + TABLENAME + "." + IMGFORMAT_ID + "=")
       .append(imgFormat_id);
    String filter = buf.toString();
    return RecordSources.getOptFirstRecord(getTable().getQuery(filter, filter, Query.NOSQLORDERING),
                                           null);
  }

  private PersistentRecord validateImgFormat(PersistentRecord imgFormat)
  {
    if (imgFormat == null) {
      return getDefaultImgFormat();
    }
    imgFormat.assertIsTableNamed(ImgFormat.TABLENAME);
    return imgFormat.is(ImgFormat.ISDEFINEDFORMAT) ? imgFormat : getDefaultImgFormat();
  }

  private File initScaledFile(String extension)
      throws IOException
  {
    return initFile(__uploadDir, extension);
  }

  protected static File initFile(File dir,
                                 String extension)
      throws IOException
  {
    long t = System.currentTimeMillis();
    while (true) {
      File scaledFile = new File(dir, t + extension);
      if (scaledFile.createNewFile()) {
        return scaledFile;
      }
      t++;
    }
  }

  public PersistentRecord getOrCreateInstanceByWidth(PersistentRecord imgOriginal,
                                                     int width,
                                                     PersistentRecord imgFormat)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    TransientRecord.assertIsTableNamed(imgOriginal, ImgOriginal.TABLENAME);
    imgFormat = validateImgFormat(imgFormat);
    PersistentRecord existingInstance =
        getExistingInstanceByWidth(imgOriginal.getId(), width, imgFormat.getId());
    if (existingInstance != null) {
      return existingInstance;
    }
    TransientRecord instance = getTable().createTransientRecord();
    instance.setField(IMGORIGINAL_ID, imgOriginal);
    instance.setField(IMGFORMAT_ID, imgFormat);
    ImageMetaData originalIMD = getImgMgr().getImgOriginal().getImageMetaData(imgOriginal);
    File scaledFile = initScaledFile(imgFormat.opt(ImgFormat.EXTENSION));
    ImageMetaData scaledIMD = originalIMD.convertByWidth(scaledFile,
                                                         width,
                                                         imgFormat.opt(ImgFormat.IMPARAMETER),
                                                         imgFormat.opt(ImgFormat.IMFORMAT));
    String webPath = getImgMgr().fileToWebPath(scaledFile);
    initMetaData(instance, scaledIMD, webPath, imgFormat);
    existingInstance = getExistingInstanceByWidth(imgOriginal.getId(), width, imgFormat.getId());
    if (existingInstance != null) {
      scaledFile.delete();
      return existingInstance;
    }
    return instance.makePersistent(null);
  }

  public PersistentRecord getOrCreateInstanceByWidthHeight(final PersistentRecord imgOriginal,
                                                           final int width,
                                                           final int height,
                                                           PersistentRecord imgFormat)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    TransientRecord.assertIsTableNamed(imgOriginal, ImgOriginal.TABLENAME);
    imgFormat = validateImgFormat(imgFormat);
    PersistentRecord existingInstance =
        getExistingInstanceByWidthHeight(imgOriginal.getId(), width, height, imgFormat.getId());
    if (existingInstance != null) {
      return existingInstance;
    }
    TransientRecord instance = getTable().createTransientRecord();
    instance.setField(IMGORIGINAL_ID, imgOriginal);
    instance.setField(IMGFORMAT_ID, imgFormat);
    ImageMetaData originalIMD = getImgMgr().getImgOriginal().getImageMetaData(imgOriginal);
    File scaledFile = initScaledFile(imgFormat.opt(ImgFormat.EXTENSION));
    ImageMetaData scaledIMD = originalIMD.convert(scaledFile,
                                                  width,
                                                  height,
                                                  imgFormat.opt(ImgFormat.IMPARAMETER),
                                                  imgFormat.opt(ImgFormat.IMFORMAT));
    String webPath = getImgMgr().fileToWebPath(scaledFile);
    initMetaData(instance, scaledIMD, webPath, imgFormat);
    existingInstance =
        getExistingInstanceByWidthHeight(imgOriginal.getId(), width, height, imgFormat.getId());
    if (existingInstance != null) {
      scaledFile.delete();
      return existingInstance;
    }
    return instance.makePersistent(null);
  }

  public PersistentRecord getOrCreateInstanceByHeight(PersistentRecord imgOriginal,
                                                      int height,
                                                      PersistentRecord imgFormat)
      throws MirrorFieldException, MirrorInstantiationException, IOException
  {
    TransientRecord.assertIsTableNamed(imgOriginal, ImgOriginal.TABLENAME);
    imgFormat = validateImgFormat(imgFormat);
    PersistentRecord existingInstance =
        getExistingInstanceByHeight(imgOriginal.getId(), height, imgFormat.getId());
    if (existingInstance != null) {
      return existingInstance;
    }
    TransientRecord instance = getTable().createTransientRecord();
    instance.setField(IMGORIGINAL_ID, imgOriginal);
    instance.setField(IMGFORMAT_ID, imgFormat);
    ImageMetaData originalIMD = getImgMgr().getImgOriginal().getImageMetaData(imgOriginal);
    File scaledFile = initScaledFile(imgFormat.opt(ImgFormat.EXTENSION));
    ImageMetaData scaledIMD = originalIMD.convertByHeight(scaledFile,
                                                          height,
                                                          imgFormat.opt(ImgFormat.IMPARAMETER),
                                                          imgFormat.opt(ImgFormat.IMFORMAT));
    String webPath = getImgMgr().fileToWebPath(scaledFile);
    initMetaData(instance, scaledIMD, webPath, imgFormat);
    existingInstance = getExistingInstanceByHeight(imgOriginal.getId(), height, imgFormat.getId());
    if (existingInstance != null) {
      scaledFile.delete();
      return existingInstance;
    }
    return instance.makePersistent(null);
  }

  public PersistentRecord getOrCreateInstanceByBox(PersistentRecord imgOriginal,
                                                   int width,
                                                   int height,
                                                   PersistentRecord imgFormat)
      throws MirrorFieldException, MirrorInstantiationException, IOException
  {
    int originalWidth = imgOriginal.compInt(WIDTH);
    int originalHeight = imgOriginal.compInt(HEIGHT);
    double originalAspect = (double) originalWidth / (double) originalHeight;
    double targetAspect = (double) width / (double) height;
    if (originalAspect < targetAspect) {
      width = originalWidth * height / originalHeight;
    }
    return getOrCreateInstanceByWidth(imgOriginal, width, imgFormat);
  }

  public PersistentRecord getOrCreateInstanceByWidthAspect(PersistentRecord imgOriginal,
                                                           int width,
                                                           double aspect,
                                                           PersistentRecord imgFormat)
      throws MirrorFieldException, MirrorInstantiationException, IOException
  {
    return getOrCreateInstanceByWidthHeight(imgOriginal,
                                            width,
                                            (int) Math.round(width / aspect),
                                            imgFormat);
  }
}
