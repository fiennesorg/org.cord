package org.cord.node.img;

import org.cord.mirror.PersistentRecord;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.webmacro.Context;

import com.google.common.base.Preconditions;

public abstract class ImgMgrView
  extends DynamicAclAuthenticatedFactory
{
  private final String __title;
  private final String __wrapperTemplatePath;
  private final String __templatePath;

  public static final String WM_H1 = "h1";
  public static final String WM_TEMPLATEPATH = "templatePath";
  public static final String WM_IMGSEARCH = "imgSearch";

  public ImgMgrView(String name,
                    String title,
                    NodeManager nodeMgr,
                    Integer aclId,
                    String wrapperTemplatePath,
                    String templatePath)
  {
    super(name,
          nodeMgr,
          InternalSuccessOperation.getInstance(),
          aclId,
          "You do not have the correct ImgMgr authentications.");
    __title = Preconditions.checkNotNull(title, "title");
    __wrapperTemplatePath = Preconditions.checkNotNull(wrapperTemplatePath, "wrapperTemplatePath");
    __templatePath = templatePath;
  }

  public final String getTitle()
  {
    return __title;
  }

  public final ImgMgr getImgMgr()
  {
    return getNodeManager().getImgMgr();
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    ImgSearch imgSearch = new ImgSearch(getImgMgr(), nodeRequest);
    context.put(WM_IMGSEARCH, imgSearch);
    context.put(WM_H1, getTitle());
    context.put(WM_TEMPLATEPATH, __templatePath);
    doWork(nodeRequest, imgSearch, node, context);
    return new NodeRequestSegment(getNodeManager(),
                                  nodeRequest,
                                  node,
                                  context,
                                  __wrapperTemplatePath);
  }

  protected abstract void doWork(NodeRequest nodeRequest,
                                 ImgSearch imgSearch,
                                 PersistentRecord node,
                                 Context context)
      throws NodeRequestException;
}
