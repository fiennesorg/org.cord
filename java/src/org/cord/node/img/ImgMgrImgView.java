package org.cord.node.img;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.webmacro.Context;

public abstract class ImgMgrImgView
  extends ImgMgrView
{
  public static final String IN_IMGORIGINAL_ID = "imgOriginal_id";

  public static final String OUT_IMGORIGINAL = "imgOriginal";

  public ImgMgrImgView(String name,
                       String title,
                       NodeManager nodeMgr,
                       Integer aclId,
                       String wrapperTemplatePath,
                       String templatePath)
  {
    super(name,
          title,
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          templatePath);
  }

  @Override
  protected final void doWork(NodeRequest nodeRequest,
                              ImgSearch imgSearch,
                              PersistentRecord node,
                              Context context)
      throws NodeRequestException
  {
    Object id = nodeRequest.get(IN_IMGORIGINAL_ID);
    PersistentRecord imgOriginal = getImgMgr().getImgOriginal().getTable().getOptRecord(id);
    if (imgOriginal == null) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       "Unable to resolve \"%s\" into an ImgOriginal",
                                       id);
    }
    context.put(OUT_IMGORIGINAL, imgOriginal);
    doWork(nodeRequest, imgSearch, node, context, imgOriginal);
  }

  protected abstract void doWork(NodeRequest nodeRequest,
                                 ImgSearch imgSearch,
                                 PersistentRecord node,
                                 Context context,
                                 PersistentRecord imgOriginal)
      throws NodeRequestException;

}
