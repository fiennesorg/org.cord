package org.cord.node.img;

import java.io.File;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.webmacro.Context;

public class ImgOriginal_Create
  extends ImgMgrView
{
  public static final String NAME = "ImgOriginal_Create";

  public static final String IN_FILE = "file";

  public static final String OUT_IMGORIGINAL = ImgMgrImgView.OUT_IMGORIGINAL;

  public ImgOriginal_Create(NodeManager nodeMgr,
                            Integer aclId,
                            String wrapperTemplatePath)
  {
    super(NAME,
          "Upload new image",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          ImgOriginal_View.TEMPLATEPATH);
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context)
      throws NodeRequestException
  {
    File file = nodeRequest.getFile(IN_FILE);
    try {
      context.put(OUT_IMGORIGINAL,
                  getImgMgr().getImgOriginal().createInstance(file, imgSearch.owner));
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(), node, e, "Error parsing or importing image");
    }
  }
}
