package org.cord.node.img;

import java.io.File;
import java.io.IOException;

import org.cord.image.ImageDimension;
import org.cord.image.ImageMetaData;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.node.NodeManager;
import org.cord.util.Casters;
import org.cord.util.LogicException;
import org.cord.webmacro.TagBuilder;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public abstract class ImgTableWrapper
  extends ImgMgrTableWrapper
{
  public static final IntFieldKey WIDTH = IntField.createKey("width");
  public static final IntFieldKey HEIGHT = IntField.createKey("height");
  public static final IntFieldKey SIZEKB = IntField.createKey("sizeKb");
  /**
   * The path to the image relative to the docbase or "" if the asset is no longer available.
   */
  public static final ObjectFieldKey<String> WEBPATH = StringField.createKey("webPath");
  public static final IntFieldKey IMGFORMAT_ID = LinkField.getLinkFieldName(ImgFormat.TABLENAME);

  /**
   * Return the java File that this record wraps or null if the record has been deleted.
   */
  public static final RecordOperationKey<File> FILE = RecordOperationKey.create("file", File.class);
  public static final RecordOperationKey<ImageMetaData> IMAGEMETADATA =
      RecordOperationKey.create("imageMetaData", ImageMetaData.class);
  public static final RecordOperationKey<TagBuilder> IMGTAG =
      RecordOperationKey.create("imgTag", TagBuilder.class);
  public static final MonoRecordOperationKey<String, TagBuilder> IMGTAGWITHALT =
      MonoRecordOperationKey.create("imgTagWithAlt", String.class, TagBuilder.class);
  public static final RecordOperationKey<String> URL =
      RecordOperationKey.create("url", String.class);

  /**
   * $img.aspect gives you the aspect ratio defined as (width/height) with landscape being greater
   * than 1 and portrait being smaller.
   */
  public static final RecordOperationKey<Double> ASPECT =
      RecordOperationKey.create("aspect", Double.class);

  public static final RecordOperationKey<ImageDimension> IMAGEDIMENSION =
      RecordOperationKey.create("imageDimension", ImageDimension.class);

  // /dyn/org/
  private final String __webPathDir;

  // /User/Alex/cvsroot/acme/www/dyn/org
  protected final File __uploadDir;

  // /dyn/org/1234/filename.jpg
  // /dyn/scl/5678.png

  public ImgTableWrapper(ImgMgr imgMgr,
                         String tableName,
                         String webPathDir)
  {
    super(imgMgr,
          tableName);
    __webPathDir = NodeManager.toValidWebPathDir(webPathDir);
    __uploadDir = new File(imgMgr.getDocBase(), __webPathDir);
  }

  protected final String getWebPathDir()
  {
    return __webPathDir;
  }

  protected final void initMetaData(TransientRecord instance,
                                    ImageMetaData imd,
                                    String webPath,
                                    PersistentRecord imgFormat)
      throws MirrorFieldException
  {
    instance.setField(WIDTH, Integer.valueOf(imd.getWidth()));
    instance.setField(HEIGHT, Integer.valueOf(imd.getHeight()));
    instance.setField(SIZEKB, Integer.valueOf(imd.getKilobytes()));
    instance.setField(WEBPATH, webPath);
    instance.setField(IMGFORMAT_ID, imgFormat);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new IntField(WIDTH, "Width in pixels"));
    table.addField(new IntField(HEIGHT, "Height in pixels"));
    table.addField(new IntField(SIZEKB, "Size in kb"));
    table.addField(new StringField(WEBPATH, "Web path"));
    table.setTitleOperationName(WEBPATH);
    table.addField(new LinkField(ImgFormat.TABLENAME,
                                 "Img Format",
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_PREDELETED_DELETED,
                                                   false,
                                                   table.getName()) {
      @Override
      public void trigger(PersistentRecord img,
                          Transaction transaction,
                          int state)
          throws Exception
      {
        File file = img.opt(FILE);
        if (file.exists()) {
          file.delete();
        }
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<File>(FILE) {
      @Override
      public File getOperation(TransientRecord img,
                               Viewpoint viewpoint)
      {
        String webpath = img.comp(WEBPATH);
        if (Strings.isNullOrEmpty(webpath)) {
          return null;
        }
        return new File(getImgMgr().getDocBase(), webpath);
      }
    });
    table.addRecordOperation(new DirectCachingSimpleRecordOperation<ImageMetaData>(IMAGEMETADATA) {
      @Override
      public ImageMetaData calculate(TransientRecord img,
                                     Viewpoint viewpoint)
      {
        try {
          return ImageMetaData.getInstance(img.opt(FILE));
        } catch (IOException e) {
          throw new LogicException(String.format("Unexpected error identifying %s on %s",
                                                 img.opt(FILE),
                                                 img),
                                   e);
        }
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<TagBuilder>(IMGTAG) {
      @Override
      public TagBuilder getOperation(TransientRecord img,
                                     Viewpoint viewpoint)
      {
        return getImgTag(img);
      }
    });
    table.addRecordOperation(new MonoRecordOperation<String, TagBuilder>(IMGTAGWITHALT,
                                                                         Casters.TOSTRING) {
      @Override
      public TagBuilder calculate(TransientRecord img,
                                  Viewpoint viewpoint,
                                  String alt)
      {
        TagBuilder tag = getImgTag(img);
        tag.addAttribute("alt", alt);
        return tag;
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<String>(URL) {
      @Override
      public String getOperation(TransientRecord img,
                                 Viewpoint viewpoint)
      {
        return getImgMgr().getHttpServletPath() + img.opt(WEBPATH);
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<Double>(ASPECT) {
      @Override
      public Double getOperation(TransientRecord img,
                                 Viewpoint viewpoint)
      {
        return Double.valueOf((double) img.compInt(WIDTH) / (double) img.compInt(HEIGHT));
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<ImageDimension>(IMAGEDIMENSION) {
      @Override
      public ImageDimension getOperation(TransientRecord img,
                                         Viewpoint viewpoint)
      {
        return new ImageDimension(img.compInt(ImgTableWrapper.WIDTH),
                                  img.compInt(ImgTableWrapper.HEIGHT));
      }
    });
    // table.addRecordSourceOperation(new OrderByComparator(BYFILENAME, BYFILENAME_COMPARATOR));
  }

  public void assertIsImgTableWrapper(TransientRecord record)
  {
    Preconditions.checkState(record != null
                             && record.getTable().getTableWrapper() instanceof ImgTableWrapper,
                             "%s is not an ImgTableWrapper",
                             record);
  }

  public TagBuilder getImgTag(TransientRecord img)
  {
    assertIsImgTableWrapper(img);
    TagBuilder tag = new TagBuilder("img");
    tag.addAttribute("src", img.opt(WEBPATH));
    tag.addAttribute("width", img.opt(WIDTH));
    tag.addAttribute("height", img.opt(HEIGHT));
    return tag;
  }

  public final File getFile(TransientRecord img)
  {
    assertIsImgTableWrapper(img);
    return img.opt(FILE);
  }

  public final ImageMetaData getImageMetaData(PersistentRecord img)
  {
    assertIsImgTableWrapper(img);
    return img.opt(IMAGEMETADATA);
  }

}
