package org.cord.node.img;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.webmacro.Context;

/**
 * ImgMgrView that lets us view a list of matching ImgOriginal records
 * 
 * @author alex
 */
public class ImgOriginal_Search
  extends ImgMgrView
{
  public static final String NAME = "ImgOriginal_Search";

  public static final String IN_SEARCH_TITLE = "search_title";
  public static final String IN_SEARCH_DESCRIPTION = "search_description";
  public static final String IN_SEARCH_FILENAME = "search_filename";
  public static final String IN_SEARCH_OWNER = "search_owner";
  public static final String IN_SEARCH_MINASPECTRATIO = "search_minAspectRatio";
  public static final String IN_SEARCH_FIXEDASPECTRATIO = "search_fixedAspectRatio";
  public static final String IN_SEARCH_MAXASPECTRATIO = "search_maxAspectRatio";

  public static final String OUT_IMGORIGINALS = "imgOriginals";

  public ImgOriginal_Search(NodeManager nodeMgr,
                            Integer aclId,
                            String wrapperTemplatePath)
  {
    super(NAME,
          "Image Search",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          "ImgMgr/ImgOriginal_Search.wm.html");
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context)
      throws NodeRequestException
  {
    context.put(OUT_IMGORIGINALS, imgSearch.getMatchingImgOriginals());
  }

}
