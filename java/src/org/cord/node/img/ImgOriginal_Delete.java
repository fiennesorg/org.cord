package org.cord.node.img;

import java.io.File;

import org.cord.mirror.Db;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.RedirectionException;
import org.webmacro.Context;

/**
 * View that resolves a given ImgOriginal record, deletes the asset and any ImgScaled records that
 * have been built from it and then sets the replacementImgOriginal_id to point at
 * {@link ImgOriginal#getDeletedImage()}
 * 
 * @author alex
 */
public class ImgOriginal_Delete
  extends ImgMgrImgView
{
  public static final String NAME = "ImgOriginal_Delete";
  private final String __pwd;

  public ImgOriginal_Delete(NodeManager nodeMgr,
                            String pwd,
                            Integer aclId,
                            String wrapperTemplatePath)
  {
    super(NAME,
          "Delete Image",
          nodeMgr,
          aclId,
          wrapperTemplatePath,
          "ImgMgr/imgOriginal_Delete.wm.html");
    __pwd = pwd;
  }

  @Override
  protected void doWork(NodeRequest nodeRequest,
                        ImgSearch imgSearch,
                        PersistentRecord node,
                        Context context,
                        PersistentRecord imgOriginal)
      throws NodeRequestException
  {
    if (!getImgMgr().getImgOriginal().isDeleteable(imgOriginal)) {
      throw new RedirectionException(imgSearch.getUrl(ImgOriginal_Search.NAME), node, null);
    }
    File file = imgOriginal.opt(ImgOriginal.FILE);
    try (Transaction t = getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                    Transaction.TRANSACTION_DELETE,
                                                                    __pwd,
                                                                    NAME)) {
      t.delete(imgOriginal.comp(ImgOriginal.IMGSCALEDS), Db.DEFAULT_TIMEOUT);
      t.commit();
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       e,
                                       "Unexpected error deleting scaled images for %s",
                                       imgOriginal);
    }
    final boolean canRemoveRecord;
    try (Transaction t = getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                    Transaction.TRANSACTION_DELETE,
                                                                    __pwd,
                                                                    NAME)) {
      t.delete(imgOriginal);
      canRemoveRecord =
          t.getTables().size() == 1 ? t.getRecords(ImgOriginal.TABLENAME).size() == 1 : false;
      if (canRemoveRecord) {
        t.commit();
      }
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(),
                                       node,
                                       e,
                                       "Unexpected error evaluating deleting %s",
                                       imgOriginal);
    }
    if (!canRemoveRecord) {
      try (
          Transaction t = getNodeManager().getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                                     Transaction.TRANSACTION_UPDATE,
                                                                     __pwd,
                                                                     NAME)) {
        WritableRecord rwImgOriginal = t.edit(imgOriginal);
        rwImgOriginal.setField(ImgOriginal.WEBPATH, "");
        rwImgOriginal.setField(ImgOriginal.REPLACEMENTIMGORIGINAL_ID,
                               getImgMgr().getImgOriginal().getDeletedImage());
        t.commit();
        file.delete();
      } catch (Exception e) {
        throw new FeedbackErrorException(getTitle(),
                                         node,
                                         e,
                                         "Unexpected error updating metadata on %s",
                                         imgOriginal);
      }
    }
    throw new RedirectionException(imgSearch.getUrl(ImgOriginal_Search.NAME), node, null);
  }
}
