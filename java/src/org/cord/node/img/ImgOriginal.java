package org.cord.node.img;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.cord.image.ImageMetaData;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.TrioRecordOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.DuoRecordOperation;
import org.cord.mirror.operation.DuoRecordOperationKey;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.operation.TrioRecordOperation;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.PredicateRecordFilter;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.mirror.recordsource.operation.MonoQueryOrRecordSourceOperation;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.Node;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;
import org.cord.util.LogicException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;

import it.unimi.dsi.fastutil.ints.IntArraySet;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;

public class ImgOriginal
  extends ImgTableWrapper
{
  public static final String TABLENAME = "ImgOriginal";

  // SQL Fields

  public static final ObjectFieldKey<String> FILENAME = StringField.createKey("filename");
  public static final ObjectFieldKey<Date> TIMESTAMP = FastDateField.createKey("timestamp");
  public static final ObjectFieldKey<String> TITLE = StringField.createKey("title");
  public static final ObjectFieldKey<String> DESCRIPTION = StringField.createKey("description");

  public static final IntFieldKey REPLACEMENTIMGORIGINAL_ID =
      IntFieldKey.create("replacementImgOriginal_id");
  public static final RecordOperationKey<PersistentRecord> REPLACEMENTIMGORIGINAL =
      LinkField.getLinkName(REPLACEMENTIMGORIGINAL_ID);
  public static final RecordOperationKey<RecordSource> REPLACEDIMGORIGINALS =
      RecordOperationKey.create("replacedImgOriginals", RecordSource.class);

  public static final RecordOperationKey<Boolean> ISDELETEABLE =
      RecordOperationKey.create("isDeleteable", Boolean.class);

  /**
   * The md5 checksum of the File related to this ImgOriginal. This will be created when the record
   * is initialised and is immutable.
   */
  public static final ObjectFieldKey<String> MD5 = StringField.createKey("md5");

  // public static final RecordOperationKey<JSONObject> DATA =
  // RecordOperationKey.create("data", JSONObject.class);
  // public static final ObjectFieldKey<String> DATASRC = JSONObjectField.createKey("dataSrc");
  public static final ObjectFieldKey<JSONObject> DATA = PureJsonObjectField.createKey("data");

  public static final String DATA__COPYRIGHT = "copyright";
  public static final String DATA__ALT = "alt";

  public static final ObjectFieldKey<String> OWNER = StringField.createKey("owner");

  public static final String OWNER_NOBODY = "";

  public static final IntFieldKey CROPPEDFROMIMGORIGINAL_ID =
      LinkField.createKey("croppedFromImgOriginal_id");
  public static final RecordOperationKey<PersistentRecord> CROPPEDFROMIMGORIGINAL =
      LinkField.getLinkName(CROPPEDFROMIMGORIGINAL_ID);
  public static final RecordOperationKey<RecordSource> CROPPEDTOIMGORIGINALS =
      RecordOperationKey.create("croppedToImgOriginals", RecordSource.class);

  // RecordOperations...

  /**
   * All of the {@link ImgScaled} records that contain scaled versions of this ImgOriginal.
   */
  public static final RecordOperationKey<RecordSource> IMGSCALEDS =
      ImgScaled.IMGORIGINAL__IMGSCALEDS;

  public static final MonoRecordOperationKey<Integer, PersistentRecord> ATWIDTH =
      MonoRecordOperationKey.create("atWidth", Integer.class, PersistentRecord.class);
  public static final DuoRecordOperationKey<Integer, Object, PersistentRecord> ATWIDTHFORMAT =
      DuoRecordOperationKey.create("atWidthFormat",
                                   Integer.class,
                                   Object.class,
                                   PersistentRecord.class);
  public static final TrioRecordOperationKey<Integer, Integer, Object, PersistentRecord> ATWIDTHHEIGHTFORMAT =
      TrioRecordOperationKey.create("atWidthHeightFormat",
                                    Integer.class,
                                    Integer.class,
                                    Object.class,
                                    PersistentRecord.class);

  public static final MonoRecordOperationKey<Integer, PersistentRecord> ATHEIGHT =
      MonoRecordOperationKey.create("atHeight", Integer.class, PersistentRecord.class);

  public static final DuoRecordOperationKey<Integer, Object, PersistentRecord> ATHEIGHTFORMAT =
      DuoRecordOperationKey.create("atHeightFormat",
                                   Integer.class,
                                   Object.class,
                                   PersistentRecord.class);

  public static final DuoRecordOperationKey<Integer, Double, PersistentRecord> ATWIDTHASPECT =
      DuoRecordOperationKey.create("atWidthAspect",
                                   Integer.class,
                                   Double.class,
                                   PersistentRecord.class);

  /**
   * Generate a RecordSource that contains all the ImgOriginal records that have been replaced by
   * this one, and so forth back up the chain so that you get all the ImgOriginal records that now
   * point to this one regardless of the number of hops that it takes. If you have any recursive
   * loops in your ImgOriginal replacement chains then this will throw a RuntimeException rather
   * than going into a non-terminating loop.
   */
  public static final RecordOperationKey<RecordSource> ALLREPLACEDIMGORIGINALS =
      RecordOperationKey.create("allReplacedImgOriginals", RecordSource.class);

  /**
   * Resolve all of the Nodes that have a reference to this ImgOriginal as a single RecordSource.
   * Nodes will only appear in the list once regardless of how many different ways they utilise the
   * ImgOriginal. This will utilise the merged set of RecordOperations that have been added via
   * {@link #registerNodeResolver(RecordOperationKey)} and it is the responsibility of the utilising
   * Tables to let the ImgOriginal know of their existence.
   */
  public static final RecordOperationKey<RecordSource> NODES =
      RecordOperationKey.create("nodes", RecordSource.class);

  /**
   * Resolve all of the Nodes that have a refernec to this ImgOriginal, either directly or by
   * referring to an ImgOriginal that is replaced by this ImgOriginal, either directly or
   * indirectly.
   */
  public static final RecordOperationKey<RecordSource> ALLNODES =
      RecordOperationKey.create("allNodes", RecordSource.class);

  // RecordSourceOperations

  public static final MonoRecordSourceOperationKey<String, RecordSource> OWNEDBY =
      MonoRecordSourceOperationKey.create("ownedBy", String.class, RecordSource.class);

  private final List<RecordOperationKey<RecordSource>> __nodeResolvers = Lists.newArrayList();

  public ImgOriginal(ImgMgr imgMgr,
                     String webPathDir)
  {
    super(imgMgr,
          TABLENAME,
          webPathDir);
  }

  /**
   * @param imgOriginalKey
   *          The RecordOperationKey that is registered on ImgOriginal and which when invoked will
   *          return a RecordSource of Node records that are using this ImgOriginal.
   */
  public void registerNodeResolver(RecordOperationKey<RecordSource> imgOriginalKey)
  {
    __nodeResolvers.add(Preconditions.checkNotNull(imgOriginalKey, "imgOriginalKey"));
  }

  /**
   * Create an ImgOriginal record from an image File after copying the File into a new location
   * under the web path, with no meta-data defined about the image.
   */
  public PersistentRecord createInstance(File image,
                                         String owner)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    return createInstance(image, null, null, owner);
  }

  public PersistentRecord createInstance(ByteSource data,
                                         String filename,
                                         String title,
                                         String description,
                                         String owner)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    File tempDir = Files.createTempDir();
    try {
      File tempDataFile = new File(tempDir, filename);
      data.copyTo(Files.asByteSink(tempDataFile));
      return createInstance(tempDataFile, title, description, owner);
    } finally {
      IoUtil.deleteRecursive(tempDir);
    }
  }

  public PersistentRecord createInstance(File image,
                                         String title,
                                         String description,
                                         String owner)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    String md5 = Files.asByteSource(image).hash(Hashing.md5()).toString();
    Optional<PersistentRecord> existingInstance = getInstanceByMD5(md5, owner, null);
    if (existingInstance.isPresent()) {
      return existingInstance.get();
    }
    ImageMetaData.autoOrientImage(image);
    ImageMetaData imd = ImageMetaData.getInstance(image);
    TransientRecord instance = getTable().createTransientRecord();
    File instanceDir = getInstanceDir(instance);
    if (!instanceDir.mkdir()) {
      throw new IOException(String.format("Unable to create instanceDir of %s", instanceDir));
    }
    File instanceFile = new File(instanceDir, image.getName());
    IoUtil.unixCp(image, instanceFile, false);
    String webPath = getImgMgr().fileToWebPath(instanceFile);
    initMetaData(instance,
                 imd,
                 webPath,
                 getImgMgr().getImgFormat().getDefaultConfiguration(imd.getFormat()));
    if (title != null) {
      instance.setField(TITLE, title);
    }
    if (description != null) {
      instance.setField(DESCRIPTION, description);
    }
    if (owner != null) {
      instance.setField(OWNER, owner);
    }
    instance.setField(MD5, md5);
    return instance.makePersistent(null);
  }

  private File getInstanceDir(TransientRecord pendingImgOriginal)
  {
    return new File(__uploadDir, Integer.toString(pendingImgOriginal.getPendingId()));
  }

  public Optional<PersistentRecord> getInstanceByMD5(String md5,
                                                     String owner,
                                                     Viewpoint viewpoint)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(ImgOriginal.TABLENAME).append('.').append(ImgOriginal.MD5).append('=');
    SqlUtil.toSafeSqlString(buf, md5);
    buf.append(" and ")
       .append(ImgOriginal.TABLENAME)
       .append('.')
       .append(ImgOriginal.OWNER)
       .append('=');
    SqlUtil.toSafeSqlString(buf, owner);
    String filter = buf.toString();
    return RecordSources.optFirstRecord(getTable().getQuery(filter, filter, Query.NOSQLORDERING),
                                        viewpoint);
  }

  private final Object __webPathSync = new Object();

  /**
   * Create or resolve an ImgOriginal record that wraps an image File that is already accessible
   * under the web path and which doesn't get copied when the record is created.
   */
  public PersistentRecord createStaticInstance(File image,
                                               Optional<String> title,
                                               Optional<String> description)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    Preconditions.checkNotNull(image, "image");
    String webPath = getImgMgr().fileToWebPath(image);
    synchronized (__webPathSync) {
      PersistentRecord existing = getOptInstanceByWebPath(webPath);
      if (existing != null) {
        return existing;
      }
      ImageMetaData imd = ImageMetaData.getInstance(image);
      TransientRecord instance = getTable().createTransientRecord();
      initMetaData(instance,
                   imd,
                   webPath,
                   getImgMgr().getImgFormat().getDefaultConfiguration(imd.getFormat()));
      instance.setField(MD5, Files.asByteSource(image).hash(Hashing.md5()).toString());
      if (title.isPresent()) {
        instance.setField(TITLE, title.get());
      }
      if (description.isPresent()) {
        instance.setField(DESCRIPTION, description.get());
      }
      return instance.makePersistent(null);
    }
  }

  /**
   * Create or resolve an ImgOriginal that is defined by the given webPath relative to the doc root
   * for the application.
   */
  public PersistentRecord createStaticInstance(String webPath,
                                               Optional<String> title,
                                               Optional<String> description)
      throws MirrorFieldException, MirrorInstantiationException, IOException
  {
    return createStaticInstance(getImgMgr().webPathToFile(webPath), title, description);
  }

  public PersistentRecord createStaticInstance(String webPath)
      throws MirrorFieldException, MirrorInstantiationException, IOException
  {
    return createStaticInstance(webPath, Optional.<String> absent(), Optional.<String> absent());
  }

  public static final String WEBPATH_DELETED_IMAGE = "/statics/build/node/img/deleted_image.gif";

  /**
   * Resolve the ImgOriginal that represents a deleted image. The id of this might conceivably
   * change from application to application depending on the order of the bootup of the process, but
   * it should never fail to resolve.
   * 
   * @see #WEBPATH_DELETED_IMAGE
   */
  public PersistentRecord getDeletedImage()
  {
    try {
      return createStaticInstance(WEBPATH_DELETED_IMAGE,
                                  Optional.<String> absent(),
                                  Optional.of("Placeholder image that is used to represent a deleted image"));
    } catch (Exception e) {
      throw new LogicException("Unable to resolve " + WEBPATH_DELETED_IMAGE + " into an image", e);
    }
  }

  /**
   * Import a directory or ImgOriginal files that are already accessible under the web path. If file
   * is a single image then that is imported directly. If it is a directory then it is parsed
   * recursively and all the contained images are imported. Any files that are not images or
   * directories get logged to DEBUG_OUT.
   * 
   * @return A List of all the ImgOriginal PersistentRecords that were created by this method.
   */
  public RecordSource createStaticInstances(File file)
      throws MirrorFieldException, MirrorInstantiationException
  {
    IdList imgOriginalIds = new ArrayIdList(this);
    createStaticInstances(imgOriginalIds, file);
    return new ConstantRecordSource(UnmodifiableIdList.getInstance(imgOriginalIds), null);
  }

  /**
   * Import a directory or ImgOriginal files that are already accessible under the web path. If file
   * is a single image then that is imported directly. If it is a directory then it is parsed
   * recursively and all the contained images are imported. Any files that are not images or
   * directories get logged to DEBUG_OUT.
   * 
   * @param webpath
   *          The path to the image or directory relative to the www folder for the site (eg
   *          "/statics/img/abc.gif")
   * @return A List of all the ImgOriginal PersistentRecords that were created by this method.
   */
  public RecordSource createStaticInstances(String webpath)
      throws MirrorFieldException, MirrorInstantiationException
  {
    return createStaticInstances(getImgMgr().webPathToFile(webpath));
  }

  private void createStaticInstances(IdList imgOriginalIds,
                                     File file)
      throws MirrorFieldException, MirrorInstantiationException
  {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        createStaticInstances(imgOriginalIds, child);
      }
    } else {
      try {
        imgOriginalIds.add(createStaticInstance(file,
                                                Optional.<String> absent(),
                                                Optional.<String> absent()).getId());
      } catch (IOException e) {
        DebugConstants.DEBUG_OUT.println(file + " --> " + e);
      }
    }
  }

  public File toFile(String webPath)
  {
    return new File(getImgMgr().getDocBase(), webPath);
  }

  public PersistentRecord getOptInstanceByWebPath(String webPath)
  {
    return TableStringMatcher.optOnlyMatch(getTable(), WEBPATH, webPath, null);
  }

  @Override
  protected void initTable(final Db db,
                           final String pwd,
                           final Table table)
      throws MirrorTableLockedException
  {
    super.initTable(db, pwd, table);
    // --------------------- SQL Fields ---------------------------
    table.addField(new StringField(FILENAME, "Filename"));
    table.addFieldTrigger(new AbstractFieldTrigger<String>(WEBPATH, true, true) {
      @Override
      protected Object triggerField(TransientRecord imgOriginal,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        String webpath = imgOriginal.comp(WEBPATH);
        int i = webpath.lastIndexOf('/');
        imgOriginal.setField(FILENAME, i == -1 ? webpath : webpath.substring(i + 1));
        return null;
      }
    });
    table.addField(new FastDateField(TIMESTAMP, "Timestamp"));
    table.addField(new StringField(TITLE, "Title"));
    table.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord imgOriginal,
                             Gettable params)
          throws MirrorException
      {
        String title = imgOriginal.comp(TITLE);
        if (title.trim().length() == 0) {
          imgOriginal.setField(TITLE, imgOriginal.comp(FILENAME));
        }
      }
    });
    table.setDefaultOrdering(TABLENAME + "." + TITLE);
    table.addField(new StringField(DESCRIPTION, "Description"));
    table.addField(new LinkField(REPLACEMENTIMGORIGINAL_ID,
                                 "Replacement ImgOriginal record",
                                 null,
                                 REPLACEMENTIMGORIGINAL,
                                 ImgOriginal.TABLENAME,
                                 REPLACEDIMGORIGINALS,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
    table.addField(new StringField(MD5, "MD5 checksum", StringField.LENGTH_VARCHAR, null, ""));
    // table.addField(new JSONObjectField(DATASRC, "Data params", DATA, null));
    table.addField(new PureJsonObjectField(DATA,
                                           "JSON Data",
                                           PureJsonObjectField.emptyFieldValueSupplier()));
    table.addField(new StringField(OWNER, "Owner"));
    table.addField(new LinkField(CROPPEDFROMIMGORIGINAL_ID,
                                 "Cropped From ImgOriginal Record",
                                 null,
                                 CROPPEDFROMIMGORIGINAL,
                                 ImgOriginal.TABLENAME,
                                 CROPPEDTOIMGORIGINALS,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));

    // --------------------- RecordOperations -----------------------
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(NODES) {
      @Override
      public RecordSource getOperation(TransientRecord imgOriginal,
                                       Viewpoint viewpoint)
      {
        IdList nodeIds = new ArrayIdList(db.getTable(Node.TABLENAME));
        for (RecordOperationKey<RecordSource> nodeResolver : __nodeResolvers) {
          for (IntIterator i = imgOriginal.comp(nodeResolver, viewpoint)
                                          .getIdList(viewpoint)
                                          .iterator(); i.hasNext();) {
            int nodeId = i.nextInt();
            if (!nodeIds.contains(nodeId)) {
              nodeIds.add(nodeId);
            }
          }
        }
        return new ConstantRecordSource(UnmodifiableIdList.getInstance(nodeIds), null);
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(ALLREPLACEDIMGORIGINALS) {
      @Override
      public RecordSource getOperation(TransientRecord imgOriginal,
                                       Viewpoint viewpoint)
      {
        IdList imgOriginalIds = new ArrayIdList(ImgOriginal.this);
        addReplacedImgOriginals(imgOriginal, viewpoint, imgOriginalIds);
        return new ConstantRecordSource(UnmodifiableIdList.getInstance(imgOriginalIds), null);
      }

      private void addReplacedImgOriginals(TransientRecord imgOriginal,
                                           Viewpoint viewpoint,
                                           IdList imgOriginalIds)
      {
        for (PersistentRecord replacedImgOriginal : imgOriginal.comp(REPLACEDIMGORIGINALS,
                                                                     viewpoint)) {
          int id = replacedImgOriginal.getId();
          if (imgOriginalIds.contains(id)) {
            throw new IllegalStateException(String.format("ImgOriginal replacement loop - %s refernces %s",
                                                          replacedImgOriginal,
                                                          imgOriginal));
          }
          imgOriginalIds.add(id);
          try {
            addReplacedImgOriginals(replacedImgOriginal, viewpoint, imgOriginalIds);
          } catch (Exception e) {
            throw new IllegalStateException(String.format("ImgOriginal replacement loop -  %s references %s",
                                                          imgOriginal,
                                                          imgOriginal.opt(REPLACEMENTIMGORIGINAL)),
                                            e);
          }
        }
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(ALLNODES) {
      @Override
      public RecordSource getOperation(TransientRecord imgOriginal,
                                       Viewpoint viewpoint)
      {
        IntSet nodeIds = new IntArraySet();
        nodeIds.addAll(imgOriginal.comp(ImgOriginal.NODES, viewpoint).getIdList(viewpoint));
        for (PersistentRecord replacedImgOriginal : imgOriginal.comp(REPLACEDIMGORIGINALS,
                                                                     viewpoint)) {
          nodeIds.addAll(replacedImgOriginal.comp(ImgOriginal.NODES, viewpoint)
                                            .getIdList(viewpoint));
        }
        return ConstantRecordSource.fromIds(getDb().getTable(Node.TABLENAME), null, nodeIds);
      }
    });
    table.addRecordOperation(new MonoRecordOperation<Integer, PersistentRecord>(ATWIDTH,
                                                                                Casters.TOINT) {
      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer width)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATWIDTH, width);
        }
        try {
          return getImgMgr().getImgScaled()
                            .getOrCreateInstanceByWidth((PersistentRecord) imgOriginal,
                                                        width.intValue(),
                                                        null);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generate scaled image of width %s from %s",
                                                 width,
                                                 imgOriginal),
                                   e);
        }
      }
    });
    table.addRecordOperation(new DuoRecordOperation<Integer, Object, PersistentRecord>(ATWIDTHFORMAT,
                                                                                       Casters.TOINT,
                                                                                       Casters.NULL) {
      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer width,
                                        Object imgFormatObj)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATWIDTHFORMAT, width, imgFormatObj);
        }
        PersistentRecord imgFormat = getImgMgr().getImgFormat().getInstance(imgFormatObj);
        try {
          return getImgMgr().getImgScaled()
                            .getOrCreateInstanceByWidth((PersistentRecord) imgOriginal,
                                                        width.intValue(),
                                                        imgFormat);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generate scaled image of width %s, format %s from %s",
                                                 width,
                                                 imgFormat,
                                                 imgOriginal),
                                   e);
        }
      }
    });
    table.addRecordOperation(new TrioRecordOperation<Integer, Integer, Object, PersistentRecord>(ATWIDTHHEIGHTFORMAT,
                                                                                                 Casters.TOINT,
                                                                                                 Casters.TOINT,
                                                                                                 Casters.NULL) {
      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer width,
                                        Integer height,
                                        Object imgFormatObj)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATWIDTHHEIGHTFORMAT, width, height, imgFormatObj);
        }
        PersistentRecord imgFormat = getImgMgr().getImgFormat().getInstance(imgFormatObj);
        try {
          return getImgMgr().getImgScaled().getOrCreateInstanceByBox((PersistentRecord) imgOriginal,
                                                                     width.intValue(),
                                                                     height.intValue(),
                                                                     imgFormat);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generate scaled image of width %s, height %s, format %s from %s",
                                                 width,
                                                 height,
                                                 imgFormat,
                                                 imgOriginal),
                                   e);
        }
      }
    });
    table.addRecordOperation(new MonoRecordOperation<Integer, PersistentRecord>(ATHEIGHT,
                                                                                Casters.TOINT) {
      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer height)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATHEIGHT, height);
        }
        try {
          return getImgMgr().getImgScaled()
                            .getOrCreateInstanceByHeight((PersistentRecord) imgOriginal,
                                                         height.intValue(),
                                                         null);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generate scaled image of height %s from %s",
                                                 height,
                                                 imgOriginal),
                                   e);
        }
      }
    });
    table.addRecordOperation(new DuoRecordOperation<Integer, Object, PersistentRecord>(ATHEIGHTFORMAT,
                                                                                       Casters.TOINT,
                                                                                       Casters.NULL) {

      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer height,
                                        Object imgFormatObj)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATHEIGHTFORMAT, height, imgFormatObj);
        }
        PersistentRecord imgFormat = getImgMgr().getImgFormat().getInstance(imgFormatObj);
        try {
          return getImgMgr().getImgScaled()
                            .getOrCreateInstanceByHeight((PersistentRecord) imgOriginal,
                                                         height.intValue(),
                                                         imgFormat);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generate scaled image of height %s, format %s from %s",
                                                 height,
                                                 imgFormat,
                                                 imgOriginal),
                                   e);
        }
      }
    });
    table.addRecordOperation(new DuoRecordOperation<Integer, Double, PersistentRecord>(ATWIDTHASPECT,
                                                                                       Casters.TOINT,
                                                                                       Casters.TODOUBLE) {
      @Override
      public PersistentRecord calculate(TransientRecord imgOriginal,
                                        Viewpoint viewpoint,
                                        Integer width,
                                        Double aspect)
      {
        PersistentRecord replacementImgOriginal = imgOriginal.opt(REPLACEMENTIMGORIGINAL);
        if (replacementImgOriginal != null) {
          return replacementImgOriginal.comp(ATWIDTHASPECT, width, aspect);
        }
        try {
          return getImgMgr().getImgScaled()
                            .getOrCreateInstanceByWidthAspect(imgOriginal.getPersistentRecord(),
                                                              width.intValue(),
                                                              aspect.doubleValue(),
                                                              null);
        } catch (Exception e) {
          throw new LogicException(String.format("Unable to generated scaled image of width %s, aspect %s from %s",
                                                 width,
                                                 aspect,
                                                 imgOriginal));
        }
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<Boolean>(ISDELETEABLE) {
      @Override
      public Boolean getOperation(TransientRecord imgOriginal,
                                  Viewpoint viewpoint)
      {
        return Boolean.valueOf(isDeleteable(imgOriginal));
      }
    });
    table.addRecordSourceOperation(new MonoQueryOrRecordSourceOperation<String, RecordSource>(OWNEDBY,
                                                                                              Casters.TOSTRING,
                                                                                              true) {
      @Override
      protected RecordSource invokeRecordSourceBranch(RecordSource recordSource,
                                                      Viewpoint viewpoint,
                                                      final String a)
      {
        return RecordSources.viewedFrom(new PredicateRecordFilter(recordSource,
                                                                  new Predicate<PersistentRecord>() {
                                                                    @Override
                                                                    public boolean apply(PersistentRecord imgOriginal)
                                                                    {
                                                                      return imgOriginal.comp(ImgOriginal.OWNER)
                                                                                        .startsWith(a);
                                                                    }
                                                                  },
                                                                  null),
                                        viewpoint);
      }

      @Override
      protected RecordSource invokeQueryBranch(Query query,
                                               Viewpoint viewpoint,
                                               String a)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(ImgOriginal.OWNER.getKeyName()).append(" like '");
        SqlUtil.escapeQuotes(buf, a);
        buf.append("%'");
        return RecordSources.viewedFrom(Query.transformQuery(query,
                                                             buf.toString(),
                                                             Query.TYPE_AND,
                                                             null),
                                        viewpoint);
      }
    });
  }

  public boolean isDeleteable(TransientRecord imgOriginal)
  {
    if (ImgOriginal.WEBPATH_DELETED_IMAGE.equals(imgOriginal.comp(ImgOriginal.WEBPATH))) {
      return false;
    }
    if (!imgOriginal.comp(ImgOriginal.WEBPATH)
                    .startsWith(getImgMgr().getImgOriginal().getWebPathDir())) {
      return false;
    }
    File file = imgOriginal.opt(ImgOriginal.FILE);
    if (file == null || !file.canWrite()) {
      return false;
    }
    return true;
  }

  public PersistentRecord createCroppedInstance(PersistentRecord fromImgOriginal,
                                                int x,
                                                int y,
                                                int width,
                                                int height,
                                                PersistentRecord toImgFormat)
      throws IOException, MirrorFieldException, MirrorInstantiationException
  {
    File fromFile = fromImgOriginal.comp(ImgOriginal.FILE);
    DebugConstants.variable("fromFile", fromFile);
    TransientRecord toImgOriginal = getTable().createTransientRecord();
    File toDir = getInstanceDir(toImgOriginal);
    if (!toDir.mkdir()) {
      throw new IOException(String.format("Unable to create instanceDir of %s", toDir));
    }
    boolean isComplete = false;
    try {
      File toFile = ImgScaled.initFile(toDir, toImgFormat.comp(ImgFormat.EXTENSION));
      DebugConstants.variable("toFile", toFile);
      String[] commandArray = { ImageMetaData.imageMagick("convert"), fromFile.toString(), "-crop",
          width + "x" + height + "+" + x + "+" + y, "-gravity", "SouthWest", "+repage",
          toFile.toString() };
      IoUtil.execWithVerboseErrors(commandArray);
      initMetaData(toImgOriginal,
                   ImageMetaData.getInstance(toFile),
                   getImgMgr().fileToWebPath(toFile),
                   toImgFormat);
      toImgOriginal.setField(ImgOriginal.TITLE, fromImgOriginal.comp(ImgOriginal.TITLE));
      toImgOriginal.setField(ImgOriginal.DESCRIPTION,
                             fromImgOriginal.comp(ImgOriginal.DESCRIPTION));
      toImgOriginal.setField(ImgOriginal.MD5, Files.hash(toFile, Hashing.md5()).toString());
      toImgOriginal.setField(ImgOriginal.CROPPEDFROMIMGORIGINAL_ID, fromImgOriginal);
      toImgOriginal.setField(ImgOriginal.OWNER, fromImgOriginal.comp(ImgOriginal.OWNER));
      try {
        JSONObject data = toImgOriginal.comp(ImgOriginal.DATA);
        WritableJSONObject crop = new WritableJSONObject();
        crop.put("x", x);
        crop.put("y", y);
        crop.put("width", width);
        crop.put("height", height);
        data.put("crop", crop);
        // toImgOriginal.setField(ImgOriginal.DATASRC, data);
      } catch (JSONException e) {
        throw new LogicException(e);
      }
      PersistentRecord result = toImgOriginal.makePersistent(null);
      isComplete = true;
      return result;
    } finally {
      if (!isComplete) {
        IoUtil.deleteRecursive(toDir);
      }
    }
  }
  // /**
  // * Calculate the MD5 value for the optional File associated with the ImgOriginal record.
  // *
  // * @return The MD5 checksum, or null if there is no File assocaited with the ImgOriginal record.
  // * @throws IOException
  // * if it is impossible to read the File associated with the ImgOriginal for whatever
  // * reason.
  // */
  // public String calculateMD5(TransientRecord imgOriginal)
  // throws IOException
  // {
  // File file = imgOriginal.opt(FILE);
  // if (file == null) {
  // return null;
  // }
  // return Files.hash(file, Hashing.md5()).toString();
  // }
  //
  // /**
  // * Calculate the MD5 via {@link #calculateMD5(TransientRecord)} and then update the {@link #MD5}
  // * field accordingly. If there is no File associated with the ImgOriginal then the field will be
  // * set to null.
  // *
  // * @return The MD5 checksum, or null if there is no File assocaited with the ImgOriginal record.
  // * @throws IOException
  // * if it is impossible to read the File associated with the ImgOriginal for whatever
  // * reason.
  // * @throws MirrorFieldException
  // * if it is not possible to set the {@link #MD5} field on imgOriginal.
  // */
  // public String updateMD5(TransientRecord imgOriginal)
  // throws IOException, MirrorFieldException
  // {
  // String md5 = calculateMD5(imgOriginal);
  // if (md5 == null) {
  // imgOriginal.setField(MD5, (Object) null);
  // return null;
  // }
  // imgOriginal.setField(MD5, md5);
  // return md5;
  // }
}
