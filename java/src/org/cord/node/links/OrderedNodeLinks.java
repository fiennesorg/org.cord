package org.cord.node.links;

import java.util.Collections;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;

import com.google.common.base.Preconditions;

public class OrderedNodeLinks
{
  private final TreeSet<NodeLink> __nodeLinks;

  private final Boolean __isPublished;

  /**
   * @param isPublished
   *          If null then all NodeLinks are accepted. If not null then only NodeLinks that refer to
   *          nodes that have the defined isPathPublished value matching this value will be
   *          accepted. This makes it possible to define links of either publically visible or
   *          publically invisible nodes.
   */
  public OrderedNodeLinks(NodeLinkComparator ordering,
                          Boolean isPublished)
  {
    __nodeLinks = new TreeSet<NodeLink>(ordering == null ? SortByLabel.getInstance() : ordering);
    __isPublished = isPublished;
  }

  /**
   * Add a new link to this list
   * 
   * @return True if the NodeLink was added, false if it wasn't added either because it was already
   *         present or because the target Node didn't conform to the isPublished rules for this
   *         OrderedNodeLinks
   */
  public boolean addNodeLink(NodeLink nodeLink)
  {
    Preconditions.checkNotNull(nodeLink, "nodeLink");
    if (__isPublished != null
        && !__isPublished.equals(nodeLink.getNode().opt(Node.ISPATHPUBLISHED))) {
      return false;
    }
    return __nodeLinks.add(nodeLink);
  }

  public boolean addNodeLabel(PersistentRecord node,
                              String label)
  {
    return addNodeLink(new NodeLink(node, label));
  }

  public SortedSet<NodeLink> getNodeLinks()
  {
    return Collections.unmodifiableSortedSet(__nodeLinks);
  }

  public Iterator<String> getLinks()
  {
    final Iterator<NodeLink> i = getNodeLinks().iterator();
    return new Iterator<String>() {
      @Override
      public void remove()
      {
        i.remove();
      }

      @Override
      public boolean hasNext()
      {
        return i.hasNext();
      }

      @Override
      public String next()
      {
        return (i.next()).getLink();
      }
    };
  }
}
