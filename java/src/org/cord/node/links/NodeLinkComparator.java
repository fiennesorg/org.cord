package org.cord.node.links;

import java.util.Comparator;

public abstract class NodeLinkComparator
  implements Comparator<NodeLink>
{
  @Override
  public abstract int compare(NodeLink nl0,
                              NodeLink nl1);
}
