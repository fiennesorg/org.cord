package org.cord.node.links;

import org.cord.node.Node;

public class SortByPath
  extends NodeLinkComparator
{
  private static SortByPath INSTANCE = new SortByPath();

  public static SortByPath getInstance()
  {
    return INSTANCE;
  }

  private SortByPath()
  {
  }

  @Override
  public int compare(NodeLink nl0,
                     NodeLink nl1)
  {
    return nl0.getNode().comp(Node.PATH).compareTo(nl1.getNode().comp(Node.PATH));
  }

}
