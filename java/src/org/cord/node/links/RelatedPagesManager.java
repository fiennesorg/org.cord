package org.cord.node.links;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.cord.mirror.PersistentRecord;

import com.google.common.base.Preconditions;

/**
 * The Manager class that is responsible for keeping track of all the RelatedPagesSuppliers and
 * merging their results into definitive RelatedPages lists.
 * 
 * @author alex
 */
public class RelatedPagesManager
{
  private final Set<RelatedPagesSupplier> __suppliers = new LinkedHashSet<RelatedPagesSupplier>();

  private final Set<RelatedPagesSupplier> __roSuppliers = Collections.unmodifiableSet(__suppliers);

  public RelatedPagesManager()
  {
  }

  /**
   * @return true if the supplier was added, false if they were already present
   * @throws NullPointerException
   *           if supplier is null
   */
  public boolean addSupplier(RelatedPagesSupplier supplier)
  {
    Preconditions.checkNotNull(supplier, "supplier");
    return __suppliers.add(supplier);
  }

  /**
   * @return unmodifiable Set of RelatedPagesSupplier
   * @see RelatedPagesSupplier
   */
  public Set<RelatedPagesSupplier> getSuppliers()
  {
    return __roSuppliers;
  }

  /**
   * Query all the registered RelatedPagesSupplier objects as to if they have links relating to the
   * given node and make a compound list of links to them.
   * 
   * @param node
   *          The node that we are interested as to whether or not there are related pages links to.
   * @return Iterator of Strings that are HTML links to a given set of nodes.
   */
  public Iterator<String> getLinks(PersistentRecord node,
                                   NodeLinkComparator ordering,
                                   Boolean isPublished)
  {
    return getNodeLinks(node, ordering, isPublished).getLinks();
  }

  public Iterator<String> getLinks(PersistentRecord node,
                                   NodeLinkComparator ordering)
  {
    return getLinks(node, ordering, Boolean.TRUE);
  }

  public OrderedNodeLinks getNodeLinks(PersistentRecord node,
                                       NodeLinkComparator ordering,
                                       Boolean isPublished)
  {
    OrderedNodeLinks nodeLinks = new OrderedNodeLinks(ordering, isPublished);
    for (RelatedPagesSupplier supplier : getSuppliers()) {
      supplier.addRelatedPages(node, nodeLinks);
    }
    return nodeLinks;
  }

  public OrderedNodeLinks getNodeLinks(PersistentRecord node,
                                       NodeLinkComparator ordering)
  {
    return getNodeLinks(node, ordering, Boolean.TRUE);
  }
}
