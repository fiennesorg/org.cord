package org.cord.node.links;

import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

public final class SortByLabel
  extends NodeLinkComparator
  implements SingletonShutdownable
{
  private static SortByLabel _instance = new SortByLabel();

  public static SortByLabel getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private SortByLabel()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  public int compare(NodeLink nl0,
                     NodeLink nl1)
  {
    return nl0.getLabel().compareTo(nl1.getLabel());
  }
}
