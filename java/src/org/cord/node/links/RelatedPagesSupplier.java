package org.cord.node.links;

import org.cord.mirror.PersistentRecord;

/**
 * An Object that is capable of providing links to a given Node.
 */
public interface RelatedPagesSupplier
{
  public void addRelatedPages(PersistentRecord node,
                              OrderedNodeLinks nodeLinks);
}
