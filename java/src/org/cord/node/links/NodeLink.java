package org.cord.node.links;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class NodeLink
{
  private final PersistentRecord __node;

  private final String __label;

  public NodeLink(PersistentRecord node,
                  String label)
  {
    __node = Preconditions.checkNotNull(node, "node");
    if (Strings.isNullOrEmpty(label)) {
      label = node.opt(Node.TITLE);
      if (Strings.isNullOrEmpty(label)) {
        label = node.opt(Node.URL);
      }
    }
    __label = label;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("node", __node).add("label", __label).toString();
  }

  public String getLabel()
  {
    return __label;
  }

  public PersistentRecord getNode()
  {
    return __node;
  }

  public String getLink()
  {
    return getNode().opt(Node.EXPLICITHREF, getLabel());
  }

  @Override
  public boolean equals(Object arg0)
  {
    if (this == arg0) {
      return true;
    }
    if (!(arg0 instanceof NodeLink)) {
      return false;
    }
    NodeLink nl = (NodeLink) arg0;
    return getLabel().equals(nl.getLabel()) && getNode().equals(nl.getNode());
  }

  @Override
  public int hashCode()
  {
    return Objects.hashCode(getLabel(), getNode());
  }
}
