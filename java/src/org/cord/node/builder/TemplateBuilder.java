package org.cord.node.builder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.cord.mirror.Db;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.node.NodeDb;
import org.cord.node.NodeDbConstants;
import org.cord.node.NodeStyle;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.WebMacro;

/**
 * TemplateBuilder is a utility class that is designed to take a Node database and generate the
 * required number of webmacro templates in a given destination directory. These templates can then
 * be hand-edited by the designer to provide the required look-and-feel.
 */
public class TemplateBuilder
  implements NodeDbConstants
{
  private final Db _db;

  private final WebMacro _webmacro;

  private final File _outputDirectory;

  private final String _viewTemplateTemplatePath;

  private final String _editTemplateTemplatePath;

  /**
   * @param db
   *          The Db that is going to be converted into templates.
   * @param webmacro
   *          An instance of webmacro that is initialised with the same configuration as that used
   *          by the web-server. This is used to determine whether or not a required template
   *          actually exists in the TemplatePath of the system.
   * @param outputDirectory
   *          The directory that the TemplateBuilder will utilise as the root of the TemplatePath
   *          for the purposes of writing out new templates. This may or may not be the same as the
   *          physical template path.
   * @param viewTemplateTemplatePath
   *          The template path to the webmacro template that is to be used to generate the
   *          viewTemplates (think about it...)
   * @param editTemplateTemplatePath
   *          The template path to the webmacro template that is to be used to generate the
   *          editTemplates.
   * @throws IllegalArgumentException
   *           If db has not had the NodeDb DbInitialiser installed on it.
   */
  public TemplateBuilder(Db db,
                         WebMacro webmacro,
                         File outputDirectory,
                         String viewTemplateTemplatePath,
                         String editTemplateTemplatePath)
      throws IOException, ResourceException, PropertyException
  {
    if (!db.getDbInitialisers().containsKey(NodeDb.NAME)) {
      throw new IllegalArgumentException(db + " is not a NodeDb");
    }
    _db = db;
    _webmacro = webmacro;
    _outputDirectory = outputDirectory;
    _viewTemplateTemplatePath = viewTemplateTemplatePath;
    _editTemplateTemplatePath = editTemplateTemplatePath;
    generateTemplates();
  }

  private boolean isWebmacroTemplate(String templatePath)
  {
    try {
      _webmacro.getTemplate(templatePath);
      return true;
    } catch (ResourceException badTemplatePathEx) {
      return false;
    }
  }

  private boolean isGeneratedTemplate(String templatePath)
  {
    File templateFile = new File(_outputDirectory, templatePath);
    return templateFile.exists();
  }

  private boolean isTemplate(String templatePath)
  {
    return isWebmacroTemplate(templatePath) || isGeneratedTemplate(templatePath);
  }

  private void generateTemplate(PersistentRecord nodeStyle,
                                boolean isViewTemplate)
      throws IOException, ResourceException, PropertyException
  {
    String templatePath = isViewTemplate
        ? nodeStyle.opt(NodeStyle.VIEWTEMPLATEPATH)
        : nodeStyle.opt(NodeStyle.EDITTEMPLATEPATH);
    if (isTemplate(templatePath)) {
      return;
    }
    Context context = _webmacro.getContext();
    context.put("db", _db);
    context.put("nodeStyle", nodeStyle);
    Template templateTemplate = _webmacro.getTemplate(isViewTemplate
        ? _viewTemplateTemplatePath
        : _editTemplateTemplatePath);
    String generatedTemplate = templateTemplate.evaluateAsString(context);
    File templateFile = new File(_outputDirectory, templatePath);
    FileWriter fileWriter = new FileWriter(templateFile);
    fileWriter.write(generatedTemplate);
    fileWriter.close();
  }

  private void generateTemplates()
      throws IOException, ResourceException, PropertyException
  {
    Table nodeStyleTable = _db.getTable(NodeStyle.TABLENAME);
    for (PersistentRecord nodeStyle : nodeStyleTable) {
      generateTemplate(nodeStyle, true);
      generateTemplate(nodeStyle, false);
    }
  }
}
