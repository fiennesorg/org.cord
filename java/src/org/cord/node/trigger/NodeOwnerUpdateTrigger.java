package org.cord.node.trigger;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.node.Node;
import org.cord.node.NodeStyle;
import org.cord.util.LogicException;

/**
 * FieldTrigger that is responsible for disseminating any updates to Node.owner_id fields to the
 * children Nodes if so required.
 */
public class NodeOwnerUpdateTrigger
  implements FieldTrigger<Integer>
{
  @Override
  public void shutdown()
  {
  }

  @Override
  public final FieldKey<Integer> getFieldName()
  {
    return Node.OWNER_ID;
  }

  @Override
  public final boolean handlesTransientRecords()
  {
    return false;
  }

  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
  {
    throw new LogicException("NodeOwnerUpdateTrigger does not support TransientRecords");
  }

  @Override
  public Object triggerField(WritableRecord node,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    switch (nodeStyle.compInt(NodeStyle.OWNER_ID)) {
      case -1:
      case 0:
        // both of these cases support owner setting and therefore we
        // continue...
        break;
      default:
        // any other value of NodeStyle.owner_id implies an immutable
        // value and we should freak-out if it has changed...
        if (nodeStyle.compInt(NodeStyle.OWNER_ID) != node.compInt(Node.OWNER_ID)) {
          throw new MirrorFieldException(node + " does not support ownership modification",
                                         null,
                                         node,
                                         Node.OWNER_ID,
                                         node.opt(Node.OWNER_ID),
                                         nodeStyle.opt(NodeStyle.OWNER_ID));
        }
    }
    // if we've got this far then we support the change and therefore
    // we should try and disseminate it to any children Nodes (if they
    // implement inherited ownership).
    for (PersistentRecord childNode : node.opt(Node.UNSORTEDCHILDRENNODES, transaction)) {
      PersistentRecord childNodeStyle = childNode.comp(Node.NODESTYLE);
      switch (childNodeStyle.compInt(NodeStyle.OWNER_ID)) {
        case -1:
          // inherited ownership so try and set the ownership of the Node...
          WritableRecord editableChildNode = null;
          try {
            editableChildNode = transaction.edit(childNode, Db.DEFAULT_TIMEOUT);
          } catch (MirrorTransactionTimeoutException cannotLockChildEx) {
            throw new MirrorFieldException("Unable to obtain lock on " + childNode
                                           + " to permit ownership inheritance",
                                           cannotLockChildEx,
                                           node,
                                           Node.OWNER_ID,
                                           node.opt(Node.OWNER_ID),
                                           null);
          }
          editableChildNode.setField(Node.OWNER_ID, Integer.valueOf(node.compInt(Node.OWNER_ID)));
          break;
        default:
          // do nothing as not inherited...
      }
    }
    return null;
  }

  /**
   * @return false;
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
