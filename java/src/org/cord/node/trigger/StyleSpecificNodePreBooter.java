package org.cord.node.trigger;

import java.util.Iterator;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.node.NodeStyle;
import org.cord.util.Gettable;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;

/**
 * PreInstantiationTrigger that is registered on the Node Table and which provides the ability for
 * triggers to be associated with instances of a particular NodeStyle.
 */
public class StyleSpecificNodePreBooter
  implements PreInstantiationTrigger
{
  private SetMultimap<Integer, PreInstantiationTrigger> _triggers = LinkedHashMultimap.create();

  // private MapToList<Integer, PreInstantiationTrigger> _triggers = new
  // MapToList<Integer, PreInstantiationTrigger>();

  private Db _db;

  public StyleSpecificNodePreBooter(Db db)
  {
    _db = db;
  }

  // public void lock()
  // {
  // _triggers.lock();
  // }

  @Override
  public void shutdown()
  {
    // _triggers.clear();
    // _triggers = null;
    _db = null;
  }

  public boolean register(int nodeStyleId,
                          PreInstantiationTrigger trigger)
  {
    return _triggers.put(Integer.valueOf(nodeStyleId), trigger);
    // return _triggers.addUniqueValue(nodeStyle, trigger);
  }

  public boolean register(String styleName,
                          PreInstantiationTrigger trigger)
      throws MirrorNoSuchRecordException
  {
    return register(RecordSources.getFirstRecord(TableStringMatcher.search(_db.getTable(NodeStyle.TABLENAME),
                                                                           null,
                                                                           NodeStyle.NAME,
                                                                           styleName,
                                                                           null,
                                                                           TableStringMatcher.SEARCH_EXACT),
                                                 null)
                                 .getId(),
                    trigger);
  }

  private Iterator<PreInstantiationTrigger> getTriggers(TransientRecord node)
  {
    Set<PreInstantiationTrigger> triggers = _triggers.get(node.opt(Node.NODESTYLE_ID));
    if (triggers == null) {
      return ImmutableSet.<PreInstantiationTrigger> of().iterator();
    }
    return triggers.iterator();
  }

  @Override
  public void releasePre(TransientRecord node,
                         Gettable params)
  {
    Iterator<PreInstantiationTrigger> triggers = getTriggers(node);
    while (triggers.hasNext()) {
      triggers.next().releasePre(node, null);
    }
  }

  @Override
  public void rollbackPre(TransientRecord node,
                          Gettable params)
      throws MirrorException
  {
    Iterator<PreInstantiationTrigger> triggers = getTriggers(node);
    while (triggers.hasNext()) {
      triggers.next().rollbackPre(node, null);
    }
  }

  @Override
  public void triggerPre(TransientRecord node,
                         Gettable params)
      throws MirrorException
  {
    Iterator<PreInstantiationTrigger> triggers = getTriggers(node);
    while (triggers.hasNext()) {
      triggers.next().triggerPre(node, null);
    }
  }
}
