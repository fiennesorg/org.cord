package org.cord.node.trigger;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.Transaction;
import org.cord.node.ContentCompiler;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeBooter;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.util.Gettable;
import org.cord.util.NoSuchNamedException;

/**
 * PostInstantiationTrigger for Node records that creates Group records and ContentCompilers as
 * defined in the style for this record.
 * <P>
 * The following operations are performed:-
 * <UL>
 * <LI>Get the list of GroupStyle records that are children of the corresponding NodeStyle for this
 * record.
 * <LI>Create a new transient Group record for each GroupStyle record. Perform the following
 * operations on the GroupStyle record:-
 * <UL>
 * <LI>Set the GROUP_NODE_ID field to point at the Node.
 * <LI>Set the GROUP_CREATECHILDACLNAME to GROUPSTYLE_CREATECHILDACLNAME
 * <LI>Set GROUP_NUMBER to GROUPSTYLE_NUMBER
 * <LI>Set GROUP_STYLE_ID to GROUP_STYLE_ID
 * <LI>Make the Group persistent. At this point, any InstantiationTriggers on the Group will kick in
 * nicely.
 * </UL>
 * <LI>Get a list of all the Region records from the NodeStyle record via NODESTYLE_REGIONS
 * <LI>Request the appropriate ContentCompiler to create an instance for each region in the Node.
 * Please note that the implementation and side-effects of this process is completed domain of this
 * class.
 * </UL>
 */
public class NodePostBooter
  extends NodeBooter
  implements PostInstantiationTrigger
{
  public NodePostBooter(NodeManager nodeManager,
                        String transactionPassword)
  {
    super(nodeManager,
          transactionPassword);
  }

  @Override
  public void triggerPost(PersistentRecord node,
                          Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      NoSuchNamedException, MirrorNoSuchRecordException, FeedbackErrorException
  {
    try (
        Transaction transaction = getNodeManager().getDb()
                                                  .createTransaction(Db.DEFAULT_TIMEOUT,
                                                                     Transaction.TRANSACTION_UPDATE,
                                                                     getPwd(),
                                                                     "NodePostBooter")) {
      PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
      for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES)) {
        ContentCompiler contentCompiler =
            getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
        Gettable namespacedParams =
            getNodeManager().getRegionStyle().getNamespaceGettable(regionStyle, params);
        contentCompiler.initialiseInstance(node, regionStyle, namespacedParams, null, transaction);
      }
      for (PersistentRecord nodeGroupStyle : nodeStyle.opt(NodeStyle.CHILDRENNODEGROUPSTYLES)) {
        PersistentRecord childNodeStyle = nodeGroupStyle.comp(NodeGroupStyle.CHILDNODESTYLE);
        int minimum = (nodeGroupStyle.opt(NodeGroupStyle.MINIMUM)).intValue();
        String[] filenames = NodeStyle.getFilenames(childNodeStyle.opt(NodeStyle.FILENAME));
        if (minimum == -1) {
          minimum = filenames.length;
        }
        for (int number = 0; number < minimum; number++) {
          String filename =
              NodeStyle.calculateFilename(filenames, number, childNodeStyle.opt(NodeStyle.NAME));
          getNodeManager().getNode()
                          .createNode(node.getId(),
                                      nodeGroupStyle.getId(),
                                      filename,
                                      filename,
                                      filename,
                                      childNodeStyle.is(NodeStyle.DEFAULTISPUBLISHED),
                                      null);
        }
      }
      transaction.commit();
    }
  }

  @Override
  public void rollbackPost(PersistentRecord node,
                           Gettable params)
      throws MirrorNoSuchRecordException, MirrorTransactionTimeoutException, NoSuchNamedException
  {
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    for (PersistentRecord regionStyle : nodeStyle.opt(NodeStyle.REGIONSTYLES)) {
      ContentCompiler contentCompiler =
          getNodeManager().getRegionStyle().getContentCompiler(regionStyle);
      contentCompiler.destroyInstance(node, regionStyle);
    }
  }

  @Override
  public void releasePost(PersistentRecord node,
                          Gettable params)
  {
  }

  @Override
  public String toString()
  {
    return "NodePostBooter";
  }
}
