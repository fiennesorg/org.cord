package org.cord.node.trigger;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Transaction;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.util.Gettable;

/**
 * This trigger takes action when a new NodeGroupStyle is registered on the system and is
 * responsible for creating any implementations of NodeGroup that should be created according to the
 * current state of the Db. A NodeGroup is normally created at the point of creation of the parent
 * Node, therefore we need to work out the existing Nodes that have been created which now need an
 * additional NodeGroup registering on them. This list is given by:-
 * <blockquote>$nodeGroupStyle.parentNodeStyle.nodes</blockquote>
 * <p>
 * Each of these Nodes has a new NodeGroup initialised according to this NodeGroupStyle via the
 * NodePostGroup creation functionality. If the action is rollbacked for whatever reason, then all
 * NodeGroups that have been initialised that implement this NodeGroupStyle will be deleted.
 */
public class NodeGroupStylePostBooter
  implements PostInstantiationTrigger
{
  private NodeManager _nodeManager;

  private Db _db;

  private String _transactionPassword;

  public NodeGroupStylePostBooter(NodeManager nodeManager,
                                  String transactionPassword)
  {
    _nodeManager = nodeManager;
    _db = nodeManager.getDb();
    _transactionPassword = transactionPassword;
  }

  public NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  @Override
  public void shutdown()
  {
    _db = null;
    _transactionPassword = null;
  }

  @Override
  public void triggerPost(PersistentRecord nodeGroupStyle,
                          Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    int minimum = nodeGroupStyle.compInt(NodeGroupStyle.MINIMUM);
    if (minimum > 0) {
      PersistentRecord parentNodeStyle = nodeGroupStyle.comp(NodeGroupStyle.PARENTNODESTYLE);
      PersistentRecord childNodeStyle = nodeGroupStyle.comp(NodeGroupStyle.CHILDNODESTYLE);
      for (PersistentRecord parentNode : parentNodeStyle.opt(NodeStyle.NODES)) {
        getNodeManager().getNode()
                        .createNode(parentNode.getId(),
                                    nodeGroupStyle.getId(),
                                    null,
                                    null,
                                    null,
                                    childNodeStyle.is(NodeStyle.DEFAULTISPUBLISHED),
                                    null);
      }
    }
  }

  @Override
  public void rollbackPost(PersistentRecord nodeGroupStyle,
                           Gettable params)
      throws MirrorTransactionTimeoutException
  {
    RecordSource nodesQuery = nodeGroupStyle.opt(NodeGroupStyle.NODES);
    if (nodesQuery.getIdList().size() == 0) {
      return;
    }
    try (Transaction t = _db.createTransaction(Db.DEFAULT_TIMEOUT,
                                               Transaction.TRANSACTION_DELETE,
                                               _transactionPassword,
                                               "NodeGroupStylePostBooter.rollback(...)")) {
      t.delete(nodesQuery, Db.DEFAULT_TIMEOUT);
      t.commit();
    }
  }

  @Override
  public void releasePost(PersistentRecord nodeGroupStyle,
                          Gettable params)
  {
  }
}
