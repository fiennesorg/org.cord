package org.cord.node.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.LinkField;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.Gettable;

import com.google.common.base.MoreObjects;

/**
 * PreInstantiationTrigger that boots a LinkFieldFilter on a field in an TableContentCompiler to
 * point at the id of a named TableContentCompiler on the parent Node.
 * 
 * @see TableContentCompiler
 * @see PreInstantiationTrigger
 * @see LinkField
 */
public class ParentReferenceBooter
  implements PreInstantiationTrigger
{
  private NodeManager _nodeManager;

  private final int __generationCount;

  private final FieldKey<?> __instanceFieldName;

  private final String __parentRegionStyleName;

  private final RecordOperationKey<?> __parentRegionFieldName;

  private final boolean __isOptional;

  /**
   * Standard constructor. <br />
   * $this.instanceFieldName = $this.region.node.parentNode.(n*parentNode).region
   * .parentRegionName.compile.parentRegionFieldName
   * 
   * @param instanceFieldName
   *          The name of the child TableContentCompiler LinkFieldFilter that this trigger is going
   *          to initialise.
   * @param generationCount
   *          The number of times that parentNode will be invoked before trying to resolve
   *          parentRegionName. The default value should be 1 which resolves against the parentNode.
   *          A value of 2 will resolve against the grand parent etc etc...
   * @param parentRegionStyleName
   *          The name of the region on the parent Node that will be used to supply the id of the
   *          target of the instanceFieldName link.
   * @param parentRegionFieldName
   *          The name of the field on the parentNode region that is going to be taken as the value
   *          for instanceFieldName. If this is not defined then a value of "id" is taken as a
   *          default. If the contents of the defined field are not compatible with the format of
   *          instanceFieldName then the trigger will fail.
   * @param isOptional
   *          if true then errors in the init process are transparently ignored, otherwise the
   *          trigger method may throw exceptions.
   */
  public ParentReferenceBooter(NodeManager nodeManager,
                               FieldKey<?> instanceFieldName,
                               int generationCount,
                               String parentRegionStyleName,
                               RecordOperationKey<?> parentRegionFieldName,
                               boolean isOptional)
  {
    _nodeManager = nodeManager;
    __instanceFieldName = instanceFieldName;
    __generationCount = Math.max(1, generationCount);
    __parentRegionStyleName = parentRegionStyleName;
    __parentRegionFieldName =
        MoreObjects.firstNonNull(parentRegionFieldName, TransientRecord.FIELD_ID);
    __isOptional = isOptional;
  }

  /**
   * Constructor that resolves against the immediate parent's id field.
   * 
   * @param instanceFieldName
   *          The name of the child TableContentCompiler LinkFieldFilter that this trigger is going
   *          to initialise.
   * @param parentRegionStyleName
   *          The name of the RegionStyle on the parent Node that will be used to supply the id of
   *          the target of the instanceFieldName link.
   * @param isOptional
   *          if true then errors in the init process are transparently ignored, otherwise the
   *          trigger method may throw exceptions.
   */
  public ParentReferenceBooter(NodeManager nodeManager,
                               FieldKey<Integer> instanceFieldName,
                               String parentRegionStyleName,
                               boolean isOptional)
  {
    this(nodeManager,
         instanceFieldName,
         1,
         parentRegionStyleName,
         null,
         isOptional);
  }

  private String _toString = null;

  @Override
  public String toString()
  {
    if (_toString == null) {
      StringBuilder buf = new StringBuilder();
      buf.append("this.").append(__instanceFieldName);
      buf.append("=");
      buf.append("this.node.");
      for (int i = 0; i < __generationCount; i++) {
        buf.append("parentNode.");
      }
      buf.append("compile.")
         .append(__parentRegionStyleName)
         .append(".")
         .append(__parentRegionFieldName);
      _toString = buf.toString();
    }
    return _toString;
  }

  protected NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  @Override
  public void triggerPre(TransientRecord instance,
                         Gettable params)
      throws MirrorNoSuchRecordException, MirrorFieldException
  {
    PersistentRecord instanceNode = TableContentCompiler.getNode(instance);
    PersistentRecord parentNode = instanceNode.comp(Node.PARENTNODE);
    for (int i = 1; i < __generationCount; i++) {
      parentNode = parentNode.comp(Node.PARENTNODE);
    }
    try {
      PersistentRecord parentInstance =
          (PersistentRecord) (getNodeManager().getNode().compile(parentNode,
                                                                 __parentRegionStyleName,
                                                                 null,
                                                                 null));
      instance.setField(__instanceFieldName, parentInstance.opt(__parentRegionFieldName));
    } catch (MirrorNoSuchRecordException mnsrEx) {
      if (!__isOptional) {
        throw mnsrEx;
      }
    }
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  @Override
  public void releasePre(TransientRecord contactOrganisationItem,
                         Gettable params)
  {
  }

  @Override
  public void rollbackPre(TransientRecord contactOrganisationItem,
                          Gettable params)
  {
  }
}
