package org.cord.node.trigger;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.User;
import org.cord.util.Gettable;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * Cluster of classes that handle the creation and resolution of Nodes that act as the Homepage for
 * individual Users. The following items of functionality are provided:-
 * <ul>
 * <li>The ability to auto create the Node that is to be linked to the User on the creation of the
 * User record. No responsibilities for the contents of the Node outside the filename and the
 * owner_id are taken by this operation and it is up to the definition of the NodeStyle to set how
 * the Homepage should behave. However, I would recommend:-
 * <ul>
 * <li>Make the view permissions to only permit the owner of the Node to view the Node. This will
 * ensure that homepage information remains private to the targetted User. Obviously the site admin
 * will also require access.
 * <li>Do not grant permission to delete the Homepage. This should be taken care of by deleting the
 * User instead.
 * <li>Make the Homepage default to published and do not grant permission to unpublish it unless so
 * required.
 * </ul>
 * <li>Check to see whether or not a User has a homepage via $user.hasHomepage.
 * <li>Get the Homepage Node of a User via $user.homepage.
 * </ul>
 * <p>
 * The UserHomepage class itself can be thought of as a binding point for all the sub-elements
 * described above. If you wish to use the class then all you need to do is:-
 * <ul>
 * <li>Ensure that you have a Singleton NodeGroupStyle that contains a NodeStyle to represent the
 * Homepages for the users. The NodeStyle should be set up as described above.
 * <li>Register whatever content regions are required for the Homepage on the given NodeStyle.
 * <li>Create the UserHomepage object which will automatically register all of the above components,
 * thereby making an instance of the appropriate Homepage Node appear whenever a new User is
 * created.
 * </ul>
 * 
 * @see org.cord.node.view.user.AddUser
 */
public class UserHomepage
{
  public final static RecordOperationKey<PersistentRecord> HOMEPAGE =
      RecordOperationKey.create("homepage", PersistentRecord.class);

  public final static ObjectFieldKey<Boolean> HASHOMEPAGE = BooleanField.createKey("hasHomepage");

  private NodeManager _nodeManager;

  private final String __transactionPassword;

  private final int __homepageParentNodeId;

  private final int __homepageNodeGroupStyleId;

  private final IntSet __nonHomepageUserIds = new IntOpenHashSet();

  /**
   * Factory method to create the UserHomepage system when the NodeGroup id is not known and it
   * should be deduced from the NodeGroupStyle that it implements (which will generally be known at
   * compile time). The act if invoking this method will automatically perform all registrations
   * that are required before the UserHomepage is returned.
   * 
   * @return The constructed UserHomepage object.
   * @param homepageNodeGroupStyleId
   *          The id of the NodeGroupStyle that controls the NodeGroup that will be the parent of
   *          the created Homepage Nodes. There should be at least one instance of NodeGroup that
   *          implements this NodeGroupStyle when the method is invoked. If there is more than one
   *          NodeGroup then the system will utilise the first one, however you should have a very
   *          good reason for setting the system up in this fashion as it is not recommended and
   *          probably wrong!
   * @param homepageAutoCreates
   *          If true then the $user.homepage method will create missing homepages automatically.
   * @throws MirrorNoSuchRecordException
   *           If the specified homepageNodeGroupStyleId is not found in the NodeGroupStyle Table,
   *           or if there is not at least one NodeGroup that implements the given NodeGroupStyle.
   */
  public static UserHomepage getInstance(NodeManager nodeManager,
                                         String transactionPassword,
                                         int homepageParentNodeStyleId,
                                         int homepageNodeGroupStyleId,
                                         boolean homepageAutoCreates)
      throws MirrorNoSuchRecordException, MirrorTableLockedException
  {
    int homepageParentNodeId = nodeManager.getNodeStyle().getFirstNodeId(homepageParentNodeStyleId);
    return new UserHomepage(nodeManager,
                            transactionPassword,
                            homepageParentNodeId,
                            homepageNodeGroupStyleId,
                            homepageAutoCreates);
  }

  /**
   * @param homepageAutoCreates
   *          If true then the $user.homepage method will create missing homepages automatically.
   * @see #getInstance(NodeManager, String, int, int, boolean)
   */
  public UserHomepage(NodeManager nodeManager,
                      String transactionPassword,
                      int homepageParentNodeId,
                      int homepageNodeGroupStyleId,
                      boolean homepageAutoCreates)
      throws MirrorTableLockedException
  {
    _nodeManager = nodeManager;
    __transactionPassword = transactionPassword;
    __homepageParentNodeId = homepageParentNodeId;
    __homepageNodeGroupStyleId = homepageNodeGroupStyleId;
    Table userTable = _nodeManager.getDb().getTable(User.TABLENAME);
    userTable.addPostInstantiationTrigger(new CreateUserTrigger());
    userTable.addRecordOperation(new HasHomepage());
    userTable.addRecordOperation(new Homepage(homepageAutoCreates));
    __nonHomepageUserIds.add(TransientRecord.ID_TRANSIENT);
  }

  /**
   * Inform the system that the given User is exempt from the Homepage creation system. If the given
   * user already has a homepage then it will be automatically removed.
   * 
   * @throws MirrorTransactionTimeoutException
   *           If there was an unresolved contested lock during the deletion of an existing homepage
   *           (unlikely).
   */
  public void addNonHomepageUser(TransientRecord user)
      throws MirrorTransactionTimeoutException
  {
    if (user != null && !__nonHomepageUserIds.contains(user.getId())) {
      deleteHomepage(user);
      __nonHomepageUserIds.add(user.getId());
    }
  }

  /**
   * Sweep across all Users and initialise any Homepages that have not currently been initialised.
   */
  public void initialiseMissingHomepages()
      throws MirrorInstantiationException, MirrorFieldException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    for (PersistentRecord user : _nodeManager.getUser().getTable()) {
      createHomepage(user);
    }
  }

  /**
   * Get the Query that resolves to all the Nodes under the specified homepageNodeGroupId which are
   * owned by user.
   */
  public RecordSource getHomepageNodes(TransientRecord user)
  {
    return _nodeManager.getNode()
                       .getTable()
                       .getQuery(null,
                                 Node.PARENTNODE_ID + "=" + __homepageParentNodeId + " and "
                                       + Node.OWNER_ID + "=" + user.getId(),
                                 null);
  }

  /**
   * Create the appropriate Homepage Node for the user. This will only be performed if user is a
   * PersistentRecord and there is not already a homepage existing for the user.
   * 
   * @throws MirrorInstantiationException
   *           If the newly created Homepage fails during the creation process.
   * @throws MirrorFieldException
   *           If the parentNodeGroup_id, filename or owner_id field are rejected during
   *           initialisation of the Homepage.
   */
  public PersistentRecord createHomepage(TransientRecord user)
      throws MirrorInstantiationException, MirrorFieldException, MirrorTransactionTimeoutException,
      MirrorNoSuchRecordException
  {
    if (__nonHomepageUserIds.contains(user.getId())) {
      // no homepage permitted for this user...
      return null;
    }
    RecordSource homepageNodes = getHomepageNodes(user);
    PersistentRecord homepageNode = RecordSources.getOptFirstRecord(homepageNodes, null);
    if (homepageNode != null) {
      return homepageNode;
    }
    return _nodeManager.getNode().createNode(__homepageParentNodeId,
                                             __homepageNodeGroupStyleId,
                                             Integer.valueOf(user.getId()),
                                             user.opt(User.LOGIN),
                                             null,
                                             null,
                                             null,
                                             null);
  }

  private void deleteHomepage(TransientRecord user)
      throws MirrorTransactionTimeoutException
  {
    RecordSource homepageNodes = getHomepageNodes(user);
    if (homepageNodes.getIdList().size() > 0) {
      try (Transaction transaction =
          (_nodeManager.getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                  Transaction.TRANSACTION_DELETE,
                                                  __transactionPassword,
                                                  "UserHomepage rollback"))) {
        transaction.delete(homepageNodes, Db.DEFAULT_TIMEOUT);
        transaction.commit();
      }
    }
  }

  /**
   * RecordOperation on User ($user.hasHomepage) that returns true if the User has an initialised
   * homepage.
   */
  public class HasHomepage
    extends SimpleRecordOperation<Boolean>
  {
    /**
     * @see UserHomepage#HASHOMEPAGE
     */
    public HasHomepage()
    {
      super(HASHOMEPAGE);
    }

    @Override
    public Boolean getOperation(TransientRecord user,
                                Viewpoint viewpoint)
    {
      return Boolean.valueOf(getHomepageNodes(user).getIdList().size() > 0);
    }
  }

  /**
   * RecordOperation on User ($user.homepage) that returns the PersistentRecord Node that acts as
   * the Users homepage, optionally creating the homepage if necessary.
   */
  public class Homepage
    extends SimpleRecordOperation<PersistentRecord>
  {
    private final boolean __autoCreateHomepages;

    /**
     * @param autoCreateHomepages
     *          if true the invoking $user.homepage on a User that does not yet have a homepage
     *          created will try and initialise the homepage automatically. if false then invoking
     *          $user.homepage on a non-initiliased User will result in null and no homepage being
     *          created.
     * @see UserHomepage#HOMEPAGE
     */
    public Homepage(boolean autoCreateHomepages)
    {
      super(HOMEPAGE);
      __autoCreateHomepages = autoCreateHomepages;
    }

    /**
     * @return The Node PersistentRecord that corresponds to the invoking user's homepage, or null
     *         if the homepage does not exist.
     */
    @Override
    public PersistentRecord getOperation(TransientRecord user,
                                         Viewpoint viewpoint)
    {
      try {
        return getHomepageNodes(user).getRecordList(viewpoint).get(0);
      } catch (MissingRecordException missingHomepageEx) {
        if (__autoCreateHomepages) {
          try {
            return createHomepage(user);
          } catch (MirrorException initialisationFailureEx) {
            return null;
          }
        } else {
          return null;
        }
      }
    }
  }

  /**
   * PostInstantiationTrigger registered on User that automatically creates a Homepage for the User
   * once the record has been created. Please note that a corresponding delete linkage is not
   * required because the declaration of $node.owner_id is declared as LINK_ENFORCED therefore
   * attempting to delete the User will automatically delete the Homepages as well.
   */
  public class CreateUserTrigger
    implements PostInstantiationTrigger
  {
    @Override
    public void shutdown()
    {
      _nodeManager = null;
    }

    /**
     * Do nothing as there is no temporary storage of assets associated with user.
     */
    @Override
    public void releasePost(PersistentRecord user,
                            Gettable params)
    {
    }

    /**
     * Delete any of the homepage Nodes that where associated with user.
     */
    @Override
    public void rollbackPost(PersistentRecord user,
                             Gettable params)
        throws MirrorTransactionTimeoutException
    {
      deleteHomepage(user);
    }

    /**
     * Create the homepage for the user as required.
     * 
     * @see UserHomepage#createHomepage(TransientRecord)
     */
    @Override
    public void triggerPost(PersistentRecord user,
                            Gettable params)
        throws MirrorInstantiationException, MirrorFieldException,
        MirrorTransactionTimeoutException, MirrorNoSuchRecordException
    {
      createHomepage(user);
    }
  }
}
