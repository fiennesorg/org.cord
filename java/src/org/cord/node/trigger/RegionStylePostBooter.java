package org.cord.node.trigger;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostInstantiationTrigger;
import org.cord.node.ContentCompiler;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.node.RegionStyle;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

/**
 * PostInstantiationTrigger that is responsible for the creation of any required Region records on a
 * running system.
 */
public class RegionStylePostBooter
  implements PostInstantiationTrigger
{
  private NodeManager _nodeManager;

  public RegionStylePostBooter(NodeManager nodeManager)
  {
    _nodeManager = nodeManager;
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  @Override
  public void triggerPost(PersistentRecord regionStyle,
                          Gettable params)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTableLockedException,
      MirrorNoSuchRecordException, MirrorTransactionTimeoutException, FeedbackErrorException
  {
    ContentCompiler contentCompiler = _nodeManager.getRegionStyle().getContentCompiler(regionStyle);
    contentCompiler.declare(regionStyle);
    PersistentRecord nodeStyle = regionStyle.comp(RegionStyle.NODESTYLE);
    Gettable namespacedParams =
        _nodeManager.getRegionStyle().getNamespaceGettable(regionStyle, params);
    for (PersistentRecord node : nodeStyle.opt(NodeStyle.NODES)) {
      // TODO: (#258) RegionStylePostBooter doesn't create a delegating
      // Transaction for
      // initialisation of ContentCompilers.
      contentCompiler.initialiseInstance(node, regionStyle, namespacedParams, null, null);
    }
  }

  @Override
  public void rollbackPost(PersistentRecord regionStyle,
                           Gettable params)
      throws MirrorTransactionTimeoutException
  {
    ContentCompiler contentCompiler = _nodeManager.getRegionStyle().getContentCompiler(regionStyle);
    // TODO (#265) make sure that there is an option for dereferencing a
    // ContentCompiler....
    PersistentRecord nodeStyle = regionStyle.comp(RegionStyle.NODESTYLE);
    for (PersistentRecord node : nodeStyle.opt(NodeStyle.NODES)) {
      try {
        contentCompiler.destroyInstance(node, regionStyle);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new LogicException("Failed to resolve ContentCompiler assets for " + regionStyle
                                 + " on " + node,
                                 mnsrEx);
      }
    }
  }

  @Override
  public void releasePost(PersistentRecord regionStyle,
                          Gettable params)
  {
  }
}
