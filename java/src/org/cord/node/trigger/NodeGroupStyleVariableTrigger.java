package org.cord.node.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.NodeGroupStyle;
import org.cord.util.NotImplementedException;

/**
 * FieldTrigger on NodeGroupStyle that links a named field on all implementing NodeGroups to the
 * NodeGroupStyle. FieldTrigger on NodeGroupStyle that enforces the relationship of
 * $nodeGroupStyle.regions.&lt;fieldName&gt;==$nodeGroupStyle .&lt;fieldName&gt;. This will be
 * utilised on the NODEGROUPSTYLE_NAME and NODEGROUPSTYLE_NUMBER fields.
 * 
 * @see NodeGroupStyle#NAME
 * @see NodeGroupStyle#NUMBER
 */
public class NodeGroupStyleVariableTrigger<T>
  extends AbstractFieldTrigger<T>
{
  // private final boolean __onlyDefaultValues;
  /**
   * @param fieldName
   *          The name of the field on NodeGroupStyle that will cause this FieldTrigger to fire.
   * @param onlyDefaultValues
   *          if true then the only NodeGroup values that will be updated will be the values that
   *          are unchanged from the previous NodeGroupStyle value. This is mainly used for ACL ids
   *          when we only want the default values to be affected by changes to the default.
   */
  public NodeGroupStyleVariableTrigger(FieldKey<T> fieldName,
                                       boolean onlyDefaultValues)
  {
    super(fieldName,
          false,
          false);
    // __onlyDefaultValues = onlyDefaultValues;
  }

  public NodeGroupStyleVariableTrigger(FieldKey<T> fieldName)
  {
    this(fieldName,
         false);
  }

  @Override
  public Object triggerField(TransientRecord nodeGroupStyle,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    // Object originalValue = (__onlyDefaultValues
    // ? ((PersistentRecord)
    // nodeGroupStyle).getPersistentRecord().getField(fieldName)
    // : null);
    // Object updatedValue = nodeGroupStyle.getField(fieldName);
    throw new NotImplementedException();
    // Iterator nodeGroups =
    // nodeGroupStyle.followReferences(NodeGroupStyle.NODEGROUPS);
    // while (nodeGroups.hasNext()) {
    // PersistentRecord nodeGroup = (PersistentRecord) nodeGroups.next();
    // if (! __onlyDefaultValues
    // ||
    // nodeGroup.getPersistentRecord().getField(fieldName).equals(originalValue))
    // {
    // nodeGroup.setField(fieldName,
    // updatedValue);
    // }
    // }
    // return null;
  }
}
