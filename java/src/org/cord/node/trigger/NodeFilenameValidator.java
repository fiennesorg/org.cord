package org.cord.node.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.node.FilenameGenerator;
import org.cord.node.Node;
import org.cord.node.NodeStyle;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Field Trigger on "filename" that keeps track of auto-generated filenames.
 */
public class NodeFilenameValidator
  implements FieldTrigger<String>
{
  private FilenameGenerator _filenameGenerator;

  public NodeFilenameValidator(FilenameGenerator filenameGenerator)
  {
    _filenameGenerator = Preconditions.checkNotNull(filenameGenerator, "filenameGenerator");
  }

  @Override
  public void shutdown()
  {
    _filenameGenerator = null;
  }

  /**
   * @return FILENAME
   * @see Node#FILENAME
   */
  @Override
  public FieldKey<String> getFieldName()
  {
    return Node.FILENAME;
  }

  /**
   * @return "NodeFilenameValidator"
   */
  @Override
  public String toString()
  {
    return "NodeFilenameValidator()";
  }

  @Override
  public boolean handlesTransientRecords()
  {
    return true;
  }

  @Override
  public Object triggerField(TransientRecord node,
                             String fieldName)
      throws MirrorFieldException
  {
    String existingFilename = node.opt(Node.FILENAME);
    String tidiedFilename = Strings.isNullOrEmpty(existingFilename)
        ? NodeStyle.calculateFilename(node)
        : existingFilename;
    if (tidiedFilename.indexOf(Node.FILENAME_ID) != -1) {
      tidiedFilename =
          tidiedFilename.replace(Node.FILENAME_ID, Integer.toString(node.getPendingId()));
    }
    tidiedFilename = Strings.nullToEmpty(_filenameGenerator.generateFilename(tidiedFilename));
    if (Strings.isNullOrEmpty(tidiedFilename) & node.compInt(Node.NUMBER) != 0
        & node.compInt(Node.NODEGROUPSTYLE_ID) != NodeStyle.ROOTNODESTYLE) {
      throw new MirrorFieldException("Proposed filename not accepted",
                                     null,
                                     node,
                                     Node.FILENAME,
                                     existingFilename,
                                     NodeStyle.calculateFilename(node));
    } else {
      if (!tidiedFilename.equals(existingFilename)) {
        throw new MirrorFieldException("Proposed filename has been tidied",
                                       null,
                                       node,
                                       Node.FILENAME,
                                       existingFilename,
                                       tidiedFilename);
      }
    }
    return null;
  }

  @Override
  public Object triggerField(WritableRecord node,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    if (!node.isChangedField(Node.FILENAME)) {
      return null;
    }
    return triggerField(node, fieldName);
  }

  /**
   * @return false;
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
