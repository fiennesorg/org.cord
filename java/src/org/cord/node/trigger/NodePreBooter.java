package org.cord.node.trigger;

import java.util.concurrent.atomic.AtomicInteger;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.Query;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.node.NodeBooter;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeManager;
import org.cord.node.NodeStyle;
import org.cord.util.Gettable;
import org.cord.util.PrivateCacheKey;
import org.cord.util.UndefinedCompException;

import com.google.common.base.Strings;

// 2DO ## NodePreBooter currently does too many things. The rollback
// and debugging of the class would be cleaner if it was split into
// multiple triggers.
/**
 * Trigger that initialises Node objects according to their NodeStyle.
 * <P>
 * This involves the following steps:-
 * <UL>
 * <LI>Initialise the following fields from their defaults in NodeStyle:-
 * <UL>
 * <LI>NODE_VIEWACLNAME (NODESTYLE_VIEWACLNAME)
 * <LI>NODE_EDITACLNAME (NODESTYLE_EDITACLNAME)
 * <LI>NODE_DELETEACLNAME (NODESTYLE_DELETEACLNAME)
 * </UL>
 * <LI>Set the NODE_NUMBER field to the size of Node.NODE_PARENTGROUP.GROUP_CHILDREN, i.e. the
 * existing siblings of the Node. This should ensure that all the Nodes are ordered sequentially in
 * order of creation unless two Nodes are initialised at the same time, in which case they could
 * have the same Number (unlikely and not catastrophic as the gap will automatically get left for
 * one of them to get sorted into).
 * <LI>Set the following fields to the value of NODE_FILENAME if they are still undeclared:-
 * <UL>
 * <LI>NODE_TITLE
 * <LI>NODE_HTMLTITLE
 * </UL>
 * </UL>
 */
public class NodePreBooter
  extends NodeBooter
  implements PreInstantiationTrigger
{
  private final PrivateCacheKey KEY_CHILDNUMBER = new PrivateCacheKey("NodePreBooter.childNumber");

  public NodePreBooter(NodeManager nodeManager,
                       String transactionPassword)
  {
    super(nodeManager,
          transactionPassword);
  }

  private String buildExistingSiblingsFilter(int nodeGroupStyleId,
                                             int parentNodeId)
  {
    StringBuilder filter = new StringBuilder(32);
    filter.append("(Node.nodeGroupStyle_id=")
          .append(nodeGroupStyleId)
          .append(") and (Node.parentNode_id=")
          .append(parentNodeId)
          .append(')');
    return filter.toString();
  }

  @Override
  public void triggerPre(TransientRecord node,
                         Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    // PersistentRecord parentGroup =
    // node.followEnforcedLink(Node.PARENTNODEGROUP);
    // PersistentRecord parentGroupStyle =
    // parentGroup.followEnforcedLink(NodeGroup.NODEGROUPSTYLE);
    PersistentRecord nodeGroupStyle = node.comp(Node.NODEGROUPSTYLE);
    int maximumSiblingCount = nodeGroupStyle.opt(NodeGroupStyle.MAXIMUM).intValue();
    Query highestNumberSiblings = null;
    String existingSiblingsFilter = null;
    if (maximumSiblingCount != -1) {
      existingSiblingsFilter =
          buildExistingSiblingsFilter(nodeGroupStyle.getId(), node.compInt(Node.PARENTNODE_ID));
      highestNumberSiblings =
          node.getTable().getQuery(null, existingSiblingsFilter, "Node.number desc");
      int existingSiblingCount = highestNumberSiblings.getIdList().size();
      if (existingSiblingCount >= maximumSiblingCount) {
        throw new MirrorFieldException(String.format("%s:%s: can't create: %s only permits %d children of this "
                                                     + "type, and there are already %d according to %s, namely %s",
                                                     node,
                                                     node.get("filename"),
                                                     nodeGroupStyle,
                                                     Integer.valueOf(maximumSiblingCount),
                                                     Integer.valueOf(existingSiblingCount),
                                                     highestNumberSiblings,
                                                     highestNumberSiblings.getRecordList()),
                                       null,
                                       node,
                                       Node.NUMBER,
                                       Integer.valueOf(existingSiblingCount),
                                       null);
      }
    }
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    Integer nodeStyleOwner_id = nodeStyle.opt(NodeStyle.OWNER_ID);
    switch (nodeStyleOwner_id.intValue()) {
      case NodeStyle.OWNER_ID_INHERITED:
        try {
          Integer ownerId = nodeStyleOwner_id;
          TransientRecord currentNode = node;
          while (ownerId.intValue() == -1) {
            currentNode = node.comp(Node.PARENTNODE);
            ownerId = currentNode.opt(Node.OWNER_ID);
          }
          node.setField(Node.OWNER_ID, ownerId);
        } catch (UndefinedCompException noParentNodeEx) {
          throw new MirrorFieldException("root NodeStyle.owner is set to -1",
                                         noParentNodeEx,
                                         node,
                                         Node.OWNER_ID);
        }
        break;
      case NodeStyle.OWNER_ID_CUSTOM:
        try {
          node.comp(Node.OWNER);
        } catch (UndefinedCompException badOwnerEx) {
          throw new MirrorFieldException("owner_id has not been initialised to a valid value",
                                         badOwnerEx,
                                         node,
                                         Node.OWNER_ID,
                                         node.opt(Node.OWNER_ID),
                                         null);
        }
        break;
      default:
        if (nodeStyleOwner_id.intValue() > 0) {
          node.setField(Node.OWNER_ID, nodeStyleOwner_id);
        }
    }
    PersistentRecord parentNode = node.opt(Node.PARENTNODE);
    // TODO: work out a nicer way of working out NUMBER...
    // Maybe a conditional limit of 1 on the Query depending on the value of
    // MAXIMUM????
    // or storing the value as an invalidatable value on the parent node?
    int number = node.compInt(Node.NUMBER);
    AtomicInteger cachedNumber =
        parentNode == null ? null : (AtomicInteger) parentNode.getCachedValue(KEY_CHILDNUMBER);
    if (cachedNumber == null) {
      if (number == 0) {
        if (highestNumberSiblings == null) {
          highestNumberSiblings =
              node.getTable()
                  .getQuery(null,
                            buildExistingSiblingsFilter(nodeGroupStyle.getId(),
                                                        node.compInt(Node.PARENTNODE_ID)),
                            "Node.number desc",
                            null,
                            1,
                            null);
        }
        PersistentRecord highestNumberSibling =
            RecordSources.getOptLastRecord(highestNumberSiblings, null);
        number = highestNumberSibling == null ? 1 : highestNumberSibling.compInt(Node.NUMBER) + 1;
        node.setField(Node.NUMBER, Integer.valueOf(number));
      }
      if (parentNode != null) {
        cachedNumber = new AtomicInteger(number);
        parentNode.setCachedValue(KEY_CHILDNUMBER, cachedNumber);
      }
    } else {
      int nextNumber = cachedNumber.incrementAndGet();
      if (number == 0) {
        node.setField(Node.NUMBER, Integer.valueOf(nextNumber));
      }
    }
    String proposedFilename = node.opt(Node.FILENAME);
    node.setField(Node.FILENAME, node.opt(Node.FILENAME));
    String cleanedFilename = node.opt(Node.FILENAME);
    if (Strings.isNullOrEmpty(node.opt(Node.TITLE))) {
      node.setField(Node.TITLE,
                    Strings.isNullOrEmpty(proposedFilename) ? cleanedFilename : proposedFilename);
    }
    if (Strings.isNullOrEmpty(node.opt(Node.HTMLTITLE))) {
      node.setField(Node.HTMLTITLE,
                    Strings.isNullOrEmpty(proposedFilename) ? cleanedFilename : proposedFilename);
    }
  }

  @Override
  public void releasePre(TransientRecord node,
                         Gettable params)
  {
  }

  @Override
  public void rollbackPre(TransientRecord node,
                          Gettable params)
  {
  }
}
