package org.cord.node.trigger;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PreInstantiationTrigger;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.RegionStyle;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

/**
 * FieldTrigger that lets you link the value of an instance of TableContentCompiler into a field in
 * a Node that it is registered on. This should be declared during the declare(regionStyle) method
 * of the ContentCompiler lifecycle.
 * 
 * @see org.cord.node.ContentCompiler#declare(PersistentRecord)
 */
public class AbstractContentCompilerFieldLinker<V>
  implements FieldTrigger<V>
{
  private int _nodeStyleId;

  private String _regionName;

  private FieldKey<V> _nodeFieldName;

  private FieldKey<V> _instanceFieldName;

  private NodeManager _nodeManager;

  public static <V> void register(NodeManager nodeManager,
                                  int nodeStyleId,
                                  String regionName,
                                  FieldKey<V> nodeFieldName,
                                  String instanceTableName,
                                  FieldKey<V> instanceFieldName)
      throws MirrorTableLockedException
  {
    FieldTrigger<V> nodeTrigger = new AbstractContentCompilerFieldLinker<V>(nodeManager,
                                                                            nodeStyleId,
                                                                            regionName,
                                                                            nodeFieldName,
                                                                            instanceFieldName);
    nodeManager.getDb().getTable(Node.TABLENAME).addFieldTrigger(nodeTrigger);
    PreInstantiationTrigger instanceTrigger = new Booter<V>(nodeFieldName, instanceFieldName);
    nodeManager.getDb().getTable(instanceTableName).addPreInstantiationTrigger(instanceTrigger);
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  public static <V> void register(NodeManager nodeManager,
                                  PersistentRecord regionStyle,
                                  FieldKey<V> nodeFieldName,
                                  String instanceTableName,
                                  FieldKey<V> instanceFieldName)
      throws MirrorTableLockedException
  {
    register(nodeManager,
             regionStyle.comp(RegionStyle.NODESTYLE).getId(),
             regionStyle.opt(RegionStyle.NAME),
             nodeFieldName,
             instanceTableName,
             instanceFieldName);
  }

  static class Booter<V>
    extends PreInstantiationTriggerImpl
  {
    private final FieldKey<V> _instanceFieldName;

    private final FieldKey<V> _nodeFieldName;

    private Booter(FieldKey<V> nodeFieldName,
                   FieldKey<V> instanceFieldName)
    {
      _instanceFieldName = instanceFieldName;
      _nodeFieldName = nodeFieldName;
    }

    @Override
    public void triggerPre(TransientRecord instance,
                           Gettable params)
        throws MirrorNoSuchRecordException, MirrorFieldException
    {
      instance.setField(_instanceFieldName,
                        TableContentCompiler.getNode(instance).opt(_nodeFieldName));
    }
  }

  /**
   * @param nodeManager
   * @param nodeStyleId
   *          The NodeStyle that this trigger is relevant to. This should be the NodeStyle that has
   *          the target RegionStyle attached to it to prevent other Nodes attempting to resolve the
   *          RegionStyle when they are not attached.
   * @param regionName
   *          RegionStyle.name for the TableContentCompiler that will be resolved.
   * @param nodeFieldName
   *          The name of the field in the Node that will will trigger this operation.
   * @param instanceFieldName
   *          The name of the field in the AbstractContentCompilerInstance that will be set to the
   *          value of nodeFieldName when it is updated.
   */
  private AbstractContentCompilerFieldLinker(NodeManager nodeManager,
                                             int nodeStyleId,
                                             String regionName,
                                             FieldKey<V> nodeFieldName,
                                             FieldKey<V> instanceFieldName)
  {
    _nodeManager = nodeManager;
    _nodeStyleId = nodeStyleId;
    _regionName = regionName;
    _nodeFieldName = nodeFieldName;
    _instanceFieldName = instanceFieldName;
  }

  protected NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  @Override
  public FieldKey<V> getFieldName()
  {
    return _nodeFieldName;
  }

  @Override
  public boolean handlesTransientRecords()
  {
    return false;
  }

  @Override
  public Object triggerField(TransientRecord record,
                             String fieldName)
  {
    throw new LogicException("doesn't handle transient records");
  }

  @Override
  public Object triggerField(WritableRecord node,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
    if (nodeStyle.getId() == _nodeStyleId) {
      try {
        PersistentRecord instance =
            (PersistentRecord) getNodeManager().getNode()
                                               .compile(node, _regionName, null, transaction);
        instance.setField(_instanceFieldName, node.opt(_nodeFieldName));
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new MirrorFieldException("Error resolving AbstractContentCompilerFieldLinker",
                                       mnsrEx,
                                       node,
                                       _nodeFieldName,
                                       node.opt(_nodeFieldName),
                                       null);
      }
    }
    return null;
  }

  /**
   * @return true;
   */
  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
