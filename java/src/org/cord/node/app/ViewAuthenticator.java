package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;

/**
 * A ViewAuthenticator represents an authentication method that validates a NodeRequest against a
 * target node.
 * 
 * @author alex
 */
public interface ViewAuthenticator<A extends AppMgr>
  extends AppComponent<A>
{
  /**
   * Check to see whether the user encapsulated in the given NodeRequest can pass Authentication
   * against the given Node. This Node might not be the target leaf node of the NodeRequest.
   * 
   * @param nodeRequest
   *          The compulsory NodeRequest that contains the incoming request.
   * @param node
   *          The node which this authenticator is going to validate whether it accepts
   * @return The ACL that the Authentication was performed against. Might be null if the
   *         implementation of ViewAuthenticator doesn't utilise an ACL structure for its
   *         authentication.
   * @throws NodeRequestException
   *           if the authentication is not accepted. This will generally be an
   *           {@link AuthenticationException}, but you can also raise other NodeRequestExceptions
   *           if you want more control as to how the failure is procesed.
   */
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException;

  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node);

  public boolean isAllowed(int userId,
                           PersistentRecord node);
}
