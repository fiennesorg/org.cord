package org.cord.node.app;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class JSONStringSubCC<A extends AppMgr>
  extends JSONSubCC<A, String>
{
  public static RecordOperationKey<String> createKey(String name)
  {
    return RecordOperationKey.create(name, String.class);
  }

  /**
   * Create a field on a JsonAppCc that exposes the value via a RecordOperationKey<String>
   */
  public JSONStringSubCC(JsonAppCc<A> appCC,
                         String label,
                         RecordOperationKey<String> jsonKey,
                         boolean isOptional,
                         Supplier<String> defaultValue,
                         Preprocessor<String> preprocessor)
  {
    super(appCC,
          JsonAppCc.DATA,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  /**
   * Create a field that exposes the value onto the containing record via a
   * RecordOperationKey<String>.
   * 
   * @param defaultValue
   *          The optional Supplier of the default value for this field. If null then it a Supplier
   *          of null will be substituted.
   */
  public JSONStringSubCC(AppCC<A> appCC,
                         RecordOperationKey<JSONObject> asJsonKey,
                         String label,
                         RecordOperationKey<String> jsonKey,
                         boolean isOptional,
                         Supplier<String> defaultValue,
                         Preprocessor<String> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  /**
   * Create a field that is only accessible via resolving the value inside the JSONObject.
   * 
   * @param defaultValue
   *          The optional Supplier of the default value for this field. If null then it a Supplier
   *          of null will be substituted.
   */
  public JSONStringSubCC(AppCC<A> appCC,
                         RecordOperationKey<JSONObject> asJsonKey,
                         String label,
                         String jsonKey,
                         boolean isOptional,
                         Supplier<String> defaultValue,
                         Preprocessor<String> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  @Override
  protected String getValue(TransientRecord instance)
  {
    return asJsonObject(instance).optString(getJSONKey(), isOptional() ? null : getDefaultValue());
  }

  @Override
  protected String getValue(Gettable inputs)
  {
    return inputs.getString(getName());
  }

  @Override
  protected Object toJsonValue(String value)
  {
    return value;
  }
}
