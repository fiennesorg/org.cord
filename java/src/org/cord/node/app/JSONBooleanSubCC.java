package org.cord.node.app;

import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.json.JSONObject;
import org.json.Null;

import com.google.common.base.Supplier;

public class JSONBooleanSubCC<A extends AppMgr>
  extends JSONSubCC<A, Boolean>
{
  public static RecordOperationKey<Boolean> createKey(String name)
  {
    return RecordOperationKey.create(name, Boolean.class);
  }

  public JSONBooleanSubCC(AppCC<A> appCC,
                          RecordOperationKey<JSONObject> asJsonKey,
                          String label,
                          RecordOperationKey<Boolean> recordAlias,
                          boolean isOptional,
                          Supplier<? extends Boolean> defaultValue,
                          Preprocessor<Boolean> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          recordAlias,
          isOptional,
          defaultValue,
          preprocessor);
  }

  public JSONBooleanSubCC(AppCC<A> appCC,
                          RecordOperationKey<JSONObject> asJsonKey,
                          String label,
                          String jsonKey,
                          boolean isOptional,
                          Supplier<? extends Boolean> defaultValue,
                          Preprocessor<Boolean> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  @Override
  protected Boolean getValue(TransientRecord instance)
  {
    JSONObject jObj = asJsonObject(instance);
    Object obj = jObj.opt(getJSONKey());
    if (Null.getInstance().equals(obj)) {
      return isOptional() ? null : getDefaultValue();
    }
    return (Boolean) obj;
  }

  @Override
  protected Boolean getValue(Gettable inputs)
  {
    return inputs.getBoolean(getName());
  }

  // @Override
  // protected boolean hasValue(Gettable params)
  // {
  // return params.getBoolean(getName()) != null;
  // }

  @Override
  protected Object toJsonValue(Boolean value)
  {
    return value;
  }

  @Override
  protected void appendEditWidget(PersistentRecord instance,
                                  String webmacroInstanceName,
                                  PersistentRecord user,
                                  StringBuilder buf)
      throws IOException
  {
    HtmlUtils.checkboxInput(buf,
                            "true",
                            getValue(instance).booleanValue(),
                            false,
                            false,
                            instance.opt(AppCC.NAMEPREFIX),
                            getName());
  }
}
