package org.cord.node.app;

import org.cord.mirror.Db;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.PureJsonObjectField;
import org.json.JSONObject;
import org.json.WritableJSON;

/**
 * Abstract implementation of {@link AppTableWrapper} that adds a single {@link PureJsonObjectField}
 * named {@link #DATA}.
 * 
 * @author alex
 */
public abstract class JsonAppTableWrapper<A extends AppMgr>
  extends AppTableWrapper<A>
{
  // public static final RecordOperationKey<JSONObject> DATA =
  // RecordOperationKey.create("data", JSONObject.class);
  // public static final ObjectFieldKey<String> DATASRC = JSONObjectField.createKey("dataSrc");

  public static final ObjectFieldKey<JSONObject> DATA = PureJsonObjectField.createKey("data");

  private final JSONObject __defaultValue;
  // private JSONObjectField _data = null;
  private PureJsonObjectField _data = null;

  public JsonAppTableWrapper(A appMgr,
                             String tableName,
                             JSONObject defaultValue)
  {
    super(appMgr,
          tableName);
    __defaultValue = defaultValue;
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    // _data = new JSONObjectField(DATASRC, "Data params", DATA, __defaultValue);
    _data = new PureJsonObjectField(DATA,
                                    "JSON Data",
                                    FieldValueSuppliers.ofDefined(WritableJSON.get().toJSONObject(__defaultValue)));
    table.addField(_data);
    init(db, pwd, table, _data);
  }

  protected abstract void init(final Db db,
                               final String pwd,
                               final Table table,
                               PureJsonObjectField data)
      throws MirrorTableLockedException;

}
