package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.util.DebugConstants;

import com.google.common.base.Preconditions;

/**
 * Implementation of ViewAuthenticator that forwards all requests onto an inner ViewAuthenticator
 * with AUTH debugging (if enabled) detailing that the forwarding is taking place.
 * 
 * @author alex
 * @param <A>
 */
public class ForwardingViewAuthenticator<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  private final ViewAuthenticator<? extends A> __delegate;

  public ForwardingViewAuthenticator(A appMgr,
                                     ViewAuthenticator<? extends A> delegate)
  {
    super(appMgr);
    __delegate = Preconditions.checkNotNull(delegate, "delegate");
  }

  private void debugAuth()
  {
    if (DebugConstants.DEBUG_AUTH) {
      DebugConstants.DEBUG_OUT.println("AUTH: " + this + " --> " + __delegate);
    }
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException
  {
    debugAuth();
    return __delegate.authenticate(nodeRequest, node);
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    debugAuth();
    return __delegate.isAllowed(nodeRequest, node);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    debugAuth();
    return __delegate.isAllowed(userId, node);
  }
}
