package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.Field;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

/**
 * An implementation of HeadlessFieldSubCC that only updates the field from the incoming Gettable if
 * the instance is still Transient - once it has been delivered into the database and become
 * Persistent then the field is no longer updated. This is intended for modelling initialisation
 * parameters.
 */
public class InitFieldSubCC<A extends AppMgr, V>
  extends HeadlessFieldSubCC<A, V>
{
  public InitFieldSubCC(AppCC<A> appCC,
                        Field<V> field)
  {
    super(appCC,
          field);
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    if (instance instanceof PersistentRecord) {
      return false;
    }
    return super.updateTransientInstance(node,
                                         regionStyle,
                                         instance,
                                         style,
                                         transaction,
                                         inputs,
                                         outputs,
                                         user);
  }

}
