package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Id_User;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;

public class ViewAuthenticators
{
  public static final String DEFAULT_AUTH_FAILURE_MESSAGE =
      "You do not have the appropriate permissions.";

  /**
   * Authenticate against the given ViewAuthenticator and wrap up the pass / fail into a boolean
   * return value.
   * 
   * @deprecated
   */
  @Deprecated
  public static <A extends AppMgr> boolean isAuthenticated(ViewAuthenticator<A> viewAuthenticator,
                                                           NodeRequest nodeRequest,
                                                           PersistentRecord node)
  {
    try {
      viewAuthenticator.authenticate(nodeRequest, node);
      return true;
    } catch (NodeRequestException e) {
      return false;
    }
  }

  /**
   * Create a ViewAuthenticator that returns the inverse of the given viewAuthenticator
   */
  public static <A extends AppMgr> ViewAuthenticator<A> not(final ViewAuthenticator<A> viewAuthenticator)
  {
    return new ViewAuthenticatorImpl<A>(viewAuthenticator.getAppMgr()) {
      @Override
      public boolean isAllowed(int userId,
                               PersistentRecord node)
      {
        return !viewAuthenticator.isAllowed(userId, node);
      }
    };
  }

  /**
   * Create a ViewAuthenticator that always allows requests.
   */
  public static <A extends AppMgr> ViewAuthenticator<A> allowAll(A appMgr)
  {
    return new ViewAuthenticatorImpl<A>(appMgr) {
      /**
       * @return true
       */
      @Override
      public boolean isAllowed(int userId,
                               PersistentRecord node)
      {
        return true;
      }
    };
  }

  /**
   * Create a ViewAuthenticator that never allows requests.
   */
  public static <A extends AppMgr> ViewAuthenticator<A> allowNone(A appMgr)
  {
    return new ViewAuthenticatorImpl<A>(appMgr) {
      /**
       * @return false
       */
      @Override
      public boolean isAllowed(int userId,
                               PersistentRecord node)
      {
        return false;
      }
    };
  }

  public static <A extends AppMgr> ViewAuthenticator<A> isLoggedIn(A appMgr)
  {
    return new ViewAuthenticatorImpl<A>(appMgr) {
      @Override
      public boolean isAllowed(int userId,
                               PersistentRecord node)
      {
        return userId != Id_User.ANONYMOUS;
      }
    };

  }
}
