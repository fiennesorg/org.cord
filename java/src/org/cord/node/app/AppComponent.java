package org.cord.node.app;

import org.cord.node.NodeManager;

/**
 * Interface that describes a component of the App system and which is capable of describing which
 * element of AppMgr it is associated with.
 * 
 * @author alex
 */
public interface AppComponent<A extends AppMgr>
{
  public A getAppMgr();

  public NodeManager getNodeMgr();
}
