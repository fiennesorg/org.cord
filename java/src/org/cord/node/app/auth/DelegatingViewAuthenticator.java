package org.cord.node.app.auth;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.Node;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.app.AppComponentImpl;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.util.DebugConstants;

import com.google.common.base.MoreObjects;

/**
 * Abstract ViewAuthenticator that accepts requests that have the ability to pass an alternative
 * named view on the Node that the request is invoked on.
 * 
 * @author alex
 * @param <A>
 */
public class DelegatingViewAuthenticator<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  private final int __delegatingTransactionType;
  private final String __delegatingViewName;
  private final String __errorMessage;

  public DelegatingViewAuthenticator(A appMgr,
                                     int delegatingTransactionType,
                                     String delegatingViewName,
                                     String errorMessage)
  {
    super(appMgr);
    __delegatingTransactionType = delegatingTransactionType;
    __delegatingViewName = delegatingViewName;
    __errorMessage = errorMessage;
  }

  @Override
  public final PersistentRecord authenticate(NodeRequest nodeRequest,
                                             PersistentRecord node)
      throws NodeRequestException
  {
    if (isAllowed(nodeRequest, node)) {
      return null;
    }
    throw new AuthenticationException(__errorMessage, node, null);
  }

  @Override
  public final boolean isAllowed(NodeRequest nodeRequest,
                                 PersistentRecord node)
  {
    NodeRequestSegmentFactory factory = getFactory(node);
    boolean isAllowed = factory.isAllowed(node, nodeRequest);
    if (DebugConstants.DEBUG_AUTH) {
      DebugConstants.DEBUG_OUT.println("AUTH: " + this + " --> " + isAllowed);
    }
    return isAllowed;
  }

  @Override
  public final boolean isAllowed(int userId,
                                 PersistentRecord node)
  {
    NodeRequestSegmentFactory factory = getFactory(node);
    boolean isAllowed = factory.isAllowed(node, userId);
    if (DebugConstants.DEBUG_AUTH) {
      DebugConstants.DEBUG_OUT.println("AUTH: " + this + " --> " + isAllowed);
    }
    return isAllowed;
  }

  private final NodeRequestSegmentFactory getFactory(PersistentRecord node)
  {
    return getNodeMgr().getNodeRequestSegmentManager().getFactory(node.compInt(Node.NODESTYLE_ID),
                                                                  __delegatingTransactionType,
                                                                  __delegatingViewName);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("delegatingTransactionType", __delegatingTransactionType)
                      .add("delegatingViewName", __delegatingViewName)
                      .toString();
  }
}
