package org.cord.node.app.auth;

import org.cord.mirror.Transaction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticators;
import org.cord.node.view.ReadViewFactory;

/**
 * ViewAuthenticator that accepts requests that would pass the restrictions on being able to read
 * the page that it is invoked on.
 * 
 * @author alex
 */
public class CanReadPage<A extends AppMgr>
  extends DelegatingViewAuthenticator<A>
{
  public static <A extends AppMgr> CanReadPage<A> getInstance(A appMgr,
                                                              String errorMessage)
  {
    return new CanReadPage<A>(appMgr, errorMessage);
  }

  public static <A extends AppMgr> CanReadPage<A> getInstance(A appMgr)
  {
    return getInstance(appMgr, ViewAuthenticators.DEFAULT_AUTH_FAILURE_MESSAGE);
  }

  public CanReadPage(A appMgr,
                     String errorMessage)
  {
    super(appMgr,
          Transaction.TRANSACTION_NULL,
          ReadViewFactory.NAME,
          errorMessage);
  }

}
