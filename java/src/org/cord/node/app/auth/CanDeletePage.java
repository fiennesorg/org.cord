package org.cord.node.app.auth;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.app.AppComponentImpl;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.node.app.ViewAuthenticators;
import org.cord.util.DebugConstants;

/**
 * ViewAuthenticator that accepts requests that are issued by Users that have permissions to delete
 * the Node that the request is issued against.
 * 
 * @author alex
 * @param <A>
 */
public class CanDeletePage<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  public static <A extends AppMgr> CanDeletePage<A> getInstance(A appMgr,
                                                                String errorMessage)
  {
    return new CanDeletePage<A>(appMgr, errorMessage);
  }

  public static <A extends AppMgr> CanDeletePage<A> getInstance(A appMgr)
  {
    return getInstance(appMgr, ViewAuthenticators.DEFAULT_AUTH_FAILURE_MESSAGE);
  }

  private final String __errorMessage;

  public CanDeletePage(A appMgr,
                       String errorMessage)
  {
    super(appMgr);
    __errorMessage = errorMessage;
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException
  {
    if (isAllowed(nodeRequest, node)) {
      return null;
    }
    throw new AuthenticationException(__errorMessage, node, null);
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return getNodeMgr().getNode().isDeletable(node,
                                              nodeRequest.getUser(),
                                              true,
                                              "ViewAuthenticatorCanDeletePage");
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    boolean isAllowed =
        getNodeMgr().getNode().isDeletable(node,
                                           getNodeMgr().getUser().getTable().getCompRecord(userId),
                                           true,
                                           "ViewAuthenticatorCanDeletePage");
    if (DebugConstants.DEBUG_AUTH) {
      System.err.println("AUTH: " + this + ".isAllowed(" + userId + "," + node + ") --> "
                         + isAllowed);
    }
    return isAllowed;
  }
}
