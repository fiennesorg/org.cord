package org.cord.node.app.auth;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.app.AppComponentImpl;
import org.cord.node.app.AppMgr;
import org.cord.node.app.ViewAuthenticator;
import org.cord.node.app.ViewAuthenticators;

/**
 * ViewAuthenticator that accepts requests that would pass the restrictions on being able to edit
 * the page that it is invoked on.
 * 
 * @author alex
 */
public class CanEditPage<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  public static <A extends AppMgr> CanEditPage<A> getInstance(A appMgr,
                                                              String errorMessage)
  {
    return new CanEditPage<A>(appMgr, errorMessage);
  }

  public static <A extends AppMgr> CanEditPage<A> getInstance(A appMgr)
  {
    return getInstance(appMgr, ViewAuthenticators.DEFAULT_AUTH_FAILURE_MESSAGE);
  }

  private final String __errorMessage;

  public CanEditPage(A appMgr,
                     String errorMessage)
  {
    super(appMgr);
    __errorMessage = errorMessage;
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException
  {
    if (isAllowed(nodeRequest, node)) {
      return null;
    }
    throw new AuthenticationException(__errorMessage, node, null);
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return getNodeMgr().getNode().isEditable(node, nodeRequest.getUser(), true);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    return getNodeMgr().getNode()
                       .isEditable(node,
                                   getNodeMgr().getUser().getTable().getCompRecord(userId),
                                   true);
  }

}
