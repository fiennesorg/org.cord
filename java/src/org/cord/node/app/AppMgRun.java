package org.cord.node.app;

import java.io.File;

import org.cord.mirror.Db;
import org.cord.node.NodeManager;
import org.cord.node.config.NodeConfig;

public abstract class AppMgRun<A extends AppMgr>
{
  private final AppMgrBootup<A> __appMgrBootup;

  public AppMgRun(AppMgrBootup<A> appMgrBootup)
  {
    __appMgrBootup = appMgrBootup;
  }

  public final void bootAndRun(File docBase,
                               boolean isBooted)
      throws Exception
  {
    String pwd = Db.createTransactionPassword();
    NodeManager nodeManager =
        __appMgrBootup.createNodeManager(pwd, new NodeConfig(docBase), isBooted);
    try {
      nodeManager.lock();
      run(__appMgrBootup.getAppMgr());
    } finally {
      nodeManager.shutdown();
    }
  }

  public abstract void run(A appMgr)
      throws Exception;
}
