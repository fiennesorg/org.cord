package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.node.Acl;
import org.cord.node.AuthenticationException;
import org.cord.node.Node;
import org.cord.node.NodeRequest;

/**
 * ViewAuthenticator that resolves an ACL from the target node by name and then authenticates
 * against that
 * 
 * @author alex
 */
public class NamedAclAuthenticator<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  private final RecordOperationKey<PersistentRecord> __aclName;

  public NamedAclAuthenticator(A appMgr,
                               RecordOperationKey<PersistentRecord> aclName)
  {
    super(appMgr);
    if (aclName == null) {
      __aclName = RecordOperationKey.create("aclName", PersistentRecord.class);
    } else {
      __aclName = aclName;
    }
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws AuthenticationException
  {
    Acl acls = getAppMgr().getNodeMgr().getAcl();
    PersistentRecord acl = node.opt(__aclName);
    if (acl == null) {
      return null;
    }
    if (acls.acceptsNodeRequest(acl, nodeRequest, node)) {
      return acl;
    }
    throw new AuthenticationException(String.format("%s doesn't pass the %s ACL",
                                                    nodeRequest.getUser(),
                                                    acl),
                                      node,
                                      null);
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return isAllowed(nodeRequest.getUserId(), node);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    Acl acls = getAppMgr().getNodeMgr().getAcl();
    PersistentRecord acl = node.opt(__aclName);
    if (acl == null) {
      return false;
    }
    return acls.acceptsAclUserOwner(acl.getId(), userId, node.compInt(Node.OWNER_ID));
  }
}
