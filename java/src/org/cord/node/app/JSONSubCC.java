package org.cord.node.app;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.NodeRequest;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.Null;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * An implementation of SubCC that provides a framework for implementations that write their state
 * to inside a JSONObjectField.
 */
public abstract class JSONSubCC<A extends AppMgr, T>
  extends SubCC<A>
{
  // private final PureJsonObjectField __jsonObjectField;
  private final RecordOperationKey<JSONObject> __asJsonKey;
  private final String __jsonKey;
  private final RecordOperationKey<T> __recordAlias;
  private final String __label;
  private final Supplier<? extends T> __defaultValue;
  private final boolean __isOptional;
  private final Preprocessor<T> __preprocessor;

  private JSONSubCC(AppCC<A> appCC,
                    RecordOperationKey<JSONObject> asJsonKey,
                    String label,
                    String jsonKey,
                    boolean isOptional,
                    Supplier<? extends T> defaultValue,
                    RecordOperationKey<T> recordAlias,
                    Preprocessor<T> preprocessor)
  {
    super(appCC,
          asJsonKey + "_" + jsonKey);
    __asJsonKey = asJsonKey;
    __jsonKey = jsonKey;
    __isOptional = isOptional;
    __label = label;
    __defaultValue = defaultValue == null ? Suppliers.<T> ofInstance(null) : defaultValue;
    __recordAlias = recordAlias;
    __preprocessor = preprocessor;
  }

  /**
   * @param defaultValue
   *          The Supplier of the default value for a new record, or a record which does not yet
   *          have a value. If null then a Supplier of null will automatically be used instead.
   */
  public JSONSubCC(AppCC<A> appCC,
                   RecordOperationKey<JSONObject> asJsonKey,
                   String label,
                   RecordOperationKey<T> recordAlias,
                   boolean isOptional,
                   Supplier<? extends T> defaultValue,
                   Preprocessor<T> preprocessor)
  {
    this(appCC,
         asJsonKey,
         label,
         recordAlias.getKeyName(),
         isOptional,
         defaultValue,
         recordAlias,
         preprocessor);
  }

  /**
   * @param defaultValue
   *          The Supplier of the default value for a new record, or a record which does not yet
   *          have a value. If null then a Supplier of null will automatically be used instead.
   */
  public JSONSubCC(AppCC<A> appCC,
                   RecordOperationKey<JSONObject> asJsonKey,
                   String label,
                   String jsonKey,
                   boolean isOptional,
                   Supplier<? extends T> defaultValue,
                   Preprocessor<T> preprocessor)
  {
    this(appCC,
         asJsonKey,
         label,
         jsonKey,
         isOptional,
         defaultValue,
         null,
         preprocessor);
  }

  public final boolean isOptional()
  {
    return __isOptional;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    if (__recordAlias != null) {
      instances.addRecordOperation(new SimpleRecordOperation<T>(__recordAlias) {
        @Override
        public T getOperation(TransientRecord targetRecord,
                              Viewpoint viewpoint)
        {
          return getValue(targetRecord);
        }
      });
    }
  }

  public final T getDefaultValue()
  {
    return __defaultValue.get();
  }

  public final String getJSONKey()
  {
    return __jsonKey;
  }

  public final RecordOperationKey<T> getRecordAlias()
  {
    return __recordAlias;
  }

  protected final JSONObject asJsonObject(TransientRecord instance)
  {
    return instance.comp(__asJsonKey);
  }

  /**
   * Resolve the T value out of the stored JSONData.
   */
  protected abstract T getValue(TransientRecord instance);

  /**
   * Resolve the T value out of the passed Gettable under the {@link #getName()} key.
   */
  protected abstract T getValue(Gettable inputs);

  /**
   * Convert a T value into an Object that can safely be passed to
   * {@link JSONObject#put(String, Object)} without causing a JSONException. This should be the
   * inverse of the operation that is encapsulated in {@link #getValue(TransientRecord)}
   */
  protected abstract Object toJsonValue(T value);

  /**
   * Apply the preprocessor (if defined) to the value, otherwise just return the value.
   */
  public final T preprocess(TransientRecord instance,
                            T value)
  {
    return __preprocessor == null ? value : __preprocessor.preprocess(instance, value);
  }

  /**
   * Update the state of the passed JSONObject with any appropriate information contained in the
   * Gettable params. The default implementation of this will just resolve {@link #getName()} as a
   * String in the Gettable and if there is a defined value then it will set {@link #getJSONKey()}
   * in the JSONObject. Subclasses should override this if they want to have an alternative
   * resolution or storage method.
   * 
   * @return true if the state of the jObj may have changed. This will cause the appropriate
   *         instance record to be updated accordingly.
   * @throws JSONException
   */
  protected final boolean updateJsonObject(PersistentRecord node,
                                           PersistentRecord regionStyle,
                                           TransientRecord instance,
                                           PersistentRecord style,
                                           Transaction transaction,
                                           Gettable params,
                                           PersistentRecord user,
                                           JSONObject jObj)
      throws JSONException
  {
    if (params.containsKey(getName())) {
      T value = preprocess(instance, getValue(params));
      if (value == null) {
        if (isOptional()) {
          jObj.put(getJSONKey(), Null.getInstance());
          return true;
        }
      } else {
        jObj.put(getJSONKey(), toJsonValue(value));
        return true;
      }
    }
    return false;
  }

  @Override
  protected final boolean updateTransientInstance(PersistentRecord node,
                                                  PersistentRecord regionStyle,
                                                  TransientRecord instance,
                                                  PersistentRecord style,
                                                  Transaction transaction,
                                                  Gettable inputs,
                                                  Map<Object, Object> outputs,
                                                  PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    JSONObject jObj = asJsonObject(instance);
    try {
      return updateJsonObject(node, regionStyle, instance, style, transaction, inputs, user, jObj);
    } catch (JSONException e) {
      throw new MirrorFieldException(String.format("JSON error setting %s.%s",
                                                   __asJsonKey,
                                                   getJSONKey()),
                                     e,
                                     instance,
                                     __asJsonKey);
    }
  }

  @Override
  public final String getEditTemplate(PersistentRecord instance,
                                      String webmacroInstanceName,
                                      PersistentRecord user)
  {
    StringBuilder buf = new StringBuilder();
    try {
      HtmlUtils.openLabelWidget(buf,
                                __label,
                                false,
                                instance.opt(TableContentCompiler.NAMEPREFIX),
                                getName());
      final boolean isDefined = getValue(instance) != null;
      if (isOptional()) {
        HtmlUtils.checkboxInputWidget(buf,
                                      null,
                                      isDefined,
                                      true,
                                      false,
                                      NodeRequest.ISDEFINED_PREFIX,
                                      instance.opt(TableContentCompiler.NAMEPREFIX),
                                      getName());
      } else {
        HtmlUtilsTheme.hiddenInput(buf,
                                   Boolean.TRUE,
                                   NodeRequest.ISDEFINED_PREFIX,
                                   instance.opt(TableContentCompiler.NAMEPREFIX),
                                   getName());
      }
      if (isDefined) {
        appendEditWidget(instance, webmacroInstanceName, user, buf);
      } else {
        appendDefaultDefinedWidget(instance, webmacroInstanceName, user, buf);
      }
      HtmlUtilsTheme.closeDiv(buf);
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  protected void appendEditWidget(PersistentRecord instance,
                                  String webmacroInstanceName,
                                  PersistentRecord user,
                                  StringBuilder buf)
      throws IOException
  {
    HtmlUtils.textWidget(null,
                         buf,
                         StringUtils.toString(getValue(instance)),
                         instance.opt(TableContentCompiler.NAMEPREFIX),
                         getName());
  }

  protected void appendDefaultDefinedWidget(PersistentRecord instance,
                                            String webmacroInstanceName,
                                            PersistentRecord user,
                                            StringBuilder buf)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(buf,
                               getDefaultValue(),
                               instance.opt(TableContentCompiler.NAMEPREFIX),
                               getName(),
                               NodeRequest.DEFAULTDEFINEDVALUE_POSTFIX);
  }
  public final String getLabel()
  {
    return __label;
  }

  @Override
  public String getViewTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    StringBuilder buf = new StringBuilder();
    try {
      HtmlUtils.labelValue(buf,
                           __label,
                           getValue(instance),
                           instance.opt(TableContentCompiler.NAMEPREFIX),
                           getName());
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }

  /**
   * An Object that is capable of transforming values read from a Gettable before they are put into
   * the JSONObject. This will be invoked on the output of {@link JSONSubCC#getValue(Gettable)}
   * during the
   * {@link JSONSubCC#updateJsonObject(PersistentRecord, PersistentRecord, TransientRecord, PersistentRecord, Transaction, Gettable, PersistentRecord, JSONObject)}
   * method.
   */
  public interface Preprocessor<T>
  {
    /**
     * Perform any transformations and validations on the value that are necessary before it gets
     * stored in the JSONObject.
     */
    public T preprocess(TransientRecord instance,
                        T value);
  }
}
