package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.FieldValueSupplier;
import org.cord.mirror.FieldValueSuppliers;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.field.NamedMapField;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.KeyGettableSupplier;
import org.cord.util.StringUtils;
import org.json.JSONObject;

import com.google.common.base.Preconditions;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;

/**
 * Immutable data object to represent a TemplatePath and associated CSS class.
 */
public class TemplatePath
  extends EnglishNamedImpl
{
  public static final String TITLE = "title";
  public static final String TEMPLATEPATH = "templatePath";
  public static final String CSSCLASS = "cssClass";

  public static KeyGettableSupplier<TemplatePath> SUPPLIER =
      new KeyGettableSupplier<TemplatePath>() {
        @Override
        public TemplatePath get(String key,
                                Gettable gettable)
        {
          return new TemplatePath(key,
                                  Preconditions.checkNotNull(gettable.getString(TITLE),
                                                             "%s." + TITLE,
                                                             key)
                                               .trim(),
                                  Preconditions.checkNotNull(gettable.getString(TEMPLATEPATH),
                                                             "%s." + TEMPLATEPATH,
                                                             key)
                                               .trim(),
                                  StringUtils.trim(gettable.getString(CSSCLASS)));
        }
      };

  public static final String NODE_KEYS = "node.keys";
  public static final String APP_KEYS = "app.keys";
  public static final String DEFAULT = "default";

  /**
   * Create a new NamedMapField that represents a choice of TemplatePath elements and initialise it
   * from a Gettable configuration.
   * 
   * @param fieldName
   *          The SQL field name
   * @param label
   *          The displayed label in the edit interface
   * @param gettable
   *          The source of configuration data
   * @param namespace
   *          The namespace that configurations will be stored under in the Gettable. The will be
   *          looked up using {@link #NODE_KEYS}, {@link #APP_KEYS} with a default key of
   *          {@link #DEFAULT}.
   * @return The appropriate NamedMapField which can then be added to a Table or wrapped in a SubCC
   *         as appropriate.
   */
  public static NamedMapField<TemplatePath> createField(ObjectFieldKey<TemplatePath> fieldName,
                                                        String label,
                                                        Gettable gettable,
                                                        String namespace)
  {
    NamedMapField<TemplatePath> field =
        new NamedMapField<TemplatePath>(fieldName, label, TemplatePath.class, null);
    field.addValues(GettableUtils.getValues(gettable, namespace, NODE_KEYS, SUPPLIER));
    field.addValues(GettableUtils.getValues(gettable, namespace, APP_KEYS, SUPPLIER));
    String defaultKey = Preconditions.checkNotNull(gettable.getString(namespace + "." + DEFAULT),
                                                   "Default key not defined under %s." + DEFAULT,
                                                   namespace);
    TemplatePath defaultTemplatePath = Preconditions.checkNotNull(field.getValues().get(defaultKey),
                                                                  "Unregistered default key of %s",
                                                                  defaultKey);
    FieldValueSupplier<TemplatePath> defaultSupplier =
        FieldValueSuppliers.ofDefined(defaultTemplatePath);
    field.setDefaultDefinedValue(defaultSupplier);
    field.setNewRecordValue(defaultSupplier);
    return field;
  }

  public static ObjectFieldKey<TemplatePath> createFieldKey(String key)
  {
    return ObjectFieldKey.create(key, TemplatePath.class);
  }

  public static ImmutableMap<String, TemplatePath> getValues(Gettable gettable,
                                                             String namespace)
  {
    ImmutableMap.Builder<String, TemplatePath> builder = ImmutableMap.builder();
    for (TemplatePath templatePath : GettableUtils.getValues(gettable,
                                                             namespace,
                                                             NODE_KEYS,
                                                             SUPPLIER)) {
      builder.put(templatePath.getName(), templatePath);
    }
    for (TemplatePath templatePath : GettableUtils.getValues(gettable,
                                                             namespace,
                                                             APP_KEYS,
                                                             SUPPLIER)) {
      builder.put(templatePath.getName(), templatePath);
    }
    return builder.build();
  }

  public static <A extends AppMgr> JSONMapSubCC<A, TemplatePath> createJsonField(AppCC<A> appCC,
                                                                                 RecordOperationKey<JSONObject> asJsonKey,
                                                                                 String label,
                                                                                 RecordOperationKey<TemplatePath> key,
                                                                                 boolean isOptional,
                                                                                 Gettable gettable,
                                                                                 String namespace)
  {
    Map<String, TemplatePath> values = getValues(gettable, namespace);
    String defaultKey = Preconditions.checkNotNull(gettable.getString(namespace + "." + DEFAULT),
                                                   "Default key not defined under %s." + DEFAULT,
                                                   namespace);
    TemplatePath defaultTemplatePath =
        Preconditions.checkNotNull(values.get(defaultKey),
                                   "Unregistered default key of %s defined in %s",
                                   defaultKey,
                                   key);
    return new JSONMapSubCC<A, TemplatePath>(appCC,
                                             asJsonKey,
                                             label,
                                             key,
                                             values,
                                             isOptional,
                                             Suppliers.ofInstance(defaultTemplatePath),
                                             null);
  }

  private final String __cssClass;
  private final String __templatePath;

  public TemplatePath(String name,
                      String title,
                      String templatePath,
                      String cssClass)
  {
    super(name,
          title);
    __templatePath = Preconditions.checkNotNull(templatePath, "templatePath");
    __cssClass = cssClass;
  }

  public final String getCssClass()
  {
    return __cssClass;
  }

  public final String getTemplatePath()
  {
    return __templatePath;
  }
}
