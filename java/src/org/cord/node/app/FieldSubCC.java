package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Field;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;

/**
 * An Implementation of SubCC that wraps a single Field
 * 
 * @author alex
 */
public class FieldSubCC<A extends AppMgr, V>
  extends SubCC<A>
{
  public static <A extends AppMgr, V> FieldSubCC<A, V> create(AppCC<A> appCC,
                                                              Field<V> field)
  {
    return new FieldSubCC<A, V>(appCC, field);
  }

  private final Field<V> __field;
  private final boolean __isInstantiationOnly;

  /**
   * Create a FieldSubCC that wraps a Field on an AppCC with the choice as to whether it operates at
   * boot time or for all updates.
   * 
   * @param isInstantiationOnly
   *          If true then the contained Field will only be booted up against a TransientRecord and
   *          then will not get updated again by this FieldSubCC once the record has been made
   *          persistent. If false then the Field can be updated against both TransientRecords and
   *          WritableRecords.
   */
  public FieldSubCC(AppCC<A> appCC,
                    Field<V> field,
                    boolean isInstantiationOnly)
  {
    super(appCC,
          field.getName());
    __field = field;
    __isInstantiationOnly = isInstantiationOnly;
  }

  /**
   * Create a FieldSubCC that wraps a Field on an AppCC that updates against both TransientRecords
   * and WritableRecords.
   */
  public FieldSubCC(AppCC<A> appCC,
                    Field<V> field)
  {
    this(appCC,
         field,
         false);
  }

  public final Field<V> getField()
  {
    return __field;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    instances.addField(getField());
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    if (__isInstantiationOnly & instance instanceof PersistentRecord) {
      return false;
    }
    Object value = getField().getValue(instance, inputs);
    if (value == null) {
      if (inputs.containsKey(getName())) {
        instance.setField(getName(), (Object) null);
      }
    } else {
      instance.setField(getName(), value);
    }
    return instance.getIsUpdated();
  }

  @Override
  public String getEditTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    if (__isInstantiationOnly) {
      return "";
    }
    return String.format("$%s.asFormElement.%s\n", webmacroInstanceName, getField().getName());
  }

  @Override
  public String getViewTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    if (__isInstantiationOnly) {
      return "";
    }
    return String.format("$%s.asFormValue.%s\n", webmacroInstanceName, getField().getName());
  }

}
