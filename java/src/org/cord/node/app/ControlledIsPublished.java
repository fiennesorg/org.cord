package org.cord.node.app;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.operation.AbstractTableQueryResolver;
import org.cord.node.Node;
import org.cord.util.DuplicateNamedException;

public final class ControlledIsPublished<A extends AppMgr>
  extends NodeControlsInstanceSubCC<A, Boolean>
{
  public static final ObjectFieldKey<Boolean> ISPUBLISHED()
  {
    return Node.ISPUBLISHED.copy();
  }

  public ControlledIsPublished(AppCC<A> appCC,
                               ObjectFieldKey<Boolean> key)
  {
    super(appCC,
          Node.ISPUBLISHED,
          new BooleanField(key, "Is published?"));
  }

  public ControlledIsPublished(AppCC<A> appCC)
  {
    this(appCC,
         ISPUBLISHED());
  }

  public final static RecordSourceOperationKey<RecordSource> PUBLISHEDALL =
      RecordSourceOperationKey.create("publishedAll", RecordSource.class);

  @Override
  protected void init(final Db db,
                      final String pwd,
                      final Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    instances.addRecordSourceOperation(new AbstractTableQueryResolver(PUBLISHEDALL) {
      @Override
      protected Query createQuery(Table invokingTable,
                                  String queryName)
      {
        StringBuilder filter = new StringBuilder();
        filter.append("(")
              .append(instances.getTableName())
              .append(".isPublished=1)" + " and (")
              .append(instances.getTableName())
              .append(".isDefined=1)");
        return invokingTable.getQuery(queryName, filter.toString(), null);
      }
    });
  }

}
