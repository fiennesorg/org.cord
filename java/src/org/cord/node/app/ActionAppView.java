package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.webmacro.Context;

/**
 * Implementation of AppView that does no work but directly invokes the {@link PostViewAction} once
 * the {@link ViewAuthenticator} has been satisfied.
 * 
 * @author alex
 * @param <A>
 */
public class ActionAppView<A extends AppMgr>
  extends AppView<A>
{
  public ActionAppView(A appMgr,
                       String name,
                       String title,
                       ViewAuthenticator<? super A> viewAuthenticator,
                       PostViewAction action)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator,
          action);
  }

  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    return handleSuccess(nodeRequest, node, context);
  }
}
