package org.cord.node.app;

/**
 * A Builder is an Object that is capable of constructing an instance of a given type of Object once
 * the configuration has been completed. One should use a Builder as an equivalent of new() - ie one
 * should create a new Builder for each object that one wishes to build and once one has invoked
 * build() then one should discard the Builder and not invoke any other methods on it.
 * 
 * @author alex
 * @param <V>
 *          The class of value that this Builder will produce.
 */
public interface Builder<V>
{
  public V build();
}
