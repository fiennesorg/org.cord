package org.cord.node.app;

import java.util.Iterator;

import org.cord.util.ArrayUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.AbstractIterator;

/**
 * Generate an Iterator of rows from a 2 dimensional CSV import with optional row skipping and
 * padding.
 */
public class CsvRowIterator
{
  private static final Predicate<String[]> NEVER = Predicates.alwaysFalse();

  /**
   * Get a Predicate that passes arrays of strings whose first element starts with the specified
   * header string.
   */
  public static Predicate<String[]> STARTSWITH(final String header)
  {
    return new Predicate<String[]>() {
      @Override
      public boolean apply(String[] input)
      {
        return input[0].startsWith(header);
      }
    };
  }

  private final Predicate<String[]> __shouldSkipRow;
  private final Predicate<String[]> __shouldTerminate;
  private final int __minRowLength;
  private final int __firstRowIndex;

  public CsvRowIterator()
  {
    this(NEVER,
         NEVER,
         0,
         0);
  }

  private CsvRowIterator(Predicate<String[]> shouldSkipRow,
                         Predicate<String[]> shouldTerminate,
                         int minRowLength,
                         int firstRowIndex)
  {
    __shouldSkipRow = shouldSkipRow;
    __shouldTerminate = shouldTerminate;
    __minRowLength = minRowLength;
    __firstRowIndex = firstRowIndex;
  }

  public CsvRowIterator minRowLength(int minRowLength)
  {
    Preconditions.checkState(minRowLength >= 0,
                             "Cannot have a minRowLenght of %s because negative lengths are not permitted",
                             Integer.valueOf(minRowLength));
    return new CsvRowIterator(__shouldSkipRow, __shouldTerminate, minRowLength, __firstRowIndex);
  }

  public CsvRowIterator shouldSkipRow(Predicate<String[]> logic)
  {
    Preconditions.checkNotNull(logic, "no logic supplied for shouldSkipRow");
    return new CsvRowIterator(logic, __shouldTerminate, __minRowLength, __firstRowIndex);
  }

  public CsvRowIterator shouldTerminate(Predicate<String[]> logic)
  {
    Preconditions.checkNotNull(logic, "no logic supplied for shouldTerminate");
    return new CsvRowIterator(__shouldSkipRow, logic, __minRowLength, __firstRowIndex);
  }

  public CsvRowIterator firstRowIndex(int i)
  {
    Preconditions.checkState(i >= 0,
                             "Cannot start from row %s because that is less than 0",
                             Integer.valueOf(i));
    return new CsvRowIterator(__shouldSkipRow, __shouldTerminate, __minRowLength, i);
  }

  public Iterator<String[]> iterator(final String[][] rows)
  {
    return new AbstractIterator<String[]>() {
      int r = __firstRowIndex;

      @Override
      protected String[] computeNext()
      {
        while (r < rows.length) {
          String[] next = ArrayUtils.copyAndPad(rows[r], __minRowLength);
          if (__shouldTerminate.apply(next)) {
            endOfData();
            return null;
          }
          r++;
          if (!__shouldSkipRow.apply(next)) {
            return next;
          }
        }
        endOfData();
        return null;
      }
    };
  }

  public Iterable<String[]> iterable(final String[][] rows)
  {
    return new Iterable<String[]>() {
      @Override
      public Iterator<String[]> iterator()
      {
        return CsvRowIterator.this.iterator(rows);
      }
    };
  }
}
