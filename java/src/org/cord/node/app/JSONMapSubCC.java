package org.cord.node.app;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.json.JSONObject;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

/**
 * Implementation of AbstractJSONSubCC that provides a selection widget onto a Map of defined
 * values.
 */
public class JSONMapSubCC<A extends AppMgr, T>
  extends JSONSubCC<A, T>
{
  private final BiMap<String, T> __values;

  /**
   * constructor that creates a RecordOperation on the record that resolves the value of the JSON
   * field as a primary operation.
   * 
   * @param values
   *          the Map that this will represent. This will be copied into the internal data structure
   *          so future changes to the original Map will not be tracked.
   */
  public JSONMapSubCC(AppCC<A> appCC,
                      RecordOperationKey<JSONObject> asJsonKey,
                      String label,
                      RecordOperationKey<T> jsonKey,
                      Map<String, ? extends T> values,
                      boolean isOptional,
                      Supplier<? extends T> defaultValue,
                      Preprocessor<T> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
    __values = ImmutableBiMap.copyOf(Preconditions.checkNotNull(values, "values"));
  }

  /**
   * constructor that doesn't register a top level RecordOperation.
   * 
   * @param values
   *          the Map that this will represent. This will be copied into the internal data structure
   *          so future changes to the original Map will not be tracked.
   */
  public JSONMapSubCC(AppCC<A> appCC,
                      RecordOperationKey<JSONObject> asJsonKey,
                      String label,
                      String jsonKey,
                      Map<String, ? extends T> values,
                      boolean isOptional,
                      Supplier<? extends T> defaultValue,
                      Preprocessor<T> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
    __values = ImmutableBiMap.copyOf(Preconditions.checkNotNull(values, "values"));
  }

  public Map<String, T> getValues()
  {
    return Collections.unmodifiableMap(__values);
  }

  @Override
  protected T getValue(TransientRecord instance)
  {
    String jsonValue = asJsonObject(instance).optString(getJSONKey());
    if (jsonValue != null) {
      T value = __values.get(jsonValue);
      if (value != null) {
        return value;
      }
    }
    return isOptional() ? null : getDefaultValue();
  }

  @Override
  protected T getValue(Gettable inputs)
  {
    String valueKey = inputs.getString(getName());
    if (valueKey != null) {
      return __values.get(valueKey);
    }
    return null;
  }

  /**
   * Convert a value into a readable String for use in building an HTML select option.
   * 
   * @return value.toString() - override this method if alternative behaviour is required.
   */
  protected String toOptionString(T value)
  {
    return value.toString();
  }

  @Override
  public void appendEditWidget(PersistentRecord instance,
                               String webmacroInstanceName,
                               PersistentRecord user,
                               StringBuilder buf)
      throws IOException
  {
    final String jsonValue = asJsonObject(instance).optString(getJSONKey());
    HtmlUtils.openSelect(buf, false, false, false, instance.opt(AppCC.NAMEPREFIX), getName());
    for (Map.Entry<String, T> entry : __values.entrySet()) {
      HtmlUtils.option(buf,
                       entry.getKey(),
                       toOptionString(entry.getValue()),
                       Objects.equal(entry.getKey(), jsonValue));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  protected Object toJsonValue(T value)
  {
    return __values.inverse().get(value);
  }
}
