package org.cord.node.app;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class JSONDoubleSubCC<A extends AppMgr>
  extends JSONSubCC<A, Double>
{
  /**
   * Create an instance that is only accessible from inside the JSONObject.
   */
  public JSONDoubleSubCC(AppCC<A> appCC,
                         RecordOperationKey<JSONObject> asJsonKey,
                         String label,
                         String jsonKey,
                         boolean isOptional,
                         Supplier<Double> defaultValue,
                         Preprocessor<Double> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  /**
   * Create an instance that exposes its state as a RecordOperation on the containing record.
   */
  public JSONDoubleSubCC(AppCC<A> appCC,
                         RecordOperationKey<JSONObject> asJsonKey,
                         String label,
                         RecordOperationKey<Double> recordAlias,
                         boolean isOptional,
                         Supplier<Double> defaultValue,
                         Preprocessor<Double> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          recordAlias,
          isOptional,
          defaultValue,
          preprocessor);
  }

  @Override
  protected Double getValue(TransientRecord instance)
  {
    JSONObject jObj = asJsonObject(instance);
    Object o = jObj.opt(getJSONKey());
    if (o != null) {
      return NumberUtil.toDouble(o);
    }
    if (isOptional()) {
      return null;
    }
    return getDefaultValue();
  }

  @Override
  protected Double getValue(Gettable inputs)
  {
    return NumberUtil.toDouble(inputs.get(getName()), null);
  }

  @Override
  protected Object toJsonValue(Double value)
  {
    return value;
  }
}
