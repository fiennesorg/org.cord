package org.cord.node.app;

import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.field.StringField;
import org.cord.node.Node;

public class ControlledDescription<A extends AppMgr>
  extends NodeControlsInstanceSubCC<A, String>
{
  public static final ObjectFieldKey<String> DESCRIPTION()
  {
    return Node.DESCRIPTION.copy();
  }

  public ControlledDescription(AppCC<A> appCC,
                               ObjectFieldKey<String> key)
  {
    super(appCC,
          Node.DESCRIPTION,
          new StringField(key, "Title", StringField.LENGTH_TEXT, ""));
  }

  public ControlledDescription(AppCC<A> appCC)
  {
    this(appCC,
         DESCRIPTION());
  }
}
