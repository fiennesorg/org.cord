package org.cord.node.app;

import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeStyle;
import org.cord.node.RegionStyle;

import com.google.common.base.Preconditions;

/**
 * An AppCc with a constant regionStyleName that only permits a single delcaration in a given
 * project.
 * 
 * @author alex
 */
public abstract class SingletonAppCc<A extends AppMgr>
  extends ConstantRegionStyleNameCC<A>
{
  private boolean _isDeclared;

  public SingletonAppCc(A appMgr,
                        String name,
                        TemplatePaths templatePaths,
                        String regionStyleName)
  {
    super(appMgr,
          name,
          templatePaths,
          regionStyleName);
  }

  private void assertIsSingleton(PersistentRecord nodeStyle)
  {
    if (nodeStyle.getId() == 1) {
      return;
    }
    RecordSource parentNodeGroupStyles = nodeStyle.opt(NodeStyle.PARENTNODEGROUPSTYLES);
    PersistentRecord parentNodeGroupStyle =
        RecordSources.getOnlyRecord(parentNodeGroupStyles,
                                    null,
                                    "%s should only be in 1 NodeGroupStyle as opposed to %s",
                                    nodeStyle,
                                    parentNodeGroupStyles);
    int maximum = parentNodeGroupStyle.compInt(NodeGroupStyle.MAXIMUM);
    Preconditions.checkArgument(maximum == 1,
                                "%s should be a singleton but it permits %s instances",
                                parentNodeGroupStyle,
                                Integer.valueOf(maximum));
    assertIsSingleton(parentNodeGroupStyle.comp(NodeGroupStyle.PARENTNODESTYLE));
  }

  @Override
  protected void internalDeclare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.internalDeclare(regionStyle);
    if (_isDeclared) {
      throw new MirrorLogicException(String.format("%s cannot be declared a second time on %s in %s",
                                                   this,
                                                   regionStyle,
                                                   regionStyle.opt(RegionStyle.NODESTYLE)),
                                     null);
    }
    assertIsSingleton(regionStyle.comp(RegionStyle.NODESTYLE));
    _isDeclared = true;
  }

  public final PersistentRecord getInstance()
  {
    return RecordSources.getOnlyRecord(getInstanceTable(), null);
  }
}
