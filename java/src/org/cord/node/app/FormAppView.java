package org.cord.node.app;

import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringBuilderIOException;
import org.webmacro.Context;

public abstract class FormAppView<A extends AppMgr>
  extends AppView<A>
{
  public static final String IN_HASRENDEREDFORM = "hasRenderedForm";
  
  public static final String WM_FORMAPPHIDDENFIELDS = "FormAppHiddenFields";

  public FormAppView(A appMgr,
                     String name,
                     String title,
                     ViewAuthenticator<? super A> viewAuthenticator)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator);
  }

  protected NodeRequestSegment renderForm(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context)
      throws NodeRequestException
  {
    context.put(WM_FORMAPPHIDDENFIELDS, getHiddenFormFields());
    return new NodeRequestSegment(getNodeMgr(),
                                  nodeRequest,
                                  node,
                                  context,
                                  createTemplatePath("form.wm.html"));
  }
  
  protected final String getHiddenFormFields()
  {
    StringBuilder buf = new StringBuilder();
    try {
      HtmlUtilsTheme.hiddenInput(buf, getName(), NodeRequest.CGI_VAR_VIEW);
      HtmlUtilsTheme.hiddenInput(buf, Boolean.TRUE, IN_HASRENDEREDFORM);
      HtmlUtilsTheme.hiddenInput(buf, Boolean.FALSE, "hasSubmitted");
    } catch (IOException e) {
      throw new StringBuilderIOException(buf, e);
    }
    return buf.toString();
  }


  @Override
  protected final NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    
    if (!Boolean.TRUE.equals(nodeRequest.getBoolean(IN_HASRENDEREDFORM))) {
      return renderForm(nodeRequest, node, context);
    }
    return processForm(nodeRequest, node, context, acl);
  }
  
  protected abstract NodeRequestSegment processForm(NodeRequest nodeRequest,
                                                    PersistentRecord node,
                                                    Context context,
                                                    PersistentRecord acl)
      throws NodeRequestException;
}
