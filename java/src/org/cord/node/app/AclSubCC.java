package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;

import com.google.common.base.Preconditions;

/**
 * Wrapper around another SubCC that only provides an edit interface to it if the user satisfies a
 * given ACL. This lets you define AppCC implementations that expose different sets of fields to
 * different users.
 */
public class AclSubCC<A extends AppMgr>
  extends SubCC<A>
{
  public AclSubCC(AppCC<A> appCC,
                  SubCC<A> wrappedSubCC,
                  Integer editAclId)
  {
    super(appCC,
          wrappedSubCC.getName());
    __wrappedSubCC = Preconditions.checkNotNull(wrappedSubCC, "wrappedSubCC");
    __editAclId = editAclId;
  }

  private final SubCC<A> __wrappedSubCC;

  protected SubCC<A> getWrappedSubCC()
  {
    return __wrappedSubCC;
  }

  private final Integer __editAclId;

  public Integer getEditAclId()
  {
    return __editAclId;
  }

  public PersistentRecord getEditAcl()
  {
    Integer id = getEditAclId();
    if (id == null) {
      return null;
    }
    return getNodeMgr().getAcl().getTable().getOptRecord(id);
  }

  public boolean isEditableBy(PersistentRecord user)
  {
    PersistentRecord editAcl = getEditAcl();
    if (editAcl == null) {
      return true;
    }
    return getNodeMgr().getAcl().acceptsAclUser(editAcl.getId(), user == null ? 0 : user.getId());
  }

  @Override
  public String getEditTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    if (isEditableBy(user)) {
      return getWrappedSubCC().getEditTemplate(instance, webmacroInstanceName, user);
    }
    return getWrappedSubCC().getViewTemplate(instance, webmacroInstanceName, user);
  }
  @Override
  protected void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.declare(regionStyle);
    getWrappedSubCC().declare(regionStyle);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    getWrappedSubCC().init(db, pwd, instances);
  }

  @Override
  protected void shutdown()
  {
    super.shutdown();
    getWrappedSubCC().shutdown();
  }

  @Override
  protected boolean updatePersistentInstance(PersistentRecord node,
                                             PersistentRecord regionStyle,
                                             PersistentRecord instance,
                                             PersistentRecord style,
                                             Transaction transaction,
                                             Gettable inputs,
                                             Map<Object, Object> outputs,
                                             PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    boolean result = super.updatePersistentInstance(node,
                                                    regionStyle,
                                                    instance,
                                                    style,
                                                    transaction,
                                                    inputs,
                                                    outputs,
                                                    user);
    if (isEditableBy(user)) {
      result = result | getWrappedSubCC().updatePersistentInstance(node,
                                                                   regionStyle,
                                                                   instance,
                                                                   style,
                                                                   transaction,
                                                                   inputs,
                                                                   outputs,
                                                                   user);
    }
    return result;
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    boolean result = super.updateTransientInstance(node,
                                                   regionStyle,
                                                   instance,
                                                   style,
                                                   transaction,
                                                   inputs,
                                                   outputs,
                                                   user);
    if (isEditableBy(user)) {
      result = result | getWrappedSubCC().updateTransientInstance(node,
                                                                  regionStyle,
                                                                  instance,
                                                                  style,
                                                                  transaction,
                                                                  inputs,
                                                                  outputs,
                                                                  user);
    }
    return result;
  }

  @Override
  public String getViewTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    return getWrappedSubCC().getViewTemplate(instance, webmacroInstanceName, user);
  }

}
