package org.cord.node.app;

import org.cord.mirror.Db;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.node.Node;
import org.cord.util.DuplicateNamedException;

public final class ControlledNumber<A extends AppMgr>
  extends NodeControlsInstanceSubCC<A, Integer>
{
  public static final IntFieldKey NUMBER()
  {
    return Node.NUMBER.copy();
  }

  public ControlledNumber(AppCC<A> appCC,
                          IntFieldKey fieldKey)
  {
    super(appCC,
          Node.NUMBER,
          new IntField(fieldKey, "Number"));
  }

  public ControlledNumber(AppCC<A> appCC)
  {
    this(appCC,
         NUMBER());
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    String title = ControlledTitle.TITLE().getKeyName();
    if (instances.getField(title) == null) {
      instances.setDefaultOrdering(instances.getTableName() + "." + getField().getName() + ","
                                   + instances.getTableName() + "." + TransientRecord.FIELD_ID);
    } else {
      instances.setDefaultOrdering(instances.getTableName() + "." + getField().getName() + ","
                                   + instances.getTableName() + "." + title + ","
                                   + instances.getTableName() + "." + TransientRecord.FIELD_ID);
    }
  }
}
