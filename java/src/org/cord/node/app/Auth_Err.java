package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/**
 * A ViewAuthenticator that wraps an existing ViewAuthenticator and throws a FeedbackErrorException
 * if the wrapped ViewAuthenticator throws any error in it's authenticate method.
 */
public class Auth_Err<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  /**
   * @param wrappedAuth
   *          The compulsory ViewAuthenticator that actually does the work.
   * @param title
   *          The optional title that is used in the FeedbackErrorException. If null, then the
   *          node.title of the invoking context is used instead.
   * @param message
   *          The compulsory content of the FeedbackErrorException
   */
  public static <A extends AppMgr> Auth_Err<A> wrap(ViewAuthenticator<A> wrappedAuth,
                                                    String title,
                                                    String message)
  {
    return new Auth_Err<A>(wrappedAuth, title, message);
  }

  /**
   * Create an Auth_Err that delegates the title to the $node.title that it is being invoked on.
   */
  public static <A extends AppMgr> Auth_Err<A> wrap(ViewAuthenticator<A> wrappedAuth,
                                                    String message)
  {
    return wrap(wrappedAuth, null, message);
  }

  private final ViewAuthenticator<A> __wrappedAuth;
  private final String __title;
  private final String __message;

  /**
   * @param wrappedAuth
   *          The compulsory ViewAuthenticator that actually does the work.
   * @param title
   *          The optional title that is used in the FeedbackErrorException. If null, then the
   *          node.title of the invoking context is used instead.
   * @param message
   *          The compulsory content of the FeedbackErrorException
   */
  public Auth_Err(ViewAuthenticator<A> wrappedAuth,
                  String title,
                  String message)
  {
    super(Preconditions.checkNotNull(wrappedAuth.getAppMgr(), "wrappedAuth"));
    __wrappedAuth = wrappedAuth;
    __title = title;
    __message = Preconditions.checkNotNull(message, "message");
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException
  {
    try {
      return __wrappedAuth.authenticate(nodeRequest, node);
    } catch (NodeRequestException e) {
      throw new FeedbackErrorException(MoreObjects.firstNonNull(__title, node.opt(Node.TITLE)),
                                       node,
                                       __message);
    }
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return __wrappedAuth.isAllowed(nodeRequest, node);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    return __wrappedAuth.isAllowed(userId, node);
  }
}
