package org.cord.node.app;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.PureJsonObjectField;
import org.json.JSONObject;

/**
 * An implementation of ConstantRegionStyleNameCC that contains a single JSONObjectField which can
 * then be populated as required by the subclasses.
 */
public abstract class JsonAppCc<A extends AppMgr>
  extends ConstantRegionStyleNameCC<A>
{
  // public static final RecordOperationKey<JSONObject> DATA =
  // RecordOperationKey.create("data", JSONObject.class);
  // public static final ObjectFieldKey<String> DATASRC = JSONObjectField.createKey("dataSrc");

  public static final ObjectFieldKey<JSONObject> DATA = PureJsonObjectField.createKey("data");

  private final JSONObject __defaultValue;
  // private JSONObjectField _data = null;
  private PureJsonObjectField _data = null;

  public JsonAppCc(A appMgr,
                   String name,
                   TemplatePaths templatePaths,
                   String regionStyleName,
                   JSONObject defaultValue)
  {
    super(appMgr,
          name,
          templatePaths,
          regionStyleName);
    __defaultValue = defaultValue;
  }

  // public JSONObjectField getData()
  // {
  // return _data;
  // }
  public PureJsonObjectField getData()
  {
    return _data;
  }

  @Override
  protected final void init(final Db db,
                            final String pwd,
                            final Table instances)
      throws MirrorTableLockedException
  {
    // _data = new JSONObjectField(DATASRC, "Data params", DATA, __defaultValue);
    _data =
        new PureJsonObjectField(DATA, "JSON Data", PureJsonObjectField.emptyFieldValueSupplier());
    instances.addField(_data);
    init(db, pwd, instances, _data);
  }

  // protected abstract void init(final Db db,
  // final String pwd,
  // final Table instances,
  // JSONObjectField data)
  // throws MirrorTableLockedException;
  protected abstract void init(final Db db,
                               final String pwd,
                               final Table instances,
                               PureJsonObjectField data)
      throws MirrorTableLockedException;

  /**
   * Resolve the JSONObject that contains the data for this record.
   */
  public JSONObject getData(TransientRecord record)
  {
    return record.opt(DATA);
  }

}
