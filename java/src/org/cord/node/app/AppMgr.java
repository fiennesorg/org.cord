package org.cord.node.app;

import java.util.Map;

import javax.servlet.ServletException;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.Transaction;
import org.cord.node.NodeManager;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableFactory;
import org.cord.util.GettableUtils;
import org.cord.util.NamespaceGettable;
import org.cord.util.StringUtils;
import org.webmacro.Context;
import org.webmacro.engine.StaticClassWrapper;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

public abstract class AppMgr
  implements GettableFactory
{
  private final String __appTemplateDir;

  public final String getAppTemplateDir()
  {
    return __appTemplateDir;
  }

  private final String __contextAppMgrName;

  /**
   * Get the name that this AppMgr should be placed into a Webmacro {@link Context} as defined in
   * the constructor.
   */
  public final String getContextAppMgrName()
  {
    return __contextAppMgrName;
  }

  /**
   * Create a new empty WebMacro {@link Context} and place a copy of this AppMgr in it under
   * {@link #getContextAppMgrName()}.
   */
  public Context newContext()
  {
    Context context = getNodeMgr().getWebMacro().getContext();
    context.put(getContextAppMgrName(), this);
    return context;
  }

  public final String getAppTemplate(String templatePath)
  {
    return getAppTemplateDir() + templatePath;
  }

  private GettableFactory _gettables;

  @Override
  public final Gettable getGettable()
  {
    return _gettables.getGettable();
  }

  private NodeManager _nodeMgr;

  public final NodeManager getNodeMgr()
  {
    return _nodeMgr;
  }

  private String _pwd;

  /**
   * Get the mirror transaction password that was used to initialise the system. This is protected
   * because this fact should not be publically available (ie not to webmacro), however if the pwd
   * is to be shared by other classes in extensions of AppMgr then they can override it with a
   * protected method that just invokes this method thereby exposing the functionality. Note that
   * sub-classes should not make it public if at all possible.
   * 
   * @return The password as passed to {@link #initialise(GettableFactory, NodeManager, String)}
   */
  protected String getPwd()
  {
    return _pwd;
  }

  public AppMgr(String appTemplateDir,
                String contextAppMgrName)
  {
    __appTemplateDir = StringUtils.padEmpty(appTemplateDir, "app/");
    __contextAppMgrName =
        StringUtils.padEmpty(contextAppMgrName, AppServlet.WEBMACRO_APPMGR_DEFAULT);
  }

  private Map<String, StaticClassWrapper<?>> _staticClassWrappers;

  public static Map<String, StaticClassWrapper<?>> getStaticClassWrappers(Gettable gettable)
  {
    Map<String, StaticClassWrapper<?>> map = Maps.newHashMap();
    Gettable namespaced = new NamespaceGettable(gettable, "StaticClassWrapper.");
    addStaticClassWrappers(namespaced, "node.keys", map);
    addStaticClassWrappers(namespaced, "app.keys", map);
    return map;
  }

  private static void addStaticClassWrappers(Gettable namespaced,
                                             String keysKey,
                                             Map<String, StaticClassWrapper<?>> map)
  {
    String nodeKeys = namespaced.getString(keysKey);
    if (!Strings.isNullOrEmpty(nodeKeys)) {
      for (String key : GettableUtils.getKeySplitter().split(nodeKeys)) {
        try {
          Class<?> klass = Class.forName(namespaced.getString(key));
          map.put(key, StaticClassWrapper.getInstance(klass));
          DebugConstants.DEBUG_OUT.println("StaticClassWrapper: $" + key + " = " + klass);
        } catch (NullPointerException e) {
          throw new IllegalArgumentException(String.format("StaticClassWrapper unable to resolve className for \"%s\"",
                                                           key),
                                             e);
        } catch (ClassNotFoundException e) {
          throw new IllegalArgumentException(String.format("StaticClassWrapper unable to resolve \"%s\" as %s",
                                                           key,
                                                           namespaced.getString(key)),
                                             e);
        }
      }
    }
  }

  public void addStaticClassWrappers(Context context)
  {
    context.putAll(_staticClassWrappers);
  }

  protected final void initialise(GettableFactory gettables,
                                  NodeManager nodeMgr,
                                  String pwd)
      throws MirrorTableLockedException, MirrorTransactionTimeoutException
  {
    _gettables = gettables;
    _nodeMgr = nodeMgr;
    _pwd = pwd;
    _staticClassWrappers = ImmutableMap.copyOf(getStaticClassWrappers(nodeMgr.getGettable()));
    initAppMgr(gettables, nodeMgr, pwd);
  }

  protected abstract void initAppMgr(GettableFactory gettables,
                                     NodeManager nodeMgr,
                                     String pwd)
      throws MirrorTableLockedException, MirrorTransactionTimeoutException;

  protected final void lock()
  {
    lockAppMgr();
  }

  protected abstract void lockAppMgr();

  protected abstract void initAppServlet(AppServlet<?> appServlet,
                                         NodeManager nodeMgr)
      throws ServletException, MirrorTableLockedException;

  public Transaction createUpdateTransaction(String pwd,
                                             String details)
  {
    return createTransaction(Transaction.TRANSACTION_UPDATE, pwd, details);
  }

  public Transaction createDeleteTransaction(String pwd,
                                             String details)
  {
    return createTransaction(Transaction.TRANSACTION_DELETE, pwd, details);
  }

  public Transaction createTransaction(int transactionType,
                                       String pwd,
                                       String details)
  {
    return getNodeMgr().getDb()
                       .createTransaction(Db.DEFAULT_TIMEOUT, transactionType, pwd, details);
  }
}
