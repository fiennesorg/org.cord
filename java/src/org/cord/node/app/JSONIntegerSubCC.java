package org.cord.node.app;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class JSONIntegerSubCC<A extends AppMgr>
  extends JSONSubCC<A, Integer>
{
  public static RecordOperationKey<Integer> createKey(String name)
  {
    return RecordOperationKey.create(name, Integer.class);
  }

  /**
   * Create an instance that is only accessible from inside the JSONObject.
   */
  public JSONIntegerSubCC(AppCC<A> appCC,
                          RecordOperationKey<JSONObject> asJsonKey,
                          String label,
                          String jsonKey,
                          boolean isOptional,
                          Supplier<Integer> defaultValue,
                          Preprocessor<Integer> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
  }

  /**
   * Create an instance that exposes its state as a RecordOperation on the containing record.
   */
  public JSONIntegerSubCC(AppCC<A> appCC,
                          RecordOperationKey<JSONObject> asJsonKey,
                          String label,
                          RecordOperationKey<Integer> recordAlias,
                          boolean isOptional,
                          Supplier<Integer> defaultValue,
                          Preprocessor<Integer> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          recordAlias,
          isOptional,
          defaultValue,
          preprocessor);
  }

  /**
   * Create an instance that exposes its state as a RecordOperation on the containing record.
   */
  public JSONIntegerSubCC(JsonAppCc<A> appCC,
                          String label,
                          RecordOperationKey<Integer> recordAlias,
                          boolean isOptional,
                          Supplier<Integer> defaultValue,
                          Preprocessor<Integer> preprocessor)
  {
    super(appCC,
          JsonAppCc.DATA,
          label,
          recordAlias,
          isOptional,
          defaultValue,
          preprocessor);
  }

  @Override
  protected Integer getValue(TransientRecord instance)
  {
    return asJsonObject(instance).optInteger(getJSONKey(), isOptional() ? null : getDefaultValue());
  }

  @Override
  protected Integer getValue(Gettable inputs)
  {
    return NumberUtil.toInteger(inputs.get(getName()), null);
  }

  @Override
  protected Object toJsonValue(Integer value)
  {
    return value;
  }
}
