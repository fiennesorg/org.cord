package org.cord.node.app;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.JSONObjectField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public abstract class JSONField<V>
  extends SimpleRecordOperation<V>
  implements HtmlGuiSupplier
{
  private final String __label;
  private final JSONObjectField __jsonObjectField;

  public JSONField(RecordOperationKey<V> name,
                   String label,
                   JSONObjectField jsonObjectField)
  {
    super(name);
    __label = MoreObjects.firstNonNull(label, name.getKeyName());
    __jsonObjectField = Preconditions.checkNotNull(jsonObjectField, "jsonObjectField");
  }

  protected final JSONObjectField getJsonObjectField()
  {
    return __jsonObjectField;
  }

  public abstract V resolveValue(TransientRecord targetRecord,
                                 Viewpoint viewpoint);

  public String toHtmlValue(V value)
  {
    return value == null ? "" : value.toString();
  }

  @Override
  public final V getOperation(TransientRecord targetRecord,
                              Viewpoint viewpoint)
  {
    return resolveValue(targetRecord, viewpoint);
  }

  @Override
  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf, getHtmlLabel(), false, namePrefix, getName());
    appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    HtmlUtilsTheme.closeDiv(buf);
  }

  @Override
  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    HtmlUtils.labelValue(buf,
                         getHtmlLabel(),
                         toHtmlValue(resolveValue(record, viewpoint)),
                         namePrefix,
                         getName());
  }

  @Override
  public abstract void appendHtmlWidget(TransientRecord record,
                                        Viewpoint viewpoint,
                                        String namePrefix,
                                        Appendable buf,
                                        Map<Object, Object> context)
      throws IOException;

  @Override
  public String getHtmlLabel()
  {
    return __label;
  }

  @Override
  public boolean hasHtmlWidget(TransientRecord record)
  {
    return true;
  }

  @Override
  public boolean shouldReloadOnChange()
  {
    return false;
  }

}
