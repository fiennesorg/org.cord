package org.cord.node.app;

import com.google.common.base.Preconditions;

/**
 * Implementation of {@link ForwardingViewAuthenticator} that has a definable {@link #toString()}
 * method to make the intention clearer in debugging.
 * 
 * @author alex
 * @param <A>
 */
public class NamedForwardingViewAuthenticator<A extends AppMgr>
  extends ForwardingViewAuthenticator<A>
{
  private final String __name;

  public NamedForwardingViewAuthenticator(A appMgr,
                                          String name,
                                          ViewAuthenticator<? extends A> delegate)
  {
    super(appMgr,
          delegate);
    __name = Preconditions.checkNotNull(name);
  }

  @Override
  public String toString()
  {
    return __name;
  }

}
