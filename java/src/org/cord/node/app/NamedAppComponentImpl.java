package org.cord.node.app;

import org.cord.node.NodeManager;
import org.cord.util.NamedImpl;

import com.google.common.base.Preconditions;

/**
 * Base class that gives you a Named AppComponent as a building block for other functionality.
 * 
 * @author alex
 */
public class NamedAppComponentImpl<A extends AppMgr>
  extends NamedImpl
  implements AppComponent<A>
{
  private final A __appMgr;

  public NamedAppComponentImpl(A appMgr,
                               String name)
  {
    super(name);
    __appMgr = Preconditions.checkNotNull(appMgr, "appMgr");
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

}
