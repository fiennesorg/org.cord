package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.Node;
import org.cord.node.NodeRequest;
import org.cord.util.DebugConstants;

import com.google.common.base.MoreObjects;

/**
 * ViewAuthenticator that authenticates against a given constant ACL
 * 
 * @author alex
 */
public class ConstantAclAuthenticator<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  public static <A extends AppMgr> ConstantAclAuthenticator<A> of(A appMgr,
                                                                  int aclId)
  {
    return new ConstantAclAuthenticator<A>(appMgr, aclId);
  }

  private final int __aclId;

  public ConstantAclAuthenticator(A appMgr,
                                  int aclId)
  {
    super(appMgr);
    __aclId = aclId;
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws AuthenticationException
  {
    PersistentRecord acl = getOptAcl();
    if (acl == null) {
      debugAuth(false);
      return null;
    }
    boolean isAllowed = getNodeMgr().getAcl().acceptsNodeRequest(acl, nodeRequest, node);
    debugAuth(isAllowed);
    if (isAllowed) {
      return acl;
    }
    throw new AuthenticationException(String.format("%s doesn't pass the %s ACL",
                                                    nodeRequest.getUser(),
                                                    acl),
                                      node,
                                      null);
  }

  private void debugAuth(boolean isAllowed)
  {
    if (DebugConstants.DEBUG_AUTH) {
      DebugConstants.DEBUG_OUT.println("AUTH: " + this + " --> " + isAllowed);
    }
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return isAllowed(nodeRequest.getUserId(), node);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    PersistentRecord acl = getOptAcl();
    if (acl == null) {
      debugAuth(false);
      return false;
    }
    boolean isAllowed =
        getNodeMgr().getAcl().acceptsAclUserOwner(acl.getId(), userId, node.compInt(Node.OWNER_ID));
    debugAuth(isAllowed);
    return isAllowed;
  }

  private PersistentRecord getOptAcl()
  {
    return getNodeMgr().getAcl().getTable().getOptRecord(__aclId);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("id", __aclId).add("Acl", getOptAcl()).toString();
  }
}
