package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class Auth_Not<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  public static <A extends AppMgr> Auth_Not<A> not(ViewAuthenticator<A> auth)
  {
    return new Auth_Not<A>(auth);
  }

  private final ViewAuthenticator<A> __auth;

  private Auth_Not(ViewAuthenticator<A> auth)
  {
    super(Preconditions.checkNotNull(auth, "auth").getAppMgr());
    __auth = auth;
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws AuthenticationException
  {
    if (!isAllowed(nodeRequest, node)) {
      throw new AuthenticationException(String.format("%s must fail", __auth), node, null);
    }
    return null;
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return !__auth.isAllowed(nodeRequest, node);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    return !__auth.isAllowed(userId, node);
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("of", __auth).toString();
  }

}
