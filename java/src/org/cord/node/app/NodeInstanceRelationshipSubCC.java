package org.cord.node.app;

import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.util.Gettable;

public class NodeInstanceRelationshipSubCC<A extends AppMgr, V>
  extends FieldSubCC<A, V>
{
  private final FieldKey<V> __nodeKey;
  private final FieldKey<V> __instanceKey;

  /**
   * @throws IllegalArgumentException
   *           if there is not a Field named field.getName() registered on Node
   */
  public NodeInstanceRelationshipSubCC(AppCC<A> appCC,
                                       FieldKey<V> nodeKey,
                                       Field<V> instanceField)
  {
    super(appCC,
          instanceField);
    getNodeMgr().getNode().getTable().getField(instanceField.getName());
    __nodeKey = nodeKey;
    __instanceKey = instanceField.getKey();
  }

  public final FieldKey<V> getNodeKey()
  {
    return __nodeKey;
  }

  public final FieldKey<V> getInstanceKey()
  {
    return __instanceKey;
  }

  @Override
  protected void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.declare(regionStyle);
    getAppCC().getInstanceTable().addPreInstantiationTrigger(new NodeToInstanceBooter());
  }

  private class NodeToInstanceBooter
    extends PreInstantiationTriggerImpl
  {
    @Override
    public void triggerPre(TransientRecord instance,
                           Gettable params)
        throws MirrorNoSuchRecordException, MirrorFieldException
    {
      instance.setField(getInstanceKey(), AppCC.getNode(instance).opt(getNodeKey()));
    }

    @Override
    public String toString()
    {
      return ("NodeToInstanceBooter(Node." + getField().getName() + " --> instance."
              + getField().getName() + ")");
    }
  }

}
