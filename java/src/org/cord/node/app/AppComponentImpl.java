package org.cord.node.app;

import org.cord.node.NodeManager;

import com.google.common.base.Preconditions;

/**
 * Basic top-level implementation of AppComponent that stores the appropriate AppMgr
 * 
 * @author alex
 */
public class AppComponentImpl<A extends AppMgr>
  implements AppComponent<A>
{
  private final A __appMgr;

  public AppComponentImpl(A appMgr)
  {
    __appMgr = Preconditions.checkNotNull(appMgr, "appMgr");
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

}
