package org.cord.node.app;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.node.ConstantValueFactory;
import org.cord.node.Node;
import org.cord.node.NodeFieldValueFactory;
import org.cord.node.NodeFilter;
import org.cord.node.NodeFilterByNodeStyles;
import org.cord.node.NodeManager;
import org.cord.node.RegionStyle;
import org.cord.node.ValueFactory;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.HtmlUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Implementation of LinkField that provides for a link to an AppCC
 * 
 * @author alex
 */
public abstract class AppCcLink<A extends AppMgr>
  extends LinkField
  implements AppComponent<A>
{
  private final A __appMgr;

  private String _questionName;

  private boolean _hasRegisteredLinkQuestion = false;

  public AppCcLink(A appMgr,
                   IntFieldKey linkFieldName,
                   String label,
                   Integer defaultValue,
                   RecordOperationKey<PersistentRecord> linkName,
                   String targetTableName,
                   RecordOperationKey<RecordSource> referencesName,
                   String reverseOrderClause,
                   String transactionPassword,
                   int linkStyle)
  {
    super(linkFieldName,
          label,
          defaultValue,
          linkName,
          targetTableName,
          referencesName,
          reverseOrderClause,
          transactionPassword,
          linkStyle);
    __appMgr = Preconditions.checkNotNull(appMgr, "appMgr");
  }

  public AppCcLink(A appMgr,
                   String targetTableName,
                   String label,
                   String pwd,
                   int linkStyle)
  {
    this(appMgr,
         null,
         label,
         null,
         null,
         targetTableName,
         null,
         null,
         pwd,
         linkStyle);
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Map<Object, Object> context)
      throws IOException
  {
    if (_hasRegisteredLinkQuestion) {
      int c = getValidTargets(record).getIdList(viewpoint).size();
      if (c > 50) {
        HtmlUtils.value(buf,
                        c + " valid choices<br />"
                             + getHtmlWidgetSummary(record, record.opt(getLinkName(), viewpoint)),
                        namePrefix,
                        getName());
        return;
      }
    }
    super.appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
  }

  @Override
  public void appendHtmlValue(TransientRecord instance,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    if (mayResolveRecord(instance, viewpoint)) {
      try {
        PersistentRecord target = resolveRecord(instance, viewpoint);
        HtmlUtils.value(buf,
                        target.opt(Node.HREF, target.getTable().getTitleOperationName()),
                        namePrefix,
                        getName());
        return;
      } catch (MirrorNoSuchRecordException e) {
      }
    }
    super.appendHtmlValue(instance, viewpoint, namePrefix, buf, context);
  }

  public String getQuestionName()
  {
    if (Strings.isNullOrEmpty(_questionName)) {
      _questionName = getSource().getInstanceTableName() + "." + getName();
    }
    return _questionName;
  }

  @Override
  protected String getHtmlWidgetSummary(TransientRecord instance,
                                        PersistentRecord target)
  {
    StringBuilder buf = new StringBuilder();
    if (mayResolveRecord(instance, null)) {
      try {
        buf.append("current: ")
           .append(resolveRecord(instance, null).opt(Node.HREF, Node.TITLE.getKeyName()))
           .append("<br />");
      } catch (MirrorNoSuchRecordException e) {
      }
    } else {
      buf.append("undefined<br />");
    }
    if (_hasRegisteredLinkQuestion) {
      buf.append("<a href=\"")
         .append(instance.comp(TableContentCompiler.NODE).opt(Node.URL))
         .append("?view=")
         .append(getQuestionName())
         .append("\">Browse</a>");
    }
    return buf.toString();
  }

  public ValueFactory getAskQuestionUrl()
  {
    return new ConstantValueFactory("/");
  }

  public ValueFactory getQuestionEnglishFactory()
  {
    return new NodeFieldValueFactory("What is the " + getHtmlLabel() + " for ", Node.TITLE, "?");
  }

  public NodeFilter getEligibleNodeFilter()
  {
    return NodeFilterByNodeStyles.getInstanceFromNodeStyles(getTarget().getNodeStyles());
  }

  public String getTargetRegionStyleName()
  {
    return getTarget().getDefaultRegionStyleName();
  }

  public String getSourceRegionStyleName()
  {
    return getSource().getDefaultRegionStyleName();
  }

  public abstract AppCC<?> getSource();

  public abstract AppCC<?> getTarget();

  public void registerLinkQuestion(PersistentRecord regionStyle)
      throws MirrorNoSuchRecordException
  {
    getSource().registerLinkQuestion(regionStyle.comp(RegionStyle.NODESTYLE),
                                     getQuestionName(),
                                     getAskQuestionUrl(),
                                     getQuestionEnglishFactory(),
                                     1,
                                     getEligibleNodeFilter(),
                                     getTarget(),
                                     getTargetRegionStyleName(),
                                     getSourceRegionStyleName() + RegionStyle.NAMESPACE_SEPARATOR
                                                                 + getName(),
                                     getSourceRegionStyleName(),
                                     null,
                                     null);
    _hasRegisteredLinkQuestion = true;
  }
}
