package org.cord.node.app;

import org.cord.mirror.Field;
import org.cord.mirror.PersistentRecord;

/**
 * A HeadlessFieldSubCC is a FieldSubCC that doesn't let the user edit the field via the edit
 * template, but it will update the value if is in the incoming Gettable. This is useful for values
 * that get initialised or updated via a Gettable but not under the control of an admin via a GUI.
 * 
 * @author alex
 */
public class HeadlessFieldSubCC<A extends AppMgr, V>
  extends FieldSubCC<A, V>
{
  public HeadlessFieldSubCC(AppCC<A> appCC,
                            Field<V> field)
  {
    super(appCC,
          field);
  }

  @Override
  public String getEditTemplate(PersistentRecord instance,
                                String webmacroInstanceName,
                                PersistentRecord user)
  {
    return String.format("$%s.asFormValue.%s\n", webmacroInstanceName, getField().getName());
  }

}
