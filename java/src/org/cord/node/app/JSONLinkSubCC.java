package org.cord.node.app;

import java.io.IOException;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.HtmlUtils;
import org.json.JSONObject;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public class JSONLinkSubCC<A extends AppMgr>
  extends JSONIntegerSubCC<A>
{
  private final RecordSource __targets;
  private final boolean __shouldReloadOnChange;

  /**
   * @param followLink
   *          The optional key that the RecordOperation to resolve the PersistentRecord pointed at
   *          by this link is to be registered under. If the link does not have a valid target then
   *          the RecordOperation will return null. If the key is null then no RecordOperation will
   *          be registered.
   */
  public JSONLinkSubCC(AppCC<A> appCC,
                       RecordOperationKey<JSONObject> asJsonKey,
                       String label,
                       final RecordSource targets,
                       boolean shouldReloadOnChange,
                       final RecordOperationKey<Integer> linkId,
                       RecordOperationKey<PersistentRecord> followLink,
                       boolean isOptional,
                       Supplier<Integer> defaultValue,
                       Preprocessor<Integer> preprocessor)
      throws MirrorTableLockedException
  {
    super(appCC,
          asJsonKey,
          label,
          linkId,
          isOptional,
          defaultValue,
          preprocessor);
    __targets = Preconditions.checkNotNull(targets, "targets");
    __shouldReloadOnChange = shouldReloadOnChange;
    if (followLink != null) {
      appCC.getTable().addRecordOperation(new SimpleRecordOperation<PersistentRecord>(followLink) {
        @Override
        public PersistentRecord getOperation(TransientRecord record,
                                             Viewpoint viewpoint)
        {
          Integer id = record.opt(linkId);
          if (id == null) {
            return null;
          }
          return targets.getTable().getOptRecord(id, viewpoint);
        }
      });
    }
  }

  @Override
  protected void appendEditWidget(PersistentRecord instance,
                                  String webmacroInstanceName,
                                  PersistentRecord user,
                                  StringBuilder buf)
      throws IOException
  {
    Integer value = getValue(instance);
    HtmlUtils.openSelect(buf,
                         false,
                         __shouldReloadOnChange,
                         false,
                         instance.opt(TableContentCompiler.NAMEPREFIX),
                         getName());
    for (PersistentRecord target : __targets) {
      Integer id = Integer.valueOf(target.getId());
      HtmlUtils.option(buf, id, target.getTitle(), id.equals(value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

}
