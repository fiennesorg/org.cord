package org.cord.node.app;

/**
 * An AbstractBuilder is a base class for Objects that are used for building instances of other
 * objects. A given Builder should only be used to build a single instance and should not have
 * anything invoked on it after the object has been built.
 * 
 * @author alex
 * @param <V>
 *          The type of object that this builder will build.
 */
public abstract class AbstractBuilder<V>
{
  private boolean _isBuilt = false;

  /**
   * @return true if this Builder has been built, otherwise false
   */
  public boolean isBuilt()
  {
    return _isBuilt;
  }

  /**
   * @throws IllegalStateException
   *           if this builder has already been used to build an object.
   */
  public void assertNotBuilt()
  {
    if (_isBuilt) {
      throw new IllegalStateException(String.format("%s is already built", this));
    }
  }

  /**
   * Build the appropriate Object. This will fail if it has been invoked before. It will invoke
   * buildImpl() once it has validated that the Builder is in the appropriate state.
   * 
   * @return the result of invoking buildImpl()
   */
  public final V build()
  {
    assertNotBuilt();
    _isBuilt = true;
    return buildImpl();
  }

  /**
   * Create the instance of V that represents the current configuration of this AbstractBuilder.
   * This should not be invoked directly as clients are expected to invoke build(). This method
   * should only ever be invoked once by build().
   * 
   * @return The appropriate instance
   */
  protected abstract V buildImpl();
}
