package org.cord.node.app;

import java.util.ArrayList;
import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class Auth_Or<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  private final List<ViewAuthenticator<?>> __viewAuthenticators =
      new ArrayList<ViewAuthenticator<?>>();

  private Auth_Or(A appMgr)
  {
    super(appMgr);
    // TODO Auto-generated constructor stub
  }

  /**
   * @return The result of the last authenticate method on the sub-authenticator methods.
   */
  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws AuthenticationException
  {
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      try {
        return viewAuthenticator.authenticate(nodeRequest, node);
      } catch (NodeRequestException authEx) {
        // do nothing and try the next ViewAuthenticator...
      }
    }
    throw new AuthenticationException(String.format("None of %s passed authentication",
                                                    __viewAuthenticators),
                                      node,
                                      null);
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      if (viewAuthenticator.isAllowed(userId, node)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      if (viewAuthenticator.isAllowed(nodeRequest, node)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("of", __viewAuthenticators).toString();
  }

  public static <A extends AppMgr> Builder<A> or(ViewAuthenticator<A> auth)
  {
    Builder<A> builder = new Builder<A>(auth.getAppMgr());
    builder.or(auth);
    return builder;
  }

  public static class Builder<A extends AppMgr>
    extends AbstractBuilder<Auth_Or<A>>
  {
    private final Auth_Or<A> __instance;

    public Builder(A appMgr)
    {
      __instance = new Auth_Or<A>(appMgr);
    }

    public Builder<A> or(ViewAuthenticator<? extends A> viewAuthenticator)
    {
      assertNotBuilt();
      Preconditions.checkNotNull(viewAuthenticator, "viewAuthenticator");
      __instance.__viewAuthenticators.add(viewAuthenticator);
      return this;
    }

    @Override
    protected Auth_Or<A> buildImpl()
    {
      return __instance;
    }
  }
}
