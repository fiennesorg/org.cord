package org.cord.node.app;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.StringField;
import org.cord.node.Node;
import org.cord.util.DuplicateNamedException;

public final class ControlledTitle<A extends AppMgr>
  extends NodeControlsInstanceSubCC<A, String>
{
  public static final ObjectFieldKey<String> TITLE()
  {
    return Node.TITLE.copy();
  }

  public ControlledTitle(AppCC<A> appCC,
                         ObjectFieldKey<String> key)
  {
    super(appCC,
          Node.TITLE,
          new StringField(key, "Title"));
  }

  public ControlledTitle(AppCC<A> appCC)
  {
    this(appCC,
         TITLE());
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, instances);
    // instances.addField(new StringField(TableContentCompiler.TITLE, "Node title"));
    instances.setTitleOperationName(getInstanceKey());
    instances.setDefaultOrdering(instances.getTableName() + "." + getInstanceKey() + ","
                                 + instances.getTableName() + "." + TransientRecord.FIELD_ID);
  }
}
