package org.cord.node.app;

import org.cord.node.TemplateSuccessOperation;

/**
 * Implementation of AppView that always renders the given templatePath once the
 * {@link ViewAuthenticator} has been satisfied.
 */
public class TemplateAppView<A extends AppMgr>
  extends ActionAppView<A>
{
  public TemplateAppView(A appMgr,
                         String name,
                         String title,
                         ViewAuthenticator<? super A> viewAuthenticator,
                         String templatePath)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator,
          new TemplateSuccessOperation(appMgr.getNodeMgr(), templatePath));
  }

}
