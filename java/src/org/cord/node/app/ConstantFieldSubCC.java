package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.Field;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

/**
 * A ConstantFieldSubCC never tries to update the value of the Field that it wraps, but it does
 * display the value of the Field in auto-generated edit templates. This makes it useful when you
 * have an external process that is responsible for updating the value of the Field but you still
 * want to see the result embedded inside a UI.
 */
public class ConstantFieldSubCC<A extends AppMgr, V>
  extends HeadlessFieldSubCC<A, V>
{

  public ConstantFieldSubCC(AppCC<A> appCC,
                            Field<V> field)
  {
    super(appCC,
          field);
  }

  /**
   * Do nothing
   * 
   * @return false
   */
  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    return false;
  }
}
