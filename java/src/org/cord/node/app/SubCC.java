package org.cord.node.app;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.NamedImpl;

import com.google.common.base.Preconditions;

public abstract class SubCC<A extends AppMgr>
  extends NamedImpl
  implements AppComponent<A>
{
  private final static Set<Table> __implementingTables = new HashSet<Table>();

  private final static Set<Table> __roImplementingTables =
      Collections.unmodifiableSet(__implementingTables);

  /**
   * Get the Set of all Tables that have an instance of this SubCc registered against them. This is
   * built automatically as the init method is called for each instance.
   * 
   * @return Immutable Set of Tables that are supporting this Cc.
   * @see #getContainingTable()
   */
  public static Set<Table> getImplementingTables()
  {
    return __roImplementingTables;
  }

  private final A __appMgr;

  private final AppCC<A> __appCC;

  private Table _containingTable;

  public SubCC(AppCC<A> appCC,
               String name)
  {
    super(name);
    __appCC = Preconditions.checkNotNull(appCC, "appCC");
    __appMgr = __appCC.getAppMgr();
  }

  public abstract String getEditTemplate(PersistentRecord instance,
                                         String webmacroInstanceName,
                                         PersistentRecord user);

  public abstract String getViewTemplate(PersistentRecord instance,
                                         String webmacroInstanceName,
                                         PersistentRecord user);

  private volatile boolean _isInit = false;

  protected void init(Db db,
                      String pwd,
                      Table instances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    if (_isInit) {
      throw new IllegalStateException("Cannot invoke init twice on the same instance of SubCc");
    }
    _isInit = true;
    Preconditions.checkNotNull(instances, "instances");
    __implementingTables.add(instances);
    _containingTable = instances;
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

  public final AppCC<A> getAppCC()
  {
    return __appCC;
  }

  protected void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
  }

  protected void shutdown()
  {
  }

  protected boolean updatePersistentInstance(PersistentRecord node,
                                             PersistentRecord regionStyle,
                                             PersistentRecord instance,
                                             PersistentRecord style,
                                             Transaction transaction,
                                             Gettable inputs,
                                             Map<Object, Object> outputs,
                                             PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    return false;
  }

  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    return false;
  }

  /**
   * Get the Table that this SubCc is registered on.
   * 
   * @return The Table that this SubCc has been initialised against. If invoked before it is
   *         registered then it will be null.
   * @see #getImplementingTables()
   */
  public final Table getContainingTable()
  {
    return _containingTable;
  }
}
