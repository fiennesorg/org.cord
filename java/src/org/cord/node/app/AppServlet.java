package org.cord.node.app;

import javax.servlet.ServletException;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeServlet;
import org.webmacro.servlet.WebContext;

/**
 * AppServlet provides an implementation of {@link NodeServlet} that is parametric by a subclass of
 * {@link AppMgr}.
 * 
 * @author alex
 * @param <A>
 *          the implementation of {@link AppMgr}
 */
public abstract class AppServlet<A extends AppMgr>
  extends NodeServlet
  implements AppComponent<A>
{
  private static final long serialVersionUID = 1L;

  private final A __appMgr;

  public static final String WEBMACRO_APPMGR_DEFAULT = "appMgr";
  public static final String WEBMACRO_NODEMGR = "nodeMgr";

  public AppServlet(A appMgr) throws ServletException
  {
    super(new AppMgrBootup<A>(appMgr));
    __appMgr = appMgr;
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getNodeManager();
  }

  /**
   * Delegate the initialisation to {@link AppMgr#initAppServlet(AppServlet, NodeManager)}. If it
   * becomes necessary for a subclass to override this method then they should invoke super
   * {@link #initialise(NodeManager)} otherwise the behaviour will become unpredictable.
   */
  @Override
  protected void initialise(NodeManager nodeMgr)
      throws ServletException, MirrorTableLockedException
  {
    getAppMgr().initAppServlet(this, nodeMgr);
  }
  
  @Override
  protected void prepopulate(WebContext context)
  {
    super.prepopulate(context);
    context.put("appMgr", getAppMgr());
  }

  /**
   * Initialise common webmacro variables. More precisely:
   * <ul>
   * <li>Set the character encoding of the response to UTF-8</li>
   * <li>Set up $nodeMgr</li>
   * <li>Set up $appMgr and the custom name for the same defined in
   * {@link AppMgr#getContextAppMgrName()}</li>
   * <li>Drop in the static class wrappers supplied by
   * {@link AppMgr#addStaticClassWrappers(org.webmacro.Context)}</li>
   * </ul>
   * Subclasses that override this method should invoke this implementation otherwise standard app
   * templates might well fail.
   */
  @Override
  protected void customiseContext(NodeManager nodeManager,
                                  WebContext context,
                                  NodeRequest nodeRequest)
  {
    context.getResponse().setCharacterEncoding("UTF-8");
    context.put(WEBMACRO_NODEMGR, nodeManager);
    String appMgrName = getAppMgr().getContextAppMgrName();
    context.put(appMgrName, getAppMgr());
    if (!WEBMACRO_APPMGR_DEFAULT.equals(appMgrName)) {
      context.put(WEBMACRO_APPMGR_DEFAULT, getAppMgr());
    }
    getAppMgr().addStaticClassWrappers(context);
  }
}
