package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;

/**
 * A ViewAuthenticator that only lets through requests that are issued against a node that is
 * currently locked for updates by the owner of the NodeRequest.
 */
public class UpdateableAuthenticator<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{

  public UpdateableAuthenticator(A appMgr)
  {
    super(appMgr);
  }

  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws AuthenticationException
  {
    Transaction t = nodeRequest.getSessionTransaction().getTransactionByNode(node.getId());
    if (t == null) {
      throw new AuthenticationException("Node is not locked by a Transaction", node, null);
    }
    switch (t.getType()) {
      case Transaction.TRANSACTION_UPDATE:
        return null;
      default:
        throw new AuthenticationException("Node is locked by non-update Transaction " + t,
                                          node,
                                          null);
    }
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    Transaction t = nodeRequest.getSessionTransaction().getTransactionByNode(node.getId());
    if (t == null) {
      return false;
    }
    switch (t.getType()) {
      case Transaction.TRANSACTION_UPDATE:
        return true;
    }
    return false;
  }

  /**
   * @return false because it is impossible to know whether the session associated with the given
   *         userId is the one that has the lock on this record.
   */
  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    return false;
  }
}
