package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeGroupStyle;
import org.cord.node.NodeGroupStyle.CreateNodePolicy;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/**
 * {@link CreateNodePolicy} that will reject creation requests that do not pass the given
 * {@link ViewAuthenticator}
 * 
 * @author alex
 */
public class ViewAuthenticatorCreateNodePolicy<A extends AppMgr>
  extends AppComponentImpl<A>
  implements CreateNodePolicy
{
  private final ViewAuthenticator<A> __viewAuthenticator;

  public ViewAuthenticatorCreateNodePolicy(A appMgr,
                                           ViewAuthenticator<A> viewAuthenticator)
  {
    super(appMgr);
    __viewAuthenticator = Preconditions.checkNotNull(viewAuthenticator, "viewAuthenticator");
  }

  @Override
  public Boolean isPermitted(PersistentRecord parentNode,
                             PersistentRecord nodeGroupStyle,
                             PersistentRecord user)
  {
    return Boolean.valueOf(isPermitted(parentNode, user));
  }

  @Override
  public String getRejectionMessage(PersistentRecord parentNode,
                                    PersistentRecord nodeGroupStyle,
                                    PersistentRecord user)
  {
    if (isPermitted(parentNode, user)) {
      return null;
    }
    return nodeGroupStyle.comp(NodeGroupStyle.NAME) + " creation rejected by "
           + __viewAuthenticator;
  }

  private boolean isPermitted(PersistentRecord parentNode,
                              PersistentRecord user)
  {
    return __viewAuthenticator.isAllowed(user.getId(), parentNode);
  }

  @Override
  public String toString(PersistentRecord parentNode,
                         PersistentRecord nodeGroupStyle)
  {
    return MoreObjects.toStringHelper(this)
                      .add("viewAuthenticator", __viewAuthenticator)
                      .add("parentNode", parentNode)
                      .add("nodeGroupStyle", nodeGroupStyle)
                      .toString();
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("viewAuthenticator", __viewAuthenticator)
                      .toString();
  }

}
