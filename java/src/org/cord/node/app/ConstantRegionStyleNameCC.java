package org.cord.node.app;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Viewpoint;
import org.cord.node.Node;
import org.cord.node.RegionStyle;
import org.cord.node.cc.StyleTableWrapper;
import org.cord.util.Assertions;
import org.cord.util.LogicException;
import org.cord.util.NamespacedNameCache;
import org.cord.util.NamespacedNameCache.Ordering;

public abstract class ConstantRegionStyleNameCC<A extends AppMgr>
  extends AppCC<A>
{
  private final String __regionStyleName;

  private final NamespacedNameCache __namespacedNameCache;

  public ConstantRegionStyleNameCC(A appMgr,
                                   String name,
                                   TemplatePaths templatePaths,
                                   StyleTableWrapper styleTableWrapper,
                                   String regionStyleName)
  {
    super(appMgr,
          name,
          templatePaths,
          styleTableWrapper);
    __regionStyleName = Assertions.notEmpty(regionStyleName, "regionStyleName");
    __namespacedNameCache =
        new NamespacedNameCache(__regionStyleName + '-', Ordering.PREFIXNAMESPACE);
  }

  public ConstantRegionStyleNameCC(A appMgr,
                                   String name,
                                   TemplatePaths templatePaths,
                                   String regionStyleName)
  {
    this(appMgr,
         name,
         templatePaths,
         null,
         regionStyleName);
  }

  public final String getRegionStyleName()
  {
    return __regionStyleName;
  }

  public final String getNamespacedName(Object key)
  {
    return __namespacedNameCache.getNamespacedName(key.toString());
  }

  @Override
  protected void internalDeclare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    if (!getRegionStyleName().equals(regionStyle.opt(RegionStyle.NAME))) {
      throw new LogicException(String.format("%s.name is %s rather than %s",
                                             regionStyle,
                                             regionStyle.opt(RegionStyle.NAME),
                                             getRegionStyleName()));
    }
    super.internalDeclare(regionStyle);
  }

  public final PersistentRecord getCompInstance(PersistentRecord node,
                                                Viewpoint viewpoint)
  {
    try {
      return getInstance(node, viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      throw new LogicException(String.format("Cannot resolve %s under %s at %s",
                                             this,
                                             node,
                                             node.opt(Node.PATH)),
                               e);
    }
  }

  public final PersistentRecord getOptInstance(PersistentRecord node,
                                               Viewpoint viewpoint)
  {
    try {
      return getInstance(node, viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      return null;
    }
  }

  public final PersistentRecord getInstance(PersistentRecord node,
                                            Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return getInstance(node, getRegionStyleName(), null, viewpoint);
  }
}
