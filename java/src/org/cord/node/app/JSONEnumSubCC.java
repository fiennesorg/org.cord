package org.cord.node.app;

import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class JSONEnumSubCC<A extends AppMgr, T extends Enum<T>>
  extends JSONSubCC<A, T>
{
  private final Class<T> __valueClass;

  public JSONEnumSubCC(AppCC<A> appCC,
                       RecordOperationKey<JSONObject> asJsonKey,
                       String label,
                       RecordOperationKey<T> jsonKey,
                       boolean isOptional,
                       Supplier<T> defaultValue,
                       Preprocessor<T> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
    __valueClass = jsonKey.getValueClass();
  }

  public JSONEnumSubCC(AppCC<A> appCC,
                       RecordOperationKey<JSONObject> asJsonKey,
                       String label,
                       String jsonKey,
                       Class<T> valueClass,
                       boolean isOptional,
                       Supplier<T> defaultValue,
                       Preprocessor<T> preprocessor)
  {
    super(appCC,
          asJsonKey,
          label,
          jsonKey,
          isOptional,
          defaultValue,
          preprocessor);
    __valueClass = valueClass;
  }

  @Override
  protected T getValue(TransientRecord instance)
  {
    String jsonValue = asJsonObject(instance).optString(getJSONKey());
    if (jsonValue != null) {
      try {
        return Enum.valueOf(__valueClass, jsonValue);
      } catch (RuntimeException e) {
      }
    }
    return isOptional() ? null : getDefaultValue();
  }

  @Override
  protected T getValue(Gettable inputs)
  {
    String value = inputs.getString(getName());
    if (value != null) {
      try {
        return Enum.valueOf(__valueClass, value);
      } catch (RuntimeException e) {
      }
    }
    return null;
  }

  @Override
  public void appendEditWidget(PersistentRecord instance,
                               String webmacroInstanceName,
                               PersistentRecord user,
                               StringBuilder buf)
      throws IOException
  {
    final String value = getValue(instance).name();
    HtmlUtils.openSelect(buf, false, false, false, instance.opt(AppCC.NAMEPREFIX), getName());
    for (T option : __valueClass.getEnumConstants()) {
      HtmlUtils.option(buf, option.name(), option.toString(), option.name().equals(value));
    }
    HtmlUtils.closeSelect(buf, null);
  }

  @Override
  protected Object toJsonValue(T value)
  {
    return value.name();
  }
}
