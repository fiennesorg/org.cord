package org.cord.node.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.cord.mirror.Db;
import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeFilter;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.ValueFactory;
import org.cord.node.cc.StyleTableWrapper;
import org.cord.node.cc.StylelessTableContentCompilerDb;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.cc.TableContentCompilerDb;
import org.cord.util.Assertions;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Context;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.engine.StringTemplate;
import org.webmacro.engine.WMTemplate;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public abstract class AppCC<A extends AppMgr>
  extends TableContentCompiler
  implements AppComponent<A>, TableWrapper
{
  public static final String VIEWTEMPLATEPATH_DEFAULT = "node/App/AppCC.view.wm.html";
  public static final String EDITTEMPLATEPATH_DEFAULT = "node/App/AppCC.edit.wm.html";

  public static final RecordOperationKey<String> DOCTEMPLATEPATH =
      RecordOperationKey.create("docTemplatePath", String.class);
  public static final String DOCTEMPLATEPATH_DEFAULT = "node/App/AppCC.doc.wm.html";

  public static final RecordOperationKey<Macro> RENDEREDITTEMPLATE =
      RecordOperationKey.create("renderEditTemplate", Macro.class);

  public final static MonoRecordOperationKey<String, Macro> RENDEREDITTEMPLATEFOR =
      MonoRecordOperationKey.create("renderEditTemplateFor", String.class, Macro.class);

  private final A __appMgr;

  private final List<SubCC<A>> __subCCs = new ArrayList<SubCC<A>>();

  private final List<SubCC<A>> __roSubCCs = Collections.unmodifiableList(__subCCs);

  private AppCC(A appMgr,
                String name,
                StyleTableWrapper styleTableWrapper,
                TemplatePaths templatePaths,
                Iterable<FieldKey<?>> strings,
                Iterable<FieldKey<Boolean>> booleans,
                Iterable<FieldKey<Date>> fastDates)
  {
    super(name,
          appMgr.getNodeMgr(),
          appMgr.getPwd(),
          new InitTableCallback(appMgr,
                                name,
                                name,
                                styleTableWrapper == null
                                    ? new StyleTableWrapper(StylelessTableContentCompilerDb.ABSTRACTSTYLE)
                                    : styleTableWrapper,
                                templatePaths),
          strings,
          false,
          booleans,
          fastDates);
    __appMgr = appMgr;
  }

  public AppCC(A appMgr,
               String name,
               TemplatePaths templatePaths,
               StyleTableWrapper styleTableWrapper)
  {
    this(appMgr,
         name,
         styleTableWrapper,
         templatePaths,
         // TableContentCompiler.LINK_NONE,
         // TableContentCompiler.LINK_NONE,
         // TableContentCompiler.LINK_NONE,
         null,
         null,
         null);
  }

  public AppCC(A appMgr,
               String name,
               TemplatePaths templatePaths)
  {
    this(appMgr,
         name,
         templatePaths,
         null);
  }

  /**
   * @see #getInstanceTable()
   */
  @Override
  public final Table getTable()
  {
    return getInstanceTable();
  }

  /**
   * @see #getInstanceTableName()
   */
  @Override
  public final String getTableName()
  {
    return getInstanceTableName();
  }

  private String _docTemplatePath = DOCTEMPLATEPATH_DEFAULT;

  protected void setDocTemplatePath(String docTemplatePath)
  {
    _docTemplatePath =
        Strings.isNullOrEmpty(docTemplatePath) ? DOCTEMPLATEPATH_DEFAULT : docTemplatePath;
  }

  public String getDocTemplatePath()
  {
    return _docTemplatePath;
  }

  public final List<SubCC<A>> getSubCCs()
  {
    return __roSubCCs;
  }

  public final SubCC<A> getSubCc(String name)
  {
    Assertions.notEmpty(name, "Illegal SubCC name of \"%s\"", name);
    for (SubCC<A> subCc : getSubCCs()) {
      if (name.equals(subCc.getName())) {
        return subCc;
      }
    }
    throw new NoSuchElementException(String.format("No SubCC named \"%s\" found on %s",
                                                   name,
                                                   this));
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

  protected SubCC<A> addSubCc(Table instances,
                              SubCC<A> subCc)
      throws DuplicateNamedException, MirrorTableLockedException
  {
    Preconditions.checkNotNull(subCc, "subCc");
    __subCCs.add(subCc);
    subCc.init(instances.getDb(), getAppMgr().getPwd(), instances);
    return subCc;
  }

  /**
   * Create an {@link FieldSubCC} that wraps the given field and register it on this AppCC
   */
  protected final <T> Field<T> addFieldSubCC(Table instances,
                                             Field<T> field)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new FieldSubCC<A, T>(this, field));
    return field;
  }

  protected final <T> Field<T> addInstantiationFieldSubCC(Table instances,
                                                          Field<T> field)
      throws MirrorTableLockedException
  {
    addSubCc(instances, new FieldSubCC<A, T>(this, field, true));
    return field;
  }

  @Override
  protected void internalDeclare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.internalDeclare(regionStyle);
    for (SubCC<A> subCC : getSubCCs()) {
      subCC.declare(regionStyle);
    }
  }

  @Override
  protected boolean updatePersistentInstance(PersistentRecord node,
                                             PersistentRecord regionStyle,
                                             PersistentRecord instance,
                                             PersistentRecord style,
                                             Transaction transaction,
                                             Gettable inputs,
                                             Map<Object, Object> outputs,
                                             PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    boolean result = super.updatePersistentInstance(node,
                                                    regionStyle,
                                                    instance,
                                                    style,
                                                    transaction,
                                                    inputs,
                                                    outputs,
                                                    user);
    for (SubCC<A> subCC : getSubCCs()) {
      result = result | subCC.updatePersistentInstance(node,
                                                       regionStyle,
                                                       instance,
                                                       style,
                                                       transaction,
                                                       inputs,
                                                       outputs,
                                                       user);
    }
    return result;
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      FeedbackErrorException
  {
    boolean result = super.updateTransientInstance(node,
                                                   regionStyle,
                                                   instance,
                                                   style,
                                                   transaction,
                                                   inputs,
                                                   outputs,
                                                   user);
    for (SubCC<A> subCC : getSubCCs()) {
      result = result | subCC.updateTransientInstance(node,
                                                      regionStyle,
                                                      instance,
                                                      style,
                                                      transaction,
                                                      inputs,
                                                      outputs,
                                                      user);
    }
    return result;
  }

  protected abstract void init(Db db,
                               String pwd,
                               Table instances)
      throws MirrorTableLockedException;

  protected void initStyleTable(Db db,
                                String pwd,
                                Table styleTable)
  {
  }

  protected void appendEditTemplateHeader(StringBuilder buf,
                                          String webmacroInstanceName)
  {
    buf.append("<fieldset>\n");
    buf.append("#include as template \"node/abstractContentCompilerHeader.edit.wm.html\"\n");
    buf.append("#if ($").append(webmacroInstanceName).append(".isDefined) {\n");
  }

  protected void appendEditTemplateFooter(StringBuilder buf,
                                          String webmacroInstanceName)
  {
    buf.append("}\n");
    buf.append("</fieldset>\n");
  }

  public String getEditTemplateSource(PersistentRecord instance,
                                      String webmacroInstanceName,
                                      PersistentRecord user,
                                      String... subCcNames)
  {
    StringBuilder buf = new StringBuilder();
    appendEditTemplateHeader(buf, webmacroInstanceName);
    if (subCcNames.length == 0) {
      for (SubCC<A> subCc : getSubCCs()) {
        buf.append(subCc.getEditTemplate(instance, webmacroInstanceName, user));
      }
    } else {
      for (String subCcName : subCcNames) {
        buf.append(getSubCc(subCcName).getEditTemplate(instance, webmacroInstanceName, user));
      }
    }
    appendEditTemplateFooter(buf, webmacroInstanceName);
    return buf.toString();
  }

  public String getEditTemplateSource(PersistentRecord compiledRegion,
                                      PersistentRecord user)
  {
    return getEditTemplateSource(compiledRegion, "compiledRegion", user);
  }

  public WMTemplate getSubCcEditTemplate(PersistentRecord instance,
                                         String webmacroInstanceName,
                                         PersistentRecord user,
                                         String subCcName)
  {
    return new StringTemplate(getNodeMgr().getWebMacro().getBroker(),
                              getSubCc(subCcName).getEditTemplate(instance,
                                                                  webmacroInstanceName,
                                                                  user));
  }

  public WMTemplate getEditTemplate(PersistentRecord instance,
                                    String webmacroInstanceName,
                                    PersistentRecord user,
                                    String... subCcNames)
  {
    return new StringTemplate(getNodeMgr().getWebMacro().getBroker(),
                              getEditTemplateSource(instance,
                                                    webmacroInstanceName,
                                                    user,
                                                    subCcNames));

  }

  public WMTemplate getEditTemplate(PersistentRecord instance,
                                    PersistentRecord user)
  {
    return getEditTemplate(instance, "compiledRegion", user);
  }

  @Override
  protected void registerLinkQuestion(PersistentRecord nodeStyle,
                                      String askQuestionName,
                                      ValueFactory askQuestionUrl,
                                      ValueFactory questionEnglishFactory,
                                      int maxAnswers,
                                      NodeFilter eligibleNodeFilter,
                                      TableContentCompiler targetContentCompiler,
                                      String targetName,
                                      String linkFieldName,
                                      String thisName,
                                      RecordOperationKey<RecordSource> linkRecordsName,
                                      RecordOperationKey<PersistentRecord> linkTargetName)
      throws MirrorNoSuchRecordException
  {
    super.registerLinkQuestion(nodeStyle,
                               askQuestionName,
                               askQuestionUrl,
                               questionEnglishFactory,
                               maxAnswers,
                               eligibleNodeFilter,
                               targetContentCompiler,
                               targetName,
                               linkFieldName,
                               thisName,
                               linkRecordsName,
                               linkTargetName);
  }

  private static class InitTableCallback
    extends TableContentCompilerDb
  {
    public InitTableCallback(AppMgr appMgr,
                             String name,
                             String instanceTableName,
                             StyleTableWrapper styleTableWrapper,
                             TemplatePaths templatePaths)
    {
      super(name,
            instanceTableName,
            null,
            0,
            styleTableWrapper,
            templatePaths.getViewTemplatePath(),
            templatePaths.getEditTemplatePath());
    }

    public AppCC<?> getAppCC()
    {
      return ((AppCC<?>) getTableContentCompiler());
    }

    @Override
    protected void internalInit(Db db,
                                String pwd,
                                Table instances)
        throws MirrorTableLockedException
    {
      getAppCC().init(db, pwd, instances);
      instances.addRecordOperation(new SimpleRecordOperation<String>(DOCTEMPLATEPATH) {
        @Override
        public String getOperation(TransientRecord targetRecord,
                                   Viewpoint viewpoint)
        {
          return getAppCC().getDocTemplatePath();
        }
      });
      instances.addRecordOperation(new SimpleRecordOperation<Macro>(RENDEREDITTEMPLATE) {
        @Override
        public Macro getOperation(final TransientRecord targetRecord,
                                  final Viewpoint viewpoint)
        {
          return new AppendableParametricMacro() {
            @Override
            public void append(Appendable out,
                               Map<Object, Object> context)
                throws IOException
            {
              try {
                out.append(getAppCC().getEditTemplate((PersistentRecord) targetRecord,
                                                      NodeRequest.getNodeRequest(context).getUser())
                                     .evaluateAsString((Context) context));
              } catch (PropertyException e) {
                throw new LogicException("Unable to evaluate edit template", e);
              }
            }
          };
        }
      });
      instances.addRecordOperation(new MonoRecordOperation<String, Macro>(RENDEREDITTEMPLATEFOR,
                                                                          Casters.TOSTRING) {
        @Override
        public Macro calculate(final TransientRecord instance,
                               final Viewpoint viewpoint,
                               final String subCcName)
        {
          return new AppendableParametricMacro() {

            @Override
            public void append(Appendable out,
                               Map<Object, Object> context)
                throws IOException
            {
              try {
                out.append(getAppCC().getSubCcEditTemplate((PersistentRecord) instance,
                                                           "compiledRegion",
                                                           NodeRequest.getNodeRequest(context)
                                                                      .getUser(),
                                                           subCcName)
                                     .evaluateAsString((Context) context));
              } catch (PropertyException e) {
                throw new LogicException("Unable to evalute subCC edit template", e);
              }
            }
          };
        }
      });
    }
  }

}
