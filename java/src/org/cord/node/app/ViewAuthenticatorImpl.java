package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.AuthenticationException;
import org.cord.node.NodeRequest;

/**
 * Utility base class for implementing ViewAuthenticators where everything delegates to
 * {@link #isAllowed(int, PersistentRecord)}
 * 
 * @author alex
 * @param <A>
 */
public abstract class ViewAuthenticatorImpl<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  public ViewAuthenticatorImpl(A appMgr)
  {
    super(appMgr);
  }

  /**
   * Authenticate against {@link #isAllowed(NodeRequest, PersistentRecord)} and if it fails then
   * throw an AuthenticationException using
   * {@link #getAuthenticationExceptionMessage(NodeRequest, PersistentRecord)} for the message.
   */
  @Override
  public final PersistentRecord authenticate(NodeRequest nodeRequest,
                                             PersistentRecord node)
      throws AuthenticationException
  {
    if (!isAllowed(nodeRequest, node)) {
      throw new AuthenticationException(getAuthenticationExceptionMessage(nodeRequest, node),
                                        node,
                                        null);
    }
    return null;
  }

  /**
   * Generate the message to be used in the AuthenticationException that will be thrown by
   * {@link #authenticate(NodeRequest, PersistentRecord)} if the authentication fails.
   * 
   * @return "Authentication failure."
   */
  protected String getAuthenticationExceptionMessage(NodeRequest nodeRequest,
                                                     PersistentRecord node)
  {
    return "Authentication failure.";
  }

  /**
   * @return the result of invoking {@link #isAllowed(int, PersistentRecord)} with
   *         {@link NodeRequest#getUserId()}. Override this if you want different behaviour.
   */
  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return isAllowed(nodeRequest.getUserId(), node);
  }

  @Override
  public abstract boolean isAllowed(int userId,
                                    PersistentRecord node);

}
