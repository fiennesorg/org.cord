package org.cord.node.app;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.messages.MessageMgr;
import org.cord.node.messages.Messages;
import org.cord.node.messages.Validators;
import org.webmacro.Context;

import com.google.common.base.Predicate;

public abstract class FormAppValidatingView<A extends AppMgr>
  extends FormAppView<A>
{
  private final MessageMgr<A> __messageMgr;

  public FormAppValidatingView(A appMgr,
                               MessageMgr<A> messageMgr,
                               String name,
                               String title,
                               ViewAuthenticator<? super A> viewAuthenticator)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator);
    __messageMgr = messageMgr;
  }

  public final MessageMgr<A> getMessageMgr()
  {
    return __messageMgr;
  }

  @Override
  protected final NodeRequestSegment renderForm(NodeRequest nodeRequest,
                                                PersistentRecord node,
                                                Context context)
      throws NodeRequestException
  {
    Messages<A> messages = getMessageMgr().getMessages(nodeRequest.getSession());
    return renderForm(nodeRequest, node, context, messages);
  }

  protected NodeRequestSegment renderForm(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          Messages<A> messages)
      throws NodeRequestException
  {
    return super.renderForm(nodeRequest, node, context);
  }

  @Override
  protected final NodeRequestSegment processForm(NodeRequest nodeRequest,
                                                 PersistentRecord node,
                                                 Context context,
                                                 PersistentRecord acl)
      throws NodeRequestException
  {
    Messages<A> messages = getMessageMgr().getMessages(nodeRequest.getSession());
    validateInputs(nodeRequest, node, context, acl, messages);
    if (messages.size() > 0) {
      return renderForm(nodeRequest, node, context, messages);
    }
    return processValidatedForm(nodeRequest, node, context, acl, messages);
  }

  /**
   * Check that the inputs as provided are valid placing any errors into the Messages stack. This
   * should have no side effects other than placing Messages. If there are any Messages added then
   * they will be returned to the user and
   * {@link #processValidatedForm(NodeRequest, PersistentRecord, Context, PersistentRecord, Messages)}
   * will not get invoked.
   */
  protected abstract void validateInputs(NodeRequest nodeRequest,
                                         PersistentRecord node,
                                         Context context,
                                         PersistentRecord acl,
                                         Messages<A> messages)
      throws NodeRequestException;

  /**
   * Do the work associated with this view after all the inputs have been validated by
   * {@link #processForm(NodeRequest, PersistentRecord, Context, PersistentRecord)}
   */
  protected abstract NodeRequestSegment processValidatedForm(NodeRequest nodeRequest,
                                                             PersistentRecord node,
                                                             Context context,
                                                             PersistentRecord acl,
                                                             Messages<A> messages)
      throws NodeRequestException;

  /**
   * @param messageValueText
   *          The text that will be returned in the error Message if the validation fails. This will
   *          be parametric formatted with value available as %s.
   */
  protected <T> T validate(T value,
                           Predicate<T> isValid,
                           Messages<A> messages,
                           String key,
                           String messageValueText)
  {
    return Validators.validate(value,
                               isValid,
                               messages,
                               Validators.getMessageGroupingKey(this, key),
                               getTitle(),
                               messageValueText);
  }
}
