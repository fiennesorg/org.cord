package org.cord.node.app;

import java.util.Map;

import org.cord.mirror.Field;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.Node;
import org.cord.node.RegionStyle;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class NodeControlsInstanceSubCC<A extends AppMgr, V>
  extends NodeInstanceRelationshipSubCC<A, V>
{
  public NodeControlsInstanceSubCC(AppCC<A> appCC,
                                   FieldKey<V> nodeKey,
                                   Field<V> appCCField)
  {
    super(appCC,
          nodeKey,
          appCCField.setMayHaveHtmlWidget(false));
  }

  @Override
  protected void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    super.declare(regionStyle);
    getNodeMgr().getNode()
                .getTable()
                .addFieldTrigger(new NodeToInstanceTrigger(regionStyle.compInt(RegionStyle.NODESTYLE_ID),
                                                           regionStyle.comp(RegionStyle.NAME),
                                                           getNodeKey(),
                                                           getInstanceKey()));
  }

  /**
   * Do nothing because any update will come changes to the controlling Node field.
   * 
   * @return false
   */
  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    return false;
  }

  private class NodeToInstanceTrigger
    extends AbstractFieldTrigger<V>
  {
    private final int __nodeStyleId;

    private final String __regionStyleName;

    private final FieldKey<V> __instanceFieldName;

    public NodeToInstanceTrigger(int nodeStyleId,
                                 String regionStyleName,
                                 FieldKey<V> nodeFieldName,
                                 FieldKey<V> instanceFieldName)
    {
      super(nodeFieldName,
            false,
            false);
      Preconditions.checkNotNull(instanceFieldName, "instanceFieldName");
      Preconditions.checkNotNull(regionStyleName, "regionStyleName");
      __nodeStyleId = nodeStyleId;
      __regionStyleName = regionStyleName;
      __instanceFieldName = instanceFieldName;
    }

    @Override
    public String toString()
    {
      return MoreObjects.toStringHelper(this)
                        .add("nodeStyleId", __nodeStyleId)
                        .add("regionStyleName", __regionStyleName)
                        .add("instanceFieldName", __instanceFieldName)
                        .toString();
    }

    @Override
    public Object triggerField(TransientRecord node,
                               String fieldName,
                               Transaction transaction)
        throws MirrorFieldException
    {
      PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
      if (nodeStyle.getId() == __nodeStyleId) {
        try {
          PersistentRecord roInstance =
              getAppCC().getInstance((WritableRecord) node, __regionStyleName, null, transaction);
          try {
            WritableRecord rwInstance = (WritableRecord) roInstance;
            rwInstance.setField(__instanceFieldName, node.opt(getFieldName()));
          } catch (ClassCastException ccEx) {
            // the node wasn't locked!!!
            DebugConstants.DEBUG_OUT.println();
            DebugConstants.method(this, "triggerField", node, transaction);
            DebugConstants.variable("roInstance", roInstance);
            DebugConstants.variable("transaction.debug()", transaction.debug());
            DebugConstants.DEBUG_OUT.println();
            throw new LogicException("Invalid object lock on " + node, ccEx);
          }
        } catch (MirrorNoSuchRecordException mnsrEx) {
          throw new MirrorFieldException("Error resolving AbstractContentCompilerFieldLinker",
                                         mnsrEx,
                                         node,
                                         getName(),
                                         node.opt(getFieldName()),
                                         null);
        }
      }
      return null;
    }
  }

}
