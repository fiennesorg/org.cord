package org.cord.node.app;

import java.util.List;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.MissingRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeManagerFactory;
import org.cord.util.GettableFactory;
import org.cord.util.SettableMap;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class AppMgrBootup<A extends AppMgr>
  extends NodeManagerFactory
  implements AppComponent<A>
{
  private final A __appMgr;

  public AppMgrBootup(A appMgr)
  {
    __appMgr = Preconditions.checkNotNull(appMgr, "appMgr");
  }

  @Override
  public final A getAppMgr()
  {
    return __appMgr;
  }

  @Override
  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }

  @Override
  protected void initialise(GettableFactory gettableFactory,
                            NodeManager nodeManager,
                            String transactionPassword)
      throws MirrorTableLockedException, MirrorTransactionTimeoutException
  {
    getAppMgr().initialise(gettableFactory, nodeManager, transactionPassword);
  }

  @Override
  protected void lock()
  {
    getAppMgr().lock();
  }

  /**
   * Create and return a published Node of the given style as a child of the given Node with
   * optional custom params.
   * 
   * @param isMenuItem
   *          whether the created Node should have isMenuItem set to true and appear in the
   *          navigation structure. Setting this to null will delegate to the default behaviour
   *          defined in the appropriate NodeStyle.
   */
  protected PersistentRecord createNode(PersistentRecord parentNode,
                                        int childNodeStyleId,
                                        Boolean isMenuItem,
                                        SettableMap params,
                                        String name)
      throws MissingRecordException, MirrorFieldException, MirrorInstantiationException,
      MirrorTransactionTimeoutException, MirrorNoSuchRecordException, FeedbackErrorException
  {
    name = name.trim();
    params = SettableMap.newIfNull(params);
    params.putIfAbsent(Node.ISPUBLISHED, Boolean.TRUE);
    params.putIfAbsent(Node.HTMLTITLE, name);
    params.putIfAbsent(Node.FILENAME, name);
    params.add(Node.TITLE, name).add(Node.ISMENUITEM, isMenuItem);
    return getNodeMgr().getNode()
                       .createAndUpdateNewNode(parentNode.getId(),
                                               getNodeMgr().getNodeGroupStyle()
                                                           .getCompFirstInstance(parentNode.compInt(Node.NODESTYLE_ID),
                                                                                 childNodeStyleId,
                                                                                 null)
                                                           .getId(),
                                               params,
                                               null,
                                               getAppMgr().getPwd());
  }

  /**
   * Create a sequence of named published Nodes of the given style as a children of the given Node
   * with optional shared custom params and return a List of the created Node records.
   * 
   * @param isMenuItem
   *          whether the created Nodes should have isMenuItem set to true and appear in the
   *          navigation structure. Setting this to null will delegate to the default behaviour
   *          defined in the appropriate NodeStyle.
   * @throws FeedbackErrorException
   */
  protected List<PersistentRecord> createNodes(PersistentRecord parentNode,
                                               int childNodeStyleId,
                                               Boolean isMenuItem,
                                               SettableMap params,
                                               String... names)
      throws MissingRecordException, MirrorFieldException, MirrorInstantiationException,
      MirrorTransactionTimeoutException, MirrorNoSuchRecordException, FeedbackErrorException
  {
    List<PersistentRecord> nodes = Lists.newArrayList();
    for (String name : names) {
      nodes.add(createNode(parentNode, childNodeStyleId, isMenuItem, params, name));
    }
    return nodes;
  }

  /**
   * Utility shortcut method to resolve the root node in the current tree.
   */
  public PersistentRecord getRootNode()
  {
    return getNodeMgr().getNode().getRootNode(null);
  }
}
