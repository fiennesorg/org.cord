package org.cord.node.app;

import java.util.ArrayList;
import java.util.List;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public class Auth_And<A extends AppMgr>
  extends AppComponentImpl<A>
  implements ViewAuthenticator<A>
{
  private final List<ViewAuthenticator<?>> __viewAuthenticators =
      new ArrayList<ViewAuthenticator<?>>();

  private Auth_And(A appMgr)
  {
    super(appMgr);
  }

  /**
   * @return The result of the last authenticate method on the sub-authenticator methods.
   */
  @Override
  public PersistentRecord authenticate(NodeRequest nodeRequest,
                                       PersistentRecord node)
      throws NodeRequestException
  {
    PersistentRecord acl = null;
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      acl = viewAuthenticator.authenticate(nodeRequest, node);
    }
    return acl;
  }

  @Override
  public boolean isAllowed(int userId,
                           PersistentRecord node)
  {
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      if (!viewAuthenticator.isAllowed(userId, node)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    for (ViewAuthenticator<?> viewAuthenticator : __viewAuthenticators) {
      if (!viewAuthenticator.isAllowed(nodeRequest, node)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this).add("of", __viewAuthenticators).toString();
  }

  public static <A extends AppMgr> Builder<A> and(ViewAuthenticator<A> auth)
  {
    Builder<A> builder = new Builder<A>(auth.getAppMgr());
    builder.and(auth);
    return builder;
  }

  public static class Builder<A extends AppMgr>
    extends AbstractBuilder<Auth_And<A>>
  {
    private final Auth_And<A> __instance;

    public Builder(A appMgr)
    {
      __instance = new Auth_And<A>(appMgr);
    }

    public Builder<A> and(ViewAuthenticator<? extends A> viewAuthenticator)
    {
      assertNotBuilt();
      Preconditions.checkNotNull(viewAuthenticator, "viewAuthenticator");
      __instance.__viewAuthenticators.add(viewAuthenticator);
      return this;
    }

    @Override
    protected Auth_And<A> buildImpl()
    {
      return __instance;
    }
  }
}
