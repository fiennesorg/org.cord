package org.cord.node.app;

import java.io.IOException;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.CurrentUrlFactory;
import org.cord.node.MonoNodeQuestion;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.view.AskNodeQuestion;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringUtils;
import org.json.JSONObject;
import org.webmacro.Context;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public class JSONNodeLinkSubCC<A extends AppMgr>
  extends JSONIntegerSubCC<A>
{
  private final RecordOperationKey<PersistentRecord> __nodeKey;

  public static final String VIEW_JSONNODELINKSUBCC_BROWSE = "JSONNodeLinkSubCC_browse";
  public static final String IN_BROWSE_VARNAME = "varName";
  public static final String IN_BROWSE_QUESTION = "question";

  private final String __varName;

  public JSONNodeLinkSubCC(ConstantRegionStyleNameCC<A> appCC,
                           RecordOperationKey<JSONObject> asJsonKey,
                           String label,
                           RecordOperationKey<Integer> recordAlias,
                           RecordOperationKey<PersistentRecord> nodeKey,
                           boolean isOptional,
                           Supplier<Integer> defaultValue,
                           Preprocessor<Integer> preprocessor)
      throws MirrorTableLockedException
  {
    super(appCC,
          asJsonKey,
          label,
          recordAlias,
          isOptional,
          defaultValue,
          preprocessor);
    __varName = StringUtils.urlEncodeUtf8(String.format("%s-%s_%s",
                                                        appCC.getRegionStyleName(),
                                                        asJsonKey,
                                                        recordAlias));
    __nodeKey = Preconditions.checkNotNull(nodeKey, "nodeKey");
    OpenTargetSearch.init(appCC.getNodeMgr());
    appCC.getInstanceTable()
         .addRecordOperation(new SimpleRecordOperation<PersistentRecord>(__nodeKey) {
           @Override
           public PersistentRecord getOperation(TransientRecord record,
                                                Viewpoint viewpoint)
           {
             return resolveNode(record, viewpoint);
           }
         });
  }

  public PersistentRecord resolveNode(TransientRecord record,
                                      Viewpoint viewpoint)
  {
    return getNodeMgr().getNode().getTable().getOptRecord(getValue(record), viewpoint);
  }

  public final RecordOperationKey<PersistentRecord> getNodeKey()
  {
    return __nodeKey;
  }

  @Override
  protected void appendEditWidget(PersistentRecord instance,
                                  String webmacroInstanceName,
                                  PersistentRecord user,
                                  StringBuilder buf)
      throws IOException
  {
    StringBuilder value = new StringBuilder();
    PersistentRecord targetNode = resolveNode(instance, null);
    if (targetNode == null) {
      value.append("undefined");
    } else {
      value.append(targetNode.opt(Node.HREF, Node.PATH.getKeyName()));
      HtmlUtilsTheme.hiddenInput(buf,
                                 Integer.toString(targetNode.getId()),
                                 instance.comp(TableContentCompiler.NAMEPREFIX),
                                 getName());
    }
    PersistentRecord containingNode = instance.comp(AppCC.NODE);
    value.append("<br />\n");
    value.append("<a href=\"?view=" + VIEW_JSONNODELINKSUBCC_BROWSE + "&" + IN_BROWSE_VARNAME + "=")
         .append(__varName)
         .append("&" + IN_BROWSE_QUESTION + "=")
         .append(StringUtils.urlEncodeUtf8(String.format("What is the target page for \"%s\" on %s (%s)?",
                                                         getLabel(),
                                                         containingNode.comp(Node.TITLE),
                                                         containingNode.comp(Node.PATH))))
         .append("\">Browse...</a>");
    HtmlUtils.value(buf,
                    value.toString(),
                    instance.opt(TableContentCompiler.NAMEPREFIX),
                    getName());
  }

  static class OpenTargetSearch
    extends AskNodeQuestion
  {
    private static boolean _isInit = false;

    protected static void init(NodeManager nodeMgr)
    {
      if (!_isInit) {
        nodeMgr.getNodeRequestSegmentManager().register(Transaction.TRANSACTION_UPDATE,
                                                        new OpenTargetSearch(nodeMgr));
        _isInit = true;
      }
    }

    private OpenTargetSearch(NodeManager nodeManager)
    {
      super(VIEW_JSONNODELINKSUBCC_BROWSE,
            nodeManager,
            new RedirectionSuccessOperation(nodeManager, CurrentUrlFactory.getInstance()),
            Node.EDITACL,
            "You don't have permission to browse the target");
    }

    @Override
    protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                              final PersistentRecord node,
                                              Context context)
        throws MirrorNoSuchRecordException
    {
      final String varName =
          Preconditions.checkNotNull(nodeRequest.getString(IN_BROWSE_VARNAME), IN_BROWSE_VARNAME);
      final String isDefined = varName.substring(0, varName.indexOf('-'));
      final String question =
          Preconditions.checkNotNull(nodeRequest.getString(IN_BROWSE_QUESTION), IN_BROWSE_QUESTION);
      return new MonoNodeQuestion("JSONNodeLinkSubCC-" + node.getId() + "-" + varName,
                                  question,
                                  "JSONNodeLinkSubCC page resolution",
                                  node,
                                  null,
                                  getTransaction(nodeRequest, node)) {

        @Override
        public String getAnswerUrl()
        {
          StringBuilder url = new StringBuilder();
          url.append(node.comp(Node.URL))
             .append("?view=update&")
             .append(isDefined)
             .append("=true&")
             .append(varName)
             .append("=")
             .append(getSelectedNodeId());
          return url.toString();
        }
      };
    }
  }
}
