package org.cord.node.app;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.node.NodeManager;

import com.google.common.base.Preconditions;

/**
 * Abstract class that attaches an instance of an AppMgr onto an implementation of an
 * AbstractTableWrapper.
 */
public abstract class AppTableWrapper<A extends AppMgr>
  extends AbstractTableWrapper
{
  private final A __appMgr;

  public AppTableWrapper(A appMgr,
                         String tableName)
  {
    super(tableName);
    __appMgr = Preconditions.checkNotNull(appMgr, "appMgr");
  }

  public final A getAppMgr()
  {
    return __appMgr;
  }

  public final NodeManager getNodeMgr()
  {
    return getAppMgr().getNodeMgr();
  }
}
