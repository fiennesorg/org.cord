package org.cord.node;

public class ConstantValueFactory
  implements ValueFactory
{
  private final String __value;

  public ConstantValueFactory(String value)
  {
    __value = value;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    return __value;
  }
}