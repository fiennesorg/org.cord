package org.cord.node;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.PostInstantiationTriggerImpl;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;

import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;

@Deprecated
public class UnpublishedNodeTracker
{
  private final Table __nodeTable;

  private final Table __nodeStyleTable;

  private final IntSet __rootNodeStyleIds = IntSets.synchronize(new IntOpenHashSet());
  private final IntSet __publicRootNodeStyleIds = IntSets.unmodifiable(__rootNodeStyleIds);

  private final IntSet __unpublishedNodeIds = IntSets.synchronize(new IntOpenHashSet());
  private final IntSet __publicUnpublishedNodeIds = IntSets.unmodifiable(__unpublishedNodeIds);

  private final IntSet __inheritedNodeIds = IntSets.synchronize(new IntOpenHashSet());
  private final IntSet __publicInheritedNodeIds = IntSets.unmodifiable(__inheritedNodeIds);

  private String _inheritedNodeIdListing = null;

  private String _unpublishedNodeIdListing = null;

  public UnpublishedNodeTracker(Db db) throws MirrorTableLockedException
  {
    __nodeTable = db.getTable(Node.TABLENAME);
    __nodeTable.addStateTrigger(new UpdatedNodeTrigger());
    __nodeTable.addStateTrigger(new DeletedNodeTrigger());
    __nodeTable.addPostInstantiationTrigger(new CreatedNodeTrigger());
    __nodeStyleTable = db.getTable(NodeStyle.TABLENAME);
    __nodeStyleTable.addStateTrigger(new DeletedNodeStyleTrigger());
    db.getTable(NodeGroupStyle.TABLENAME)
      .addPostInstantiationTrigger(new CreatedNodeGroupStyleTrigger());
    addPotentialRootNodeStyles(__rootNodeStyleIds);
    calculateUnpublishedNodeIds();
  }

  /**
   * Remove the given id from the list of unpublishedNodeIds and inheritedNodeIds, flushing any
   * cached lists of this data as appropriate.
   */
  protected void removeNodeId(int id)
  {
    if (__unpublishedNodeIds.remove(id)) {
      unpublishedNodeIdsChanged();
      if (__inheritedNodeIds.remove(id)) {
        inheritedNodeIdsChanged();
      }
    }
  }

  protected void addNode(PersistentRecord node)
  {
    if (node.is(Node.ISPUBLISHED)) {
      if (!calculateIsPublished(node)) {
        if (__unpublishedNodeIds.add(node.getId())) {
          unpublishedNodeIdsChanged();
          if (__inheritedNodeIds.add(node.getId())) {
            inheritedNodeIdsChanged();
          } else {
          }
        }
      }
    } else {
      __unpublishedNodeIds.add(node.getId());
    }
  }

  protected void addSubTree(final PersistentRecord node,
                            final Set<Integer> potentialRootNodeStyles)
  {
    final int nodeId = node.getId();
    if (__unpublishedNodeIds.add(nodeId)) {
      unpublishedNodeIdsChanged();
      if (node.is(Node.ISPUBLISHED)) {
        if (__inheritedNodeIds.add(nodeId)) {
          inheritedNodeIdsChanged();
        }
      } else {
      }
      Integer nodeStyleId = node.opt(Node.NODESTYLE_ID);
      if (potentialRootNodeStyles.contains(nodeStyleId)) {
        for (PersistentRecord childNode : node.opt(Node.UNSORTEDCHILDRENNODES)) {
          addSubTree(childNode, potentialRootNodeStyles);
        }
      }
    }
  }

  protected void removeSubTree(PersistentRecord node,
                               Set<Integer> potentialRootNodeStyles)
  {
    DebugConstants.method(this, "removeSubTree", node, potentialRootNodeStyles);
    DebugConstants.variable("__unpublishedNodeIds", __unpublishedNodeIds);
    DebugConstants.variable("__inheritedNodeIds", __inheritedNodeIds);
    if (!node.is(Node.ISPUBLISHED)) {
      DebugConstants.message("Node not published so taking no action");
      return;
    }
    if (__unpublishedNodeIds.remove(node.getId())) {
      DebugConstants.message("removed from __unpublishedNodeIds");
      unpublishedNodeIdsChanged();
      if (__inheritedNodeIds.remove(node.getId())) {
        DebugConstants.message("removed from __inheritedNodeIds");
        inheritedNodeIdsChanged();
      } else {
      }
      if (potentialRootNodeStyles.contains(node.opt(Node.NODESTYLE_ID))) {
        DebugConstants.message("processing child pages...");
        for (PersistentRecord childNode : node.opt(Node.UNSORTEDCHILDRENNODES)) {
          removeSubTree(childNode, potentialRootNodeStyles);
        }
      }
    }
  }

  public void shutdown()
  {
    __rootNodeStyleIds.clear();
    __unpublishedNodeIds.clear();
    __inheritedNodeIds.clear();
  }

  /**
   * Trigger that is invoked when a new Node is created and whose responsibility is to work out if a
   * Node is effectively unpublished because of the state of a parent Node and add it to the
   * appropriate list if so.
   * 
   * @author alex
   */
  class CreatedNodeTrigger
    extends PostInstantiationTriggerImpl
  {
    private CreatedNodeTrigger()
    {
    }

    @Override
    public void rollbackPost(PersistentRecord node,
                             Gettable params)
    {
      removeNodeId(node.getId());
    }

    @Override
    public void triggerPost(PersistentRecord node,
                            Gettable params)
    {
      addNode(node);
    }
  }

  class DeletedNodeTrigger
    extends AbstractStateTrigger
  {
    private DeletedNodeTrigger()
    {
      super(STATE_PREDELETED_DELETED,
            false,
            Node.TABLENAME);
    }

    @Override
    public void trigger(PersistentRecord node,
                        Transaction transaction,
                        int state)
    {
      removeNodeId(node.getId());
    }
  }

  class UpdatedNodeTrigger
    extends AbstractStateTrigger
  {
    private UpdatedNodeTrigger()
    {
      super(STATE_WRITABLE_UPDATED,
            false,
            Node.TABLENAME);
    }

    @Override
    public void trigger(final PersistentRecord node,
                        final Transaction transaction,
                        final int state)
    {
      DebugConstants.method(this, "trigger", node, transaction, Integer.valueOf(state));
      final int id = node.getId();
      boolean isPublished = node.is(Node.ISPUBLISHED);
      DebugConstants.variable("node.isPublished", Boolean.toString(isPublished));
      DebugConstants.variable("__unpublishedNodeIds.contains(id)",
                              Boolean.valueOf(__unpublishedNodeIds.contains(id)));
      DebugConstants.variable("__inheritedNodeIds.contains(id)",
                              Boolean.valueOf(__inheritedNodeIds.contains(id)));
      if (isPublished) {
        if (__unpublishedNodeIds.contains(id)) {
          // node is published, but is in list so first we determine
          // if it is really published...
          if (calculateIsPublished(node)) {
            // really published so list is wrong...
            removeSubTree(node, getPotentialRootNodeStyles());
          } else {
            // child of unpublished node so list is correct...
            __unpublishedNodeIds.remove(id);
            __inheritedNodeIds.add(id);
            inheritedNodeIdsChanged();
            return;
          }
        } else {
          // node is published and not on current list of
          // unpublished nodes so all OK...
          return;
        }
      } else {
        if (__unpublishedNodeIds.contains(id)) {
          // node is unpublished and already in list of unpublished
          // nodes so all OK...
          return;
        } else {
          // node is unpublished and not in list so we need to add
          // to list...
          addSubTree(node, getPotentialRootNodeStyles());
        }
      }
    }
  }

  private boolean calculateIsPublished(PersistentRecord node)
  {
    boolean isPublished = node.is(Node.ISPUBLISHED);
    if (!isPublished) {
      return false;
    }
    PersistentRecord parentNode = node.opt(Node.PARENTNODE);
    if (parentNode == null) {
      return isPublished;
    }
    return calculateIsPublished(parentNode);
  }

  private void addPotentialRootNodeStyles(IntSet ids)
  {
    for (PersistentRecord nodeStyle : __nodeStyleTable) {
      if (nodeStyle.comp(NodeStyle.CHILDRENNODEGROUPSTYLES, null).getIdList().size() != 0) {
        ids.add(nodeStyle.getId());
      }
    }
  }

  /**
   * Check to see if the given node is unpublished in relationship to its parentage. This is done by
   * the node either being specifically unpublished or the node being in the list of inherited node
   * ids.
   * 
   * @return true if the node is published
   * @see #getInheritedNodeIdListing()
   */
  public boolean isPublished(PersistentRecord node)
  {
    TransientRecord.assertIsTableNamed(node, Node.TABLENAME);
    return node.is(Node.ISPUBLISHED) && !getInheritedNodeIds().contains(node.getId());
  }

  public final Set<Integer> getPotentialRootNodeStyles()
  {
    return __publicRootNodeStyleIds;
  }

  private void calculateUnpublishedNodeIds()
  {
    Set<Integer> potentialRootNodeStyles = getPotentialRootNodeStyles();
    if (potentialRootNodeStyles.size() == 0) {
      return;
    }
    Iterator<Integer> nodeStyleIds = potentialRootNodeStyles.iterator();
    StringBuilder filter = new StringBuilder();
    filter.append("(Node.isPublished=0) and (Node.nodeStyle_id in (");
    while (nodeStyleIds.hasNext()) {
      filter.append(nodeStyleIds.next());
      if (nodeStyleIds.hasNext()) {
        filter.append(", ");
      }
    }
    filter.append("))");
    Query unpublishedNodesQuery = __nodeTable.getQuery(null, filter.toString(), null);
    for (PersistentRecord unpublishedNode : unpublishedNodesQuery.getRecordList()) {
      addSubTree(unpublishedNode, potentialRootNodeStyles);
    }
  }

  private void inheritedNodeIdsChanged()
  {
    _inheritedNodeIdListing = null;
  }

  public static final String NODEIDS_EMPTY = "(0)";

  public String getInheritedNodeIdListing()
  {
    String inheritedNodeIdListing = _inheritedNodeIdListing;
    if (inheritedNodeIdListing == null) {
      Set<Integer> inheritedNodeIds = getInheritedNodeIds();
      if (inheritedNodeIds.size() == 0) {
        _inheritedNodeIdListing = NODEIDS_EMPTY;
      } else {
        StringBuilder buf = new StringBuilder(inheritedNodeIds.size() * 5);
        buf.append('(');
        Iterator<Integer> nodeIds = getInheritedNodeIds().iterator();
        try {
          while (nodeIds.hasNext()) {
            buf.append(nodeIds.next());
            if (nodeIds.hasNext()) {
              buf.append(", ");
            }
          }
        } catch (ConcurrentModificationException cmEx) {
          return getInheritedNodeIdListing();
        }
        buf.append(')');
        _inheritedNodeIdListing = buf.toString();
      }
    }
    return _inheritedNodeIdListing;
  }

  /**
   * @param nodes
   *          any Query that includes Node as one of the referenced tables in its resolution. null
   *          or a Query failing to reference Nodes will result in a RuntimeException.
   * @param nodeTableAsName
   *          The name that the Node Table is referred to in this query. If it has been recursively
   *          joined then it may no longer be known as Node and the name should be supplied here. If
   *          it is a conventional non-renamed query then null will suffice to utilise the default
   *          name of Node
   * @return The originally query if it is either empty or there are no unpublished node ids or a
   *         transformed version of the same that excludes the unpublished nodes.
   * @deprecated as it should be passed a RecordSource of nodes. This cannot work until we have a
   *             way of filtering the RecordSource in a similar manner to Query.transformQuery
   */
  @Deprecated
  public RecordSource filterInheritedUnpublishedNodes(Query nodes,
                                                      String nodeTableAsName)
  {
    if (!nodes.getRecordSources().contains(__nodeTable)) {
      throw new IllegalArgumentException(nodes + " doesn't reference the Node Table");
    }
    if (getInheritedNodeIds().size() == 0 || nodes.getIdList().size() == 0) {
      return nodes;
    }
    String inheritedNodeIds = getInheritedNodeIdListing();
    StringBuilder buf = new StringBuilder(inheritedNodeIds.length() + 16);
    buf.append(Strings.isNullOrEmpty(nodeTableAsName) ? Node.TABLENAME : nodeTableAsName)
       .append(".id not in ")
       .append(inheritedNodeIds);
    Query filteredQuery = Query.transformQuery(nodes, buf.toString(), Query.TYPE_AND, null);
    return filteredQuery;
  }

  private void unpublishedNodeIdsChanged()
  {
    _unpublishedNodeIdListing = null;
  }

  public String getUnpublishedNodeIdListing()
  {
    String unpublishedNodeIdListing = _unpublishedNodeIdListing;
    if (unpublishedNodeIdListing == null) {
      Set<Integer> unpublishedNodeIds = getUnpublishedNodeIds();
      if (unpublishedNodeIds.size() == 0) {
        _unpublishedNodeIdListing = NODEIDS_EMPTY;
      } else {
        StringBuilder buf = new StringBuilder(unpublishedNodeIds.size() * 5);
        buf.append('(');
        Iterator<Integer> nodeIds = unpublishedNodeIds.iterator();
        try {
          while (nodeIds.hasNext()) {
            buf.append(nodeIds.next());
            if (nodeIds.hasNext()) {
              buf.append(", ");
            }
          }
        } catch (ConcurrentModificationException cmEx) {
          return getUnpublishedNodeIdListing();
        }
        buf.append(')');
        _unpublishedNodeIdListing = buf.toString();
      }
    }
    return _unpublishedNodeIdListing;
  }

  public IntSet getUnpublishedNodeIds()
  {
    return __publicUnpublishedNodeIds;
  }

  public IntSet getInheritedNodeIds()
  {
    return __publicInheritedNodeIds;
  }

  /**
   * StateTrigger that sits on NodeStyle waiting for STATE_PREDELETED_DELETED and removing the
   * NodeStyle that has been deleted from the list of potential root nodes.
   */
  class DeletedNodeStyleTrigger
    extends AbstractStateTrigger
  {
    private DeletedNodeStyleTrigger()
    {
      super(STATE_PREDELETED_DELETED,
            false,
            NodeStyle.TABLENAME);
    }

    @Override
    public void trigger(PersistentRecord nodeStyle,
                        Transaction transaction,
                        int state)
    {
      synchronized (__rootNodeStyleIds) {
        __rootNodeStyleIds.remove(nodeStyle.getId());
        // it is not necessary to bubble down the children nodeStyles
        // because these will receive separate triggers as they are
        // deleted, and the same for the nodes that implement this
        // style...
      }
    }
  }

  /**
   * PostInstantiationTrigger that sits on NodeGroupStyle and adds the parentNodeStyle to the list
   * of potential root nodes because it now potentially has children.
   */
  class CreatedNodeGroupStyleTrigger
    extends PostInstantiationTriggerImpl
  {
    private CreatedNodeGroupStyleTrigger()
    {
    }

    @Override
    public void triggerPost(PersistentRecord nodeGroupStyle,
                            Gettable params)
    {
      synchronized (__rootNodeStyleIds) {
        __rootNodeStyleIds.add(nodeGroupStyle.opt(NodeGroupStyle.PARENTNODESTYLE_ID));
      }
    }
  }

}