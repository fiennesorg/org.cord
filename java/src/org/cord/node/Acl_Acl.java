package org.cord.node;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.RecordSources;

public class Acl_Acl
  extends NodeTableWrapper
{
  public static final String TABLENAME = "Acl_Acl";

  public static final IntFieldKey CONTAINERACL_ID = IntFieldKey.create("containerAcl_id");
  public static final RecordOperationKey<PersistentRecord> CONTAINERACL =
      LinkField.getLinkName(CONTAINERACL_ID);
  public static final RecordOperationKey<RecordSource> ACL_NESTEDACLLINKS =
      RecordOperationKey.create("nestedAclLinks", RecordSource.class);

  public static final IntFieldKey NESTEDACL_ID = IntFieldKey.create("nestedAcl_id");
  public static final RecordOperationKey<PersistentRecord> NESTEDACL =
      LinkField.getLinkName(NESTEDACL_ID);
  public static final RecordOperationKey<RecordSource> ACL_CONTAININGACLLINKS =
      RecordOperationKey.create("containingAclLinks", RecordSource.class);

  public Acl_Acl(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(CONTAINERACL_ID,
                                 "Containing ACL",
                                 null,
                                 CONTAINERACL,
                                 Acl.TABLENAME,
                                 ACL_NESTEDACLLINKS,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(NESTEDACL_ID,
                                 "Nested ACL",
                                 null,
                                 NESTEDACL,
                                 Acl.TABLENAME,
                                 ACL_CONTAININGACLLINKS,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
  }

  /**
   * Resolve the record, if any, that connects the two supplied Acl ids.
   */
  public PersistentRecord optRecord(Integer container_id,
                                    Integer nested_id,
                                    Viewpoint viewpoint)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(CONTAINERACL_ID + "=")
       .append(container_id)
       .append(" and " + NESTEDACL_ID + "=")
       .append(nested_id);
    String filter = buf.toString();
    RecordSource records = getTable().getQuery(filter, filter, null);
    return RecordSources.getOptOnlyRecord(records,
                                          viewpoint,
                                          "Duplicate Acl_Acl records for %s inside %s",
                                          nested_id,
                                          container_id);
  }

  /**
   * Establish the nesting of two Acl ids or return the existing connection if any.
   */
  public PersistentRecord createOrResolveRecord(Integer containerAcl_id,
                                                Integer nestedAcl_id,
                                                Viewpoint viewpoint)
      throws MirrorInstantiationException, MirrorFieldException
  {
    PersistentRecord record = optRecord(containerAcl_id, nestedAcl_id, viewpoint);
    if (record != null) {
      return record;
    }
    TransientRecord tRecord = getTable().createTransientRecord();
    tRecord.setField(CONTAINERACL_ID, containerAcl_id);
    tRecord.setField(NESTEDACL_ID, nestedAcl_id);
    return tRecord.makePersistent(null);
  }
}
