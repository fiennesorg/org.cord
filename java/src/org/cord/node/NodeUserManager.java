package org.cord.node;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.operation.UserPasswordValidator;
import org.cord.util.DebugConstants;
import org.cord.util.Expirable;
import org.cord.util.LogicException;
import org.cord.util.Maps;
import org.cord.util.NamedImpl;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;
import org.cord.util.StringUtils;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * The NodeUserManager is responsible for tracking the Users that are currently logged into the Node
 * system, their ongoing Transactions and sessions.
 */
public class NodeUserManager
  extends NamedImpl
  implements Expirable
{
  private NodeManager _nodeManager;

  private Table _nodeTable;

  private Table _userTable;

  // Session --> UserSession
  private final Map<HttpSession, UserSession> __sessionUserSessions = Maps.newConcurrentHashMap();

  // user id (Integer) --> UserSession
  private final Int2ObjectMap<UserSession> __userIdUserSessions =
      Int2ObjectMaps.synchronize(new Int2ObjectOpenHashMap<UserSession>());

  private final Map<Transaction, HttpSession> __transactionSession = Maps.newConcurrentHashMap();

  // sessionId (String) --> HttpSession
  private final Map<String, HttpSession> __sessionIdHttpSessions = Maps.newConcurrentHashMap();

  private final Set<String> __crossSessionAttributes = Sets.newConcurrentHashSet();

  /**
   * Boot up a new NodeUserManager. This will normally be called automatically by the NodeManaager
   * during its bootup procedure which is why the constructer is protected.
   */
  protected NodeUserManager(NodeManager nodeManager)
  {
    super("NodeUserManager");
    _nodeManager = nodeManager;
    _nodeTable = _nodeManager.getDb().getTable(Node.TABLENAME);
    _userTable = _nodeManager.getDb().getTable(User.TABLENAME);
    _nodeManager.getDb().addExpirable(this);
  }

  /**
   * Add the name of a session attribute that won't be cleared when changing login. Note that it
   * will still be cleared when moving from logged in to anonymous.
   */
  public void addCrossSessionAttribute(String name)
  {
    __crossSessionAttributes.add(name);
  }

  public HttpSession getSession(Transaction transaction)
  {
    return __transactionSession.get(transaction);
  }

  private void debugState()
  {
    System.err.println("  __sessionUserSessions: " + __sessionUserSessions);
    System.err.println("   __userIdUserSessions: " + __userIdUserSessions);
    System.err.println("   __transactionSession: " + __transactionSession);
    System.err.println("__sessionIdHttpSessions:" + __sessionIdHttpSessions);
  }

  @SuppressWarnings("unused")
  @Override
  public int expire()
  {
    int count = 0;
    Iterator<UserSession> userSessions = getUserSessions();
    while (userSessions.hasNext()) {
      UserSession userSession = userSessions.next();
      count += userSession.expireRedundantSessions();
    }
    if (DebugConstants.DEBUG_SESSION_VERBOSE && count > 0) {
      System.err.println("Expired: " + count);
      debugState();
    }
    return count;
  }

  public synchronized void shutdown()
  {
    logoutAll();
    __sessionUserSessions.clear();
    __sessionIdHttpSessions.clear();
    __userIdUserSessions.clear();
    _nodeManager = null;
    _nodeTable = null;
    _userTable = null;
  }

  protected synchronized void logoutAll()
  {
    for (HttpSession session : __sessionUserSessions.keySet()) {
      changeLogin(session, null);
    }
  }

  public void logoutAll(String transactionPassword)
  {
    _nodeManager.checkTransactionPassword(transactionPassword,
                                          this + ".logoutAll(" + transactionPassword + ")");
    logoutAll();
  }

  protected final Iterator<UserSession> getUserSessions()
  {
    return __userIdUserSessions.values().iterator();
  }

  private final synchronized UserSession createUserSession(int userId)
  {
    UserSession userSession = __userIdUserSessions.get(userId);
    if (userSession != null) {
      return userSession;
    }
    userSession = new UserSession(userId);
    __userIdUserSessions.put(userId, userSession);
    return userSession;
  }

  /**
   * Get the UserSession that is associated with the given session. If the session has not been
   * previously stored, then this implies that the Session is new, and therefore hasn't yet been
   * authenticated. In this case, the UserSession that is currently associated with the
   * USER_ID_ANONYMOUS is retrieved and this Session is associated with it.
   * 
   * @param session
   *          The HttpSession that the request originates from.
   * @return The associated UserSession. This should theoretically never be null.
   */
  public final synchronized UserSession getUserSession(HttpSession session)
  {
    UserSession userSession = __sessionUserSessions.get(session);
    if (userSession == null) {
      userSession = getUserSession(User.ID_ANONYMOUS);
      userSession.addSession(session);
      __sessionUserSessions.put(session, userSession);
      __sessionIdHttpSessions.put(session.getId(), session);
    }
    return userSession;
  }

  /**
   * Get te SessionTransaction associated with the given Session.
   * 
   * @param session
   *          The HttpSession that is to be looked up.
   * @return The associated SessionTransaction. This should theoretically never be null.
   * @see #getUserSession(HttpSession)
   */
  public final SessionTransaction getSessionTransaction(HttpSession session)
  {
    return getUserSession(session).getSessionTransaction(session);
  }

  /**
   * Get the UserSession that is associated with the given user id. If there is not currently a
   * UserSession object associated with the current user then a new UserSession will be created.
   * This UserSession will initially have no sessions associated with it.
   * 
   * @param userId
   *          The id of the targetted user.
   * @return The required UserSession.
   */
  protected final synchronized UserSession getUserSession(int userId)
  {
    UserSession userSession = __userIdUserSessions.get(userId);
    return userSession == null ? createUserSession(userId) : userSession;
  }

  /**
   * Change the userId associated with a particular session. This will have the following side
   * effects:-
   * <ul>
   * <li>break the association between the session and it's previous user id (if any).
   * <li>cancel any open Transactions (if any)
   * <li>register a new user id (if required)
   * <li>associate the session with the new user id.
   * </ul>
   */
  public final synchronized UserSession changeLogin(HttpSession session,
                                                    Integer newUserId)
  {
    UserSession oldUserSession = getUserSession(session);
    if (newUserId != null && oldUserSession.getUserId() == newUserId.intValue()) {
      return oldUserSession;
    }
    oldUserSession.logoutSession(session, newUserId);
    if (newUserId == null) {
      return null;
    }
    UserSession newUserSession = getUserSession(newUserId.intValue());
    newUserSession.addSession(session);
    __sessionUserSessions.put(session, newUserSession);
    __sessionIdHttpSessions.put(session.getId(), session);
    return newUserSession;
  }

  public UserSession inheritLogin(HttpSession currentSession,
                                  String previousSessionId)
  {
    UserSession currentUserSession = getUserSession(currentSession);
    if (DebugConstants.DEBUG_GRIPCOMMERCE) {
      DebugConstants.DEBUG_OUT.println("*** CHANGELOGIN ***");
      DebugConstants.DEBUG_OUT.println("previousSessionId:" + previousSessionId);
    }
    if (Strings.isNullOrEmpty(previousSessionId)) {
      if (DebugConstants.DEBUG_GRIPCOMMERCE) {
        DebugConstants.DEBUG_OUT.println("previousSessionId not defined, returning current");
      }
      return currentUserSession;
    }
    if (DebugConstants.DEBUG_GRIPCOMMERCE) {
      DebugConstants.DEBUG_OUT.println("currentUserSession:" + currentUserSession);
    }
    HttpSession previousSession = __sessionIdHttpSessions.get(previousSessionId);
    if (previousSession != null && !currentSession.equals(previousSession)) {
      Enumeration<?> attributeNames = previousSession.getAttributeNames();
      while (attributeNames.hasMoreElements()) {
        String attributeName = (String) attributeNames.nextElement();
        Object previousValue = previousSession.getAttribute(attributeName);
        Object currentValue = currentSession.getAttribute(attributeName);
        currentSession.setAttribute(attributeName, previousValue);
        if (DebugConstants.DEBUG_GRIPCOMMERCE) {
          DebugConstants.DEBUG_OUT.println(attributeName + ":" + previousValue + ":"
                                           + currentValue);
        }
      }
    }
    UserSession previousUserSession = getUserSession(previousSession);
    if (DebugConstants.DEBUG_GRIPCOMMERCE) {
      DebugConstants.DEBUG_OUT.println("previousSession:" + previousSession);
      DebugConstants.DEBUG_OUT.println("previousUserSession:" + previousUserSession);
    }
    if (previousUserSession == null) {
      if (DebugConstants.DEBUG_GRIPCOMMERCE) {
        DebugConstants.DEBUG_OUT.println("previous == null, returning current");
      }
      return currentUserSession;
    }
    int previousUserId = previousUserSession.getUserId();
    int currentUserId = currentUserSession.getUserId();
    if (DebugConstants.DEBUG_GRIPCOMMERCE) {
      DebugConstants.DEBUG_OUT.println("previousUserId:" + previousUserId);
      DebugConstants.DEBUG_OUT.println("currentUserId:" + currentUserId);
    }
    if (previousUserId == currentUserId) {
      if (DebugConstants.DEBUG_GRIPCOMMERCE) {
        DebugConstants.DEBUG_OUT.println("previous id == current id, returning current");
      }
      return currentUserSession;
    }
    UserSession newUserSession = changeLogin(currentSession, Integer.valueOf(previousUserId));
    if (DebugConstants.DEBUG_GRIPCOMMERCE) {
      DebugConstants.DEBUG_OUT.println("newUserSession:" + newUserSession);
    }
    return newUserSession;
  }

  private PersistentRecord getUser(PersistentRecord node,
                                   String login)
      throws AuthenticationException
  {
    return getUser(node, login, "The username and password you supplied were not accepted");
  }

  public PersistentRecord getUser(PersistentRecord node,
                                  String login,
                                  String failureMessage)
      throws AuthenticationException
  {
    try {
      return getUser(login);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new AuthenticationException(failureMessage, node, mnsrEx);
    }
  }

  public PersistentRecord getUser(String login)
      throws MirrorNoSuchRecordException
  {
    return RecordSources.getFirstRecord(TableStringMatcher.search(_userTable,
                                                                  null,
                                                                  User.LOGIN,
                                                                  login,
                                                                  null,
                                                                  TableStringMatcher.SEARCH_EXACT),
                                        null);
  }

  /**
   * @return The User that was authenticated against, or null if the authentication failed.
   */
  public PersistentRecord authenticate(String login,
                                       String password,
                                       String ipAddress)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord user = getUser(login);
    if (UserPasswordValidator.accepts(user, password, ipAddress, _nodeManager)) {
      return user;
    }
    return null;
  }

  /**
   * @return true if the user accepts the given authentication.
   */
  public boolean authenticate(PersistentRecord user,
                              String password,
                              String ipAddress)
  {
    return UserPasswordValidator.accepts(user, password, ipAddress, _nodeManager);
  }

  public RecordSource getUsers(String email)
  {
    return TableStringMatcher.search(_userTable,
                                     null,
                                     User.EMAIL,
                                     email,
                                     null,
                                     TableStringMatcher.SEARCH_EXACT);
  }

  public final void changeLogin(NodeRequest nodeRequest,
                                PersistentRecord node,
                                String login,
                                String ipAddress,
                                String password)
      throws AuthenticationException
  {
    PersistentRecord user = getUser(node, login);
    if (!UserPasswordValidator.accepts(user, password, ipAddress, _nodeManager)) {
      throw new AuthenticationException("The username and password you supplied were not accepted",
                                        node,
                                        null);
    }
    switch (user.compInt(User.STATUS)) {
      case User.STATUS_INIT:
        throw new AuthenticationException("You haven't confirmed your subscription.", node, null);
      case User.STATUS_CONFIRMED:
        nodeRequest.setUserSession(changeLogin(nodeRequest.getSession(),
                                               Integer.valueOf(user.getId())));
        break;
      case User.STATUS_SUSPENDED:
        throw new AuthenticationException("This user identity is suspended", node, null);
    }
  }

  /**
   * @param node
   *          The page that should be used as the base for rendering any error messages
   * @return true if the login was updated, false if the NodeRequest was already logged in as User.
   * @throws AuthenticationException
   *           If the supplied user was {@link User#STATUS_INIT} or {@link User#STATUS_SUSPENDED}.
   */
  public final boolean changeLogin(NodeRequest nodeRequest,
                                   PersistentRecord node,
                                   PersistentRecord user)
      throws AuthenticationException
  {
    switch (user.compInt(User.STATUS)) {
      case User.STATUS_INIT:
        throw new AuthenticationException("You haven't confirmed your subscription.", node, null);
      case User.STATUS_CONFIRMED:
        if (nodeRequest.getUser().equals(user)) {
          return false;
        }
        nodeRequest.setUserSession(changeLogin(nodeRequest.getSession(),
                                               Integer.valueOf(user.getId())));
        return true;
      case User.STATUS_SUSPENDED:
        throw new AuthenticationException("This user identity is suspended", node, null);
      default:
        throw new IllegalStateException(String.format("Unknown User.status of %s on %s",
                                                      Integer.toString(user.compInt(User.STATUS)),
                                                      user));
    }
  }

  /**
   * Take the given user record and attempt to activate the user account, assuming the
   * proposedSecretCode is accepted. If the method returns null then this means that user is now in
   * an active and confirmed status. It is the responsibility of the invoking method to update any
   * login status as required.
   * 
   * @param user
   *          The user record to confirm
   * @param proposedSecretCode
   *          The code that has been supplied as a checksum value.
   * @throws FeedbackErrorException
   *           Is there is an error. This might be:
   *           <ul>
   *           <li>If the proposedSecretCode doesn't match the value of user.secretCode</li>
   *           <li>If the given user is already locked by an existing transaction (unlikely)</li>
   *           <li>If the edit transaction on user timed out on commit (unlikely unless you have a
   *           load of 3rd party dependent records that are already locked)</li>
   *           <li>If user has been disabled and therefore non-eligible for activation</li>
   *           </ul>
   */
  public void confirmUser(long proposedSecretCode,
                          PersistentRecord node,
                          PersistentRecord user)
      throws FeedbackErrorException
  {
    confirmUser(proposedSecretCode, node, user, Optional.<String> absent());
  }

  public void confirmUser(long proposedSecretCode,
                          PersistentRecord node,
                          PersistentRecord user,
                          Optional<String> password)
      throws FeedbackErrorException
  {
    TransientRecord.assertIsTableNamed(node, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(user, User.TABLENAME);
    final String title = "Confirm User";
    if (proposedSecretCode != user.comp(User.SECRETCODE).longValue()) {
      throw new FeedbackErrorException(title, node, "Bad secret code");
    }
    switch (user.compInt(User.STATUS)) {
      case User.STATUS_INIT:
        try (Transaction updateTransaction =
            _nodeManager.getDb()
                        .createTransaction(Db.DEFAULT_TIMEOUT,
                                           Transaction.TRANSACTION_UPDATE,
                                           _nodeManager.getTransactionPassword(),
                                           "confirm user")) {
          WritableRecord editableUser = updateTransaction.edit(user, Db.DEFAULT_TIMEOUT);
          editableUser.setField(User.STATUS, Integer.valueOf(User.STATUS_CONFIRMED));
          if (password.isPresent()) {
            editableUser.setField(User.PASSWORD, password.get());
          }
          updateTransaction.commit();
        } catch (Exception e) {
          throw new FeedbackErrorException("User.confirmUser",
                                           node,
                                           e,
                                           "Unexpected error updating User database");
        }
      case User.STATUS_CONFIRMED:
        break;
      case User.STATUS_SUSPENDED:
        throw new FeedbackErrorException(title,
                                         node,
                                         " %s has been suspended",
                                         user.opt(User.LOGIN));
    }
  }

  /**
   * Extract the login and secretCode from the NodeRequest and then use this to attempt to confirm
   * the login of the User. If this method succeeds then the user will be in state confirmed and the
   * current session will be associated with the targetted user (ie the user is automatically logged
   * in).
   * 
   * @param nodeRequest
   *          The incoming NodeRequest that contains the appropriate information to process.
   * @param node
   *          The Node that this request was invoked upon. This is used purely for generating
   *          FeedbackErrorExceptions rather than the processing of the request.
   * @throws FeedbackErrorException
   *           <ul>
   *           <li>If the login is not found in the current list of users</li>
   *           <li>Any of the nested confirmUser(...) error situations</li>
   *           </ul>
   * @see #confirmUser(long, PersistentRecord, PersistentRecord)
   */
  public void confirmUser(NodeRequest nodeRequest,
                          PersistentRecord node,
                          PersistentRecord user)
      throws FeedbackErrorException
  {
    String secretCodeParam = nodeRequest.getString(User.SECRETCODE.toString());
    long secretCode = 0;
    try {
      secretCode = secretCodeParam == null ? 0 : Long.valueOf(secretCodeParam).longValue();
    } catch (NumberFormatException badSecretCodeParamEx) {
      // let secretCode stay at 0
    }
    confirmUser(secretCode, node, user);
    nodeRequest.setUserSession(changeLogin(nodeRequest.getSession(),
                                           Integer.valueOf(user.getId())));
  }

  public final UserSession logout(HttpSession session)
  {
    return changeLogin(session, Integer.valueOf(User.ID_ANONYMOUS));
  }

  public final void logout(NodeRequest nodeRequest)
  {
    nodeRequest.setUserSession(logout(nodeRequest.getSession()));
  }

  /**
   * If there are other Sessions currently logged in with the same User ID as the given session then
   * log them out. This does not do anything if the current session is {@link User#ID_ANONYMOUS}
   * 
   * @return the number of Sessions that were invalidated.
   */
  public int logoutOtherSessionForUser(HttpSession session)
  {
    UserSession userSession = getUserSession(session);
    if (userSession.getUserId() == User.ID_ANONYMOUS) {
      return 0;
    }
    int count = 0;
    Iterator<Map.Entry<HttpSession, SessionTransaction>> i = userSession.getEntrySet().iterator();
    while (i.hasNext()) {
      Map.Entry<HttpSession, SessionTransaction> entry = i.next();
      if (!session.equals(entry.getKey())) {
        entry.getValue().invalidate();
        i.remove();
        count++;
      }
    }
    return count;
  }
  /**
   * Force logout the targetted user. This will logout all instances of the targetted user
   * regardless of the number of times they are logged in. I think that there should probably be
   * some security to govern who can call this method, but we can deal with that at a later date
   * (the worst possible scenario is a denial of service attack, no data can actually be changed).
   * 
   * @param userId
   *          The id of the User that is to be completely logged out of the system.
   * @return The number of Sessions of the given user that were actually logged out of the system.
   */
  public final int logout(int userId)
  {
    UserSession userSession = getUserSession(userId);
    int sessionCount = 0;
    Iterator<HttpSession> sessions = userSession.getSessions();
    while (sessions.hasNext()) {
      HttpSession session = sessions.next();
      changeLogin(session, Integer.valueOf(User.ID_ANONYMOUS));
      sessionCount++;
    }
    return sessionCount;
  }

  protected final void addTransaction(HttpSession session,
                                      Transaction transaction)
  {
    UserSession userSession = getUserSession(session);
    SessionTransaction sessionTransaction = userSession.getSessionTransaction(session);
    sessionTransaction.addTransaction(transaction);
  }

  /**
   * Remove a given Transaction from the list of Transactions associated with the Session. Please
   * note that this does not cancel the Transaction.
   */
  protected final boolean removeTransaction(HttpSession session,
                                            Transaction transaction)
  {
    UserSession userSession = getUserSession(session);
    SessionTransaction sessionTransaction = userSession.getSessionTransaction(session);
    return sessionTransaction.removeTransaction(transaction);
  }

  /**
   * Singleton Comparator classes that imposes an ordering via $node.path on a list of Nodes. This
   * probably should be migrated out to a seperate class at some point, but for now it can reside
   * here.
   */
  private final static class NodeFilenameComparator
    implements Comparator<TransientRecord>, SingletonShutdownable
  {
    private static NodeFilenameComparator _instance = new NodeFilenameComparator();

    public static Comparator<TransientRecord> getInstance()
    {
      return _instance;
    }

    @Override
    public void shutdownSingleton()
    {
      _instance = null;
    }

    private NodeFilenameComparator()
    {
      SingletonShutdown.registerSingleton(this);
    }

    @Override
    public int compare(TransientRecord n1,
                       TransientRecord n2)
    {
      return n1.opt(Node.PATH).compareTo(n2.opt(Node.PATH));
    }
  }

  // ***********
  // UserSession
  // ***********
  /**
   * Class that models the relationship between a single User and a number (0..n) of Sessions that
   * the User is currently involved with.
   */
  public class UserSession
  {
    private final int __userId;

    // HttpSession --> SessionTransaction
    private final HashMap<HttpSession, SessionTransaction> __sessionTransactions =
        new HashMap<HttpSession, SessionTransaction>();

    public UserSession(int userId)
    {
      __userId = userId;
    }

    /**
     * Get the Id of the User.
     */
    public int getUserId()
    {
      return __userId;
    }

    public PersistentRecord getUser(Transaction transaction)
    {
      try {
        return _userTable.getRecord(__userId, transaction);
      } catch (MirrorNoSuchRecordException missingUserEx) {
        throw new LogicException("Missing User in UserSession", missingUserEx);
      }
    }

    public PersistentRecord getUser()
    {
      return getUser(null);
    }

    @Override
    public String toString()
    {
      return "UserSession(" + getUser().opt(User.NAME) + ", " + __sessionTransactions + ")";
    }

    /**
     * Register a new session with this UserSession. This has the effect of dropping a new empty
     * SessionTransaction into this object, filed under the session. This method is not synchronized
     * as it is assumed that each session is associated with a single thread and therefore duplicate
     * requests for the same session are not expected.
     * 
     * @param session
     *          The session that is to be associated with the user.
     * @return The existing SessionTransaction or the newly created SessionTransaction.
     */
    public synchronized SessionTransaction addSession(HttpSession session)
    {
      if (session == null) {
        throw new NullPointerException("null HttpSession is not permitted");
      }
      SessionTransaction sessionTransaction = __sessionTransactions.get(session);
      if (sessionTransaction == null) {
        sessionTransaction = new SessionTransaction(this, session);
        __sessionTransactions.put(session, sessionTransaction);
        if (DebugConstants.DEBUG_SESSION) {
          DebugConstants.DEBUG_OUT.println(_nodeManager.getHostName() + ":   login ("
                                           + getUser().opt(User.LOGIN) + ")");
        }
      }
      return sessionTransaction;
    }

    /**
     * Get the SessionTransaction associated with the given session for this UserSession.
     * 
     * @param session
     *          The given session.
     * @return The appropriate SessionTransaction or null if the given session is not currently
     *         associated with this UserSession.
     */
    public synchronized SessionTransaction getSessionTransaction(HttpSession session)
    {
      return __sessionTransactions.get(session);
    }

    /**
     * Get a list of all the Session objects that are currently associated with this User.
     * 
     * @return Iterator of HttpSession objects
     */
    public Iterator<HttpSession> getSessions()
    {
      return __sessionTransactions.keySet().iterator();
    }

    /**
     * Get a list of all the SessionTransaction objects that are currently associated with this
     * User.
     * 
     * @return Iterator of SessionTransaction objects.
     */
    public Iterator<SessionTransaction> getSessionTransactions()
    {
      return __sessionTransactions.values().iterator();
    }

    public Set<Map.Entry<HttpSession, SessionTransaction>> getEntrySet()
    {
      return __sessionTransactions.entrySet();
    }

    /**
     * Request the UserSession to invalidate itself and all its related information. This will have
     * the effect of causing all associated SessionTransaction objects to become invalidated and
     * then dropped. Effectively this will force all clients logged in as the User to be dropped out
     * of the system and therefore be forced to re-authenticate themselves with the system. This may
     * be used in cases when the authentication requirements for a particular user are updated such
     * as changing the password for the user.
     * 
     * @return The number of sessions that were invalidated during this operation (>=0).
     */
    protected synchronized int invalidate()
    {
      int sessionCount = size();
      Iterator<HttpSession> sessions = getSessions();
      while (sessions.hasNext()) {
        HttpSession session = sessions.next();
        SessionTransaction sessionTransaction = getSessionTransaction(session);
        sessionTransaction.invalidate();
      }
      __sessionTransactions.clear();
      return sessionCount;
    }

    protected synchronized int size()
    {
      return __sessionTransactions.size();
    }

    /**
     * Housekeeping method to check through all Sessions currently associated with this user and
     * clean up any sessions that have timed out. This method should be called periodically to clean
     * up any sessions that have been left hanging. If this is not the case, then any users which do
     * not explicitly log out of the system (especially the anonymous users) will keep hold of
     * references to their redundant sessions after they leave the system leading to memory problems
     * over time. Once the method has been called, there should only be SessionTransaction objects
     * for HttpSessions that are still current and valid. The frequency with which this operation
     * needs to be performed depends on the loading of the system, but I would reckon that something
     * along the lines of every 10 minutes should be a fine safety target.
     * 
     * @return The number of HttpSessions that turned out to be invalid and which were garbage
     *         collected.
     */
    protected synchronized int expireRedundantSessions()
    {
      HashSet<HttpSession> badSessionSet = new HashSet<HttpSession>();
      Iterator<HttpSession> sessions = getSessions();
      while (sessions.hasNext()) {
        HttpSession session = sessions.next();
        try {
          // this method is irrelevant, but does get the session to
          // check whether or not it has been invalidated...
          session.getLastAccessedTime();
        } catch (IllegalStateException ilsEx) {
          // if invalid, then lets kill any associated things
          // associated with it.
          badSessionSet.add(session);
        }
      }
      for (HttpSession badSession : badSessionSet) {
        logoutSession(badSession, null);
      }
      return badSessionSet.size();
    }

    protected synchronized boolean logoutSession(HttpSession session,
                                                 Integer newUserId)
    {
      try {
        Enumeration<?> valueNames = session.getAttributeNames();
        if (DebugConstants.DEBUG_SESSION) {
          DebugConstants.DEBUG_OUT.println(_nodeManager.getHostName() + ":   logout ("
                                           + getUser().opt(User.NAME) + ")");
        }
        while (valueNames.hasMoreElements()) {
          String valueName = StringUtils.toString(valueNames.nextElement());
          if (newUserId == null || newUserId.intValue() == User.ID_ANONYMOUS
              || !__crossSessionAttributes.contains(valueName)) {
            session.removeAttribute(valueName);
          }
        }
      } catch (IllegalStateException isEx) {
        if (DebugConstants.DEBUG_SESSION) {
          DebugConstants.DEBUG_OUT.println(_nodeManager.getHostName() + ":   expire ("
                                           + getUser().opt(User.NAME) + ")");
        }
        // session is already invalidated. We don't care about this
        // and continue to tidy up as before....
      }
      SessionTransaction sessionTransaction = __sessionTransactions.remove(session);
      __sessionIdHttpSessions.remove(session.getId());
      __sessionUserSessions.remove(session);
      if (DebugConstants.DEBUG_SESSION_VERBOSE) {
        System.err.println("logoutSession(" + session + ")");
      }
      if (sessionTransaction == null) {
        if (DebugConstants.DEBUG_SESSION_VERBOSE) {
          debugState();
        }
        return false;
      } else {
        sessionTransaction.cancelTransactions();
        if (DebugConstants.DEBUG_SESSION_VERBOSE) {
          debugState();
        }
        return true;
      }
    }
  }

  // ******************
  // SessionTransaction
  // ******************
  /**
   * Class that models the relationship between a single Session and a number (0..n) of
   * Transactions.
   */
  public class SessionTransaction
  {
    private final UserSession __userSession;

    private final HttpSession __session;

    private Set<Transaction> _transactions = null;

    /**
     * @param userSession
     *          The UserSession that "owns" this SessionTransaction.
     * @param session
     *          The Session that this SessionTransaction is associated with.
     */
    protected SessionTransaction(UserSession userSession,
                                 HttpSession session)
    {
      __userSession = userSession;
      __session = session;
    }

    /**
     * Get an Iterator of all of the Node PersistentRecords that are locked by this
     * SessionTransaction, sorted by the $node.path value. This operation is relatively expensive
     * unless there are no locked Nodes in which case it is pretty efficient.
     * 
     * @return Iterator of Node PersistentRecord items.
     */
    public Set<PersistentRecord> getLockedNodes()
    {
      Set<Transaction> transactions = getTransactions();
      if (transactions.size() == 0) {
        return Collections.emptySet();
      }
      TreeSet<PersistentRecord> treeSet =
          new TreeSet<PersistentRecord>(NodeFilenameComparator.getInstance());
      for (Transaction transaction : transactions) {
        treeSet.addAll(transaction.getRecords(Node.TABLENAME));
      }
      return Collections.unmodifiableSet(treeSet);
    }

    /**
     * Get the UserSession that "owns" this SessionTransaction.
     */
    public final UserSession getUserSession()
    {
      return __userSession;
    }

    @Override
    public String toString()
    {
      return "SessionTransaction(" + __session + ", " + _transactions + ")";
    }

    public HttpSession getSession()
    {
      return __session;
    }

    /**
     * Request the SessionTransaction to invalidate itself and all its related information. This
     * will iterate through the Transaction objects that have been linked to the Session and cancel
     * each of them. Finally it will then invalidate the Session itself. All errors will be caught
     * and dropped immediately (in the Transaction cancellation and the session invalidation) as
     * they will be incidental to the process itself which will complete.
     * 
     * @see Transaction#cancel()
     */
    protected synchronized void invalidate()
    {
      cancelTransactions();
      try {
        __session.invalidate();
      } catch (IllegalStateException ilEx) {
        // not fussed if the session has already been invalidated.
      }
    }

    public synchronized void cancelTransactions()
    {
      if (_transactions == null)
        return;
      Iterator<Transaction> i = _transactions.iterator();
      while (i.hasNext()) {
        Transaction transaction = i.next();
        __transactionSession.remove(transaction);
        transaction.cancel();
        i.remove();
      }
      _transactions = null;
    }

    /**
     * Add a new Transaction to the set of transactions that are associated with the session.
     */
    public synchronized boolean addTransaction(Transaction transaction)
    {
      __transactionSession.put(transaction, getSession());
      if (_transactions == null) {
        _transactions = new HashSet<Transaction>();
      }
      return _transactions.add(transaction);
    }

    /**
     * Get the number of Transactions associated with this session.
     * 
     * @return The number of associated Transactions (0..n)
     */
    public synchronized int getTransactionCount()
    {
      return _transactions == null ? 0 : _transactions.size();
    }

    /**
     * @return Unmodifiable Set of the Transactions that this SessionTransaction contains. Never
     *         null, although may be empty
     */
    public synchronized Set<Transaction> getTransactions()
    {
      return _transactions == null
          ? Collections.<Transaction> emptySet()
          : Collections.unmodifiableSet(_transactions);
    }

    public boolean containsNode(int nodeId)
    {
      return getTransactionByNode(nodeId) != null;
    }

    private Transaction dropDeadTransaction(Transaction transaction)
    {
      try {
        return transaction.checkValidity();
      } catch (MirrorTransactionTimeoutException deadTransaction) {
        removeTransaction(transaction);
        return null;
      }
    }

    /**
     * Get the viewpoint (if any) onto the specified Node. The transaction will have been validated
     * before returning it so it "should" still be valid.
     * 
     * @param nodeId
     *          The id of the requested Node.
     * @return The Transaction viewpoint or null if the lock is not held by this SessionTransaction.
     */
    public synchronized Transaction getTransactionByNode(int nodeId)
    {
      if (_transactions == null) {
        return null;
      }
      for (Transaction transaction : getTransactions()) {
        if (transaction.contains(_nodeTable, nodeId)) {
          return dropDeadTransaction(transaction);
        }
      }
      return null;
    }

    public int getTransactionTypeByNode(int nodeId)
    {
      Transaction transaction = getTransactionByNode(nodeId);
      return transaction == null ? Transaction.TRANSACTION_NULL : transaction.getType();
    }

    /**
     * Get a specified Node applying the appropriate viewpoint if it is held by this
     * SessionTransaction.
     * 
     * @param nodeId
     *          The id of the requested Node.
     * @return The appropriate PersistentRecord or the WritableRecord if the lock is currently held.
     * @throws MirrorNoSuchRecordException
     *           If the given nodeId isn't held in the Node table at present.
     */
    public PersistentRecord getNode(int nodeId)
        throws MirrorNoSuchRecordException
    {
      return _nodeTable.getRecord(nodeId, getTransactionByNode(nodeId));
    }

    /**
     * If the node is locked by an update Transaction that is held by the current session then
     * return the appropriate WritableRecord otherwise just return the original PersistentRecord.
     */
    public PersistentRecord getNode(PersistentRecord node)
    {
      Preconditions.checkNotNull(node, "NodeUserManager.getNode - pre");
      node = node.filterByTransaction(getTransactionByNode(node.getId()));
      Preconditions.checkNotNull(node, "NodeUserManager.getNode - post");
      return node;
    }

    /**
     * Remove the given Transaction from the list of Transactions. Note that this does nothing to
     * the Transaction itself other than remove it from the data structure.
     * 
     * @param transaction
     *          The Transaction to remove.
     * @return true if the Transaction was clearly removed, false if it wasn't registered on the
     *         SessionTransaction in the first place.
     */
    public synchronized final boolean removeTransaction(Transaction transaction)
    {
      __transactionSession.remove(transaction);
      return _transactions.remove(transaction);
    }
  }
}
