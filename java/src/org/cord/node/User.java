package org.cord.node;

import java.util.Date;
import java.util.Random;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.FastDateField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.LongField;
import org.cord.mirror.field.PureJsonObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.ConstantTableQuery;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.mirror.trigger.UniqueFieldTrigger;
import org.cord.node.operation.UserPasswordValidator;
import org.cord.util.Gettable;
import org.json.JSONObject;

public class User
  extends NodeTableWrapper
{
  public final static String TABLENAME = "User";

  /**
   * @see NodeDbConstants#NAME
   */
  public final static ObjectFieldKey<String> NAME = NodeDbConstants.NAME.copy();

  public final static ObjectFieldKey<String> EMAIL = StringField.createKey("email");

  public final static ObjectFieldKey<String> LOGIN = StringField.createKey("login");

  public final static ObjectFieldKey<String> PASSWORD = StringField.createKey("password");

  public final static ObjectFieldKey<Date> CREATIONDATE = FastDateField.createKey("creationDate");

  public final static ObjectFieldKey<String> IPFILTER = StringField.createKey("ipFilter");

  public final static IntFieldKey STATUS = IntField.createKey("status");

  public final static int STATUS_INIT = 0;

  public final static int STATUS_CONFIRMED = 1;

  public final static int STATUS_SUSPENDED = 2;

  public final static ObjectFieldKey<Long> SECRETCODE = LongField.createKey("secretCode");

  public final static ObjectFieldKey<String> PLAINTEXTPASSWORD =
      StringField.createKey("plainTextPassword");

  public final static ObjectFieldKey<String> PASSWORDCLUE = StringField.createKey("passwordClue");

  public final static IntFieldKey USERSTYLE_ID = IntFieldKey.create("userStyle_id");
  public final static RecordOperationKey<PersistentRecord> USERSTYLE =
      RecordOperationKey.create("userStyle", PersistentRecord.class);

  // public static final ObjectFieldKey<String> JSONSRC = StringField.createKey("jsonSrc");
  // public static final RecordOperationKey<JSONObject> JSON =
  // RecordOperationKey.create("json", JSONObject.class);

  public static final ObjectFieldKey<JSONObject> JSON = PureJsonObjectField.createKey("json");

  public final static RecordOperationKey<RecordSource> USERACLS =
      RecordOperationKey.create("userAcls", RecordSource.class);

  /**
   * RecordOperation ("ownedNodes") that returns QueryWrapper containing Node records that are owned
   * by this User.
   * 
   * @see Node#TABLENAME
   * @see Node#OWNER_ID
   */
  public final static RecordOperationKey<RecordSource> OWNEDNODES =
      RecordOperationKey.create("ownedNodes", RecordSource.class);

  /**
   * "ownedNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> OWNEDNODESTYLES =
      RecordOperationKey.create("ownedNodeStyles", RecordSource.class);

  /**
   * TableStringMatcher ("findByLogin") on the User table that returns a QueryWrapper around a list
   * of Users with a specific login. This will normally only be a single user.
   */
  public final static MonoRecordSourceOperationKey<String, RecordSource> FINDBYLOGIN =
      MonoRecordSourceOperationKey.create("findByLogin", String.class, RecordSource.class);

  /**
   * ConstantTableQuery ("unconfirmedUsers") that resolves to the list of all User records whose
   * USER_STATUS is STATUS_UNCONFIRMED, ordered by EMAIL.
   * 
   * @see #STATUS
   * @see #EMAIL
   * @see org.cord.mirror.operation.ConstantTableQuery
   */
  public final static RecordSourceOperationKey<RecordSource> UNCONFIRMEDUSERS =
      RecordSourceOperationKey.create("unconfirmedUsers", RecordSource.class);

  /**
   * ConstantTableQuery ("confirmedUsers") that resolves to the list of all User records whose
   * STATUS is STATUS_CONFIRMED, ordered by EMAIL.
   * 
   * @see #STATUS
   * @see #STATUS_CONFIRMED
   * @see #EMAIL
   * @see org.cord.mirror.operation.ConstantTableQuery
   */
  public final static RecordSourceOperationKey<RecordSource> CONFIRMEDUSERS =
      RecordSourceOperationKey.create("confirmedUsers", RecordSource.class);

  /**
   * ConstantTableQuery ("suspendedUsers") that resolves to the list of all User records whose
   * STATUS is STATUS_SUSPENDED, ordered by EMAIL.
   * 
   * @see #STATUS
   * @see #STATUS_SUSPENDED
   * @see #EMAIL
   * @see org.cord.mirror.operation.ConstantTableQuery
   */
  public final static RecordSourceOperationKey<RecordSource> SUSPENDEDUSERS =
      RecordSourceOperationKey.create("suspendedUsers", RecordSource.class);

  /**
   * User id of the root administrator of the system.
   */
  public final static int ID_ROOT = 1;

  /**
   * User id of the anonymous public user of the system.
   */
  public final static int ID_ANONYMOUS = 2;

  public static final RecordOperationKey<String> ENGLISHSTATUS =
      RecordOperationKey.create("englishStatus", String.class);

  private final Random __random = new Random(System.currentTimeMillis());
  // private final JSONObject __defaultValue =
  // UnmodifiableJSONObject.getInstance(new WritableJSONObject());
  // private JSONObjectField _jsonSrc = null;
  private PureJsonObjectField _json = null;

  protected User(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME);
  }

  @Override
  public void initTable(Db db,
                        String pwd,
                        Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NAME, "Screen name"));
    table.addField(new StringField(EMAIL, "Email address"));
    // table.addFieldTrigger(new UniqueFieldTrigger(table, EMAIL));
    // TODO: (#260) review the immutable status of User.email as it should be
    // "mostly"
    // immutable
    // table.addFieldTrigger(new ImmutableFieldTrigger(EMAIL));
    table.addField(new StringField(LOGIN, "Login"));
    table.addFieldTrigger(new UniqueFieldTrigger(table, LOGIN));
    // table.addFieldTrigger(new ImmutableFieldTrigger(LOGIN));
    table.addField(new StringField(PASSWORD, "Password"));
    table.addField(new FastDateField(CREATIONDATE, "Creation date"));
    table.addField(new StringField(IPFILTER, "IP address filter"));
    table.addField(new IntField(STATUS, "User status"));
    table.addField(new LongField(SECRETCODE, "Secret code"));
    table.addField(new StringField(PLAINTEXTPASSWORD, "Plain text password"));
    table.addField(new StringField(PASSWORDCLUE, "Password clue"));
    table.addField(new LinkField(USERSTYLE_ID,
                                 "User style",
                                 null,
                                 USERSTYLE,
                                 UserStyle.TABLENAME,
                                 UserStyle.USERS,
                                 LOGIN.getKeyName(),
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    // table.addField(_jsonSrc = new JSONObjectField(JSONSRC, "Data params", JSON, __defaultValue));
    table.addField(_json =
        new PureJsonObjectField(JSON, "JSON", PureJsonObjectField.emptyFieldValueSupplier()));
    table.setTitleOperationName(LOGIN);
    table.setDefaultOrdering(LOGIN.getKeyName());
    table.addFieldTrigger(new AbstractFieldTrigger<String>(PASSWORD, true, false) {
      @Override
      protected Object triggerField(TransientRecord user,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        PersistentRecord userStyle = user.comp(USERSTYLE);
        String plainTextPassword = user.opt(PASSWORD);
        user.setField(PLAINTEXTPASSWORD,
                      userStyle.is(UserStyle.HASPLAINTEXTPASSWORD) ? plainTextPassword : "");
        String encryptedPassword =
            UserPasswordValidator.encrypt(plainTextPassword, getNodeManager());
        return encryptedPassword;
      }
    });
    table.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord user,
                             Gettable params)
          throws MirrorFieldException
      {
        user.setField(CREATIONDATE, new Date());
        user.setField(SECRETCODE, newSecretCode());
      }
    });
    table.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_PREDELETED_DELETED,
                                                   false,
                                                   TABLENAME) {
      @Override
      public void trigger(PersistentRecord user,
                          Transaction transaction,
                          int state)
      {
        getNodeManager().getUserManager().logout(user.getId());
      }
    });
    table.addRecordSourceOperation(new TableStringMatcher(FINDBYLOGIN,
                                                          LOGIN,
                                                          TableStringMatcher.SEARCH_EXACT,
                                                          null));
    table.addRecordOperation(new UserPasswordValidator(getNodeManager()));
    table.addRecordOperation(new SimpleRecordOperation<String>(ENGLISHSTATUS) {
      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        return getEnglishStatus(targetRecord.compInt(STATUS));
      }
    });
    table.addRecordSourceOperation(new ConstantTableQuery(UNCONFIRMEDUSERS,
                                                          STATUS + "=" + STATUS_INIT,
                                                          EMAIL));
    table.addRecordSourceOperation(new ConstantTableQuery(CONFIRMEDUSERS,
                                                          STATUS + "=" + STATUS_CONFIRMED,
                                                          EMAIL));
    table.addRecordSourceOperation(new ConstantTableQuery(SUSPENDEDUSERS,
                                                          STATUS + "=" + STATUS_SUSPENDED,
                                                          EMAIL));
  }

  public Long newSecretCode()
  {
    return new Long(__random.nextInt());
  }

  public static String getEnglishStatus(int status)
  {
    switch (status) {
      case STATUS_CONFIRMED:
        return "confirmed";
      case STATUS_INIT:
        return "init";
      case STATUS_SUSPENDED:
        return "suspended";
      default:
        return "unknown";
    }
  }

  /**
   * Resolve a User by their login.
   * 
   * @return the User in question. Never null
   * @throws MirrorNoSuchRecordException
   *           if login is not found in the database.
   */
  public PersistentRecord getInstance(String login)
      throws MirrorNoSuchRecordException
  {
    return TableStringMatcher.firstMatch(getTable(), LOGIN, login);
  }

  /**
   * Resolve a User by their login returning null if not found.
   * 
   * @return The User record, or null if the login is not found.
   */
  public PersistentRecord optUserByLogin(String login)
  {
    return TableStringMatcher.optOnlyMatch(getTable(), LOGIN, login, null);
  }

  /**
   * Resolve the userObject into a User record using the following rules:
   * <ul>
   * <li>If userObject is an Integer then look it up as an id</li>
   * <li>If userObject is a Number then convert it into an int and look it up as an id</li>
   * <li>If userObject is a NodeRequest then return the User that logged into the Session that
   * submitted the NodeRequest</li>
   * <li>If userObject is a PersistentRecord then validate that it is from the User Table and then
   * return it</li>
   * <li>Otherwise assume that userObject is a login so convert it to a String and then look it up
   * in the database</li>
   * </ul>
   * 
   * @return The User id or null if it is not possible to resolve.
   */
  public Integer optUserId(Object userObject)
  {
    if (userObject instanceof Integer) {
      return (Integer) userObject;
    }
    if (userObject instanceof Number) {
      return Integer.valueOf(((Number) userObject).intValue());
    }
    if (userObject instanceof NodeRequest) {
      return Integer.valueOf(((NodeRequest) userObject).getUser().getId());
    }
    if (userObject instanceof PersistentRecord) {
      PersistentRecord user = (PersistentRecord) userObject;
      PersistentRecord.assertIsTableNamed(user, User.TABLENAME);
      return Integer.valueOf(user.getId());
    }
    String userString = userObject.toString();
    try {
      return Integer.valueOf(userString);
    } catch (NumberFormatException nfEx) {
      PersistentRecord user = TableStringMatcher.optOnlyMatch(getTable(), LOGIN, userString, null);
      if (user != null) {
        return Integer.valueOf(user.getId());
      }
    }
    return null;
  }

  public int compUserId(Object userObject)
  {
    if (userObject instanceof Number) {
      return ((Number) userObject).intValue();
    }
    if (userObject instanceof NodeRequest) {
      return ((NodeRequest) userObject).getUser().getId();
    }
    if (userObject instanceof PersistentRecord) {
      PersistentRecord user = (PersistentRecord) userObject;
      PersistentRecord.assertIsTableNamed(user, User.TABLENAME);
      return user.getId();
    }
    String userString = userObject.toString();
    try {
      return Integer.parseInt(userString);
    } catch (NumberFormatException nfEx) {
      PersistentRecord user = TableStringMatcher.optOnlyMatch(getTable(), LOGIN, userString, null);
      if (user != null) {
        return user.getId();
      }
    }
    throw new IllegalArgumentException("Unable to convert " + userObject + " of type "
                                       + userObject.getClass() + " unto a UserId");
  }

  public Integer optOwnerId(Object userObject)
  {
    if (userObject instanceof Integer) {
      return (Integer) userObject;
    }
    if (userObject instanceof Number) {
      return Integer.valueOf(((Number) userObject).intValue());
    }
    if (userObject instanceof NodeRequest) {
      return Integer.valueOf(((NodeRequest) userObject).getTargetLeafNode().compInt(Node.OWNER_ID));
    }
    if (userObject instanceof PersistentRecord) {
      PersistentRecord user = (PersistentRecord) userObject;
      PersistentRecord.assertIsTableNamed(user, User.TABLENAME);
      return Integer.valueOf(user.getId());
    }
    String userString = userObject.toString();
    try {
      return Integer.valueOf(userString);
    } catch (NumberFormatException nfEx) {
      PersistentRecord user = TableStringMatcher.optOnlyMatch(getTable(), LOGIN, userString, null);
      if (user != null) {
        return Integer.valueOf(user.getId());
      }
    }
    return null;

  }
}
