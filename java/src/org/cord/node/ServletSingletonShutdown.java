package org.cord.node;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.cord.util.DebugConstants;
import org.cord.util.SingletonShutdown;

public final class ServletSingletonShutdown
  implements ServletContextListener
{
  public ServletSingletonShutdown()
  {
    if (DebugConstants.DEBUG_RESOURCES) {
      DebugConstants.DEBUG_OUT.println("ServletSingletonShutdown()");
    }
  }

  @Override
  public void contextInitialized(ServletContextEvent arg0)
  {
    if (DebugConstants.DEBUG_RESOURCES) {
      DebugConstants.DEBUG_OUT.println("ServletSingletonShutdown.contextInitialized()");
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent arg0)
  {
    if (DebugConstants.DEBUG_RESOURCES) {
      DebugConstants.DEBUG_OUT.println("ServletSingletonShutdown.contextDestroyed()");
    }
    SingletonShutdown.shutdown();
  }

}
