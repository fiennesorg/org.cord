package org.cord.node;

import java.util.Iterator;

import org.cord.mirror.IntFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.util.SkippingIterator;

import com.google.common.base.Preconditions;

/**
 * Class that wraps an Iterator of Nodes and only passes items that have a named ACL that satisfies
 * a constant User. The resolution of the ACL is full-depth (ie the user has to pass all of the
 * nodes from the root Node down to the child node). This is useful in situations when you have an
 * assortment of nodes and you wish to know which ones are readable as this will filter out the
 * nodes that have private parents which would otherwise not be caught by a shallow ACL check.
 */
public class AclNodeIteratorFilter
  extends SkippingIterator<PersistentRecord>
{
  private final NodeManager __nodeMgr;

  private final int __userId;

  private final IntFieldKey __aclFieldKey;

  /**
   * @param aclFieldKey
   *          The operation that can be applied to each Node which will result in an Acl to check
   *          against. Normally VIEWACL, EDITACL, DELETEACL or PUBLISHACL.
   * @param userId
   *          The id of the user who is expected to pass these ACLs.
   * @see Node#VIEWACL
   * @see Node#EDITACL
   * @see Node#DELETEACL
   * @see Node#PUBLISHACL
   */
  public AclNodeIteratorFilter(NodeManager nodeMgr,
                               Iterator<PersistentRecord> nodes,
                               IntFieldKey aclFieldKey,
                               int userId)
  {
    super(nodes);
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
    __aclFieldKey = aclFieldKey;
    __userId = userId;
  }

  @Override
  protected boolean shouldSkip(PersistentRecord node)
  {
    while (node != null) {
      if (!__nodeMgr.getAcl().acceptsAclUserOwner(node.compInt(__aclFieldKey),
                                                  __userId,
                                                  node.compInt(Node.OWNER_ID))) {
        return true;
      }
      node = node.opt(Node.PARENTNODE);
    }
    return false;
  }
}