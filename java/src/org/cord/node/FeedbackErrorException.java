package org.cord.node;

import org.cord.mirror.PersistentRecord;

import com.google.common.base.Strings;

/**
 * NodeRequestException that encapsulates a message from the NodeRequestSegmentFactory that should
 * be returned to the client rather than the default success operation being invoked.
 */
public class FeedbackErrorException
  extends NodeRequestException
{
  private static final long serialVersionUID = -5538954851111149225L;

  private final boolean __isVisibleCause;
  private final boolean __isLoggedCause;

  private final String __title;

  private String _webmacroTemplate = null;

  /**
   * @param isErrorCondition
   *          If true then this FeedbackErrorException is being thrown to reflect a condition that
   *          should not normally occur and which the system should provide supporting information
   *          to help diagnosing the error. If false then this FeedbackErrorException should be
   *          taken as an instruction to purely impart the message onto the client - the system is
   *          working correctly but we just wanted to pass a message across.
   */
  @Deprecated
  public FeedbackErrorException(String title,
                                String message,
                                PersistentRecord node,
                                Exception cause,
                                boolean isErrorCondition)
  {
    super(message,
          node,
          cause);
    __title = Strings.nullToEmpty(title);
    __isVisibleCause = isErrorCondition;
    __isLoggedCause = isErrorCondition;
  }

  @Deprecated
  public FeedbackErrorException(String title,
                                String message,
                                PersistentRecord node)
  {
    this(title,
         message,
         node,
         null,
         false);
  }

  /**
   * Create a FeedbackErrorException that is just used as a message to the user.
   */
  public FeedbackErrorException(String title,
                                PersistentRecord node,
                                String message,
                                Object... messageParams)
  {
    this(title,
         node,
         null,
         false,
         false,
         message,
         messageParams);
  }

  /**
   * Create a FeedbackErrorException with control as to whether it is shown to the user and / or the
   * log.
   * 
   * @param e
   *          The cause of the exception.
   * @param isVisibleCause
   *          If true then the Exception e will be shown to the user. If false then the exception
   *          will still be there as the cause of the exception but it will not be shown in public.
   * @param isLoggedCause
   *          if true then the stack trace of the nested error (if any) will be logged to the
   *          standard error output.
   */
  public FeedbackErrorException(String title,
                                PersistentRecord node,
                                Exception e,
                                boolean isVisibleCause,
                                boolean isLoggedCause,
                                String message,
                                Object... messageParams)
  {
    super(String.format(message, messageParams),
          node,
          e);
    __title = Strings.nullToEmpty(title);
    __isVisibleCause = isVisibleCause;
    __isLoggedCause = isLoggedCause;
  }

  /**
   * Create a FeedbackErrorException with a nested exception which is shown to both the user and the
   * log.
   */
  public FeedbackErrorException(String title,
                                PersistentRecord node,
                                Exception e,
                                String message,
                                Object... messageParams)
  {
    this(title,
         node,
         e,
         true,
         true,
         message,
         messageParams);
  }

  /**
   * Define a path to a webmacro template. If this is defined then the template should be included
   * by the webmacro template that is used to render the error.
   * 
   * @see #hasWebmacroTemplate()
   * @see #setWebmacroTemplate(String)
   */
  public void setWebmacroTemplate(String templatePath)
  {
    _webmacroTemplate = templatePath;
  }

  /**
   * Check to see if a non-empty value has been passed to setWebmacroTemplate(...)
   * 
   * @see #setWebmacroTemplate(String)
   * @see #getWebmacroTemplate()
   */
  public boolean hasWebmacroTemplate()
  {
    return !Strings.isNullOrEmpty(_webmacroTemplate);
  }

  /**
   * Get the nested Webmacro template for this exception if any
   * 
   * @return The value passed to setWebmacroTemplate(templatePath) or null if the method has not
   *         been invoked
   * @see #setWebmacroTemplate(String)
   * @see #hasWebmacroTemplate()
   */
  public String getWebmacroTemplate()
  {
    return _webmacroTemplate;
  }

  public final boolean isVisibleCause()
  {
    return __isVisibleCause;
  }

  public final boolean isLoggedCause()
  {
    return __isLoggedCause;
  }

  public String getTitle()
  {
    return __title;
  }
}
