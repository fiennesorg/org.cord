package org.cord.node;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.cord.mirror.Query;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.NamedImpl;
import org.cord.util.NumberUtil;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

/**
 * AbstractPaginator is a base class for representing the ability to divide a Query up into a series
 * of number sub-pages. The pages will be numbered sequentially from 0 to n.
 */
public abstract class AbstractPaginator
  extends NamedImpl
{
  /**
   * The namespace sensitive name of the parameter that contains the list of non-namespace sensitive
   * additional fields that are part of this AbstractPaginator. The field names will be comma
   * seperated with no whitespace.
   */
  public final static String CGI_ADDITIONALFIELDS = "additionalFields";

  /**
   * The value that the AbstractPaginator takes as the first page in the index. This will generally
   * be either 1 or 0 depending on whether or not we are going for a computer friendly or a human
   * friendly counting system. This is currently set to 1 and should probably be migrated to a
   * configuration variable at some point in the future.
   */
  public final static int PAGEINDEX_FIRST = 1;

  private RecordSource _sourceQuery;

  private Multimap<String, String> _additionalFields;

  private int _pageIndex = -1;

  private boolean _hasExplicitPage = false;

  protected AbstractPaginator(String name,
                              RecordSource sourceQuery,
                              Multimap<String, String> additionalFields,
                              boolean hasExplicitPage)
  {
    super(name);
    setSourceQuery(sourceQuery);
    _additionalFields = additionalFields;
    _hasExplicitPage = hasExplicitPage;
  }

  protected AbstractPaginator(AbstractPaginator paginator)
  {
    this(paginator.getName(),
         paginator.getSourceQuery(),
         paginator.getAdditionalFields(),
         paginator.hasExplicitPage());
  }

  /**
   * @return The appropriate Table or null if getSourceQuery() returns null
   * @see #getSourceQuery()
   */
  public Table getTable()
  {
    RecordSource recordSource = getSourceQuery();
    return recordSource == null ? null : recordSource.getTable();
  }

  public boolean hasExplicitPage()
  {
    return _hasExplicitPage;
  }

  /**
   * Abstract method to get an instance of the implementation of the AbstractPaginator for the given
   * page in the current selection.
   */
  public abstract AbstractPaginator getPage(int pageIndex);

  /**
   * Abstract method to get the number of pages that are supported by this AbstractPaginator with
   * the current configuration.
   */
  public abstract int getPageCount();

  /**
   * Add the specified field,value tuplet onto an http get parameter.
   */
  public final static void appendGetParam(StringBuilder buffer,
                                          String field,
                                          String value)
  {
    if (Strings.isNullOrEmpty(field) || Strings.isNullOrEmpty(value)) {
      return;
    }
    if (buffer.length() > 0) {
      buffer.append("&amp;");
    }
    buffer.append(field).append('=').append(StringUtils.urlEncodeUtf8(value));
  }

  /**
   * Add the set of field,value tuplets onto an http get parameter string.
   * 
   * @param buffer
   *          The buffer that is to contain the http request.
   * @param values
   *          Map of (String-->String) tuplets that represent field,value tuplets to be added to the
   *          request.
   * @see #appendGetParam(StringBuilder,String,String)
   */
  protected final void appendGetParam(StringBuilder buffer,
                                      Multimap<String, String> values)
  {
    if (values == null) {
      return;
    }
    for (Map.Entry<String, String> entry : values.entries()) {
      appendGetParam(buffer, entry.getKey(), entry.getValue());
    }
  }

  /**
   * Get the AbstractPaginator that represents the first page in the list of pages that this
   * AbstractPaginator represents.
   * 
   * @see #getPage(int)
   * @see #PAGEINDEX_FIRST
   */
  public final AbstractPaginator getFirst()
  {
    return getPage(PAGEINDEX_FIRST);
  }

  /**
   * Get the AbstractPaginator that represents the last page in the list of pages that this
   * AbstractPaginator represents.
   * 
   * @see #getPage(int)
   * @see #getPageCount()
   */
  public final AbstractPaginator getLast()
  {
    return getPage(getPageCount() + PAGEINDEX_FIRST - 1);
  }

  /**
   * Check to see whether or not there is a page before this page in this list represented by this
   * AbstractPaginator.
   * 
   * @return true if there is a previous page.
   * @see #getPageIndex()
   * @see #PAGEINDEX_FIRST
   */
  public final boolean getHasPreviousPage()
  {
    return getPageIndex() > PAGEINDEX_FIRST;
  }

  /**
   * Check to see whether or not there is a page after this page in the list represented by this
   * AbstractPaginator.
   * 
   * @return true if there is a next page.
   * @see #getPageIndex()
   * @see #getPageCount()
   */
  public final boolean getHasNextPage()
  {
    return getPageIndex() < getPageCount();
  }

  /**
   * Get the previous page from this page in the current list.
   * 
   * @return The previous page, or this page if there is no previous page.
   * @see #getHasPreviousPage()
   */
  public final AbstractPaginator getPreviousPage()
  {
    return (getHasPreviousPage() ? getPage(getPageIndex() - 1) : this);
  }

  /**
   * Get the next page from this page in the current list.
   * 
   * @return The next page, or this page if there is no next page.
   * @see #getHasNextPage()
   */
  public final AbstractPaginator getNextPage()
  {
    return (getHasNextPage() ? getPage(getPageIndex() + 1) : this);
  }

  /**
   * Get the page index of this page within the list counting by PAGEINDEX_FIRST.
   * 
   * @return pageIndex value ranging from PAGEINDEX_FIRST to (PAGEINDEX_FIRST + getPageCount())
   * @see #PAGEINDEX_FIRST
   */
  public final int getPageIndex()
  {
    return _pageIndex;
  }

  /**
   * Set the page index for this page within the list. This doesn't currently do any validation of
   * the value which should be fixed at some point in the future, but there is an unusual error in
   * the order of initialisation of the classes that leads to erroneous results.
   * 
   * @return The page index after validation (which is currently none).
   */
  public final int setPageIndex(int pageIndex)
  {
    _pageIndex = pageIndex;
    return _pageIndex;
  }

  /**
   * Import the list of additional fields as defined in CGI_ADDITIONALFIELDS from NodeRequest into
   * this AbstractPaginator. Fields that are declared but not defined will not be added.
   */
  protected final void importAdditionalFields(Gettable nodeRequest)
  {
    String additionalFields = nodeRequest.getString(createParameterName(CGI_ADDITIONALFIELDS));
    if (additionalFields != null) {
      StringTokenizer fields = new StringTokenizer(additionalFields, ",");
      while (fields.hasMoreTokens()) {
        String fieldName = fields.nextToken();
        String value = nodeRequest.getString(fieldName);
        if (value != null) {
          addAdditionalField(fieldName, value);
        }
      }
    }
  }

  /**
   * Set the Query that this AbstractPaginator is to divide up into sublists.
   * 
   * @see #getSourceQuery()
   */
  public void setSourceQuery(RecordSource sourceQuery)
  {
    _sourceQuery = sourceQuery;
  }

  // /**
  // * Set the Query that this AbstractPaginator is to divide up into sublists
  // to
  // * the contents of the given QueryWrapper.
  // *
  // * @see #setSourceQuery(RecordSource)
  // */
  // public void setSourceQuery(QueryWrapper sourceQueryWrapper)
  // {
  // if (sourceQueryWrapper != null) {
  // setSourceQuery(sourceQueryWrapper.getQuery());
  // }
  // }

  /**
   * Get the core Query that this AbstractPaginator is using as its source of records to iterate
   * across.
   * 
   * @return The source Query That is used to generate records for this AbstractPaginator. Please
   *         note that this may be null if an alternative storage model is being utilised by the
   *         implementation of AbstractPaginator.
   * @see #setSourceQuery(RecordSource)
   */
  public final RecordSource getSourceQuery()
  {
    return _sourceQuery;
  }

  /**
   * Get the additional fields Map even if it is null.
   */
  protected final Multimap<String, String> getAdditionalFields()
  {
    return _additionalFields;
  }

  /**
   * Lookup all the values associated with the given key in the additional fields for this
   * AbstractPaginator.
   * 
   * @return unmodifiable Collection. If no data then it will be an empty Collection, never null.
   */
  public Collection<String> getAdditionalField(String key)
  {
    Multimap<String, String> additionalFields = getAdditionalFields();
    return additionalFields == null
        ? ImmutableSet.<String> of()
        : Collections.unmodifiableCollection(additionalFields.get(key));
  }

  /**
   * Lookup all the values associated with the given key and transform them into a String with the
   * given separator.
   * 
   * @return The appropriate String. If there are no values then the result will be an empty String,
   *         never null.
   */
  public String getAdditionalField(String key,
                                   String separator)
  {
    return Joiner.on(separator).join(getAdditionalField(key));
  }

  /**
   * Get the additional fields Map, creating a new Map if it doesn't already exist.
   */
  protected final Multimap<String, String> createAdditionalFields()
  {
    if (_additionalFields == null) {
      _additionalFields = ArrayListMultimap.create();
    }
    return _additionalFields;
  }

  /**
   * Add a name:value mapping to the list of additional fields, creating the additional field map if
   * this is the first added field.
   * 
   * @param key
   *          The key of the field. null will cause the operation to transparently abort. Will be
   *          converted into a String for actual storage.
   * @param value
   *          The value to be assigned to name. null will cause the operation to transparently
   *          abort.
   */
  public final void addAdditionalField(Object key,
                                       String value)
  {
    if (key != null & value != null) {
      createAdditionalFields().put(key.toString(), value);
    }
  }

  /**
   * Add a name:boolean mapping to the list of additional fields.
   */
  public final void addAdditionalField(Object name,
                                       boolean value)
  {
    addAdditionalField(NodeRequest.BOOLEAN_ACTIVE_NAMECACHE.getNamespacedName(name), "true");
    if (value) {
      addAdditionalField(name, "true");
    }
  }

  public final void addAdditionalField(Object name,
                                       Object value)
  {
    if (value != null) {
      if (value instanceof Boolean) {
        addAdditionalField(name, ((Boolean) value).booleanValue());
      } else if (value instanceof Enum<?>) {
        addAdditionalField(name, ((Enum<?>) value).name());
      } else {
        addAdditionalField(name, StringUtils.toString(value));
      }
    }
  }

  public final void addAdditionalFields(String name,
                                        Iterable<?> values)
  {
    if (values != null) {
      for (Object value : values) {
        addAdditionalField(name, value);
      }
    }
  }
  
  public final void addAdditionalFields(String name, NodeRequest nodeRequest)
  {
    for (Object value : nodeRequest.getValues(name)) {
      addAdditionalField(name, value);
    }
  }

  /**
   * Query the given NodeRequest for a named value and if it is present then add it to this
   * Paginator as an additional field.
   */
  public final void addAdditionalField(String name,
                                       NodeRequest nodeRequest)
  {
    String value = nodeRequest.getString(name);
    if (value != null) {
      addAdditionalField(name, value);
    }
  }

  /**
   * Create a namespace sensitive parameter name.
   * 
   * @return paginatorName_variableName
   */
  public final static String createParameterName(String paginatorName,
                                                 String variableName)
  {
    return paginatorName + "_" + variableName;
  }

  /**
   * Create a namespace sensitive parameter name that is appropriate to this AbstractPaginator.
   * 
   * @see #createParameterName(String,String)
   */
  public final String createParameterName(String variableName)
  {
    return createParameterName(getName(), variableName);
  }

  public final static void appendFormField(Appendable buffer,
                                           Object fieldName,
                                           Object value)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(buffer, value, StringUtils.toString(fieldName));
  }

  public final static void appendFormField(Appendable buffer,
                                           Object fieldName,
                                           int value)
      throws IOException
  {
    HtmlUtilsTheme.hiddenInput(buffer, Integer.toString(value), StringUtils.toString(fieldName));
  }

  public final void appendAdditionalFormFields(Appendable buffer)
      throws IOException
  {
    if (_additionalFields != null) {
      StringBuilder additionalFieldNames = new StringBuilder();
      Iterator<Map.Entry<String, String>> entries = _additionalFields.entries().iterator();
      while (entries.hasNext()) {
        Map.Entry<String, String> entry = entries.next();
        String fieldName = entry.getKey();
        appendFormField(buffer, fieldName, entry.getValue());
        additionalFieldNames.append(fieldName);
        if (entries.hasNext()) {
          additionalFieldNames.append(',');
        }
      }
      appendFormField(buffer,
                      createParameterName(CGI_ADDITIONALFIELDS),
                      additionalFieldNames.toString());
    }
  }

  public final String getAdditionalFormFields()
  {
    StringBuilder buffer = new StringBuilder();
    try {
      appendAdditionalFormFields(buffer);
    } catch (IOException e) {
      throw new StringBuilderIOException(buffer, e);
    }
    return buffer.toString();
  }

  /**
   * Abstract method to get any hashCode related information that should match up with the extended
   * equals(...) method. This will be invoked by the original hashCode() method and the result XORed
   * with the output. It should correspond to any properties that are referenced in the
   * equals(AbstractPaginator) method to preserve the hashCode() contract.
   * 
   * @see #hashCode()
   * @see #customEquals(AbstractPaginator)
   * @see #equals(Object)
   */
  protected abstract int subHashCode();

  /**
   * Calculate the hashCode by taking the hashCode of the source Query, XORing it with the page
   * index and then XORing it with the output of subHashCode().
   * 
   * @see Query#hashCode()
   * @see #getPageIndex()
   * @see #subHashCode()
   */
  @Override
  public final int hashCode()
  {
    int hashCode = 0;
    RecordSource sourceQuery = getSourceQuery();
    if (sourceQuery != null) {
      hashCode = hashCode ^ sourceQuery.hashCode();
    }
    return (hashCode ^ getPageIndex() ^ subHashCode());
  }

  /**
   * Abstract equality check that should verify any fields that are required beyond the core
   * properties defined in equals(Object). This will be invoked automatically by equals(Object) if
   * and only if the AbstractPaginator that this is being compared to is found to be equal in all
   * core properties. Any additional fields that contribute to the equality test should be reflected
   * in the subHashCode() method.
   * 
   * @see #subHashCode()
   * @see #equals(Object)
   */
  protected abstract boolean customEquals(AbstractPaginator paginator);

  @Override
  public final boolean equals(Object object)
  {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    AbstractPaginator paginator = (AbstractPaginator) object;
    if (getPageIndex() != paginator.getPageIndex()) {
      return false;
    }
    return (getPageIndex() == paginator.getPageIndex()
            && Objects.equal(getSourceQuery(), paginator.getSourceQuery())
            && customEquals(paginator));
  }

  /**
   * Get a list of all of the pages in this list starting with the first through to the last.
   * 
   * @return Iterator of AbstractPaginator
   */
  public final Iterator<AbstractPaginator> iterator()
  {
    return new AbstractPaginatorIterator(getPage(PAGEINDEX_FIRST), -1);
  }

  /**
   * Get a list of segmentSize pages from this list, doing the best to keep the current page in the
   * middle of the list while ensuring that there is always segmentSize elements in the list.
   * 
   * @return Iterator of AbstractPaginator
   */
  public Iterator<AbstractPaginator> iterator(int segmentSize)
  {
    int startingPoint = PAGEINDEX_FIRST;
    if (getPageCount() > segmentSize) {
      startingPoint = getPageIndex() - (segmentSize / 2);
      if (startingPoint < PAGEINDEX_FIRST) {
        startingPoint = PAGEINDEX_FIRST;
      } else if (getPageCount() - startingPoint < segmentSize) {
        startingPoint = getPageCount() - segmentSize + PAGEINDEX_FIRST;
      }
    }
    return new AbstractPaginatorIterator(getPage(startingPoint), segmentSize);
  }

  public final String getIdentifier()
  {
    return createParameterName(NumberUtil.toString(getPageIndex()));
  }

  /**
   * Abstract method that should append any of the form fields that are not contained inside the
   * additionalFields model to the given StringBuilder. The resulting fields should be sufficient to
   * re-initialise an implementation of AbstractPaginator from an incoming NodeRequest.
   */
  public abstract void appendCoreFormFields(Appendable buffer)
      throws IOException;

  public final String getCoreFormFields()
  {
    StringBuilder buffer = new StringBuilder();
    try {
      appendCoreFormFields(buffer);
    } catch (IOException e) {
      throw new StringBuilderIOException(buffer, e);
    }
    return buffer.toString();
  }

  public final String getFormFields()
  {
    StringBuilder buffer = new StringBuilder();
    try {
      appendCoreFormFields(buffer);
      appendAdditionalFormFields(buffer);
    } catch (IOException e) {
      throw new StringBuilderIOException(buffer, e);
    }
    return buffer.toString();
  }

  public class AbstractPaginatorIterator
    implements Iterator<AbstractPaginator>
  {
    private AbstractPaginator _paginator;

    private int _count;

    public AbstractPaginatorIterator(AbstractPaginator paginator,
                                     int count)
    {
      _paginator = paginator;
      _count = count;
    }

    @Override
    public boolean hasNext()
    {
      return (_paginator != null && _count != 0);
    }

    @Override
    public AbstractPaginator next()
    {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      _count = (_count > 0 ? _count - 1 : _count);
      AbstractPaginator result = _paginator;
      _paginator = (_paginator.getHasNextPage() ? _paginator.getNextPage() : null);
      return result;
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException();
    }
  }
}
