package org.cord.node;

import org.cord.util.GettableFactory;
import org.cord.util.SingletonShutdown;
import org.cord.util.SingletonShutdownable;

/**
 * Singleton Implementation of NodeManagerFactory that returns an instance that supports no custom
 * functionality above and beyond the default Node implementation. This is very useful for
 * situations when you just want to boot a standard site very quickly without the generation of
 * additional functionality.
 */
public final class NullNodeManagerFactory
  extends NodeManagerFactory
  implements SingletonShutdownable
{
  private static NodeManagerFactory _instance = new NullNodeManagerFactory();

  public static NodeManagerFactory getInstance()
  {
    return _instance;
  }

  @Override
  public void shutdownSingleton()
  {
    _instance = null;
  }

  private NullNodeManagerFactory()
  {
    SingletonShutdown.registerSingleton(this);
  }

  @Override
  protected void initialise(GettableFactory gettableFactory,
                            NodeManager nodeManager,
                            String transactionPassword)
  {
  }

  @Override
  protected void lock()
  {
  }
}
