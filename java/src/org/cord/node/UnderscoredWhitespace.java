package org.cord.node;

/**
 * Implementation of FilenameGenerator that replaces all illegal characters with single instance of
 * "_" while maintaining case of legal characters. This is 100% compatible with the legacy
 * hard-coded filename handling routines and is the default approach if an alternative is not
 * specified.
 */
public class UnderscoredWhitespace
  implements FilenameGenerator
{
  @Override
  public String generateFilename(String proposedFilename)
  {
    if (proposedFilename == null || proposedFilename.length() == 0) {
      return null;
    }
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < proposedFilename.length(); i++) {
      char c = proposedFilename.charAt(i);
      if (Character.isLetterOrDigit(c)) {
        result.append(c);
      } else {
        switch (c) {
          case '\'':
            break;
          case '@':
          case '.':
          case '_':
          case '-':
            result.append(c);
            break;
          default:
            if (result.length() > 0 && result.charAt(result.length() - 1) != '_') {
              result.append('_');
            }
        }
      }
    }
    return (result.length() == 0
        ? null
        : (result.charAt(result.length() - 1) == '_'
            ? result.toString().substring(0, result.length() - 1)
            : result.toString()));
  }
}
