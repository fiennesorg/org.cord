package org.cord.node;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.cord.mirror.Db;
import org.cord.mirror.DbBooter;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.node.config.NodeConfig;
import org.cord.util.ConstantSettableFactory;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableFactory;
import org.cord.util.GettableUtils;
import org.cord.util.ImmutableSettableGettable;
import org.cord.util.Settable;
import org.cord.util.SettableFactory;
import org.webmacro.WebMacro;
import org.xml.sax.SAXException;

/**
 * Factory class that permits the booting of a NodeManager for use in both a Servlet style or a
 * standalone application.
 */
public abstract class NodeManagerFactory
{
  public final static String PARAM_SITEADMINEMAIL = "siteAdminEmail";

  public final static String DEFAULT_SITEADMINEMAIL = "webmaster@localhost";

  /**
   * Method that is invoked from NodeManager.lock() after all of the internal locking definitions
   * have been completed relative to the NodeManager and before the status of the NodeManager is set
   * to locked. This can therefore be used for last-moment things that need a fully initialised
   * NodeManager to be able to do the work.
   */
  protected abstract void lock();

  protected void preInitialiseNodeTree(NodeManager nodeMgr)
      throws Exception
  {
  }

  protected void postInitialiseNodeTree(NodeManager nodeMgr)
      throws Exception
  {
  }

  public final NodeManager createNodeManager(String transactionPassword,
                                             NodeConfig config,
                                             boolean isBooted)
      throws IOException, ParserConfigurationException, MirrorTableLockedException,
      MirrorNoSuchRecordException, SAXException, MirrorTransactionTimeoutException
  {
    return createNodeManager(transactionPassword, config, isBooted, null);
  }

  public final NodeManager createNodeManager(String transactionPassword,
                                             NodeConfig config,
                                             boolean isBooted,
                                             WebMacro webmacro)
      throws MirrorTableLockedException, MirrorNoSuchRecordException,
      MirrorTransactionTimeoutException
  {
    Gettable gettable = config.getGettable(NodeConfig.Namespace.NODE);
    Settable settable = new ImmutableSettableGettable(gettable);
    SettableFactory settableFactory = new ConstantSettableFactory(settable);
    Gettable dbConfiguration = config.getGettable(NodeConfig.Namespace.MIRROR);
    return createNodeManager(settableFactory,
                             transactionPassword,
                             config.getDocBase(),
                             webmacro,
                             isBooted,
                             dbConfiguration);
  }

  /**
   * @deprecated use the NodeConfig-based {@link #createNodeManager(String, NodeConfig, boolean)}
   *             instead, or consider using an ant task like initialise-node-tree
   */
  @Deprecated
  public final NodeManager createNodeManager(String transactionPassword,
                                             File webXml,
                                             boolean stripDtd,
                                             boolean isBooted)
      throws IOException, ParserConfigurationException, MirrorTableLockedException,
      MirrorNoSuchRecordException, SAXException, MirrorTransactionTimeoutException
  {
    return createNodeManager(transactionPassword,
                             new NodeConfig(webXml.getParentFile().getParentFile(),
                                            DebugConstants.DEBUG_RESOURCES
                                                ? DebugConstants.DEBUG_OUT
                                                : null),
                             isBooted);
  }

  /**
   * Create a NodeManager, taking the configuration parameters from an unspecified GettableFactory
   * source. This is the preferred method of booting the NodeManager as it permits the extension of
   * the NodeManager initialisation process without invalidating legacy client APIs.
   */
  public final NodeManager createNodeManager(SettableFactory settableFactory,
                                             String transactionPassword,
                                             String webRootPath,
                                             WebMacro webMacro,
                                             boolean isBooted,
                                             Gettable dbConfiguration)
      throws MirrorTableLockedException, MirrorNoSuchRecordException,
      MirrorTransactionTimeoutException
  {
    System.err.println(this + ".createNodeManager: " + settableFactory);
    Db db = null;
    NodeManager nodeManager = null;
    boolean isClean = false;
    try {
      db = DbBooter.getDb(settableFactory.getGettable(),
                          "NodeDb",
                          transactionPassword,
                          dbConfiguration);
      db.lockConnectionToThread();
      String siteAdminEmail = GettableUtils.get(settableFactory.getGettable(),
                                                PARAM_SITEADMINEMAIL,
                                                DEFAULT_SITEADMINEMAIL);
      try {
        nodeManager =
            new NodeManager(this,
                            settableFactory,
                            db,
                            transactionPassword,
                            new File("/tmp"),
                            GettableUtils.get(settableFactory,
                                              NodeServlet.INITPARAM_HOSTNAME,
                                              null),
                            GettableUtils.get(settableFactory,
                                              NodeServlet.INITPARAM_HTTPSERVLETPATH,
                                              null),
                            siteAdminEmail,
                            webRootPath,
                            webMacro,
                            isBooted,
                            Authenticators.getOptInstance(settableFactory.getGettable(),
                                                          NodeServlet.INITPARAM_AUTHENTICATOR_CLASSNAME));
      } catch (Exception e) {
        throw new IllegalArgumentException("Unable to initialise NodeManager", e);
      }
      db.preLock();
      initialise(settableFactory, nodeManager, transactionPassword);
      db.preLock();
      isClean = true;
      return nodeManager;
    } finally {
      if (db != null) {
        db.releaseConnectionFromThread();
      }
      if (!isClean) {
        if (nodeManager != null) {
          nodeManager.shutdown();
        } else if (db != null) {
          db.shutdown(true);
        }
      }
    }
  }

  /**
   * Do any application specific initialisation of the NodeManager that is required. This will
   * generally include anything that is not web-site specific and should bring the NodeManager up to
   * the level whereby the data structure is coherent. The following should be registered:-
   * <ul>
   * <li>Custom ContentCompilers
   * <li>Db extensions, including tables and triggers
   * </ul>
   * <p>
   * It should not be necessary to register the following:-
   * <ul>
   * <li>Custom views onto the data
   * </ul>
   * 
   * @param gettableFactory
   *          The data source for configuration options. In a web environment, this will generally
   *          be a descendant of NodeServlet.
   * @param nodeManager
   *          The NodeManager that has had its initial configuration completed and which requires
   *          the final customisation before deployment.
   * @param transactionPassword
   *          The db specific transactionPassword.
   * @throws MirrorTableLockedException
   *           If there is a problem registering new database functionality onto the system.
   * @throws MirrorTransactionTimeoutException
   *           If any of the registration processes run into a contested lock situation. This is
   *           unlikely because the system will generally be running in mono-threaded mode during
   *           bootup so it would imply a coding error rather than a multi-threading issue (clients
   *           not closing Transactional locks properly during bootup).
   */
  protected abstract void initialise(GettableFactory gettableFactory,
                                     NodeManager nodeManager,
                                     String transactionPassword)
      throws MirrorTableLockedException, MirrorTransactionTimeoutException;

  /** @deprecated use the ant initialise-node-tree task instead */
  @Deprecated
  public NodeManager initialiseNodeTree(String transactionPassword,
                                        File webXml,
                                        boolean stripDtd,
                                        String rootHtmlTitle,
                                        String rootTitle)
      throws Exception
  {
    NodeManager nodeManager = createNodeManager(transactionPassword, webXml, stripDtd, false);
    nodeManager.lock();
    return initialiseNodeTree(nodeManager, rootHtmlTitle, rootTitle);
  }

  @Deprecated
  private NodeManager initialiseNodeTree(NodeManager nodeManager,
                                         String rootHtmlTitle,
                                         String rootTitle)
      throws Exception
  {
    boolean isInitialised = false;
    try {
      nodeManager.initialiseNodeTree(rootHtmlTitle, rootTitle);
      isInitialised = true;
    } finally {
      if (!isInitialised) {
        nodeManager.shutdown();
        nodeManager = null;
      }
    }
    return nodeManager;
  }
}
