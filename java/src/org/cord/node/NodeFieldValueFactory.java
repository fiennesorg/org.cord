package org.cord.node;

import org.cord.mirror.FieldKey;

/**
 * Implementation of ValueFactory that wraps a constant header and footer around a named Node field
 * on the TargetLeafNOde of the given NodeRequest.
 * 
 * @author alex
 * @see NodeRequest#getTargetLeafNode()
 */
public class NodeFieldValueFactory
  implements ValueFactory
{
  private final String __header;

  private final FieldKey<?> __fieldName;

  private final String __footer;

  public NodeFieldValueFactory(String header,
                               FieldKey<?> fieldName,
                               String footer)
  {
    __header = header;
    __fieldName = fieldName;
    __footer = footer;
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    return __header + nodeRequest.getTargetLeafNode().opt(__fieldName) + __footer;
  }

  @Override
  public void shutdown()
  {
  }
}
