package org.cord.node;

import javax.servlet.http.HttpServletRequest;

public class RequestPath
{
  private final String _hostName;

  private final String _servletAlias;

  private final String _virtualPath;

  public RequestPath(String hostName,
                     String servletAlias,
                     String virtualPath)
  {
    _hostName = hostName == null ? "" : hostName;
    _servletAlias = servletAlias == null ? "" : servletAlias;
    _virtualPath = virtualPath == null ? "" : virtualPath;
  }

  public RequestPath(HttpServletRequest request,
                     String servletAlias)
  {
    this(request.getHeader("host"),
         servletAlias,
         request.getPathInfo());
  }

  public RequestPath(HttpServletRequest request,
                     NodeManager nodeManager)
  {
    this(request,
         nodeManager.getHttpServletPath());
  }

  public RequestPath(HttpServletRequest request)
  {
    this(request,
         request.getServletPath());
  }

  public String getHostName()
  {
    return _hostName;
  }

  public String getServletAlias()
  {
    return _servletAlias;
  }

  public String getVirtualPath()
  {
    return _virtualPath;
  }

  @Override
  public String toString()
  {
    return "http://[" + _hostName + "]/[" + _servletAlias + "]/[" + _virtualPath + "]";
  }
}
