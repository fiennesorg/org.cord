package org.cord.node;

/**
 * Implementation of FilenameGenerator that strips out all whitespace and then mashes the words
 * together ensuring that sentence case is preserved resulting in filenames such as
 * "ThisIsAFilename".
 */
public class SentenceCaseNoWhitespace
  implements FilenameGenerator
{
  @Override
  public String generateFilename(String proposedFilename)
  {
    if (proposedFilename == null) {
      return null;
    }
    String[] words = proposedFilename.split("[^a-zA-Z_0-9'\\-\\.]");
    if (words.length == 1) {
      return filterApostrophes(proposedFilename);
    }
    StringBuilder result = new StringBuilder(proposedFilename.length());
    for (int i = 0; i < words.length; i++) {
      String word = words[i];
      if (word.length() > 0) {
        char firstCharacter = word.charAt(0);
        if (Character.isLowerCase(firstCharacter)) {
          result.append(Character.toUpperCase(firstCharacter));
          if (word.length() > 1) {
            result.append(word.substring(1));
          }
        } else {
          result.append(word);
        }
      }
    }
    return filterApostrophes(result.toString());
  }

  private String filterApostrophes(String source)
  {
    return source.replaceAll("'", "");
  }
}
