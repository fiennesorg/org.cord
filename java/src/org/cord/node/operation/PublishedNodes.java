package org.cord.node.operation;

import org.cord.mirror.Db;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractRecordSourceResolverOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.node.NodeStyle;

public class PublishedNodes
  extends AbstractRecordSourceResolverOperation
{
  private final Db __db;

  public PublishedNodes(Db db)
  {
    super(NodeStyle.PUBLISHEDNODES);
    __db = db;
  }

  @Override
  protected Table getRecordSourceTable()
  {
    return __db.getTable(Node.TABLENAME);
  }

  @Override
  protected RecordSource createRecordSource(TransientRecord nodeStyle,
                                            Object cacheKey,
                                            Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(getCachedRecordSourceTable().getQuery(cacheKey,
                                                                          Node.NODESTYLE_ID + "="
                                                                                    + nodeStyle.getId()
                                                                                    + " AND "
                                                                                    + Node.ISPUBLISHED
                                                                                    + "=1",
                                                                          null),
                                    viewpoint);
  }
}
