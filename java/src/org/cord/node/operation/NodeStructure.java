package org.cord.node.operation;

import org.cord.node.NodeStyle;
import org.cord.util.NumberUtil;

/**
 * Class of static utility methods that enables changes to be made to the structure of a populated
 * Node system.
 */
public class NodeStructure
{
  /**
   * Get the error in the number of instances of a resource, given the updated values for minimum
   * and maximum.
   * 
   * @param existingCount
   *          The number of instances of asset (either Node or Region in all probability) that exist
   *          and that need to be validated.
   * @param minimum
   *          The encoded target minimum value.
   * @param filenames
   *          The pre-defined filename field (NODESTYLE_FILENAME) if relevant (ie if minimum == -1)
   * @param maximum
   *          The encoded target maximum value.
   * @return The error in existing Count. A value of 0 indicates that existingCount is OK. Less than
   *         0 indicates that assets must be deleted. More than 0 indicates that assets should be
   *         created. The magnitude of the return value indicates the number of assets in each case.
   */
  public final static int getNodeCountError(int existingCount,
                                            int minimum,
                                            String filenames,
                                            int maximum)
  {
    switch (minimum) {
      case -1:
        int filenameMaximum = NodeStyle.getFilenameCount(filenames);
        // int filenameMaximum =
        // NodeGroupPostBooter.getFilenameCount(filenames);
        if (existingCount < filenameMaximum) {
          // --> that there have not been enough nodes created
          // --> the result should be positive...
          return filenameMaximum - existingCount;
        }
        // more than filenameMaximum is ok so continue...
        break;
      case 0:
        // no minimum so all values are OK.
        break;
      default:
        if (existingCount < minimum) {
          // not enough records created so result so positive...
          return minimum - existingCount;
        }
    }
    switch (maximum) {
      case -1:
        // no maximum so all values are OK.
        break;
      default:
        if (existingCount > maximum) {
          // too many nodes so result is negative...
          return maximum - existingCount;
        }
    }
    return 0;
  }

  /**
   * Get the error in the number of instances of Region, given the updated values for minimum and
   * maximum.
   * 
   * @param existingCount
   *          The number of instances of the Region that exist and that need to be validated.
   * @param minimum
   *          The encoded target minimum value.
   * @param maximum
   *          The encoded target maximum value.
   * @return The error in existing Count. A value of 0 indicates that existingCount is OK. Less than
   *         0 indicates that Regions must be deleted. More than 0 indicates that Regions should be
   *         created. The magnitude of the return value indicates the number of Regions in each
   *         case.
   */
  public final static int getRegionCountError(int existingCount,
                                              int minimum,
                                              int maximum)
  {
    switch (minimum) {
      case 0:
        // no minimum so all values are ok...
        break;
      default:
        if (existingCount < minimum) {
          // --> not enough regions, so we will have to create some
          return minimum - existingCount;
        }
    }
    switch (NumberUtil.sign(maximum)) {
      case -1:
        // non-auto-create maximum...
        int maximumCount = maximum * -1;
        if (existingCount > maximumCount) {
          return maximumCount - existingCount;
        }
      case 0:
        // no maximum so OK...
        break;
      case 1:
        // auto-create maximum...
        if (existingCount != maximum) {
          return maximum - existingCount;
        }
        break;
    }
    // everything OK so far so nothing needs changed...
    return 0;
  }
}
