package org.cord.node.operation;

import org.cord.mirror.Db;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.AbstractRecordSourceResolverOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;

/**
 * RecordOperation on Node that defines a Query which returns all the elder siblings of the invoking
 * Node. An elder sibling is defined as (parentNode_id = $node.parentNode_id and number &gt;
 * $node.number). The Nodes will be listed by ascending numbers followed by title alphabetically.
 * <p>
 * This query will handle not include any siblings that have the same number as the invoking node,
 * despite the fact that this may be viewed as being an appropriate sibling depending on the value
 * of the title.
 */
public class ElderNodeSiblings
  extends AbstractRecordSourceResolverOperation
{
  public final static RecordOperationKey<RecordSource> NAME =
      RecordOperationKey.create("elderSiblings", RecordSource.class);

  private final Db __db;

  public ElderNodeSiblings(Db db)
  {
    super(NAME);
    __db = db;
  }

  @Override
  protected Table getRecordSourceTable()
  {
    return __db.getTable(Node.TABLENAME);
  }

  @Override
  protected RecordSource createRecordSource(TransientRecord node,
                                            Object cacheKey,
                                            Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(getCachedRecordSourceTable().getQuery(cacheKey,
                                                                          Node.PARENTNODE_ID + "="
                                                                                    + node.opt(Node.PARENTNODE_ID)
                                                                                    + " and "
                                                                                    + Node.NUMBER
                                                                                    + ">"
                                                                                    + node.opt(Node.NUMBER),
                                                                          Node.NUMBER + "," + Node.TITLE),
                                    viewpoint);
  }
}
