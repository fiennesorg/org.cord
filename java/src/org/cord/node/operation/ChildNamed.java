package org.cord.node.operation;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.util.Casters;

/**
 * ParametricRecordOperation that is responsible for resolving named children of a given record.
 * This performs caching of the results on the parent record.
 * 
 * @author alex
 */
public final class ChildNamed
  extends MonoRecordOperation<String, PersistentRecord>
{
  public static final MonoRecordOperationKey<String, PersistentRecord> NAME =
      MonoRecordOperationKey.create("childNamed", String.class, PersistentRecord.class);

  public static final int P0_FILENAME = 0;

  private final NodeManager __nodeManager;

  public ChildNamed(NodeManager nodeManager)
  {
    super(NAME,
          Casters.TOSTRING);
    __nodeManager = nodeManager;
  }

  protected NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  public Integer invalidateCache(TransientRecord parentNode,
                                 String filename)
  {
    return (Integer) parentNode.removeCachedValue(new CacheKey(filename));
  }

  public Integer cacheChild(TransientRecord childNode)
  {
    PersistentRecord parentNode = childNode.opt(Node.PARENTNODE);
    if (parentNode == null) {
      return null;
    }
    return (Integer) parentNode.setCachedValue(new CacheKey(childNode.comp(Node.FILENAME)),
                                               Integer.valueOf(childNode.getId()));
  }

  public PersistentRecord resolveChild(TransientRecord parentNode,
                                       String filename,
                                       Viewpoint viewpoint)
  {
    if (filename == null || filename.length() == 0) {
      return null;
    }
    final Node _Node = getNodeManager().getNode();
    final CacheKey cacheKey = new CacheKey(filename);
    Integer childId = (Integer) parentNode.getCachedValue(cacheKey);
    if (childId == null) {
      childId = _Node.optNodeIdByFilename(parentNode.getId(), filename);
      PersistentRecord childNode = _Node.getTable().getOptRecord(childId, viewpoint);
      if (childNode == null) {
        return null;
      }
      parentNode.setCachedValue(cacheKey, childId);
      return childNode;
    }
    PersistentRecord childNode = _Node.getTable().getOptRecord(childId, viewpoint);
    if (childNode == null) {
      parentNode.removeCachedValue(cacheKey);
      return null;
    }
    if (!filename.equals(childNode.opt(Node.FILENAME))) {
      parentNode.removeCachedValue(cacheKey);
      return resolveChild(parentNode, filename, viewpoint);
    }
    return childNode;
  }

  @Override
  public PersistentRecord calculate(TransientRecord parentNode,
                                    Viewpoint viewpoint,
                                    String filename)
  {
    return resolveChild(parentNode, filename, viewpoint);
  }

  public static final class CacheKey
  {
    private final String __filename;

    private CacheKey(String filename)
    {
      __filename = filename;
    }

    private CacheKey(TransientRecord node)
    {
      this(node.opt(Node.FILENAME));
    }

    @Override
    public boolean equals(Object obj)
    {
      if (!(obj instanceof CacheKey)) {
        return false;
      }
      CacheKey that = (CacheKey) obj;
      return this.__filename.equals(that.__filename);
    }

    @Override
    public int hashCode()
    {
      return __filename.hashCode();
    }

    @Override
    public String toString()
    {
      return "ChildNamed(" + __filename + ")";
    }
  }

}
