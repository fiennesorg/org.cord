package org.cord.node.operation;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.Node;
import org.cord.util.Casters;

public class NodeChildNumber
  extends MonoRecordOperation<Integer, PersistentRecord>
{
  public final static MonoRecordOperationKey<Integer, PersistentRecord> NAME =
      MonoRecordOperationKey.create("childNumber", Integer.class, PersistentRecord.class);

  public NodeChildNumber()
  {
    super(NAME,
          Casters.TOINT);
  }

  // FIXME: NodeChildNumber is actually wrong because it doesn't
  // take into account the group that the child node falls into.
  public static PersistentRecord getChildNumber(TransientRecord parentNode,
                                                int childNumber,
                                                Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    Table nodeTable = parentNode.getTable();
    Query query = nodeTable.getQuery(null,
                                     Node.PARENTNODE_ID + "=" + parentNode.getId() + " AND "
                                           + Node.NUMBER + "=" + childNumber,
                                     null);
    return RecordSources.getFirstRecord(query, viewpoint);
  }

  @Override
  public PersistentRecord calculate(TransientRecord parentNode,
                                    Viewpoint viewpoint,
                                    Integer childNumber)
  {
    try {
      return getChildNumber(parentNode, childNumber.intValue(), viewpoint);
    } catch (MirrorNoSuchRecordException badChildNumberEx) {
      return null;
    }
  }
}
