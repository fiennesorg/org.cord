package org.cord.node.operation;

import org.cord.mirror.Db;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.NodeDb;
import org.cord.util.Casters;
import org.cord.util.StringUtils;

/**
 * RecordOperation that converts the result of invoking its parameter into a value with all of the
 * whitespace replaced by HTML non-breaking space characters. Invoked as
 * <em>$record.nbsp.operationName</em>. This is automatically registered across the Db by
 * NodeDb.init.
 * 
 * @author alex
 * @see org.cord.util.StringUtils#whitespaceToNbsp(Object)
 * @see NodeDb#init(Db, String)
 */
public class Nbsp
  extends MonoRecordOperation<String, String>
{
  public static final MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("nbsp", String.class, String.class);

  public Nbsp()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperationName)
  {
    return StringUtils.whitespaceToNbsp(record.getString(recordOperationName));
  }
}
