package org.cord.node.operation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.util.Casters;
import org.cord.util.LogicException;

public class UrlEncode
  extends MonoRecordOperation<String, String>
{
  public final static MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("urlEncode", String.class, String.class);

  public final static int P0_KEY = 0;

  public UrlEncode()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperationName)
  {
    String unencodedValue = record.getString(recordOperationName);
    try {
      return URLEncoder.encode(unencodedValue, "UTF-8");
    } catch (UnsupportedEncodingException ueEx) {
      throw new LogicException("Cannot find UTF-8 encoding", ueEx);
    }
  }
}
