package org.cord.node.operation;

import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.Node;
import org.cord.node.NodeManager;

import com.google.common.base.Preconditions;

/**
 * Delegating implementation of RecordOperation that permits a different implementation of
 * RecordOperation to be associated with a Node for each NodeStyle in the system.
 */
public class UserStyleSpecificNodeRecordOperation<V>
  extends UserRecordOperation<V>
{
  private final UserRecordOperation<V> __defaultOperation;

  private final Map<Integer, UserRecordOperation<V>> __operations =
      new HashMap<Integer, UserRecordOperation<V>>();

  public UserStyleSpecificNodeRecordOperation(MonoRecordOperationKey<Object, V> name,
                                              NodeManager nodeManager,
                                              int defaultUserId,
                                              UserRecordOperation<V> defaultRecordOperation)
  {
    super(name,
          nodeManager,
          defaultUserId);
    __defaultOperation =
        Preconditions.checkNotNull(defaultRecordOperation, "defaultRecordOperation");
  }

  public UserRecordOperation<V> getDefaultUserRecordOperation()
  {
    return __defaultOperation;
  }

  public void addUserRecordOperation(Integer nodeStyleId,
                                     UserRecordOperation<V> recordOperation)
  {
    if (nodeStyleId != null && recordOperation != null) {
      __operations.put(nodeStyleId, recordOperation);
    }
  }

  @Override
  public V resolveParametricRequest(TransientRecord node,
                                    TransientRecord user,
                                    Viewpoint viewpoint)
  {
    UserRecordOperation<V> operation = __operations.get(node.opt(Node.NODESTYLE_ID));
    operation = operation == null ? __defaultOperation : operation;
    return operation.resolveParametricRequest(node, user, viewpoint);
  }

  private void lockNestedOperation(UserRecordOperation<V> operation)
      throws MirrorTableLockedException
  {
    operation.initialise(getTable());
    operation.preLock();
    operation.lock();
  }

  @Override
  protected void internalPreLock()
      throws MirrorTableLockedException
  {
    lockNestedOperation(__defaultOperation);
    for (UserRecordOperation<V> operation : __operations.values()) {
      lockNestedOperation(operation);
    }
  }

}