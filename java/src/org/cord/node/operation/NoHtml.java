package org.cord.node.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.util.Casters;
import org.cord.util.StringUtils;

import com.google.common.base.Optional;

public class NoHtml
  extends MonoRecordOperation<String, String>
{
  private static NoHtml INSTANCE = new NoHtml();

  public static NoHtml getInstance()
  {
    return INSTANCE;
  }

  public static final MonoRecordOperationKey<String, String> NOHTML()
  {
    return getInstance().getKey();
  }

  private NoHtml()
  {
    super(MonoRecordOperationKey.create("noHtml", String.class, String.class),
          Casters.TOSTRING);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String a)
  {
    Object value = record.get(a);
    if (value == null) {
      return "";
    }
    if (value instanceof Optional<?>) {
      Optional<?> optional = (Optional<?>) value;
      if (optional.isPresent()) {
        return noHtml(optional.get().toString());
      } else {
        return "";
      }
    }
    return noHtml(value.toString());
  }

  private String noHtml(String text)
  {
    return StringUtils.noHtml(text);
  }

}
