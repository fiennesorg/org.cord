package org.cord.node.operation;

import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.NodeManager;
import org.cord.node.UserAcl;
import org.cord.util.Casters;

/**
 * RecordOperation that acts on Acl records and resolves whether or not a particular User will pass
 * the invoking Acl.
 */
public class AclAcceptanceOperation
  extends MonoRecordOperation<Object, Boolean>
{
  /**
   * Name ("accepts") that the record is registered under on the UserAcl Table.
   * 
   * @see UserAcl#TABLENAME
   */
  public final static MonoRecordOperationKey<Object, Boolean> KEY =
      MonoRecordOperationKey.create("accepts", Object.class, Boolean.class);

  public final static int P0_USER = 0;

  private NodeManager _nodeManager;

  public AclAcceptanceOperation(NodeManager nodeManager)
  {
    super(KEY,
          Casters.NULL);
    _nodeManager = nodeManager;
  }

  protected NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  @Override
  public Boolean calculate(TransientRecord acl,
                           Viewpoint viewpoint,
                           Object userObject)
  {
    return Boolean.valueOf(getNodeManager().getAcl()
                                           .acceptsAclUser(acl.getId(),
                                                           getNodeManager().getUser()
                                                                           .compUserId(userObject)));
  }
}
