package org.cord.node.operation;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.Node;

import com.google.common.base.Preconditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * Delegating implementation of RecordOperation that permits a different implementation of
 * RecordOperation to be associated with a Node for each NodeStyle in the system.
 */
public class StyleSpecificNodeRecordOperation<V>
  extends SimpleRecordOperation<V>
{
  private final RecordOperation<V> __defaultRecordOperation;

  private final Int2ObjectMap<RecordOperation<V>> __recordOperations =
      new Int2ObjectOpenHashMap<>();

  private final boolean __delegateToDefaultForNulls;

  private final V __resultIfNull;

  /**
   * Create an instance that has the ability to define how null return values should be handled.
   * 
   * @param name
   *          The name by which this RecordOperation is invoked.
   * @param defaultRecordOperation
   *          The compulsory RecordOperation to invoke if there is not a NodeStyle-specific
   *          operation, or if there is, it returns null and deleteToDefaultForNulls is true.
   * @param delegateToDefaultForNulls
   *          If true then if there is a NodeStyle-specific RecordOperation and it returns null when
   *          invoked then the defaultRecordOperation will be invoked following it.
   * @param resultIfNull
   *          If the final RecordOperation (either the NodeStyle-specific or the default depending
   *          on the NodeStyle and the value of delegateToDefaultForNulls) that is invoked returns
   *          null then this value will be returned instead. Obviously a value of null will not
   *          change the default implementation.
   */
  public StyleSpecificNodeRecordOperation(RecordOperationKey<V> name,
                                          RecordOperation<V> defaultRecordOperation,
                                          boolean delegateToDefaultForNulls,
                                          V resultIfNull)
  {
    super(name);
    __defaultRecordOperation =
        Preconditions.checkNotNull(defaultRecordOperation, "defaultRecordOperation");
    __delegateToDefaultForNulls = delegateToDefaultForNulls;
    __resultIfNull = resultIfNull;
  }

  /**
   * Create an instance that doesn't do any clever handling of nulls at all and which returns them
   * directly to the invoker.
   * 
   * @param name
   *          The name by which this RecordOperation is invoked.
   * @param defaultRecordOperation
   *          The compulsory RecordOperation to invoke if there is not a NodeStyle-specific
   *          operation registerd that is appropriate for the Node.
   */
  public StyleSpecificNodeRecordOperation(RecordOperationKey<V> name,
                                          RecordOperation<V> defaultRecordOperation)
  {
    this(name,
         defaultRecordOperation,
         false,
         null);
  }

  public void addRecordOperation(int nodeStyleId,
                                 RecordOperation<V> recordOperation)
  {
    if (recordOperation != null) {
      __recordOperations.put(nodeStyleId, recordOperation);
    }
  }

  @Override
  public V getOperation(final TransientRecord node,
                        final Viewpoint viewpoint)
  {
    V result = null;
    final RecordOperation<V> specificRecordOperation =
        __recordOperations.get(node.compInt(Node.NODESTYLE_ID));
    if (specificRecordOperation != null) {
      result = specificRecordOperation.getOperation(node, viewpoint);
      if (result == null && __delegateToDefaultForNulls) {
        result = __defaultRecordOperation.getOperation(node, viewpoint);
      }
    } else {
      result = __defaultRecordOperation.getOperation(node, viewpoint);
    }
    return result == null ? __resultIfNull : result;
  }
}