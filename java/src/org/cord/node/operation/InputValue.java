package org.cord.node.operation;

import org.cord.mirror.Db;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.NodeDb;
import org.cord.util.Casters;
import org.cord.util.StringUtils;

/**
 * RecordOperation that converts the result of invoking its parameter into a value that can be
 * safely used inside an HTML value context. Invoked as <em>$record.inputValue.operationName</em>.
 * This is automatically registered across the Db by NodeDb.init
 * 
 * @author alex
 * @see org.cord.util.StringUtils#toHtmlValue(Object)
 * @see NodeDb#init(Db, String)
 */
public class InputValue
  extends MonoRecordOperation<String, String>
{
  public final static MonoRecordOperationKey<String, String> NAME =
      MonoRecordOperationKey.create("inputValue", String.class, String.class);

  public InputValue()
  {
    super(NAME,
          Casters.TOSTRING);
  }

  @Override
  public String calculate(TransientRecord record,
                          Viewpoint viewpoint,
                          String recordOperationName)
  {
    return StringUtils.toHtmlValue(record.getString(recordOperationName));
  }
}
