package org.cord.node.operation;

import java.util.ArrayList;
import java.util.List;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.Node;
import org.cord.util.ObjectUtil;

/**
 * Caching record operation that is responsible for generating the compound path on a given Node and
 * storing it until a filename element is updated on a Node.
 */
public class BreadcrumbTrail
  extends SimpleRecordOperation<List<TransientRecord>>
{
  public final static RecordOperationKey<List<TransientRecord>> NAME =
      RecordOperationKey.create("breadcrumbTrail",
                                ObjectUtil.<List<TransientRecord>> castClass(List.class));

  public BreadcrumbTrail()
  {
    super(NAME);
  }

  /**
   * Calculate the path for the given Node by traversing the descendant tree back to the root Node.
   */
  public static List<TransientRecord> getNodes(TransientRecord node)
  {
    List<TransientRecord> result =
        new ArrayList<TransientRecord>(node.comp(Node.GENERATION).intValue());
    result.add(node);
    while (node.compInt(Node.PARENTNODE_ID) != 0) {
      node = node.comp(Node.PARENTNODE);
      result.add(0, node);
    }
    return result;
  }

  @Override
  public List<TransientRecord> getOperation(TransientRecord node,
                                            Viewpoint viewpoint)
  {
    return getNodes(node);
  }
}
