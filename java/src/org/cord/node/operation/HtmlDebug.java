package org.cord.node.operation;

import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.util.ExceptionUtil;
import org.cord.util.NamedComparator;
import org.cord.util.SortedIterator;

public class HtmlDebug
  extends SimpleRecordOperation<String>
{
  public static final RecordOperationKey<String> NAME =
      RecordOperationKey.create("htmlDebug", String.class);

  public HtmlDebug()
  {
    super(NAME);
  }

  @Override
  public String getOperation(TransientRecord targetRecord,
                             Viewpoint viewpoint)
  {
    StringBuilder result = new StringBuilder();
    result.append("<h1>").append(targetRecord).append("</h1>\n");
    result.append("<h2>RecordOperations</h2>\n");
    result.append("<dl>\n");
    try {
      Iterator<RecordOperation<?>> recordOperations =
          new SortedIterator<RecordOperation<?>>(getTable().getRecordOperations()
                                                           .values()
                                                           .iterator(),
                                                 NamedComparator.getInstance());
      while (recordOperations.hasNext()) {
        RecordOperation<?> recordOperation = recordOperations.next();
        if (recordOperation != this) {
          result.append("<dt>").append(recordOperation.getName()).append("</dt><dd>");
          try {
            result.append(targetRecord.get(recordOperation.getName()));
            result.append("<br />\n<em>")
                  .append(recordOperation.getClass().getName())
                  .append("</em>");
          } catch (RuntimeException rEx) {
            result.append(ExceptionUtil.printStackTrace(rEx));
          }
          result.append("</dd>\n");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      return e.toString();
    }
    result.append("</dl>\n");
    result.append("<h2>Cached values</h2>");
    Map<Object, Object> cachedValues = targetRecord.getCachedValues();
    if (cachedValues == null) {
      result.append("<p>None</p>");
    } else {
      result.append("<dl>\n");
      for (Map.Entry<Object, Object> entry : cachedValues.entrySet()) {
        result.append("<dt>").append(entry.getKey()).append("</dt>\n");
        result.append("<dd>").append(entry.getValue()).append("</dd>\n");
      }
      result.append("</dl>\n");
    }
    return result.toString();
  }
}
