package org.cord.node.operation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.NodeManager;
import org.cord.node.User;
import org.cord.sql.ConnectionPool;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.LogicException;

import com.google.common.base.Predicate;

/**
 * ParametricRecordOperation that acts on the User table and validates an unencrypted password and
 * ip address against the stored version. The format for the check string is "password@ip".
 */
public class UserPasswordValidator
  extends MonoRecordOperation<String, Boolean>
{
  /**
   * "validate"
   */

  public final static MonoRecordOperationKey<String, Boolean> NAME =
      MonoRecordOperationKey.create("validate", String.class, Boolean.class);

  public final static int P0_PASSWORDIP = 0;

  private NodeManager _nodeManager;

  public UserPasswordValidator(NodeManager nodeManager)
  {
    super(NAME,
          Casters.TOSTRING);
    _nodeManager = nodeManager;
  }

  @Override
  public void shutdown()
  {
    super.shutdown();
    _nodeManager = null;
  }

  @Override
  public Boolean calculate(TransientRecord user,
                           Viewpoint viewpoint,
                           String passwordIpString)
  {
    int atIndex = passwordIpString.indexOf('@');
    if (atIndex == -1) {
      return Boolean.FALSE;
    }
    String password = (atIndex == 0 ? "" : passwordIpString.substring(0, atIndex));
    String ip = passwordIpString.substring(atIndex + 1);
    return accepts(user, password, ip, _nodeManager) ? Boolean.TRUE : Boolean.FALSE;
  }

  /**
   * Validate that the given User accepts both the password and ip address of an external
   * authentication request.
   * 
   * @param user
   *          The user to validate against
   * @param plainTextPassword
   *          The password being checked. null values are taken as ""
   * @param ipAddress
   *          The IP address of the source request in dotted decimal notation.
   * @param nodeManager
   *          The NodeManager responsible for the system.
   * @return true if <i>both</i> the IP address and the password where accepted.
   */
  public final static boolean accepts(TransientRecord user,
                                      String plainTextPassword,
                                      String ipAddress,
                                      NodeManager nodeManager)
  {
    return acceptsIp(user, ipAddress) && acceptsPassword(user, plainTextPassword, nodeManager);
  }

  /**
   * Validate that the given User accepts the source IP address. This currently does not perform the
   * validation and will always return true. In the future, I'll implement it, but it is not a
   * priority at present...
   * 
   * @param user
   *          The user who is being validated against.
   * @param ipAddress
   *          The IP address of the source request in dotted decimal notation.
   * @return Always true :-(
   */
  // 2DO ### Implement IP filtering (take from InetAddressFilter)
  public final static boolean acceptsIp(TransientRecord user,
                                        String ipAddress)
  {
    return true;
  }

  /**
   * Validate that the given User accepts the plain text password.
   * 
   * @param user
   *          The user who is being validated against.
   * @param plainTextPassword
   *          The password being checked. null values are taken as ""
   * @param nodeManager
   *          The NodeManager controlling the system.
   * @return true if the encrypted version of plainTextPassword is the same as the USER_PASSWORD
   *         field on the user.
   * @see User#PASSWORD
   */
  public final static boolean acceptsPassword(TransientRecord user,
                                              String plainTextPassword,
                                              NodeManager nodeManager)
  {
    return user.opt(User.PASSWORD).equals(encrypt(plainTextPassword, nodeManager));
  }

  public final static Predicate<String> isValidPassword(NodeManager nodeMgr,
                                                        PersistentRecord user)
  {
    return new Predicate<String>() {
      @Override
      public boolean apply(String password)
      {
        return UserPasswordValidator.acceptsPassword(user, password, nodeMgr);
      }
    };
  }

  /**
   * Convert a plain text password into an encrypted version of the same. This uses the mysql
   * encryption routines.
   * 
   * @param plainText
   *          The String that is to be encrypted. null values are taken to be empty Strings.
   * @param connections
   *          The ConnectionPool holding the connections to the db (any db, doesn't matter).
   * @return The encrypted version of the same.
   */
  public final static String encrypt(String plainText,
                                     String passwordHashFunction,
                                     ConnectionPool connections)
  {
    StringBuilder sql = new StringBuilder();
    sql.append("select ").append(passwordHashFunction).append("(");
    SqlUtil.toSafeSqlString(sql, plainText);
    sql.append(')');
    Connection connection = connections.checkOutConnection("UserPasswordValidator");
    Statement statement = null;
    try {
      statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sql.toString());
      resultSet.next();
      return resultSet.getString(1);
    } catch (SQLException sqlEx) {
      throw new LogicException("SQL Failure: " + sql, sqlEx);
    } finally {
      if (statement != null) {
        try {
          statement.close();
        } catch (SQLException sqlEx) {
          connections.checkInBroken(connection, "UserPasswordValidator");
          connection = null;
        }
      }
      if (connection != null) {
        connections.checkInConnection(connection, "UserPasswordValidator");
      }
    }
  }

  /**
   * Convert a plain text password into an encrypted version of the same.
   * 
   * @param plainText
   *          The String that is to be encrypted. null values are taken as empty Strings.
   * @param nodeManager
   *          The NodeManager! (used to get the ConnectionPool)
   * @return The encrypted version.
   */
  public final static String encrypt(String plainText,
                                     NodeManager nodeManager)
  {
    return encrypt(plainText,
                   nodeManager.getPasswordHashFunction(),
                   nodeManager.getDb().getConnectionPool());
  }

}
