package org.cord.node.operation;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.User;
import org.cord.util.Casters;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

/**
 * ParametricRecordOperation that tries to make the parametricVariable into a User. The following
 * invocation methods are supported:-
 * <ul>
 * <li><strong>$[record].[name]</strong> Invokes the resolution with the resolved
 * defaultUserId.</li>
 * <li><strong>$[record].[name].get(null)</strong> Invokes the resolution with the resolved
 * defaultUserId.</li>
 * <li><strong>$[record].[name].get([TransientRecord]</strong> If [TransientRecord] is from the User
 * table then use this as the user otherwise throw an IllegalArgumentException</li>
 * <li><strong>$[record].[name].get([NodeRequest])</strong> Utilise the User found in
 * NodeRequest.getUser()</li>
 * <li><strong>$[record].[name].get([Object])</strong> Utilise the Table.getRecordFromObject method
 * to resolve the User. If this fails, then it is assumed that it is a parameterless operation that
 * wants to be invoked on the default operation and it will expand into:
 * <em>$[record].[name].get(null).get([Object])</em> which mimics the behaviour of a non-parametric
 * operation. Currently this will only expand items that initially resolve to the Gettable
 * interface, but this will be re-examined in the future to support the full WebMacro style of
 * accessors.</li>
 * </ul>
 */
public abstract class UserRecordOperation<V>
  extends MonoRecordOperation<Object, V>
{
  public static final int P0_USER = 0;

  private final NodeManager __nodeManager;

  private final int __defaultUserId;

  public UserRecordOperation(MonoRecordOperationKey<Object, V> name,
                             NodeManager nodeManager,
                             int defaultUserId)
  {
    super(name,
          Casters.NULL);
    // super(name,
    // true,
    // defaultUserId);
    __nodeManager = nodeManager;
    __defaultUserId = defaultUserId;
  }

  public NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  @SuppressWarnings("unchecked")
  @Override
  public V calculate(TransientRecord invokingRecord,
                     Viewpoint viewpoint,
                     Object userObj)
  {
    TransientRecord user = null;
    int userId = 0;
    try {
      if (userObj != null) {
        if (userObj instanceof TransientRecord) {
          user = (TransientRecord) userObj;
          user.assertIsTableNamed(User.TABLENAME);
        } else if (userObj instanceof NodeRequest) {
          user = ((NodeRequest) userObj).getUser();
        } else {
          userId = NumberUtil.toInt(userObj, 0);
          if (userId != 0) {
            try {
              user = getNodeManager().getUser().getTable().getRecord(userId);
            } catch (MirrorNoSuchRecordException mnsrEx) {
            }
          }
          if (user == null) {
            Object result = resolveParametricRequest(invokingRecord, user, viewpoint);
            if (result instanceof Gettable) {
              return (V) ((Gettable) result).get(userObj.toString());
            } else {
              throw new IllegalArgumentException(invokingRecord + "." + getName() + "(" + userObj
                                                 + ") fails to produce a Gettable target");
            }
          }
        }
      }
      userId = userId == 0 ? __defaultUserId : userId;
      user = user == null ? getNodeManager().getUser().getTable().getRecord(userId) : user;
      return resolveParametricRequest(invokingRecord, user, viewpoint);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      IllegalArgumentException iaEx =
          new IllegalArgumentException(invokingRecord + "." + getName() + "(" + userObj + ") --> "
                                       + mnsrEx);
      iaEx.initCause(mnsrEx);
      throw iaEx;
    }
  }

  public abstract V resolveParametricRequest(TransientRecord invokingRecord,
                                             TransientRecord user,
                                             Viewpoint viewpoint);
}
