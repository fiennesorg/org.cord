package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * Implementation of NodeFilter that only passes Nodes of a given NodeStyle.
 * 
 * @author alex
 */
public class NodeFilterByNodeStyle
  extends NodeFilter
{
  private final int __nodeStyleId;

  public NodeFilterByNodeStyle(int nodeStyleId)
  {
    __nodeStyleId = nodeStyleId;
  }

  @Override
  protected boolean acceptsNode(PersistentRecord node)
  {
    return node.compInt(Node.NODESTYLE_ID) == __nodeStyleId;
  }
}
