package org.cord.node;

import java.util.List;

import org.cord.mirror.PersistentRecord;

import com.google.common.collect.ImmutableList;

/**
 * An implementation of Authenticator that contains a list of nested Authenticators which are asked
 * in sequence until one of them comes up with an answer. If none of the contained Authenticators
 * have an answer then the DelegatingAuthenticator will return null from the methods.
 * 
 * @author alex
 */
public class DelegatingAuthenticator
  implements Authenticator
{
  private final ImmutableList<Authenticator> __authenticators;

  public DelegatingAuthenticator(List<Authenticator> authenticators)
  {
    __authenticators = ImmutableList.copyOf(authenticators);
  }

  @Override
  public Boolean isAllowed(NodeRequest nodeRequest)
  {
    for (int i = 0; i < __authenticators.size(); i++) {
      Boolean result = __authenticators.get(i).isAllowed(nodeRequest);
      if (result != null) {
        return result;
      }
    }
    return null;
  }

  @Override
  public Boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    for (int i = 0; i < __authenticators.size(); i++) {
      Boolean result = __authenticators.get(i).isAllowed(nodeRequest, node);
      if (result != null) {
        return result;
      }
    }
    return null;
  }

  @Override
  public Boolean isAllowed(NodeManager nodeMgr,
                           PersistentRecord node,
                           String view,
                           int userId)
  {
    for (int i = 0; i < __authenticators.size(); i++) {
      Boolean result = __authenticators.get(i).isAllowed(nodeMgr, node, view, userId);
      if (result != null) {
        return result;
      }
    }
    return null;
  }

}
