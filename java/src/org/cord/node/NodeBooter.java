package org.cord.node;

public abstract class NodeBooter
{
  protected String _pwd;

  private NodeManager _nodeManager;

  protected NodeBooter(NodeManager nodeManager,
                       String transactionPassword)
  {
    _pwd = transactionPassword;
    _nodeManager = nodeManager;
  }

  protected final String getPwd()
  {
    return _pwd;
  }

  protected final NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  public void shutdown()
  {
    _nodeManager = null;
    _pwd = null;
  }
}
