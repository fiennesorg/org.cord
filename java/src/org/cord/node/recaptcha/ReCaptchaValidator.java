package org.cord.node.recaptcha;

import org.cord.mirror.PersistentRecord;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.NodeRequestSegmentFactory;
import org.cord.node.ReloadSuccessOperation;
import org.cord.util.Gettable;
import org.webmacro.Context;

/**
 * Wraps an existing {@link NodeRequestSegmentFactory}, and takes its name. Invoking the new view
 * thus created (through {@link #getInstance}) will call the recaptcha validation code on the
 * parameters submitted in the nodeRequest. If validation fails, then setErrorMessage() is called on
 * the nodeRequest, and a ReloadException is thrown. If validation succeeds, the wrapped view is
 * invoked via {@link NodeRequestSegmentFactory#doWork}.
 */
public class ReCaptchaValidator
  extends NodeRequestSegmentFactory
{
  private final NodeRequestSegmentFactory __nextView;

  public ReCaptchaValidator(NodeManager nodeManager,
                            NodeRequestSegmentFactory nextView)
  {
    super(nextView.getName(),
          nodeManager,
          ReloadSuccessOperation.DEFAULT_RELOAD);
    __nextView = nextView;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           boolean isSummaryRole)
      throws NodeRequestException
  {
    Gettable params = getNodeManager().getGettable();

    if (!ReCaptcha.validate(nodeRequest,
                            params.getString("recaptcha.publickey"),
                            params.getString("recaptcha.privatekey"))) {
      nodeRequest.setErrorMessage("Sorry, the text you entered didn't "
                                  + "match the two words in the reCaptcha "
                                  + "box: please try again.");
      return null;
    }

    return __nextView.doWork(nodeRequest, node, context, isSummaryRole);
  }
}
