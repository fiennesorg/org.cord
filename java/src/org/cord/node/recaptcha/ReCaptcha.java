package org.cord.node.recaptcha;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.cord.node.NodeRequest;
import org.cord.util.IoUtil;
import org.cord.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSON;

import com.google.common.base.Strings;

import net.tanesha.recaptcha.ReCaptchaFactory;
import net.tanesha.recaptcha.ReCaptchaResponse;

public class ReCaptcha
{
  /**
   * @return whether the specified nodeRequest has a valid recaptcha response in it. TODO: Return an
   *         enum that differentiates between failed authentication and network failures.
   */
  public static boolean validate(NodeRequest nodeRequest,
                                 String publickey,
                                 String privatekey)
  {
    net.tanesha.recaptcha.ReCaptcha captcha =
        ReCaptchaFactory.newReCaptcha(publickey, privatekey, true);
    ReCaptchaResponse response =
        captcha.checkAnswer(nodeRequest.getRequest().getRemoteAddr(),
                            Strings.nullToEmpty(nodeRequest.getString("recaptcha_challenge_field")),
                            Strings.nullToEmpty(nodeRequest.getString("recaptcha_response_field")));
    return response.isValid();
  }

  public static JSONObject siteverify(String secret,
                                      String response,
                                      String remoteip)
      throws MalformedURLException, IOException, JSONException
  {
    StringBuilder buf = new StringBuilder();
    buf.append("https://www.google.com/recaptcha/api/siteverify?secret=")
       .append(StringUtils.urlEncodeUtf8(secret))
       .append("&response=")
       .append(StringUtils.urlEncodeUtf8(response))
       .append("&remoteip=")
       .append(StringUtils.urlEncodeUtf8(remoteip));
    return WritableJSON.get().toJSONObject(IoUtil.getContent(new URL(buf.toString()), null));
  }

  public static boolean isValid(JSONObject siteverify)
  {
    return siteverify.optBoolean("success");
  }

  public static boolean isValid(String secret,
                                String response,
                                String remoteip)
  {
    try {
      return isValid(siteverify(secret, response, remoteip));
    } catch (Exception e) {
    }
    return false;
  }

  public static String getValidityError(String secret,
                                        String response,
                                        String remoteip)
  {
    try {
      JSONObject siteverify = siteverify(secret, response, remoteip);
      if (!isValid(siteverify)) {
        return siteverify.get("error-codes").toString();
      }
    } catch (Exception e) {
      return e.toString();
    }
    return null;
  }
}
