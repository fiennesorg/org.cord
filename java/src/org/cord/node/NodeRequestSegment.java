package org.cord.node;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.util.Assertions;
import org.cord.util.LogicException;
import org.cord.util.StringUtils;

/**
 * NodeRequestSegment corresponds to the information supplied by a particular node during the
 * resolution of a single request. As a result it is responsible for supplying the following items
 * of information:-
 * <UL>
 * <LI>Information about the Node that it corresponds to.
 * <LI>Information about the role of the node (see NodeRole)
 * <LI>Links to resolved RegionRequestSegments for each of the registered regions on the Node.
 * </UL>
 * Please note that there will be a RegionRequestSegment for every registered region, regardless of
 * the NodeRole, but the decision as to whether or not to populate the region rests with the content
 * compiler.
 */
public class NodeRequestSegment
{
  private final NodeManager __nodeManager;

  private final NodeRequest __nodeRequest;

  private final int __nodeId;

  private PersistentRecord _nodeStyle;

  private final Map<?, ?> __context;

  private final String __templatePath;

  private boolean _isInitEditable = false;

  private boolean _isEditable = false;

  private boolean _isInitDeletable = false;

  private boolean _isDeletable = false;

  private boolean _isInitPublishable = false;

  private boolean _isPublishable = false;

  private boolean _isRenderReset = false;

  private boolean _isRootTemplatePath = false;

  /**
   * @param nodeManager
   *          The NodeManager overseeing this application.
   * @param nodeRequest
   *          The NodeRequest that is responsible for generating this request.
   * @param node
   *          The PersistentRecord from the Node Table for the Node that this NodeRequestSegment
   *          represents. This will automatically be transformed into the Writable Node <i>if</i>
   *          the current NodeRequest has the Transaction lock on the Node.
   * @param context
   *          The webmacro context that will be passed to the Template during rendering. This should
   *          be populated with the appropriate data required by the Template before creating the
   *          NodeRequestSegment by the NodeRequestSegmentFactory.
   * @param templatePath
   *          The path to the template that should be embedded into the response that is capable of
   *          rendering the NodeRequestSegment. This must be defined and not empty otherwise an
   *          IllegalArgumentException will be thrown.
   */
  public NodeRequestSegment(NodeManager nodeManager,
                            NodeRequest nodeRequest,
                            PersistentRecord node,
                            Map<?, ?> context,
                            String templatePath)
  {
    __nodeManager = nodeManager;
    __nodeRequest = nodeRequest;
    __nodeId = node.getId();
    __context = context;
    __templatePath = Assertions.notEmpty(templatePath, "templatePath");
  }

  public boolean getIsRootTemplatePath()
  {
    return _isRootTemplatePath;
  }

  public NodeRequestSegment setIsRootTemplatePath(boolean flag)
  {
    _isRootTemplatePath = flag;
    return this;
  }

  /**
   * Check to see whether or not this NodeRequestSegment wants to override the value given by the
   * NodeStyle as to whether or not to render this Node in it's own window. Please note that this
   * will only be invoked if the NodeStyle does not opt to perform a renderReset. It is therefore
   * not possible to "re-nest" a segment that is reset by default.
   * 
   * @return false by default unless set otherwise.
   * @see #setIsRenderReset(boolean)
   */
  public boolean getIsRenderReset()
  {
    return _isRenderReset;
  }

  /**
   * @see #getIsRenderReset()
   */
  public NodeRequestSegment setIsRenderReset(boolean flag)
  {
    _isRenderReset = flag;
    return this;
  }

  protected final PersistentRecord getNodeStyle()
  {
    if (_nodeStyle == null) {
      _nodeStyle = getNode().comp(Node.NODESTYLE);
    }
    return _nodeStyle;
  }

  /**
   * Get an existing Transaction as associated with the current Session if one exists. This is done
   * by looking the information up in the SessionTransaction.
   */
  protected Transaction getTransaction()
  {
    return __nodeRequest.getSessionTransaction().getTransactionByNode(getNode().getId());
  }

  protected Transaction touchTransaction()
      throws TransactionExpiredException
  {
    Transaction transaction = getTransaction();
    if (transaction != null) {
      try {
        transaction.resetCreationTime();
      } catch (MirrorTransactionTimeoutException mttEx) {
        throw new TransactionExpiredException("Your transaction has expired", getNode(), mttEx);
      }
    }
    return transaction;
  }

  /**
   * Get the Map that is used to represent the webmacro context for this NodeRequestSegment. This is
   * provided as a Map rather than an explicit WebContext Object in order to permit decoupling from
   * the webmacro interface at a later date if required.
   * 
   * @return The Map that was passed to the creator of this NodeRequestSegment. Objects placed into
   *         the Map will be made available to templates.
   */
  protected final Map<?, ?> getContext()
  {
    return __context;
  }

  /**
   * Get a list of all the Nodes that are dependent on the Node that is locked by this
   * NodeRequestSegment (if any).
   * 
   * @return Iterator of PersistentRecord objects from the Node Table.
   * @see PersistentRecord
   */
  public final Collection<PersistentRecord> getEffectedNodes()
  {
    Transaction nodeTransaction = getTransaction();
    if (nodeTransaction == null) {
      return Collections.emptySet();
    } else {
      return nodeTransaction.getRecords(__nodeManager.getNode().getTable());
    }
  }

  /**
   * Check to see if there are any Nodes that depend on the Node represented by this
   * NodeRequestSegment.
   * 
   * @return true if this NodeRequestSegment has the lock on the Node and the Node has dependent
   *         Nodes.
   * @see Transaction#getRecordCount(Table)
   */
  public final boolean getHasDependentNodes()
  {
    Transaction nodeTransaction = getTransaction();
    return (nodeTransaction == null
        ? false
        : nodeTransaction.getRecordCount(__nodeManager.getNode().getTable()) > 1);
  }

  public final int getLockType()
  {
    Transaction nodeTransaction = getTransaction();
    return (nodeTransaction == null ? Transaction.TRANSACTION_NULL : nodeTransaction.getType());
  }

  public final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  public final NodeRequest getNodeRequest()
  {
    return __nodeRequest;
  }

  /**
   * Get the webmacro template path for this segment. The default implementation of this merely
   * returns the value passed to the creator, although other implementations may supply more
   * parametric implementations.
   */
  public final String getTemplatePath()
  {
    return __templatePath;
  }

  /**
   * Check to see if the User associated with the NodeRequest that this NodeRequestSegment is a
   * member of has permission to edit the Node that this NodeRequestSegment encapsulates. This is a
   * caching method and will only calculate the value on the first invocation of the method.
   * 
   * @return true if the Node is editable.
   */
  public final boolean getIsEditable()
  {
    if (!_isInitEditable) {
      _isEditable = __nodeRequest.isEditable(getNode());
      _isInitEditable = true;
    }
    return _isEditable;
  }

  /**
   * Overide the algorithmically derived version of getIsEditable with a customised value. This
   * should not be invoked except in very specific circumstances, but it is "safe" because even if a
   * template thinks that the NodeRequestSegment is editable, the request to edit the Node
   * associated with the NodeRequestSegment will be rejected (unless the User has legitimate rights
   * to edit the Node in which case it doesn't change anything). This method may be invoked
   * repeatedly.
   * 
   * @param isEditable
   *          The state that will be returned from getIsEditable() in precedence to any previously
   *          declared state.
   */
  public final void setIsEditable(boolean isEditable)
  {
    _isEditable = isEditable;
    _isInitEditable = true;
  }

  public final boolean getIsPublishable()
  {
    if (!_isInitPublishable) {
      _isPublishable = getNodeManager().getAcl().acceptsNodeRequest(getNode().comp(Node.PUBLISHACL),
                                                                    __nodeRequest,
                                                                    getNode());
      _isInitPublishable = true;
    }
    return _isPublishable;
  }

  /**
   * Check to see if the User associated with the NodeRequest that this NodeRequestSegment is a
   * member of has permission to delete the Node that this NodeRequestSegment encapsulates. This is
   * a caching method and will only calculate the value on the first invocation of the method.
   * 
   * @return true if the Node is deletable.
   */
  public boolean getIsDeletable()
  {
    if (!_isInitDeletable) {
      _isDeletable = __nodeRequest.isDeletable(getNode());
      _isInitDeletable = true;
    }
    return _isDeletable;
  }

  /**
   * Get the Node that this NodeRequestSegment is wrapping.
   * 
   * @return Either a PersistentRecord or a WritableRecord depending on whether or not the current
   *         user has a lock on the current node.
   */
  public PersistentRecord getNode()
  {
    try {
      return __nodeRequest.getSessionTransaction().getNode(__nodeId);
    } catch (MirrorNoSuchRecordException deletedNodeEx) {
      throw new LogicException("Deleted Node", deletedNodeEx);
    }
  }

  public WritableRecord getWritableNode()
  {
    try {
      return (WritableRecord) getNode();
    } catch (ClassCastException notLockedEx) {
      throw new LogicException("Current user doesn't have lock on " + getNode(), notLockedEx);
    }
  }

  @Override
  public String toString()
  {
    return "NodeRequestSegment(" + getNode() + "," + getTemplatePath() + ")";
  }

  public Object get(Object regionName)
  {
    try {
      return getNodeManager().getNode().compile(getNode(),
                                                StringUtils.toString(regionName),
                                                getNodeRequest(),
                                                getTransaction());
    } catch (MirrorNoSuchRecordException badRegionNameEx) {
      throw new LogicException("Bad Region Name:" + regionName, badRegionNameEx);
    }
  }
}
