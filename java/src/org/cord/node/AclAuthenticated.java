package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.util.OptionalInt;

/**
 * Interface for Objects that have an ACL associated with their actions.
 * 
 * @author alex
 */
public interface AclAuthenticated
{
  /**
   * Get the ACL that this AclAuthenticated is currently authenticating against.
   * 
   * @return The appropriate ACL or null if there is no authentication
   */
  public PersistentRecord getAcl()
      throws MirrorNoSuchRecordException;

  /**
   * Get the id of the ACL that this AclAuthenticated would try to resolve to perform
   * authentication. There is no guarantee that this id is a valid id.
   * 
   * @return The ACL id or null if there is no authentication
   */
  public int getAclId();

  /**
   * Put in a request to update the ACL that this AclAuthenticated is validating against
   * 
   * @param userId
   *          The user who is requesting the change
   * @param ownerId
   *          The owner of the node (if any) in whose context the request is being performed
   * @param aclId
   *          The proposed new ACL
   * @return true if the userId is allowed to make the change, false if the ACL could not be
   *         accepted.
   */
  public boolean setAclId(int userId,
                          OptionalInt ownerId,
                          OptionalInt aclId)
      throws MirrorNoSuchRecordException;

  /**
   * Does the current ACL accept the given user?
   * 
   * @param userId
   *          The user to validate
   * @param ownerId
   *          The owner of the node (if any) whose context the request is being placed.
   * @return true if the current ACL accepts userId or if there is no authentication. false if the
   *         userId is rejected.
   */
  public boolean accepts(int userId,
                         OptionalInt ownerId)
      throws MirrorNoSuchRecordException;
}
