package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.util.ExceptionUtil;
import org.webmacro.Context;

/**
 * PostViewAction that renders a fixed templatePath with the render reset and root template status
 * set by the containing {@link NodeRequestSegmentFactory}.
 * 
 * @author alex
 */
public class TemplateSuccessOperation
  implements PostViewAction
{
  private NodeManager _nodeManager;

  private final String __templatePath;

  public TemplateSuccessOperation(NodeManager nodeManager,
                                  String templatePath)
  {
    _nodeManager = nodeManager;
    __templatePath = templatePath;
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  /**
   * The webmacro key for the nestedException passed to
   * {@link #handleSuccess(NodeRequest, PersistentRecord, Context, NodeRequestSegmentFactory, Exception)}
   * - will not be utilised if not defined.
   */
  public static final String WM_NESTEDEXCEPTION = "nestedException";

  /**
   * The webmacro key for the {@link ExceptionUtil#printStackTrace(Throwable)} representation of the
   * nestedException passed to
   * {@link #handleSuccess(NodeRequest, PersistentRecord, Context, NodeRequestSegmentFactory, Exception)}
   * - will not be utilised if not defined.
   */
  public static final String WM_NESTEDEXCEPTIONTRACE = "nestedExceptionTrace";

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws NodeRequestException
  {
    if (nestedException != null) {
      context.put(WM_NESTEDEXCEPTION, nestedException);
      context.put(WM_NESTEDEXCEPTIONTRACE, ExceptionUtil.printStackTrace(nestedException));
    }
    NodeRequestSegment nrs =
        new NodeRequestSegment(_nodeManager, nodeRequest, node, context, __templatePath);
    nrs.setIsRenderReset(factory.getIsRenderReset());
    nrs.setIsRootTemplatePath(factory.getIsRootTemplatePath());
    return nrs;
  }
}
