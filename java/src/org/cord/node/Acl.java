package org.cord.node;

import java.util.Iterator;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.LoadingCache_AlwaysTableListener;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.DuoRecordOperation;
import org.cord.mirror.operation.DuoRecordOperationKey;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.AbstractRecordSource;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.operation.AclAcceptanceOperation;
import org.cord.util.Casters;
import org.cord.util.LogicException;
import org.cord.util.ObjectUtil;
import org.cord.util.OptionalInt;
import org.cord.util.SkippingIterator;

import com.google.common.collect.ImmutableList;

import it.unimi.dsi.fastutil.ints.IntArraySet;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

public class Acl
  extends NodeTableWrapper
{
  /**
   * "Acl"
   */
  public final static String TABLENAME = "Acl";

  /**
   * @see NodeDbConstants#NAME
   */
  public final static ObjectFieldKey<String> NAME = NodeDbConstants.NAME.copy();

  /**
   * "isInclusiveAcl"
   */
  public final static ObjectFieldKey<Boolean> ISINCLUSIVEACL =
      BooleanField.createKey("isInclusiveAcl");

  /**
   * @see Boolean#TRUE
   */
  public final static Boolean ISINCLUSIVEACL_DEFAULT = Boolean.TRUE;

  /**
   * "includeOwner"
   */
  public final static ObjectFieldKey<Boolean> INCLUDEOWNER = BooleanField.createKey("includeOwner");

  /**
   * @see Boolean#FALSE
   */
  public final static Boolean INCLUDEOWNER_DEFAULT = Boolean.FALSE;

  /**
   * "editAcl_id"
   */
  public final static IntFieldKey EDITACL_ID = IntFieldKey.create("editAcl_id");

  /**
   * "editAcl"
   */
  public final static RecordOperationKey<PersistentRecord> EDITACL =
      RecordOperationKey.create("editAcl", PersistentRecord.class);

  /**
   * "editAcls"
   */
  public final static RecordOperationKey<RecordSource> EDITACLS =
      RecordOperationKey.create("editAcls", RecordSource.class);

  /**
   * "userAcls"
   */
  public final static RecordOperationKey<RecordSource> USERACLS =
      RecordOperationKey.create("userAcls", RecordSource.class);

  /**
   * "viewNodes"
   */
  public final static RecordOperationKey<RecordSource> VIEWNODES =
      RecordOperationKey.create("viewNodes", RecordSource.class);

  /**
   * "editNodes"
   */
  public final static RecordOperationKey<RecordSource> EDITNODES =
      RecordOperationKey.create("editNodes", RecordSource.class);

  /**
   * "publishNodes"
   */
  public final static RecordOperationKey<RecordSource> PUBLISHNODES =
      RecordOperationKey.create("publishNodes", RecordSource.class);

  /**
   * "publishNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> PUBLISHNODESTYLES =
      RecordOperationKey.create("publishNodeStyles", RecordSource.class);

  /**
   * "updatePublishNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> UPDATEPUBLISHNODESTYLES =
      RecordOperationKey.create("updatePublishNodeStyles", RecordSource.class);

  /**
   * "deleteNodes"
   */
  public final static RecordOperationKey<RecordSource> DELETENODES =
      RecordOperationKey.create("deleteNodes", RecordSource.class);

  /**
   * "createNodeGroups"
   */
  public final static RecordOperationKey<RecordSource> CREATENODEGROUPS =
      RecordOperationKey.create("createNodeGroups", RecordSource.class);

  /**
   * "viewNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> VIEWNODESTYLES =
      RecordOperationKey.create("viewNodeStyles", RecordSource.class);

  /**
   * "updateViewNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> UPDATEVIEWNODESTYLES =
      RecordOperationKey.create("updateViewNodeStyles", RecordSource.class);

  /**
   * "editNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> EDITNODESTYLES =
      RecordOperationKey.create("editNodeStyles", RecordSource.class);

  /**
   * "updateEditNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> UPDATEEDITNODESTYLES =
      RecordOperationKey.create("updateEditNodeStyles", RecordSource.class);

  /**
   * "deleteNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> DELETENODESTYLES =
      RecordOperationKey.create("deleteNodeStyles", RecordSource.class);

  /**
   * "updateDeleteNodeStyles"
   */
  public final static RecordOperationKey<RecordSource> UPDATEDELETENODESTYLES =
      RecordOperationKey.create("updateDeleteNodeStyles", RecordSource.class);

  /**
   * "createNodeGroupStyles"
   */
  public final static RecordOperationKey<RecordSource> CREATENODEGROUPSTYLES =
      RecordOperationKey.create("createNodeGroupStyles", RecordSource.class);

  /**
   * "updateCreateNodeGroupStyles"
   */
  public final static RecordOperationKey<RecordSource> UPDATECREATENODEGROUPSTYLES =
      RecordOperationKey.create("updateCreateNodeGroupStyles", RecordSource.class);

  public final static MonoRecordOperationKey<Object, Boolean> ACCEPTS = AclAcceptanceOperation.KEY;

  /**
   * 1
   */
  public final static int ROOTONLY = 1;

  public static final int ACL_ROOTONLY_ID = ROOTONLY;

  public final static int PUBLIC = 2;

  public static final int ACL_PUBLIC_ID = PUBLIC;

  public final static int ANYLOGGEDIN = 3;

  public final static int COREADMINONLY = 4;

  public final static int ACL_ANYLOGGEDIN_ID = ANYLOGGEDIN;

  public final static RecordOperationKey<RecordSource> EDITREGIONSTYLES =
      RecordOperationKey.create("editRegionStyles", RecordSource.class);

  public final static RecordOperationKey<RecordSource> UPDATEEDITREGIONSTYLES =
      RecordOperationKey.create("updateEditRegionStyles", RecordSource.class);

  public final static RecordOperationKey<RecordSource> NESTEDACLLINKS = Acl_Acl.ACL_NESTEDACLLINKS;
  public final static RecordOperationKey<RecordSource> CONTAININGACLLINKS =
      Acl_Acl.ACL_CONTAININGACLLINKS;

  /**
   * RecordOperation that returns a RecordSource of all the User records that are either directly
   * contained in this Acl or which are contained in an Acl that is nested inside this Acl. Please
   * note that this does not take into account the {@link #ISINCLUSIVEACL} or {@link #INCLUDEOWNER}
   * flags - it is purely concerned with listing the related Users.
   */
  public final static RecordOperationKey<RecordSource> ALLCONTAINEDUSERS =
      RecordOperationKey.create("allContainedUsers", RecordSource.class);

  public static final DuoRecordOperationKey<Object, Object, Boolean> ACCEPTSUSEROWNER =
      DuoRecordOperationKey.create("acceptsUserOwner", Object.class, Object.class, Boolean.class);

  private final LoadingCache_AlwaysTableListener<AclRequest, Boolean> __acceptsCache;

  protected Acl(final NodeManager nodeMgr)
  {
    super(nodeMgr,
          TABLENAME);
    __acceptsCache = new LoadingCache_AlwaysTableListener<AclRequest, Boolean>(nodeMgr.getDb()) {
      @Override
      protected Boolean calculate(AclRequest key)
      {
        return Boolean.valueOf(calculateAccepts(key.__acl_id, key.__user_id, key.__owner_id));
      }

      @Override
      protected ImmutableList<Table> getTableListenerTargets()
      {
        return ImmutableList.of(nodeMgr.getAcl().getTable(),
                                nodeMgr.getAcl_Acl().getTable(),
                                nodeMgr.getUser().getTable(),
                                nodeMgr.getUserAcl().getTable());
      }
    };
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new StringField(NAME, "Name", 32, ""));
    table.addField(new BooleanField(ISINCLUSIVEACL, "Is inclusive ACL?", ISINCLUSIVEACL_DEFAULT));
    table.addField(new BooleanField(INCLUDEOWNER, "Includes owner in ACL?", INCLUDEOWNER_DEFAULT));
    table.addField(new LinkField(EDITACL_ID,
                                 "Rights to edit this ACL",
                                 null,
                                 EDITACL,
                                 TABLENAME,
                                 EDITACLS,
                                 NAME,
                                 pwd,
                                 Dependencies.LINK_ENFORCED).setIgnoreId(ROOTONLY));
    table.addRecordOperation(new AclAcceptanceOperation(getNodeManager()));
    table.setTitleOperationName(NAME);
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(ALLCONTAINEDUSERS) {
      @Override
      public RecordSource getOperation(final TransientRecord acl,
                                       Viewpoint viewpoint)
      {
        return new AbstractRecordSource(getNodeManager().getUser(), null) {
          @Override
          public boolean shouldRebuild(Viewpoint viewpoint)
          {
            return RecordSources.shouldRebuild(viewpoint,
                                               getNodeManager().getUserAcl().getTable(),
                                               getNodeManager().getAcl_Acl().getTable());
          }

          @Override
          public long getLastChangeTime()
          {
            return RecordSources.getLastChangeTime(getNodeManager().getUserAcl(),
                                                   getNodeManager().getAcl_Acl());
          }

          @Override
          protected IdList buildIdList(Viewpoint viewpoint)
          {
            IntSet userIds = new IntOpenHashSet();
            IntSet aclIds = new IntOpenHashSet();
            addAllUsers(acl, viewpoint, userIds, aclIds);
            return new ArrayIdList(getNodeManager().getUser(), userIds);
          }
        };
      }
    });
    table.addRecordOperation(new DuoRecordOperation<Object, Object, Boolean>(ACCEPTSUSEROWNER,
                                                                             Casters.NULL,
                                                                             Casters.NULL) {
      @Override
      public Boolean calculate(TransientRecord acl,
                               Viewpoint viewpoint,
                               Object userObj,
                               Object ownerObj)
      {
        return Boolean.valueOf(acceptsAclUserOptionalOwner(acl.getId(),
                                                           getNodeManager().getUser()
                                                                           .compUserId(userObj),
                                                           getNodeManager().getUser()
                                                                           .optOwnerId(ownerObj)));
      }
    });
  }

  private void addAllUsers(TransientRecord acl,
                           Viewpoint viewpoint,
                           IntSet userIds,
                           IntSet aclIds)
  {
    if (aclIds.contains(acl.getId())) {
      return;
    }
    for (PersistentRecord userAcl : acl.comp(USERACLS, viewpoint)) {
      userIds.add(userAcl.compInt(UserAcl.USER_ID));
    }
    aclIds.add(acl.getId());
    for (PersistentRecord acl_acl : acl.comp(NESTEDACLLINKS, viewpoint)) {
      addAllUsers(acl_acl.comp(Acl_Acl.NESTEDACL), viewpoint, userIds, aclIds);
    }
  }

  private boolean acceptsDirectly(PersistentRecord acl,
                                  int userId,
                                  int ownerId)
  {
    boolean match = acl.is(INCLUDEOWNER) & userId == ownerId;
    match = match || getNodeManager().getUserAcl().containsLink(acl.getId(), userId);
    return acl.is(ISINCLUSIVEACL) ? match : !match;
  }

  private boolean acceptsNested(PersistentRecord acl,
                                int userId,
                                int ownerId,
                                IntSet aclIds)
  {
    if (aclIds.contains(acl.getId())) {
      return false;
    }
    aclIds.add(acl.getId());
    if (acceptsDirectly(acl, userId, ownerId)) {
      return true;
    }
    for (PersistentRecord acl_acl : acl.comp(Acl_Acl.ACL_NESTEDACLLINKS)) {
      if (acceptsNested(acl_acl.comp(Acl_Acl.NESTEDACL), userId, ownerId, aclIds)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param aclId
   *          The Acl id to authenticate against. If it is not possible to resolve this, then it
   *          will return false.
   */
  public boolean acceptsAclUserOwner(int aclId,
                                     int userId,
                                     int ownerId)
  {
    try {
      return __acceptsCache.get(new AclRequest(aclId, userId, ownerId)).booleanValue();
    } catch (Exception e) {
      throw new LogicException("Unexpected error resolving accepts(" + aclId + "," + userId + ","
                               + ownerId + ")",
                               e);
    }
  }

  public boolean acceptsAclUserOwner(PersistentRecord acl,
                                     PersistentRecord user,
                                     PersistentRecord owner)
  {
    return acceptsAclUserOwner(acl.getId(), user.getId(), owner.getId());
  }

  public boolean acceptsAclUser(int aclId,
                                int userId)
  {
    return acceptsAclUserOwner(aclId, userId, OWNER_UNDEFINED);
  }

  public boolean acceptsAclUser(PersistentRecord acl,
                                PersistentRecord user)
  {
    return acceptsAclUser(acl.getId(), user.getId());
  }

  public static final int OWNER_UNDEFINED = 0;

  public boolean acceptsAclUserOptionalOwner(int aclId,
                                             int userId,
                                             Integer ownerId)
  {
    return ownerId == null
        ? acceptsAclUser(aclId, userId)
        : acceptsAclUserOwner(aclId, userId, ownerId.intValue());
  }

  public boolean acceptsAclUserOptionalOwner(PersistentRecord acl,
                                             PersistentRecord user,
                                             PersistentRecord owner)
  {
    return owner == null
        ? acceptsAclUser(acl.getId(), user.getId())
        : acceptsAclUserOwner(acl.getId(), user.getId(), owner.getId());
  }

  /**
   * @param acl
   *          compulsory
   * @param nodeRequest
   *          compulsory NodeRequest that is used to resolve {@link NodeRequest#getUserId()}
   * @param optionalOwnerNode
   *          optional Node that is used to resolve the {@link Node#OWNER_ID} if present.
   */
  public boolean acceptsNodeRequest(PersistentRecord acl,
                                    NodeRequest nodeRequest,
                                    PersistentRecord optionalOwnerNode)
  {
    if (optionalOwnerNode == null) {
      return acceptsAclUser(acl.getId(), nodeRequest.getUserId());
    }
    return acceptsAclUserOwner(acl.getId(),
                               nodeRequest.getUserId(),
                               optionalOwnerNode.compInt(Node.OWNER_ID));
  }

  public boolean calculateAccepts(int aclId,
                                  int userId,
                                  int ownerId)
  {
    PersistentRecord acl = getTable().getOptRecord(aclId);
    if (acl == null) {
      return false;
    }
    if (acceptsDirectly(acl, userId, ownerId)) {
      return true;
    }
    IntSet aclIds = new IntArraySet();
    aclIds.add(acl.getId());
    for (PersistentRecord acl_acl : acl.comp(Acl_Acl.ACL_NESTEDACLLINKS)) {
      if (acceptsNested(acl_acl.comp(Acl_Acl.NESTEDACL), userId, ownerId, aclIds)) {
        return true;
      }
    }
    return false;
  }

  public PersistentRecord getAcl(String name)
      throws MirrorNoSuchRecordException
  {
    return TableStringMatcher.firstMatch(getTable(), NAME, name);
  }

  /**
   * Take an Iterator of Objects and filter out all Objects that fail to authenticated as
   * AclAuthenticated objects.
   * 
   * @return Iterator of AclAuthenticated objects that pass userId and ownerId
   */
  public static <T extends AclAuthenticated> Iterator<T> acceptsAclAuthenticated(Iterator<T> source,
                                                                                 final int userId,
                                                                                 final OptionalInt ownerId)
  {
    return new SkippingIterator<T>(source) {
      @Override
      protected boolean shouldSkip(T aclAuthenticated)
      {
        try {
          return !aclAuthenticated.accepts(userId, ownerId);
        } catch (MirrorNoSuchRecordException mnsrEx) {
          return true;
        }
      }
    };
  }

  private static final class AclRequest
  {
    public final int __acl_id;
    public final int __user_id;
    public final int __owner_id;
    private final int __hashCode;

    private AclRequest(int acl_id,
                       int user_id,
                       int owner_id)
    {
      __acl_id = acl_id;
      __user_id = user_id;
      __owner_id = owner_id;
      __hashCode = ObjectUtil.hashCode(__acl_id, __user_id, __owner_id);
    }

    @Override
    public boolean equals(Object o)
    {
      if (!(o instanceof AclRequest)) {
        return false;
      }
      AclRequest that = (AclRequest) o;
      return __acl_id == that.__acl_id & this.__user_id == that.__user_id
             & this.__owner_id == that.__owner_id;
    }
    @Override
    public int hashCode()
    {
      return __hashCode;
    }

    @Override
    public String toString()
    {
      return "AclRequest(" + __acl_id + "," + __user_id + "," + __owner_id + ")";
    }

  }

}
