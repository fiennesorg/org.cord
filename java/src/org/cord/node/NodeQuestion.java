package org.cord.node;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordIterator;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.node.view.AbstractNodeQuestion;
import org.cord.node.view.AnswerNodeQuestion;
import org.cord.node.view.RemoveAnswerNodeQuestion;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.NumberUtil;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;

/**
 * Abstract class that represents an open question that can be answered by one or more Nodes.
 * 
 * @author alex
 * @see NodeQuestionManager
 */
public abstract class NodeQuestion
  extends EnglishNamedImpl
{
  private final String __encodedName;

  private final String __title;

  private final PersistentRecord __answerNode;

  private final NodeFilter __eligibleNodeFilter;

  private final Transaction __transaction;

  private final IntSet __nodeIds = IntSets.synchronize(new IntOpenHashSet());

  private final IntSet __roNodeIds = IntSets.unmodifiable(__nodeIds);

  private final int __maxAnswers;

  public static NodeFilter NODEFILTER_ANYNODE = null;

  public static final int MAXANSWERS_UNLIMITED = -1;

  /**
   * @param name
   *          The internal name by which this NodeQuestion is going to be stored in the
   *          NodeQuestionManager. This should be unique across all the possible NodeQuestions in
   *          the system.
   * @param englishName
   *          The human readable version of this Question.
   * @param title
   *          The optional title for this question. If empty then englishName will be used as the
   *          title instead.
   * @param maxAnswers
   *          The maximum number of nodes that may be supplied as an answer to this NodeQuestion. If
   *          this is -1, then there is no limit to the number of nodes.
   * @param eligibleNodeFilter
   *          The filter that decides which target nodes will be accepted as answers to this
   *          question. If null then all target nodes are fair game.
   */
  public NodeQuestion(String name,
                      String englishName,
                      String title,
                      PersistentRecord answerNode,
                      int maxAnswers,
                      NodeFilter eligibleNodeFilter,
                      Transaction transaction)
  {
    super(name,
          englishName);
    __encodedName = HtmlUtilsTheme.urlEncode(name);
    __title = Strings.isNullOrEmpty(title) ? englishName : title;
    __answerNode = Preconditions.checkNotNull(answerNode, "answerNode");
    __maxAnswers = maxAnswers;
    __eligibleNodeFilter = eligibleNodeFilter;
    __transaction = transaction;
  }

  public final String getTitle()
  {
    return __title;
  }

  public final boolean hasTitle()
  {
    return !getTitle().equals(getEnglishName());
  }

  public boolean isClosed()
  {
    Transaction transaction = getTransaction();
    if (transaction != null) {
      return !transaction.isValid();
    }
    return false;
  }

  public int getMaxAnswers()
  {
    return __maxAnswers;
  }

  /**
   * Check to see whether the given node is an appropriate answer to this question. The default
   * implementation will check whether:
   * <ul>
   * <li>The given node is already in the list of answers?</li>
   * <li>Whether or not the question already has it's maximum quota of answers</li>
   * <li>if the NodeFilter (if any) for this question accepts the node</li>
   * </ul>
   * Subclasses can override this or extend this if the default implementation is not sufficient.
   */
  public boolean isEligible(PersistentRecord node)
  {
    IntSet nodeIds = getSelectedNodeIds();
    if (nodeIds.contains(node.getId())) {
      return false;
    }
    if (__maxAnswers == -1 || nodeIds.size() < __maxAnswers) {
      NodeFilter nodeFilter = getEligibleNodeFilter();
      return nodeFilter == null ? true : nodeFilter.accepts(node);
    }
    return false;
  }

  public boolean isRemovable(PersistentRecord node)
  {
    return getSelectedNodeIds().contains(node.getId());
  }

  /**
   * Get the RecordFilter that is going to decide whether each visited Node is a possible solution
   * to this NodeQuestion.
   * 
   * @return The given NodeFilter to generate a subset of the complete Node set or null if all Nodes
   *         are to be viewed as elligible solutions.
   */
  public NodeFilter getEligibleNodeFilter()
  {
    return __eligibleNodeFilter;
  }

  /**
   * Get the Transaction that this NodeQuestion is associated with if any. If this is defined then
   * the Transaction will be queried to see if it is still active when the NodeQuestion is processed
   * with respect to a given Node.
   */
  public Transaction getTransaction()
  {
    return __transaction;
  }

  /**
   * Get the Node that was responsible for asking this NodeQuestion in the first place.
   * 
   * @return The PersistentRecord that the answer will be submitted to.
   */
  public PersistentRecord getAnswerNode()
  {
    return __answerNode;
  }

  /**
   * @return Immutable Set of Node.id records that have been selected so far.
   */
  public IntSet getSelectedNodeIds()
  {
    return __roNodeIds;
  }

  /**
   * Get the first node id from the Set of currently selected node ids. If multiple node ids have
   * been selected then it isn't guaranteed as to which is the first element so this is primarily
   * intended for situations where there is only ever one answer.
   * 
   * @return The selected node id. Never null
   * @throws NoSuchElementException
   *           if there are no selected node ids
   */
  public int getSelectedNodeId()
      throws NoSuchElementException
  {
    return getSelectedNodeIds().iterator().nextInt();
  }

  public Integer optSelectedNodeId()
  {
    Set<Integer> nodeIds = getSelectedNodeIds();
    if (nodeIds.size() == 0) {
      return null;
    }
    return nodeIds.iterator().next();
  }

  public Iterator<PersistentRecord> getSelectedNodes()
  {
    return new RecordIterator(__answerNode.getTable(), getSelectedNodeIds().iterator());
  }

  public PersistentRecord getSelectedNode()
      throws MirrorNoSuchRecordException
  {
    return __answerNode.getTable().getRecord(getSelectedNodeId());
  }

  /**
   * Get the URL that should be submitted to take whatever action that is necessary to be invoked on
   * answering of this NodeQuestion.
   * 
   * @return The URL as generated by the subclass that communicates the answer to the question to
   *         whichever process asked the question.
   */
  public abstract String getAnswerUrl();

  /**
   * Add an eligible node to the list of answers that this NodeQuestion currently has.
   * 
   * @return true if the node was accepted as a valid answer.
   */
  public boolean addAnswer(PersistentRecord node)
  {
    if (!node.getTable().getName().equals(Node.TABLENAME)) {
      return false;
    }
    if (!isEligible(node)) {
      return false;
    }
    return __nodeIds.add(node.getId());
  }

  public boolean removeAnswer(PersistentRecord node)
  {
    return __nodeIds.remove(node.getId());
  }

  public int clearAnswers()
  {
    int count = __nodeIds.size();
    __nodeIds.clear();
    return count;
  }

  public int addAnswers(Gettable gettable,
                        String key)
  {
    int count = 0;
    Collection<?> answers = gettable.getValues(key);
    if (answers != null) {
      Table nodeTable = __answerNode.getTable();
      Iterator<?> i = answers.iterator();
      while (i.hasNext()) {
        int nodeId = NumberUtil.toInt(i.next(), -1);
        if (nodeId > 0) {
          try {
            if (addAnswer(nodeTable.getRecord(nodeId))) {
              count++;
            }
          } catch (MirrorNoSuchRecordException e) {
            // ignore this - just a badly formatted node_id...
          }
        }
      }
    }
    return count;
  }

  public String getAddUrl(PersistentRecord node)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(node.opt(Node.URL));
    buf.append("?view=" + AnswerNodeQuestion.NAME);
    buf.append("&" + AbstractNodeQuestion.CGI_QUESTIONNAME + "=").append(__encodedName);
    return buf.toString();
  }

  public String getSubmitUrl(PersistentRecord node)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(node.opt(Node.URL));
    buf.append("?view=" + AnswerNodeQuestion.NAME);
    buf.append("&" + AbstractNodeQuestion.CGI_QUESTIONNAME + "=").append(__encodedName);
    buf.append("&" + AnswerNodeQuestion.CGI_SUBMITONLY + "=true");
    return buf.toString();
  }

  public String getRemoveUrl(PersistentRecord node)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(node.opt(Node.URL));
    buf.append("?view=" + RemoveAnswerNodeQuestion.NAME);
    buf.append("&" + AbstractNodeQuestion.CGI_QUESTIONNAME + "=").append(__encodedName);
    return buf.toString();
  }

  /**
   * Is this NodeQuestion currently in a state where the answers that it contains are sufficient for
   * it to be submissable? If true then the GUI interface will include a link to submit this
   * question as complete.
   * 
   * @return True if the subclass implementation feels that the questions has been sufficiently
   *         answered. This will normally be some kind of decision based around the size of
   *         getSelectedNodeIds()
   * @see #getSelectedNodeIds()
   */
  public abstract boolean maySubmit();

  /**
   * Should this question be automatically submitted? This is invoked during AnswerNodeQuestion
   * invocation after the answer has been processed an if maySubmit() returns true, and it will
   * automatically invoke a RedirectionException to the getAnswerUrl().
   * 
   * @return True if this NodeQuestion should be automatically submitted in its current state.
   * @see AnswerNodeQuestion#getInstance(NodeRequest, PersistentRecord, org.webmacro.Context,
   *      boolean)
   * @see #maySubmit()
   * @see #getAnswerUrl()
   */
  public abstract boolean shouldSubmit();
}
