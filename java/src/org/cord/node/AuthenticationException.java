package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * Exception that is thrown during the construction of a NodeRequest to imply that one of the Nodes
 * has rejected the requesting User, implying the need for a login screen.
 */
public class AuthenticationException
  extends NodeRequestException
{
  /**
   * 
   */
  private static final long serialVersionUID = -2457155457564119371L;

  public AuthenticationException(String message,
                                 PersistentRecord triggerNode,
                                 Exception nestedException)
  {
    super(message,
          triggerNode,
          nestedException);
  }

  public AuthenticationException(String message,
                                 PersistentRecord triggerNode)
  {
    this(message,
         triggerNode,
         null);
  }
}
