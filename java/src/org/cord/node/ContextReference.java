package org.cord.node;

import org.webmacro.Context;

public class ContextReference
{
  private final Context __context;

  public ContextReference(Context context)
  {
    __context = context;
  }

  public final Context getContext()
  {
    return __context;
  }
}
