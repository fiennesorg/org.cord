package org.cord.node;

import java.io.File;
import java.io.IOException;

import org.cord.util.IoUtil;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Information about a single file that can be registered with an IncludeMgr and added to an
 * IncludeList. The IncludeFile is immutable and can be safely shared.
 * 
 * @author alex
 */
@Deprecated
public class IncludeFile
{
  private final IncludeMgr __includeMgr;
  private final String __key;
  private final String __webPath;
  private final File __file;
  private final ImmutableList<String> __dependencies;
  private final String __html;

  protected IncludeFile(IncludeMgr includeMgr,
                        String key,
                        String webPath,
                        String... dependencies)
  {
    __includeMgr = Preconditions.checkNotNull(includeMgr, "includeMgr");
    __key = Preconditions.checkNotNull(key, "key");
    __webPath = Preconditions.checkNotNull(webPath, "webPath");
    File file = new File(includeMgr.getWebRoot(), webPath);
    Preconditions.checkState(file.isFile() && file.canRead(), "Cannot read %s", file);
    __file = file;
    __dependencies = ImmutableList.copyOf(dependencies);
    __html = String.format("<script type=\"text/javascript\" src=\"/%s\"></script>", __webPath);
  }

  public String toHtml()
  {
    return __html;
  }

  @Override
  public String toString()
  {
    return "IncludeFile(" + __key + '=' + __webPath + ")";
  }

  public final ImmutableList<String> getDependencies()
  {
    return __dependencies;
  }

  public final IncludeMgr getIncludeMgr()
  {
    return __includeMgr;
  }

  public final String getKey()
  {
    return __key;
  }

  public final String getWebPath()
  {
    return __webPath;
  }

  public final File getFile()
  {
    return __file;
  }

  /**
   * @throws IOException
   *           if it is not possible to read the contents of the File. This should not normally be
   *           thrown as we are checking that the File is readable when we construct the IncludeFile
   *           so it probably means that a 3rd party is messing with the state of the file system.
   */
  public String getContents()
      throws IOException
  {
    return IoUtil.getContent(getFile());
  }
}
