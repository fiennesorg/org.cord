package org.cord.node;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.util.StringUtils;
import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.servlet.WMServlet;
import org.webmacro.servlet.WebContext;

/**
 * Exception that should be thrown by modules during the construction of a NodeRequest that
 * signifies that some state change has taken place which requires the user to be redirected to a
 * new URL now that it has completed.
 */
public class RedirectionException
  extends NodeRequestException
{

  private static final long serialVersionUID = 9182826838316787948L;
  private final String __targetUrl;
  private final PersistentRecord __targetNode;

  private RedirectionException(String targetUrl,
                               PersistentRecord targetNode,
                               PersistentRecord triggerNode,
                               Exception nestedException)
  {
    super(targetUrl,
          triggerNode,
          nestedException);
    __targetUrl = targetUrl;
    __targetNode = targetNode;
  }

  public RedirectionException(String targetUrl,
                              PersistentRecord triggerNode,
                              Exception nestedException)
  {
    this(targetUrl,
         null,
         triggerNode,
         nestedException);
  }

  /**
   * @deprecated in preference of
   *             {@link #RedirectionException(PersistentRecord, PersistentRecord, Exception)}
   */
  @Deprecated
  public RedirectionException(NodeManager nodeManager,
                              PersistentRecord triggerNode,
                              PersistentRecord targetNode,
                              Exception nestedException)
  {
    this(targetNode.opt(Node.URL),
         targetNode,
         triggerNode,
         nestedException);
  }

  public RedirectionException(PersistentRecord triggerNode,
                              PersistentRecord targetNode,
                              Exception nestedException)
  {
    this(targetNode.opt(Node.URL),
         targetNode,
         triggerNode,
         nestedException);
  }

  /**
   * Create a RedirectionException that just reloads the same Node - this is useful if you don't
   * want people to be able to reload the view that they invoked. However the page rendering will
   * not have any information about the view that was just invoked - if you want this then you
   * should work with a {@link ReloadException}
   */
  public RedirectionException(PersistentRecord triggerNode)
  {
    this(triggerNode,
         triggerNode,
         null);
  }

  /**
   * @deprecated in preference of
   *             {@link #RedirectionException(PersistentRecord, PersistentRecord, String, Exception)}
   */
  @Deprecated
  public RedirectionException(NodeManager nodeManager,
                              PersistentRecord triggerNode,
                              PersistentRecord targetNode,
                              String targetView,
                              Exception nestedException)
  {
    this(targetNode.opt(Node.URL) + "?view=" + targetView,
         targetNode,
         triggerNode,
         nestedException);
  }

  public RedirectionException(PersistentRecord triggerNode,
                              PersistentRecord targetNode,
                              String targetView,
                              Exception nestedException)
  {
    this(targetNode.opt(Node.URL) + "?view=" + targetView,
         targetNode,
         triggerNode,
         nestedException);
  }

  public RedirectionException(PersistentRecord triggerNode,
                              String targetView)
  {
    this(triggerNode,
         triggerNode,
         targetView,
         null);
  }

  public RedirectionException(PersistentRecord triggerNode,
                              PersistentRecord targetNode,
                              Map<String, Object> params,
                              Exception nestedException)
  {
    this(buildQuery(targetNode.opt(Node.URL), params),
         triggerNode,
         targetNode,
         nestedException);
  }

  public static String buildQuery(String baseUrl,
                                  Map<String, Object> params)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(baseUrl).append("?");

    for (Iterator<Map.Entry<String, Object>> i = params.entrySet().iterator(); i.hasNext();) {
      Map.Entry<String, Object> entry = i.next();
      buf.append(entry.getKey())
         .append('=')
         .append(StringUtils.urlEncodeUtf8(entry.getValue().toString()));
      if (i.hasNext()) {
        buf.append('&');
      }
    }
    return buf.toString();
  }

  /**
   * Get the optional target Node that will not have been defined if an explicit URL was passed to
   * the constructor
   */
  public final PersistentRecord getTargetNode()
  {
    return __targetNode;
  }

  public final String getTargetUrl()
  {
    return __targetUrl;
  }

  public final Template issueRedirect(WebContext webContext,
                                      WMServlet wmServlet)
      throws IOException, ResourceException
  {
    webContext.getResponse().sendRedirect(__targetUrl);
    return null;
  }
}
