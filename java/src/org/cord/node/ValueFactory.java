package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.util.Shutdownable;

/**
 * Interface that describes an Object that is capable of supplying a value on request based around
 * current state of a Node system. It is up to the implementation of the ValueFactory as to whether
 * it supplies constant values or if it utilises some factor in the NodeRequest or NodeManager to
 * dynamically supply values based around state. This therefore lets the plugin of more advanced
 * configurations to existing ValueFactory consumers without having to recode the legacy code or
 * clients.
 */
public interface ValueFactory
  extends Shutdownable
{
  /**
   * Get the value that this ValueFactory feels is appropriate for the supplied state.
   * 
   * @return The appropriate value. I believe this may be null, but this should be discouraged.
   *         However clients should probably check for null...
   * @param nodeManager
   *          The NodeManager that this ValueFactory is expected to resolve against. Having this as
   *          null should be taken as an error condition.
   * @param nodeRequest
   *          The NodeRequest that this ValueFactory is expected to resovle against. This may be
   *          null when there is not an incoming request to resolve against. Note: having
   *          nodeRequest.getNodeManager() != nodeManager is an error condition and therefore it
   *          should be assumed that this is the case. The inclusion of the nodeManager parameter is
   *          just provided to enable queries without nodeRequests to be resolved.
   */
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
      throws MirrorNoSuchRecordException;
}