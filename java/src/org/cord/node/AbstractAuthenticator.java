package org.cord.node;

import org.cord.mirror.PersistentRecord;

/**
 * Base implementation of Authenticator that forwards requests through the interface down to the
 * lowest level. Subclasses can either just implement
 * {@link #isAllowed(NodeManager, PersistentRecord, String, int)} or override the existing methods
 * as required.
 * 
 * @author alex
 */
public abstract class AbstractAuthenticator
  implements Authenticator
{
  /**
   * Resolve the target leaf node and pass the request across to
   * {@link #isAllowed(NodeRequest, PersistentRecord)}. Subclasses should override this if other
   * behaviour is required.
   */
  @Override
  public Boolean isAllowed(NodeRequest nodeRequest)
  {
    return isAllowed(nodeRequest, nodeRequest.getTargetLeafNode());
  }

  /**
   * Resolve the view and the user id and pass the request across to the subclass' implementation of
   * {@link #isAllowed(NodeManager, PersistentRecord, String, int)}. Subclasses should override this
   * if other behaviour is required.
   */
  @Override
  public Boolean isAllowed(NodeRequest nodeRequest,
                           PersistentRecord node)
  {
    return isAllowed(nodeRequest.getNodeManager(),
                     node,
                     nodeRequest.getView(),
                     nodeRequest.getUserId());
  }
}
