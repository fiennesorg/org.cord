package org.cord.node;

import com.google.common.base.MoreObjects;

/**
 * Implementation that looks the value up as a parameter in the NodeRequest, delegating to a default
 * value if the NodeRequest is either not defined or has no definition of the value.
 */
public class NodeRequestValueFactory
  implements ValueFactory
{
  private final String __paramName;

  private final String __defaultValue;

  public NodeRequestValueFactory(String paramName,
                                 String defaultValue)
  {
    __paramName = paramName;
    __defaultValue = defaultValue;
  }

  @Override
  public String getValue(NodeManager nodeManager,
                         NodeRequest nodeRequest)
  {
    return nodeRequest == null
        ? __defaultValue
        : MoreObjects.firstNonNull(nodeRequest.getString(__paramName), __defaultValue);
  }

  @Override
  public void shutdown()
  {
  }
}