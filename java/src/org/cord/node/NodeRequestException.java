package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.util.ExceptionUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Strings;

/**
 * The superclass of all Exceptions that may be legitimately thrown by NodeRequestSegmentFactory
 * objects while creating instances of NodeRequestSegment.
 * 
 * @see NodeRequestSegmentFactory
 * @see NodeRequestSegment
 */
public class NodeRequestException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = -955695251071077358L;

  private final PersistentRecord _triggerNode;

  public NodeRequestException(String message,
                              PersistentRecord triggerNode,
                              Exception nestedException)
  {
    super(Strings.nullToEmpty(message),
          nestedException);
    _triggerNode = triggerNode;
  }

  public PersistentRecord getTriggerNode()
  {
    return _triggerNode;
  }

  public String getStackTraceAsString()
  {
    return ExceptionUtil.printStackTrace(this);
  }

  /**
   * @return the stack dump of the cause of this exception, or "" if there was no cause defined.
   */
  public String getCauseStackTraceAsString()
  {
    Throwable cause = getCause();
    return cause == null ? "" : ExceptionUtil.printStackTrace(cause);
  }

  /**
   * Return a block of HTML that contains the stack trace of the cause of this exception formatted
   * such that it can be safely included in an HTML document.
   * 
   * @return the stack dump of the cause of this exception with the invalid XHTML character
   *         represented properly so that it can be included in an error report inside a browser. If
   *         there is no stack trace then an empty string is returned.
   * @see #getCauseStackTraceAsString()
   * @see StringUtils#noHtml(Object)
   */
  public String getCauseStackTraceAsHtml()
  {
    String stackTrace = getCauseStackTraceAsString();
    if (stackTrace == null) {
      return "";
    }
    return "<p class=\"stack\" style=\"white-space: pre-wrap;\">" + StringUtils.noHtml(stackTrace) + "</p>";
  }
}
