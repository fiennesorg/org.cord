package org.cord.node.requires;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.cord.node.IncludeList;
import org.cord.util.DebugConstants;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

public class Requirements
{
  private final RequirementMgr __requirementMgr;
  private final LinkedHashSet<Requirement> __requirements = Sets.newLinkedHashSet();
  private Set<Requirement> _roRequirements = null;

  protected Requirements(RequirementMgr requirementMgr)
  {
    __requirementMgr = requirementMgr;
  }

  public RequirementMgr getRequirementMgr()
  {
    return __requirementMgr;
  }

  public void needs(String key)
  {
    needs(Preconditions.checkNotNull(getRequirementMgr().getKey_Requirement().get(key),
                                     "Unable to resolve Requirement.need of \"%s\"",
                                     key));
  }

  /**
   * Compatibility method to ensure that existing webmacro code that uses the legacy
   * {@link IncludeList#addFile(String)} continues to function. Warning message is printed to log to
   * encourage refactoring.
   * 
   * @deprecated in preference of {@link #needs(String)}
   */
  @Deprecated
  public void addFile(String key)
  {
    DebugConstants.DEBUG_OUT.println("Requirements.addFile(" + key
                                     + ") needs refactoring to Requirements.needs(" + key + ")");
    needs(key);
  }

  public void needs(Requirement requirement)
  {
    if (__requirements.contains(requirement)) {
      return;
    }
    for (Requirement dependency : requirement.getDependencies()) {
      needs(dependency);
    }
    __requirements.add(requirement);
  }

  /**
   * @return unmodifiable Set of registered requirements and associated dependencies
   */
  public final Set<Requirement> getRequirements()
  {
    if (_roRequirements == null) {
      _roRequirements = Collections.unmodifiableSet(__requirements);
    }
    return _roRequirements;
  }

  public String toWebMacro()
  {
    return toWebMacro(new StringBuilder()).toString();
  }

  public StringBuilder toWebMacro(StringBuilder buf)
  {
    for (Requirement requirement : __requirements) {
      requirement.toWebmacro(buf).append("\n");
    }
    return buf;
  }
}
