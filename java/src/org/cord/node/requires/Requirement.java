package org.cord.node.requires;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;

public class Requirement
{
  private final RequirementMgr __requirementMgr;
  private final String __key;
  private final String __templatePath;
  private final ImmutableSet<Requirement> __dependencies;

  protected Requirement(RequirementMgr requirementMgr,
                        String key,
                        String templatePath,
                        ImmutableSet<Requirement> dependencies)
  {
    __requirementMgr = requirementMgr;
    __key = key;
    __templatePath = templatePath;
    __dependencies = dependencies;
  }

  public RequirementMgr getRequirementsMgr()
  {
    return __requirementMgr;
  }

  public String getKey()
  {
    return __key;
  }

  public String getTemplatePath()
  {
    return __templatePath;
  }

  public ImmutableSet<Requirement> getDependencies()
  {
    return __dependencies;
  }

  public StringBuilder toWebmacro(StringBuilder buf)
  {
    buf.append("#include as template \"").append(getTemplatePath()).append("\"");
    return buf;
  }

  @Override
  public String toString()
  {
    return MoreObjects.toStringHelper(this)
                      .add("key", __key)
                      .add("templatePath", __templatePath)
                      .add("dependencies", __dependencies)
                      .toString();
  }
}
