package org.cord.node.requires;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import org.cord.util.DebugConstants;
import org.cord.util.Maps;
import org.webmacro.ResourceException;
import org.webmacro.WebMacro;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;

public class RequirementMgr
{
  private final WebMacro __webmacro;

  private final ConcurrentMap<String, Requirement> __key_requirement = Maps.newConcurrentHashMap();
  private final Map<String, Requirement> __ro_key_requirements =
      Collections.unmodifiableMap(__key_requirement);

  public static final String KEY_JQUERY = "jquery";
  public static final String KEY_QUERY_UI = "jquery-ui";
  public static final String KEY_JQUERY_TIMEPICKER = "jquery-timepicker";

  public RequirementMgr(WebMacro webmacro)
  {
    __webmacro = webmacro;
  }

  public WebMacro getWebMacro()
  {
    return __webmacro;
  }

  /**
   * Add a Requirement to the hierarchy of Requirements maintained by this RequirementMgr.
   * 
   * @param key
   *          The unique key. Attempting to re-register a Requirement with the same key as an
   *          existing Requirement will fail with an IllegalArgumentException
   * @param templatePath
   *          The webmacro templatePath expressed in a format that can be resolved by the WebMacro
   *          passed to the initialiser. If the RequirementMgr was booted in standalone mode without
   *          a WebMacro then this is not resolved, otherwise the Template will be resolved and an
   *          IllegalArgumentException thrown if this is not possible.
   * @param dependencyKeys
   *          The optional list of keys that this Requirement needs to be satisfied before it can be
   *          utilised. They must be registered before this Requirement, and any resolution failure
   *          will result in an IllegalArgumentException.
   * @return the appropriately parsed Requirement that has been created and registered.
   */
  public synchronized Requirement register(String key,
                                           String templatePath,
                                           String... dependencyKeys)
  {
    if (__key_requirement.containsKey(key)) {
      throw new IllegalArgumentException(String.format("%s is already registered as %s",
                                                       key,
                                                       __key_requirement.get(key)));
    }
    try {
      WebMacro webmacro = getWebMacro();
      if (webmacro != null) {
        webmacro.getTemplate(templatePath);
      }
    } catch (ResourceException e) {
      throw new IllegalArgumentException(String.format("Unable to resolve templatePath of %s for %s",
                                                       templatePath,
                                                       key),
                                         e);
    }
    ImmutableSet.Builder<Requirement> dependencies = ImmutableSet.builder();
    for (String dependencyKey : dependencyKeys) {
      Requirement dependency = __key_requirement.get(dependencyKey);
      if (dependency == null) {
        throw new IllegalArgumentException(String.format("%s has an unresolved dependency of %s",
                                                         key,
                                                         dependencyKey));
      }
      dependencies.add(dependency);
    }
    Requirement requirement = new Requirement(this, key, templatePath, dependencies.build());
    __key_requirement.put(key, requirement);
    return requirement;
  }

  public Requirement register(String config)
  {
    try {
      Iterator<String> i = Splitter.on(" ").omitEmptyStrings().split(config).iterator();
      return register(i.next(), i.next(), Iterators.toArray(i, String.class));
    } catch (RuntimeException e) {
      throw new IllegalArgumentException(String.format("Unable to parse \"%s\"", config), e);
    }
  }

  public void registerAll(File configFile,
                          boolean debugFiles)
      throws IOException
  {
    if (debugFiles) {
      DebugConstants.method(this, "registerAll", configFile);
    }
    if (!configFile.isFile()) {
      if (debugFiles) {
        DebugConstants.DEBUG_OUT.println(" --> file not found");
      }
      return;
    }
    BufferedReader br = new BufferedReader(new FileReader(configFile));
    try {
      String line = null;
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (!(line.length() == 0 || line.startsWith("#"))) {
          Requirement requirement = register(line);
          if (debugFiles) {
            DebugConstants.DEBUG_OUT.println(" --> " + requirement);
          }
        }
      }
    } finally {
      br.close();
    }
  }

  public Map<String, Requirement> getKey_Requirement()
  {
    return __ro_key_requirements;
  }

  public Requirements newRequirements()
  {
    return new Requirements(this);
  }

}
