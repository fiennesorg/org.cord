package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;

/**
 * An interface that ContentCompilers should implement if they are prepared to let the node that
 * they are registered on be moved between locations in the node tree. No node will be permitted to
 * be moveable unless all of the ContentCompilers on it and its children all implement
 * MoveableContentCompiler.
 * 
 * @author alex
 */
public interface MoveableContentCompiler
{
  /**
   * Perform any work that is required to be done because the fromNode is being moved from being a
   * child of fromParentNode to being a child of toParentNode.
   * 
   * @param transaction
   *          The lock on the operation
   * @param fromNode
   *          The locked node that is being moved
   * @param regionStyle
   *          The RegionStyle that the ContentCompiler is associated with on fromNode
   * @param fromParentNode
   *          The old parentNode of fromNode
   * @param toParentNode
   *          The new parentNode of fromNode
   */
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException;

  /**
   * Perform any work that is required on fromChildNode because one of its parents is being moved
   * from being a child of fromParentNode to toParentNode. Note that the parentage of fromChildNode
   * will not be changing, but there may still be work that is required to be done if the
   * ContentCompiler maintains parentage related information of a depth of more than one generation.
   * 
   * @param transaction
   *          The lock on the operation
   * @param fromNode
   *          The locked node that is being moved
   * @param fromParentNode
   *          The old parentNode of fromNode
   * @param toParentNode
   *          The new parentNode of fromNode
   * @param fromChildNode
   *          The child node of fromNode that is being considered as part of the move operation.
   * @param regionStyle
   *          the RegionStyle that the ContentCompiler is associated with on fromChildNode
   */
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException;
}
