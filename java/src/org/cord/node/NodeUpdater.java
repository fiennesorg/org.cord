package org.cord.node;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

public interface NodeUpdater
{
  /**
   * @return true if the state of the database has changed as a result of calling this method.
   */
  public boolean updateNode(TransientRecord node,
                            Gettable in)
      throws MirrorFieldException;
}
