package org.cord.node;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

/**
 * Success operation that redirects to a constant URL via a RedirectionException
 * 
 * @see RedirectionException
 */
public class RedirectionSuccessOperation
  implements PostViewAction
{
  private final NodeManager __nodeManager;

  private final ValueFactory __targetUrl;

  public RedirectionSuccessOperation(NodeManager nodeManager,
                                     ValueFactory targetUrl)
  {
    __nodeManager = nodeManager;
    __targetUrl = targetUrl;
  }

  public RedirectionSuccessOperation(NodeManager nodeManager,
                                     final int targetNodeId)
  {
    this(nodeManager,
         new ValueFactory() {
           @Override
           public String getValue(NodeManager nodeManager,
                                  NodeRequest nodeRequest)
               throws MirrorNoSuchRecordException
           {
             return nodeManager.getNode().getTable().getRecord(targetNodeId).opt(Node.URL);
           }

           @Override
           public void shutdown()
           {
           }
         });
  }

  /**
   * Create a RedirectionSuccessOperation that issues a ReloadException on the same Node that it was
   * invoked on. This is useful for situations where you want to "clear" the previous action so that
   * it cannot be reloaded, but stay on the same page.
   * 
   * @param nodeMgr
   */
  public RedirectionSuccessOperation(NodeManager nodeMgr)
  {
    this(nodeMgr,
         new ValueFactory() {
           @Override
           public String getValue(NodeManager nodeManager,
                                  NodeRequest nodeRequest)
               throws MirrorNoSuchRecordException
           {
             return nodeRequest.getTargetLeafNode().opt(Node.URL);
           }

           @Override
           public void shutdown()
           {
           }
         });
  }

  public RedirectionSuccessOperation(NodeManager nodeMgr,
                                     String targetView)
  {
    this(nodeMgr,
         new ValueFactory() {
           @Override
           public String getValue(NodeManager nodeManager,
                                  NodeRequest nodeRequest)
               throws MirrorNoSuchRecordException
           {
             return nodeRequest.getTargetLeafNode().opt(Node.URL) + "?view=" + targetView;
           }

           @Override
           public void shutdown()
           {
           }
         });
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws RedirectionException, FeedbackErrorException
  {
    try {
      throw new RedirectionException(__targetUrl.getValue(__nodeManager, nodeRequest),
                                     node,
                                     nestedException);
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(null,
                                       node,
                                       e,
                                       true,
                                       true,
                                       "It has not been possible to resolve the target URL in this Redirection");
    }
  }

  @Override
  public void shutdown()
  {
  }
}