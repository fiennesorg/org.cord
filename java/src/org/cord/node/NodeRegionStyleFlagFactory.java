package org.cord.node;

import org.cord.mirror.PersistentRecord;

public interface NodeRegionStyleFlagFactory
{
  public Boolean getFlag(PersistentRecord node,
                         PersistentRecord regionStyle);
}
