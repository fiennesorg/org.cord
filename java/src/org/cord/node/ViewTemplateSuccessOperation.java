package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.webmacro.Context;

/**
 * PostViewAction that returns the default viewTemplatePath for the node that it is being rendered.
 * It is important to note that this is not the same as the ReloadSuccessOperation - while they will
 * both return the view template, the ReloadSuccessOperation will be rendered as a read view whereas
 * this will be the view that was being previously invoked.
 * 
 * @author alex
 */
public class ViewTemplateSuccessOperation
  implements PostViewAction
{
  public static ViewTemplateSuccessOperation getInstance()
  {
    return INSTANCE;
  }

  private static final ViewTemplateSuccessOperation INSTANCE = new ViewTemplateSuccessOperation();

  private ViewTemplateSuccessOperation()
  {
  }

  @Override
  public NodeRequestSegment handleSuccess(NodeRequest nodeRequest,
                                          PersistentRecord node,
                                          Context context,
                                          NodeRequestSegmentFactory factory,
                                          Exception nestedException)
      throws NodeRequestException
  {
    return new NodeRequestSegment(nodeRequest.getNodeManager(),
                                  nodeRequest,
                                  node,
                                  context,
                                  node.comp(Node.NODESTYLE).opt(NodeStyle.VIEWTEMPLATEPATH));
  }

  @Override
  public void shutdown()
  {
  }

}
