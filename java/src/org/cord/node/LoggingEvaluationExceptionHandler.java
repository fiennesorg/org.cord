package org.cord.node;

import org.webmacro.Broker;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.engine.EvaluationExceptionHandler;
import org.webmacro.engine.Variable;
import org.webmacro.util.Settings;

public class LoggingEvaluationExceptionHandler
  implements EvaluationExceptionHandler
{
  @Override
  public void init(Broker b,
                   Settings config)
  {
  }

  @Override
  public String expand(Variable variable,
                       Context context,
                       Exception problem)
      throws PropertyException
  {
    System.err.println("expand(" + variable + "," + context + "," + problem + ")");
    problem.printStackTrace();
    throw new PropertyException("LogginEvaluationExceptionHandler:" + problem, problem);
  }

  @Override
  public void evaluate(Variable variable,
                       Context context,
                       Exception problem)
      throws PropertyException
  {
    System.err.println("evaluate(" + variable + "," + context + "," + problem + ")");
    problem.printStackTrace();
    throw new PropertyException("LogginEvaluationExceptionHandler:" + problem, problem);
  }

  @Override
  public String warningString(String warningText,
                              Exception exception)
      throws PropertyException
  {
    throw new PropertyException(warningText, exception);
  }

  @Override
  public String errorString(String errorText,
                            Exception exception)
      throws PropertyException
  {
    throw new PropertyException(errorText, exception);
  }
}