package org.cord.node.cc;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.NodeManager;
import org.cord.node.search.AdvancedWordSearchable;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.StringUtils;

import com.google.common.collect.ImmutableList;

public class HtmlContentCompiler
  extends TableContentCompiler
  implements AdvancedWordSearchable
{
  /**
   * "Html"
   */
  public final static String NAME = ContentCompilerFactory.NAME_HTML;

  /**
   * "HtmlInstance"
   */
  public final static String HTMLINSTANCE = "HtmlInstance";

  /**
   * "Htmls"
   */
  public final static String REFERENCES = "Htmls";

  /**
   * "html"
   */
  public final static ObjectFieldKey<String> HTML = StringField.createKey("html");

  /**
   * "safeHtml"
   */
  public final static RecordOperationKey<String> SAFEHTML =
      RecordOperationKey.create("safeHtml", String.class);

  /**
   * "node/Html/html.view.wm.html"
   */
  public final static String VIEWTEMPLATEPATH = "node/Html/html.view.wm.html";

  /**
   * "node/Html/html.edit.wm.html"
   */
  public final static String EDITTEMPLATEPATH = "node/Html/html.edit.wm.html";

  public HtmlContentCompiler(NodeManager nodeManager,
                             String transactionPassword)
  {
    super(NAME,
          nodeManager,
          transactionPassword,
          new TableContentCompilerDb("org.cord.node.cc.HtmlDb",
                                     HTMLINSTANCE,
                                     null,
                                     1,
                                     new HtmlStyle(),
                                     REFERENCES,
                                     VIEWTEMPLATEPATH,
                                     EDITTEMPLATEPATH) {
            @Override
            protected void internalInit(Db db,
                                        String transactionPassword,
                                        Table instanceTable)
                throws MirrorTableLockedException, DuplicateNamedException
            {
              instanceTable.addField(new StringField(HTML, null, StringField.LENGTH_TEXT, ""));
              instanceTable.addRecordOperation(new SimpleRecordOperation<String>(SAFEHTML) {
                @Override
                public String getOperation(TransientRecord htmlInstance,
                                           Viewpoint viewpoint)
                {
                  return StringUtils.noHtml(htmlInstance.opt(HTML));
                }
              });
            }
          });
  }

  private final List<RecordOperationKey<?>> AWS_FIELDS =
      ImmutableList.<RecordOperationKey<?>> builder().add(HTML).build();

  @Override
  public Collection<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getInstance(node, regionStyle, null, viewpoint);
    return instance.is(ISDEFINED) ? instance : null;
  }

  @Override
  protected final boolean updateTransientInstance(PersistentRecord node,
                                                  PersistentRecord regionStyle,
                                                  TransientRecord htmlRecord,
                                                  PersistentRecord ntmlStyle,
                                                  Transaction transaction,
                                                  Gettable inputs,
                                                  Map<Object, Object> outputs,
                                                  PersistentRecord user)
      throws MirrorTransactionTimeoutException
  {
    String html = inputs.getString(HTML);
    if (html != null) {
      try {
        htmlRecord.setField(HTML, html);
      } catch (MirrorFieldException mfEx) {
        throw new LogicException("Unexpected value problem when setting Html.html to " + html,
                                 mfEx);
      }
    }
    return false;
  }
}
