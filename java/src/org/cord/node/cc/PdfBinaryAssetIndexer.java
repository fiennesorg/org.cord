package org.cord.node.cc;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.node.NodeManager;
import org.cord.node.search.AdvancedWordSearchable;
import org.cord.node.search.FileIndexer;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;
import org.cord.util.NamedImpl;

public class PdfBinaryAssetIndexer
  extends NamedImpl
  implements FileIndexer, AdvancedWordSearchable
{
  private NodeManager _nodeManager;

  private final String __pdfToTextPath;

  public PdfBinaryAssetIndexer(NodeManager nodeManager,
                               String pdfToTextPath)
  {
    super(BinaryAssetContentCompiler.BINARYASSET_FORMAT_PDF);
    _nodeManager = nodeManager;
    __pdfToTextPath = pdfToTextPath;
  }

  public String readPdf(File pdfFile)
      throws IOException
  {
    String tempTextPath = pdfFile.getAbsolutePath() + ".txt";
    String[] command = new String[] { __pdfToTextPath, pdfFile.getAbsolutePath(), tempTextPath };
    IoUtil.exec(command);
    File tempTextFile = new File(tempTextPath);
    String pdfContent = IoUtil.getContent(tempTextFile);
    tempTextFile.delete();
    return pdfContent;
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  @Override
  public void appendIndexableContent(TransientRecord region,
                                     Viewpoint viewpoint,
                                     StringBuilder buffer,
                                     PersistentRecord instance)
  {
    if (instance.is(BinaryAssetContentCompiler.ISUPLOADED)) {
      try {
        buffer.append(readPdf(BinaryAssetContentCompiler.getInstanceFile(_nodeManager, instance)));
      } catch (IOException ioEx) {
        System.err.println("PdfBinaryAssetIndexer:" + ioEx);
        // don't do anything because if we cant read the PDF then the
        // index will be empty...
      }
    }
  }

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    // TODO (#263) Implement the AdvancedWordSearchable requirements for
    // PdfBinaryAssetIndexed
    return null;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return null;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return null;
  }
}
