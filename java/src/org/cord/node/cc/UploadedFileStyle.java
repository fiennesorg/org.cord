package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Table;

public class UploadedFileStyle
  extends StyleTableWrapper
{
  public static final String TABLENAME = "UploadedFileStyle";

  public final static String FORMAT_ID = "format_id";

  public UploadedFileStyle()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styles)
      throws MirrorTableLockedException
  {
  }
}
