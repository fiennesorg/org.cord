package org.cord.node.cc;

import java.io.IOException;
import java.util.Map;

import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.AbstractRecordSourceResolverOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.ConstantValueFactory;
import org.cord.node.Node;
import org.cord.node.NodeFieldValueFactory;
import org.cord.node.NodeFilter;
import org.cord.node.NodeFilterByNodeStyles;
import org.cord.node.RegionStyle;
import org.cord.node.ValueFactory;
import org.cord.node.app.ControlledIsPublished;
import org.cord.util.HtmlUtils;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;

/**
 * Implementation of LinkField that provides for a link to an instance of an TableContentCompiler
 */
public class AccLinkField
  extends LinkField
{
  private String _questionName;

  private boolean _hasRegisteredLinkQuestion = false;

  private final Supplier<? extends TableContentCompiler> __sourceSupplier;
  private final Supplier<? extends TableContentCompiler> __targetSupplier;

  public AccLinkField(Supplier<? extends TableContentCompiler> sourceSupplier,
                      Supplier<? extends TableContentCompiler> targetSupplier,
                      IntFieldKey linkFieldName,
                      String label,
                      Integer defaultValue,
                      RecordOperationKey<PersistentRecord> linkName,
                      String targetTableName,
                      RecordOperationKey<RecordSource> referencesName,
                      String reverseOrderClause,
                      String transactionPassword,
                      int linkStyle)
  {
    super(linkFieldName,
          label,
          defaultValue,
          linkName,
          targetTableName,
          referencesName,
          reverseOrderClause,
          transactionPassword,
          linkStyle);
    __sourceSupplier = Preconditions.checkNotNull(sourceSupplier, "sourceSupplier");
    __targetSupplier = Preconditions.checkNotNull(targetSupplier, "targetSupplier");
  }

  public AccLinkField(Supplier<? extends TableContentCompiler> sourceTableContentCompiler,
                      Supplier<? extends TableContentCompiler> targetTableContentCompiler,
                      String targetTableName,
                      String label,
                      String pwd,
                      int linkStyle)
  {
    this(sourceTableContentCompiler,
         targetTableContentCompiler,
         null,
         label,
         null,
         null,
         targetTableName,
         null,
         null,
         pwd,
         linkStyle);
  }

  @Override
  protected void postInternalPreLock()
      throws MirrorTableLockedException
  {
    if (getTable().hasFieldNamed(ControlledIsPublished.ISPUBLISHED().getKeyName())) {
      final String filterHeader = "(" + getTable().getName() + ".isPublished=1) and ("
                                  + getTable().getName() + ".isDefined=1) and ("
                                  + getTable().getName() + "." + getName() + "=";
      getTargetTable().addRecordOperation(new AbstractRecordSourceResolverOperation(RecordOperationKey.create("published"
                                                                                                              + StringUtils.toUpperCaseFirstLetter(getReferencesName().toString()),
                                                                                                              RecordSource.class)) {
        @Override
        public RecordSource createRecordSource(TransientRecord targetRecord,
                                               Object cacheKey,
                                               Viewpoint viewpoint)
        {
          StringBuilder filter = new StringBuilder();
          filter.append(filterHeader).append(targetRecord.getId()).append(")");
          return RecordSources.viewedFrom(getCachedRecordSourceTable().getQuery(cacheKey,
                                                                                filter.toString(),
                                                                                getReverseOrderClause()),
                                          viewpoint);
        }

        @Override
        protected Table getRecordSourceTable()
        {
          return AccLinkField.this.getTable();
        }
      });
    }
  }

  @Override
  public void appendHtmlValue(TransientRecord instance,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Map<Object, Object> context)
      throws IOException
  {
    if (mayResolveRecord(instance, viewpoint)) {
      try {
        PersistentRecord target = resolveRecord(instance, viewpoint);
        HtmlUtils.value(buf,
                        target.opt(Node.HREF, target.getTable().getTitleOperationName()),
                        namePrefix,
                        getName());
        return;
      } catch (MirrorNoSuchRecordException e) {
      }
    }
    super.appendHtmlValue(instance, viewpoint, namePrefix, buf, context);
  }

  public String getQuestionName()
  {
    if (Strings.isNullOrEmpty(_questionName)) {
      _questionName = getSourceAcc().getInstanceTableName() + "." + getName();
    }
    return _questionName;
  }

  @Override
  protected String getHtmlWidgetSummary(TransientRecord instance,
                                        PersistentRecord target)
  {
    StringBuilder buf = new StringBuilder();
    if (mayResolveRecord(instance, null)) {
      try {
        buf.append(resolveRecord(instance, null).opt(Node.HREF, Node.TITLE.getKeyName()));
        buf.append("<br />");
      } catch (MirrorNoSuchRecordException e) {
      }
    }
    if (_hasRegisteredLinkQuestion) {
      buf.append("<a href=\"")
         .append(instance.comp(TableContentCompiler.NODE).opt(Node.URL))
         .append("?view=")
         .append(getQuestionName())
         .append("\">Browse</a><br />");
    }
    return buf.toString();
  }

  public ValueFactory getAskQuestionUrl()
  {
    return new ConstantValueFactory("/");
  }

  public ValueFactory getQuestionEnglishFactory()
  {
    return new NodeFieldValueFactory("What is the " + getHtmlLabel() + " for ", Node.TITLE, "?");
  }

  public NodeFilter getEligibleNodeFilter()
  {
    return NodeFilterByNodeStyles.getInstanceFromNodeStyles(getTargetAcc().getNodeStyles());
  }

  public String getTargetAccRegionStyleName()
  {
    return getTargetAcc().getDefaultRegionStyleName();
  }

  public String getSourceAccRegionStyleName()
  {
    return getSourceAcc().getDefaultRegionStyleName();
  }

  public TableContentCompiler getSourceAcc()
  {
    return __sourceSupplier.get();
  }

  public TableContentCompiler getTargetAcc()
  {
    return __targetSupplier.get();
  }

  public void registerLinkQuestion(PersistentRecord regionStyle)
      throws MirrorNoSuchRecordException
  {
    getSourceAcc().registerLinkQuestion(regionStyle.comp(RegionStyle.NODESTYLE),
                                        getQuestionName(),
                                        getAskQuestionUrl(),
                                        getQuestionEnglishFactory(),
                                        1,
                                        getEligibleNodeFilter(),
                                        getTargetAcc(),
                                        getTargetAccRegionStyleName(),
                                        getSourceAccRegionStyleName() + RegionStyle.NAMESPACE_SEPARATOR
                                                                       + getName(),
                                        getSourceAccRegionStyleName(),
                                        null,
                                        null);
    _hasRegisteredLinkQuestion = true;
  }
}
