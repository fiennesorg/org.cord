package org.cord.node.cc;

import org.cord.mirror.FieldKey;
import org.cord.mirror.FieldTrigger;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;

/**
 * Field trigger on ACC_ISDEFINED that checks to see if the style that this instance implements
 * supports optional values.
 */
public class IsDefinedTrigger
  implements FieldTrigger<Boolean>
{
  @Override
  public void shutdown()
  {
  }

  /**
   * @return ISDEFINED
   * @see TableContentCompiler#ISDEFINED
   */
  @Override
  public final FieldKey<Boolean> getFieldName()
  {
    return TableContentCompiler.ISDEFINED;
  }

  /**
   * @return true;
   */
  @Override
  public boolean handlesTransientRecords()
  {
    return true;
  }

  /**
   * If TableContentCompiler.ISDEFINED is set to false, then check to see if ACCSTYLE_ISOPTIONAL is
   * set and complain if this is not the case.
   * 
   * @throws MirrorFieldException
   *           If the instance is set to non-defined, but the style is set to compulsory.
   */
  @Override
  public Object triggerField(TransientRecord accInstance,
                             String fieldName)
      throws MirrorFieldException
  {
    if (!accInstance.is(TableContentCompiler.ISDEFINED)) {
      PersistentRecord accStyle = accInstance.comp(TableContentCompiler.STYLE);
      if (!accStyle.is(TableContentCompilerStyle.ISOPTIONAL)) {
        throw new MirrorFieldException("Instances of " + accStyle + " are compulsory",
                                       null,
                                       accInstance,
                                       TableContentCompiler.ISDEFINED,
                                       Boolean.FALSE,
                                       Boolean.TRUE);
      }
    }
    return null;
  }

  @Override
  public Object triggerField(WritableRecord accInstance,
                             String fieldName,
                             Transaction transaction)
      throws MirrorFieldException
  {
    return triggerField(accInstance, fieldName);
  }

  @Override
  public boolean triggerOnInstantiation()
  {
    return false;
  }
}
