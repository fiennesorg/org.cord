package org.cord.node.cc;

import org.cord.image.ImageStyleTable;
import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Table;
import org.cord.node.appcc.ScalableSingleImage;

/**
 * @deprecated in preference of {@link ScalableSingleImage}
 */
@Deprecated
public class SingleImageStyle
  extends StyleTableWrapper
{

  public SingleImageStyle()
  {
    super(ImageStyleTable.SINGLEIMAGESTYLE);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styles)
      throws MirrorTableLockedException
  {
    ImageStyleTable.initialiseStyleTable(pwd, styles);
  }

}
