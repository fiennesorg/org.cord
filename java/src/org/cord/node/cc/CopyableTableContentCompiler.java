package org.cord.node.cc;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.FeedbackErrorException;
import org.cord.node.NodeManager;
import org.cord.node.RegionStyle;
import org.cord.node.json.HandlesJSON;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.util.Gettable;
import org.cord.util.JSONUtils;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.SettableMap;
import org.json.JSONException;
import org.json.JSONObject;

public class CopyableTableContentCompiler
  extends TableContentCompiler
  implements CopyableContentCompiler, HandlesJSON
{
  public CopyableTableContentCompiler(String name,
                                      NodeManager nodeManager,
                                      String transactionPassword,
                                      TableContentCompilerDb abstractContentCompilerDb,
                                      Iterable<FieldKey<?>> strings,
                                      boolean isSafeHtml,
                                      Iterable<FieldKey<Boolean>> booleans,
                                      Iterable<FieldKey<Date>> fastDates)
  {
    super(name,
          nodeManager,
          transactionPassword,
          abstractContentCompilerDb,
          strings,
          isSafeHtml,
          booleans,
          fastDates);
  }

  /**
   * Set the toRegionStyle version of all of the names in the given iterator to the values that they
   * currently hold in fromInstance
   * 
   * @param settable
   * @param fromInstance
   * @param names
   * @throws SettableException
   */
  private final void addStringCopyParameters(Settable settable,
                                             PersistentRecord fromInstance,
                                             Iterator<FieldKey<?>> names)
      throws SettableException
  {
    while (names.hasNext()) {
      FieldKey<?> name = names.next();
      settable.set(name, fromInstance.opt(name));
    }
  }
  private final <T> void addCopyParameters(Settable settable,
                                           PersistentRecord fromInstance,
                                           Iterator<FieldKey<T>> names)
      throws SettableException
  {
    while (names.hasNext()) {
      FieldKey<?> name = names.next();
      settable.set(name, fromInstance.opt(name));
    }
  }

  /**
   * Set up the copy parameters for all of the strings, booleans and fastDates passed to the
   * constructor and then let the implementation do any further configuration if required. This is
   * done by overriding the extended version of copyContentCompiler
   * 
   * @see #copyContentCompiler(Settable, PersistentRecord, PersistentRecord, PersistentRecord,
   *      PersistentRecord)
   */
  @Override
  public final void copyContentCompiler(Settable settable,
                                        PersistentRecord fromNode,
                                        PersistentRecord fromRegionStyle,
                                        PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord fromInstance = getInstance(fromNode, fromRegionStyle, null, null);
    settable.set(ISDEFINED, fromInstance.opt(ISDEFINED));
    this.addStringCopyParameters(settable, fromInstance, getStringNames());
    addCopyParameters(settable, fromInstance, getBooleanNames());
    addCopyParameters(settable, fromInstance, getFastDateNames());
    copyContentCompiler(settable, fromNode, fromRegionStyle, toRegionStyle, fromInstance);
  }

  /**
   * Do nothing by default. Subclasses should override this if additional copying is required. It
   * will be invoked by copyContentCompiler after the automated copy parameters have been set up.
   * 
   * @param settable
   * @param fromNode
   * @param fromRegionStyle
   * @param toRegionStyle
   * @param fromInstance
   * @throws MirrorNoSuchRecordException
   * @throws SettableException
   * @see #copyContentCompiler(Settable, PersistentRecord, PersistentRecord, PersistentRecord)
   */
  protected void copyContentCompiler(Settable settable,
                                     PersistentRecord fromNode,
                                     PersistentRecord fromRegionStyle,
                                     PersistentRecord toRegionStyle,
                                     PersistentRecord fromInstance)
      throws MirrorNoSuchRecordException, SettableException
  {
  }

  /**
   * Invoke copyContentCompiler and then convert the resulting Settable into a JSONObject. This will
   * generally do pretty much what you want it to do. However if your implementation touches on
   * shared assets outside the core Node hierarchy then you may want to override this and add the
   * appropriate assets to the JSONSaver so that they are present when the code is reloaded.
   * 
   * @see JSONUtils#asJSONObject(Gettable, String)
   */
  @Override
  public JSONObject saveJSON(JSONSaver jsonSaver,
                             PersistentRecord node,
                             PersistentRecord regionStyle)
      throws JSONException, IOException
  {
    SettableMap map = new SettableMap();
    try {
      PersistentRecord instance = getInstance(node, regionStyle, null, null);
      copyContentCompiler(map, node, instance.comp(REGIONSTYLE), instance.comp(REGIONSTYLE));
    } catch (Exception e) {
      throw new JSONException("Unexpected exception", e);
    }
    return JSONUtils.asJSONObject(map, RegionStyle.NAMESPACE_SEPARATOR);
  }

  @Override
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jObj,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException
  {
    JSONUtils.addToSettable(jObj, settable, "", RegionStyle.NAMESPACE_SEPARATOR);
  }
}
