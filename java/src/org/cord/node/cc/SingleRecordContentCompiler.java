package org.cord.node.cc;

import java.util.Date;
import java.util.List;

import org.cord.mirror.FieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.NodeManager;

/**
 * A TableContentCompiler that is intended to only ever have a single instance such as a site-wide
 * configuration file.
 * 
 * @author alex
 */
public abstract class SingleRecordContentCompiler
  extends TableContentCompiler
{
  public SingleRecordContentCompiler(String name,
                                     NodeManager nodeManager,
                                     String transactionPassword,
                                     TableContentCompilerDb tableContentCompilerDb,
                                     List<FieldKey<?>> strings,
                                     boolean isSafeHtml,
                                     List<FieldKey<Boolean>> booleans,
                                     List<FieldKey<Date>> fastDates)
  {
    super(name,
          nodeManager,
          transactionPassword,
          tableContentCompilerDb,
          strings,
          isSafeHtml,
          booleans,
          fastDates);
  }

  public final PersistentRecord getInstance()
  {
    return RecordSources.getOnlyRecord(getInstanceTable(), null);
  }
}
