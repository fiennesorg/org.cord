package org.cord.node.cc;

import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.node.Node;
import org.cord.node.UnpublishedNodes;

import com.google.common.base.Preconditions;

/**
 * RecordSourceOperation that takes a {@link RecordSource} of {@link TableContentCompiler} and
 * filters out instances that are registered on Nodes that are not globally published.
 * 
 * @author alex
 */
public class IsPathPublished
  extends SimpleRecordSourceOperation<RecordSource>
{
  public static final RecordSourceOperationKey<RecordSource> KEY = Node.RS_ISPATHPUBLISHED.copy();

  protected static final void register(TableContentCompiler tableContentCompiler)
      throws MirrorTableLockedException
  {
    tableContentCompiler.getInstanceTable()
                        .addRecordSourceOperation(new IsPathPublished(tableContentCompiler.getNodeManager()
                                                                                          .getUnpublishedNodes()));
  }

  private final UnpublishedNodes __unpublishedNodes;

  private IsPathPublished(UnpublishedNodes unpublishedNodes)
  {
    super(KEY);
    __unpublishedNodes = Preconditions.checkNotNull(unpublishedNodes, "unpublishedNodes");
  }

  @Override
  public RecordSource invokeRecordSourceOperation(RecordSource instances,
                                                  Viewpoint viewpoint)
  {
    return RecordSources.viewedFrom(new SingleRecordSourceFilter(instances, null) {
      @Override
      protected boolean accepts(PersistentRecord instance)
      {
        return __unpublishedNodes.isPublished(instance.compInt(TableContentCompiler.NODE_ID));
      }
    }, viewpoint);
  }

}
