package org.cord.node.cc;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;

/**
 * Version of AbstractContentCompilerDb that models an TableContentCompiler that has no dedicated
 * style parameters associated with it. The most common case for this will be in situations when
 * there is only ever a single instance of the ContentCompiler within a site and therefore it is not
 * necessary to distinguish between the different versions. This will make the coding of the db
 * layer faster because there is only a single instance table rather than an instance and a style
 * table.
 * <p>
 * Please note that because this extends AbstractContentCompilerDb then you can extends
 * TableContentCompiler to complete the implementation.
 */
public abstract class StylelessTableContentCompilerDb
  extends TableContentCompilerDb
{
  public final static String ABSTRACTSTYLE = "AbstractStyle";

  /**
   * The RegionStyle.compilerStyleName for an instance that is compulsory and always defined.
   */
  public static final String STYLENAME_COMPULSORY = "compulsory";

  /**
   * The RegionStyle.compilerStyleName for an instance that is optional and initially undefined.
   */
  public static final String STYLENAME_OPTIONAL = "optional";

  /**
   * The RegionStyle.compilerStyleName for an instance that is optional and initially defined.
   */
  public static final String STYLENAME_OPTIONAL_DEFINED = "optional_defined";

  /**
   * Create an instance that has user-defined links between the title and isPublished fields on the
   * Node Table.
   * 
   * @param name
   *          The name by which this Db component should be known when registering on the database.
   *          To make things clean, I normally use the class name of the component as this ensures a
   *          clean namespace.
   * @param instanceTableName
   *          The MySQL name of the Table that holds instances of this ContentCompiler
   * @param instanceReferencesName
   *          The default reverse lookup name for the instance table. If null then a standard plural
   *          form will be generated.
   * @param instanceFieldCount
   *          The number of user defined fields (not including the standards defined in
   *          AbstractContentCompilerConstants) that are going to be registered on the instance
   *          Table by this component.
   * @param viewTemplatePath
   *          The webmacro path to the view template for this ContentCompiler. This will be
   *          automatically registered on the instance Table as a ConstantRecordOperation under the
   *          name of "viewTemplatePath"
   * @param editTemplatePath
   *          The webmacro path to the edit template for this ContentCompiler. This will be
   *          automatically registered on the instance Table as a ConstantRecordOperation under the
   *          name of "editTemplatePath".
   */
  public StylelessTableContentCompilerDb(String name,
                                         String instanceTableName,
                                         RecordOperationKey<RecordSource> instanceReferencesName,
                                         int instanceFieldCount,
                                         String viewTemplatePath,
                                         String editTemplatePath)
  {
    super(name,
          instanceTableName,
          instanceReferencesName,
          instanceFieldCount,
          new StyleTableWrapper(ABSTRACTSTYLE),
          viewTemplatePath,
          editTemplatePath);
  }
}
