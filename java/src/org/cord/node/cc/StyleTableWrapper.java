package org.cord.node.cc;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.recordsource.operation.MonoRecordSourceOperationKey;
import org.cord.node.NodeDbConstants;

/**
 * Extendable TableWrapper that is used to represent the Tables that model the styles that a given
 * ContentCompiler is capable of supporting. Out Of The Box it supports the minimum required, ie
 * name, isOptional and isDefined which is enough to implement the StylessTableContentCompiler. This
 * should be extended by ContentCompilers that want to define their own style configurations to
 * model the additional fields that are required.
 * 
 * @author alex
 */
public class StyleTableWrapper
  extends AbstractTableWrapper
{
  public static final ObjectFieldKey<String> NAME = StringField.createKey("name");

  public static final ObjectFieldKey<Boolean> ISOPTIONAL = BooleanField.createKey("isOptional");

  public static final ObjectFieldKey<Boolean> ISDEFINED = BooleanField.createKey("isDefined");

  public static final MonoRecordSourceOperationKey<String, RecordSource> NAMED =
      NodeDbConstants.NAMED.copy();

  public StyleTableWrapper(String tableName)
  {
    super(tableName);
  }

  @Override
  protected final void initTable(Db db,
                                 String pwd,
                                 Table styles)
      throws MirrorTableLockedException
  {
    styles.addField(new StringField(NAME, "Name", 32, ""));
    styles.addField(new BooleanField(ISOPTIONAL, "Instances are optional?", Boolean.FALSE));
    styles.addField(new BooleanField(ISDEFINED, "Instances default to defined?", Boolean.TRUE));
    styles.setTitleOperationName(NAME);
    styles.addRecordSourceOperation(new TableStringMatcher(NAMED,
                                                           NAME,
                                                           TableStringMatcher.SEARCH_EXACT,
                                                           null));
    initTableSub(db, pwd, styles);
  }

  /**
   * Perform any extension specific implementation of the style table after the core fields have
   * been initialised. This will be invoked by initTable automatically and should be subclassed as
   * required. The default implementation does nothing.
   */
  protected void initTableSub(Db db,
                              String pwd,
                              Table styles)
      throws MirrorTableLockedException
  {
  }
}
