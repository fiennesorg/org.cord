package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Table;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;

public class UploadedFile
  extends TableContentCompiler
{
  public static final String NAME = "UploadedFile";

  public static final String TABLENAME = NAME;

  public UploadedFile(NodeManager nodeMgr,
                      String pwd,
                      TableContentCompilerDb abstractContentCompilerDb)
  {
    super(NAME,
          nodeMgr,
          pwd,
          abstractContentCompilerDb);
    // TODO Auto-generated constructor stub
  }

  public class UploadedFileDb
    extends TableContentCompilerDb
  {

    public UploadedFileDb(String name,
                          String instanceTableName,
                          String instanceReferencesName,
                          int instanceFieldCount,
                          StyleTableWrapper styleTableWrapper,
                          String referencesName,
                          String viewTemplatePath,
                          String editTemplatePath)
    {
      super(NAME,
            TABLENAME,
            null,
            0,
            styleTableWrapper,
            referencesName,
            viewTemplatePath,
            editTemplatePath);
      // TODO Auto-generated constructor stub
    }

    @Override
    protected void internalInit(Db db,
                                String transactionPassword,
                                Table instanceTable)
        throws MirrorTableLockedException, DuplicateNamedException
    {
      // TODO Auto-generated method stub

    }

  }
}
