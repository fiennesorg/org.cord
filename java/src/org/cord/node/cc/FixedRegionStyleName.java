package org.cord.node.cc;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Viewpoint;

/**
 * An interface that states that the ContentCompiler should only be registered on a RegionStyle with
 * a fixed name. Any attempt to use it on a differently named RegionStyle should be unacceptable.
 * This also implies that there should be a maximum of one instance of the ContentCompiler registers
 * on each node.
 * 
 * @author alex
 */
public interface FixedRegionStyleName
{
  /**
   * Get the constant name of RegionStyles that it is permissable to register this ContentCompiler
   * against.
   */
  public String getRegionStyleName();

  /**
   * Resolve an instance of this record against the given node assuming that it registered with the
   * supplied name.
   */
  public PersistentRecord getInstance(PersistentRecord node,
                                      Viewpoint viewpoint)
      throws MirrorNoSuchRecordException;

  public PersistentRecord getCompInstance(PersistentRecord node,
                                          Viewpoint viewpoint);

  public PersistentRecord getOptInstance(PersistentRecord node,
                                         Viewpoint viewpoint);
}
