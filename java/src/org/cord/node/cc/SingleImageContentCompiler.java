package org.cord.node.cc;

import java.io.File;
import java.util.Map;

import org.cord.image.ImageTable;
import org.cord.image.SingleImageFormat;
import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.RegionStyle;
import org.cord.node.appcc.ScalableSingleImage;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Strings;

/**
 * @deprecated in preference of {@link ScalableSingleImage}
 */
@Deprecated
public class SingleImageContentCompiler
  extends TableContentCompiler
  implements CopyableContentCompiler, MoveableContentCompiler
{
  /**
   * "singleImages"
   */
  public final static String SINGLEIMAGE_REFERENCES = "singleImages";

  public final static String SINGLEIMAGEINSTANCE = "SingleImageInstance";

  /**
   * Optional CGI parameter that is used to represent the timestamp of an image asset that is being
   * uploaded.
   */
  public final static String CGI_FILETIMESTAMP = "fileTimestamp";

  public final static RecordOperationKey<Object> FILE =
      RecordOperationKey.create("file", Object.class);

  public final static String CGI_FILE = FILE.getKeyName();

  public final static String SINGLEIMAGE_VIEWTEMPLATEPATH =
      "node/SingleImage/singleImage.view.wm.html";

  public final static String SINGLEIMAGE_EDITTEMPLATEPATH =
      "node/SingleImage/singleImage.edit.wm.html";

  public final static RecordOperationKey<RecordSource> SINGLEIMAGEFORMAT_INSTANCES =
      RecordOperationKey.create("instances", RecordSource.class);

  /**
   * The address of the template ("node/SingleImage/singleImage-core.edit.wm.html") that does the
   * internal work of building the edit interface for a SingleImage editor.
   */
  public final static String SINGLEIMAGE_EDITTEMPLATEPATH_CORE =
      "node/SingleImage/singleImage-core.edit.wm.html";

  /**
   * "SingleImage"
   */
  public final static String NAME = ContentCompilerFactory.NAME_SINGLEIMAGE;

  public SingleImageContentCompiler(final NodeManager nodeManager,
                                    String transactionPassword)
      throws MirrorTableLockedException
  {
    super(NAME,
          nodeManager,
          transactionPassword,
          new TableContentCompilerDb("org.cord.node.cc.SingleImageDb",
                                     SINGLEIMAGEINSTANCE,
                                     null,
                                     5,
                                     new SingleImageStyle(),
                                     SINGLEIMAGE_REFERENCES,
                                     SINGLEIMAGE_VIEWTEMPLATEPATH,
                                     SINGLEIMAGE_EDITTEMPLATEPATH) {
            @Override
            protected void internalInit(Db db,
                                        String transactionPassword,
                                        Table instanceTable)
                throws MirrorTableLockedException, DuplicateNamedException
            {
              ImageTable.initialiseInstanceTable(nodeManager,
                                                 transactionPassword,
                                                 instanceTable,
                                                 SINGLEIMAGEFORMAT_INSTANCES);
              instanceTable.addRecordOperation(new SimpleRecordOperation<String>(ImageTable.EXPLICITCSS) {
                @Override
                public String getOperation(TransientRecord targetRecord,
                                           Viewpoint viewpoint)
                {
                  return "";
                }
              });
            }
          });
    nodeManager.getDb().add(new SingleImageFormat());
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getInstance(fromNode, fromRegionStyle, null, null);
    settable.set(ISDEFINED, instance.opt(ISDEFINED));
    ImageTable.copy(settable, instance);
  }

  public static TransientRecord updateInstance(NodeManager nodeManager,
                                               TransientRecord instance,
                                               PersistentRecord singleImageStyle,
                                               PersistentRecord regionStyle,
                                               Transaction transaction,
                                               Gettable params,
                                               File imageFile)
      throws MirrorFieldException
  {
    imageFile = imageFile == null ? params.getFile(CGI_FILE) : imageFile;
    String alt = params.getString(ImageTable.ALT);
    if (imageFile == null) {
      if (Boolean.TRUE.equals(params.getBoolean("restoreDefault"))) {
        ImageTable.setDefaultImage(instance);
      }
      ImageTable.updateAlt(instance, singleImageStyle, alt);
      return instance;
    } else {
      String fileTimestamp = params.getString(CGI_FILETIMESTAMP);
      if (fileTimestamp != null) {
        try {
          long parsedTimestamp = Long.parseLong(fileTimestamp);
          Long liveTimestamp = instance.opt(ImageTable.LASTMODIFIED);
          if (liveTimestamp.longValue() > parsedTimestamp) {
            ImageTable.updateAlt(instance, singleImageStyle, alt);
            return instance;
          }
        } catch (NumberFormatException nfEx) {
          // ignore and continue uploading image...
        }
      }
      Integer format_id = NumberUtil.toInteger(params.get(ImageTable.FORMAT_ID), null);
      if (format_id == null) {
        format_id = singleImageStyle.opt(ImageTable.FORMAT_ID);
      }
      try {
        return ImageTable.updateInstance(instance,
                                         singleImageStyle,
                                         imageFile,
                                         format_id,
                                         nodeManager.getWebRoot(),
                                         alt);
      } catch (MirrorFieldException mfEx) {
        if (regionStyle != null && !Strings.isNullOrEmpty(regionStyle.opt(RegionStyle.ALIASNAME))) {
          System.err.println("Caught Aliased error");
          System.err.println("Reseting isDefined on " + instance);
          instance.setField(ISDEFINED, Boolean.FALSE);
          return instance;
        }
        throw mfEx;
      }
    }
  }

  @Override
  protected final boolean updateTransientInstance(PersistentRecord node,
                                                  PersistentRecord regionStyle,
                                                  TransientRecord instance,
                                                  PersistentRecord style,
                                                  Transaction transaction,
                                                  Gettable inputs,
                                                  Map<Object, Object> outputs,
                                                  PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException
  {
    updateInstance(getNodeManager(), instance, style, regionStyle, transaction, inputs, null);
    return false;
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
  }
}
