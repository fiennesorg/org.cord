package org.cord.node.cc;

import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.node.ContentCompiler;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.RegionStyle;
import org.cord.util.AbstractGettable;
import org.cord.util.Gettable;

public class StaticTemplateContentCompiler
  extends ContentCompiler
{
  public final static String NAME = ContentCompilerFactory.NAME_STATICTEMPLATE;

  public final static String EMPTY_TEMPLATE = "node/StaticTemplate/empty.wm.html";

  public StaticTemplateContentCompiler(NodeManager nodeManager)
  {
    super(nodeManager,
          NAME);
  }

  /**
   * @return false
   */
  @Override
  public boolean utilisesNodeRequest()
  {
    return false;
  }

  /**
   * Do nothing.
   */
  @Override
  public void declare(PersistentRecord regionStyle)
  {
  }

  /**
   * Do nothing.
   */
  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle)
  {
  }

  /**
   * Do nothing.
   */
  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle,
                              Transaction deleteTransaction)
  {
  }

  /**
   * Do nothing.
   */
  @Override
  public void editInstance(PersistentRecord node,
                           PersistentRecord regionStyle,
                           Transaction editTransaction)
  {
  }

  @Override
  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    return new StaticTemplateInstance(regionStyle.opt(RegionStyle.COMPILERSTYLENAME));
  }

  /**
   * Do nothing.
   */
  @Override
  public void initialiseInstance(PersistentRecord node,
                                 PersistentRecord regionStyle,
                                 Gettable initParams,
                                 Map<Object, Object> outputs,
                                 Transaction transaction)
  {
  }

  /**
   * Do nothing.
   */
  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                Map<Object, Object> outputs,
                                PersistentRecord user)
  {
    return false;
  }

  /**
   * Accept the changed style as this will just reference a different template next time it is
   * invoked.
   */
  @Override
  public void updateStyle(TransientRecord regionStyle)
  {
  }

  public class StaticTemplateInstance
    extends AbstractGettable
    implements Gettable
  {
    private final String _templatePath;

    public StaticTemplateInstance(String templatePath)
    {
      _templatePath = templatePath;
    }

    @Override
    public Object get(Object name)
    {
      if (name.equals("isDefined")) {
        return (_templatePath.length() > 0 ? Boolean.TRUE : Boolean.FALSE);
      }
      return _templatePath.length() > 0 ? _templatePath : EMPTY_TEMPLATE;
    }

  }

}
