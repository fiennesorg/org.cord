package org.cord.node.cc;

import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.mirror.Viewpoint;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.util.Gettable;
import org.cord.util.GettableMapWrapper;
import org.cord.util.Maps;

/**
 * ContentCompiler that resolves a target webmacro template by the filename of the page that it is
 * being invoked on, relative to a constant webmacro template directory and then parses the
 * targetted directory. This therefore enables the use of webmacro based templates where there is
 * one template per filename without having to create a new NodeStyle and NodeGroupStyle for every
 * target template.
 */
public class IncludeTemplateByFilename
  extends ContentCompiler
{
  private final String __webmacroDirectory;

  // filename(String) --> Map(String --> String)
  private final Map<String, Gettable> __populatedInstances = Maps.newConcurrentHashMap();

  public IncludeTemplateByFilename(NodeManager nodeManager,
                                   String name,
                                   String englishName,
                                   String webmacroDirectory)
  {
    super(nodeManager,
          name,
          englishName);
    __webmacroDirectory = webmacroDirectory;
  }

  /**
   * Do nothing because we are not going to delete the webmacro template (which would be the only
   * realistic interpretation of this command)
   */
  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle,
                              Transaction deleteTransaction)
  {
  }

  /**
   * Do nothing because we are not going to edit the webmacro template (which would be the only
   * realistic interpretation of this command)
   */
  @Override
  public void editInstance(PersistentRecord node,
                           PersistentRecord regionStyle,
                           Transaction editTransaction)
  {
  }

  /**
   * Do nothing because we are not going to create a new webmacro template (which would be the only
   * realistic interpretation of this command)
   */
  @Override
  public void initialiseInstance(PersistentRecord node,
                                 PersistentRecord regionStyle,
                                 Gettable initParams,
                                 Map<Object, Object> outputs,
                                 Transaction transaction)
  {
  }

  /**
   * Do nothing because we are not going to edit the webmacro template (which would be the only
   * realistic interpretation of this command)
   */
  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                Map<Object, Object> outputs,
                                PersistentRecord user)
  {
    return false;
  }

  @Override
  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    String filename = node.opt(Node.FILENAME);
    Gettable result = __populatedInstances.get(filename);
    if (result == null) {
      String templatePath = __webmacroDirectory + filename;
      Map<String, String> map = new HashMap<String, String>();
      map.put(VIEWTEMPLATEPATH.getKeyName(), templatePath);
      map.put(EDITTEMPLATEPATH.getKeyName(), templatePath);
      result = new GettableMapWrapper(map);
      __populatedInstances.put(filename, result);
    }
    return result;
  }

  @Override
  public void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
  }
}
