package org.cord.node.cc;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.StringField;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.search.AdvancedWordSearchable;
import org.cord.sql.SqlUtil;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.LogicException;

import com.google.common.collect.ImmutableList;

public class SimpleTextContentCompiler
  extends TableContentCompiler
  implements AdvancedWordSearchable, MoveableContentCompiler
{
  /**
   * "SimpleTextInstance"
   */
  public final static String SIMPLETEXTINSTANCE = "SimpleTextInstance";

  /**
   * "simpleTexts"
   */
  public final static String SIMPLETEXT_REFERENCES = "simpleTexts";

  /**
   * "text"
   */
  public final static ObjectFieldKey<String> TEXT = StringField.createKey("text");

  /**
   * "html"
   */
  public final static ObjectFieldKey<String> HTML = StringField.createKey("html");

  /**
   * "node/SimpleText/simpleText.view.wm.html"
   */
  public final static String VIEWTEMPLATEPATH = "node/SimpleText/simpleText.view.wm.html";

  /**
   * "node/SimpleText/simpleText.edit.wm.html"
   */
  public final static String EDITTEMPLATEPATH = "node/SimpleText/simpleText.edit.wm.html";

  /**
   * "SimpleText"
   */
  public final static String NAME = ContentCompilerFactory.NAME_SIMPLETEXT;

  public SimpleTextContentCompiler(NodeManager nodeManager,
                                   String transactionPassword)
  {
    super(NAME,
          nodeManager,
          transactionPassword,
          new TableContentCompilerDb(NAME,
                                     SIMPLETEXTINSTANCE,
                                     null,
                                     1,
                                     new SimpleTextStyle(),
                                     SIMPLETEXT_REFERENCES,
                                     VIEWTEMPLATEPATH,
                                     EDITTEMPLATEPATH) {
            @Override
            protected void internalInit(Db db,
                                        String transactionPassword,
                                        Table instanceTable)
                throws MirrorTableLockedException, DuplicateNamedException
            {
              instanceTable.addField(new StringField(TEXT,
                                                     "Text",
                                                     StringField.LENGTH_TEXT,
                                                     "").setMayBeTextArea(true));
              instanceTable.addField(new StringField(HTML,
                                                     "Compiled text",
                                                     StringField.LENGTH_TEXT,
                                                     ""));
              instanceTable.addFieldTrigger(new AbstractFieldTrigger<String>(TEXT, true, false) {
                @Override
                public Object triggerField(TransientRecord instance,
                                           String fieldName,
                                           Transaction pwd)
                {
                  PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
                  String text = instance.opt(TEXT);
                  int maxLength = style.compInt(SimpleTextStyle.MAXLENGTH);
                  if (maxLength > 0 && text.length() > maxLength) {
                    text = text.substring(0, maxLength);
                  }
                  boolean supportsLineBreaks = style.is(SimpleTextStyle.SUPPORTSLINEBREAKS);
                  StringBuilder htmlBuffer = new StringBuilder();
                  String viewCss = style.opt(SimpleTextStyle.VIEWCSS);
                  if (viewCss.length() > 0 || style.is(SimpleTextStyle.ISBLOCK)) {
                    String tagType = style.is(SimpleTextStyle.ISBLOCK) ? "DIV" : "SPAN";
                    htmlBuffer.append("<").append(tagType);
                    if (viewCss.length() > 0) {
                      htmlBuffer.append(" CLASS=\"").append(viewCss).append("\"");
                    }
                    htmlBuffer.append(">");
                    if (supportsLineBreaks) {
                      SqlUtil.textToHtml(htmlBuffer, text);
                    } else {
                      SqlUtil.noHtml(htmlBuffer, text);
                    }
                    htmlBuffer.append("</").append(tagType).append(">");
                  } else {
                    if (supportsLineBreaks) {
                      SqlUtil.textToHtml(htmlBuffer, text);
                    } else {
                      SqlUtil.noHtml(htmlBuffer, text);
                    }
                  }
                  instance.setSafeField(HTML, htmlBuffer.toString());
                  return null;
                }
              });
            }
          });
  }

  @Override
  protected final boolean updateTransientInstance(PersistentRecord node,
                                                  PersistentRecord regionStyle,
                                                  TransientRecord simpleTextInstance,
                                                  PersistentRecord simpleTextStyle,
                                                  Transaction transaction,
                                                  Gettable inputs,
                                                  Map<Object, Object> outputs,
                                                  PersistentRecord user)
      throws MirrorTransactionTimeoutException
  {
    String text = inputs.getString(TEXT);
    if (text != null) {
      try {
        simpleTextInstance.setField(TEXT, text);
      } catch (MirrorFieldException mfEx) {
        throw new LogicException("Unexpected field exception on " + simpleTextInstance + " with "
                                 + text,
                                 mfEx);
      }
    }
    return false;
  }

  public static final List<RecordOperationKey<?>> AWS_FIELDS =
      ImmutableList.<RecordOperationKey<?>> builder().add(TEXT).build();

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getInstance(node, regionStyle, null, viewpoint);
    return instance.is(ISDEFINED) ? instance : null;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return AWS_FIELDS;
  }

  // --------------------------------------------------------------------------
  // MoveableContentCompiler methods

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
    // No need to do anything
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
    // No need to do anything
  }
}
