package org.cord.node.cc;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.ConstantRecordOperation;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.SingleRecordSourceFilter;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.RegionStyle;
import org.cord.node.UnpublishedNodes;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.NamedImpl;
import org.cord.util.StringUtils;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Macro;

import com.google.common.base.Preconditions;

/**
 * Class that declares the appropriate database structures for an instance:style
 * TableContentCompiler. The default ordering for the instance table will be set up as follows:-
 * <ul>
 * <li>If just title is declared then "title"
 * <li>If title and number is declared then "number, title"
 * <li>If just number is declared then "number"
 * </ul>
 * Note that this is prepared before the internalInit is invoked so it is possible to override these
 * settings with custom configurations as required.
 */
public abstract class TableContentCompilerDb
  extends NamedImpl
  implements DbInitialiser
{
  private final String __instanceTableName;

  private final RecordOperationKey<RecordSource> __instanceReferencesName;

  private final StyleTableWrapper __styleTableWrapper;

  public final StyleTableWrapper getStyleTableWrapper()
  {
    return __styleTableWrapper;
  }

  private final String __viewTemplatePath;

  private final String __editTemplatePath;

  private TableContentCompiler _tableContentCompiler;

  private boolean _decoupleUpdateLock = false;

  /**
   * @param name
   *          The name by which this Db component should be known when registering on the database.
   *          To make things clean, I normally use the class name of the component as this ensures a
   *          clean namespace.
   * @param instanceTableName
   *          The MySQL name of the Table that holds instances of this ContentCompiler
   * @param instanceReferencesName
   *          The default reverse lookup name for the instance table. If null then a standard plural
   *          form will be generated.
   * @param instanceFieldCount
   *          The number of user defined fields (not including the standards defined in
   *          AbstractContentCompilerConstants) that are going to be registered on the instance
   *          Table by this component.
   * @param viewTemplatePath
   *          The webmacro path to the view template for this ContentCompiler. This will be
   *          automatically registered on the instance Table as a ConstantRecordOperation under the
   *          name of "viewTemplatePath"
   * @param editTemplatePath
   *          The webmacro path to the edit template for this ContentCompiler. This will be
   *          automatically registered on the instance Table as a ConstantRecordOperation under the
   *          name of "editTemplatePath".
   */
  public TableContentCompilerDb(String name,
                                String instanceTableName,
                                RecordOperationKey<RecordSource> instanceReferencesName,
                                int instanceFieldCount,
                                StyleTableWrapper styleTableWrapper,
                                String viewTemplatePath,
                                String editTemplatePath)
  {
    super(name);
    __instanceTableName = instanceTableName;
    __instanceReferencesName = instanceReferencesName;
    __styleTableWrapper = styleTableWrapper;
    __viewTemplatePath = viewTemplatePath;
    __editTemplatePath = editTemplatePath;
    // Assertions.withinRange(linkTitle,
    // TableContentCompiler.LINK_NONE,
    // TableContentCompiler.LINK_NODE_TO_INSTANCE,
    // "linkTitle");
    // __linkTitle = linkTitle;
    // Assertions.withinRange(linkIsPublished,
    // TableContentCompiler.LINK_NONE,
    // TableContentCompiler.LINK_NODE_TO_INSTANCE,
    // "linkIsPublished");
    // __linkIsPublished = linkIsPublished;
    // Assertions.withinRange(linkNumber,
    // TableContentCompiler.LINK_NONE,
    // TableContentCompiler.LINK_NODE_TO_INSTANCE,
    // "linkNumber");
    // __linkNumber = linkNumber;
  }

  /**
   * Set whether or not this ContentCompiler should utilise the DecoupleUpdate system. This should
   * be invoked before the DbInitialiser is invoked otherwise it will have no affect. The default
   * value is false, ie there is no decoupling. If you set it to true then the instance record will
   * never be locked when the Region is locked for editing and the Region will never be locked when
   * the instance is locked. This makes it easier to bind in third party systems that update
   * autonomously independantly of the main CMS system.
   */
  public void setDecoupleUpdateLock(boolean flag)
  {
    _decoupleUpdateLock = flag;
  }

  public boolean getDecoupleUpdateLock()
  {
    return _decoupleUpdateLock;
  }

  /**
   * Inform this db compiler which TableContentCompiler it is associated with. This is invoked
   * automatically in the constructor for TableContentCompiler and should not be invoked by clients.
   * 
   * @see #getTableContentCompiler()
   */
  protected synchronized void setTableContentCompiler(TableContentCompiler tableContentCompiler)
  {
    if (_tableContentCompiler != null && !_tableContentCompiler.equals(tableContentCompiler)) {
      throw new IllegalStateException("Cannot set " + tableContentCompiler
                                      + " as TableContentCompiler on " + this
                                      + " when already initialised as " + _tableContentCompiler);
    }
    _tableContentCompiler = tableContentCompiler;
  }

  /**
   * Get the TableContentCompiler that this DbInitialiser has been registered on. This will not
   * return a sensible value unless it has been registered, but this will be the case before it has
   * to initialise itself.
   * 
   * @see #setTableContentCompiler(TableContentCompiler)
   */
  public TableContentCompiler getTableContentCompiler()
  {
    return _tableContentCompiler;
  }

  @Override
  public void shutdown()
  {
  }

  public TableContentCompilerDb(String name,
                                String instanceTableName,
                                RecordOperationKey<RecordSource> instanceReferencesName,
                                int instanceFieldCount,
                                StyleTableWrapper styleTableWrapper,
                                String referencesName,
                                String viewTemplatePath,
                                String editTemplatePath)
  {
    this(name,
         instanceTableName,
         instanceReferencesName,
         instanceFieldCount,
         styleTableWrapper,
         viewTemplatePath,
         editTemplatePath);
  }

  /**
   * Get the name of the Table that holds information about individual instances of this
   * TableContentCompiler. This value is passed to the constructor and is a constant.
   * 
   * @return The name of the instance Table.
   */
  public final String getInstanceTableName()
  {
    return __instanceTableName;
  }

  /**
   * Get the name of the Table that holds information about styles or configurations of this
   * TableContentCompiler. This value is passed to the constructor and is a constant.
   * 
   * @return The name of the style Table.
   */
  public final String getStyleTableName()
  {
    return __styleTableWrapper.getTableName();
  }

  @Override
  public final void init(final Db db,
                         final String transactionPassword)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    Table instanceTable =
        db.addTable(null, __instanceTableName, __instanceReferencesName, null, null);
    int nodeDependencies = Dependencies.BIT_ENFORCED | Dependencies.BIT_S_ON_T_DELETE
                           | Dependencies.BIT_T_ON_S_DELETE | Dependencies.BIT_S_ON_T_UPDATE
                           | (_decoupleUpdateLock ? 0 : Dependencies.BIT_T_ON_S_UPDATE);
    instanceTable.addField(new LinkField(TableContentCompiler.NODE_ID,
                                         "Containing Node",
                                         null,
                                         TableContentCompiler.NODE,
                                         Node.TABLENAME,
                                         __instanceReferencesName,
                                         null,
                                         transactionPassword,
                                         nodeDependencies) {
      @Override
      public boolean mayHaveDependencies(TransientRecord node,
                                         int transactionType)
      {
        PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
        Set<?> names = (Set<?>) nodeStyle.get("contentCompilers");
        return names == null || names.contains(getTableContentCompiler().getName());
      }
    });
    instanceTable.addField(new LinkField(TableContentCompiler.REGIONSTYLE_ID,
                                         "Controlling RegionStyle",
                                         null,
                                         TableContentCompiler.REGIONSTYLE,
                                         RegionStyle.TABLENAME,
                                         __instanceReferencesName,
                                         null,
                                         transactionPassword,
                                         Dependencies.LINK_ENFORCED));
    instanceTable.addField(new LinkField(TableContentCompiler.STYLE_ID,
                                         "Controlling Style",
                                         null,
                                         TableContentCompiler.STYLE,
                                         getStyleTableName(),
                                         __instanceReferencesName,
                                         null,
                                         transactionPassword,
                                         Dependencies.LINK_ENFORCED));
    instanceTable.addField(new BooleanField(TableContentCompiler.ISDEFINED,
                                            "Is defined on current Node?",
                                            Boolean.TRUE));
    instanceTable.addFieldTrigger(ImmutableFieldTrigger.create(TableContentCompiler.REGIONSTYLE_ID));
    instanceTable.addFieldTrigger(ImmutableFieldTrigger.create(TableContentCompiler.NODE_ID));
    instanceTable.addFieldTrigger(ImmutableFieldTrigger.create(TableContentCompiler.STYLE_ID));
    instanceTable.addFieldTrigger(new AbstractFieldTrigger<Boolean>(TableContentCompiler.ISDEFINED,
                                                                    true,
                                                                    false) {
      /**
       * If TableContentCompiler.ISDEFINED is set to false, then check to see if ACCSTYLE_ISOPTIONAL
       * is set and complain if this is not the case.
       * 
       * @throws MirrorFieldException
       *           If the instance is set to non-defined, but the style is set to compulsory.
       */
      @Override
      public Object triggerField(TransientRecord accInstance,
                                 String fieldName,
                                 Transaction transaction)
          throws MirrorFieldException
      {
        if (!accInstance.is(TableContentCompiler.ISDEFINED)) {
          PersistentRecord accStyle = accInstance.comp(TableContentCompiler.STYLE);
          if (!accStyle.is(TableContentCompilerStyle.ISOPTIONAL)) {
            throw new MirrorFieldException("Instances of " + accStyle + " are compulsory",
                                           null,
                                           accInstance,
                                           TableContentCompiler.ISDEFINED,
                                           Boolean.FALSE,
                                           Boolean.TRUE);
          }
        }
        return null;
      }
    });
    instanceTable.addRecordOperation(ConstantRecordOperation.create(ContentCompiler.VIEWTEMPLATEPATH,
                                                                    __viewTemplatePath));
    instanceTable.addRecordOperation(ConstantRecordOperation.create(ContentCompiler.EDITTEMPLATEPATH,
                                                                    __editTemplatePath));
    instanceTable.addRecordOperation(new SimpleRecordOperation<String>(TableContentCompiler.URL) {
      @Override
      public String getOperation(TransientRecord instance,
                                 Viewpoint viewpoint)
      {
        return TableContentCompiler.getNode(instance).opt(Node.URL);
      }
    });
    instanceTable.addRecordOperation(new MonoRecordOperation<String, String>(TableContentCompiler.HREF,
                                                                             Casters.TOSTRING) {
      @Override
      public String calculate(TransientRecord instance,
                              Viewpoint viewpoint,
                              String variable)
      {
        String linkContents = instance.hasRecordOperation(variable)
            ? StringUtils.noHtml(instance.getString(variable))
            : variable;
        return TableContentCompiler.getNode(instance).opt(Node.EXPLICITHREF, linkContents);
      }
    });
    instanceTable.addRecordOperation(new MonoRecordOperation<String, String>(TableContentCompiler.EXPLICITHREF,
                                                                             Casters.TOSTRING) {
      @Override
      public String calculate(TransientRecord instance,
                              Viewpoint viewpoint,
                              String contents)
      {
        return TableContentCompiler.getNode(instance).opt(Node.EXPLICITHREF, contents);
      }
    });
    instanceTable.addRecordOperation(new MonoRecordOperation<String, Macro>(HtmlGuiSupplier.ASFORMELEMENT,
                                                                            Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord instance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            appendFormElement(instance, viewpoint, name, out, context);
          }
        };
      }
    });
    instanceTable.addRecordOperation(new MonoRecordOperation<String, Macro>(HtmlGuiSupplier.ASFORMVALUE,
                                                                            Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord instance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            appendFormValue(instance, viewpoint, name, out, context);
          }
        };
      }
    });
    // instanceTable.addQueryOperation(new AbstractQueryTransform("joinNodes")
    // {
    // public Query transformQuery(Query sourceQuery)
    // {
    // StringBuilder filter = new StringBuilder();
    // filter.append("(").append(__instanceTableName).append(".node_id=Node.id)");
    // return ConstantQueryTransform.transformQuery(sourceQuery,
    // filter.toString(),
    // ConstantQueryTransform.TYPE_AND,
    // null,
    // db.getTable(Node.TABLENAME));
    // }
    // });
    // handle the following backward compatability issues:-
    // $instance.region.node
    // $instance.region.name
    // $instance.region.regionStyle
    instanceTable.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(TableContentCompiler.NAMEPREFIX) {
      @Override
      public String calculate(TransientRecord instance,
                              Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(instance.comp(TableContentCompiler.REGIONSTYLE).opt(RegionStyle.NAME))
           .append(RegionStyle.NAMESPACE_SEPARATOR);
        return buf.toString();
      }
    });
    final UnpublishedNodes unpublishedNodes =
        Preconditions.checkNotNull(getTableContentCompiler().getNodeManager().getUnpublishedNodes(),
                                   "unpublishedNodes");
    instanceTable.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(TableContentCompiler.ISPATHPUBLISHED) {

      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource instances,
                                                      Viewpoint viewpoint)
      {
        return RecordSources.viewedFrom(new SingleRecordSourceFilter(instances, null) {
          @Override
          protected boolean accepts(PersistentRecord instance)
          {
            return unpublishedNodes.isPublished(instance.compInt(TableContentCompiler.NODE_ID));
          }
        }, viewpoint);
      }
    });
    internalInit(db, transactionPassword, instanceTable);
  }
  protected static void appendFormElement(TransientRecord instance,
                                          Viewpoint viewpoint,
                                          String name,
                                          Appendable buf,
                                          Map<Object, Object> context)
      throws IOException
  {
    String namePrefix = instance.opt(TableContentCompiler.NAMEPREFIX);
    instance.appendFormElement(namePrefix, name, buf, context);
  }

  protected static void appendFormValue(TransientRecord instance,
                                        Viewpoint viewpoint,
                                        String name,
                                        Appendable buf,
                                        Map<Object, Object> context)
      throws IOException
  {
    String namePrefix = instance.opt(TableContentCompiler.NAMEPREFIX);
    instance.appendFormValue(namePrefix, name, buf, context);
  }

  protected abstract void internalInit(Db db,
                                       String transactionPassword,
                                       Table instanceTable)
      throws MirrorTableLockedException, DuplicateNamedException;

  /**
   * Check to see whether a given Style Table is possible to have defined instances. This is
   * calculated by ($style.isOptional || $style.isDefined).
   * 
   * @param style
   *          The style to analyse
   * @return true if it would feasibly be possible to have a defined instance of this style, false
   *         if all instances must be undefined.
   */
  public static boolean isPossible(TransientRecord style)
  {
    return style.is(TableContentCompilerStyle.ISOPTIONAL)
           | style.is(TableContentCompilerStyle.ISDEFINED);
  }

  /**
   * RecordOperation ("isPossible") that returns whether or not it is possible to create an instance
   * based on this Style definition. This is calculated as ($style.isOptional || $style.isDefined).
   * 
   * @see #isPossible(TransientRecord)
   */
  protected class IsPossible
    extends SimpleRecordOperation<Boolean>
  {
    public IsPossible()
    {
      super(TableContentCompilerStyle.ISPOSSIBLE);
    }

    @Override
    public Boolean getOperation(TransientRecord style,
                                Viewpoint viewpoint)
    {
      return isPossible(style) ? Boolean.TRUE : Boolean.FALSE;
    }
  }
}
