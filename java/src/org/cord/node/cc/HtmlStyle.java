package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.StringField;

public class HtmlStyle
  extends StyleTableWrapper
{
  /**
   * "HtmlStyle"
   */
  public final static String TABLENAME = "HtmlStyle";

  /**
   * "textareaCss"
   */
  public final static ObjectFieldKey<String> TEXTAREACSS = StringField.createKey("textareaCss");

  /**
   * "textareaCols"
   */
  public final static IntFieldKey TEXTAREACOLS = IntField.createKey("textareaCols");

  /**
   * "textareaRows"
   */
  public final static IntFieldKey TEXTAREAROWS = IntField.createKey("textareaRows");

  public HtmlStyle()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styles)
      throws MirrorTableLockedException
  {
    styles.addField(new StringField(TEXTAREACSS, null));
    styles.addField(new IntField(TEXTAREACOLS, null));
    styles.addField(new IntField(TEXTAREAROWS, null));
  }
}
