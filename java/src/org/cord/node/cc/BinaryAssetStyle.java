package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;

public class BinaryAssetStyle
  extends StyleTableWrapper
{
  public final static String TABLENAME = "BinaryAssetStyle";

  public final static ObjectFieldKey<String> FOLDER = StringField.createKey("folder");

  public final static IntFieldKey FORMAT_ID = IntFieldKey.create("format_id");

  public final static ObjectFieldKey<Boolean> ISFIXEDFORMAT =
      BooleanField.createKey("isFixedFormat");

  public final static ObjectFieldKey<Boolean> HASTITLE = BooleanField.createKey("hasTitle");

  public final static ObjectFieldKey<String> DEFAULTTITLE = StringField.createKey("defaultTitle");

  public final static ObjectFieldKey<Boolean> HASDESCRIPTION =
      BooleanField.createKey("hasDescription");

  public final static ObjectFieldKey<String> DEFAULTDESCRIPTION =
      StringField.createKey("defaultDescription");

  public final static IntFieldKey MAXSIZEKB = IntField.createKey("maxSizeKb");

  public final static RecordOperationKey<PersistentRecord> FORMAT =
      LinkField.getLinkName(FORMAT_ID);

  public BinaryAssetStyle()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styleTable)
      throws MirrorTableLockedException
  {
    styleTable.addField(new StringField(FOLDER, "Webpath to folder containing asset directories"));
    styleTable.addField(new LinkField(FORMAT_ID,
                                      "Default asset format",
                                      null,
                                      FORMAT,
                                      BinaryAssetContentCompiler.BINARYASSETFORMAT,
                                      BinaryAssetContentCompiler.BINARYASSETFORMAT_STYLES,
                                      TableContentCompilerStyle.NAME,
                                      pwd,
                                      Dependencies.LINK_ENFORCED));
    styleTable.addField(new BooleanField(ISFIXEDFORMAT, "Is asset format fixed?", Boolean.FALSE));
    styleTable.addField(new BooleanField(HASTITLE,
                                         "Assets will have visible titles?",
                                         Boolean.FALSE));
    styleTable.addField(new StringField(DEFAULTTITLE, "Default value for title"));
    styleTable.addField(new BooleanField(HASDESCRIPTION,
                                         "Assets will have visible descriptions?",
                                         Boolean.FALSE));
    styleTable.addField(new StringField(DEFAULTDESCRIPTION, "Default value for description"));
    styleTable.addField(new IntField(MAXSIZEKB, "Maximum asset size in kilobytes"));
  }
}
