package org.cord.node.cc;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionOwnershipException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.PostFieldTrigger;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.StateTrigger;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.FileFormElement;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractStateTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.CopyableContentCompiler;
import org.cord.node.FilenameGenerator;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.search.AdvancedWordSearchable;
import org.cord.node.search.FileIndexer;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;
import org.cord.util.LogicException;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

public class BinaryAssetContentCompiler
  extends TableContentCompiler
  implements AdvancedWordSearchable, CopyableContentCompiler, MoveableContentCompiler
{
  public final static String NAME = ContentCompilerFactory.NAME_BINARYASSET;

  public final static String BINARYASSETFORMAT = "BinaryAssetFormat";

  public final static ObjectFieldKey<String> BINARYASSETFORMAT_NAME = StringField.createKey("name");

  public final static ObjectFieldKey<String> BINARYASSETFORMAT_EXTENSION =
      StringField.createKey("extension");

  public final static ObjectFieldKey<String> BINARYASSETFORMAT_MIMETYPE =
      StringField.createKey("mimetype");

  public final static ObjectFieldKey<String> BINARYASSETFORMAT_ICONPATH =
      StringField.createKey("iconPath");

  public final static IntFieldKey BINARYASSETFORMAT_ICONWIDTH = IntField.createKey("iconWidth");

  public final static IntFieldKey BINARYASSETFORMAT_ICONHEIGHT = IntField.createKey("iconHeight");

  public final static RecordOperationKey<RecordSource> BINARYASSETFORMAT_INSTANCES =
      RecordOperationKey.create("instances", RecordSource.class);

  public final static RecordOperationKey<RecordSource> BINARYASSETFORMAT_STYLES =
      RecordOperationKey.create("styles", RecordSource.class);

  public final static int BINARYASSETFORMAT_ID_PDF = 1;

  public final static int BINARYASSETFORMAT_ID_UNSPECIFIED = 100;

  public final static String BINARYASSETINSTANCE = "BinaryAssetInstance";

  public final static ObjectFieldKey<Boolean> ISUPLOADED = BooleanField.createKey("isUploaded");

  public final static ObjectFieldKey<String> PATH = StringField.createKey("path");

  public final static ObjectFieldKey<String> FOLDER = StringField.createKey("folder");

  public final static ObjectFieldKey<String> FILENAME = StringField.createKey("filename");

  public final static IntFieldKey FORMAT_ID = IntFieldKey.create("format_id");

  public final static IntFieldKey SIZEKB = IntField.createKey("sizeKb");

  public final static ObjectFieldKey<String> TITLE = StringField.createKey("title");

  public final static ObjectFieldKey<String> DESCRIPTION = StringField.createKey("description");

  public final static RecordOperationKey<PersistentRecord> FORMAT =
      LinkField.getLinkName(FORMAT_ID);

  public final static RecordOperationKey<Object> FILE =
      RecordOperationKey.create("file", Object.class);

  public final static RecordOperationKey<Date> TIMESTAMP =
      RecordOperationKey.create("timestamp", Date.class);

  public final static String REFERENCES = "BinaryAssets";

  public final static String VIEWTEMPLATEPATH = "node/BinaryAsset/binaryasset.view.wm.html";

  public final static String EDITTEMPLATEPATH = "node/BinaryAsset/binaryasset.edit.wm.html";

  public final static String BINARYASSET_FORMAT_PDF = "Adobe PDF";

  public static final String CGI_FILE = FILE.getKeyName();

  public static final RecordOperationKey<File> JAVAFILE =
      RecordOperationKey.create("javaFile", File.class);

  private final Map<String, FileIndexer> __indexers = new HashMap<String, FileIndexer>();

  public BinaryAssetContentCompiler(NodeManager nodeManager,
                                    String transactionPassword)
  {
    super(NAME,
          nodeManager,
          transactionPassword,
          new BinaryAssetDb(nodeManager));
  }

  @Override
  public void shutdown()
  {
    super.shutdown();
    Iterator<FileIndexer> indexers = __indexers.values().iterator();
    while (indexers.hasNext()) {
      indexers.next().shutdown();
    }
  }

  public void registerBinaryAssetIndexer(FileIndexer indexer)
  {
    if (indexer != null) {
      __indexers.put(indexer.getName(), indexer);
    }
  }

  public FileIndexer getIndexer(String formatName)
  {
    return __indexers.get(formatName);
  }

  /**
   * Take a user-supplied filename, tidy it up and then ensure it contains a valid ending as
   * specified by a BinaryAssetFormat record.
   * 
   * @param format
   *          The BinaryAssetFormat that this filename is to be associated with. If this is null
   *          then no file extension checks will be performed.
   * @param filename
   *          The user supplied filename.
   * @return The tidied filename.
   * @see FilenameGenerator#generateFilename(String)
   */
  public static String validateFilename(NodeManager nodeManager,
                                        PersistentRecord format,
                                        String filename)
  {
    filename = nodeManager.getFilenameGenerator().generateFilename(filename);
    if (format != null) {
      String extension = format.opt(BINARYASSETFORMAT_EXTENSION);
      if (extension.length() > 0) {
        if (!filename.toLowerCase().endsWith(extension)) {
          filename = filename + extension;
        }
      }
    }
    return filename;
  }

  public static String validateFilename(NodeManager nodeManager,
                                        PersistentRecord format,
                                        File file)
  {
    return validateFilename(nodeManager, format, file.getName());
  }

  public final static int TIMESTAMP_ATTEMPTS = 50;

  /**
   * Create a new temporary directory inside the subdirectory pointed to by style.webpathRoot which
   * will be guaranteed empty and available for storing new information in. Please note that this
   * does <i>not</i> create any new database information as this is the responsibility of the client
   * calling the operation.
   * 
   * @param nodeManager
   *          the source of the overall htdocs root directory.
   * @param style
   *          The PersistentRecord that contains the subdirectory in which we are going to try and
   *          create the file.
   * @return The File representing the created directory.
   */
  public static synchronized File createAssetDirectory(NodeManager nodeManager,
                                                       PersistentRecord style)
  {
    File webRoot = new File(nodeManager.getWebRoot(), style.opt(BinaryAssetStyle.FOLDER));
    if (!(webRoot.exists() && webRoot.isDirectory() && webRoot.canWrite())) {
      throw new LogicException("Bad asset directory:" + webRoot);
    }
    long timestamp = System.currentTimeMillis();
    File assetDirectory = null;
    for (int i = 0; i < TIMESTAMP_ATTEMPTS; i++) {
      assetDirectory = new File(webRoot, Long.toString(timestamp));
      if (!assetDirectory.exists() && assetDirectory.mkdir()) {
        return assetDirectory;
      }
      timestamp++;
    }
    throw new LogicException("Could not create asset directory, final attempt:" + assetDirectory);
  }

  /**
   * Attempt to "import" a file into the database structure.
   * 
   * @throws MirrorFieldException
   *           if the uploaded file is larger than the maxSizeKb set for the corresponding style for
   *           the target instance.
   * @see BinaryAssetStyle#MAXSIZEKB
   */
  public static void uploadFile(NodeManager nodeManager,
                                TransientRecord instance,
                                File uploadedFile,
                                Transaction transaction)
      throws MirrorFieldException
  {
    if (uploadedFile == null) {
      return;
    }
    PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
    int maxSizeKb = style.compInt(BinaryAssetStyle.MAXSIZEKB);
    int sizeKb = (int) (uploadedFile.length() / 1024);
    if (maxSizeKb > 0 && maxSizeKb < sizeKb) {
      throw new MirrorFieldException("Uploaded file too big (" + sizeKb + ">" + maxSizeKb + ")",
                                     null,
                                     instance,
                                     PATH,
                                     uploadedFile,
                                     null);
    }
    String webpathRoot = style.opt(BinaryAssetStyle.FOLDER);
    File assetDirectory = createAssetDirectory(nodeManager, style);
    PersistentRecord format = instance.opt(FORMAT);
    String filename = validateFilename(nodeManager, format, uploadedFile);
    File assetFile = new File(assetDirectory, filename);
    try {
      IoUtil.unixCp(uploadedFile, assetFile, false);
      instance.setField(SIZEKB, Integer.valueOf(sizeKb));
      instance.setField(FOLDER, assetDirectory.getName());
      instance.setField(FILENAME, filename);
      instance.setField(PATH, webpathRoot + "/" + assetDirectory.getName() + "/" + filename);
      instance.setField(ISUPLOADED, Boolean.TRUE);
    } catch (IOException failedMvEx) {
      throw new LogicException("Failed to execute upload mv command", failedMvEx);
    } catch (MirrorFieldException mfEx) {
      throw new LogicException("Failed to extract uploaded file size", mfEx);
    }
  }

  /**
   * @throws MirrorFieldException
   *           if the uploaded file is larger than the maxSizeKb set for the corresponding style for
   *           the target instance. It is also possible that MirrorFieldExceptions may be thrown but
   *           this is the most "normal process" one.
   */
  public static void updateInstance(NodeManager nodeManager,
                                    TransientRecord instance,
                                    PersistentRecord style,
                                    PersistentRecord regionStyle,
                                    Transaction transaction,
                                    Gettable params,
                                    boolean updateIsDefined)
      throws MirrorFieldException
  {
    if (updateIsDefined) {
      updateIsDefined(params, regionStyle, style, instance);
    }
    File uploadedFile = params.getFile(CGI_FILE);
    if (uploadedFile != null) {
      if (!style.is(BinaryAssetStyle.ISFIXEDFORMAT)) {
        instance.setFieldIfDefined(FORMAT_ID, params.get(FORMAT_ID));
      }
      uploadFile(nodeManager, instance, uploadedFile, transaction);
    }
    if (style.is(BinaryAssetStyle.HASTITLE)) {
      instance.setFieldIfDefined(TITLE, params.get(TITLE));
    }
    if (style.is(BinaryAssetStyle.HASDESCRIPTION)) {
      instance.setFieldIfDefined(DESCRIPTION, params.get(DESCRIPTION));
    }
  }

  @Override
  public void copyContentCompiler(Settable settable,
                                  PersistentRecord fromNode,
                                  PersistentRecord fromRegionStyle,
                                  PersistentRecord toRegionStyle)
      throws MirrorNoSuchRecordException, SettableException
  {
    PersistentRecord instance = getInstance(fromNode, fromRegionStyle, null, null);
    settable.set(ISDEFINED, instance.opt(ISDEFINED));
    copy(settable, instance);
  }

  public static void copy(Settable settable,
                          PersistentRecord instance)
      throws SettableException
  {
    PersistentRecord style = instance.comp(STYLE);
    if (style.is(BinaryAssetStyle.HASTITLE)) {
      settable.set(TITLE, instance.opt(TITLE));
    }
    if (style.is(BinaryAssetStyle.HASDESCRIPTION)) {
      settable.set(DESCRIPTION, instance.opt(DESCRIPTION));
    }
    settable.set(FORMAT_ID, instance.opt(FORMAT_ID));
    if (instance.is(ISUPLOADED)) {
      settable.set(CGI_FILE, instance.opt(JAVAFILE));
    }
  }

  @Override
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorFieldException
  {
    updateInstance(getNodeManager(), instance, style, regionStyle, transaction, inputs, true);
    return false;
  }

  public static void initialiseInstanceTable(final NodeManager nodeManager,
                                             Table instances,
                                             String transactionPassword,
                                             RecordOperationKey<RecordSource> references)
      throws MirrorTableLockedException
  {
    instances.addField(new BooleanField(ISUPLOADED, "Is asset uploaded?", Boolean.FALSE));
    instances.addField(new StringField(PATH, "Webpath to asset"));
    instances.addField(new StringField(FOLDER, "Dynamically generated folder name"));
    instances.addField(new StringField(FILENAME, "Filename of asset"));
    instances.addField(new LinkField(FORMAT_ID,
                                     "Asset format",
                                     null,
                                     FORMAT,
                                     BINARYASSETFORMAT,
                                     references,
                                     FORMAT_ID + "," + TITLE,
                                     transactionPassword,
                                     Dependencies.LINK_ENFORCED));
    instances.addField(new IntField(SIZEKB, "Asset size in kilobytes"));
    instances.addField(new StringField(TITLE, "Visible title"));
    instances.addField(new StringField(DESCRIPTION, "Visible description"));
    // TODO - FileFormElement return type????
    instances.addRecordOperation(new FileFormElement(FILE, "Select asset") {
      @Override
      public Object getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        return getInstanceFile(nodeManager, targetRecord);
      }
    });
    instances.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord instance,
                             Gettable params)
          throws MirrorFieldException
      {
        PersistentRecord style = instance.comp(TableContentCompiler.STYLE);
        if (!instance.mayFollowLink(FORMAT_ID.getKeyName(), null)) {
          instance.setField(FORMAT_ID, style.opt(BinaryAssetStyle.FORMAT_ID));
        }
        if (!Strings.isNullOrEmpty(instance.opt(TITLE))) {
          instance.setField(TITLE, style.opt(BinaryAssetStyle.DEFAULTTITLE));
        }
        if (!Strings.isNullOrEmpty(instance.opt(DESCRIPTION))) {
          instance.setField(DESCRIPTION, style.opt(BinaryAssetStyle.DEFAULTDESCRIPTION));
        }
      }
    });
    instances.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_WRITABLE_CANCELLED,
                                                       false,
                                                       instances.getName()) {
      @Override
      public void trigger(PersistentRecord instance,
                          Transaction transaction,
                          int state)
          throws MirrorTransactionOwnershipException
      {
        WritableRecord writableInstance = instance.getWritableRecord(transaction);
        String persistentWebpath = instance.opt(PATH);
        String writableWebpath = writableInstance.opt(PATH);
        if (persistentWebpath.equals(writableWebpath)) {
          return;
        }
        deleteAsset(nodeManager, writableInstance);
      }
    });
    instances.addStateTrigger(new AbstractStateTrigger(StateTrigger.STATE_PREDELETED_DELETED,
                                                       false,
                                                       instances.getName()) {
      @Override
      public void trigger(PersistentRecord instance,
                          Transaction transaction,
                          int state)
          throws Exception
      {
        deleteAsset(nodeManager, instance);
      }
    });
    instances.addPostFieldTrigger(new PostFieldTrigger<String>() {
      @Override
      public FieldKey<String> getFieldName()
      {
        return PATH;
      }

      @Override
      public void trigger(TransientRecord instance,
                          String fieldName,
                          Object originalValue,
                          boolean hasChanged)
      {
        if (!hasChanged || instance.opt(FOLDER).equals("")) {
          return;
        }
        File originalFile = new File(nodeManager.getWebRoot(), originalValue.toString());
        originalFile.delete();
        originalFile.getParentFile().delete();
      }

      @Override
      public void shutdown()
      {
      }
    });
    instances.addRecordOperation(new SimpleRecordOperation<String>(BINARYASSETINSTANCE_ASTEXT) {
      @Override
      public String getOperation(TransientRecord instance,
                                 Viewpoint viewpoint)
      {
        // PersistentRecord binaryAssetFormat =
        // binaryAssetInstance.followEnforcedLink(BINARYASSETINSTANCE_FORMAT);
        // BinaryAssetIndexer binaryAssetIndexer =
        // getIndexer(binaryAssetFormat.getStringField(BINARYASSETFORMAT_NAME));;
        // TODO (#267) Sort out the plugin architecture for the
        // $binaryAssetInstance.asText functionality
        return null;
      }
    });
    instances.addRecordOperation(new SimpleRecordOperation<Date>(TIMESTAMP) {
      @Override
      public Date getOperation(TransientRecord instance,
                               Viewpoint viewpoint)
      {
        String folder = instance.opt(FOLDER);
        try {
          return new Date(Long.parseLong(folder));
        } catch (NumberFormatException nfEx) {
          return null;
        }
      }
    });
    instances.addRecordOperation(new SimpleRecordOperation<File>(JAVAFILE) {
      @Override
      public File getOperation(TransientRecord instance,
                               Viewpoint viewpoint)
      {
        return new File(nodeManager.getWebRoot(), instance.opt(PATH));
      }
    });
  }

  public static class BinaryAssetDb
    extends TableContentCompilerDb
  {
    private NodeManager _nodeManager;

    public final static String NAME = "org.cord.node.cc.BinaryAssetDb";

    public BinaryAssetDb(NodeManager nodeManager)
    {
      super(NAME,
            BINARYASSETINSTANCE,
            null,
            8,
            new BinaryAssetStyle(),
            REFERENCES,
            VIEWTEMPLATEPATH,
            EDITTEMPLATEPATH);
      _nodeManager = nodeManager;
    }

    @Override
    public void shutdown()
    {
      _nodeManager = null;
    }

    @Override
    protected void internalInit(Db db,
                                String transactionPassword,
                                Table instanceTable)
        throws MirrorTableLockedException
    {
      // TODO: refactor BinaryAssetFormat into AbstractTableWrapper class
      Table formatTable = db.addTable(null, BINARYASSETFORMAT);
      formatTable.addField(new StringField(BINARYASSETFORMAT_NAME, "Name", 32, ""));
      formatTable.addField(new StringField(BINARYASSETFORMAT_EXTENSION, "File extension"));
      formatTable.addField(new StringField(BINARYASSETFORMAT_MIMETYPE, "MIME type"));
      formatTable.addField(new StringField(BINARYASSETFORMAT_ICONPATH, "Webpath to icon file"));
      formatTable.addField(new IntField(BINARYASSETFORMAT_ICONWIDTH, "Width of icon in pixels"));
      formatTable.addField(new IntField(BINARYASSETFORMAT_ICONHEIGHT, "Height of icon in pixels"));
      initialiseInstanceTable(_nodeManager,
                              instanceTable,
                              transactionPassword,
                              BINARYASSETFORMAT_INSTANCES);
    }
  }

  private static boolean deleteAsset(NodeManager nodeManager,
                                     TransientRecord instance)
  {
    File instanceFile = getInstanceFile(nodeManager, instance);
    if (instanceFile.exists()) {
      instanceFile.delete();
      getInstanceFolder(nodeManager, instance).delete();
      return true;
    }
    return false;
  }

  /**
   * @return the File pointing to the binary asset referenced by the given instance.
   */
  public static File getInstanceFile(NodeManager nodeManager,
                                     TransientRecord instance)
  {
    return new File(nodeManager.getWebRoot(), instance.opt(PATH));
  }

  /**
   * Get the temporary instance folder with the timestamp name that contains the asset for this
   * instance.
   */
  public static File getInstanceFolder(NodeManager nodeManager,
                                       TransientRecord instance)
  {
    return new File(getStyleFolder(nodeManager, instance.comp(TableContentCompiler.STYLE)),
                    instance.opt(FOLDER));
  }

  /**
   * Get the folder that contains all the instance folders for binary assets of the given style.
   */
  public static File getStyleFolder(NodeManager nodeManager,
                                    TransientRecord style)
  {
    return new File(nodeManager.getWebRoot(), style.opt(BinaryAssetStyle.FOLDER));
  }

  public final static RecordOperationKey<String> BINARYASSETINSTANCE_ASTEXT =
      RecordOperationKey.create("asText", String.class);

  private final List<RecordOperationKey<?>> FIELDS_WORD =
      ImmutableList.<RecordOperationKey<?>> builder()
                   .add(DESCRIPTION, BINARYASSETINSTANCE_ASTEXT, TITLE)
                   .build();

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getInstance(node, regionStyle, null, viewpoint);
    return instance.is(ISDEFINED) ? instance : null;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return FIELDS_WORD;
  }

  @Override
  public Collection<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return FIELDS_WORD;
  }

  // --------------------------------------------------------------------------
  // MoveableContentCompiler methods

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
    // No need to do anything
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
    // No need to do anything
  }
}
