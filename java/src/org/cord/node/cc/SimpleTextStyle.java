package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.IntField;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.StringField;

public class SimpleTextStyle
  extends StyleTableWrapper
{
  /**
   * "SimpleTextStyle"
   */
  public final static String TABLENAME = "SimpleTextStyle";

  /**
   * "maxLength"
   */
  public final static IntFieldKey MAXLENGTH = IntField.createKey("maxLength");

  public final static int MAXLENGTH_UNLIMITED = -1;

  /**
   * "supportsLineBreaks"
   */
  public final static ObjectFieldKey<Boolean> SUPPORTSLINEBREAKS =
      BooleanField.createKey("supportsLinebreaks");

  /**
   * "editorRows"
   */
  public final static IntFieldKey EDITORROWS = IntField.createKey("editorRows");

  /**
   * "editorColumns"
   */
  public final static IntFieldKey EDITORCOLUMNS = IntField.createKey("editorColumns");

  /**
   * "editCss"
   */
  public final static ObjectFieldKey<String> EDITCSS = StringField.createKey("editCss");

  /**
   * "viewCss"
   */
  public final static ObjectFieldKey<String> VIEWCSS = StringField.createKey("viewCss");

  /**
   * "isBLock"
   */
  public final static ObjectFieldKey<Boolean> ISBLOCK = BooleanField.createKey("isBlock");

  protected SimpleTextStyle()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styles)
      throws MirrorTableLockedException
  {
    styles.addField(new IntField(MAXLENGTH, "Maximum length"));
    styles.addField(new BooleanField(SUPPORTSLINEBREAKS, "Supports line breaks?", Boolean.FALSE));
    styles.addField(new IntField(EDITORROWS, "Editor rows"));
    styles.addField(new IntField(EDITORCOLUMNS, "Editor columns"));
    styles.addField(new StringField(EDITCSS, "Edit CSS class"));
    styles.addField(new StringField(VIEWCSS, "View CSS class"));
    styles.addField(new BooleanField(ISBLOCK, "Is block?", Boolean.FALSE));
  }
}
