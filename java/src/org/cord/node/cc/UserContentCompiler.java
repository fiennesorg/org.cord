package org.cord.node.cc;

import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.operation.ConstantRecordOperation;
import org.cord.node.ContentCompiler;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.RegionStyle;
import org.cord.node.User;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;

/**
 * ContentCompiler implementation that provides an editable interface onto records in the User
 * Table. There is a bit of a hack about this in that it drops in some ConstantRecordOperations onto
 * the User Table for viewTemplatePath and editTemplatePath. In itself, this is not a bad thing, for
 * this is the same tactic used by TableContentCompiler on the instance Tables, but this does imply
 * that there is only one permissable interface for editing the User Table per instance of the Node
 * system. In the first cases, this is not a situation, but it may become a situation in the future.
 * If this is the case, then some kind of proxy wrapper may need to be considered.
 */
public class UserContentCompiler
  extends ContentCompiler
{
  private final String _transactionPassword;

  /**
   * "UserContentCompiler"
   */
  public final static String NAME = "UserContentCompiler";

  public final static RecordOperationKey<String> VIEWTEMPLATEPATH =
      RecordOperationKey.create("viewTemplatePath", String.class);
  public final static RecordOperationKey<String> EDITTEMPLATEPATH =
      RecordOperationKey.create("editTemplatePath", String.class);

  public UserContentCompiler(NodeManager nodeManager,
                             String transactionPassword,
                             String viewTemplatePath,
                             String editTemplatePath)
      throws MirrorTableLockedException
  {
    super(nodeManager,
          NAME);
    _transactionPassword = transactionPassword;
    Table userTable = getDb().getTable(User.TABLENAME);
    userTable.addRecordOperation(ConstantRecordOperation.create(VIEWTEMPLATEPATH,
                                                                viewTemplatePath));
    userTable.addRecordOperation(ConstantRecordOperation.create(EDITTEMPLATEPATH,
                                                                editTemplatePath));
  }

  /**
   * @return false
   */
  @Override
  public boolean utilisesNodeRequest()
  {
    return false;
  }

  /**
   * Do nothing.
   */
  @Override
  public void declare(PersistentRecord regionStyle)
  {
  }

  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle,
                              Transaction deleteTransaction)
      throws MirrorTransactionTimeoutException
  {
    deleteTransaction.delete(node.comp(Node.OWNER), Db.DEFAULT_TIMEOUT);
  }

  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle)
      throws MirrorTransactionTimeoutException
  {
    Transaction deleteTransaction = null;
    deleteTransaction = getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                  Transaction.TRANSACTION_DELETE,
                                                  _transactionPassword,
                                                  "UserContentCompiler.destroyInstance");
    destroyInstance(node, regionStyle, deleteTransaction);
    deleteTransaction.commit();
  }

  @Override
  public void editInstance(PersistentRecord node,
                           PersistentRecord regionStyle,
                           Transaction editTransaction)
      throws MirrorTransactionTimeoutException
  {
    editTransaction.edit(node.comp(Node.OWNER), Db.DEFAULT_TIMEOUT);
  }

  @Override
  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    return node.comp(Node.OWNER);
  }

  @Override
  public void initialiseInstance(PersistentRecord node,
                                 PersistentRecord regionStyle,
                                 Gettable initParams,
                                 Map<Object, Object> outputs,
                                 Transaction transaction)
  {
  }

  /**
   * Atte
   */
  public static boolean updateInstance(TransientRecord user,
                                       Gettable params)
      throws MirrorFieldException
  {
    String name = params.getString(User.NAME);
    if (name != null) {
      user.setField(User.NAME, name);
    }
    String password1 = params.getString("password1");
    if (password1 != null && password1.length() > 0) {
      String password2 = params.getString("password2");
      if (password1.equals(password2)) {
        user.setField(User.PASSWORD, password1);
      }
    }
    int status = NumberUtil.toInt(params.get(User.STATUS), -1);
    // ### 2DO need to check for rights to update status
    switch (status) {
      case -1:
        // no param passed, so skip the set operation...
        break;
      case User.STATUS_CONFIRMED:
      case User.STATUS_SUSPENDED:
        user.setField(User.STATUS, Integer.valueOf(status));
        break;
      default:
        throw new MirrorFieldException("Unnacceptable value for user.status",
                                       null,
                                       user,
                                       User.STATUS,
                                       Integer.valueOf(status),
                                       null);
    }
    return user.getIsUpdated();
  }

  /**
   * UserContentCompiler does not currently support style migration and will therefore reject all
   * requests to perform the operation. Please note that UserContentCompiler does not actually
   * utilise REGIONSTYLE_CONTENTCOMPILERSTYLE at present, but if it does in the future, then the
   * exception should be removed.
   * 
   * @throws MirrorFieldException
   *           always!
   */
  @Override
  public void updateStyle(TransientRecord regionStyle)
      throws MirrorFieldException
  {
    throw new MirrorFieldException("UserContentCompiler does not support style migration",
                                   null,
                                   regionStyle,
                                   RegionStyle.COMPILERSTYLENAME,
                                   regionStyle.opt(RegionStyle.COMPILERSTYLENAME),
                                   null);
  }

  @Override
  public boolean updateInstance(PersistentRecord node,
                                PersistentRecord regionStyle,
                                Transaction editTransaction,
                                Gettable params,
                                Map<Object, Object> outputs,
                                PersistentRecord user)
      throws MirrorFieldException
  {
    return updateInstance(node.comp(Node.OWNER), params);
  }

}
