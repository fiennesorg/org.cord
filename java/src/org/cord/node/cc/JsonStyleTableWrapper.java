package org.cord.node.cc;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.DelegatingJSONObjectField;
import org.cord.mirror.field.JSONObjectField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.TableStringMatcher;
import org.json.ImmutableJSON;
import org.json.JSONObject;

public class JsonStyleTableWrapper
  extends StyleTableWrapper
{
  public static JSONObject getJSONObject(TransientRecord style)
  {
    return style.opt(JSON);
  }

  public static final ObjectFieldKey<String> EXTENDS = StringField.createKey("extends");

  public static final ObjectFieldKey<String> JSON_TEXT = JSONObjectField.createKey("json_text");
  public static final RecordOperationKey<JSONObject> LOCALJSON =
      JSONObjectField.createAsJSONKey("localJSON");
  public static final RecordOperationKey<JSONObject> JSON = JSONObjectField.createAsJSONKey("JSON");

  public JsonStyleTableWrapper(String tableName)
  {
    super(tableName);
  }

  @Override
  protected final void initTableSub(Db db,
                                    String pwd,
                                    Table styles)
      throws MirrorTableLockedException
  {
    styles.addField(new StringField(EXTENDS, "Extends name"));
    styles.addField(new DelegatingJSONObjectField(JSON_TEXT, "JSON", LOCALJSON, JSON) {
      @Override
      public JSONObject resolveDefaults(TransientRecord record)
      {
        PersistentRecord defaults =
            TableStringMatcher.optOnlyMatch(getTable(), NAME, record.opt(EXTENDS), null);
        return defaults == null ? ImmutableJSON.get().emptyJSONObject() : getJSONObject(defaults);
      }
    });
  }
}
