package org.cord.node.cc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.GettableRecordProcessor;
import org.cord.mirror.IdList;
import org.cord.mirror.IntFieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperation;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BitLinkField;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.ManyToManyRecursiveReference;
import org.cord.mirror.operation.ManyToManyReference;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.ContentCompiler;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeFilter;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestSegmentManager;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.RegionStyle;
import org.cord.node.ValueFactory;
import org.cord.node.listing.QueryListingFactory;
import org.cord.node.view.AskNodeQuestion;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.LogicException;
import org.cord.util.Maps;
import org.cord.util.NumberUtil;
import org.cord.util.PrivateCacheKey;
import org.cord.util.StringUtils;
import org.webmacro.Context;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * The TableContentCompiler provides a persistent ContentCOmpiler where each RegionStyle on a given
 * Node is backed by a single record in an instance Table.
 * 
 * @author alex
 */
public class TableContentCompiler
  extends ContentCompiler
{
  public static final IntFieldKey ID = TransientRecord.FIELD_ID.copy();

  public final static IntFieldKey NODE_ID = LinkField.getLinkFieldName(Node.TABLENAME);

  public final static RecordOperationKey<PersistentRecord> NODE = LinkField.getLinkName(NODE_ID);

  public final static IntFieldKey REGIONSTYLE_ID =
      LinkField.getLinkFieldName(RegionStyle.TABLENAME);

  public final static RecordOperationKey<PersistentRecord> REGIONSTYLE =
      LinkField.getLinkName(REGIONSTYLE_ID);

  /**
   * "style_id"
   */
  public final static IntFieldKey STYLE_ID = IntFieldKey.create("style_id");

  /**
   * "style"
   */
  public final static RecordOperationKey<PersistentRecord> STYLE = LinkField.getLinkName(STYLE_ID);

  /**
   * The field ("isDefined") that states whether or not a TableContentCompiler record is active. In
   * addition it is the field that is submitted in an incoming update request to state that
   * parameters for this TableContentCompiler are being transmitted for update in this request.
   */
  public final static ObjectFieldKey<Boolean> ISDEFINED = BooleanField.createKey("isDefined");

  public final static RecordOperationKey<String> URL = Node.URL;

  public final static MonoRecordOperationKey<String, String> HREF = Node.HREF.copy();
  public final static MonoRecordOperationKey<String, String> EXPLICITHREF =
      Node.EXPLICITHREF.copy();

  /**
   * Get the standard prefix to params relating to an instance of this TableContentCompiler -
   * "$instance.regionStyle.name-"
   */
  public static final RecordOperationKey<String> NAMEPREFIX =
      RecordOperationKey.create("namePrefix", String.class);

  public static final RecordSourceOperationKey<RecordSource> ISPATHPUBLISHED =
      Node.RS_ISPATHPUBLISHED.copy();

  /**
   * Resolve the node that an instance of any TableContentCompiler is attached to. This is
   * equivalent to $instance.node
   */
  public final static PersistentRecord getNode(TransientRecord instance)
  {
    return instance.comp(NODE);
  }

  public static Iterable<FieldKey<?>> strings(FieldKey<?>... keys)
  {
    List<FieldKey<?>> strings = Lists.newArrayList();
    for (FieldKey<?> key : keys) {
      strings.add(key);
    }
    return strings;
  }

  @Deprecated
  public static List<String> unmodifiableList(String... values)
  {
    return Collections.unmodifiableList(Arrays.asList(values));
  }

  /*
   * Take an optional existing List of Strings and add in the contents of an optional array of
   * Strings. All null values are filtered out of the resulting List.
   */
  public static List<String> unmodifiableList(List<String> existingValues,
                                              String... additionalValues)
  {
    List<String> values = new ArrayList<String>();
    if (existingValues != null) {
      for (String v : existingValues) {
        if (v != null) {
          values.add(v);
        }
      }
    }
    if (additionalValues != null) {
      for (String v : additionalValues) {
        if (v != null) {
          values.add(v);
        }
      }
    }
    return Collections.unmodifiableList(values);
  }

  private TableContentCompilerDb _tableContentCompilerDb;

  private Table _instanceTable = null;

  private Table _styleTable = null;

  private String _transactionPassword;

  private Iterable<FieldKey<?>> _strings;

  protected final Iterator<FieldKey<?>> getStringNames()
  {
    if (_strings == null) {
      return ImmutableSet.<FieldKey<?>> of().iterator();
    }
    return _strings.iterator();
  }

  private boolean _isSafeHtml;

  private Iterable<FieldKey<Boolean>> _booleans;

  protected final Iterator<FieldKey<Boolean>> getBooleanNames()
  {
    if (_booleans == null) {
      return ImmutableSet.<FieldKey<Boolean>> of().iterator();
    }
    return _booleans.iterator();
  }

  private Iterable<FieldKey<Date>> _fastDates;

  protected final Iterator<FieldKey<Date>> getFastDateNames()
  {
    if (_fastDates == null) {
      return ImmutableSet.<FieldKey<Date>> of().iterator();
    }
    return _fastDates.iterator();
  }

  private Calendar _calendar;

  public TableContentCompiler(String name,
                              NodeManager nodeMgr,
                              String pwd,
                              TableContentCompilerDb tableContentCompilerDb,
                              Iterable<FieldKey<?>> strings,
                              boolean isSafeHtml,
                              Iterable<FieldKey<Boolean>> booleans,
                              Iterable<FieldKey<Date>> fastDates)
  {
    super(nodeMgr,
          name);
    _transactionPassword = pwd;
    _tableContentCompilerDb = tableContentCompilerDb;
    _strings = strings;
    _isSafeHtml = isSafeHtml;
    _booleans = booleans;
    _fastDates = fastDates;
    if (_fastDates != null) {
      _calendar = Calendar.getInstance();
    }
    _tableContentCompilerDb.setTableContentCompiler(this);
  }

  public TableContentCompiler(String name,
                              NodeManager nodeManager,
                              String transactionPassword,
                              TableContentCompilerDb abstractContentCompilerDb)
  {
    this(name,
         nodeManager,
         transactionPassword,
         abstractContentCompilerDb,
         null,
         false,
         null,
         null);
  }

  @Override
  public void shutdown()
  {
    _tableContentCompilerDb = null;
    _instanceTable = null;
    _styleTable = null;
    _transactionPassword = null;
    _strings = null;
    _booleans = null;
    _fastDates = null;
    super.shutdown();
  }

  public final RecordOperationKey<RecordSource> getReferenceName()
  {
    return _instanceTable.getReferencesName();
  }

  protected final String getTransactionPassword()
  {
    return _transactionPassword;
  }

  private int _declarationCount = 0;

  /**
   * Verify that this is a valid style and declare any links that have been declared between the
   * NODE_TITLE and NODE_ISPUBLISHED fields by the AbstractContentCompilerDb. If this
   * ContentCompiler is a FixedRegionStyleName then it will validate that the named of the declaring
   * regionStyle conforms to the required name. This will then invoke internalDeclare(regionStyle)
   * to enable subclasses to insert additional functionality that may be required.
   * 
   * @throws IllegalArgumentException
   *           If this is a FixedRegionStyleName and the passed regionStyle doesn't conform to the
   *           required RegionStyle name
   * @see #internalDeclare(PersistentRecord)
   * @see #getStyleRecord(PersistentRecord,Viewpoint)
   * @see FixedRegionStyleName#getRegionStyleName()
   */
  @Override
  public final void declare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    getStyleRecord(regionStyle, null);
    internalDeclare(regionStyle);
    _declarationCount++;
  }

  /**
   * Get the number of times that this class has been registered already on the current Node
   * deployment. This count is incremented after internalDeclare has been cleanly invoked.
   */
  public final int getDeclarationCount()
  {
    return _declarationCount;
  }

  /**
   * Overrideable method as a substitute for declare(regionStyle)
   * 
   * @see #declare(PersistentRecord)
   */
  protected void internalDeclare(PersistentRecord regionStyle)
      throws MirrorTableLockedException, MirrorNoSuchRecordException
  {
    final Node Node_ = getNodeManager().getNode();
    final String name = regionStyle.comp(RegionStyle.NAME);
    RecordOperation<?> resolver = Node_.getTable().getRecordOperation(name);
    if (resolver == null) {
      Node_.getTable()
           .addRecordOperation(new SimpleRecordOperation<PersistentRecord>(RecordOperationKey.create(name,
                                                                                                     PersistentRecord.class)) {
             @Override
             public PersistentRecord getOperation(TransientRecord node,
                                                  Viewpoint viewpoint)
             {
               return (PersistentRecord) node.opt(Node.COMPILE, name);
             }
           });
    }
  }
  public final String getInstanceTableName()
  {
    return _tableContentCompilerDb.getInstanceTableName();
  }

  /**
   * Get the instance Table associated with this ContentCompiler. This method will cache the Table
   * value. It is not pre-cached earlier because the Table may not have been initialised when the
   * ContentCompiler is created.
   * 
   * @return The instance Table for the ContentCompiler.
   */
  public final Table getInstanceTable()
  {
    if (_instanceTable == null) {
      _instanceTable = getDb().getTable(getInstanceTableName());
    }
    return _instanceTable;
  }

  /**
   * Get the style Table associated with this ContentCompiler.
   * 
   * @return The style Table for the ContentCompiler.
   */
  protected final Table getStyleTable()
  {
    if (_styleTable == null) {
      _styleTable = getDb().getTable(_tableContentCompilerDb.getStyleTableName());
    }
    return _styleTable;
  }

  /**
   * @param node
   *          compulsory, from Node.
   */
  public final PersistentRecord getInstance(PersistentRecord node,
                                            String regionStyleName,
                                            Gettable params,
                                            Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    TransientRecord.assertIsTable(node, getNodeManager().getNode().getTable());
    if (viewpoint == null && node instanceof WritableRecord) {
      viewpoint = node.getTransaction(getTransactionPassword(), "TableContentCompiler.getInstance");
    }
    PersistentRecord instance = getCachedInstance(node, regionStyleName, viewpoint);
    if (instance != null) {
      return instance;
    }
    return getInstance(node,
                       getNodeManager().getRegionStyle()
                                       .getInstance(node.compInt(Node.NODESTYLE_ID),
                                                    regionStyleName,
                                                    viewpoint),
                       params,
                       viewpoint);
  }

  public final PersistentRecord getOptInstance(PersistentRecord node,
                                               String regionStyleName,
                                               Gettable params,
                                               Viewpoint viewpoint)
  {
    PersistentRecord instance = getCachedInstance(node, regionStyleName, viewpoint);
    if (instance != null) {
      return instance;
    }
    PersistentRecord regionStyle = getNodeManager().getRegionStyle()
                                                   .getOptInstance(node.compInt(Node.NODESTYLE_ID),
                                                                   regionStyleName,
                                                                   viewpoint);
    if (regionStyle == null) {
      return null;
    }
    return getOptInstance(node, regionStyle, params, viewpoint);
  }

  private Query getInstanceQuery(PersistentRecord node,
                                 PersistentRecord regionStyle)
  {
    Table table = getInstanceTable();
    String tableName = table.getName();
    StringBuilder buf = new StringBuilder(50);
    buf.append("(")
       .append(tableName)
       .append(".node_id=")
       .append(node.getId())
       .append(" and ")
       .append(tableName)
       .append(".regionStyle_id=")
       .append(regionStyle.getId())
       .append(")");
    String filter = buf.toString();
    return table.getQuery(filter, filter, null);
  }

  public final PersistentRecord getInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            Gettable params,
                                            Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getOptInstance(node, regionStyle, params, viewpoint);
    if (instance != null) {
      return instance;
    }
    throw new MirrorNoSuchRecordException(String.format("Cannot resolve TableContentCompiler for %s on %s",
                                                        regionStyle,
                                                        node),
                                          getInstanceTable());
  }
  public final PersistentRecord getOptInstance(PersistentRecord node,
                                               PersistentRecord regionStyle,
                                               Gettable params,
                                               Viewpoint viewpoint)
  {
    TransientRecord.assertIsTableNamed(node, Node.TABLENAME);
    TransientRecord.assertIsTableNamed(regionStyle, RegionStyle.TABLENAME);
    String regionStyleName = regionStyle.opt(RegionStyle.NAME);
    PersistentRecord instance = getCachedInstance(node, regionStyleName, viewpoint);
    if (instance != null) {
      return instance;
    }
    return precacheInstances(node, regionStyle, viewpoint);
  }

  private PersistentRecord precacheInstances(PersistentRecord node,
                                             PersistentRecord desiredRegionStyle,
                                             Viewpoint viewpoint)
  {
    final int desiredRegionStyleId = desiredRegionStyle.getId();
    PersistentRecord desiredInstance = null;
    StringBuilder buf = new StringBuilder(32);
    buf.append(getTableContentCompilerDb().getInstanceTableName())
       .append("." + NODE_ID + "=")
       .append(NumberUtil.toString(node.getId()));
    final String filter = buf.toString();
    for (PersistentRecord instance : getInstanceTable().getQuery(filter, filter, null)
                                                       .getRecordList(viewpoint)) {
      PersistentRecord regionStyle = instance.comp(REGIONSTYLE);
      if (regionStyle.getId() == desiredRegionStyleId) {
        desiredInstance = instance;
      }
      setCachedInstance(node, regionStyle.opt(RegionStyle.NAME), Integer.valueOf(instance.getId()));
    }
    return desiredInstance;
  }

  private ConcurrentMap<String, PrivateCacheKey> __cacheKeys = Maps.newConcurrentHashMap();

  private PrivateCacheKey getCacheKey(String regionStyleName)
  {
    PrivateCacheKey key = __cacheKeys.get(regionStyleName);
    if (key != null) {
      return key;
    }
    PrivateCacheKey newKey = new PrivateCacheKey(regionStyleName);
    PrivateCacheKey existingKey = __cacheKeys.putIfAbsent(regionStyleName, newKey);
    return existingKey == null ? newKey : existingKey;
  }

  public PersistentRecord getCachedInstance(PersistentRecord node,
                                            String regionStyleName,
                                            Viewpoint viewpoint)
  {
    Object cachedObj = node.getCachedValue(getCacheKey(regionStyleName));
    if (cachedObj == null) {
      return null;
    }
    try {
      return getInstanceTable().getRecord((Integer) cachedObj, viewpoint);
    } catch (MirrorNoSuchRecordException e) {
      throw new MirrorLogicException(String.format("invalid cached RegionStyle of %s on %s.%s",
                                                   cachedObj,
                                                   node,
                                                   regionStyleName),
                                     e);
    }
  }

  public Integer getCachedId(PersistentRecord node,
                             String regionStyleName)
  {
    Object cachedObj = node.getCachedValue(getCacheKey(regionStyleName));
    try {
      return (Integer) cachedObj;
    } catch (ClassCastException e) {
      throw new MirrorLogicException(String.format("invalid cached RegionStyle of %s on %s.%s",
                                                   cachedObj,
                                                   node,
                                                   regionStyleName),
                                     e);
    }
  }

  private void setCachedInstance(PersistentRecord node,
                                 String regionStyleName,
                                 Integer instanceId)
  {
    node.setCachedValue(getCacheKey(regionStyleName), instanceId);
  }

  private static final PrivateCacheKey CACHEKEY_STYLEID = new PrivateCacheKey("styleId");

  /**
   * Get the record maintained by the TableContentCompiler that represents the style that is
   * referenced by the given RegionStyle record.
   * 
   * @param regionStyle
   *          The regionStyle which is going to be resolved into the appropriate style record. Note
   *          that this used to be the Region record and has since been refactored.
   */
  protected final PersistentRecord getStyleRecord(PersistentRecord regionStyle,
                                                  Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    Integer styleId = (Integer) regionStyle.getCachedValue(CACHEKEY_STYLEID);
    if (styleId != null) {
      return getStyleTable().getRecord(styleId, viewpoint);
    }
    String compilerStyleName = regionStyle.opt(RegionStyle.COMPILERSTYLENAME);
    PersistentRecord styleRecord = getOptStyleRecord(compilerStyleName, viewpoint);
    if (styleRecord == null) {
      throw new MirrorNoSuchRecordException("Cannot resolve ContentCompilerStyle named " + getName()
                                            + "." + compilerStyleName + " in " + regionStyle,
                                            getStyleTable());
    }
    regionStyle.setCachedValue(CACHEKEY_STYLEID, Integer.valueOf(styleRecord.getId()));
    return styleRecord;
  }

  protected final PersistentRecord getOptStyleRecord(String compilerStyleName,
                                                     Viewpoint viewpoint)
  {
    return TableStringMatcher.optOnlyMatch(getStyleTable(),
                                           TableContentCompilerStyle.NAME,
                                           compilerStyleName,
                                           viewpoint);

  }

  public final PersistentRecord getStyle(String styleName)
      throws MirrorNoSuchRecordException
  {
    return TableStringMatcher.firstMatch(getStyleTable(),
                                         TableContentCompilerStyle.NAME,
                                         styleName);
  }

  /**
   * Initialise a new instance of the ContentCompiler. The core code will set up the values of
   * Node.ID and REGION_ID in a new PersistentRecord from the core table and then commit the record.
   * <p>
   * Please note that any additional functionality that is required to be implemented by subclasses
   * should be attached to the core table as either PreInstantiationTriggers or
   * PostInstantiationTriggers.
   * 
   * @param regionStyle
   *          The RegionStyle that this ContentCompiler is to be attached to.
   * @throws MirrorTransactionTimeoutException
   *           If the operation cannot complete due to a locking issue.
   * @throws MirrorFieldException
   *           Can be thrown for one of two reasons:-
   *           <OL>
   *           <LI>regionId is already associated with a record for this ContentCompiler. As such it
   *           is an illegal value to make a new record from. This could be viewed as a
   *           LogicException, but I'm delegating this to the calling class.
   *           <LI>regionId doesn't correspond to a valid record in the Region Table.
   *           </OL>
   * @throws MirrorInstantiationException
   *           If there are any triggers assigned to the core table that prevent the record being
   *           created.
   */
  @Override
  public final void initialiseInstance(PersistentRecord node,
                                       PersistentRecord regionStyle,
                                       Gettable inputs,
                                       Map<Object, Object> outputs,
                                       Transaction transaction)
      throws MirrorNoSuchRecordException, MirrorFieldException, MirrorInstantiationException,
      MirrorTransactionTimeoutException, FeedbackErrorException
  {
    inputs = GettableUtils.padNull(inputs);
    PersistentRecord style = getStyleRecord(regionStyle, null);
    TransientRecord instance = getInstanceTable().createTransientRecord();
    instance.setField(NODE_ID, Integer.valueOf(node.getId()));
    instance.setField(REGIONSTYLE_ID, Integer.valueOf(regionStyle.getId()));
    instance.setField(STYLE_ID, Integer.valueOf(style.getId()));
    instance.setField(ISDEFINED, style.opt(TableContentCompilerStyle.ISDEFINED));
    updateCoreFields(node, regionStyle, instance, style, transaction, inputs);
    updateTransientInstance(node, regionStyle, instance, style, transaction, inputs, outputs, null);
    PersistentRecord pInstance = instance.makePersistent(inputs);
    setCachedInstance(node, regionStyle.opt(RegionStyle.NAME), Integer.valueOf(pInstance.getId()));
    updatePersistentInstance(node,
                             regionStyle,
                             pInstance,
                             style,
                             transaction,
                             inputs,
                             outputs,
                             null);
  }

  /**
   * Prepare an instance of the TableContentCompiler for destruction. Basically just drops the
   * record (if existing) into the deleteTransaction. Most likely, it will already be there 'cos the
   * Region field will have been locked, thereby linking in this record, but better safe than
   * sorry...
   * <p>
   * Any additional work that should be done by the sub-class should be handled by ensuring that
   * there are the correct permissions on any links to or from external tables and the core table,
   * thereby getting them included in the deletion lock process automatically. Remember that the
   * core table record can be deleted automatically by the deletion of the node that owns it rather
   * than just through this method, so any side-effects that need handling as well should be fully
   * integrated with the mirror trigger system.
   */
  @Override
  public void destroyInstance(PersistentRecord node,
                              PersistentRecord regionStyle,
                              Transaction deleteTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
  }

  @Override
  public final void destroyInstance(PersistentRecord node,
                                    PersistentRecord regionStyle)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
    Transaction transaction = getDb().createTransaction(Db.DEFAULT_TIMEOUT,
                                                        Transaction.TRANSACTION_DELETE,
                                                        _transactionPassword,
                                                        "ContentCompiler.destroyInstance(...)");
    destroyInstance(node, regionStyle, transaction);
    transaction.commit();
  }

  /**
   * This actually does nothing because mirror will have taken care of locking the appropriate
   * instance record so this would just be duplicating the efforts. It is made non-final so that
   * sub-classes which have some other edit requirents can override it (although they may well be
   * advised to perform the work as a mirror trigger so that they maintain the same operations when
   * invoked from a mirror level as well as through a node level.
   */
  @Override
  public void editInstance(PersistentRecord node,
                           PersistentRecord regionStyle,
                           Transaction editTransaction)
      throws MirrorTransactionTimeoutException, MirrorNoSuchRecordException
  {
  }

  protected static void updateIsDefined(Gettable params,
                                        PersistentRecord regionStyle,
                                        PersistentRecord style,
                                        TransientRecord instance)
  {
    if (style.is(TableContentCompilerStyle.ISOPTIONAL)) {
      try {
        instance.setFieldIfDefined(ISDEFINED, params.getBoolean(ISDEFINED));
      } catch (MirrorFieldException oddFieldEx) {
        // shouldn't get a boolean value rejected as we've already
        // checked that it is optional...
        throw new LogicException("Unexpected MirrorFieldException when setting the isPublished field",
                                 oddFieldEx);
      }
    }
  }

  private final boolean updateCoreFields(PersistentRecord node,
                                         PersistentRecord regionStyle,
                                         TransientRecord instance,
                                         PersistentRecord style,
                                         Transaction editTransaction,
                                         Gettable params)
      throws MirrorFieldException, MirrorTransactionTimeoutException, MirrorInstantiationException
  {
    updateIsDefined(params, regionStyle, style, instance);
    if (_strings != null) {
      uploadStringFields(params, instance, regionStyle, _strings.iterator(), _isSafeHtml);
    }
    if (_booleans != null) {
      for (FieldKey<Boolean> fieldName : _booleans) {
        uploadBooleanField(params, instance, regionStyle, fieldName);
      }
    }
    if (_fastDates != null) {
      uploadFastDateFields(params, instance, regionStyle, _fastDates.iterator(), _calendar);
    }
    return instance.getIsUpdated();
  }

  @Override
  public final boolean updateInstance(PersistentRecord node,
                                      PersistentRecord regionStyle,
                                      Transaction editTransaction,
                                      Gettable inputs,
                                      Map<Object, Object> outputs,
                                      PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, FeedbackErrorException
  {
    PersistentRecord pInstance = getInstance(node, regionStyle, inputs, editTransaction);
    WritableRecord instance = null;
    try {
      instance = (WritableRecord) pInstance;
    } catch (ClassCastException ccEx) {
      System.err.println("*** node ***");
      System.err.println(node.debug());
      System.err.println("*** pInstance ***");
      System.err.println(pInstance.debug());
      System.err.println("*** instanceQuery ***");
      Query query = getInstanceQuery(node, regionStyle);
      System.err.println(query);
      System.err.println(query.getCacheKey());
      System.err.println(query.getIdList());
      System.err.println(query.getRecordList());
      System.err.println("query.getUpdateTime: " + query.getLastBuildTime());
      System.err.println(query.getRecordSources());
      for (RecordSource recordSource : query.getRecordSources()) {
        System.err.println(recordSource + " --> " + recordSource.getLastChangeTime());
      }
      throw new LogicException(node + " resolves to " + pInstance + " which is not owned by "
                               + editTransaction.debug() + " and therefore cannot be updated:"
                               + getInstanceTable().debugRecordStatus(pInstance.getId()),
                               ccEx);
    }
    boolean isUpdated = false;
    PersistentRecord style = instance.comp(STYLE);
    isUpdated = updateCoreFields(node, regionStyle, instance, style, editTransaction, inputs);
    isUpdated = isUpdated | updateTransientInstance(node,
                                                    regionStyle,
                                                    instance,
                                                    style,
                                                    editTransaction,
                                                    inputs,
                                                    outputs,
                                                    user);
    isUpdated = isUpdated | updatePersistentInstance(node,
                                                     regionStyle,
                                                     instance,
                                                     style,
                                                     editTransaction,
                                                     inputs,
                                                     outputs,
                                                     user);
    return isUpdated;
  }

  /**
   * Perform any implementation specifc value updating of the ContentCompiler. Note that this will
   * be called after the ISDEFINED field has been updated, and any default String, Boolean and
   * FastDate fields (as declared in the constructor) have been updated.
   * 
   * @return Flag to state whether or not the values associated with the Node have been updated.
   * @throws FeedbackErrorException
   */
  protected boolean updateTransientInstance(PersistentRecord node,
                                            PersistentRecord regionStyle,
                                            TransientRecord instance,
                                            PersistentRecord style,
                                            Transaction transaction,
                                            Gettable inputs,
                                            Map<Object, Object> outputs,
                                            PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      FeedbackErrorException
  {
    return false;
  }

  /**
   * Perform any updates contained in the Gettable params to an instance that has a backend database
   * record. This will be invoked by both
   * {@link #initialiseInstance(PersistentRecord, PersistentRecord, Gettable, Map, Transaction)}
   * after the instance has been committed to the database and by
   * {@link #updateInstance(PersistentRecord, PersistentRecord, Transaction, Gettable, Map, PersistentRecord)}
   * when the record is being updated during an edit session. If your implementation requires you to
   * make changes to the contents of the record, then be warned that it will not be locked by
   * transaction by default during initialisation so you should request a WritableRecord version of
   * instance before setting the fields. It will already be locked for editing during an update
   * session so this will have no effect.
   */
  protected boolean updatePersistentInstance(PersistentRecord node,
                                             PersistentRecord regionStyle,
                                             PersistentRecord instance,
                                             PersistentRecord style,
                                             Transaction transaction,
                                             Gettable inputs,
                                             Map<Object, Object> outputs,
                                             PersistentRecord user)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    return false;
  }

  @Override
  public Gettable compile(PersistentRecord node,
                          PersistentRecord regionStyle,
                          NodeRequest nodeRequest,
                          Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return getInstance(node, regionStyle, nodeRequest, viewpoint);
  }

  @Override
  public Gettable compileOpt(PersistentRecord node,
                             PersistentRecord regionStyle,
                             NodeRequest nodeRequest,
                             Viewpoint viewpoint)
  {
    return getOptInstance(node, regionStyle, nodeRequest, viewpoint);
  }

  /**
   * @return DbInitialisers containing the TableContentCompilerDb and the StyleTableWrapper that it
   *         contains. If subclasses override this method then they should include the contents of
   *         this DbInitialisers inside their definitions to ensure that the appropriate elements
   *         are included. This is most easily done with the linked methods below.
   * @see #getDefaultDbInitialisers()
   * @see #getDbInitialisers(Collection)
   */
  @Override
  public DbInitialisers getDbInitialisers()
  {
    final List<DbInitialiser> ldi = getDefaultDbInitialisers();
    return getDbInitialisers(ldi);
  }

  /**
   * Get a List of the default DbInitialiser values that a TableContent needs to function. This can
   * be used when overriding getDbInitialisers() as a starting point before adding new values and
   * then invoking getDbInitialisers(ldi)
   * 
   * @return new mutable List containing the instance table wrapper and the style table wrapper
   * @see #getDbInitialisers(Collection)
   */
  protected final List<DbInitialiser> getDefaultDbInitialisers()
  {
    final List<DbInitialiser> ldi = new ArrayList<DbInitialiser>();
    ldi.add(getTableContentCompilerDb().getStyleTableWrapper());
    ldi.add(getTableContentCompilerDb());
    return ldi;
  }

  /**
   * Build a DbInitialisers implementation out of a List<Dbinitialiser> which may be used for
   * overriding getDbInitialisers()
   * 
   * @see #getDbInitialisers()
   */
  protected final DbInitialisers getDbInitialisers(final Collection<DbInitialiser> ldi)
  {
    return new DbInitialisers() {
      @Override
      public Iterator<DbInitialiser> getDbInitialisers()
      {
        return ldi.iterator();
      }
    };
  }

  /**
   * Get the associated DbInitialiser that is used to construct the mirror components of this
   * ContentCompiler. This is functionally equivalent to getDbInitialiser, but provides a more
   * accurate class for the returned value.
   * 
   * @return The TableContentCompilerDb as passed to the constructor.
   */
  public final TableContentCompilerDb getTableContentCompilerDb()
  {
    return _tableContentCompilerDb;
  }

  /**
   * TableContentCompiler does not currently support style migration and will therefore reject all
   * requests to perform the operation.
   * 
   * @throws MirrorFieldException
   *           always!
   */
  @Override
  public final void updateStyle(TransientRecord regionStyle)
      throws MirrorFieldException
  {
    throw new MirrorFieldException("TableContentCompiler does not support style migration",
                                   null,
                                   regionStyle,
                                   RegionStyle.COMPILERSTYLENAME,
                                   regionStyle.opt(RegionStyle.COMPILERSTYLENAME),
                                   null);
  }

  public final <T> void linkInstanceToNode(FieldKey<T> instanceFieldName,
                                           FieldKey<T> nodeFieldName)
      throws MirrorTableLockedException
  {
    getInstanceTable().addFieldTrigger(new InstanceToNodeTrigger<T>(instanceFieldName,
                                                                    nodeFieldName));
    getInstanceTable().addPreInstantiationTrigger(new NodeToInstanceBooter<T>(nodeFieldName,
                                                                              instanceFieldName));
  }

  public final <T> void linkNodeToInstance(int nodeStyleId,
                                           String regionStyleName,
                                           FieldKey<T> nodeFieldName,
                                           FieldKey<T> instanceFieldName)
      throws MirrorTableLockedException
  {
    getNodeManager().getDb()
                    .getTable(Node.TABLENAME)
                    .addFieldTrigger(new NodeToInstanceTrigger<T>(nodeStyleId,
                                                                  regionStyleName,
                                                                  nodeFieldName,
                                                                  instanceFieldName));
    getInstanceTable().addPreInstantiationTrigger(new NodeToInstanceBooter<T>(nodeFieldName,
                                                                              instanceFieldName));
  }

  public final <T> void linkNodeToInstance(PersistentRecord regionStyle,
                                           FieldKey<T> nodeFieldName,
                                           FieldKey<T> instanceFieldName)
      throws MirrorTableLockedException
  {
    linkNodeToInstance(regionStyle.comp(RegionStyle.NODESTYLE).getId(),
                       regionStyle.opt(RegionStyle.NAME),
                       nodeFieldName,
                       instanceFieldName);
  }

  protected class NodeToInstanceBooter<T>
    extends PreInstantiationTriggerImpl
  {
    private final FieldKey<T> __instanceFieldName;

    private final FieldKey<T> __nodeFieldName;

    private NodeToInstanceBooter(FieldKey<T> nodeFieldName,
                                 FieldKey<T> instanceFieldName)
    {
      __instanceFieldName = instanceFieldName;
      __nodeFieldName = nodeFieldName;
    }

    @Override
    public void triggerPre(TransientRecord instance,
                           Gettable params)
        throws MirrorNoSuchRecordException, MirrorFieldException
    {
      instance.setField(__instanceFieldName, getNode(instance).opt(__nodeFieldName));
    }

    @Override
    public String toString()
    {
      return ("NodeToInstanceBooter(Node." + __nodeFieldName + " --> instance."
              + __instanceFieldName + ")");
    }
  }

  protected class NodeToInstanceTrigger<T>
    extends AbstractFieldTrigger<T>
  {
    private final int __nodeStyleId;

    private final String __regionStyleName;

    private final FieldKey<T> __instanceFieldName;

    public NodeToInstanceTrigger(int nodeStyleId,
                                 String regionStyleName,
                                 FieldKey<T> nodeFieldName,
                                 FieldKey<T> instanceFieldName)
    {
      super(nodeFieldName,
            false,
            false);
      Preconditions.checkNotNull(instanceFieldName, "instanceFieldName");
      Preconditions.checkNotNull(regionStyleName, "regionStyleName");
      __nodeStyleId = nodeStyleId;
      __regionStyleName = regionStyleName;
      __instanceFieldName = instanceFieldName;
    }

    @Override
    public Object triggerField(TransientRecord node,
                               String fieldName,
                               Transaction transaction)
        throws MirrorFieldException
    {
      PersistentRecord nodeStyle = node.comp(Node.NODESTYLE);
      if (nodeStyle.getId() == __nodeStyleId) {
        try {
          WritableRecord instance = (WritableRecord) getInstance((WritableRecord) node,
                                                                 __regionStyleName,
                                                                 null,
                                                                 transaction);
          instance.setField(__instanceFieldName, node.get(getFieldName().toString()));
        } catch (ClassCastException ccEx) {
          // the node wasn't locked!!!
          DebugConstants.method(this, "triggerField", node, transaction);
          System.err.println(transaction.debug());
          throw new LogicException("Invalid object lock on " + node, ccEx);
        } catch (MirrorNoSuchRecordException mnsrEx) {
          throw new MirrorFieldException("Error resolving AbstractContentCompilerFieldLinker",
                                         mnsrEx,
                                         node,
                                         getName(),
                                         node.get(getFieldName().toString()),
                                         null);
        }
      }
      return null;
    }
  }

  protected class InstanceToNodeTrigger<T>
    extends AbstractFieldTrigger<T>
  {
    private final FieldKey<T> __nodeFieldName;

    public InstanceToNodeTrigger(FieldKey<T> instanceFieldName,
                                 FieldKey<T> nodeFieldName)
    {
      super(instanceFieldName,
            false,
            false);
      Preconditions.checkNotNull(nodeFieldName, "nodeFieldName");
      __nodeFieldName = nodeFieldName;
    }

    @Override
    public Object triggerField(TransientRecord instance,
                               String fieldName,
                               Transaction transaction)
        throws MirrorFieldException
    {
      PersistentRecord node = getNode(instance);
      node.setField(__nodeFieldName, instance.opt(getFieldName()));
      return null;
    }
  }

  /**
   * Construct a Query that selects a set of Instance records for this TableContentCompiler such
   * that all of the referenced Nodes are included. Any Node ids that do not have at least one
   * instance of this ContentCompiler registered will be discarded. Nodes that include multiple
   * copies will get all versions.
   * 
   * @param nodeIds
   *          The Node ids that are going to be resolved into instance ids. The ordering of the ids
   *          is not important.
   * @param orderBy
   *          The ordering clause with respect to the InstanceTable.
   * @deprecated This will change to returning a RecordSource, but we can't do that yet without
   *             breaking things until QueryListingFactory is tidied up.
   * @see QueryListingFactory
   */
  @Deprecated
  public final Query getInstancesFromNodes(IdList nodeIds,
                                           String orderBy)
  {
    Table instances = getInstanceTable();
    switch (nodeIds.size()) {
      case 0:
        return instances.getQuery(null, "(id < 0)", null);
      default:
        StringBuilder filter = new StringBuilder();
        filter.append('(')
              .append(instances.getName())
              .append(".node_id=Node.id) and (Node.id in (");
        IntIterator i = nodeIds.iterator();
        filter.append(i.next());
        while (i.hasNext()) {
          filter.append(",").append(i.next());
        }
        filter.append("))");
        return instances.getQuery(null,
                                  filter.toString(),
                                  orderBy,
                                  getNodeManager().getNode().getTable());
    }
  }

  /**
   * @param nodeStyle
   *          The NodeStyle which it will be appropriate to attach this question to. You will
   *          generally be invoking this from the internalDeclare method which means that you can
   *          utilise the passed regionStyle to get the nodeStyle that is appropriate. If null then
   *          it will be registered across all NodeStyles.
   * @param askQuestionName
   *          The unique name that this question will be known by. It will automatically be extended
   *          with the id of the Node that it is being asked against, so it only needs to be unique
   *          with respect to other potential questions on the same Node(Style)
   * @param askQuestionUrl
   *          The URL that the client will be redirected to on opening this question. Will generally
   *          be a suitable place to go looking for the answer to the question such as a listing or
   *          search page.
   * @param questionEnglishFactory
   *          The ValueFactory which when invoked on the page that the question is being asked from
   *          will give the text of the question. If you want to include the name of the page that
   *          is asking the question then you probably want a NodeFieldValueFactory.
   * @param maxAnswers
   *          1 if it is a single target link, more than 1 if it has a fixed maximum number of
   *          answers and -1 if there can be as many answers as you like.
   * @param eligibleNodeFilter
   *          NodeFilter that only lets past Nodes that are suitable answers to this question. These
   *          Nodes will be given a suitable link to select them for open questions, whereas other
   *          non-suitable Nodes will just have a reminder that the question is open.
   * @param targetContentCompiler
   *          The TableContentCompiler registered on the target node that will be used to resolve
   *          targetName and therefore work out the id that will be used as the answer to this
   *          question. If this is null then the targetNode itself is assumed to be the answer to
   *          the question.
   * @param targetName
   *          The registration name of the targetContentCompiler on the targetNode. If the
   *          targetContentCompiler is null then this variable is ignored.
   * @param linkFieldName
   *          The name of the parameter on the node that instigated the question that the answers
   *          will be uploaded to. If this is an TableContentCompiler field then you will need to
   *          include the appropriate namespaces (ie "regionStyleName-fieldName")
   * @param thisName
   *          The name that the TableContentCompiler is registered on the invoking node as.
   * @param linkRecordsName
   *          The name of the operation to get the link records if this is a many-many reference
   *          operation.
   * @param linkTargetName
   *          The name of the operation on the link record to get to the answer record.
   * @throws MirrorNoSuchRecordException
   * @see #internalDeclare(PersistentRecord)
   * @see org.cord.node.NodeFieldValueFactory
   */
  protected void registerLinkQuestion(final PersistentRecord nodeStyle,
                                      final String askQuestionName,
                                      ValueFactory askQuestionUrl,
                                      final ValueFactory questionEnglishFactory,
                                      final int maxAnswers,
                                      final NodeFilter eligibleNodeFilter,
                                      final TableContentCompiler targetContentCompiler,
                                      final String targetName,
                                      final String linkFieldName,
                                      final String thisName,
                                      final RecordOperationKey<RecordSource> linkRecordsName,
                                      final RecordOperationKey<PersistentRecord> linkTargetName)
      throws MirrorNoSuchRecordException
  {
    final NodeManager nodeManager = getNodeManager();
    AskNodeQuestion askNodeQuestion =
        new AskNodeQuestion(askQuestionName,
                            nodeManager,
                            new RedirectionSuccessOperation(nodeManager, askQuestionUrl),
                            Node.EDITACL,
                            "You do not have permission to ask this question") {
          @Override
          protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                    final PersistentRecord node,
                                                    Context context)
              throws MirrorNoSuchRecordException
          {
            Transaction transaction = getTransaction(nodeRequest, node);
            NodeQuestion question =
                new NodeQuestion(askQuestionName + node.getId(),
                                 questionEnglishFactory.getValue(nodeManager, nodeRequest),
                                 null,
                                 node,
                                 maxAnswers,
                                 eligibleNodeFilter,
                                 transaction) {
                  @Override
                  public boolean maySubmit()
                  {
                    switch (getMaxAnswers()) {
                      case -1:
                        return true;
                      default:
                        return getSelectedNodeIds().size() == getMaxAnswers();
                    }
                  }

                  @Override
                  public boolean shouldSubmit()
                  {
                    switch (getMaxAnswers()) {
                      case 1:
                        return getSelectedNodeIds().size() == 1;
                      default:
                        return false;
                    }
                  }

                  @Override
                  public String getAnswerUrl()
                  {
                    StringBuilder url = new StringBuilder();
                    url.append(node.opt(Node.URL));
                    if (maySubmit()) {
                      url.append("?view=update");
                      if (getMaxAnswers() != 1) {
                        url.append('&').append(linkFieldName).append("-active=true");
                      }
                      PersistentRecord targetNode = null;
                      Iterator<PersistentRecord> selectedNodes = getSelectedNodes();
                      while (selectedNodes.hasNext()) {
                        try {
                          targetNode = selectedNodes.next();
                          PersistentRecord target = targetContentCompiler == null
                              ? targetNode
                              : targetContentCompiler.getInstance(targetNode,
                                                                  targetName,
                                                                  null,
                                                                  null);
                          url.append('&').append(linkFieldName).append('=').append(target.getId());
                        } catch (MirrorNoSuchRecordException mnsrEx) {
                          throw new LogicException(this + " cannot resolve target on  "
                                                   + targetNode,
                                                   mnsrEx);
                        }
                      }
                    }
                    return url.toString();
                  }
                };
            if (question.getMaxAnswers() != 1) {
              try {
                PersistentRecord instance =
                    TableContentCompiler.this.getInstance(node, thisName, null, transaction);
                for (PersistentRecord linkRecord : instance.opt(linkRecordsName, transaction)) {
                  if (linkRecord.is(ISDEFINED)) {
                    question.addAnswer(linkRecord.comp(linkTargetName).comp(NODE));
                  }
                }
              } catch (MirrorNoSuchRecordException mnsrEx) {
                throw new LogicException("Lost " + thisName + " record on " + node, mnsrEx);
              }
            }
            return question;
          }
        };
    nodeManager.getNodeRequestSegmentManager()
               .register(nodeStyle == null
                   ? NodeRequestSegmentManager.NODESTYLEID_GLOBAL
                   : nodeStyle.getId(), Transaction.TRANSACTION_UPDATE, askNodeQuestion);
  }

  protected final void registerLinkQuestion(PersistentRecord regionStyle,
                                            IntFieldKey accLinkFieldName)
      throws MirrorNoSuchRecordException
  {
    ((AccLinkField) getInstanceTable().getField(accLinkFieldName)).registerLinkQuestion(regionStyle);
  }

  /**
   * Put an Object into the outputs under a String key if all values are defined.
   * 
   * @return true if key=value in outputs, false otherwise.
   */
  protected boolean out(Map<Object, Object> outputs,
                        String key,
                        Object value)
  {
    if (outputs != null & key != null & value != null) {
      outputs.put(key, value);
      return true;
    }
    return false;
  }

  /**
   * @deprecated in preference of
   *             {@link #uploadManyToManyReference(Gettable, PersistentRecord, Transaction, Table, Table, String, String, GettableRecordProcessor, IntFieldKey, IntFieldKey)}
   *             which doesn't require the regionStyle which is not used.
   */
  @Deprecated
  public static boolean uploadManyToManyReference(Gettable params,
                                                  PersistentRecord instance,
                                                  PersistentRecord regionStyle,
                                                  Transaction transaction,
                                                  Table linkTable,
                                                  Table targetTable,
                                                  String cgiTargetIdDefined,
                                                  String cgiTargetId,
                                                  GettableRecordProcessor initialiser,
                                                  IntFieldKey instanceLinkName,
                                                  IntFieldKey targetLinkName)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    return uploadManyToManyReference(params,
                                     instance,
                                     transaction,
                                     linkTable,
                                     targetTable,
                                     cgiTargetIdDefined,
                                     cgiTargetId,
                                     initialiser,
                                     instanceLinkName,
                                     targetLinkName);
  }

  /**
   * @param cgiTargetIdDefined
   *          The name of the boolean parameter that must be present in the NodeRequest for the
   *          upload to take place. If it is not found then the action will do nothing unless
   *          cgiTargetId is a defined key in params.
   * @param cgiTargetId
   *          The name of the select box that contains the ids that is going to be uploaded.
   * @param initialiser
   *          the optional GettableRecordProcessor that permits you to attach meta-data to join
   *          records
   * @param instanceLinkName
   *          The name of the field that contains the reference to the instance in the join table. A
   *          value of null will be autopadded with the default link name.
   * @param targetLinkName
   *          The name of the field that contains the reference to the target record in the join
   *          table. A value of null will be auto-padded with the default link name for the table.
   */
  public static boolean uploadManyToManyReference(Gettable params,
                                                  PersistentRecord instance,
                                                  Transaction transaction,
                                                  Table linkTable,
                                                  Table targetTable,
                                                  String cgiTargetIdDefined,
                                                  String cgiTargetId,
                                                  GettableRecordProcessor initialiser,
                                                  IntFieldKey instanceLinkName,
                                                  IntFieldKey targetLinkName)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    boolean isUpdated = false;
    if (Boolean.TRUE.equals(params.getBoolean(cgiTargetIdDefined))
        | params.containsKey(cgiTargetId)) {
      Collection<?> c = params.getValues(cgiTargetId);
      Iterator<?> targetIds = c == null ? ImmutableSet.of().iterator() : c.iterator();
      try {
        isUpdated = isUpdated | ManyToManyReference.defineLinks(params,
                                                                linkTable,
                                                                instanceLinkName,
                                                                targetLinkName,
                                                                instance,
                                                                targetTable,
                                                                targetIds,
                                                                true,
                                                                transaction,
                                                                Db.DEFAULT_TIMEOUT,
                                                                null,
                                                                initialiser);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new LogicException("impossible exception with ignoreBadIds=true", mnsrEx);
      } catch (MirrorInstantiationException miEx) {
        throw new LogicException("unexpected InstantiationException when defining LinkRecord",
                                 miEx);
      }
    }
    return isUpdated;
  }

  /**
   * Resolve all the named tables and then delegate to the explicit uploadManyToManyReference.
   * 
   * @param cgiTargetIdDefined
   *          The name of the boolean parameter that must be present in the NodeRequest for the
   *          upload to take place. If it is not found then the action will do nothing unless
   *          cgiTargetId is a defined key in the params.
   * @param cgiTargetId
   *          The name of the select box that contains the ids that is going to be uploaded.
   * @param initialiser
   *          The optional processor that will handle additional meta-data on the link records if
   *          any
   * @param instanceLinkName
   *          The name of the field on the join record that points at the instance of the
   *          ContentCompiler
   * @param targetLinkName
   *          The name of the field on the join record that points at the target record.
   * @return true if the action has changed the state of the db.
   */
  public boolean uploadManyToManyReference(Gettable params,
                                           PersistentRecord instance,
                                           PersistentRecord regionStyle,
                                           Transaction transaction,
                                           String linkTable,
                                           String targetTable,
                                           String cgiTargetIdDefined,
                                           String cgiTargetId,
                                           GettableRecordProcessor initialiser,
                                           IntFieldKey instanceLinkName,
                                           IntFieldKey targetLinkName)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    return uploadManyToManyReference(params,
                                     instance,
                                     regionStyle,
                                     transaction,
                                     getDb().getTable(linkTable),
                                     getDb().getTable(targetTable),
                                     cgiTargetIdDefined,
                                     cgiTargetId,
                                     initialiser,
                                     instanceLinkName,
                                     targetLinkName);
  }

  public static boolean uploadManyToManyRecursiveReferences(Gettable params,
                                                            PersistentRecord instance,
                                                            PersistentRecord regionStyle,
                                                            Transaction transaction,
                                                            Table linkTable,
                                                            String cgiTargetIdDefined,
                                                            String cgiTargetId)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    return uploadManyToManyRecursiveReferences(params,
                                               instance,
                                               regionStyle,
                                               transaction,
                                               linkTable,
                                               null,
                                               null,
                                               null,
                                               cgiTargetIdDefined,
                                               cgiTargetId);
  }

  /**
   * @param instance
   *          The record which is being linked from by the Many:Many recursive reference. With an
   *          TableContentCompiler this would be the instance record, but it can be utilised for
   *          other purposes as well.
   * @return True if the state of the data has changed, false if there is no change
   */
  public static boolean uploadManyToManyRecursiveReferences(Gettable params,
                                                            PersistentRecord instance,
                                                            PersistentRecord regionStyle,
                                                            Transaction transaction,
                                                            Table linkTable,
                                                            IntFieldKey sourceLinkName,
                                                            IntFieldKey targetLinkName,
                                                            FieldKey<Boolean> isDefinedName,
                                                            String cgiTargetIdDefined,
                                                            String cgiTargetId)
      throws MirrorFieldException, MirrorTransactionTimeoutException
  {
    // TODO: (#268) set value of isUpdated in ManyToManyRecursive upload
    boolean isUpdated = false;
    if (Boolean.TRUE.equals(params.getBoolean(cgiTargetIdDefined))) {
      Collection<?> c = params.getValues(cgiTargetId);
      Iterator<?> targetIds = c == null ? ImmutableSet.of().iterator() : c.iterator();
      try {
        ManyToManyRecursiveReference.setIsDefined(linkTable,
                                                  sourceLinkName,
                                                  targetLinkName,
                                                  isDefinedName,
                                                  instance,
                                                  targetIds,
                                                  true,
                                                  transaction,
                                                  Db.DEFAULT_TIMEOUT,
                                                  null);
      } catch (MirrorInstantiationException miEx) {
        throw new LogicException("Unable to create link record", miEx);
      } catch (MirrorNoSuchRecordException mnsrEx) {
        throw new LogicException("impossible bad id exception when ignoreBadIds == true", mnsrEx);
      }
    }
    return isUpdated;
  }

  /**
   * @param calendar
   *          The Calendar Object that is to be utilised for parsing the date information that is
   *          uploaded. This will be synchronized on during parsing to preserve thread safety, so it
   *          is recommended that you utilise a reference to a Calendar Object that is unlikely to
   *          be contested. Probably good value is a single value of Calendar for each instance of
   *          TableContentCompiler that exists.
   */
  public static void uploadFastDateFields(Gettable params,
                                          TransientRecord instance,
                                          PersistentRecord regionStyle,
                                          Iterator<FieldKey<Date>> fieldNames,
                                          Calendar calendar)
      throws MirrorFieldException
  {
    while (fieldNames.hasNext()) {
      ContentCompiler.uploadFastDateField(params,
                                          instance,
                                          regionStyle,
                                          fieldNames.next(),
                                          calendar);
    }
  }

  public static Object uploadBooleanField(Gettable gettable,
                                          TransientRecord instance,
                                          PersistentRecord regionStyle,
                                          FieldKey<Boolean> fieldKeyBoolean)
      throws MirrorFieldException
  {
    Boolean value = gettable.getBoolean(fieldKeyBoolean.getKeyName());
    if (value == null) {
      if (instance.getTable().getField(fieldKeyBoolean).getSupportsNull()
          & gettable.containsKey(fieldKeyBoolean.getKeyName())) {
        return instance.setField(fieldKeyBoolean, (Object) null);
      }
      return instance.opt(fieldKeyBoolean);
    } else {
      return instance.setField(fieldKeyBoolean, value);
    }
  }

  public static void uploadStringFields(Gettable params,
                                        TransientRecord instance,
                                        PersistentRecord regionStyle,
                                        Iterator<FieldKey<?>> fieldNames,
                                        boolean isSafeHtml)
      throws MirrorFieldException
  {
    while (fieldNames.hasNext()) {
      uploadStringField(params, instance, regionStyle, fieldNames.next(), isSafeHtml);
    }
  }

  /**
   * Attempt to update the value of a field in a record from the data contained in the Gettable. If
   * the value is not defined in the Gettable, then the value is unchanged in the record.
   * 
   * @param regionStyle
   *          optional regionStyle. This is not currently being used and will be removed in future.
   * @param convertToSafeHtml
   *          If true and the value in the params is a String then &lt; & &gt; will be automatically
   *          escaped in the uploaded value.
   * @return The value of the Object that is stored in the record regardless of whether it has been
   *         changed or not.
   * @see StringUtils#noHtml(Object)
   */
  public static Object uploadStringField(Gettable gettable,
                                         TransientRecord instance,
                                         PersistentRecord regionStyle,
                                         FieldKey<?> fieldName,
                                         boolean convertToSafeHtml)
      throws MirrorFieldException
  {
    Object value = gettable.get(fieldName.getKeyName());
    if (value == null) {
      if (instance.getTable().getField(fieldName).getSupportsNull()
          & gettable.containsKey(fieldName.getKeyName())) {
        return instance.setField(fieldName, (Object) null);
      }
      return instance.opt(fieldName);
    } else {
      if (convertToSafeHtml && value instanceof String) {
        value = StringUtils.noHtml(value);
      }
      return instance.setField(fieldName, value);
    }
  }

  public static Object uploadBitLinkField(Gettable params,
                                          TransientRecord instance,
                                          PersistentRecord regionStyle,
                                          String fieldName)
      throws MirrorFieldException
  {
    Collection<?> c = params.getValues(fieldName);
    if (c != null) {
      Iterator<?> values = c.iterator();
      long fieldValue = 0;
      while (values.hasNext()) {
        String value = (String) values.next();
        if (value != null) {
          try {
            fieldValue = BitLinkField.setFlag(fieldValue, Integer.parseInt(value), true);
          } catch (NumberFormatException notAnIntegerParamEx) {
            // ignore...
          }
        }
      }
      return instance.setField(fieldName, new Long(fieldValue));
    }
    return null;
  }
}
