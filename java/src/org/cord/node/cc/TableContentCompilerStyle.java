package org.cord.node.cc;

import org.cord.mirror.FieldKey;
import org.cord.mirror.ObjectFieldKey;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.field.BooleanField;
import org.cord.node.NodeDbConstants;

public class TableContentCompilerStyle
{
  /**
   * "name"
   * 
   * @see org.cord.node.NodeDbConstants#NAME
   */
  public final static FieldKey<String> NAME = NodeDbConstants.NAME.copy();

  /**
   * "instances"
   */
  public final static String INSTANCES = "instances";

  /**
   * "isOptional"
   */
  public final static ObjectFieldKey<Boolean> ISOPTIONAL = BooleanField.createKey("isOptional");

  /**
   * "isDefined"
   */
  public final static ObjectFieldKey<Boolean> ISDEFINED = BooleanField.createKey("isDefined");

  /**
   * "named"
   */
  public final static String NAMED = "named";

  /**
   * RecordOperation ("isPossible") that returns whether or not it is possible to create an instance
   * based on this Style definition.
   */
  public final static RecordOperationKey<Boolean> ISPOSSIBLE =
      RecordOperationKey.create("isPossible", Boolean.class);

  private TableContentCompilerStyle()
  {
  }
}