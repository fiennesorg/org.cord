package org.cord.node;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.Named;

/**
 * The ContestedLockFilter interface describes classes that are capable of making decisions about
 * which User should maintain a lock on a Node in a contested situation.
 * 
 * @see ContestedLockFilterManager
 */
public interface ContestedLockFilter
  extends Named
{
  /**
   * Result that says that the requesting User should definately grab the lock from the locked User.
   * Processing should stop at this point.
   */
  public final static int STEAL_LOCK = 1;

  /**
   * Result that says that there is no definable result from this ContestedLockFilter and therefore
   * the decision is delegated to the next filter in the list. This should be the default behaviour
   * if return values are received that are not defined by the system.
   */
  public final static int UNDECIDED = 0;

  /**
   * Result that says that there is no way that the requesting User should be permitted to steal the
   * lock from the locked User. Processing should stop at this point.
   */
  public final static int MAINTAIN_LOCK = -1;

  /**
   * Make a decision as to the outcome of the given contested lock situation. Please note that this
   * operation should <i>not</i> have any side-effects, it is purely to advise a 3rd party as to
   * what operation is preferred. It should especially not commit or cancel the lockedTransaction .
   * 
   * @param contestedRequest
   *          The NodeRequest that is being made by the requesting User.
   * @param contestedRecord
   *          The PersistentRecord that is currently being contested. This will either be a Node or
   *          a NodeGroup record and it will currently be locked by lockedTransaction under the
   *          lockedUser.
   * @param lockedUser
   *          The User that currently has the lock on the contested node.
   * @param lockedTransaction
   *          The Transaction that relates is owned by the lockedUser. This may be used to determine
   *          the style and scope of the lock and the repurcussions of opting to abort the
   *          Transaction.
   * @return MAINTAIN_LOCK, STEAL_LOCK or UNDECIDED.
   * @see #MAINTAIN_LOCK
   * @see #STEAL_LOCK
   * @see #UNDECIDED
   * @see NodeRequest#getTargetLeafNode()
   */
  public int resolveContestedLock(NodeRequest contestedRequest,
                                  PersistentRecord contestedRecord,
                                  PersistentRecord lockedUser,
                                  Transaction lockedTransaction);
}
