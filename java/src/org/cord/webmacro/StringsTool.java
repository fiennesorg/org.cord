package org.cord.webmacro;

import org.cord.util.StringUtils;
import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

/**
 * ContextTool that drops the singleton instance of StringUtils into the Context.
 * 
 * @author alex
 * @see org.cord.util.StringUtils
 * @see org.cord.util.StringUtils#getInstance()
 */
public class StringsTool
  extends ContextTool
{

  /**
   * @return an instance of StringUtils
   * @see StringUtils#getInstance()
   */
  @Override
  public Object init(Context arg0)
      throws PropertyException
  {
    return StringUtils.getInstance();
  }
}
