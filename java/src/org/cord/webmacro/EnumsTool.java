package org.cord.webmacro;

import java.util.List;
import java.util.Map;

import org.cord.util.Maps;
import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

import com.google.common.collect.ImmutableList;

/**
 * ContextTool for manipulating Enum objects.
 * 
 * @author alex
 */
public class EnumsTool
  extends ContextTool
{
  private final static EnumsTool INSTANCE = new EnumsTool();

  @Override
  public Object init(Context c)
      throws PropertyException
  {
    return INSTANCE;
  }

  private Map<String, List<Object>> __values = Maps.newConcurrentHashMap();

  /**
   * Take the fully qualified classname of an Enum and translate it into a List of the values in the
   * Enum. Note that if you are trying to pass in an inner class then you will probably have to
   * escape the $ to avoid webmacro trying to resolve it and getting very weird errors.
   */
  public List<?> getValues(String qualifiedName)
      throws ClassNotFoundException
  {
    List<Object> cached = __values.get(qualifiedName);
    if (cached == null) {
      Object[] values = Class.forName(qualifiedName).getEnumConstants();
      if (values == null) {
        throw new IllegalArgumentException(String.format("%s does not resolve to an Enum",
                                                         qualifiedName));
      }
      cached = ImmutableList.copyOf(values);
      __values.put(qualifiedName, cached);
    }
    return cached;
  }
}
