package org.cord.webmacro;

import org.cord.util.NumberUtil;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.broker.ContextObjectFactory;

public class NumbersTool
  implements ContextObjectFactory
{

  @Override
  public Object get(Context c)
      throws PropertyException
  {
    return NumberUtil.getInstance();
  }
}
