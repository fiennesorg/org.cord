package org.cord.webmacro;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.cord.mirror.RecordSource;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.broker.ContextObjectFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public class ListTool
  implements ContextObjectFactory
{
  // ==========================================================================
  // ContextObjectFactory methods

  @Override
  public Object get(Context c)
      throws PropertyException
  {
    return this;
  }

  // ==========================================================================
  // Webmacro-accessible methods

  /**
   * @return the number of objects in obj; if obj is not a container of objects, returns
   *         {@link #SIZE_NOTACONTAINER}
   */
  public static int size(Object obj)
  {
    if (obj instanceof Collection)
      return ((Collection<?>) obj).size();
    if (obj instanceof Object[])
      return ((Object[]) obj).length;
    if (obj instanceof RecordSource)
      return ((RecordSource) obj).getIdList().size();
    if (obj.getClass().isArray())
      return java.lang.reflect.Array.getLength(obj);
    return SIZE_NOTACONTAINER;
  }

  public final static int SIZE_NOTACONTAINER = -1;

  /**
   * @return whether obj is empty. If obj is iterable, an iterator, or an enumeration, returns
   *         whether iteration can continue. If obj is null, returns true.
   */
  public static boolean isEmpty(Object obj)
  {
    if (obj == null)
      return true;
    if (obj instanceof Iterator)
      return !((Iterator<?>) obj).hasNext();
    if (obj instanceof Iterable)
      return !((Iterable<?>) obj).iterator().hasNext();
    if (obj instanceof Enumeration)
      return !((Enumeration<?>) obj).hasMoreElements();
    return size(obj) != 0;
  }

  /**
   * provide a reversed view of the given List.
   * 
   * @see Lists#reverse(List)
   */
  public static <T> List<T> reverse(List<T> list)
  {
    return Lists.reverse(list);
  }
  
  public static String join(List<?> list, String separator)
  {
    return Joiner.on(separator).skipNulls().join(list);
  }
  
  public static String join(List<?> list)
  {
    return join(list, ", ");
  }
}
