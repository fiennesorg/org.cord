package org.cord.webmacro;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

/**
 * Context tool that either resolves to the Context itself if you are in the global Context space or
 * to the OuterVars, ie the parent Context if you are in a sub-context. This means that you can
 * safely invoke $Global.name without worrying as to whether your current context is a nested
 * evaluation or not.
 * 
 * @author alex
 */
public class GlobalTool
  extends ContextTool
{
  public static final String OUTERVARS = VarsTool.OUTERVARS;

  @Override
  public Object init(Context context)
      throws PropertyException
  {
    if (context.containsKey(OUTERVARS)) {
      return context.get(OUTERVARS);
    }
    return context;
  }
}
