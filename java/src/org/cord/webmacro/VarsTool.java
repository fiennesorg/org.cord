package org.cord.webmacro;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.operation.Opt;
import org.cord.node.IncludeList;
import org.cord.node.requires.Requirements;
import org.cord.util.Assertions;
import org.cord.util.DebugConstants;
import org.cord.util.ExceptionUtil;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.util.ObjectUtil;
import org.joda.time.DateTime;
import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.broker.ContextObjectFactory;
import org.webmacro.engine.Block;
import org.webmacro.engine.StringMacroAdapter;
import org.webmacro.engine.UndefinedMacro;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntLists;

/**
 * A Webmacro ContextTool that provides additional functionalities above and beyond the existing
 * VariableTool
 * 
 * @author alex
 */
public class VarsTool
  extends ContextTool
{
  private final Context __context;

  public VarsTool()
  {
    this(null);
  }

  /**
   * Get a Predicate that passes Optional values that are defined.
   */
  public static Predicate<Optional<?>> isPresent()
  {
    return new Predicate<Optional<?>>() {
      @Override
      public boolean apply(Optional<?> input)
      {
        return input.isPresent();
      }
    };
  }

  public VarsTool(Context context)
  {
    __context = context;
  }

  @Override
  public Object init(Context context)
      throws PropertyException
  {
    return new VarsTool(context);
  }

  /**
   * Get the Context that was used to initialise this instance of $Vars
   */
  public Context getContext()
  {
    return __context;
  }

  /**
   * Do nothing with the supplied Object. This is useful if you have a method that you want to
   * invoke and don't want to display the return value. Previously you would have had to assign it
   * to a temporary variable which clutters up the namespace of the Context, but now you can do
   * $Vars.discard($foo.bar())
   */
  public void discard(Object obj)
  {
  }

  /**
   * Get a shallow copy of the current Context containing the same values. This can then be used as
   * a starting point for params for inner template evaluation with the values being preserved from
   * template to template without the templates having to know which parameters are involved. This
   * will <em>not</em> copy any items in the current context that are instances of
   * ContextObjectFactory as otherwise you would end up with a sub-context that contained items that
   * where initialised against the this Context and all sorts of problems would occur.
   * 
   * @return a mutable Map containing the same value as the containing Context of this $Vars
   */
  public Map<Object, Object> copyContext()
  {
    Map<Object, Object> map = Maps.newHashMapWithExpectedSize(__context.size());
    for (Entry<Object, Object> entry : __context.entrySet()) {
      Object value = entry.getValue();
      if (!(value instanceof ContextObjectFactory)) {
        map.put(entry.getKey(), value);
      }
    }
    return map;
  }

  /**
   * Get a shallow copy of the current Context with the addition of the values contained in
   * customParams. This permits you to invoke sub-templets as follows:- <blockquote><code>
   * #eval $subTemplate using $Vars.extendContext({"oldParam":"value 1", "newParam":"value 2"})
   * </code> </blockquote> The templet that is invoking the call doesn't need to know which
   * variables are in the Context, they will all automatically get passed onto the sub-templet
   * thereby letting you inject additional functionality and configuration onto existing hierarchies
   * without having to go back and revisit the existing templet structure. It also means that the
   * logic of the structure is more apparent because the templets are only explicit about additionas
   * and alterations rather than them getting obscured by all the replications...
   * 
   * @see #copyContext()
   */
  public Map<Object, Object> extendContext(Map<Object, Object> customParams)
  {
    Map<Object, Object> map = copyContext();
    if (customParams != null) {
      map.putAll(customParams);
    }
    return map;
  }

  /**
   * Take a shallow copy of the supplied Map. This is intended for invoking sub-templets as follows:
   * <blockquote>
   * <code>#eval $subTemplate using $Vars.copyMap({ "key1":"value1", "key2":"value2" })</code>
   * </blockquote> This works around the bug which is described in
   * https://dev.fiennes.org/redmine/issues/3723 where webmacro both uses the contents of the parsed
   * Map as params, but also as the Context that the templet is evaluated in leading to values
   * hanging around between invocations.
   */
  public Map<Object, Object> copyMap(Map<Object, Object> map)
  {
    Map<Object, Object> copy = Maps.newHashMapWithExpectedSize(map.size());
    copy.putAll(map);
    return copy;
  }

  /**
   * Does the current Context contain any mapping for name that doesn't map to a value of null?
   */
  public boolean hasValue(Object name)
  {
    return __context.containsKey(name) && __context.get(name) != null;
  }

  /**
   * Get the value that name refers to in the current Context or an explicit value of null if it is
   * not defined.
   */
  public Object padUndefined(Object name)
  {
    return __context.containsKey(name) ? __context.get(name) : null;
  }

  /**
   * Pad an undefined Object in the context with a user supplied value
   */
  public Object padUndefined(Object name,
                             Object undefinedValue)
  {
    return __context.containsKey(name) ? __context.get(name) : undefinedValue;
  }

  /**
   * Pad an undefined or null value with a user supplied value
   */
  public Object pad(Object name,
                    Object undefinedOrNullValue)
  {
    Object value = padUndefined(name, undefinedOrNullValue);
    return value == null ? undefinedOrNullValue : value;
  }

  /**
   * Pad an undefined or empty value with an empty string ("")
   * 
   * @deprecated in preference of {@link #pad(Object)} as the name is currently misleading
   */
  @Deprecated
  public Object padNull(Object name)
  {
    Object result = padUndefined(name);
    return result != null ? result : "";
  }

  /**
   * Pad an undefined or empty value with an empty string ("")
   * 
   * @param name
   *          The Object whose toString() method is the value that we are trying to resolve
   * @return The value or "", never null
   */
  public Object pad(Object name)
  {
    Object result = padUndefined(name);
    return result != null ? result : "";
  }

  /**
   * Pad an actual Object (as opposed to looking up a key) such that an instance of an
   * UndefinedMacro becomes null.
   * 
   * @return value unless value was an instance of an UndefinedMacro in which case return null
   */
  public static Object padUndefinedMacro(Object value)
  {
    return value instanceof UndefinedMacro ? null : value;
  }

  public static Object padUndefinedMacro(Object value,
                                         Object paddedValue)
  {
    if (value instanceof UndefinedMacro || value == null) {
      return paddedValue;
    }
    return value;
  }

  /**
   * Resolve the variable in the Context named "from" and then save it in the context named "to".
   * This is different from invoking "#set $to=$from" as there is no evaluation of $from invoked if
   * it is a templet or other form of Block. This therefore lets us start to play around with
   * alternative forms of templet substition.
   * 
   * @param from
   *          The name of the variable that is going to be copied from. This can be of the form
   *          "OuterVars.xyz" if you want to copy from a variable in the global Context
   * @param to
   *          The name of the variable that is going to be copied to. This can be of the form
   *          "OuterVars.xyz" if you want to copy to a variable in the global context and you are in
   *          a sub-context. Note that the method doesn't check whether there is already a variable
   *          named "to" and therefore it can overwrite items...
   * @see #get(String)
   * @see #set(String, Object)
   */
  public void copy(String from,
                   String to)
  {
    set(to, get(from));
  }

  public static final String OUTERVARS = "OuterVars";

  private static final int OUTERVARS_INDEX = OUTERVARS.length() + 1;

  /**
   * Get a variable out of the Context taking account for references to OuterVars if utilised. ie an
   * invocation to get "OuterVars.abc" will resolve the OuterVars Map in the current Context and
   * then resolve "abc" against this Map.
   * 
   * @return The requested Object or null if it cannot be resolved in the appropriate conbtext.
   * @throws IllegalArgumentException
   *           If you ask for OuterVars.xyz and OuterVars cannot be resolved. This will be the case
   *           if you are invoking this in the global Context rather than in a sub-context.
   */
  public Object get(String name)
  {
    if (name.startsWith(OUTERVARS)) {
      Map<Object, Object> outerVars = getOuterVars(__context);
      if (outerVars == null) {
        throw new IllegalArgumentException(String.format("Cannot resolve %s because OuterVars cannot be resolved",
                                                         name));
      }
      return outerVars.get(name.substring(OUTERVARS_INDEX));
    }
    return __context.get(name);
  }

  /**
   * Look up the "OuterVars" reference in the supplied context and return it as a
   * Map&lt;Object,Object&gt;
   * 
   * @return The appropriate OuterVars Map or null if there is not one defined.
   * @throws ClassCastException
   *           if there is some variable named OuterVars in the context that isn't a Map. In an
   *           ideal world this shouldn't really be happening and if it does then you probably have
   *           some problems to look into...
   */
  @SuppressWarnings("unchecked")
  public static Map<Object, Object> getOuterVars(Map<Object, Object> context)
  {
    return (Map<Object, Object>) context.get(OUTERVARS);
  }

  /**
   * Return the OuterVars or if not defined then the original context.
   */
  public static Map<Object, Object> getGlobalContext(Map<Object, Object> context)
  {
    Map<Object, Object> outerVars = getOuterVars(context);
    return outerVars == null ? context : outerVars;
  }

  /**
   * Get an Object out of the Context, automatically looking in OuterVars if it is not defined
   * directly.
   */
  public Object delegatingGet(Object key)
  {
    return delegatingGet(__context, key);
  }

  /**
   * Get an Object out a supplied context, automatically looking in OuterVars if it is not defined
   * directly.
   */
  public static Object delegatingGet(Map<Object, Object> context,
                                     Object key)
  {
    Object value = context.get(key);
    if (value == null) {
      Map<Object, Object> outerVars = getOuterVars(context);
      if (outerVars != null) {
        return outerVars.get(key);
      }
    }
    return value;
  }

  /**
   * Get an Object from the outermost version of Context
   */
  public static Object outerGet(Map<Object, Object> context,
                                Object key)
  {
    context = MoreObjects.firstNonNull(getOuterVars(context), context);
    return context.get(key);
  }

  /**
   * Set a variable in the context taking into account for references to OuterVars if utilised, ie
   * an invocation set "OuterVars.abc" will set the value of "abc" in the OuterVars if defined.
   * 
   * @throws IllegalArgumentException
   *           If you have requested setting OuterVars.xyz and OuterVars cannot be resolved. This
   *           will be the case if you are invoking this in the global Context space rather than a
   *           sub-context.
   */
  public void set(String name,
                  Object value)
  {
    set(__context, name, value);
  }

  /**
   * Set a variable in the context taking into account for references to OuterVars if utilised, ie
   * an invocation set "OuterVars.abc" will set the value of "abc" in the OuterVars if defined.
   * 
   * @throws IllegalArgumentException
   *           If you have requested setting OuterVars.xyz and OuterVars cannot be resolved. This
   *           will be the case if you are invoking this in the global Context space rather than a
   *           sub-context.
   * @see #setOuter(Map, Object, Object)
   */
  public static void set(Map<Object, Object> context,
                         String name,
                         Object value)
  {
    if (name.startsWith(OUTERVARS)) {
      Map<Object, Object> outerVars = getOuterVars(context);
      if (outerVars == null) {
        throw new IllegalArgumentException(String.format("Cannot set %s because OuterVars cannot be resolved",
                                                         name));
      }
      outerVars.put(name.substring(OUTERVARS_INDEX), value);
    } else {
      context.put(name, value);
    }
  }

  /**
   * Resolve the OuterVars Context (if defined) and set the appropriate name=value in it. If
   * OuterVars is not defined then set it directly in the supplied Context. This will have the
   * effect of defining it in the global namespace of the Contexts.
   */
  public static void setOuter(Map<Object, Object> context,
                              Object name,
                              Object value)
  {
    Map<Object, Object> outerVars = getOuterVars(context);
    if (outerVars == null) {
      context.put(name, value);
    } else {
      outerVars.put(name, value);
    }
  }

  /**
   * Resolve the given Block by its Context name and then wrap it in a BlockWrapper so that it can
   * be passed around without it being evaluated until one wants it to be.
   * 
   * @param blockName
   *          The name that the block is stored in the current Context under. This may include
   *          OuterVars if the block is in the enclosing Context. If blockName resolves to a
   *          BlockWrapper then the Block is extracted from the BlockWrapper and a new BlockWrapper
   *          is constructed with the new defaultParams.
   * @param defaultParams
   *          The optional params that will always be passed to the Block when it is evaluated
   *          unless they are overridden by the customParams at invocation time. This lets you set
   *          up the default behaviour of a block of functionality without having to worry about the
   *          templates passing the BlockWrapper around knowing anything about the required
   *          configuration parameters.
   * @throws IllegalArgumentException
   *           if blockName doesn't resolve to anything or if blockName doesn't resolve to a Block
   */
  public BlockWrapper wrapBlock(String blockName,
                                Map<Object, Object> defaultParams)
  {
    Object obj = get(blockName);
    if (obj == null) {
      throw new IllegalArgumentException(String.format("%s is unkown within the current Context",
                                                       blockName));
    }
    if (obj instanceof BlockWrapper) {
      obj = ((BlockWrapper) obj).getBlock();
    } else {
      if (!(obj instanceof Block)) {
        throw new IllegalArgumentException(String.format("%s resolves to a %s which is not a Block",
                                                         blockName,
                                                         obj.getClass()));
      }
    }
    return new BlockWrapper((Block) obj, blockName, defaultParams);
  }

  /**
   * wrap a Block in a BlockWrapper with no defaultParams associated with it. Evaluating this
   * BlockWrapper with no customParams supplied will be the equivalent of invoking: <blockquote>
   * <code>
   * #eval $blockWrapper.Block using { }
   * </code></blockquote>
   * 
   * @see #wrapBlock(String, Map)
   */
  public BlockWrapper wrapBlock(String blockName)
  {
    return wrapBlock(blockName, null);
  }

  /**
   * Ensure that the given Object is a BlockWrapper by resolving it if it is the name of a
   * BlockWrapper or just casting it if it isn't.
   * 
   * @param blockWrapperOrBlockName
   *          The Object that we want as a BlockWrapper. If this is a BlockWrapper already then it
   *          will just be returned as appropriate, otherwise it will be converted to a String and
   *          then passed to wrapBlock for turning into a BlockWrapper.
   * @see #wrapBlock(String)
   */
  public BlockWrapper asBlockWrapper(Object blockWrapperOrBlockName)
  {
    if (blockWrapperOrBlockName instanceof BlockWrapper) {
      return (BlockWrapper) blockWrapperOrBlockName;
    }
    return wrapBlock(blockWrapperOrBlockName.toString());
  }

  /**
   * Ensure the given Object is evaluatable as a Macro. Macros will pass through untouched,
   * BlockWrappers will get eval() called and everything else will be converted to a String and then
   * wrapped up in a StringMacroAdaptor
   * 
   * @see BlockWrapper#eval()
   * @see StringMacroAdapter
   */
  public static Macro asMacro(Object obj)
  {
    if (obj instanceof Macro) {
      return (Macro) obj;
    }
    if (obj instanceof BlockWrapper) {
      return ((BlockWrapper) obj).eval();
    }
    return new StringMacroAdapter(obj.toString());
  }

  /**
   * BlockWrapper is a class that encapsulates a webmacro Block and default parameters for invoking
   * it inside a variable that can be safely passed around without the Block being prematurely
   * expanded.
   * 
   * @author alex
   * @see VarsTool#wrapBlock(String, Map)
   */
  public class BlockWrapper
  {
    private final Block __block;

    private final String __blockName;

    private final Map<Object, Object> __defaultParams;

    /**
     * @param block
     *          The block that is going to be wrapped by this BlockWrapper
     * @param blockName
     *          The name which was used to resolve this Block. This lets us give slightly more
     *          informative error messages.
     * @param defaultParams
     *          The optional list of params that will be used as the starting block whenever we try
     *          and evaluate this block. If this is null then an empty HashMap will be utilised
     *          instead.
     * @see VarsTool#wrapBlock(String, Map)
     */
    private BlockWrapper(Block block,
                         String blockName,
                         Map<Object, Object> defaultParams)
    {
      __block = block;
      __blockName = blockName;
      __defaultParams = defaultParams == null ? new HashMap<Object, Object>() : defaultParams;
    }

    /**
     * Get the Block that is contained by this BlockWrapper. This lets us do webmacro along the
     * lines of:- <blockquote><code>
     * #eval $blockWrapper.Block (using { .... })
     * </code></blockquote> However it may be easier and more readable to use the eval and evalUsing
     * methods on the BlockWrapper
     * 
     * @return The nested Block. Never null.
     * @see #eval()
     * @see #evalUsing(Map)
     */
    public Block getBlock()
    {
      return __block;
    }

    /**
     * Merge the customParams to the defaultParams and then return a Macro that can be evaluated
     * just by using it in the context. Therefore you can use:- <blockquote><code>
     * $blockWrapper.evalUsing({ "name":"value" })
     * </code></blockquote> which is equivalent to using:- <blockquote><code>
     * #eval $blockWrapper.Block using $blockWrapper.getMergedParams({"name":"value")}
     * </code></blockquote>
     * 
     * @param customParams
     *          The optional additional params that will be added to the defaultParams. If this is
     *          empty or null then the Macro will invoke the block with just the contents of the
     *          defaultParams.
     * @return A Macro that can be efficiently resolved by the webmacro engine
     * @see #eval()
     */
    public Macro evalUsing(final Map<Object, Object> customParams)
    {
      return new Macro() {
        @Override
        public void write(FastWriter arg0,
                          Context arg1)
            throws PropertyException, IOException
        {
          VarsTool.this.write(__block, arg0, getMergedParams(customParams));
        }

        @Override
        public Object evaluate(Context arg0)
            throws PropertyException
        {
          FastWriter fw = FastWriter.getInstance(__context.getBroker());
          try {
            write(fw, arg0);
          } catch (IOException e) {
            return "";
          }
          return fw.toString();
        }
      };
    }

    /**
     * Build a Macro that evokes the nested Block with the defaultParams. This lets you do:-
     * <blockquote><code>
     * $blockWrapper.eval()
     * </code></blockquote> which is equivalent to using:- <blockquote><code>
     * #eval $blockWrapper.Block using $blockWrapper.getDefaultParams(null)
     * </code> </blockquote>
     * 
     * @return A Macro that can be efficiently resolved by the webmacro engine
     * @see #evalUsing(Map)
     */
    public Macro eval()
    {
      return evalUsing(null);
    }

    /**
     * Get a copy of the defaultParams that were passed to the constructor of this BlockWrapper.
     * 
     * @return A Map containing zero or more values. This will always be a new Map so it doesn't
     *         matter if when this is changed by a Block when it is evaluated as a Context.
     * @see #getMergedParams(Map)
     */
    public Map<Object, Object> getDefaultParams()
    {
      return new HashMap<Object, Object>(__defaultParams);
    }

    /**
     * Get the Map that is the result of taking the default params (if any) and overlaying the
     * customParams (if any) on top of it.
     * 
     * @param customParams
     *          The optional additional params that should be added to the defaultParams to make the
     *          compound params. Note that this is only for this invocation and doesn't affect the
     *          defaultParams. If null then the result will be a copy of the defaultParams.
     * @return A parameter Map. If both defaultParams and customParams are null then the Map will be
     *         empty. This will always be a new Map so it doesn't matter if when this is changed by
     *         a Block when it is evaluated as a Context.
     * @see #getDefaultParams()
     */
    public Map<Object, Object> getMergedParams(Map<Object, Object> customParams)
    {
      Map<Object, Object> params = getDefaultParams();
      if (customParams != null) {
        params.putAll(customParams);
      }
      return params;
    }

    /**
     * @return BlockWrapper(blockName,defaultParams)
     */
    @Override
    public String toString()
    {
      return String.format("BlockWrapper(%s,%s)", __blockName, __defaultParams);
    }
  }

  public static final String EVALDEPTH = "EvalDepth";

  public static final String SELF = "Self";

  /**
   * Utility method that contains the functionality from the #eval webmacro method but invocable in
   * a programmatic java context. The following items will be added to the sub-context: OuterVars,
   * EvalDepth and Self. Over time we will refactor the core webmacro code such that this can be
   * invoked against the webmacro codebase rather than duplicating their code.
   * 
   * @param params
   *          The values in the Context that the block will be evaluated in. If this is null then
   *          there will be no values added to the Context, ie the block will be evaluated in an
   *          empty Context.
   * @see #SELF
   * @see #EVALDEPTH
   * @see #OUTERVARS
   * @see Block#write(org.webmacro.FastWriter, org.webmacro.Context)
   */
  public void write(Block block,
                    FastWriter out,
                    Map<Object, Object> params)
      throws PropertyException, IOException
  {
    Map<Object, Object> outerVars = getOuterVars(__context);
    if (outerVars == null) {
      outerVars = __context;
    }
    int evalDepth = 0;
    Object obj = __context.get(EVALDEPTH);
    if (obj != null && obj instanceof Integer) {
      evalDepth = ((Integer) obj).intValue() + 1;
    }
    Context c = params == null
        ? new Context(__context.getBroker())
        : new Context(__context.getBroker(), params);
    c.put(OUTERVARS, outerVars);
    c.put(EVALDEPTH, Integer.valueOf(evalDepth));
    c.put(SELF, block);
    block.write(out, c);
  }

  /**
   * Debug information about the given named variable in the current Context to the DEBUG_OUT
   * channel.
   * 
   * @param header
   *          The string that is prefixed to the output line in the debugger
   * @param name
   *          The name of the variable that is to be debugged. This can include OuterVars. if so
   *          required. If this resolves to null then this is explicitly annotated.
   */
  public void debugVariable(String header,
                            String name)
  {
    Object obj = get(name);
    if (obj == null) {
      DebugConstants.DEBUG_OUT.println(String.format("%s: %s --> null", header, name));
    } else {
      DebugConstants.DEBUG_OUT.println(String.format("%s: %s --> %s (%s)",
                                                     header,
                                                     name,
                                                     obj,
                                                     obj.getClass()));
    }
  }

  public void debug(String text)
  {
    DebugConstants.DEBUG_OUT.println(text);
  }

  private void debugMap(String title,
                        Map<Object, Object> map,
                        boolean includeValues,
                        Appendable buf)
      throws IOException
  {
    buf.append(title).append('\n');
    if (map == null) {
      buf.append("  map is null\n");
    } else if (map.size() == 0) {
      buf.append("  map is empty\n");
    } else {
      TreeMap<String, Object> sortedMap = new TreeMap<String, Object>();
      for (Entry<Object, Object> entry : map.entrySet()) {
        sortedMap.put(entry.getKey().toString(), entry.getValue());
      }
      for (Entry<String, Object> entry : sortedMap.entrySet()) {
        if (entry.getValue() == null) {
          buf.append(String.format("  %s --> null\n", entry.getKey()));
        } else if (includeValues) {
          buf.append(String.format("  %s --> %s (%s)\n",
                                   entry.getKey(),
                                   entry.getValue(),
                                   entry.getValue().getClass()));
        } else {
          buf.append(String.format("  %s --> ... (%s)\n",
                                   entry.getKey(),
                                   entry.getValue().getClass()));
        }
      }
    }
  }

  /**
   * Debug the state of the current Context to the DEBUG_OUT channel
   * 
   * @param title
   *          The string that will prefix the debug to the standard out.
   * @param includeValues
   *          If false then only the keys and the class of the values will be output (or null if the
   *          key maps to a null value), whereas if true then the values will be output as well
   *          which can get a little noisy...
   */
  public void debugContext(String title,
                           boolean includeValues)
  {
    try {
      debugMap(title, __context, includeValues, DebugConstants.DEBUG_OUT);
    } catch (IOException e) {
      throw new LogicException("DEBUG_OUT should not throw IOExceptions", e);
    }
  }

  /**
   * Convert an Object into an Integer so that we can do maths on it in webmacro space which isn't
   * possible with String representations of numbers.
   * 
   * @return The appropriate Integer
   * @throws NumberFormatException
   *           if it is not possible to parse obj
   */
  public Integer asInteger(Object obj)
      throws NumberFormatException
  {
    return NumberUtil.toInteger(obj);
  }

  /**
   * Concert an Object into an Integer so that we can do maths on it in webmacro space with the
   * option of specifying a fallback value if it isn't possible to perform the conversion.
   * 
   * @param obj
   *          The optional value to be converted, If it isn't defined or cannot be converted then
   *          defaultObj will be used instead.
   * @param defaultObj
   *          The optional fallback value. If it isn't possible to convert this into an Integer,
   *          then it will be treated as null
   * @return obj as an Integer, or defaultObj as an Integer or null if none of them are resolveable.
   */
  public Integer asInteger(Object obj,
                           Object defaultObj)
  {
    Integer defaultInteger = NumberUtil.toInteger(defaultObj, null);
    return NumberUtil.toInteger(obj, defaultInteger, defaultInteger);
  }

  public Float asFloat(Object obj)
  {
    return NumberUtil.toFloat(obj, null, null);
  }

  /**
   * Construct an unmodifiable List of Integers from 1 to the specified inclusive upper bound.
   */
  public IntList oneTo(Integer upperBound)
  {
    Preconditions.checkArgument(upperBound != null,
                                "Cannot generate a list from 1 to %s",
                                upperBound);
    Assertions.isTrue(upperBound != null, "Cannot generate a list from 1 to %s", upperBound);
    final int u = upperBound.intValue();
    IntList list = new IntArrayList(u);
    for (int i = 1; i <= u; i++) {
      list.add(i);
    }
    return IntLists.unmodifiable(list);
  }

  /**
   * Generate a random int that lies between min and max
   */
  public int random(int min,
                    int max)
  {
    if (max < min) {
      throw new IllegalArgumentException("Cannot generate a random number between " + min + " and "
                                         + max);
    }
    if (max == min) {
      return max;
    }
    double r = Math.random() * (max - min) + min;
    return (int) r;
  }

  /**
   * Generate a random int that lies between 0 and max
   */
  public int random(int max)
  {
    return random(0, max);
  }

  /**
   * Return a Macro which when evaluated will throw a RuntimeException with the given message. This
   * slightly awkward approach is used rather than just throwing the Exception directly because it
   * means that we can include information about the Context of the error if required.
   */
  public Macro exception(final String message,
                         final boolean includeContext)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        if (includeContext) {
          StringBuilder buf = new StringBuilder();
          debugMap(message, context, false, buf);
          throw new RuntimeException(buf.toString());
        }
        throw new RuntimeException(message);
      }
    };
  }

  public void assertIsTable(final Object record,
                            final Object table,
                            String recordName)
  {
    if (record == null || (record instanceof UndefinedMacro)
        || !(record instanceof PersistentRecord)) {
      throw new IllegalArgumentException(String.format("%s is not a valid type for %s",
                                                       padUndefinedMacro(record, "UndefinedMacro"),
                                                       recordName));
    }
    PersistentRecord r = (PersistentRecord) record;
    if (table instanceof Table) {
      if (r.getTable().equals(table)) {
        return;
      }
    } else if (r.getTable().getName().equals(table.toString())) {
      return;
    }
    throw new IllegalArgumentException(String.format("A value of %s for %s is not a %s",
                                                     record,
                                                     recordName,
                                                     table));
  }

  public static final String WM_INCLUDES = "includes";
  public static final String WM_REQUIREMENTS = "Requirements";

  @Deprecated
  public static IncludeList getIncludeList(Map<Object, Object> context)
  {
    return ObjectUtil.castTo(delegatingGet(context, WM_INCLUDES), IncludeList.class);
  }

  public static Requirements getRequirements(Map<Object, Object> context)
  {
    return ObjectUtil.castTo(delegatingGet(context, WM_REQUIREMENTS), Requirements.class);
  }

  /**
   * Convert an Object to String returning "" if obj is null.
   */
  public static String toString(Object obj)
  {
    return obj == null ? "" : obj.toString();
  }

  /**
   * If the given Optional is present then return the toString() of the contents, otherwise return
   * "".
   */
  public static String toString(Optional<?> optional)
  {
    return optional == null ? "" : optional.isPresent() ? optional.get().toString() : "";
  }

  /**
   * Convert an Object to String returning nullValue if obj is null.
   */
  public static String toString(Object obj,
                                String nullValue)
  {
    return obj == null ? nullValue : obj.toString();
  }

  /**
   * Convert the list of causes for a Throwable to a List with the passed throwable param as the
   * first item and the deepest nested throwable as the last item.
   */
  public static List<Throwable> getCauses(Throwable throwable)
  {
    ArrayList<Throwable> list = Lists.newArrayList();
    list.add(throwable);
    while ((throwable = throwable.getCause()) != null) {
      list.add(throwable);
    }
    return list;
  }

  public static String printStackTrace(Throwable throwable)
  {
    return ExceptionUtil.printStackTrace(throwable);
  }

  /**
   * If obj is not null then return it otherwise return {@link Opt#NOVALUE}.
   */
  public Object opt(Object obj)
  {
    return obj == null ? Opt.NOVALUE : obj;
  }

  /**
   * If the optional variable is defined then return the contents, otherwise return
   * {@link Opt#NOVALUE}.
   */
  public Object opt(Optional<?> optional)
  {
    if (optional != null) {
      if (optional.isPresent()) {
        return optional.get();
      }
    }
    return Opt.NOVALUE;
  }

  public <T> T firstNotNull(T first,
                            T second)
  {
    return MoreObjects.firstNonNull(first, second);
  }

  /**
   * Get the JODA {@link DateTime} that represents the current time in milliseconds.
   */
  public DateTime now()
  {
    return DateTime.now();
  }
}
