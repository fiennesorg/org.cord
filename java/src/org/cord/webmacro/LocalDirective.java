package org.cord.webmacro;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.directive.Directive;
import org.webmacro.directive.DirectiveBuilder;
import org.webmacro.directive.DirectiveDescriptor;
import org.webmacro.engine.Block;
import org.webmacro.engine.BuildContext;
import org.webmacro.engine.BuildException;

/**
 * "#local { inner block }" provides a clone of the context so that any changes that you make to the
 * code inside the Block doesn't effect the state of the Context before or after the directive.
 * 
 * @author alex
 */
public class LocalDirective
  extends Directive
{
  private static final int LOCAL_BLOCK = 1;

  private static final ArgDescriptor[] MYARGS = new ArgDescriptor[] { new BlockArg(LOCAL_BLOCK) };
  private static final DirectiveDescriptor MYDESC =
      new DirectiveDescriptor("local", null, MYARGS, null);

  public static DirectiveDescriptor getDescriptor()
  {
    return MYDESC;
  }

  private Macro _block;

  @Override
  public Object build(DirectiveBuilder b,
                      BuildContext bc)
      throws BuildException
  {
    _block = (Block) b.getArg(LOCAL_BLOCK, bc);
    return this;
  }

  @Override
  public void write(FastWriter out,
                    Context context)
      throws PropertyException, IOException
  {
    Context clone = context.cloneContext();
    Iterator<Entry<Object, Object>> i = clone.entrySet().iterator();
    while (i.hasNext()) {
      Entry<Object, Object> entry = i.next();
      if (entry.getValue() instanceof ContextTool) {
        i.remove();
      }
    }
    _block.write(out, clone);
  }
}
