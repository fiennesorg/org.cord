package org.cord.webmacro;

import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.broker.ContextObjectFactory;

/**
 * ContextObjectFactory that produces an Object that just wraps the Context but when you ask for
 * something that doesn't exist then it gives you null rather than an UndefinedMacro as directly
 * querying the context does. This is registered as $Optional inside the standard
 * WebMacro.properties which means that if you want to invoke a method with a variable that should
 * either be defined or null then you can go $object.method($compulsoryVariable,
 * $Optional.optionalVariable) and the second variable will automatically be either null or the
 * value of $optionalVariable without any webmacro errors appearing. This also greatly improves the
 * readability of webmacro templates as the intent of how variables should be used is much more
 * transparent.
 * 
 * @author alex
 */
public final class NullPaddedContext
  implements ContextObjectFactory
{
  private final Context __context;

  public NullPaddedContext()
  {
    this(null);
  }

  public NullPaddedContext(Context context)
  {
    __context = context;
  }

  @Override
  public Object get(Context context)
      throws PropertyException
  {
    return new NullPaddedContext(context);
  }

  public Object get(Object key)
  {
    Object value = __context.get(key);
    return value;
  }
}
