package org.cord.webmacro;

import java.io.IOException;
import java.util.Map;

import org.cord.node.grid.Grid;
import org.cord.util.NumberUtil;
import org.webmacro.Context;
import org.webmacro.Macro;

public class CssTool
  extends ContextConstantTool<CssTool.Instance>
{

  public static void clearfix(Appendable out,
                              String selector)
      throws IOException
  {
    String s = selector.trim();
    out.append(s)
       .append(":before, ")
       .append(s)
       .append(":after { content:\"\"; display: table; }\n");
    out.append(s).append(":after { clear: both; }\n");
    out.append(s).append(" { zoom: 1; }\n");
  }

  public CssTool()
  {
    super(Instance.class);
  }

  @Override
  public Instance createInstance(Context context)
  {
    return new Instance(context);
  }

  public class Instance
  {
    protected final Grid __grid;

    public Instance(Context context)
    {
      __grid = Grid.get(context);
    }

    private void overhang(Appendable out,
                          String dim,
                          Integer size)
        throws IOException
    {
      if (size != null) {
        out.append("margin-")
           .append(dim)
           .append(": ")
           .append(NumberUtil.toString(-size.intValue()))
           .append("px;\n");
        out.append("padding-")
           .append(dim)
           .append(": ")
           .append(NumberUtil.toString(size.intValue()))
           .append("px;\n");
      }
    }

    /**
     * Generate negative margins and corresponding padding to create an overhang by the specified
     * number of pixels in each of the four edges. Any null elements will ommit the definition for
     * that axis leaving it at the default or inherited setting.
     */
    public Macro overhang(final Integer top,
                          final Integer right,
                          final Integer bottom,
                          final Integer left)
    {
      return new AppendableConstantMacro() {

        @Override
        public void append(Appendable out)
            throws IOException
        {
          overhang(out, "top", top);
          overhang(out, "right", right);
          overhang(out, "bottom", bottom);
          overhang(out, "left", left);
        }
      };
    }

    /**
     * Generate a rule for selector:after that will auto-close selector around any contained floats.
     */
    public Macro clearfix(final String selector)
    {
      return new AppendableConstantMacro() {
        @Override
        public void append(Appendable out)
            throws IOException
        {
          CssTool.clearfix(out, selector);
        }
      };
    }

    /**
     * Generate negative margins and corresponding padding to create an overhang by the specified
     * number of pixels in the vertical and horizontal axis. Any null elements will ommit the
     * definition for that axis leaving it at the default or inherited setting.
     */
    public Macro overhang(Integer vertical,
                          Integer horizontal)
    {
      return overhang(vertical, horizontal, vertical, horizontal);
    }

    /**
     * Generate negative margins and corresponding padding to create an overhang by the specified
     * number of pixels in all directions. Any null elements will ommit the definition for that axis
     * leaving it at the default or inherited setting.
     */
    public Macro overhang(Integer size)
    {
      return overhang(size, size);
    }

    public Macro borderRadius(final int topLeft,
                              final int topRight,
                              final int bottomRight,
                              final int bottomLeft)
    {
      return new AppendableConstantMacro() {
        @Override
        public void append(Appendable out)
            throws IOException
        {
          String tl = NumberUtil.toString(topLeft);
          out.append("-webkit-border-top-left-radius: ").append(tl).append("px; \n");
          out.append("-moz-border-top-left-radius: ").append(tl).append("px; \n");
          out.append("border-top-left-radius: ").append(tl).append("px; \n");
          String tr = NumberUtil.toString(topRight);
          out.append("-webkit-border-top-right-radius: ").append(tr).append("px; \n");
          out.append("-moz-border-top-right-radius: ").append(tr).append("px; \n");
          out.append("border-top-right-radius: ").append(tr).append("px; \n");
          String br = NumberUtil.toString(bottomRight);
          out.append("-webkit-border-bottom-right-radius: ").append(br).append("px; \n");
          out.append("-moz-border-bottom-right-radius: ").append(br).append("px; \n");
          out.append("border-bottom-right-radius: ").append(br).append("px; \n");
          String bl = NumberUtil.toString(bottomLeft);
          out.append("-webkit-border-bottom-left-radius: ").append(bl).append("px; \n");
          out.append("-moz-border-bottom-left-radius: ").append(bl).append("px; \n");
          out.append("border-bottom-left-radius: ").append(bl).append("px; \n");
          out.append("-moz-background-clip: padding;\n" + "-webkit-background-clip: padding-box;\n"
                     + "background-clip: padding-box;\n");
        }
      };
    }

    public Macro borderRadius(final int radius)
    {
      return new AppendableConstantMacro() {
        @Override
        public void append(Appendable out)
            throws IOException
        {
          String r = NumberUtil.toString(radius);
          out.append("-webkit-border-radius: ").append(r).append("px; \n");
          out.append("-moz-border-radius: ").append(r).append("px; \n");
          out.append("border-radius: ").append(r).append("px; \n");
        }
      };
    }

    public Macro shadow(final int x,
                        final int y,
                        final int blur,
                        final String color)
    {
      return new AppendableConstantMacro() {

        @Override
        public void append(Appendable out)
            throws IOException
        {
          String xx = NumberUtil.toString(x);
          String yy = NumberUtil.toString(y);
          String bb = NumberUtil.toString(blur);
          out.append("-webkit-box-shadow: ")
             .append(xx)
             .append("px ")
             .append(yy)
             .append("px ")
             .append(bb)
             .append("px ")
             .append(color)
             .append(";\n");
          out.append("-moz-box-shadow: ")
             .append(xx)
             .append("px ")
             .append(yy)
             .append("px ")
             .append(bb)
             .append("px ")
             .append(color)
             .append(";\n");
          out.append("box-shadow: ")
             .append(xx)
             .append("px ")
             .append(yy)
             .append("px ")
             .append(bb)
             .append("px ")
             .append(color)
             .append(";\n");
        }
      };
    }

    public Block getBlock(int cols)
    {
      return new Block(cols, null, null, null, null);
    }

    public class Block
    {
      private final int __cols;
      private final Block __parent;
      private final Integer __margin;
      private final Integer __border;
      private final Integer __padding;

      public Block(int cols,
                   Integer margin,
                   Integer border,
                   Integer padding,
                   Block parent)
      {
        __cols = cols;
        __parent = parent;
        if (parent == null) {
          __margin = margin;
          __border = border;
          __padding = padding;
        } else {
          __margin = margin != null ? margin : __parent.getMargin();
          __border = border != null ? border : __parent.getBorder();
          __padding = padding != null ? padding : __parent.getPadding();
        }
      }

      public final Block withMargin(int px)
      {
        return new Block(__cols, Integer.valueOf(px), __border, __padding, this);
      }

      public final Block withBorder(int px)
      {
        return new Block(__cols, __margin, Integer.valueOf(px), __padding, this);
      }

      public final Block withPadding(int px)
      {
        return new Block(__cols, __margin, __border, Integer.valueOf(px), this);
      }

      public int getSizeDelta()
      {
        int delta = 0;
        if (__margin != null) {
          delta += 2 * __margin.intValue();
        }
        if (__border != null) {
          delta += 2 * __border.intValue();
        }
        if (__padding != null) {
          delta += 2 * __padding.intValue();
        }
        return delta;
      }

      public int getPixelHeight(int px)
      {
        return px - getSizeDelta();
      }

      public int getLineHeight(int lines)
      {
        return __grid.getLineHeight(lines) - getSizeDelta();
      }

      public int getAspectHeight(float aspect)
      {
        return (int) (__grid.getWidth(__cols) * aspect - getSizeDelta());
      }

      public Macro appendWidth()
      {
        return appendCss();
      }

      public int getWidth()
      {
        return __grid.getWidth(__cols) - getSizeDelta();
      }

      public Macro appendCss()
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            if (__margin != null) {
              out.append("margin:").append(__margin.toString()).append("px; ");
            }
            if (__border != null) {
              out.append("border:").append(__border.toString()).append("px; ");
            }
            if (__padding != null) {
              out.append("padding:").append(__padding.toString()).append("px; ");
            }
            out.append("width:").append(Integer.toString(getWidth())).append("px;\n");
          }
        };
      }

      public Macro appendHeight(final int lines)
      {
        return new AppendableConstantMacro() {
          @Override
          public void append(Appendable out)
              throws IOException
          {
            out.append("height:").append(Integer.toString(getLineHeight(lines))).append("px;\n");
          }
        };
      }

      public Macro appendMinHeight(final int lines)
      {
        return new AppendableConstantMacro() {
          @Override
          public void append(Appendable out)
              throws IOException
          {
            out.append("min-height:")
               .append(Integer.toString(getLineHeight(lines)))
               .append("px;\n");
          }
        };
      }

      public final Integer getMargin()
      {
        return __margin;
      }

      public final Integer getPadding()
      {
        return __padding;
      }

      public final Integer getBorder()
      {
        return __border;
      }

    }
  }
}
