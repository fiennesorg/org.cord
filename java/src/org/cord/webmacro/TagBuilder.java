package org.cord.webmacro;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringBuilderIOException;
import org.webmacro.engine.UndefinedMacro;

import com.google.common.base.Preconditions;

public class TagBuilder
{
  private final String __name;
  private final Map<String, String> __attributes = new HashMap<String, String>();

  public TagBuilder(String name)
  {
    __name = Preconditions.checkNotNull(name, "name");
  }

  public TagBuilder attr(String name,
                         Object value)
  {
    addAttribute(name, value);
    return this;
  }

  public void addAttribute(String name,
                           Object value)
  {
    Preconditions.checkState(name != null && name.length() > 0, "name");
    Preconditions.checkNotNull(value, "value for %s", name);
    __attributes.put(name, value.toString());
  }

  /**
   * Invoke {@link #addAttribute(String, Object)} if the value is defined.
   * 
   * @param value
   *          The value of the attribute. If this is either null or UndefinedMacro then no action is
   *          taken.
   */
  public void addOptAttribute(String name,
                              Object value)
  {
    if (value == null || value instanceof UndefinedMacro) {
      return;
    }
    addAttribute(name, value);
  }

  /**
   * @return The given attribute value or null if it has not been defined.
   */
  public String getAttribute(String name)
  {
    return __attributes.get(name);
  }

  public void build(Appendable buf,
                    boolean hasContents)
      throws IOException
  {
    buf.append("<").append(__name);
    for (Map.Entry<String, String> e : __attributes.entrySet()) {
      HtmlUtilsTheme.addAttribute(buf, e.getKey(), e.getValue());
    }
    if (hasContents) {
      buf.append('>');
    } else {
      buf.append("/>");
    }
  }

  public void build(StringBuilder buf,
                    boolean hasContents)
  {
    try {
      build((Appendable) buf, hasContents);
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
  }

  public String build(boolean hasContents)
  {
    StringBuilder buf = new StringBuilder();
    build(buf, hasContents);
    return buf.toString();
  }

  public String getOpen()
  {
    return build(true);
  }

  public String getClose()
  {
    return "</" + __name + ">";
  }

  public void close(StringBuilder buf)
  {
    try {
      close((Appendable) buf);
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
  }

  public void close(Appendable out)
      throws IOException
  {
    out.append("</").append(__name).append(">");
  }

  /**
   * Render this tag as an HTML element which doesn't contain anything
   */
  @Override
  public String toString()
  {
    return build(false);
  }
}
