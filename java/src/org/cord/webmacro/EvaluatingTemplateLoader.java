package org.cord.webmacro;

import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.engine.StringTemplate;
import org.webmacro.resource.AbstractTemplateLoader;
import org.webmacro.resource.CacheElement;
import org.webmacro.resource.CacheReloadContext;

/**
 * TemplateLoader implementation that expects the webmacro template to be the key of the template,
 * ie it never loads a separate template, just resolves the key as is. The caching policy is
 * obviously to never reload the template because if it changed then it would be a different key...
 * 
 * @author alex
 */
public class EvaluatingTemplateLoader
  extends AbstractTemplateLoader
{
  public final static String PROTOCOL = "evaluate:";

  @Override
  public void setConfig(String config)
  {
  }

  @Override
  public Template load(String query,
                       CacheElement ce)
      throws ResourceException
  {
    if (!query.startsWith(PROTOCOL)) {
      return null;
    }
    ce.setReloadContext(__neverReload);
    return new StringTemplate(broker, query.substring(PROTOCOL.length()));
  }

  private static NeverReload __neverReload = new NeverReload();

  private static class NeverReload
    extends CacheReloadContext
  {
    private NeverReload()
    {
    }

    @Override
    public boolean shouldReload()
    {
      return false;
    }
  }
}
