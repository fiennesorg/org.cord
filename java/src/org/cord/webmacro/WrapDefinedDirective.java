package org.cord.webmacro;

import java.io.IOException;

import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.directive.Directive;
import org.webmacro.directive.DirectiveBuilder;
import org.webmacro.directive.DirectiveDescriptor;
import org.webmacro.engine.Block;
import org.webmacro.engine.BuildContext;
import org.webmacro.engine.BuildException;

/**
 * Webmacro directive that gives you the ability to state #wrapDefined $header $contents $footer
 * where if $contents evaluates to empty or only whitespace then nothing is rendered.
 * 
 * @author alex
 */
public class WrapDefinedDirective
  extends Directive
{
  private static final int WRAPDEFINED_HEADER = 1;
  private static final int WRAPDEFINED_CONTENTS = 2;
  private static final int WRAPDEFINED_FOOTER = 3;

  private static final ArgDescriptor[] MYARGS =
      new ArgDescriptor[] { new QuotedStringArg(WRAPDEFINED_HEADER),
          new BlockArg(WRAPDEFINED_CONTENTS), new QuotedStringArg(WRAPDEFINED_FOOTER) };
  private static final DirectiveDescriptor MYDESC =
      new DirectiveDescriptor("wrapDefined", null, MYARGS, null);

  public static DirectiveDescriptor getDescriptor()
  {
    return MYDESC;
  }

  private Object _header;
  private Block _contents;
  private Object _footer;

  @Override
  public Object build(DirectiveBuilder b,
                      BuildContext bc)
      throws BuildException
  {
    _header = b.getArg(WRAPDEFINED_HEADER, bc);
    _contents = (Block) b.getArg(WRAPDEFINED_CONTENTS, bc);
    _footer = b.getArg(WRAPDEFINED_FOOTER, bc);
    return this;
  }

  @Override
  public void write(FastWriter out,
                    Context context)
      throws PropertyException, IOException
  {
    FastWriter contentsOut = FastWriter.getInstance(context.getBroker());
    _contents.write(contentsOut, context);
    if (!contentsOut.isEmptyOrWhitespaceBuffer()) {
      if (_header instanceof Macro) {
        ((Macro) _header).write(out, context);
      } else {
        out.append(_header.toString());
      }
      contentsOut.writeTrimmedBufferTo(out);
      if (_footer instanceof Macro) {
        ((Macro) _footer).write(out, context);
      } else {
        out.append(_footer.toString());
      }
    }
  }
}
