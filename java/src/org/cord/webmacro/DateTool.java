package org.cord.webmacro;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.mail.internet.MailDateFormat;

import org.cord.util.Maps;
import org.cord.util.Today;
import org.webmacro.Context;
import org.webmacro.PropertyException;
import org.webmacro.broker.ContextObjectFactory;

/** Useful methods for date and time formatting and manipulation */
public final class DateTool
  implements ContextObjectFactory
{
  @Override
  public Object get(Context c)
      throws PropertyException
  {
    return this;
  }

  /** Return the date at this point in time */
  public Date getNow()
  {
    return new Date();
  }

  /** Return the the number of years since 0AD at this point in time */
  public int getThisYear()
  {
    return Today.getInstance().get(Calendar.YEAR);
  }

  private final MailDateFormat __mailDateFormat = new MailDateFormat();

  /** Return the date as an RFC822-formatted string */
  public String rfc822(Date date)
  {
    synchronized (__mailDateFormat) {
      return __mailDateFormat.format(date);
    }
  }

  private final Map<String, SimpleDateFormat> __dateFormats = Maps.newConcurrentHashMap();

  /**
   * Return date formatted using the SimpleDateFormat string specified in fmt
   */
  public String format(Date date,
                       String fmt)
  {
    SimpleDateFormat formatter = __dateFormats.get(fmt);
    if (formatter == null) {
      formatter = new SimpleDateFormat(fmt);
      __dateFormats.put(fmt, formatter);
    }
    synchronized (formatter) {
      return formatter.format(date);
    }
  }
}
