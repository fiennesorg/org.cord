package org.cord.webmacro;

import org.cord.node.grid.Grid;
import org.cord.node.grid.GridDimensionMgr;
import org.cord.util.ObjectUtil;
import org.webmacro.Context;

public class DimensionsTool
  extends ContextConstantTool<GridDimensionMgr>
{

  public DimensionsTool()
  {
    super(GridDimensionMgr.class);
  }

  @Override
  public GridDimensionMgr createInstance(Context context)
  {
    Grid grid = ObjectUtil.castTo(context.get(Grid.WEBMACRO_GRID), Grid.class);
    return new GridDimensionMgr(grid, grid.getColumnCount());
  }

}
