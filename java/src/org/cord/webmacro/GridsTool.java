package org.cord.webmacro;

import java.util.HashMap;
import java.util.Map;

import org.cord.util.Assertions;
import org.webmacro.Context;

/**
 * ContextTool that goes under the name of $Grids and provides functionality for handling Grid and
 * Layout related functionality.
 * 
 * @author alex
 */
public class GridsTool
  extends ContextConstantTool<GridsTool.Instance>
{
  public static class Instance
  {
    private final Context __context;
    private final Map<String, NamedMacroList> __grids = new HashMap<String, NamedMacroList>();

    private Instance(Context context)
    {
      __context = context;
    }

    public NamedMacroList get(String name)
    {
      Assertions.notEmpty(name, "name");
      NamedMacroList grid = __grids.get(name);
      if (grid != null) {
        return grid;
      }
      grid = new NamedMacroList(name);
      __grids.put(name, grid);
      return grid;
    }
  }

  public GridsTool()
  {
    super(Instance.class);
  }

  @Override
  public Instance createInstance(Context context)
  {
    return new Instance(context);
  }

}
