package org.cord.webmacro;

import java.io.IOException;

import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.WebMacro;

import com.google.common.base.Preconditions;

/**
 * Implementation of a Macro that resolves a constant templatePath and then evaluates it on demand.
 */
public class TemplateMacro
  implements Macro
{
  /**
   * If both webmacro and templatePath are defined then create a TemplateMacro otherwise return
   * null. This is useful for situations when you don't know whether the WebMacro engine has been
   * initialised (eg during initsql) but you don't need the functionality.
   */
  public static TemplateMacro opt(WebMacro webmacro,
                                  String templatePath)
  {
    if (webmacro == null | templatePath == null) {
      return null;
    }
    return new TemplateMacro(webmacro, templatePath);
  }

  private final WebMacro __webmacro;
  private final String __templatePath;

  public TemplateMacro(WebMacro webmacro,
                       String templatePath)
  {
    __webmacro = Preconditions.checkNotNull(webmacro, "webmacro");
    __templatePath = Preconditions.checkNotNull(templatePath, "templatePath");
  }

  private Template getTemplate()
      throws PropertyException
  {
    try {
      return __webmacro.getTemplate(__templatePath);
    } catch (ResourceException e) {
      throw new PropertyException(String.format("Unable to resolve templatepath of %s",
                                                __templatePath),
                                  e);
    }
  }

  @Override
  public Object evaluate(Context context)
      throws PropertyException
  {
    return getTemplate().evaluateAsString(context);
  }

  @Override
  public void write(FastWriter out,
                    Context context)
      throws PropertyException, IOException
  {
    getTemplate().write(out, context);
  }

}
