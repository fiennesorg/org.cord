package org.cord.webmacro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.NotFoundException;
import org.webmacro.PropertyException;
import org.webmacro.Provider;
import org.webmacro.ResourceException;
import org.webmacro.Template;

public class TemplatesTool
  extends ContextTool
{
  @Override
  public Object init(Context c)
      throws PropertyException
  {
    try {
      return new Instance(c.getBroker().getProvider("template"));
    } catch (NotFoundException e) {
      throw new PropertyException("Unable to resolve template Provider", e);
    }
  }

  public static class Instance
  {
    private final Provider __provider;

    private Instance(Provider provider)
    {
      __provider = provider;
    }

    public Template load(String templatePath)
    {
      try {
        return (Template) __provider.get(templatePath);
      } catch (ResourceException e) {
        return null;
      }
    }

    public Template loadByDirs(String localPath,
                               Object... dirs)
    {
      for (Object dir : dirs) {
        Template t = load(dir + "/" + localPath);
        if (t != null) {
          return t;
        }
      }
      return load("node/" + localPath);
    }

    public Template loadByNextDirs(String formattedPath,
                                   Iterator<?> dirs)
    {
      while (dirs.hasNext()) {
        Object dir = dirs.next();
        String path = String.format(formattedPath, dir);
        Template t = load(path);
        if (t != null) {
          return t;
        }
      }
      return null;
    }

    public Template loadByPrevDirs(String formattedPath,
                                   ListIterator<?> dirs)
    {
      while (dirs.hasPrevious()) {
        Object dir = dirs.previous();
        String path = String.format(formattedPath, dir);
        Template t = load(path);
        if (t != null) {
          return t;
        }
      }
      return null;
    }

    public List<String> test(String formattedPath,
                             ListIterator<?> dirs)
    {
      List<String> list = new ArrayList<String>();
      while (dirs.hasPrevious()) {
        Object dir = dirs.previous();
        String path = String.format(formattedPath, dir);
        list.add(path);
      }
      return list;
    }
  }

}
