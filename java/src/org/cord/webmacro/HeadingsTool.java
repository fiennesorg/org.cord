package org.cord.webmacro;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

import com.google.common.base.Preconditions;

public class HeadingsTool
  extends ContextTool
{
  private int _depth = 1;

  @Override
  public Object init(Context c)
      throws PropertyException
  {
    return new HeadingsTool();
  }

  public String render(Object contents,
                       String cssClass)
  {
    StringBuilder buf = new StringBuilder();
    buf.append("<h").append(_depth);
    if (cssClass != null) {
      buf.append(" class=\"").append(cssClass).append('"');
    }
    buf.append(">");
    buf.append(contents);
    buf.append("</h").append(_depth).append(">\n");
    return buf.toString();
  }

  public void larger()
  {
    Preconditions.checkState(_depth > 1, "Cannot have a heading larger than h1");
    _depth--;
  }

  public void smaller()
  {
    Preconditions.checkState(_depth < 6, "Cannot have a heading smaller than h6");
    _depth++;
  }

  public int getDepth()
  {
    return _depth;
  }

  public void init()
  {
  }
}
