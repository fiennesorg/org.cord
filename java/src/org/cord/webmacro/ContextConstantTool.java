package org.cord.webmacro;

import org.cord.util.ObjectUtil;
import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

import com.google.common.base.Preconditions;

/**
 * Base implementation of ContextTool that will only create a single instance for invocations that
 * share the same common OuterVars context. This effectively gives you a page-local global. The
 * instance is stored in the Context but under a private key so it should not be touched by
 * templates and therefore should be considered trusted.
 * 
 * @author alex
 */
public abstract class ContextConstantTool<T>
  extends ContextTool
{
  private final Object __contextKey = new Object();
  private final Class<T> __instanceClass;

  public ContextConstantTool(Class<T> instanceClass)
  {
    __instanceClass = Preconditions.checkNotNull(instanceClass, "instanceClass");
  }

  @Override
  public final Object init(Context context)
      throws PropertyException
  {
    T instance = ObjectUtil.castTo(VarsTool.delegatingGet(context, __contextKey), __instanceClass);
    if (instance != null) {
      return instance;
    }
    instance = createInstance(context);
    VarsTool.setOuter(context, __contextKey, instance);
    return instance;
  }

  public abstract T createInstance(Context context);

  public static abstract class Instance
  {
    private final Context __context;

    public Instance(Context context)
    {
      __context = context;
    }

    public final Context getContext()
    {
      return __context;
    }
  }
}
