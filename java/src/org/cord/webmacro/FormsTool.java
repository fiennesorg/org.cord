package org.cord.webmacro;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cord.node.NodeRequest;
import org.cord.node.requires.RequirementMgr;
import org.cord.node.requires.Requirements;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.IsoCountryCode;
import org.cord.util.StringUtils;
import org.joda.time.DateTime;
import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.Macro;
import org.webmacro.PropertyException;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public final class FormsTool
  extends ContextTool
{
  @Override
  public Object init(Context arg0)
      throws PropertyException
  {
    return new FormsTool();
  }

  /**
   * Take an Object, and if it is a Gettable then getString(key) on it otherwise just invoke
   * padString on it instead.
   * 
   * @param key
   *          The key that will be looked up inside obj if obj is a Gettable
   * @see #padString(Object)
   */
  protected static String padOrResolveString(Object obj,
                                             String key)
  {
    if (obj instanceof Gettable) {
      return ((Gettable) obj).getString(key);
    }
    return padString(obj);
  }

  /**
   * Take an Object, filter out UndefinedMacro to null and then convert the result to a String
   * 
   * @return A String version of obj or null
   */
  protected static String padString(Object obj)
  {
    return StringUtils.toString(VarsTool.padUndefinedMacro(obj));
  }

  /**
   * Take an Object, and either return its Boolean representation or the supplied paddedValue if the
   * convertion doesn't make sense.
   * 
   * @see GettableUtils#toBoolean(Object, Boolean)
   */
  protected static boolean padBoolean(Object obj,
                                      Boolean paddedValue)
  {
    return GettableUtils.toBoolean(obj, paddedValue).booleanValue();
  }

  /**
   * Take an Object, and if it is a Gettable then getBoolean(key) on it otherwise invoke invoke
   * padBoolean instead.
   * 
   * @see #padBoolean(Object, Boolean)
   */
  protected static boolean padOrResolveBoolean(Object obj,
                                               Boolean paddedValue,
                                               String key)
  {
    if (obj instanceof Gettable) {
      Boolean value = ((Gettable) obj).getBoolean(key);
      return value == null ? paddedValue.booleanValue() : value.booleanValue();
    }
    return padBoolean(obj, paddedValue);
  }

  public Macro password(final String label,
                        final Object value,
                        final Object isCompulsoryObj,
                        final String id)
  {
    return password(label, value, null, isCompulsoryObj, id);
  }

  public Macro password(final String label,
                        final Object value,
                        final String autocomplete,
                        final Object isCompulsoryObj,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.password(autocomplete,
                           out,
                           label,
                           padOrResolveString(value, id),
                           isCompulsory,
                           id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  public Macro text(final String label,
                    final Object value,
                    final Object isCompulsoryObj,
                    final String id)
  {
    return text(label, value, null, isCompulsoryObj, id);
  }

  public Macro text(final String label,
                    final Object value,
                    final String autocomplete,
                    final Object isCompulsoryObj,
                    final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.text(autocomplete, out, label, padOrResolveString(value, id), isCompulsory, id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  /**
   * Provide a way of generating a text input box with optional autocomplete with the options stored
   * in an array. The webmacro format will be:
   * 
   * <pre>
   * $Forms.autocomplete("label", value, isCompulsoryObj, [ "option 1", "option 2" ...], "fieldName")
   * </pre>
   * 
   * @see HtmlUtils#autocomplete(Appendable, String, String, boolean, Iterable, Object...)
   */
  public Macro autocomplete(final String label,
                            final Object value,
                            final Object isCompulsoryObj,
                            final Object[] optionObjs,
                            final String id)
  {
    List<String> options = Lists.newArrayListWithCapacity(optionObjs.length);
    for (Object option : optionObjs) {
      options.add(option.toString());
    }
    return autocomplete(label, value, isCompulsoryObj, options, id);
  }

  /**
   * Provide a way of generating a text input box with optional autocomplete with the options stored
   * in an Iterable collection.
   * 
   * @see HtmlUtils#autocomplete(Appendable, String, String, boolean, Iterable, Object...)
   */
  public Macro autocomplete(final String label,
                            final Object value,
                            final Object isCompulsoryObj,
                            final Iterable<String> options,
                            final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        addRequirement(context, RequirementMgr.KEY_QUERY_UI);
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.autocomplete(out,
                               label,
                               padOrResolveString(value, id),
                               isCompulsory,
                               options,
                               id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  public Macro autocompleteInputWidget(final Object value,
                                       final Object isCompulsoryObj,
                                       final Iterable<String> options,
                                       final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        addRequirement(context, RequirementMgr.KEY_QUERY_UI);
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.autocompleteInputWidget(out, padOrResolveString(value, id), options, id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  /**
   * Resolve the {@link Requirements} from the context and {@link Requirements#needs(String)} a new
   * file key on it, failing if it is not possible to resolve the Requirements.
   */
  public static void addRequirement(Map<Object, Object> context,
                                    String key)
  {
    Object o = context.get(VarsTool.WM_REQUIREMENTS);
    if (o == null || !(o instanceof Requirements)) {
      throw new IllegalArgumentException(String.format("$includes resolves to %s which is not an Requirements",
                                                       o));
    }
    ((Requirements) o).needs(key);
  }
  /**
   * Append a static value into the form in the same markup as the rest of the form elements thereby
   * making it useful for constant values that integrate with the look and feel of the system.
   */
  public Macro value(final String label,
                     final Object value,
                     final String ids)
  {
    return new AppendableConstantMacro() {

      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.labelValue(out, label, value, ids);
      }
    };
  }

  /**
   * Show a visible representation of a value and provide a separate hidden form submission of the
   * same using {@link HtmlUtils#visible(Appendable, String, Object, Object, Object...)}
   */
  public Macro visible(final String label,
                       final Object visibleValue,
                       final Object submitValue,
                       final String ids)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.visible(out, label, visibleValue, submitValue, ids);
      }
    };
  }

  /**
   * @deprecated in preference of {@link #file(String, Object, String)}
   */
  @Deprecated
  public Macro file(final String label,
                    final Object value,
                    final Object isCompulsoryObj,
                    final String id)
  {
    return file(label, isCompulsoryObj, id);
  }

  public Macro file(final String label,
                    final Object isCompulsoryObj,
                    final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.openLabelWidget(out, label, HtmlUtilsTheme.CSS_W_FILE, isCompulsory, id);
        HtmlUtils.fileWidget(out, id);
        HtmlUtilsTheme.closeDiv(out);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }
  public Macro fileWidget(final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.fileWidget(out, id);
      }
    };
  }

  /**
   * Generate a text input widget with no surrounding divs
   **/
  public Macro textInputWidget(final Object value,
                               final String id)
  {
    return textInputWidget(value, null, id);
  }

  public Macro textInputWidget(final Object value,
                               final String cssClass,
                               final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.textInputWidget(null, out, cssClass, padOrResolveString(value, id), id);
      }
    };
  }

  public Macro textInputWidget(final Object value,
                               final String cssClass,
                               final String autocomplete,
                               final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.textInputWidget(autocomplete, out, cssClass, padOrResolveString(value, id), id);
      }
    };
  }

  /**
   * Generate a text input widget in standard widget divs to fit into a default form
   * 
   * @see HtmlUtils#textWidget(String, Appendable, String, Object...)
   */
  public Macro textWidget(final Object value,
                          final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.textWidget(null, out, padOrResolveString(value, id), id);
      }
    };
  }

  public Macro hidden(final String name,
                      final Object value)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtilsTheme.hiddenInput(out, value, name);
      }
    };
  }

  /**
   * Append a hidden input and display the value that will be submitted as a value. That way the
   * user cannot change it but they are made aware of what is going to be submitted.
   * 
   * @deprecated because we don't like having duplicate ids and we want to let themes override the
   *             implementation if necessary - use {@link #visible(String, Object, Object, String)}
   *             instead.
   */
  @Deprecated
  public Macro visibleHidden(final String label,
                             final String name,
                             final Object value)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtilsTheme.hiddenInput(out, value, name);
        HtmlUtils.labelValue(out, label, value, name);
      }
    };
  }
  /**
   * Generate a list of hidden form fields. This would let you do things like this in webmacro
   * space: <em>$Forms.hidden({"name1":"value1", "name2":"value2"})</em> which can make things much
   * more concise.
   */
  public Macro hidden(final Map<?, ?> values)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        for (Map.Entry<?, ?> entry : values.entrySet()) {
          HtmlUtilsTheme.hiddenInput(out, entry.getValue(), entry.getKey().toString());
        }
      }
    };
  }

  /**
   * Generate a hidden field to indicate that a field is being submitted regardless of whether or
   * not a value is defined. This is useful when you are manually creating a multi-valued submission
   * and you are not sure there are going to be any values. Adding in an activeIndicator will ensure
   * that {@link NodeRequest#getValues(Object)} will return an empty Collection rather than null.
   * The implementation of this is not theme specific and it assumes that theme implementations are
   * utilising {@link HtmlUtilsTheme#activeIndicator(Appendable, Object...)} when they are
   * generating multi-select widgets (which they should be).
   */
  public Macro activeIndicator(final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtilsTheme.activeIndicator(out, id);
      }
    };
  }

  public Macro textarea(final String label,
                        final Object value,
                        final Object isCompulsoryObj,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.textarea(out, label, padOrResolveString(value, id), 0, 8, isCompulsory, id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  public Macro textareaWidget(final Object value,
                              final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.textareaWidget(out, padOrResolveString(value, id), 0, 0, false, id);
      }
    };
  }

  public Macro checkbox(final String label,
                        final Object value,
                        final Object isChecked,
                        final Object shouldReloadOnChange,
                        final Object isCompulsoryObj,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.checkbox(out,
                           label,
                           StringUtils.toString(value),
                           padBoolean(isChecked, Boolean.FALSE),
                           padBoolean(shouldReloadOnChange, Boolean.FALSE),
                           isCompulsory,
                           id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  public Macro checkboxWidget(final Object value,
                              final Object isChecked,
                              final Object shouldReloadOnChange,
                              final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.checkboxInputWidget(out,
                                      StringUtils.toString(value),
                                      padBoolean(isChecked, Boolean.FALSE),
                                      padBoolean(shouldReloadOnChange, Boolean.FALSE),
                                      false,
                                      id);
      }
    };
  }

  public Macro dateSelector(final String label,
                            final Date value,
                            final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.dateSelector(out, context, label, value, id);
      }
    };
  }

  public Date parseDateSelector(Object date)
  {
    return HtmlUtils.parseDateSelector(date);
  }

  public String formatDateSelector(Date date)
  {
    return HtmlUtils.formatDateSelector(date);
  }

  public Macro jodaDate(final String label,
                        final DateTime value,
                        final boolean isCompulsory,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.jodaDate(out, context, label, Optional.fromNullable(value), isCompulsory, id);
      }
    };
  }

  /**
   * Render a JODA Date widget where the String value is parsed via
   * {@link HtmlUtils#jodaDateParse(String)}
   */
  public Macro jodaDate(final String label,
                        final Object value,
                        final boolean isCompulsory,
                        final String id)
  {
    DateTime dateTime = null;
    if (value != null) {
      if (value instanceof DateTime) {
        dateTime = (DateTime) value;
      } else if (value instanceof Optional) {
        Optional<?> optional = (Optional<?>) value;
        if (optional.isPresent()) {
          return jodaDate(label, optional.get(), isCompulsory, id);
        }
      } else {
        Optional<DateTime> parsedDateTime =
            HtmlUtils.jodaDateParse(StringUtils.toString(value, ""));
        if (parsedDateTime.isPresent()) {
          dateTime = parsedDateTime.get();
        }
      }
    }
    return jodaDate(label, dateTime, isCompulsory, id);
  }

  public Macro jodaDate(String label,
                        Object value,
                        String id)
  {
    return jodaDate(label, value, false, id);
  }

  public Macro jodaTime(final String label,
                        final Object value,
                        final boolean isCompulsory,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.jodaTime(out,
                           context,
                           label,
                           HtmlUtils.jodaTimeParse(StringUtils.toString(value, "")),
                           isCompulsory,
                           id);
      }
    };
  }

  public Macro jodaDate(final String label,
                        final Optional<DateTime> value,
                        final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.jodaDate(out, context, label, value, false, id);
      }
    };
  }

  public Macro submit(final String value,
                      final Object onclick)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        Set<Object> compulsoryFields = getCompulsoryFields(context);
        if (compulsoryFields != null && compulsoryFields.size() != 0) {
          HtmlUtils.getTheme().compulsorySubmitButton(out, value, context, compulsoryFields);
        } else {
          HtmlUtils.submit(out, value, padString(onclick));
        }
      }
    };
  }

  public Macro submitDirect(final String value)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.submit(out, value, "");
      }
    };
  }

  /**
   * Create a submit button that has no onclick action
   */
  public Macro submit(final String value)
  {
    return submit(value, null);
  }

  public Macro cancel(final String message,
                      final String url)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.getTheme().cancelButton(out, message, url);
      }
    };
  }

  public Macro openRadios(final String label)
  {
    return new AppendableParametricMacro() {

      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.openRadios(out, label);
      }
    };
  }

  public Macro closeRadios()
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.closeRadios(out);
      }
    };
  }

  public Macro radio(final Object name,
                     final Object value,
                     final Object isChecked,
                     final Object label,
                     final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.radio(out,
                        padString(name),
                        padString(value),
                        padBoolean(isChecked, Boolean.FALSE),
                        padString(label),
                        id);
      }
    };
  }

  public Macro openSelect(final String label,
                          final Object isMultiple,
                          final Object shouldReloadOnChange,
                          final Object isCompulsoryObj,
                          final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        boolean isCompulsory = padBoolean(isCompulsoryObj, Boolean.FALSE);
        HtmlUtils.openLabelWidget(out, label, HtmlUtilsTheme.CSS_W_SELECT, isCompulsory, id);
        HtmlUtils.openSelect(out,
                             padBoolean(isMultiple, Boolean.FALSE),
                             padBoolean(shouldReloadOnChange, Boolean.FALSE),
                             isCompulsory,
                             id);
        if (isCompulsory) {
          addCompulsoryField(context, id);
        }
      }
    };
  }

  public Macro openSelectWidget(final Object isMultiple,
                                final Object shouldReloadOnChange,
                                final String id)
  {
    return openSelectWidget(isMultiple, shouldReloadOnChange, null, id);
  }

  public Macro openSelectWidget(final Object isMultiple,
                                final Object shouldReloadOnChange,
                                final String customCssClass,
                                final String id)
  {
    return openSelectWidget(isMultiple, shouldReloadOnChange, Boolean.FALSE, customCssClass, id);
  }

  public Macro openSelectWidget(final Object isMultiple,
                                final Object shouldReloadOnChange,
                                final Object isCompulsory,
                                final String customCssClass,
                                final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        HtmlUtils.openSelectWidget(out,
                                   customCssClass,
                                   padBoolean(isMultiple, Boolean.FALSE),
                                   padBoolean(shouldReloadOnChange, Boolean.FALSE),
                                   padBoolean(isCompulsory, Boolean.FALSE),
                                   id);
      }
    };
  }

  /**
   * @param isSelectedObj
   *          If this is a Boolean then it represents whether or not the option is selected. Any
   *          other value, it is compared to the value (or contents if value is undefined) and if
   *          they are equal then the option is selected.
   */
  public Macro option(final Object value,
                      final String contents,
                      final Object isSelectedObj)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        final String paddedValue =
            StringUtils.toString(VarsTool.padUndefinedMacro(value, contents));
        HtmlUtils.option(out, paddedValue, contents, isSelected(isSelectedObj, paddedValue));
      }
    };
  }

  public Macro option(final Object value,
                      final Object isSelectedObj)
  {
    return option(value, value.toString(), isSelectedObj);
  }

  /**
   * Create a new empty Option field that is not selected by default and which defines no value.
   */
  public String option()
  {
    return "<option></option>";
  }

  private boolean isSelected(Object isSelectedObj,
                             Object value)
  {
    if (isSelectedObj == null) {
      return false;
    }
    if (isSelectedObj instanceof Boolean) {
      return ((Boolean) isSelectedObj).booleanValue();
    }
    if (isSelectedObj instanceof Collection<?>) {
      return ((Collection<?>) isSelectedObj).contains(value);
    }
    if (isSelectedObj.equals(value)) {
      return true;
    }
    return isSelectedObj.toString().equals(value.toString());
  }

  private boolean isSelectedEnum(Object isSelectedObj,
                                 Enum<?> value)
  {
    if (isSelected(isSelectedObj, value.name())) {
      return true;
    }
    return isSelected(isSelectedObj, value);
  }

  /**
   * Append an option that derives its name and value from an Enum, and the isSelectedObj can either
   * be an Enum value or the String representation of the name of the Enum value.
   */
  public Macro option(final Enum<?> value,
                      final Object isSelectedObj)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.option(out, value.name(), value.toString(), isSelectedEnum(isSelectedObj, value));
      }
    };
  }

  /**
   * Create an option with the same value as contents that is unselected.
   */
  public Macro option(final String contents)
  {
    return option(null, contents, Boolean.FALSE);
  }

  public Macro closeSelect(final Object summary)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.closeSelect(out, padString(summary));
        HtmlUtilsTheme.closeDiv(out);
      }
    };
  }

  public Macro closeSelect()
  {
    return closeSelect(null);
  }

  public Macro closeSelectWidget()
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.closeSelectWidget(out);
      }
    };
  }

  public Macro closeSelectWidget(final Object summary)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.closeSelect(out, padString(summary));
      }
    };
  }

  public Macro label(final String label,
                     final Object isCompulsory,
                     final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.label(out, label, padBoolean(isCompulsory, Boolean.FALSE), id);
      }
    };
  }

  public Macro openLabelWidget(final Object label,
                               final Object widget,
                               final Object isCompulsory,
                               final String id)
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        HtmlUtils.openLabelWidget(out,
                                  padString(label),
                                  padString(widget),
                                  padBoolean(isCompulsory, Boolean.FALSE),
                                  id);
      }
    };
  }

  public String urlEncode(Object source)
  {
    return HtmlUtilsTheme.urlEncode(source.toString());
  }

  public List<IsoCountryCode> getIsoCountryCodes()
  {
    return IsoCountryCode.getCountryCodes();
  }

  public static final String WEBMACRO_W_COMPULSORYFIELDS = "w_compulsoryFields";

  /**
   * Open a new POST form with a choice of isMultipart.
   * 
   * @param legend
   *          The optional legend. null or empty will result in no legend tag
   * @see #closePostForm()
   */
  public Macro openPostForm(final String legend,
                            final boolean isMultipart,
                            final String url,
                            final String id)
  {
    return openPostForm(legend, null, isMultipart, url, id);
  }

  public Macro openPostForm(final String legend,
                            final String fieldsetClass,
                            final boolean isMultipart,
                            final String url,
                            final String id)
  {
    return openPostForm(legend, null, fieldsetClass, isMultipart, url, id);
  }

  /**
   * Open a new POST form with a choice of isMultipart and form and fieldset classes.
   * 
   * @param legend
   *          The optional legend. null or empty will result in no legend tag
   * @param formClass
   *          The optional CSS class applied to the form element. If null then no class is applied.
   * @param fieldsetClass
   *          The optional CSS class applied to the fieldset. If null then no class is applied.
   */
  public Macro openPostForm(final String legend,
                            final String formClass,
                            final String fieldsetClass,
                            final boolean isMultipart,
                            final String url,
                            final String id)
  {
    return new AppendableParametricMacro() {
      @Override
      public void append(Appendable out,
                         Map<Object, Object> context)
          throws IOException
      {
        addCompulsoryFieldSet(context);
        out.append("<form");
        HtmlUtilsTheme.addAttribute(out, "class", formClass);
        HtmlUtilsTheme.addAttribute(out, "method", "post");
        HtmlUtilsTheme.addAttribute(out, "action", url);
        HtmlUtilsTheme.addAttribute(out, "id", id);
        if (isMultipart) {
          HtmlUtilsTheme.addAttribute(out, "enctype", "multipart/form-data");
        }
        out.append("><fieldset");
        HtmlUtilsTheme.addAttribute(out, "class", fieldsetClass);
        out.append(">\n");
        String legendString = StringUtils.toString(legend);
        if (!Strings.isNullOrEmpty(legendString)) {
          out.append("<legend>").append(legendString).append("</legend>\n");
        }
      }
    };
  }

  public Macro openPostFormClass(final String legend,
                                 final String formClass,
                                 final String url,
                                 final String id)
  {
    return openPostFormClass(legend, formClass, false, url, id);
  }

  public Macro openPostFormClass(final String legend,
                                 final String formClass,
                                 final boolean isMultipart,
                                 final String url,
                                 final String id)
  {
    return openPostForm(legend, formClass, null, isMultipart, url, id);
  }

  /**
   * Open a non-multipart POST form.
   * 
   * @param legend
   *          The optional legend. null or empty will result in no legend tag
   */
  public Macro openPostForm(final String legend,
                            final String url,
                            final String id)
  {
    return openPostForm(legend, false, url, id);
  }

  public static final String WEBMACRO_W_HASINCLUDEDFORMVALIDATOR = "w_hasIncludedFormValidator";

  private void addCompulsoryFieldSet(Map<Object, Object> context)
  {
    context.put(WEBMACRO_W_COMPULSORYFIELDS, new HashSet<Object>());
  }

  @SuppressWarnings("unchecked")
  private Set<Object> getCompulsoryFields(Map<Object, Object> context)
  {
    return (Set<Object>) context.get(WEBMACRO_W_COMPULSORYFIELDS);
  }

  private void addCompulsoryField(Map<Object, Object> context,
                                  String fieldId)
  {
    Set<Object> compulsoryFields = getCompulsoryFields(context);
    if (compulsoryFields != null) {
      compulsoryFields.add(fieldId);
    }
  }

  /**
   * Close the fieldset and form that was opened by openPostForm
   * 
   * @see #openPostForm(String, String, String)
   * @see #openPostForm(String, boolean, String, String)
   */
  public Macro closePostForm()
  {
    return new AppendableConstantMacro() {
      @Override
      public void append(Appendable out)
          throws IOException
      {
        out.append("</fieldset></form>");
      }
    };
  }
}
