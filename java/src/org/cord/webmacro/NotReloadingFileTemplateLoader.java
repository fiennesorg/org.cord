package org.cord.webmacro;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.resource.AbstractTemplateLoader;
import org.webmacro.resource.CacheElement;
import org.webmacro.resource.FileTemplateLoader;

public class NotReloadingFileTemplateLoader
  extends AbstractTemplateLoader
{

  static Logger _log = LoggerFactory.getLogger(FileTemplateLoader.class);

  private String path;

  @Override
  public void setConfig(String config)
  {
    // leading slash isn't needed, because
    // we use File constructor.
    this.path = config;

    // However, we can check, if the directory exists.
    File f = new File(path);
    if (!f.exists()) {
      _log.warn("FileTemplateLoader: " + f.getAbsolutePath() + " does not exist.");
    } else if (!f.isDirectory()) {
      _log.warn("FileTemplateLoader: " + f.getAbsolutePath() + " is not a directory.");
    }
  }

  /**
   * Tries to load a template by interpreting query as a path relative to the path set by setPath.
   */
  @Override
  public final Template load(String query,
                             CacheElement ce)
      throws ResourceException
  {
    File tFile = new File(path, query);
    if (tFile.isFile() && tFile.canRead()) {
      _log.debug("FileTemplateProvider: Found template " + tFile.getAbsolutePath());
      return helper.load(tFile, ce);
    } else {
      return null;
    }
  }

}