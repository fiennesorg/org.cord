package org.cord.webmacro;

import org.cord.mirror.Table;
import org.webmacro.resource.CacheReloadContext;

/**
 * Implmentation of CacheReloadContext that utilises the {@link Table#getLastChangeTime()} value on
 * a nested Table to decide whether or not a Template should be reloaded.
 * 
 * @author alex
 */
public class TableReloadContext
  extends CacheReloadContext
{
  private final Table __table;
  private final long __lastChangeTime;

  public TableReloadContext(Table table)
  {
    __table = table;
    __lastChangeTime = table.getLastChangeTime();
  }

  @Override
  public boolean shouldReload()
  {
    return __table.getLastChangeTime() != __lastChangeTime;
  }

}
