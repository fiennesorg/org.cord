package org.cord.webmacro;

import java.io.IOException;

import org.cord.util.StringBuilderIOException;
import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;

/**
 * An abstract class that makes it easy to implement Macro's that can append their output onto an
 * Appendable without needing any state that is stored in the Context. It is safe to utilise these
 * outside the scope of webmacro because you don't have to worry about defining a Context to
 * evaluate it.
 * 
 * @author alex
 */
public abstract class AppendableConstantMacro
  implements Macro
{
  public abstract void append(Appendable out)
      throws IOException;

  @Override
  public final void write(FastWriter out,
                          Context context)
      throws PropertyException, IOException
  {
    append(out);
  }

  @Override
  public final Object evaluate(Context context)
      throws PropertyException
  {
    return toString();
  }

  @Override
  public final String toString()
  {
    StringBuilder buf = new StringBuilder();
    try {
      append(buf);
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
    return buf.toString();
  }
}
