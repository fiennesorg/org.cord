package org.cord.webmacro;

import org.webmacro.resource.CacheReloadContext;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

/**
 * Implementations of CacheReloadContext that always queries a Supplier for the webmacro source
 * String, and says to reload if the hashcode() of the source is different from the previously
 * loaded value. This can be useful in situations where getting at the value is easier than getting
 * at the time when it was last updated and you still want to avoid re-parsing Templates
 * unnecessarily.
 * 
 * @author alex
 */
public class CachingReloadContext
  extends CacheReloadContext
{
  private final Supplier<String> __source;
  private volatile long _hashcode = 0;

  public CachingReloadContext(Supplier<String> source)
  {
    __source = Preconditions.checkNotNull(source, "source");
  }

  @Override
  public boolean shouldReload()
  {
    String source = __source.get();
    int hashcode = source.hashCode();
    if (_hashcode == hashcode) {
      return false;
    }
    _hashcode = hashcode;
    return true;

  }

}
