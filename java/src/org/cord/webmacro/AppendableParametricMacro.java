package org.cord.webmacro;

import java.io.IOException;
import java.util.Map;

import org.cord.util.StringBuilderIOException;
import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;

/**
 * An abstract class that makes it easy to implement Macro's that can append their output onto an
 * Appendable based around the contents of a Map of configuration state. This Map may or may be a
 * Context depending on how the doWork method is invoked.
 * 
 * @author alex
 */
public abstract class AppendableParametricMacro
  implements Macro
{
  public abstract void append(Appendable out,
                              Map<Object, Object> context)
      throws IOException;

  @Override
  public final void write(FastWriter out,
                          Context context)
      throws PropertyException, IOException
  {
    append(out, context);
  }

  @Override
  public final Object evaluate(Context context)
      throws PropertyException
  {
    StringBuilder buf = new StringBuilder();
    try {
      append(buf, context);
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
    return buf.toString();
  }
}
