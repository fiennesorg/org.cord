package org.cord.webmacro;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

/**
 * ContextTool to enable loading of Classes by their qualified name. This is primarily intended for
 * invoking static methods on the classes, but I still need to think about this a bit more...
 * 
 * @author alex
 */
public class ClassTool
  extends ContextTool
{
  private final static ClassTool INSTANCE = new ClassTool();

  @Override
  public Object init(Context c)
      throws PropertyException
  {
    return INSTANCE;
  }

  /**
   * resolve the Class object for the given qualifiedName. Note that if you want access to inner
   * classes then you should use "$" between the outer and inner classes and you will have to escape
   * it to keep webmacro from trying to be smart, eg
   * 
   * <pre>
   * $Class.get("org.acme.Outer\$Inner")
   * </pre>
   */
  public Class<?> get(String qualifiedName)
      throws ClassNotFoundException
  {
    return Class.forName(qualifiedName);
  }

}
