package org.cord.webmacro;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.cord.util.Assertions;
import org.cord.util.NamedImpl;
import org.cord.webmacro.VarsTool.BlockWrapper;
import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;
import org.webmacro.engine.StringMacroAdapter;

import com.google.common.base.Preconditions;

/**
 * An ordered container of Macros that are also stored by name and which supports replacement of
 * Macros by name. Evaluating the NamedMacroList gives you the result of evaluating the (insertion)
 * ordered list of Macros. This can be used to create extendable templates where sub-templates
 * extend rather than replace functionality.
 * 
 * @see GridsTool
 * @author alex
 */
public class NamedMacroList
  extends NamedImpl
  implements Macro
{
  public NamedMacroList(String name)
  {
    super(name);
  }

  private final LinkedHashMap<String, Macro> __macros = new LinkedHashMap<String, Macro>();

  public void add(String name,
                  Object macro)
  {
    Assertions.notEmpty(name, "name");
    Preconditions.checkNotNull(macro, "macro");
    __macros.put(name, VarsTool.asMacro(macro));
  }

  public void add(String name,
                  String contents)
  {
    add(name, new StringMacroAdapter(contents));
  }

  public void add(String name,
                  BlockWrapper blockWrapper)
  {
    add(name, blockWrapper.eval());
  }

  public void remove(String name)
  {
    __macros.remove(name);
  }

  @Override
  public Object evaluate(Context context)
      throws PropertyException
  {
    StringBuilder buf = new StringBuilder();
    for (Macro Macro : __macros.values()) {
      buf.append(Macro.evaluate(context));
    }
    return buf.toString();
  }

  @Override
  public void write(FastWriter out,
                    Context context)
      throws PropertyException, IOException
  {
    for (Macro Macro : __macros.values()) {
      Macro.write(out, context);
    }
  }
}
