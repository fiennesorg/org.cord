package org.cord.webmacro;

import org.webmacro.Context;
import org.webmacro.ContextTool;
import org.webmacro.PropertyException;

public class HtmlTool
  extends ContextTool
{
  private static final HtmlTool INSTANCE = new HtmlTool();

  @Override
  public Object init(Context c)
      throws PropertyException
  {
    return INSTANCE;
  }

  public TagBuilder newTagBuilder(String name)
  {
    return new TagBuilder(name);
  }
}
