package org.cord.webmacro;

import java.io.IOException;

import org.webmacro.Context;
import org.webmacro.FastWriter;
import org.webmacro.Macro;
import org.webmacro.PropertyException;

/**
 * Implementation of a Macro that does nothing when evaluated. This can be used as a return value
 * from methods that are expecting to be invoked from webmacro space and which we don't want to have
 * a visible effect without having to create loads of pointless little classes and objects.
 */
public class EmptyMacro
  implements Macro
{
  private static final EmptyMacro INSTANCE = new EmptyMacro();

  public static EmptyMacro getInstance()
  {
    return INSTANCE;
  }

  private EmptyMacro()
  {
  }

  /**
   * @return ""
   */
  @Override
  public Object evaluate(Context context)
      throws PropertyException
  {
    return "";
  }

  /**
   * Do nothing.
   */
  @Override
  public void write(FastWriter out,
                    Context context)
      throws PropertyException, IOException
  {
  }

}
