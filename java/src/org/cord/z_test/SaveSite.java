package org.cord.z_test;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.AppView;
import org.cord.node.app.ViewAuthenticator;
import org.cord.node.json.JSONSaver;
import org.cord.util.StringUtils;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

public class SaveSite<A extends AppMgr>
  extends AppView<A>
{
  public static final String DIRNAME = "dirName";

  public static final String DIRNAME_DEFAULT = "SavedSite";

  public SaveSite(A appMgr,
                  String name,
                  String title,
                  ViewAuthenticator<A> viewAuthenticator,
                  PostViewAction postViewAction)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator,
          postViewAction);
  }

  public SaveSite(A appMgr,
                  ViewAuthenticator<A> viewAuthenticator)
  {
    this(appMgr,
         "SaveSite",
         "Save Site as JSON",
         viewAuthenticator,
         InternalSuccessOperation.getInstance());
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String dirName =
        MoreObjects.firstNonNull(Strings.emptyToNull(StringUtils.trim(nodeRequest.getString(DIRNAME))),
                                 DIRNAME_DEFAULT);
    try {
      JSONSaver jSaver = getNodeMgr().getJMgr().createSaver(dirName);
      jSaver.save(node);
      jSaver.saveIds();
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(), node, e, "Failed to save JSON state");
    }
    throw new FeedbackErrorException(getTitle(), node, "Save completed");
  }

}
