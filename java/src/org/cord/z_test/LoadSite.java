package org.cord.z_test;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.PostViewAction;
import org.cord.node.app.AppMgr;
import org.cord.node.app.AppView;
import org.cord.node.app.ViewAuthenticator;
import org.cord.node.json.JSONLoader;
import org.cord.util.StringUtils;
import org.webmacro.Context;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

public class LoadSite<A extends AppMgr>
  extends AppView<A>
{
  public static final String DIRNAME = SaveSite.DIRNAME;

  public LoadSite(A appMgr,
                  String name,
                  String title,
                  ViewAuthenticator<A> viewAuthenticator,
                  PostViewAction postViewAction)
  {
    super(appMgr,
          name,
          title,
          viewAuthenticator,
          postViewAction);
  }

  public LoadSite(A appMgr,
                  ViewAuthenticator<A> viewAuthenticator)
  {
    this(appMgr,
         "LoadSite",
         "Load Site from JSON",
         viewAuthenticator,
         InternalSuccessOperation.getInstance());
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String dirName =
        MoreObjects.firstNonNull(Strings.emptyToNull(StringUtils.trim(nodeRequest.getString(DIRNAME))),
                                 SaveSite.DIRNAME_DEFAULT);
    JSONLoader jLoader = getNodeMgr().getJMgr().createLoader(dirName);
    try {
      jLoader.loadRecords(getNodeMgr().getNode());
    } catch (Exception e) {
      throw new FeedbackErrorException(getTitle(), node, e, "Failed to load JSON state");
    }
    if (jLoader.getProblemRecords().size() > 0) {
      jLoader.processProblemRecords();
    }
    throw new FeedbackErrorException(getTitle(),
                                     node,
                                     "Load completed: problem records: %s",
                                     jLoader.getProblemRecords());
  }

}
