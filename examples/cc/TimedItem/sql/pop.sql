-- Events
insert into NodeGroupStyle values (3010,'Events', 1,3010, 5,1, 1, 0,-1, 0,0);
insert into NodeStyle values (3010,'Events', 'Events',1, 1, 2,1, 5,1, 5,1, 5,1, '', '', 'app/timedItems.view.wm.html','app/page.edit.wm.html',0,0,1,1);
insert into RegionStyle values (0,3010,1,'headerDoc','','Header Content','GNTML','mainOptDoc',0,0);
insert into RegionStyle values (0,3010,1,'footerDoc','','Footer Content','GNTML','mainOptDoc',0,0);
insert into RegionStyle values (0,3010,1,'noNewsDoc','','No News Content','GNTML','mainOptDoc',0,0);

  -- <news-item>
  insert into NodeGroupStyle values (3011,'Event', 3010,3011, 5,1, 1, 0,-1, 0,0);
  insert into NodeStyle values (3011,'Event', 'Event',1, -1, 2,1, 5,1, 5,1, 5,1, '', '', 'app/timedItems/timedItem.view.wm.html','app/page.edit.wm.html',0,0,1,0);
  insert into RegionStyle values (0,3011,1,'timedItem','','Event Details','TimedItem','compulsory',0,0);
  insert into RegionStyle values (0,3011,1,'mainDoc','','Main Content','GNTML','mainCompDoc',0,0);

    -- <content>
    insert into NodeGroupStyle values (3012,'Content', 3011,2, 5,1, 1, 0,-1, 0,0);

  -- RSS
  insert into NodeGroupStyle values (3013, 'rss.xml', 3010,3013, 1,1, 1, 1,1, 0,0);
  insert into NodeStyle values (3013, 'rss.xml', 'rss.xml',0, 1, 2,1, 1,1, 1,1, 1,1, '', 'app/timedItems/rss.wm.xml', 'app/timedItems/rss.wm.xml', 'app/timedItems/rss.wm.xml', 1,1,0,0 );
